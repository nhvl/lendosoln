﻿var app = angular.module('ImportFromSymitar', []);

app.controller('ImportFromSymitarController', function ($scope) {
    $scope.validateBorrowerInformation = function () {
        var borrowerMemberId = document.getElementById('aBCoreSystemId');

        if (borrowerMemberId.value === null || $.trim(borrowerMemberId.value) === '') {
            $('#ErrorMessage').text('Invalid Member ID. Please verify the borrower\'s member ID.');

            borrowerMemberId.focus();

            return false;
        }

        return true;
    }

    $scope.getAllFormValues = function () {
        var args = {
            loanId: ML.sLId,
            applicationId: ML.aAppId,
            sFileVersion: $('#sFileVersion').val(),
            aBCoreSystemId: $('#aBCoreSystemId').val(),
            aCCoreSystemId: $('#aCCoreSystemId').val(),
            viewModelJson: angular.toJson($scope.viewModel)
        };

        return args;
    }
    
    $scope.setErrorResultDisplay = function (result) {
        var errorMessage = result.UserMessage ||
            result.value['ErrorMessage'] ||
            'An import error has occurred. Please contact your LendingQB system administrator if this issue persists';

        $('#ErrorMessage').text(errorMessage);

        $scope.viewModel = {
            BorrowerImportMappingKey: null,
            BorrowerImportTableList: [],
            CoborrowerImportMappingKey: null,
            CoborrowerImportTableList: []
        };
    }

    $scope.onIncludeChanged = function (useImportValue, type, $index, currentValue, importValue) {
        var row = $('#' + type + 'Row' + $index);

        if (useImportValue) {
            row.addClass($scope.getDiscrepancyClassName(currentValue, importValue));
        }
        else {
            row.removeClass('ValueDiscrepancy');
        }
    }

    $scope.getRowClassName = function ($index, useImport, currentValue, importValue) {
        var className = $index % 2 == 0 ? 'GridItem' : 'GridAlternatingItem';

        if (useImport) {
            className = className + ' ' + $scope.getDiscrepancyClassName(currentValue, importValue);
        }

        return className;
    }

    $scope.getDiscrepancyClassName = function (currentValue, importValue) {
        var hasDiscrepancy = currentValue !== '' &&
                    currentValue.toUpperCase() !== importValue.toUpperCase();

        return hasDiscrepancy ? 'ValueDiscrepancy' : '';
    }

    $scope.selectAll = function (type, datasource, useImport) {
        for (var i = 0; i < datasource.length; i++) {
            datasource[i].Include = useImport;

            $scope.onIncludeChanged(useImport, type, i, datasource[i].CurrentValue, datasource[i].ImportValue);
        }
    }

    $scope.load = function () {
        $('#ErrorMessage').text('');

        if ($scope.validateBorrowerInformation() === false) {
            return;
        }

        $('#ApplyBtn').prop('disabled', true);

        gService.loanedit.callAsyncSimple('LoadDataFromSymitar', $scope.getAllFormValues(), function (result) {
            if (result.error === false) {
                if (result.value['Status'] === 'OK') {
                    $scope.viewModel = angular.fromJson(result.value['ReturnViewModel']);

                    $('#ApplyBtn').prop('disabled', false);
                }
                else {
                    // Wait briefly before displaying the error
                    // message so that the label in the UI resets.
                    window.setTimeout(function () {
                        $scope.setErrorResultDisplay(result);

                        $('#ApplyBtn').prop('disabled', true);
                    }, 300);
                }
            }
            else {
                window.setTimeout(function () {
                    $scope.setErrorResultDisplay(result);

                    $('#ApplyBtn').prop('disabled', true);
                }, 300);
            }

            $scope.$apply();
        });
    }

    $scope.apply = function () {
        $('#ErrorMessage').text('');

        gService.loanedit.callAsyncSimple('ApplyDataToLoanFile', $scope.getAllFormValues(), function (result) {
            if (result.error === false) {
                if (result.value['Status'] === 'OK') {
                    $scope.viewModel = angular.fromJson(result.value['ReturnViewModel']);

                    if (typeof window.parent !== 'undefined' &&
                        typeof window.parent.info !== 'undefined' &&
                        typeof window.parent.info.updateApplicantsForceRefresh === 'function') {
                        window.parent.info.updateApplicantsForceRefresh();
                    }
                }
                else {
                    // Wait briefly before displaying the error
                    // message so that the label in the UI resets.
                    window.setTimeout(function () {
                        $scope.setErrorResultDisplay(result);
                    }, 300);
                }
            }
            else {
                window.setTimeout(function () {
                    $scope.setErrorResultDisplay(result);
                }, 300);
            }

            $scope.$apply();
        });
    }

    $scope.init = function () {
        $('#ApplyBtn').prop('disabled', true);

        $scope.viewModel = {
            BorrowerImportMappingKey: null,
            BorrowerImportTableList: [],
            CoborrowerImportMappingKey: null,
            CoborrowerImportTableList: []
        };
    }

    $scope.init();
});

app.directive('importTable', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: { type: '@', datasource: '=' },
        link: function (scope, element, attrs) {
            // To keep control logic out of this directive,
            // we'll have methods in the directive's scope
            // point to the parent's controller.
            scope.selectAll = scope.$parent.selectAll;
            scope.getRowClassName = scope.$parent.getRowClassName;
            scope.onIncludeChanged = scope.$parent.onIncludeChanged;
        },
        templateUrl: 'SymitarPartials/ImportRow.html'
    };
});