var gCurrencyNegativeColor = 'red';
var gCurrencyColor = 'black';
function _initMask(o) {
    //In the mask, #=digit, ?=alphanumeric, *=digit or '*', ^ = alphanumeric, space, underscore, some punctuation
  switch (o.getAttribute("preset")) {
      case "zipcode":
          o.setAttribute("mask", "#####");
          o.maxLength = 5;
          o.style.width = 50;
          // Due to new script of get/set cursor location in textbox, there are couple
          // of problems if apply format on init. 1) Dirty bit will be set incorrectly on
          // first load. 2) Last input will have focus by default.
          // The easiest solution is to disable format on init. dd 9/2/2003
          //if (o.value != '') mask_keyup(o);
              addEventHandler(o, "keyup", mask_keyup, false);
          break;
      case "bankaccountnum":
          o.setAttribute("mask", "####################");
          o.maxLength = 20;
              addEventHandler(o, "keyup", mask_keyup, false);
          break;
      case "bankroutingnum":
          o.setAttribute("mask", "#########");
          o.maxLength = 9;
              addEventHandler(o, "keyup", mask_keyup, false);
          break;
    case "longzipcode":
        o.setAttribute("mask", "#####-####");
        o.maxLength = 10;
        o.setAttribute("noTrail", true);
            addEventHandler(o, "keyup", mask_keyup, false);
        break;
    case "ssn" :
      o.maxLength = 11;
      o.setAttribute("mask", "***-**-####");
      o.style.width=90;
      //if (o.value != '') mask_keyup(o);
          addEventHandler(o, "keyup", mask_keyup, false);
      break;
    case "ssn-last-four":
        o.maxLength = 4;
        o.setAttribute("mask", "####");
        if (o.attachEvent)
            o.attachEvent("onkeyup", mask_keyup);
        else
            o.addEventListener("keyup", mask_keyup, false);
        break;
    case "phone" :
      o.setAttribute("mask", "(###) ###-####???????");
      o.maxLength = 21;
      o.style.width = 120;
          addEventHandler(o, "keyup", mask_keyup, false);
      //if (o.value != '') mask_keyup(o);
      break;
    case "mersorgid":
        o.setAttribute("mask", "#######");
        o.maxLength = 7;
            addEventHandler(o, "keyup", mask_keyup, false);
        break;
    case "nmlsID":
      //This should match NMLS IDs, which aren't supposed to get longer than 12 digits. Let's make it 13 for fun.
      o.setAttribute("mask", "#############");
      o.maxLength = 13;
          addEventHandler(o, "keyup", mask_keyup, false);
      break;
  case "words":
      //Match some words - alphanumerics, plus punctuation, plus space and underscore.
      if ((typeof o.maxLength == 'undefined') && (typeof o.maxlength == 'undefined')) //If they didn't specify a max length, use a reasonable one.
          o.maxLength = 200;
      else  //Stupid lower case - o.maxlength is actually a default property of a text area, so we want to handle that if it's defined.
          o.maxLength = o.getAttribute('maxlength') || o.getAttribute('maxLength');
      o.setAttribute("mask", "");
      for (var i = 0; i < o.maxLength; i++) // build up the string - inefficient, but it's in js already.
          o.setAttribute("mask", o.getAttribute("mask") + "^");
          addEventHandler(o, "keyup", mask_keyup, false);
      break;
  case "date":
      o.maxLength = 15;
      o.style.width = 75;
      if (o.value != '') date_onblur(null, o);
          addEventHandler(o, "keyup", date_keyup, false);
          addEventHandler(o, "blur", date_onblur, false);

      break;
  case "percent":
      o.style.width = 70;
      // Don't format when first load. Assume initial value already formated.
      //if (o.value == '') o.value = '0.000%';
      //else percent_onblur(o);
      if (window.jQuery) jQuery(o).bind('blur.mask', function() { percent_onblur(null, this) });
      else {
              addEventHandler(o, "blur", percent_onblur, false);
      }
      break;
  case "money":
      o.style.width = 90;
      o.style.textAlign = "right";
      // Don't format when first load. Assume initial value already formated.
      //if (o.value == '') o.value = '$0.00';
      //else money_onblur(o);
      if (window.jQuery) jQuery(o).bind('blur.mask', function() { money_onblur(null, this) });
      else {
              addEventHandler(o, "blur", money_onblur, false);
      }
      break;
  case "dollar":
      o.setAttribute("decimalDigits", 0);
      o.style.width = 90;
      o.style.textAlign = "right";
      // Don't format when first load. Assume initial value already formated.
      //if (o.value == '') o.value = '$0.00';
      //else money_onblur(o);
      if (window.jQuery) jQuery(o).bind('blur.mask', function() { money_onblur(null, this) });
      else {
              addEventHandler(o, "blur", money_onblur, false);
      }
      break;
  case "alphanumeric":
      var len = (+o.preset_len) || 1;
      var maskBuild = [];
      while (len--) maskBuild.push('?');
      o.setAttribute("mask", maskBuild.join(''));
      o.maxLength = maskBuild.length;
          addEventHandler(o, "keyup", mask_keyup, false);
      break;
  case "numeric": // generic number string with commas and optional decimal
      o.style.width = 90;
      o.style.textAlign = "right"
          addEventHandler(o, "blur", numeric_onblur, false);
      break;
  case "price":
      o.setAttribute("decimalDigits", 6);
      if (window.jQuery) jQuery(o).bind('blur.mask', function() { price_onblur(null, this) });
      else {
              addEventHandler(o, "blur", price_onblur, false);
      }
      break;
  case "coupon":
      o.setAttribute("decimalDigits", 2);
      if (window.jQuery) jQuery(o).bind('blur.mask', function() { price_onblur(null, this) });
      else {
              addEventHandler(o, "blur", price_onblur, false);
      }
      break;
  case "usdaBorrowerId":
      // Usda Borrower Id is 9 digits.
      var UsdaBorrowerIdMask = "#########"
      o.setAttribute("mask", UsdaBorrowerIdMask);
      o.maxLength = UsdaBorrowerIdMask.length;
          addEventHandler(o, "keyup", mask_keyup, false);
      break;
  case "employerIdentificationNumber" :
      o.setAttribute('oldMaxLength', o.maxLength);
      o.maxLength = 10;
      o.setAttribute("mask", "##-#######");
          addEventHandler(o, "keyup", mask_keyup, false);
      break;
      case "curiskscore":
          o.maxLength = 3;
              addEventHandler(o, "keyup", oninput_CuRiskScore);
          break;
    case "custom":
        var customMask = o.getAttribute("mask");
        if (customMask != null && customMask.length > 0)
        {
                addEventHandler(o, "keyup", mask_keyup, false);
            break;
        }

}

  o.value += '';
}

function _removeMask(o) {
    switch (o.getAttribute("preset")) {
        case "employerIdentificationNumber":
            o.maxLength = o.getAttribute('oldMaxLength') || o.maxLength;
            o.removeAttribute('mask');
                removeEventHandler(o,"keyup", mask_keyup, false);
            break;
        default:
            console.log('Given preset (' + o.getAttribute('preset') + ') is not supported by removeMask');
    }
}

function date_keyup(event) {
  var e = retrieveEventTarget(event);
  if (event.keyCode == 84) { // enter 'T'
    if (e.readOnly != null && !e.readOnly && (e.value.length == 1))  // 12/29/06 mf - Should not change value of read-only box
    {
      e.value = dtToString(new Date());
      // 2/11/2004 dd - set value of textbox through script does not trigger onchange event. Therefore explicit action requires.
      if(typeof(doAfterDateFormat) == 'function')
	  {
		doAfterDateFormat(e);
  	  }


      if (typeof(updateDirtyBit) == 'function') updateDirtyBit(event);
    }
  }
}

function dtToString(dt) {
  return (dt.getMonth() + 1) + '/' + dt.getDate() + '/' + dt.getFullYear();
}

function getCursorPosition(element) {
    var pos = 0;
    if (document.selection) {
        s = document.selection.createRange();
        t = element.createTextRange();

        if (t.inRange(s) == true) {
            // We're in range, so we have focus.

            while (t.compareEndPoints("StartToStart", s) < 0) {
                s.move("character", -1);

                ++pos;
            }
        }

        return pos;
    }
    else {
        return element.selectionStart;
    }
}

function setCursorPosition(element, startPosition) {
    if (element.createTextRange) {
        var t = element.createTextRange();
        t.collapse();

        while (startPosition > 0) {
            t.move("character", 1);

            --startPosition;
        }

        t.select();
    }
    else {
        element.setSelectionRange(startPosition, startPosition)
    }
}

function mask_keyup(event)
{
    // Validate the event throwing object.  We choose th global
    // arguments set if the event source element is null.
    var e = retrieveEventTarget(event);

    if( e == null )
    {
        e = arguments[ 0 ];
    }

    if( event.keyCode == 9 || event.keyCode == 16 || event.keyCode == 8 || event.keyCode == 37 || event.keyCode == 39 )
    {
        // Skip tab, delete, and cursor keys.

        return;
    }

    // Get the current cursor position.  This is definitely
    // jscript voodoo, but it seems to work when the text
    // box has focus.

    var t, s, pos = 0;

    pos = getCursorPosition(e);

    if (e.value == '')
    {
        return;
    }

    // Walk the string and apply the mask.

    var maskIndex = 0;
    var index     = 0;
    var ret       = "";
    var value     = e.value;
    var mask      = e.getAttribute("mask");
    var maskSuffix = e.getAttribute("mask_suffix");

    while( true )
    {
        m = mask.charAt( maskIndex );
        c = value.charAt( index );

        if( m == '#' )
        {
            if( isDigit( c ) )
            {
                ret += c;

                if( ++maskIndex > mask.length )
                    break;
            }

            if( ++index > value.length )
                break;
        }
        else
        if (m == '*') {
            if (isDigit(c) || c == '*') {
                ret += c;

                if (++maskIndex > mask.length)
                    break;
            }

            if (++index > value.length)
                break;
        }
        else
        if( m == '?' )
        {
            if( isAlphaNumeric( c ) )
            {
                ret += c;

                if( ++maskIndex > mask.length )
                    break;
            }

            if( ++index > value.length )
                break;
        }
        else
        if (m == '^')
        {
            if( isWord(c) )
            {
                ret += c;

                if( ++maskIndex > mask.length )
                    break;
            }
            if( ++index > value.length )
                break;
        }
        else
        {
            ret += m;

            if( maskIndex <= pos && c != m )
            {
                ++pos;
            }

            if( c == m )
            {
                ++index;
            }

            if( ++maskIndex > mask.length )
                break;
        }
    }

    e.caretPos = pos;
    // This is a workaround for IE, but it will throw in chrome.
    // For more info see http://bugs.jquery.com/ticket/10818
    try {
        e.innerText = ret;
    } catch (exc) { }

    if (e.value != ret) {
        e.value = ret;
    }

    if (e.noTrail && e.value.charAt(e.value.length-1) == '-'){
        e.value = e.value.substr(0,e.value.length-1);
        pos-=1;
    }
    if( typeof( updateDirtyBit ) == 'function' )
    {
        updateDirtyBit(event);
    }

    setCursorPosition(e, pos);

}

function isDigit(ch) {
  return ch.match(/[0-9]/) != null;
}

function isAlphaNumeric(ch) {
  return ch.match(/[a-z0-9]/i) != null;
}

function isWord(ch) {
  return ch.match(/[-a-z0-9 .?!_]/i) != null;
}

function numeric_onblur(event) {
    var e = retrieveEventTarget(event) == null ? arguments[0] : retrieveEventTarget(event);
    format_numeric(e);
}

// Format a generic number that is allowed to have decimals.
function format_numeric(e) {
    if (e.value == '') return;
    var index = 0;
    var ret = '';
    var isNegative = false;
    var isLeadingZero = true;
    var hasDecimal = false;
    var str = e.value.replace(/[^0-9-\(.]/g, '');
    var decimalDigits = 0;

    for (index = 0; index < str.length; index++) {
        ch = str.charAt(index);
        if (ch == '-' && index == 0 && !isNegative) {
            isNegative = true;
            continue;
        }
        if (isLeadingZero && ch == '0') continue;
        if (isDigit(ch)) {
            ret += ch;
            isLeadingZero = false;
            if (hasDecimal) decimalDigits++;
        } else if (ch == '.' && !hasDecimal) {
            if (isLeadingZero) ret = '0.';
            else ret += '.';
            hasDecimal = true;
            isLeadingZero = false;
        }
    }

    if (isLeadingZero || ret === '0.') {
        e.value = '0';
        return;
    }

    // Place thousand separator.
    var p = ret.length - (hasDecimal ? (decimalDigits + 1) : 0);
    while (p > 3) {
        ret = ret.substr(0, p - 3) + ',' + ret.substr(p - 3);
        p -= 3;
    }

    if (isNegative) {
        e.value = '-' + ret;
    } else {
        e.value = ret;
    }
}

function oninput_CuRiskScore(event) {
    var e = retrieveEventTarget(event) == null ? arguments[0] : retrieveEventTarget(event);
    format_numeric(e);
    if (e.value === "0") {
        e.value = "";
    }
}

function money_onblur(evt, toUse) {
    var e = retrieveEventTarget(evt) == null ? arguments[0] : retrieveEventTarget(evt);
    if (toUse) e = toUse;
    format_money(e);

    if (typeof (format_money_callback) === 'function') {
        format_money_callback(e);
    }
}

function format_money(e) {
  // If initial inputbox is empty then DO NOT format this input. dd 9/8/03
  // 10/20/2009 dd - Add support for 4 decimal digits
  if (e.value == '') return;
  var index = 0;
  var ret = '';
  var isNegative = false;
  var isLeadingZero = true;
  var hasDecimal = false;
  var precision = 0;
  var str = e.value.replace(/[^0-9-\(.]/g, '');
  var decimalDigits = e.getAttribute("decimalDigits") == null ? 2 : e.getAttribute("decimalDigits");
  var precisionLimit = decimalDigits >= 2 ? decimalDigits - 1 : 1;

  for (index = 0; index < str.length; index++) {
    ch = str.charAt(index);
    if ((ch == '-' || ch == '(') && index == 0 && !isNegative) {
      isNegative = true;
      continue;
    }
    if (isLeadingZero && ch == '0') continue;
    if (isDigit(ch)) {
      ret += ch;
      isLeadingZero = false;
      if (hasDecimal) precision++;
      if (precision > precisionLimit) break;
    } else if (ch == '.' && decimalDigits == 0) {
        break;  // truncate
    } else if (ch == '.' && !hasDecimal) {
      if (isLeadingZero) ret = '0.';
      else ret += '.';
      hasDecimal = true;
      isLeadingZero = false;
    }
  }

  if (!hasDecimal) {
    switch (decimalDigits)
    {
      case 0:
        if (isLeadingZero)
          ret += '0';
        break;
      case 4:
        if (isLeadingZero)
          ret = '0.0000';
        else
          ret += '.0000';
      default:
        if (isLeadingZero)
          ret = '0.00';
        else
          ret += '.00';
    }
  }
  else if (decimalDigits == 0)
  {
    // shouldn't be able to get here.
    // DO NOTHING
  }
  else {
      var leftOverDecimalPlaces = decimalDigits - precision;
      for (leftOverDecimalPlaces; leftOverDecimalPlaces > 0; --leftOverDecimalPlaces) {
          ret += '0';
      }
  }

    // Place thousand separator.
    var decimalCharacters = decimalDigits <= 0 ? 0 : parseInt(decimalDigits) + 1;

  var p = ret.length - decimalCharacters;
  while (p > 3) {
    ret = ret.substr(0, p - 3) + ',' + ret.substr(p - 3);
    p -= 3;
  }

  // 9/5/03 - For performance reason, I don't reformat when value pass back from server.
  // Therefore color code on money field will not work consistencely.
  if (isNegative) {
    e.value = '($' + ret + ')';
  //  e.style.color = gCurrencyNegativeColor;
  } else {
    e.value = '$' + ret;
  //  e.style.color = gCurrencyColor;
  }
}
function percent_onblur(event, toUse) {
  var e = retrieveEventTarget(event) == null ? arguments[0] : retrieveEventTarget(event);
  if (toUse) e = toUse;
  format_percent(e);
  if(e){
    if(typeof(format_percent_callback) === 'function'){
        format_percent_callback.call(this, e);
    }
  }
}

function price_onblur(evt, toUse) {
    var e = evt ? retrieveEventTarget(evt) : toUse;
    format_percent(e);
    //Remove the percent sign
    e.value = e.value.substring(0, e.value.length - 1);
}

function format_percent(e) {
  // If initial inputbox is empty then DO NOT format this input. dd 9/8/03
  if (e.value == '') return;
  // Remove leading 0.
  // Append 0 if first character is .
  var index = 0;
  var ret = '';
  var isLeadingZero = true;
  var hasDecimal = false;
  var precision = 0;
  var isNegative = false;
  var noDecimal = e.getAttribute("percent_no_decimal");

  var decimalDigits = e.getAttribute("decimalDigits") == null ? 3 : e.getAttribute("decimalDigits");
  var precisionLimit;
  if (decimalDigits == 6) {
    precisionLimit = 5;
  } else if (decimalDigits == 4) {
    precisionLimit = 3;
  } else {
    precisionLimit = 2;
  }


  for (index = 0; index < e.value.length; index++) {
    ch = e.value.charAt(index);
    if ((ch == '-' || ch == '(') && index == 0 && !isNegative) {
      isNegative = true;
      continue;
    }
    if (isLeadingZero && ch == '0') continue;
    if (isDigit(ch)) {
      ret += ch;
      isLeadingZero = false;
      if (hasDecimal) precision++;
      if (precision > precisionLimit) break;
    } else if (ch == '.' && !hasDecimal) {
      if (isLeadingZero) ret = '0.';
      else ret += '.';
      hasDecimal = true;
      isLeadingZero = false;

    }
  }
  if (noDecimal) {
      precision = 5;decimalDigits = 7;hasDecimal = true;
  }

  if (!hasDecimal)
  {
      if (decimalDigits == 6) {
          if (isLeadingZero) ret = '0.000000';
          else ret += '.000000';
      } else if (decimalDigits == 4) {
          if (isLeadingZero) ret = '0.0000';
          else ret += '.0000';
      } else {
          if (isLeadingZero) ret = '0.000';
          else ret += '.000';
      }
  } else if (precision == 0) // pad with zeroes
  {
      ret += decimalDigits == 6 ? '000000' : '000';
  } else if (precision == 1)
  {
      ret += decimalDigits == 6 ? '00000' : '00';
  } else if (precision == 2)
  {
      ret += decimalDigits == 6 ? '0000' : '0';
  } else if (precision == 3)
  {
      ret += decimalDigits == 6 ? '000' : '';
  }
  else if (precision == 4)
  {
      ret += decimalDigits == 6 ? '00' : '';
  }
  else if (precision == 5)
  {
      ret += decimalDigits == 6 ? '0' : '';
  }
  if (isNegative)
    e.value = '-' + ret + '%';
  else
    e.value = ret + '%';

}

// Enter 't' will fill in today date
// Max length = 15.
// A single digit will be interpreted as a month, and we will assume 1 as the day
// If a year is not specified (mm or mm/dd), we define a date window that extends
//   from 3 months before today to 9 months after. We will locate the month and
//   day within that window and use the correspoding year.
// Enter mm/dd/yy if year is between 30 and 99 then yyyy = 19yy else 20yy
// Support format mmddyyyy, mm-dd-yyyy, mm/dd/yyyy, mm.dd.yyyy

function date_onblur(event, o) {
    var e;
    if (o){
        e = o;
    }
    else {
        e = retrieveEventTarget(event) == null ? arguments[0] : retrieveEventTarget(event);
    }

    format_date(e);

    if (isDirty()) {
        if (typeof (doAfterDateFormat) == 'function') {
            doAfterDateFormat(e);
        }
    }
}

function format_date(e) {
    if (typeof e.value == 'undefined') return;
    if (e.value == '') return;
    if (e.getAttribute('allowna') === 'true' && e.value.toLowerCase() === 'na') return;

    var minYear = 1901;
    var maxYear = 2078;
    if (e.getAttribute("dttype") == 'DateTime') {
        minYear = 1754;
        maxYear = 9999;
    }
    var value = e.value;

    // Clear out any whitespace
    value = value.replace(/ /g, "");

    var windowStartDate = new Date();
    windowStartDate.setMonth(windowStartDate.getMonth() - 3);
    var dt = new Date();
    var mm;
    var dd;
    var yyyy;
    var offsetPeriod;
    var offsetType;
    var missingYear = false;

    // Try mm?/dd?/yy(yy)?(offset)?
    var dateGroups = value.match(/^(\d{1,2})[-/.](\d{1,2})[-/.](\d{4}|\d{2})(?:([-+]\d+)([dbwmy]?))?$/);
    if (dateGroups === null) {
        // Try mmddyy(yy)?(offset)?
        dateGroups = value.match(/^(\d{2})(\d{2})(\d{4}|\d{2})(?:([-+]\d+)([dbwmy]?))?$/);
    }
    if (dateGroups === null) {
        // Try dd?(offset)? or mm?/dd?(offset)?
        // Note that ( *?) is a hack to produce an always empty group 3.
        dateGroups = value.match(/^(\d{1,2})(?:[-/.](\d{1,2}))?( *?)(?:([-+]\d+)([dbwmy]?))?$/);
        if (dateGroups != null) {
            missingYear = true;
            // If no day, assume 1.
            if (dateGroups[2] === "" || typeof (dateGroups[2]) === 'undefined') {
                dateGroups[2] = 1;
            }
            // Set the year to be the same as the start of the date window
            dateGroups[3] = windowStartDate.getFullYear();
        }
    }
    if (dateGroups === null) {
        // Give up.
        errorMsg('Invalid date format.\n\nYou can enter dates in the following formats: mm/dd/yy, mm/dd/yyyy, mm/dd, mmddyy, mmddyyyy.', e);
        return;
    }

    // Set the pieces
    mm = parseInt(dateGroups[1], 10);
    dd = parseInt(dateGroups[2], 10);
    yyyy = parseInt(dateGroups[3], 10);
    if (dateGroups[3].length <= 2) {
        if (yyyy < 30) yyyy = 2000 + yyyy;
        else yyyy = 1900 + yyyy;
    }
    offsetPeriod = dateGroups[4];
    offsetType = dateGroups[5] || "d";

    // Validate the pieces
    if (mm < 1 || mm > 12) {
        errorMsg('Invalid month. Month must be from 1 to 12.', e);
        return;
    }
    if (dd < 1 || dd > 31) {
        errorMsg('Invalid day. Day must be from 1 to 31.', e);
        return;
    }
    if (yyyy < minYear || yyyy > maxYear) {
        // SQL_SMALL_DATETIME_MAX is  6/6/2079
        dt.setFullYear(yyyy, mm - 1, dd); // Convert mmddyy to mm/dd/yyyy before alert user error.
        e.value = dtToString(dt);
        errorMsg('Invalid year. Year needs to be between ' + minYear + ' and ' + maxYear + '.', e);
        return;
    }
    if (!isValidDate(yyyy, mm, dd, e)) {
        return;
    }
    dt.setFullYear(yyyy, mm - 1, dd);
    // If the year was missing and the date is less than the date window start date,
    // add a year to bring the date into the window.
    if (missingYear && dt < windowStartDate) {
        dt.setFullYear(yyyy + 1);
    }

    e.value = dtToString(dt);

    // If the service call fails, the fallback value will just be the date without the offset.
    if (typeof (gService) !== 'undefined' && gService != null && gService.utils != null && offsetPeriod) {
        var dateOffsetArgs = {
            DateString: e.value,
            OffsetPeriod: offsetPeriod,
            OffsetType: offsetType
        };
        var dateOffsetResult = gService.utils.call("ProcessDateOffset", dateOffsetArgs);
        if (!dateOffsetResult.error && dateOffsetResult.value.IsValid === '1') {
            e.value = dateOffsetResult.value.NewDateString;
        }
    }
}

function isValidDate(yyyy, mm, dd, o) {
  var months = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
  if ((mm == 4 || mm == 6 || mm == 9 || mm == 11) && dd > 30) {
    errorMsg(months[mm - 1] + ' has maximum of 30 days.', o);
    return false;
  } else if (mm == 2 && ((yyyy % 4) > 0) && dd > 28) {
    errorMsg('February of ' + yyyy + ' has maximum of 28 days.', o);
    return false;
  } else if (mm == 2 && dd > 29) {
    errorMsg('February of ' + yyyy + ' has maximum of 29 days.', o);
    return false;
  }
  return true;
}
function getYear(y) {
  if (y < 30)
    return y += 2000;
  else if (y < 100)
    return y += 1900;
  else
    return y;
}
function errorMsg(msg, o) {
    try {
        alert(msg);

        window.setTimeout(function () {
            o.style.backgroundColor = "yellow";

            // For date fields that specify an old value, revert to that old value.
            if (typeof o.oldValue !== 'undefined')
            {
                o.value = o.oldValue;
            }
        });
    } catch (e) { }
}

// --- Functions for time control.
function time_onminuteblur(o) {
  var a = parseInt(o.value, 10);
  if (a < 10) o.value = "0" + a;
  else if (a < 60) o.value = a;
  else o.value = "00";
}
function time_onhourkeyup(o, m) {
  // skip tab & delete
  if (event.keyCode == 9 || event.keyCode == 16 || event.keyCode == 8 ||
event.keyCode == 39 || event.keyCode == 37) return;

  var v = parseInt(o.value, 10);
  var prefix = o.value.length == 2 ? o.value.charAt(0) : "0";
  //alert(o.value + " | " + parseInt(o.value));

  if (isNaN(v)) o.value = "";
  else {
    if (v > 1 && v < 10) {
      o.value = prefix + v;
      document.getElementById(m).select();
      document.getElementById(m).focus();
    } else if (v > 12) {
      o.value = prefix;
    } else if (v > 9 && v < 13) {
      document.getElementById(m).select();
      document.getElementById(m).focus();
    }
  }
}
function time_onhourblur(o) {
  var a = parseInt(o.value, 10);
  if (a < 10) o.value = "0" + a;
  else if (a < 13) o.value = a;
  else o.value = "12";
}
