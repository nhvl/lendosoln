﻿var app = angular.module('ExportToSymitar', []);

app.controller('ExportToSymitarController', function ($scope) {
    $scope.LoanIdFormat = '0000';
    $scope.NoLoansFoundText = 'NO LOANS FOUND';
    $scope.ClosedArrowHexCode = ' &#x25bc;';
    $scope.OpenArrowHexCode = ' &#x25ba;';
    $scope.MaxSymitarLoanNumber = 9999;

    $scope.SymitarLoanIdRegex = /^\d{1,4}$/i;

    $scope.HasExportError = false;
    $scope.ExportCacheKey = null;
    $scope.ExistingSymitarLoans = [];
    $scope.ExportDataSections = [];

    $scope.setInputDisabled = function (ids, disabled) {
        var inputs = $(ids);

        inputs.prop('disabled', disabled);

        if (disabled === false) {
            // jQuery sets the background color to gray for the inputs but
            // does not always remove that background color when reenabling
            // the dropdown.
            inputs.css('background-color', '');
        }
    }

    $scope.validateBorrowerInformation = function () {
        var borrowerMemberId = $('#aBCoreSystemId');

        if ($.trim(borrowerMemberId.val()) === '') {
            $('#ErrorMessage').text('Invalid Member ID. Please verify the borrower\'s member ID.');

            borrowerMemberId.focus();

            return false;
        }

        return true;
    }

    $scope.validateLoanId = function () {
        var loanId = $('#sCoreLoanId');

        if (!loanId.val().match($scope.SymitarLoanIdRegex)) {
            $('#ErrorMessage').text('Invalid loan ID. Please verify the Symitar loan ID.');

            loanId.focus();

            return false;
        }

        return true;
    }

    $scope.getAllFormValues = function () {
        var selectedLoanId = $('#sCoreLoanId').val();
        var isLoanIdUnique = function (loanId, loans) {
            for (var i = 0; i < loans.length; ++i) {
                if (loans[i].Id === loanId) {
                    return false;
                }
            }

            return true;
        };

        var args = {
            loanId: ML.sLId,
            applicationId: ML.aAppId,
            sFileVersion: $('#sFileVersion').val(),
            aBCoreSystemId: $('#aBCoreSystemId').val(),
            aCCoreSystemId: $('#aCCoreSystemId').val(),
            sCoreLoanId: selectedLoanId,
            ExportCacheKey: $scope.ExportCacheKey,
            isNewSymitarLoan: isLoanIdUnique(selectedLoanId, $scope.ExistingSymitarLoans)
        };

        return args;
    }

    $scope.setErrorResultDisplay = function (result) {
        var errorMessage = result.UserMessage ||
            result.value['ErrorMessage'] ||
            'An export error has occurred. Please contact your LendingQB system administrator if this issue persists.';

        $('#ErrorMessage').text(errorMessage);
    }

    $scope.padLeft = function (loanId) {
        var loanIdString = loanId.toString();

        return ($scope.LoanIdFormat + loanIdString).slice(loanIdString.length);
    }

    $scope.calculateNextLoanId = function () {
        var maxLoanId = -1;

        $.each($('#ExistingLoanIds option'), function (index, option) {
            var loanId = parseInt(option.value);

            if (loanId > maxLoanId) {
                maxLoanId = loanId;
            }
        });

        var inputValue = $scope.padLeft(Math.min(maxLoanId + 1, $scope.MaxSymitarLoanNumber));

        $('#sCoreLoanId').val(inputValue);

        updateDirtyBit();
    }

    $scope.toggleSection = function (id, name) {
        var section = $('#Section' + id);

        if (section.hasClass('active')) {
            section.removeClass('active');
            section.next('div').removeClass('show');
            section.html(name + $scope.ClosedArrowHexCode);
        }
        else {
            section.addClass('active');
            section.next('div').addClass('show');
            section.html(name + $scope.OpenArrowHexCode);
        }
    }

    $scope.sectionHasExportError = function (section) {
        for (var i = 0; i < section.Rows.length; i++) {
            if (section.Rows[i].HasError) {
                return true;
            }
        }

        for (var i = 0; i < section.Subsections.length; i++) {
            if ($scope.sectionHasExportError(section.Subsections[i])) {
                return true;
            }
        }

        return false;
    }

    $scope.loadSymitarData = function () {
        $('#ErrorMessage').text('');

        if ($scope.validateBorrowerInformation() === false) {
            return;
        }

        gService.loanedit.callAsyncSimple('LoadDataFromSymitar', $scope.getAllFormValues(), function (result) {
            if (result.error === false && result.value['Status'] === 'OK') {
                $scope.ExistingSymitarLoans = angular.fromJson(result.value['ExistingLoanIdJson']);

                if ($scope.ExistingSymitarLoans.length === 0) {
                    $('#NoLoansFoundOption').text($scope.NoLoansFoundText).val('0');
                }
                else {
                    $('#NoLoansFoundOption').text('').val('');
                }

                $scope.setInputDisabled('#UseNextAvailableIdButton, #ExistingLoanIds', $scope.ExistingSymitarLoans.length === 0);
            }
            else {
                // Wait briefly before displaying the error
                // message so that the label in the UI resets.
                window.setTimeout(function () {
                    $scope.setErrorResultDisplay(result);

                    $scope.setInputDisabled('#UseNextAvailableIdButton, #ExistingLoanIds', true);
                }, 300);
            }
        });        
    }

    $scope.loadExportData = function () {
        $('#ErrorMessage').text('');

        if ($scope.validateBorrowerInformation() === false ||
            $scope.validateLoanId() === false) {
            return;
        }

        // If the user has changed any values such as the Symitar
        // loan ID since the page was loaded, save those values
        // before running the batch export.
        if (isDirty()) {
            saveMe(false);
        }

        gService.loanedit.callAsyncSimple('LoadExportData', $scope.getAllFormValues(), function (result) {
            if (result.error === false && result.value['Status'] === 'OK') {
                $scope.ExportCacheKey = result.value['ExportCacheKey'];

                $scope.ExportDataSections = angular.fromJson(result.value['ExportDataSectionsJson']);

                $scope.HasExportError = $scope.ExportDataSections.length === 0 || $scope.sectionHasExportError($scope.ExportDataSections[0]);

                $scope.setInputDisabled('#SubmitButton', $scope.HasExportError);

                $("#SubmitButtonDirtyErrorImg").hide();

                // Set a timeout to initialize the error tooltips so that the controller 
                // can render the error images correctly.
                window.setTimeout(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                });
            }
            else {
                // Wait briefly before displaying the error
                // message so that the label in the UI resets.
                window.setTimeout(function () {
                    $scope.setErrorResultDisplay(result);

                    $scope.setInputDisabled('#SubmitButton', true);
                }, 300);
            }
        });        
    }

    $scope.submit = function () {
        $('#ErrorMessage').text('');

        if ($scope.validateBorrowerInformation() === false ||
            $scope.validateLoanId() === false) {
            return;
        }

        // Disable the submit button to prevent multiple submissions
        // at the same time.
        $scope.setInputDisabled("#SubmitButton", true);

        gService.loanedit.callAsyncSimple('SubmitToSymitar', $scope.getAllFormValues(), function (result) {
            $scope.setInputDisabled("#SubmitButton", false);

            if (result.error === false && result.value['Status'] === 'OK') {
                alert('Export successful.');
            }
            else {
                // Wait briefly before displaying the error
                // message so that the label in the UI resets.
                window.setTimeout(function () {
                    $scope.setErrorResultDisplay(result);
                }, 300);
            }
        });        
    }

    $scope.init = function () {
        $('#ExistingLoanIds').change(function () {
            $('#sCoreLoanId').val(this.value);
        });

        $scope.setInputDisabled('#UseNextAvailableIdButton, #ExistingLoanIds, #SubmitButton', true);

        $("#SubmitButtonDirtyErrorImg").hide();
    }

    updateDirtyBit_callback = function () {
        // We want to disable the submit button if the page becomes 
        // dirty so that any changes are reflected in the export
        // data.
        $scope.setInputDisabled("#SubmitButton", true);

        $("#SubmitButtonDirtyErrorImg").show().tooltip();
    }

    $scope.init();
});
