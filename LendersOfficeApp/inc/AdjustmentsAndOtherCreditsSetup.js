﻿var adjustmentsAndOtherCreditsSetupTemplate;
var container;
var changedList = [];
var deletedList = [];

function onAdd() {
    bindData(); // bind data before you add new data again
    var newAdjustment = { AdjustmentType: -1, AdjustmentTypeString: "Blank", Description: "", PaidFromParty: 0, PaidToParty: 0, PopulateTo: 0, POC: false, DFLP: false, ID: -1 };
    model.SerializedAdjustments.push(newAdjustment);
    var templateItem = $("#adjustmentsTable").tmplItem();
    templateItem.data.value = model.SerializedAdjustments;
    templateItem.update();
    renderData();
}

function validateDuplicateAdjustments(sendAlert) {
    var hasDuplicate = false;
    var distinctAdjustments = {};
    $.each(model.SerializedAdjustments, function (index, adjustment) {
        var key = adjustment.AdjustmentTypeString + adjustment.Description;
        if (typeof (distinctAdjustments[key]) === 'undefined') {
            // keep track of unique value.
            distinctAdjustments[key] = index;
        }
        else {
            // they match. 
            var styleDescriptions = $(".description");
            $(styleDescriptions[index]).addClass("redBorder");
            $(styleDescriptions[distinctAdjustments[key]]).addClass("redBorder");
            hasDuplicate = true;
        }
    });

    if (hasDuplicate && sendAlert) {
        alert("You may not have multiple predefined adjustments with same type and description.");
    }

    return !hasDuplicate;
}

function onSave() {
    bindData();
    var addedAdjustments = [];
    var updatedAdjustments = [];       

    if (!validateDuplicateAdjustments(true)) {
        return;
    }

    for (var index = 0; index < model.SerializedAdjustments.length; index++) {
        if (model.SerializedAdjustments[index].AdjustmentTypeString === "Blank") {
            alert("You may not save while you have blank adjustment configurations.");
            return;
        }
        if (model.SerializedAdjustments[index].Description === "") {
            alert("You may not save while you have blank adjustment description.");
            var styleDescriptions = $(".description");
            $(styleDescriptions[index]).addClass("redBorder");
            return;
        }
        if (model.SerializedAdjustments[index].dirty === true && model.SerializedAdjustments[index].ID !== -1) {
            model.SerializedAdjustments[index].AdjustmentType = predefinedAdjustmentStringMap[model.SerializedAdjustments[index].AdjustmentTypeString.toLowerCase()].Item1;
            updatedAdjustments.push(model.SerializedAdjustments[index]);
        }
        else if (model.SerializedAdjustments[index].ID === -1) {
            model.SerializedAdjustments[index].AdjustmentType = predefinedAdjustmentStringMap[model.SerializedAdjustments[index].AdjustmentTypeString.toLowerCase()].Item1;
            addedAdjustments.push(model.SerializedAdjustments[index]);
        }
    }

    parent.LQBPopup.Return({
        OK: true,
        addedAdjustments: addedAdjustments,
        updatedAdjustments: updatedAdjustments,
        deletedList: deletedList
    });
}

function descriptionsChange(rowIndex) {
    bindData();
    markDirty(rowIndex);
    var styleDescriptions = $(".description");

    for (var i = 0; i < model.SerializedAdjustments.length; i++) {
        $(styleDescriptions[i]).removeClass("redBorder");
    }

    if (!validateDuplicateAdjustments(false)) {
        return;
    }

    // mark the predefined adjustment description red in parts, in order that onSave marks them.
    for (var i = 0; i < model.SerializedAdjustments.length; i++) {
        if (model.SerializedAdjustments[i].Description === "") {
            $(styleDescriptions[i]).addClass("redBorder");
            return;
        }
    }

    bindData();
}

function disableFields(i, adjustmentTypeDdls, descriptions, fromDdls, toDdls, POCs, include1003s, DFLPs) {
    toDdls[i].disabled = false;
    switch (adjustmentTypeEnums[adjustmentTypeDdls[i].value]) {
        case -1: // Blank
            descriptions[i].disabled = true;
            fromDdls[i].disabled = true;
            toDdls[i].disabled = true;
            POCs[i].disabled = true;
            include1003s[i].disabled = true;
            DFLPs[i].disabled = true;
            return;
        case 32: // PrincipalReductionToReduceCashToBorrower
        case 33: // PrincipalReductionToCureToleranceViolation
            fromDdls[i].disabled = true;
            toDdls[i].disabled = true;
            break;
        case 22: // BuydownFund
        case 23: // CommitmentOriginationFee
        case 24: // FederalAgencyFundingFeeRefund
        case 25: // GiftOfEquity
        case 26: // MIPremiumRefund
        case 27: // SweatEquity
        case 28: // TradeEquityFromPropertySwap
        case 30: // TradeEquityCredit
        case 0: // EmployerAssistedHousing
        case 1: // LeasePurchaseFund
        case 2: // RelocationFunds
            toDdls[i].disabled = true;
            fromDdls[i].disabled = false;
            break;
    }

    descriptions[i].disabled = false;
    POCs[i].disabled = false;
    DFLPs[i].disabled = (toDdls[i].value !== "3");

    // Disable "Include in 1003" checkbox when POC or "Paid To" and "Paid From"
    // are both not borrower (1)
    include1003s[i].disabled = POCs[i].checked || (toDdls[i].value !== "1" && fromDdls[i].value !== "1");
}

function adjustmentsDdlChange(rowIndex, isInit) {
    markDirty(rowIndex);
    var adjustmentTypeDdls = $(".adjustmentsDdl");
    var descriptions = $(".description");
    var fromDdls = $(".fromDdl");
    var toDdls = $(".toDdl");
    var POCs = $(".POC");
    var include1003s = $(".include1003");
    var DFLPs = $(".DFLP");

    var typeIndex = adjustmentTypeDdls[rowIndex].value;
    var adjustmentData = predefinedAdjustmentTypeMap[typeIndex];

    if (!isInit) {
        descriptions[rowIndex].value = adjustmentData.Description;
        fromDdls[rowIndex].value = adjustmentData.PaidFromParty;
        toDdls[rowIndex].value = adjustmentData.PaidToParty;
        POCs[rowIndex].checked = adjustmentData.POC;
        include1003s[rowIndex].checked = adjustmentData.IsPopulateToLineLOn1003;
        DFLPs[rowIndex].checked = false;
    }

    disableFields(rowIndex, adjustmentTypeDdls, descriptions, fromDdls, toDdls, POCs, include1003s, DFLPs);
}

function markDirty(index) {
    model.SerializedAdjustments[index].dirty = true;
}

function partyDdlChange(rowIndex) {
    markDirty(rowIndex);
    var toDdls = $(".toDdl");
    var fromDdls = $(".fromDdl");
    var include1003s = $(".include1003");
    var DFLPs = $(".DFLP");
    include1003s[rowIndex].disabled = $(".POC")[rowIndex].checked || (toDdls[rowIndex].value !== "1" && fromDdls[rowIndex].value !== "1");
    if (toDdls[rowIndex].value !== "1" && fromDdls[rowIndex].value !== "1") {
        include1003s[rowIndex].checked = 0;
    }
    if (toDdls[rowIndex].value !== "3") {
        DFLPs[rowIndex].checked = 0;
    }
    DFLPs[rowIndex].disabled = (toDdls[rowIndex].value !== "3");
}

function pocChanged(rowIndex) {
    markDirty(rowIndex);

    if ($(".POC").eq(rowIndex).prop("checked")) {
        $('.include1003').eq(rowIndex).prop("checked", false).prop("disabled", true);
        $('.DFLP').eq(rowIndex).prop("checked", false);
    }
    else {
        // We need to bind data in case the change was made
        // on a new adjustment row.
        bindData();
        renderData();
    }
}

function dflpChanged(rowIndex) {
    markDirty(rowIndex);

    if ($(".DFLP").eq(rowIndex).prop("checked")) {
        $('.include1003').eq(rowIndex).prop("disabled", false);
        $('.POC').eq(rowIndex).prop("checked", false);
    }
    else {
        // We need to bind data in case the change was made
        // on a new adjustment row.
        bindData();
        renderData();
    }
}

function renderData() {
    var adjustmentTypeDdls = $(".adjustmentsDdl");
    var descriptions = $(".description");
    var fromDdls = $(".fromDdl");
    var toDdls = $(".toDdl");
    var POCs = $(".POC");
    var include1003s = $(".include1003");
    var DFLPs = $(".DFLP");

    for (var i = 0; i < model.SerializedAdjustments.length; i++) {
        adjustmentTypeDdls[i].value = model.SerializedAdjustments[i].AdjustmentTypeString;      
        descriptions[i].value = model.SerializedAdjustments[i].Description;
        fromDdls[i].value = model.SerializedAdjustments[i].PaidFromParty;
        toDdls[i].value = model.SerializedAdjustments[i].PaidToParty;
        POCs[i].checked = model.SerializedAdjustments[i].POC;
        include1003s[i].checked = model.SerializedAdjustments[i].PopulateTo == 1;
        DFLPs[i].checked = model.SerializedAdjustments[i].DFLP;

        disableFields(i, adjustmentTypeDdls, descriptions, fromDdls, toDdls, POCs, include1003s, DFLPs)
    }    
}

function bindData() {
    var adjustmentTypeDdls = $(".adjustmentsDdl");
    var descriptions = $(".description");
    var fromDdls = $(".fromDdl");
    var toDdls = $(".toDdl");
    var POCs = $(".POC");
    var include1003s = $(".include1003");
    var DFLPs = $(".DFLP");
    for (var i = 0; i < model.SerializedAdjustments.length; i++) {
        model.SerializedAdjustments[i].AdjustmentTypeString = adjustmentTypeDdls[i].value;
        model.SerializedAdjustments[i].Description = descriptions[i].value;
        model.SerializedAdjustments[i].PaidFromParty = fromDdls[i].value;
        model.SerializedAdjustments[i].PaidToParty = toDdls[i].value;
        model.SerializedAdjustments[i].POC = POCs[i].checked;
        model.SerializedAdjustments[i].IsPopulateToLineLOn1003 = include1003s[i].checked;
        model.SerializedAdjustments[i].PopulateTo = include1003s[i].checked ? 1 : 0;
        model.SerializedAdjustments[i].DFLP = DFLPs[i].checked;
    }
}

function deleteRow(i) {
    bindData();
    if (window.confirm("Do you want to delete this row?")) {
        if (model.SerializedAdjustments[i].ID !== -1) {
            deletedList.push(model.SerializedAdjustments[i].ID);
        }
        model.SerializedAdjustments.splice(i, 1);

        var templateItem = $("#adjustmentsTable").tmplItem();
        templateItem.data.value = model.SerializedAdjustments;
        templateItem.update();

        renderData();
    }    
}

function onPopupCancel() {
    parent.LQBPopup.Return({ OK: false });
}

function openAdjustmentsPopup() {
    var windowDimensions = f_getWindowDimensions(window);
    var windowWidth = windowDimensions.width > 1250 ? 1150 : windowDimensions.width - 100;
    var windowHeight = windowDimensions.height > 900 ? 800 : windowDimensions.height - 100;

    LQBPopup.Show("AdjustmentsAndOtherCreditsSetup/AdjustmentsAndOtherCreditsSetup.aspx", {
        height: windowHeight,
        width: windowWidth,
        onReturn: function (dialogArgs) {
            if (dialogArgs.OK) {
                var data = {
                    addedAdjustments: JSON.stringify(dialogArgs.addedAdjustments),
                    updatedAdjustments: JSON.stringify(dialogArgs.updatedAdjustments),
                    deletedList: JSON.stringify(dialogArgs.deletedList)
                };
                gService.adjustments.callAsyncSimple("Save", data, function (result) { adjustmentsAndOtherCreditsSetupSaveCallback(result, dialogArgs); });
            }
        },
        hideCloseButton: true
    }, window);
}

function adjustmentsAndOtherCreditsSetupSaveCallback(result, dialogArgs) {
    if (!result.error) {
        if (dialogArgs.addedAdjustments.length === 0 && dialogArgs.updatedAdjustments.length === 0 && dialogArgs.deletedList.length === 0) {
            alert("No changes made");
        }
        else {
            alert("Changes updated successfully");
        }
    }
    else {
        alert("Error while saving");
    }
}