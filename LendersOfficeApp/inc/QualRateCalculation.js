﻿var lpeUploadValue = '0';

function updateQualRateCalculationModel()
{
    if (typeof shouldQualRateModelUpdateReturnEarly === 'function' && shouldQualRateModelUpdateReturnEarly()) {
        return;
    }

    var useFlatValue = QualRateCalculationFlatValue.checked;

    $('#SecondCalculationRow').toggle(!useFlatValue);

    var useQualRate = ShouldUseQualRate == null || ShouldUseQualRate.checked;
    setQualRateCalculationOptionsDisabled(!useQualRate);
    if (!useQualRate) {
        return;
    }

    updateQualRateCalculationField(QualRateCalculationFieldT1, hasLegacyOption() && useFlatValue);

    var firstCalculationAdjustment = $(QualRateCalculationAdjustment1);
    if (useFlatValue && $(QualRateCalculationFieldT1).val() === lpeUploadValue) {
        firstCalculationAdjustment.prop('readonly', true).val('');
    }
    else {
        firstCalculationAdjustment.prop('readonly', false);
    }

    if (useFlatValue) {
        return;
    }

    updateQualRateCalculationField(QualRateCalculationFieldT2, false);

    if (QualRateCalculationFieldT1.selectedIndex === QualRateCalculationFieldT2.selectedIndex) {
        QualRateCalculationFieldT2.selectedIndex = (QualRateCalculationFieldT2.selectedIndex + 1) % QualRateCalculationFieldT2.options.length;
    }

    setQualRateFieldsMutuallyExclusive(QualRateCalculationFieldT1);
    setQualRateFieldsMutuallyExclusive(QualRateCalculationFieldT2);
}

function setQualRateCalculationOptionsDisabled(disabled)
{
    $(QualRateCalculationFlatValue).prop('disabled', disabled);
    $(QualRateCalculationMaxOf).prop('disabled', disabled);
    disableDDL(QualRateCalculationFieldT1, disabled);
    disableDDL(QualRateCalculationFieldT2, disabled);
    $(QualRateCalculationAdjustment1).prop('readonly', disabled);
    $(QualRateCalculationAdjustment2).prop('readonly', disabled);
}

function getCalculationFieldOptions(includeLegacyOption) {
    var options = [];

    if (includeLegacyOption) {
        options.push(new Option(LegacyValueName, lpeUploadValue));
    }

    options.push(new Option('Note Rate', '1'));
    options.push(new Option('Fully Indexed Rate', '2'));
    options.push(new Option('Index', '3'));
    options.push(new Option('Margin', '4'));    

    return options;
}

function updateQualRateCalculationField(dropdown, hasLegacyOption)
{
    var $dropdown = $(dropdown);
    var dropdownValue = $dropdown.val();
    var wasLegacyOption = dropdownValue === lpeUploadValue;

    $dropdown.empty();
    var calculationOptions = getCalculationFieldOptions(hasLegacyOption);
    for (var i = 0; i < calculationOptions.length; i++) {
        dropdown.options.add(calculationOptions[i]);
    }

    if (dropdownValue === null || (wasLegacyOption && !hasLegacyOption && dropdownValue === lpeUploadValue))
    { 
        $dropdown.val('1'/*Note Rate*/);
    }
    else
    {
        $dropdown.val(dropdownValue);
    }
}

function setQualRateFieldsMutuallyExclusive(dropdown) {
    var selectedField = $(dropdown);

    $('select.QualRateCalculationField').not(selectedField)
        .find('option[value="' + selectedField.val() + '"]')
        .prop('disabled', true)
        .end()
        .find('option:not(option[value="' + selectedField.val() + '"])')
        .prop('disabled', false);
}