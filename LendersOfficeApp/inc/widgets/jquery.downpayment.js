﻿;(function($, window, document, undefined) {
    $.widget("lqb.downpayments", {
        options: {
            data: [],
            downpaymentSources: [],
            readonly: false
        },

        _create: function() {
            var e = this.element,
                r = $('<tr class="header"/>'),
                d = this.options.data;

            $(e).on('change', 'input', function() {

            });

            this.f = $('<tfoot><tr><td colspan="4"><input type="button" class="FieldLabel" value="Add Down Payment"/></td></tr></tfoot>').appendTo(e);

            this.t = $('<tbody>').appendTo(e);
            this.t.append(r);

            r.append('<td class="FieldLabel">Amount</td>');
            r.append('<td class="FieldLabel">(Source of Down Payment)</td>');
            r.append('<td class="FieldLabel">(Explain on this line)</td>');
            r.append('<td class="FieldLabel">&nbsp;</td>');


            var that = this;
            $('input', this.f).click(function(event) {
                if (!that.options.readonly) {
                    that.addRow();
                    updateDirtyBit(event);
                }
            });

            createCombobox('sDownPmtSrc', this.options.downpaymentSources);
            this.comboIDSeed = 0;

            while (this.options.data.length < 3) {
                this.options.data.push({ b: '$0.00' });
            }

            this.addRow(this.options.data);
            this.setReadOnly(this.options.readonly);
        },

        setReadOnly: function(isReadOnly) {
            this.options.readonly = isReadOnly;
            $('input', this.t).prop({ 'readonly': isReadOnly, 'disabled': isReadOnly });
            $('input', this.f).prop('disabled', isReadOnly);
        },

        initialize: function(data) {
            this.t.children('tr:not(.header)').remove();
            this.addRow(data);
            this.setReadOnly(this.options.readonly);
        },

        addRow: function(data) {
            var e = this.element,

                tr = null,
                t = null,
                i = null,
                comboID = null;



            if (!data) {
                data = [{ b: '$0.00', s: '', e: ''}];
            }

            for (var y = 0; y < data.length; y++) {
                var r = data[y];
                tr = $('<tr>').appendTo(this.t);
                t = $('<td>').appendTo(tr);
                i = $('<input>', {
                    'type': 'text',
                    'preset': 'money',
                    'value': r.b,
                    'class': 'amount',
                    'onChange' : 'updateDirtyBit(event);'
                }).appendTo(t);

                _initMask(i.get(0));

                t = $('<td>').appendTo(tr);

                comboID = 'sDownPmtSrc_' + this.comboIDSeed;

                d = $('<div>').appendTo(t);

                i = $('<input>', {
                    'type': 'text',
                    'data-cbl': 'sDownPmtSrc',
                    'id': comboID,
                    'class': 'source',
                    'value': r.s,
                    'onChange': 'updateDirtyBit(event);'
                }).appendTo(d);

                this.comboIDSeed += 1;

                i.keydown(function(e) {
                    onComboboxKeypress(e.currentTarget, e);
                }).keypress(function(e) {
                    onComboboxKeypress(e.currentTarget, e);
                });


                i = $('<img>', {
                    'align': 'absbottom',
                    'class': 'combobox_img',
                    'src': VRoot + '/images/dropdown_arrow.gif',
                    'data-cbid': comboID
                }).appendTo(d);
                i.click(function() {
                    onSelect($(this).data('cbid'));
                });

                this.downpaymentIdSeed += 1;
                t = $('<td>').appendTo(tr);
                $('<input>', {
                    'type': 'text',
                    'class': 'explain',
                    'value': r.e,
                    'onChange': 'updateDirtyBit(event);'
                }).appendTo(t);

                t = $('<td>').appendTo(tr);
                $('<input>', {
                    'type': 'button',
                    'value': ' - '
                }).appendTo(t).click(function(event) {
                    $(this).closest('tr').remove();
                    updateDirtyBit(event);
                });
            }
        },


        serialize: function() {

            var items = [];

            this.t.children('tr').each(function(i, o) {
                if ($(o).is('.header')) {
                    return;
                }
                items.push({
                    b: $('input.amount', o).val(),
                    s: $('input.source', o).val(),
                    e: $('input.explain', o).val()
                });
            });
            return items;
        },


        // Destroy an instantiated plugin and clean up
        // modifications the widget has made to the DOM
        destroy: function() {

            // this.element.removeStuff();
            // For UI 1.8, destroy must be invoked from the
            // base widget
            $.Widget.prototype.destroy.call(this);
            // For UI 1.9, define _destroy instead and don't
            // worry about
            // calling the base widget
        }
    });

})(jQuery, window, document);
