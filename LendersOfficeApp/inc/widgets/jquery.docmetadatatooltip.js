﻿(function($, window, document, undefined) {
    $.widget("lqb.docmetadatatooltip", {
        options: {
            metadataContent: null,
        },
        _create: function () {
            if (typeof this.bindings.tooltip !== 'function') {
                throw "tooltip is required";
            }
            
            function createTableData(value) {
                var td = $('<td>');
                if (typeof value === 'object') {
                    if (value["rawHtml"]) {
                        td.append(value["rawHtml"]);
                    }
                    else {
                        td.text(value["rawHtml"]);
                    }
                }
                else {
                    td.text(value);
                }

                return td;
            }
            var metadata = this.options.metadataContent || this.bindings.data('docmetadata');
            this.bindings.tooltip({
                items: '*',
                tooltipClass: "doc-metadata-tooltip",
                content: (function () {
                    var table = $('<table>');
                    $.each(metadata, function (key, value) {
                        table.append($('<tr>').append($('<th>').text(key), createTableData(value)));
                    });
                    return $('<div>').append(table).html();
                })()
            }).each(function (index, element) {
                var display = false;
                var $element = $(element);
                $element.click(function () {
                    display = !display;
                });
                $element.bind('tooltipclose', function (event) {
                    if (display) {
                        $element.tooltip('open');
                    }
                });
            });
        },
        _destroy: function () {
            this.bindings.tooltip('destroy').off('click tooltipclose');
        }
    });
})(jQuery, window, document);
