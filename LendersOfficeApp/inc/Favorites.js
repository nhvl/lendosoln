﻿var lastMouseOverFav;

function initFavorites(bIsForLead) {
    $(document).on('click', '.FavIcon', toggleFavorite);
}

function updateFavorites(command, node, parentFolder) {
    var favoriteFolder = parentFolder ? parentFolder : getUserFavoriteFolder();
    var pages = $.map(favoriteFolder.childList, function (child, i) {
        if (!child.data.isFolder) {
            return {
                SortOrder: i,
                PageId: child.data.key
            };
        }

        return null;
    });

    pages = $.grep(pages, function (page, i) { return page != null });

    var args =
    {
        folderId: favoriteFolder.data.key,
        pagesJSON: JSON.stringify(pages),
        command: command,
        pageId:node.data.key 
    };

    gService.folderNavigation.callAsyncSimple('UpdateFavorites', args, function (result) {
        if (result.value.OK) {
            isFavoritesModified = false;
        }
    }, function () {
        alert("Failed to save favorite.  Please try again.");
    })
}

function updateFavoriteIcons(pageId, isActive) {
    var $pageLinks = $('a[id="' + pageId + '"]');
    for (var i = 0; i < $pageLinks.length; i++) {
        var $link = $($pageLinks[i]);
        $link.parent().find('.FavIcon').toggleClass('ActiveFav', isActive);
        var node = $.ui.dynatree.getNode($link);
        node.data.customAttributes['isUserFavorite'] = isActive ? 'true' : 'false';
    }
}

function getUserFavoriteFolder() {
    var $favoriteFolderLink = $('a[isuserfavoritefolder="true"]');
    return navigationTree.getNodeByKey($favoriteFolderLink.attr('path'));
}

function addFavoriteFolderLink(dynaNode) {
    var folderDynaNode = getUserFavoriteFolder();
    if (folderDynaNode == null) {
        var rootNode = navigationTree.getRoot();
        folderDynaNode = rootNode.addChild({
            title: "Favorites",
            key: -1,
            isFolder: true,
            isModified: true,
            expand: true,
            customAttributes: {isUserFavoriteFolder: 'true', path: -1},
            Folder: {
                FolderId: -1,
                InternalName: 'Favorites',
                ExternalName: 'Favorites',
                LeadLoanStatus: 0,
                RoleIds: [],
                LoanTypes: [],
                LoanPurposes: [],
                GroupIds: [],
            }
        }, rootNode.childList[0]);
    }

    dynaNode.data.customAttributes['isUserFavorite'] = 'true';

    var firstFolderChild = null;
    if (folderDynaNode.childList != null) {
        firstFolderChild = $.grep(folderDynaNode.childList, function (node) { return node.data.isFolder })[0];
    }

    folderDynaNode.addChild(dynaNode.data, firstFolderChild);
}

function removeFavoriteFolderLink(node, pageId) {
    var folderDynaNode = getUserFavoriteFolder();

    for (var i = 0; i < folderDynaNode.childList.length; i++) {
        var childNode = folderDynaNode.childList[i];
        if (childNode.data.key == pageId) {
            childNode.remove();
            break;
        }
    }
}

function toggleFavorite() {
    var $el = $(this);
    var shouldActivate = !$el.hasClass("ActiveFav");
    var pageId = $el.parent().parent().find('a.TreeNodeLink').attr('id');
    var currentNode = navigationTree.getNodeByKey(pageId);

    if (shouldActivate) {
        addFavoriteFolderLink(currentNode);
    }
    else {
        removeFavoriteFolderLink(currentNode, pageId);
    }

    updateFavoriteIcons(pageId, shouldActivate);
    updateFavorites(shouldActivate ? 'add' : 'remove', currentNode);
}


function clickLandingPage() {
    var $landingPage = $('.TreeNodeLink[landingpage]');
    if ($landingPage.length > 0 && $landingPage.attr('onclick').indexOf('.aspx') != -1) {
        $landingPage.click();
    }
    else {
        $('.TreeNodeLink:not(a[excludelandingpage])').filter(function () { return $(this).attr('onclick').indexOf('.aspx') != -1; }).first().click();
    }
}

var nodesToCollapse = [];
function initializeNodes(node, nodeSpan) {
    if (node.data.isCollapsedAfterRender) {
        nodesToCollapse.push(node);
    }

    if (node.data.key == 'Page_ConversationLog') {
        var whiteSmlSrc = VRoot + '/images/pencil-square-o-white-300dpi-11px.png';
        $(nodeSpan).after($('<img class="loadsQuickReply" src="' + whiteSmlSrc + '" />'));
    }
}

function onTreeNodeRender(node, nodeSpan) {
    var $link = $(nodeSpan).find('a');
    $.each(node.data.customAttributes, function (attrName, attrVal) {
        if (attrName.toLowerCase() == 'onclick') {
            $link.attr(attrName, 'setTimeout(function(){' + attrVal + '})')
        }
        else {
            $link.attr(attrName, attrVal);
        }
    });

    if (!node.data.isFolder) {
        var $nodeSpan = $(nodeSpan);
        var $favIcon = $('<span class="FavIcon">&#9733;</span>');
        $nodeSpan.find('.dynatree-connector').prepend($favIcon);
        $favIcon.toggleClass('ActiveFav', node.data.customAttributes['isUserFavorite'] == 'true');
    }

    if (node.data.key == 'Batch_Editor') {
        $(nodeSpan).parent().addClass('edoc-batch-editor');
    }
}

var navigationTree;
$(function () {
    var model = $.parseJSON(ML.navigationModel);
    model.onCreate = initializeNodes;
    model.onRender = onTreeNodeRender;
    model.classNames = { nodeIcon: '' };

    model.dnd =
        {
            preventVoidMoves: true,
            onDragStart: function (node) {
                if (node.parent.data.customAttributes &&
                    (node.parent.data.customAttributes.isUserFavoriteFolder == 'true' ||
                        node.parent.data.customAttributes.isUserSubFavoriteFolder == 'true')) {

                    return true;
                }

                return false;
            },
            onDragEnter: function (node, sourceNode) {
                if (node.data.isFolder) {
                    return false;
                }

                if (node.parent != sourceNode.parent) {
                    return false;
                }

                if (node.parent.data.customAttributes &&
                    (node.parent.data.customAttributes.isUserFavoriteFolder != 'true' &&
                        node.parent.data.customAttributes.isUserSubFavoriteFolder != 'true')) {
                    return false;
                }

                return ["before", "after"];
            },
            onDrop: function (node, sourceNode, hitMode, ui, draggable) {
                sourceNode.move(node, hitMode);
                updateFavorites('drag', node, node.parent);
            }
        };

    navigationTree = $('#pageNavigation').dynatree(model).dynatree("getTree");
});