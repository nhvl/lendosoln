﻿var IRLPage = (function() {
    var loanId = undefined, m_dom = {}, m_isRateLocked, m_isRateSuspended, m_adjustmentTotalFields, m_basePriceFields, m_adjustedPriceFields = [], m_isOptionArm = false, m_isReadOnly = false, m_saveFields = null;
    var m_canWrite = false;
    var sFileVersionField;

    function init() {

        //we use this quite a bit so lets cache the function pointer
        //also allows us to use g() instead of document.getElementById();
        var g = function(id) { return document.getElementById(id); };
        sFileVersionField = g('sFileVersion');

        for (var attr in PageFields) {
            if (PageFields.hasOwnProperty(attr)) {
                try {
                    m_dom[attr] = g(PageFields[attr]);
                }
                //some elements dont exist ie head 
                catch (e) { };
            }
        }

        //cache the loan id
        loanId = m_dom.LoanId.value;

        m_isReadOnly = m_dom.IsPageReadOnly.value === 'True';

        m_isRateLocked = m_dom.sInvestorLockRateLockStatusT.value === 'Locked';

        m_isRateSuspended = m_dom.sInvestorLockRateLockStatusT.value === 'Lock Suspended';

        m_isOptionArm = m_dom.sIsOptionArm.value === 'True';

        m_canWrite = m_dom.CanWrite.value === 'True';

        //set up arrays of the pricing rows, makes the onclick handlers much easier.
        m_basePriceFields = [
                m_dom.sInvestorLockBaseNoteIR,
                m_dom.sInvestorLockBaseBrokComp1PcPrice,
                m_dom.sInvestorLockBaseBrokComp1PcFee,
                m_dom.sInvestorLockBaseRAdjMarginR
            ];

        m_adjustedPriceFields = [
                m_dom.sInvestorLockNoteIR,
                m_dom.sInvestorLockBrokComp1PcPrice,
                m_dom.sInvestorLockBrokComp1Pc,
                m_dom.sInvestorLockRAdjMarginR
            ];



        if (m_isOptionArm) {
            m_basePriceFields.push(m_dom.sInvestorLockBaseOptionArmTeaserR);
            m_adjustedPriceFields.push(m_dom.sInvestorLockOptionArmTeaserR);
        }


        AdjustmentTable.EnableDeleteEntries('SelectAllAdjustments', 'DeleteSelectedAdjustments');
        AdjustmentTable.InstantiateTable('AdjustmentTable', PricingAdjustments, m_isOptionArm, m_isReadOnly, false, false, true);

        $("#CountTable input").each(function() {
            $(this).change(_triggerPricingUpdate);
        });


        addEventListenerToAllSetContext('click', [m_dom.sInvestorLockPriceRowLockedT_Base, m_dom.sInvestorLockPriceRowLockedT_Adjusted], _triggerPriceLockingUpdate);

        addEventListenerToAllSetContext('change', m_basePriceFields, _triggerPricingUpdate);

        addEventListenerToAllSetContext('change', m_adjustedPriceFields, _triggerPricingUpdate);


        addEventListenerToAllSetContext('click', [m_dom.PopulateButton], _showBrokerTransferPage);

        addEventListenerToAllSetContext('click', [m_dom.AddAdjustment], function() {
            AdjustmentTable.AddEmptyAdjustment();
        });

        addEventListenerToAllSetContext('click', [m_dom.ShowLoanSummaryBtn], _showLoanSummary);

        addEventListenerToAllSetContext('click', [m_dom.RateLockBtn], _ratelockbtnclick);

        addEventListenerToAllSetContext('click', [m_dom.SuspendLockBtn], _suspendlockbtnclick);

        addEventListenerToAllSetContext('click', [m_dom.ExtendRateLockBtn], _extendRateLock);

        addEventListenerToAllSetContext('onchange', [m_dom.sInvestorLockRLckdDays], _previewResumeLock);

        _configureUI();

        m_saveFields = [
                m_dom.sInvestorRolodexId,
        //m_dom.sInvestorLockLpInvestorNm,
                m_dom.sInvestorLockLpTemplateNm,
                m_dom.sInvestorLockRLckdDays,
                m_dom.sInvestorLockLoanNum,
                m_dom.sInvestorLockProgramId,
                m_dom.sInvestorLockConfNum,
                m_dom.sInvestorLockRateSheetID,
                m_dom.sInvestorLockRateSheetEffectiveTime,
                m_dom.sInvestorLockLockFee,
                m_dom.sInvestorLockCommitmentT,
                m_dom.sU1LockFieldDesc,
                m_dom.sU1LockFieldD,
                m_dom.sU1LockFieldAmt,
                m_dom.sU1LockFieldPc,
                m_dom.sU2LockFieldDesc,
                m_dom.sU2LockFieldD,
                m_dom.sU2LockFieldAmt,
                m_dom.sU2LockFieldPc,
                m_dom.sTrNotes
            ];

        m_saveFields = m_saveFields.concat(m_basePriceFields).concat(m_adjustedPriceFields);
        //when its option arm the teaser fields will be part of base price and adjusted price fields
        if (!m_isOptionArm) {
            m_saveFields.push(m_dom.sInvestorLockBaseOptionArmTeaserR);
            m_saveFields.push(m_dom.sInvestorLockOptionArmTeaserR);
        }

    }

    function _configureUI() {
        m_dom.RateLockBtn.value = m_isRateLocked ? 'Break Rate Lock...' : 'Lock Rate...';
        m_dom.SuspendLockBtn.value = m_isRateLocked ? 'Suspend Rate Lock...' : 'Resume Lock with Modifications...';
        m_dom.SuspendLockBtn.style.visibility = (m_isRateLocked || m_isRateSuspended) ? 'visible' : 'hidden';
        var isReadOnly = m_isRateLocked || m_isReadOnly;
        m_dom.ExtendRateLockBtn.disabled = !(m_isRateLocked && m_canWrite && !m_isReadOnly);

        AdjustmentTable.SetReadOnly(isReadOnly);


        var fieldsToDisable = [
                m_dom.sInvestorRolodexId,
        //m_dom.sInvestorLockLpInvestorNm,
                m_dom.sInvestorLockLpTemplateNm,
                m_dom.sInvestorLockRLckdDays,
                m_dom.PopulateButton,
                m_dom.sInvestorLockLoanNum,
                m_dom.sInvestorLockProgramId,
                m_dom.sInvestorLockConfNum,
                m_dom.sInvestorLockRateSheetID,
                m_dom.sInvestorLockRateSheetEffectiveTime,
                m_dom.sInvestorLockLockFee,
                m_dom.sInvestorLockCommitmentT,
                m_dom.sInvestorLockPriceRowLockedT_Base,
                m_dom.sInvestorLockPriceRowLockedT_Adjusted,
                m_dom.AddAdjustment
            ];




        m_dom.ShowLoanSummaryBtn.disabled = false;

        fieldsToDisable = fieldsToDisable.concat(m_basePriceFields, m_adjustedPriceFields);

        _setReadOnly.apply(this, [isReadOnly].concat(fieldsToDisable));

        if (!isReadOnly) {
            if (m_dom.sInvestorLockPriceRowLockedT_Base.checked) {
                _triggerPriceLockingUpdate.call(m_dom.sInvestorLockPriceRowLockedT_Base);
            }
            else if (m_dom.sInvestorLockPriceRowLockedT_Adjusted.checked) {
                _triggerPriceLockingUpdate.call(m_dom.sInvestorLockPriceRowLockedT_Adjusted);
            }
        }
    }

    //contect = this 
    function _priceFeeUpdate(fieldType, type, otherField) {
        var data = {
            'type': fieldType,
            'value': this.value,
            'conversion': type
        };

        var results = gService.loanedit.call('PriceFeeConversion', data);

        if (results.error) {
            alert('Error converting price/fee. Message : ' + results.UserMessage);
            return;
        }

        otherField.value = results.value.result;
        this.value = results.value.value;
    }

    function _handleResponse(response) {

        if (response.error) {

            if (response.ErrorType == "VersionMismatchException") {
                f_displayVersionMismatch();
            }
            else if (response.ErrorType == 'LoanFieldWritePermissionDenied') {
                f_displayFieldWriteDenied(response.UserMessage);
            }
            else {
                alert(response.UserMessage);
            }
            return false;
        }
        else {
            sFileVersionField.value = response.value["sFileVersion"];
            return true;
        }
    }


    function _extendRateLock() {
        if (!m_isRateLocked) {
            return;
        }
        var args = {
            'loanid': loanId,
            'sInvestorLockRLckExpiredD': m_dom.sInvestorLockRLckExpiredD.value,
            'sInvestorLockDeliveryExpiredD': m_dom.sInvestorLockDeliveryExpiredD.value,
            'sInvestorLockNoteIR': m_dom.sInvestorLockNoteIR.value,
            'sInvestorLockBrokComp1PcPrice': m_dom.sInvestorLockBrokComp1PcPrice.value,
            'sInvestorLockBrokComp1Pc': m_dom.sInvestorLockBrokComp1Pc.value,
            'sInvestorLockRAdjMarginR': m_dom.sInvestorLockRAdjMarginR.value,
            'sInvestorLockOptionArmTeaserR': m_dom.sInvestorLockOptionArmTeaserR.value,
            'isOptionArm': m_isOptionArm,
            'sInvestorLockLockFee': m_dom.sInvestorLockLockFee.value,
            'sInvestorLockRLckExpiredDLckd': m_dom.sInvestorLockRLckExpiredDLckd.checked,
            'sInvestorLockDeliveryExpiredDLckd': m_dom.sInvestorLockDeliveryExpiredDLckd.checked,
            'sInvestorLockCheckBrokerExtendByDefault': m_dom.sInvestorLockCheckBrokerExtendByDefault.value === 'True'
        };
        
        showModal('/newlos/LockDesk/ExtendInvestorLock.aspx?loanid=' + loanId, args, null, null, function(wargs){
            if (wargs.OK !== 'OK') {
                return;
            }

            args = {
                'loanid': loanId,
                'AddAdjustment': wargs.AddAdjustment,
                'adjDesc': wargs.adjDesc,
                'adjPrice': wargs.adjPrice,
                'adjRate': wargs.adjRate,
                'adjFee': wargs.adjFee,
                'adjMargin': wargs.adjMargin,
                'adjTRate': wargs.adjTRate,
                'sInvestorLockLockFee': wargs.sInvestorLockLockFee,
                'newsInvestorLockRLckExpiredDLckd': wargs.newsInvestorLockRLckExpiredDLckd,
                'newsInvestorLockRLckExpiredD': wargs.newsInvestorLockRLckExpiredD,
                'newsInvestorLockDeliveryExpiredDLckd': wargs.newsInvestorLockDeliveryExpiredDLckd,
                'newsInvestorLockDeliveryExpiredD': wargs.newsInvestorLockDeliveryExpiredD,
                'Reason': wargs.Reason,
                'ExtendPeriod': wargs.ExtendPeriod,
                'sFileVersion': sFileVersionField.value
            };

            var results = gService.loanedit.call('ExtendRateLock', args);

            if (_handleResponse(results) == false) {
                return;
            }

            m_dom.sInvestorLockTotAdjNoteIR.value = results.value.sInvestorLockTotAdjNoteIR;
            m_dom.sInvestorLockTotAdjBrokComp1PcPrice.value = results.value.sInvestorLockTotAdjBrokComp1PcPrice;
            m_dom.sInvestorLockTotAdjBrokComp1PcFee.value = results.value.sInvestorLockTotAdjBrokComp1PcFee;
            m_dom.sInvestorLockTotAdjRAdjMarginR.value = results.value.sInvestorLockTotAdjRAdjMarginR;
            m_dom.sInvestorLockTotAdjOptionArmTeaserR.value = results.value.sInvestorLockTotAdjOptionArmTeaserR;
            m_dom.sInvestorLockRLckExpiredD.value = results.value.sInvestorLockRLckExpiredD;
            m_dom.sInvestorLockDeliveryExpiredD.value = results.value.sInvestorLockDeliveryExpiredD;
            m_dom.RateLockHistory.innerHTML = results.value.sInvestorLockHistoryXmlContent;
            m_dom.sInvestorLockRLckdDays.value = results.value.sInvestorLockRLckdDays;
            m_dom.sInvestorLockProjectedProfit.value = results.value.sInvestorLockProjectedProfit;
            m_dom.sInvestorLockRLckExpiredDLckd.checked = results.value.sInvestorLockRLckExpiredDLckd === 'True';
            m_dom.sInvestorLockDeliveryExpiredDLckd.checked = results.value.sInvestorLockDeliveryExpiredDLckd === 'True';

            $("#sBECurrentLockExtensionCount").val(results.value.sBECurrentLockExtensionCount);
            $("#sBETotalLockExtensionCount").val(results.value.sBETotalLockExtensionCount);
            $("#sBETotalRateReLockCount").val(results.value.sBETotalRateReLockCount);

            if (wargs.AddAdjustment === 'True') {
                AdjustmentTable.AddAdjustment({
                    'Description': wargs.adjDesc,
                    'Rate': wargs.adjRate,
                    'Price': wargs.adjPrice,
                    'Fee': wargs.adjFee,
                    'Margin': wargs.adjMargin,
                    'TeaserRate': wargs.adjTRate,
                    'IsHidden': false,
                    'IsSRPAdjustment': false,
                    'IsLenderAdjustment': false,
                    'IsPersist': true
                });
            }

            if (wargs.ExtendBroker) {
                _extendBrokerLockPeriod();
            }
        }, { hideCloseButton: true });
    }

    function _extendBrokerLockPeriod() {

        showModal('/newlos/Lockdesk/ExtendRateLock.aspx?load=1&loanid=' + loanId, null, null, null, function(wargs){ 
            if (typeof (wargs.Status) === 'undefined') {
                return;
            }
    
            if (wargs.Status !== 'OK') {
                return;
            }

            var args = {};
            args.sLId = wargs.sLId;
            args.Reason = wargs.Reason;
            args.sRLckdDays = wargs.sRLckdDays;
            args.IsAdjustPrice = wargs.IsAdjustPrice;
            args.calendarDays = wargs.calendarDays;

            args.AdjustmentRate = wargs.AdjustmentRate;
            args.AdjustmentPrice = wargs.AdjustmentPrice;
            args.AdjustmentFee = wargs.AdjustmentFee;
            args.AdjustmentMargin = wargs.AdjustmentMargin;
            args.AdjustmentTRate = wargs.AdjustmentTRate;
            args.AdjustmentDesc = wargs.AdjustmentDesc;
            args.sFileVersion = sFileVersionField.value;

            var result = gService.BrokerRateLock.call("ExtendRateLock", args);
            if (_handleResponse(result) == false) {
                return;
            }

            m_dom.sNoteIR.value = result.value.sNoteIR;
            m_dom.sBrokerLockFinalBrokComp1PcPrice.value = result.value.sBrokerLockFinalBrokComp1PcPrice
        }, { hideCloseButton: true });
    }

    function _ratelockbtnclick() {
        if (m_isRateLocked) {
            _breakRateLock();
        }
        else {
            _rateLock();
        }
    }

    function _suspendlockbtnclick() {
        var fn = m_isRateLocked ? _suspendRateLock : _resumeLock;
        fn();
    }

    function _rateLock() {
        var args = {};
        args.sInvestorLockRLckdDays = m_dom.sInvestorLockRLckdDays.value;
        showModal('/newlos/LockDesk/InvestorRateLockDialog.aspx?loanid=' + loanId, args, null, null, function(wargs){
            if (wargs.OK !== 'OK') {
                return;
            }
            var data = _getSaveData();
    
            data.sInvestorLockRLckdDays = wargs.sInvestorLockRLckdDays;
            data.sInvestorLockRLckExpiredDLckd = wargs.sInvestorLockRLckExpiredDLckd;
            data.sInvestorLockDeliveryExpiredDLckd = wargs.sInvestorLockDeliveryExpiredDLckd;
            data.sInvestorLockRLckdD = wargs.sInvestorLockRLckdD;
            data.sInvestorLockRLckExpiredD = wargs.sInvestorLockRLckExpiredD;
            data.sInvestorLockDeliveryExpiredD = wargs.sInvestorLockDeliveryExpiredD;
            data.Reason = wargs.Reason;
            data.sFileVersion = sFileVersionField.value;
    
            var results = gService.loanedit.call('LockRate', data);
            if (_handleResponse(results) == false) {
                return;
            }
    
            m_dom.sInvestorLockRLckdDays.value = results.value.sInvestorLockRLckdDays;
            m_dom.sInvestorLockRLckExpiredDLckd.checked = results.value.sInvestorLockRLckExpiredDLckd === 'True';
            m_dom.sInvestorLockDeliveryExpiredDLckd.checked = results.value.sInvestorLockDeliveryExpiredDLckd === 'True';
            m_dom.sInvestorLockRLckdD.value = results.value.sInvestorLockRLckdD;
            m_dom.sInvestorLockRLckExpiredD.value = results.value.sInvestorLockRLckExpiredD;
            m_dom.sInvestorLockDeliveryExpiredD.value = results.value.sInvestorLockDeliveryExpiredD;
            m_dom.sInvestorLockRateLockStatusT.value = results.value.sInvestorLockRateLockStatusT;
            m_dom.RateLockHistory.innerHTML = results.value.sInvestorLockHistoryXmlContent;
            m_dom.sInvestorLockProjectedProfit.value = results.value.sInvestorLockProjectedProfit;
    
            $("#sBECurrentLockExtensionCount").val(results.value.sBECurrentLockExtensionCount);
            $("#sBETotalLockExtensionCount").val(results.value.sBETotalLockExtensionCount);
            $("#sBETotalRateReLockCount").val(results.value.sBETotalRateReLockCount);
    
            m_isRateLocked = m_dom.sInvestorLockRateLockStatusT.value === 'Locked';
            m_isRateSuspended = m_dom.sInvestorLockRateLockStatusT.value === 'Lock Suspended';
            _configureUI();
        }, { hideCloseButton: true });
    }


    function _breakRateLock() {
        var modalArg = new Object();

        showModal('/newlos/Underwriting/BreakRateLock.aspx', modalArg, null, null, function(wargs){
            var reason = "";
            if (wargs.OK) {
                reason = wargs.tbReason;
            }
            else {
                return;
            }
    
            if (!confirm("Are you sure you want to break the existing rate lock?")) {
                return;
            }
    
            var data = {
                'loanid': loanId,
                'Reason': reason,
                'sFileVersion': sFileVersionField.value
            }
    
            var results = gService.loanedit.call('BreakRateLock', data);
    
            if (_handleResponse(results) == false) {
                return;
            }
    
            m_dom.sInvestorLockRLckdDays.value = results.value.sInvestorLockRLckdDays;
            m_dom.sInvestorLockRLckExpiredDLckd.checked = results.value.sInvestorLockRLckExpiredDLckd === 'True';
            m_dom.sInvestorLockDeliveryExpiredDLckd.checked = results.value.sInvestorLockDeliveryExpiredDLckd === 'True';
            m_dom.sInvestorLockRLckdD.value = results.value.sInvestorLockRLckdD;
            m_dom.sInvestorLockRLckExpiredD.value = results.value.sInvestorLockRLckExpiredD;
            m_dom.sInvestorLockDeliveryExpiredD.value = results.value.sInvestorLockDeliveryExpiredD;
            m_dom.sInvestorLockRateLockStatusT.value = results.value.sInvestorLockRateLockStatusT;
            m_dom.RateLockHistory.innerHTML = results.value.sInvestorLockHistoryXmlContent;
    
            $("#sBECurrentLockExtensionCount").val(results.value.sBECurrentLockExtensionCount);
            $("#sBETotalLockExtensionCount").val(results.value.sBETotalLockExtensionCount);
            $("#sBETotalRateReLockCount").val(results.value.sBETotalRateReLockCount);
    
            m_isRateLocked = m_dom.sInvestorLockRateLockStatusT.value === 'Locked';
            m_isRateSuspended = m_dom.sInvestorLockRateLockStatusT.value === 'Lock Suspended';
    
            _configureUI();
    
        }, {hideCloseButton: true});
    }

    function _previewResumeLock() {
        m_isRateSuspended = m_dom.sInvestorLockRateLockStatusT.value === 'Lock Suspended';
        if (m_isRateSuspended == false) {
            return;
        }

        var data = {
            loanid: loanId,
            sInvestorLockRLckdDays: $("#sInvestorLockRLckdDays").val(),
            sInvestorLockRLckExpiredDLckd: $("#sInvestorLockRLckExpiredDLckd").prop("checked"),
            sInvestorLockDeliveryExpiredDLckd: $("#sInvestorLockDeliveryExpiredDLckd").prop("checked"),
            sInvestorLockRLckdD: $("#sInvestorLockRLckdD").val(),
            sInvestorLockRLckExpiredD: $("#sInvestorLockRLckExpiredD").val(),
            sInvestorLockDeliveryExpiredD: $("#sInvestorLockDeliveryExpiredD").val(),
            sFileVersion: sFileVersionField.value
        };

        var results = gService.loanedit.call('PreviewResumeLock', data);
        if (_handleResponse(results) == false) {
            return;
        }

        m_dom.sInvestorLockRLckdDays.value = results.value.sInvestorLockRLckdDays;
        m_dom.sInvestorLockRLckExpiredDLckd.checked = results.value.sInvestorLockRLckExpiredDLckd === 'True';
        m_dom.sInvestorLockDeliveryExpiredDLckd.checked = results.value.sInvestorLockDeliveryExpiredDLckd === 'True';
        m_dom.sInvestorLockRLckdD.value = results.value.sInvestorLockRLckdD;
        m_dom.sInvestorLockRLckExpiredD.value = results.value.sInvestorLockRLckExpiredD;
        m_dom.sInvestorLockDeliveryExpiredD.value = results.value.sInvestorLockDeliveryExpiredD;
    }

    function _resumeLock() {
        var args = {};
        args.sInvestorLockRLckdDays = m_dom.sInvestorLockRLckdDays.value;
        showModal('/newlos/Underwriting/ResumeLock.aspx?backend=1&loanid=' + loanId, args, null, null, function(wargs){
            if (!wargs.OK) {
                return;
            }
            var data = _getSaveData();
            
            data.sInvestorLockRLckdDays = $("#sInvestorLockRLckdDays").val();
            data.sInvestorLockRLckExpiredDLckd = $("#sInvestorLockRLckExpiredDLckd").prop("checked");
            data.sInvestorLockDeliveryExpiredDLckd = $("#sInvestorLockDeliveryExpiredDLckd").prop("checked");
            data.sInvestorLockRLckdD = $("#sInvestorLockRLckdD").val();
            data.sInvestorLockRLckExpiredD = $("#sInvestorLockRLckExpiredD").val();
            data.sInvestorLockDeliveryExpiredD = $("#sInvestorLockDeliveryExpiredD").val();
    
            data.Reason = wargs.tbReason;
            data.sFileVersion = sFileVersionField.value;
    
            var results = gService.loanedit.call('ResumeLock', data);
            if (_handleResponse(results) == false) {
                return;
            }
    
            m_dom.sInvestorLockRLckdDays.value = results.value.sInvestorLockRLckdDays;
            m_dom.sInvestorLockRLckExpiredDLckd.checked = results.value.sInvestorLockRLckExpiredDLckd === 'True';
            m_dom.sInvestorLockDeliveryExpiredDLckd.checked = results.value.sInvestorLockDeliveryExpiredDLckd === 'True';
            m_dom.sInvestorLockRLckdD.value = results.value.sInvestorLockRLckdD;
            m_dom.sInvestorLockRLckExpiredD.value = results.value.sInvestorLockRLckExpiredD;
            m_dom.sInvestorLockDeliveryExpiredD.value = results.value.sInvestorLockDeliveryExpiredD;
            m_dom.sInvestorLockRateLockStatusT.value = results.value.sInvestorLockRateLockStatusT;
            m_dom.RateLockHistory.innerHTML = results.value.sInvestorLockHistoryXmlContent;
            m_dom.sInvestorLockProjectedProfit.value = results.value.sInvestorLockProjectedProfit;
    
            $("#sBECurrentLockExtensionCount").val(results.value.sBECurrentLockExtensionCount);
            $("#sBETotalLockExtensionCount").val(results.value.sBETotalLockExtensionCount);
            $("#sBETotalRateReLockCount").val(results.value.sBETotalRateReLockCount);
    
            m_isRateLocked = m_dom.sInvestorLockRateLockStatusT.value === 'Locked';
            m_isRateSuspended = m_dom.sInvestorLockRateLockStatusT.value === 'Lock Suspended';
            _configureUI();
        }, { hideCloseButton: true });
    }


    function _suspendRateLock() {
        var modalArg = new Object();

        showModal('/newlos/Underwriting/SuspendLock.aspx', modalArg, null, null, function(wargs){
            var reason = "";
            if (wargs.OK) {
                reason = wargs.tbReason;
            }
            else {
                return;
            }
    
            if (!confirm("Are you sure you want to suspend the existing rate lock?")) {
                return;
            }
    
            var data = {
                'loanid': loanId,
                'Reason': reason,
                'sFileVersion': sFileVersionField.value
            }
    
            var results = gService.loanedit.call('SuspendLock', data);
    
            if (_handleResponse(results) == false) {
                return;
            }
    
            m_dom.sInvestorLockRLckdDays.value = results.value.sInvestorLockRLckdDays;
            m_dom.sInvestorLockRLckExpiredDLckd.checked = results.value.sInvestorLockRLckExpiredDLckd === 'True';
            m_dom.sInvestorLockDeliveryExpiredDLckd.checked = results.value.sInvestorLockDeliveryExpiredDLckd === 'True';
            m_dom.sInvestorLockRLckdD.value = results.value.sInvestorLockRLckdD;
            m_dom.sInvestorLockRLckExpiredD.value = results.value.sInvestorLockRLckExpiredD;
            m_dom.sInvestorLockDeliveryExpiredD.value = results.value.sInvestorLockDeliveryExpiredD;
            m_dom.sInvestorLockRateLockStatusT.value = results.value.sInvestorLockRateLockStatusT;
            m_dom.RateLockHistory.innerHTML = results.value.sInvestorLockHistoryXmlContent;
    
            $("#sBECurrentLockExtensionCount").val(results.value.sBECurrentLockExtensionCount);
            $("#sBETotalLockExtensionCount").val(results.value.sBETotalLockExtensionCount);
            $("#sBETotalRateReLockCount").val(results.value.sBETotalRateReLockCount);
    
            m_isRateLocked = m_dom.sInvestorLockRateLockStatusT.value === 'Locked';
            m_isRateSuspended = m_dom.sInvestorLockRateLockStatusT.value === 'Lock Suspended';
    
            _configureUI();
        }, { hideCloseButton: true });
    }



    function _showLoanSummary() {
        showModal('/newlos/LockDesk/CertFrame.aspx?loanid=' + loanId, null, null, null, null, { hideCloseButton: true });
    }

    function _showBrokerTransferPage() {
        showModal('/newlos/LockDesk/CopyFromBrokerInfo.aspx?loanid=' + loanId, null, null, null, function(wargs){ 
            if (!wargs.OK) {
                return;
            }
            //if (wargs.UseInvestorNm) {
            //    m_dom.sInvestorLockLpInvestorNm.value = wargs.InvestorNm;
            //}
    
            if (wargs.UsesLpTemplateNm) {
                m_dom.sInvestorLockLpTemplateNm.value = wargs.sLpTemplateNm;
            }
    
            if (wargs.UseInvestorLockPeriod) {
                m_dom.sInvestorLockRLckdDays.value = wargs.InvestorLockPeriod;
            }
    
            if (wargs.UsesBrokerLockRateSheetEffectiveD) {
                m_dom.sInvestorLockRateSheetEffectiveTime.value = wargs.sBrokerLockRateSheetEffectiveD;
            }
    
            if (wargs.UseBasePricingCB) {
                m_dom.sInvestorLockPriceRowLockedT_Base.checked = true;
                m_dom.sInvestorLockBaseNoteIR.value = wargs.rate;
                m_dom.sInvestorLockBaseBrokComp1PcPrice.value = wargs.price;
                m_dom.sInvestorLockBaseBrokComp1PcFee.value = wargs.fee;
                m_dom.sInvestorLockBaseRAdjMarginR.value = wargs.margin;
                m_dom.sInvestorLockBaseOptionArmTeaserR.value = wargs.trate;
            }
            for (var i = 0; i < wargs.adjustments.length; i++) {
                AdjustmentTable.AddAdjustment(wargs.adjustments[i]);
            }
            _configureUI();
            _triggerPricingUpdate();
            updateDirtyBit();
        }, { hideCloseButton: true });
    }



    //context = clicked input
    function _updateCalLockStatus(obj) {
        //be sure to set the context to input, it will go to IRLPage if not.k
        _updateLockStatus.call(this, obj);
        _disableCalendar(!this.checked, obj);

    }

    //context = clicked input
    function _updateLockStatus(obj) {
        obj.readOnly = !this.checked;
    }

    //helper function, it attaches the given function to the elements in the array
    //it sets the call context to the input

    function addEventListenerToAllSetContext(ev, fields, fn) {
        for (var i = 0; i < fields.length; i++) {
            addEventHandler(fields[i], ev,
                    (function(obj) {
                        return function() { fn.call(obj) };
                    })
                    (fields[i]));
        }
    }


    function _getSaveData() {
        var data = {
            loanid: loanId
        };
        for (var i = 0; i < m_saveFields.length; i++) {
            var field = m_saveFields[i];
            if (field.tagName === 'INPUT' && (field.type === 'checkbox' || field.type === 'radio')) {
                data[field.id] = field.checked ? 'true' : 'false';
            }

            else {
                data[field.id] = field.value;
            }
        }

        if (m_dom.sInvestorLockPriceRowLockedT_Base.checked) {
            data.xPricingLock = 'BASE';
        }
        else if (m_dom.sInvestorLockPriceRowLockedT_Adjusted.checked) {
            data.xPricingLock = 'ADJUSTED';
        }

        $("#CountTable input").each(function() {
            var elem = $(this);
            if (elem.attr("type") == "checkbox") {
                data[elem.attr("id")] = elem.prop("checked");
            }
            else {
                data[elem.attr("id")] = elem.val();
            }
        });

        data.Adjustments = AdjustmentTable.Serialize();
        data.sFileVersion = sFileVersionField.value;
        return data;
    }

    function _save() {
        var data = _getSaveData(), results = gService.loanedit.call('Save', data), response = _handleResponse(results);
        if (response) {
            clearDirty();
        }
        return response;
    }

    //assumes the first argument is readonly and the rest are inputs
    function _disableCalendar() {
        var args = makeArray(arguments), isReadOnly = args.shift();
        for (var i = 0; i < args.length; i++) {
            var input = args[i];
            input.nextSibling.style.visibility = isReadOnly ? 'hidden' : 'visible';
        }
    }

    //context = input
    function _triggerPricingUpdate() {
    
        if (this === m_dom.sInvestorLockBrokComp1PcPrice) {
            _priceFeeUpdate.call(this, 'Price', 'Pricing', m_dom.sInvestorLockBrokComp1Pc);
        }
        else if (this === m_dom.sInvestorLockBaseBrokComp1PcPrice) {
            _priceFeeUpdate.call(this, 'Price', 'Pricing', m_dom.sInvestorLockBaseBrokComp1PcFee);
        }

        var data = {
            loanid: loanId
        };

        var neededFields = m_basePriceFields.concat(m_adjustedPriceFields);
        if (!m_isOptionArm) {
            neededFields.push(m_dom.sInvestorLockBaseOptionArmTeaserR);
            neededFields.push(m_dom.sInvestorLockOptionArmTeaserR);
        }

        for (var i = 0; i < neededFields.length; i++) {
            var field = neededFields[i];
            data[field.id] = field.value;
        }

        if (m_dom.sInvestorLockPriceRowLockedT_Base.checked) {
            data.xPricingLock = 'BASE';
        }
        else if (m_dom.sInvestorLockPriceRowLockedT_Adjusted.checked) {
            data.xPricingLock = 'ADJUSTED';
        }

        data.Adjustments = AdjustmentTable.Serialize();

        $("#CountTable input").each(function() {
            var elem = $(this);
            if (elem.attr("type") == "checkbox") {
                data[elem.attr("id")] = elem.prop("checked");
            }
            else {
                data[elem.attr("id")] = elem.val();
            }
        });

        var results = gService.loanedit.call('PricingUpdate', data);

        if (results.error) {
            alert(results.UserMessage);
            return false;
        }
        for (var attr in results.value) {
            if (results.value.hasOwnProperty(attr)) {
                m_dom[attr].value = results.value[attr];
            }
        }


        m_dom.sInvestorLockNoteIRQS.value = m_dom.sInvestorLockNoteIR.value;
        m_dom.InvestorRateLockPricingP.value = m_dom.sInvestorLockBrokComp1PcPrice.value;

        return true;

    }


    //context = input
    function _triggerPriceLockingUpdate() {
        _setReadOnly(true, m_basePriceFields, m_adjustedPriceFields);
        if (this === m_dom.sInvestorLockPriceRowLockedT_Adjusted) {
            _setReadOnly(false, m_adjustedPriceFields);
        }
        else {
            _setReadOnly(false, m_basePriceFields);
        }
    }

    function makeArray(array) {
        return [].slice.call(array);
    }

    //sets the field(s) to read only.
    //expects first parameter to be the isReadOnly.
    //other parameters can be array of elements or elements
    function _setReadOnly() {
        var fields = makeArray(arguments), isReadOnly = fields.shift();

        for (var i = 0; i < fields.length; i++) {
            var field = fields[i];
            if (field instanceof Array) {
                //flatten out field
                var parameters = [isReadOnly].concat(field);
                _setReadOnly.apply(this, parameters);
                continue;
            }
            if (field.tagName === 'INPUT') {
                switch (field.type) {
                    case 'text':
                        field.readOnly = isReadOnly;
                        break;
                    case 'button':
                    case 'submit':
                    case 'radio':
                    case 'checkbox':
                        field.disabled = isReadOnly;
                        break;
                    case 'hidden':
                        break;
                    default:
                        alert('unidentified type : ' + field.type);
                        break;
                }
            }
            else if (field.tagName === 'SELECT') {
                field.disabled = isReadOnly;
            }
            else if (field.tagName === 'A') {
                field.style.visibility = isReadOnly ? 'hidden' : 'visible';
            }
            else {
                alert('Unexpected tag name ' + field.tagName + ' Field : ' + field);

            }
        }
    }

    function _adjustmentTablePriceFeeUpdate(sourceId, destinationId, sourceType) {
        var source = document.getElementById(sourceId);
        var destination = document.getElementById(destinationId);
        _priceFeeUpdate.call(source, sourceType, 'Adjustment', destination);
    }

    function _openAuditItem(eventId) {
        showModeless(m_dom.mVRoot.value + '/newlos/LockDesk/InvestorRateLockEventInfo.aspx?loanid=' + loanId + '&eventid=' + eventId, "dialogHeight:600px; dialogWidth:600px;", null, window );
        return false;
    }


    //which functions do we want to expose? 
    return {
        init: init,
        save: _save,
        PriceFeeUpdate: _adjustmentTablePriceFeeUpdate,
        PricingUpdate: _triggerPricingUpdate,
        openAuditItem: _openAuditItem
    };

})();

function _init(){
    IRLPage.init();
}

function f_updateAdjustmentPriceFee(sourceId,destinationId,sourceType){
    IRLPage.PriceFeeUpdate(sourceId, destinationId, sourceType);
    
}



function f_adjustmentChange(update){
    if(update !== 'description'){
        IRLPage.PricingUpdate();
    }
}



