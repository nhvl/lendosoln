﻿jQuery(function ($) {
    var recipientWindow;

    var module = angular.module("DocuSignEnvelopeDashboard", []);
    module.controller("EnvelopeTableController", function EnvelopeTableController($scope) {
        $scope.Envelopes = Envelopes;
        $scope.IsInternal = ML.IsInternal;
        $scope.EnvelopeOrder = 'CreationDate';
        $scope.EnvelopeOrderReverse = true;

        $scope.SetSort = function(sort) {
            if ($scope.EnvelopeOrder === sort && !$scope.EnvelopeOrderReverse) {
                $scope.EnvelopeOrderReverse = true;
            }
            else {
                $scope.EnvelopeOrderReverse = false;
            }
            $scope.EnvelopeOrder = sort;
        };

        $scope.DownloadDocument = function (document) {
            var documentId = document.EdocId;
            window.open(ML.VirtualRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx" + '?docid=' + documentId, '_self');
        };

        $scope.StatusDetails = {};
        $scope.ShowEnvelopeStatuses = function (envelope) {
            $scope.StatusDetails = {
                ParentName: envelope.Name,
                StatusDates: envelope.StatusDates,
                HasNotes: envelope.StatusDatesHasNotes,
                HasDoneBy: true
            };

            setTimeout(function () {
                LQBPopup.ShowElement($('#StatusDetails'), {
                    width: 400,
                    height: 250,
                    elementClasses: 'FullWidthHeight',
                    hideCloseButton: true
                });
            }, 100);
        }

        $scope.RequestUpdate = function (envelope) {
            var id = envelope.Id;

            var data = {
                Id: id,
                LoanId: ML.sLId
            };

            PlaceRequest("RequestUpdate", data, OnRequestSuccess);
        }

        $scope.VoidDetails = {};
        $scope.RequestVoid = function (envelope) {
            var id = envelope.Id;
            var envelopeName = envelope.Name;

            $scope.VoidDetails = {
                EnvelopeName: envelopeName
            };

            setTimeout(function () {
                LQBPopup.ShowElement($('#VoidConfirmationPopup'), {
                    width: 500,
                    height: 325,
                    elementClasses: 'FullWidthHeight',
                    hideCloseButton: true,
                    onReturn: function (results) {
                        if (results == null) {
                            return;
                        }

                        var data = {
                            Id: id,
                            LoanId: ML.sLId,
                            VoidReason: results.VoidReason
                        };

                        return function () { PlaceRequest("RequestVoid", data, OnRequestSuccess); };
                    }
                });
            }, 100);
        }

        $scope.ResumeSigning = function(envelope) {
            var data = {
                Id: envelope.Id,
                LoanId: ML.sLId
            };

            PlaceRequest("ResumeSigning", data, ResumeSigningSuccess)
        };

        $scope.ShowRecipientDetails = function (envelopeId) {
            var url = ML.VirtualRoot + "/newlos/ElectronicDocs/DocuSignRecipientDashboard.aspx?loanid=" + ML.sLId + "&envelopeid=" + encodeURIComponent(envelopeId);
            if (!!!recipientWindow || recipientWindow.closed) {
                recipientWindow = window.open(url, "DocuSignRecipientDashboard", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes,width=800,height=400");
            }

            recipientWindow.focus();
        }

        function ResumeSigningSuccess(results, envelopeIndex) {
            if (results.value.Error) {
                alert(results.value.Error);
                return;
            }

            var url = results.value.RecipientViewUrl;
            window.open(url, "DocuSignEnvelope" + envelopeIndex, "toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes")
        }

        function OnRequestSuccess(results) {
            if (results.value.Error) {
                alert(results.value.Error);
                return;
            }

            var envelope = JSON.parse(results.value.Envelope);
            for (var i = 0; i < $scope.Envelopes.length; i++) {
                if ($scope.Envelopes[i].Id === envelope.Id) {
                    $scope.Envelopes[i] = envelope;
                    break;
                }
            }
            $scope.$apply();
        }

        function PlaceRequest(methodName, data, successCallback, envelopeIndex) {
            var loadingPopup = SimplePopups.CreateLoadingPopup("Processing request...");
            LQBPopup.ShowElement($('<div>').append(loadingPopup), {
                width: 350,
                height: 200,
                hideCloseButton: true,
                elementClasses: 'FullWidthHeight'
            });

            var result = gService.loanedit.callAsyncSimple(methodName, data,
                    function (results) {
                        LQBPopup.Return(null);
                        successCallback(results, envelopeIndex);
                    },
                    function (results) {
                        LQBPopup.Return(null);
                        alert(results.UserMessage);
                    }, null);
        }
    });

    module.directive('statusDetailsPopup', function () {
        return {
            restrict: 'A',
            templateUrl: 'Common/StatusDetailsPopup.html',
            scope: {
                StatusDetails: '<statusDetails'
            }
        };
    });

    $('body').on('click', '.ConfirmVoidBtn', function () {
        var voidReason = $(this).closest('.VoidConfirmationDiv').find('.VoidReason').val();

        if (voidReason == '') {
            alert("Please provide a void reason.");
        }
        else {
            LQBPopup.Return({ VoidReason: voidReason });
        }
    });

    $('body').on('input', '.VoidReason', function () {
        var voidReason = $(this).val();
        $(this).closest('.VoidConfirmationDiv').find('.VoidReasonCharCount').text(voidReason.length);
    });
});