﻿// Author: David Dao
// Created Date: 1/20/2010.
/*
Requirement:
Need to include these two js scripts before following code.
- jquery   (www.jquery.com)
- jquery-ui   (www.jqueryui.com) - for Sortable and Selectable
*/

/**
This class will load image one by one and have call back when the images are finish loading.
$(selector).loadImages(['img.png','img1.png']);
*/
jQuery.fx.interval = 100;

//var ____debugMsgs = [];
function console_log(msg)
{
    //    ____debugMsgs.push(msg);
    if (gIsDebug && typeof(console) != "undefined")
    {
        console.log(msg);
    }
}
function display_console_log()
{
    var length = ____debugMsgs.length;
    var oParent = jQuery('#debugConsoleMsg').empty();

    for (var i = 0; i < length; i++)
    {
        oParent.append(jQuery('<div>').text(____debugMsgs[i]));
    }
    jQuery('#debugConsole').dialog('open');
}

(function($)
{
    $.fn.dimension = function()
    {
        return { width: this.width(), height: this.height() };
    };
})(jQuery);

if (!LendersOffice)
{
    var LendersOffice = {};
}
LendersOffice.PdfEditor = function(editorId, oPageList, options) {
    if (!editorId || !oPageList) {
        alert('Missing Arguments');
        return null;
    }
    var $ = jQuery;

    var FULL_PANEL_ID = 'PdfEditorFullPanel';
    var THUMB_PANEL_ID = 'PdfEditorThumbPanel';
    var PANEL_CLASS = 'PdfEditorPanel';

    var _fullPanelSelector = '#' + FULL_PANEL_ID;
    var _thumbPanelSelector = '#' + THUMB_PANEL_ID;

    //These can only be set after we setup the panels.
    var $fullPanel;
    var $thumbPanel;

    var EVT_TYPE_NS = ".PdfEditor";
    var PAGE_STATUS_CHANGED_EVT_TYPE = "PageStatusChangedEventType" + EVT_TYPE_NS;
    var ERROR_EVT_TYPE = "ErrorEventType" + EVT_TYPE_NS;
    var PAGE_MODIFIED_EVT_TYPE = "PageModifiedEventType" + EVT_TYPE_NS;
    var PDF_FIELD_DRAG_EVT_TYPE = "PdfFieldDragEventType" + EVT_TYPE_NS;
    var PAGE_MOVED_EVT_TYPE = "PageMovedEventType" + EVT_TYPE_NS;
    var PDF_FIELD_DROP_EVT_TYPE = "PdfFieldDropEventType" + EVT_TYPE_NS;
    var PDF_FIELD_SELECTED_STOP_EVT_TYPE = "PdfFieldSelectedStopEventType" + EVT_TYPE_NS;
    var PAGE_SELECTION_STOP_EVT_TYPE = "PdfPageSelectionStop" + EVT_TYPE_NS;

    var SCROLLED_PAST_LAST_PAGE_EVT_TYPE = "PdfEditorScrolledPastLastPage" + EVT_TYPE_NS;
    var SCROLLED_BEFORE_FIRST_PAGE_EVT_TYPE = "PdfEditorScrolledBeforeFirstPage" + EVT_TYPE_NS;

    var CSS_PAGE_NUMBER = "pdf-page-number";
    var CSS_DOC_TITLE = "pdf-page-title";
    var CSS_PAGE = "pdf-page";
    var CSS_PAGE_OVERLAY = "pdf-page-overlay";
    var CSS_PAGE_SELECTED = "pdf-page-selected";
    var CSS_PAGE_PANEL = "pdf-page-panel";
    var CSS_LOADING_PANEL = "pdf-loading-panel";
    var CSS_PDF_FIELD = "pdf-pdf-field";
    var CSS_PDF_THUMB_FIELD = "pdf-pdf-thumb-field";

    var hasMsTransform = false;

    var CSS_IMG_ROTATE_0 = "img-rotate-0";
    var CSS_IMG_ROTATE_90 = "img-rotate-90";
    var CSS_IMG_ROTATE_180 = "img-rotate-180";
    var CSS_IMG_ROTATE_270 = "img-rotate-270";

    var DATA_PDF_FIELD = "pdfFieldData";
    var DATA_PDF_FIELD_ID = "pdfFieldDataId";
    var DATA_PDF_THUMB_DATA = 'pdfThumbData';
    var DATA_PDF_PAGE_NEED_DROPPABLE_SETUP = '_DATA_PDF_PAGE_NEED_DROPPABLE_SETUP';

    var DATA_CACHE_WIDTH = "___cache_width____";
    var DATA_CACHE_HEIGHT = "___cache_height____";
    var DATA_IMG_ROTATE_CSS = "___data_img_rotate__";
    var IDX_PAGE_KEY = "__id__";
    var PAGE_CLASS_PREFIX = "pgid_";

    var _isInitialized = false;

    var _globalUniqueIdCount = 0;

    //var _fullPanelCache = [], _thumbCache = [];	

    // Private member variables
    var _thumbnailScale = 0.10;
    var _editorSelector = '#' + editorId;
    var _pageList = oPageList;
    var _currentPageIndex = 0;
    var _selectableFilter = '.' + CSS_PDF_FIELD + ':not(.disable-selectable)';
    var _pagesByID = [];

    // Private functions
    function _convertToJsCoord(lx, ly, ux, uy, js_page_height, js_scale, pdfScale) {
        js_scale = js_scale || 1;
        return {
            width: (Math.abs(ux - lx) * pdfScale * js_scale) + "px",
            height: (Math.abs(uy - ly) * pdfScale * js_scale) + "px",
            top: (js_page_height - Math.max(uy, ly) * pdfScale * js_scale) + "px",
            left: (Math.min(lx, ux) * pdfScale * js_scale) + "px",
            position: "absolute"
        };
    }
    function _convertToPdfCoord(top, left, width, height, js_page_height, pdfScale) {
        return {
            lx: _round(left / pdfScale),
            ly: _round((js_page_height - (top + height)) / pdfScale),
            ux: _round((left + width) / pdfScale),
            uy: _round((js_page_height - top) / pdfScale)

        };
    }
    function _round(v) {
        return Math.round(v * 1000) / 1000;
    }

    function _isInEditorViewport(element) {
        var $element = $(element);

        var editor = $('#' + FULL_PANEL_ID);
        var editorHeight = editor.height();
        var editorWidth = editor.width();

        var elementPosition = $element.position();
        var pagePosition = $element.closest('.pdf-page-parent').position();

        // Roughly 5 pixels between each page vertically.
        var elementTop = elementPosition.top + pagePosition.top + 5;
        var elementBottom = elementTop + $element.outerHeight();

        // Roughly 25 pixels of empty space on the left.
        var elementLeft = elementPosition.left + pagePosition.left + 25;
        var elementRight = elementLeft + $element.outerWidth();

        return !(elementTop < 0 || elementLeft < 0 || elementBottom > editorHeight || elementRight > editorWidth);
    }

    function _getPdfFields(context, filterSelector) {

        //Older calls to this method assume these default parameters
        context = context || _fullPanelSelector;
        filterSelector = filterSelector || ":not(.saved)";

        var list = [];

        _disableTFSForPositionCalc();


        $(context).find('.' + CSS_PDF_FIELD).filter(filterSelector).each(function() {

            var pageHeight = $(this).parent().height();

            var p = $(this).position();
            var d = $(this).dimension();

            var o = $(this).data(DATA_PDF_FIELD);

            var pdfPage = _findPageObjectById($(this).closest('li').data(IDX_PAGE_KEY));

            var scale = pdfPage.Scale || 1;
            var pdfCoord = _convertToPdfCoord(p.top, p.left, d.width, d.height, pageHeight, scale);
            list.push($.extend({}, o, pdfCoord));
        });

        _enableTFSForPositionCalc();

        return list;
    }
    var g_selectedPdfFields = [];
    function _getSelectedPdfFields() {
        var selectedFields = $fullPanel.find(_selectableFilter).filter(function() { return $(this).hasClass('ui-selected'); });

        var result = [];
        $.each(g_selectedPdfFields, function(idx, o) {
            var id = o;
            $.each(selectedFields, function() { if (id === $(this).data(DATA_PDF_FIELD_ID)) { result.push($(this)); return false; } });
        });
        return result;
    }
    function _removePdfField(o) {
        var id = $(o).data(DATA_PDF_FIELD_ID);
        if (!!id) {
            $.each(g_selectedPdfFields, function(idx, obj) { if (obj === id) { g_selectedPdfFields.splice(idx, 1); return false; } });
            var data = $(o).data(DATA_PDF_THUMB_DATA);
            if (data && data.ThumbId) {
                data.Thumb.remove();
            }
            $(o).remove();
            $(__pdfEditor).trigger(PDF_FIELD_SELECTED_STOP_EVT_TYPE, [g_selectedPdfFields.length]);

        }
    }
    function _getFields() {
        return $fullPanel.find('.' + CSS_PDF_FIELD);
    }
    function _addPdfField(o, bAddToThumb) {
        if (o.pg == 0)  //why is this here? 
        {
            o.pg = 1;
        }
        if (o.pg == null || o.pg <= 0 || o.pg > _pageList.length) {
            return;
        }
        if (!__isPdfEditorFinishLoading) return;

        var uniqueId = _globalUniqueIdCount++;
        var pgObject = _pageList[o.pg - 1];
        var pdfScale = pgObject.Scale || 1;

        if (!!bAddToThumb) {
            var thumbPage = _FindInThumbPanel(pgObject).find('.' + CSS_PAGE);
            var thumbHeight = thumbPage.data(DATA_CACHE_HEIGHT);
            if (null == thumbHeight) {
                thumbHeight = $(thumbPage).height();
                thumbPage.data(DATA_CACHE_HEIGHT, thumbHeight);
            }
            var jsThumbCoord = _convertToJsCoord(o.lx, o.ly, o.ux, o.uy, thumbHeight, _thumbnailScale, pdfScale);
            var oThumbDiv = $("<div>").css(jsThumbCoord).addClass(o.cssClass);
            oThumbDiv.data(DATA_PDF_FIELD, o).data(DATA_PDF_FIELD_ID, uniqueId);
            oThumbDiv.data("__JSCOORD__", jsThumbCoord);
            thumbPage.append(oThumbDiv);
            _InitializePdfField(oThumbDiv);
            oThumbDiv.removeClass(CSS_PDF_FIELD).addClass(CSS_PDF_THUMB_FIELD);
            console_log('insert thumb');
        }
        var fullPage = _FindInFullPanel(pgObject).find('.' + CSS_PAGE);
        var fullHeight = fullPage.data(DATA_CACHE_HEIGHT);
        if (null == fullHeight) {
            // For some reason, in IE11, Jquery's height function gives the wrong value for only the first call and only for pages after the first.
            // So if an annotation is on page 4 and we called $(fullPage).height(), it would be wrong until we called $(fullPage).height() a second time.
            // So instead, we'll just use clientHeight and subtract out the padding.
            var page = $(fullPage)[0];
            var jsHeight = page.clientHeight;
            var paddingTotal = parseInt($(page).css('padding-top').replace('px',''), 10) + parseInt($(page).css('padding-bottom').replace('px',''), 10);
            fullHeight = jsHeight - paddingTotal;
            // fullHeight = $(fullPage).height();
            console_log("Full height is " + fullHeight + ", pageobj height is: " + pgObject.PageHeight);
            fullPage.data(DATA_CACHE_HEIGHT, fullHeight);
        }
        var jsCoord = _convertToJsCoord(o.lx, o.ly, o.ux, o.uy, fullHeight, 1, pdfScale);
        //var oDiv = $("<div>").css(jsCoord).addClass(o.cssClass);
        var oDiv = $("<div class='" + o.cssClass + "' style='position:absolute;width:" + jsCoord.width + ";height:" + jsCoord.height + ";top:" + jsCoord.top + ";left:" + jsCoord.left + "'></div>");
        oDiv.data("__JSCOORD__", jsCoord);
        if (!!_settings.OnRenderPdfFieldContent) {
            var oInner = _settings.OnRenderPdfFieldContent.call(oDiv, o);
            if (oInner != null) {
                oDiv.append(oInner.addClass('pdf-field-content'));
            }
        }
        if (oDiv.hasClass('pdf-field-checkbox')) {
            oDiv.find('.pdf-field-content').width(jsCoord.width).height(jsCoord.height);
        }
        oDiv.data(DATA_PDF_FIELD, o).data(DATA_PDF_FIELD_ID, uniqueId);
        fullPage.append(oDiv);
        _InitializePdfField(oDiv);

        return oDiv;
    }

    function _addDynamicThumbnail(oPdfField, oThumbnailContent) {
        var fieldData = $(oPdfField).data(DATA_PDF_FIELD);
        var uniqueId = $(oPdfField).data(DATA_PDF_FIELD_ID);
        var thumbId = 'pdf-field-thumbnail-' + uniqueId;
        var thumbData = {};
        $(oPdfField).data(DATA_PDF_THUMB_DATA, thumbData);

        var thumbPage = _FindInThumbPanel(_pageList[fieldData.pg - 1]).find('.' + CSS_PAGE);

        var jsThumbCoord = null;

        var __jsCoord__ = $(oPdfField).data("__JSCOORD__");
        if (__jsCoord__ == null) {
            jsThumbCoord = _convertToScaledJsCoord(oPdfField, _thumbnailScale);
        } else {
            jsThumbCoord = _convertToScaledJsCoordWithPosition(parseInt(__jsCoord__.width.replace("px", "")),
                    parseInt(__jsCoord__.height.replace("px", "")),
                    parseInt(__jsCoord__.top.replace("px", "")),
                    parseInt(__jsCoord__.left.replace("px", "")),
                    _thumbnailScale);
        }
        var oThumbDiv = oThumbnailContent.css(jsThumbCoord).addClass('pdf-field-thumbnail ' + thumbId);
        thumbData.ThumbId = thumbId;
        thumbData.Thumb = oThumbDiv;
        thumbPage.append(oThumbDiv);
        $(oPdfField).resize(function() {
            var jsThumbCoord = _convertToScaledJsCoord(this, _thumbnailScale);
            $(this).data(DATA_PDF_THUMB_DATA).Thumb.css(jsThumbCoord);
        });
    }

    function _EnforceBoundaries(o) {
        _disableTFSForPositionCalc();

        var oParent = $(o).parent();
        var parentWidth = oParent.data(DATA_CACHE_WIDTH);
        var parentHeight = oParent.data(DATA_CACHE_HEIGHT);

        if (parentWidth == null) {
            parentWidth = oParent.width();
            oParent.data(DATA_CACHE_WIDTH, parentWidth);
        }
        if (parentHeight == null) {
            parentHeight = oParent.height();
            oParent.data(DATA_CACHE_HEIGHT, parentHeight);
        }
        var childWidth = 0;
        var childHeight = 0;
        var positionChildLeft = 0;
        var positionChildTop = 0;

        var jsCoord = $(o).data("__JSCOORD__");
        var width1 = 0;
        var height1 = 0;
        if (jsCoord != null) {
            // 5/19/2011 dd - since the different between outerWidth and outerHeight vs width and height is about 2 px.
            // Therefore I will choose speed over extreme accuracy.
            //childWidth = $(o).outerWidth();
            //childHeight = $(o).outerHeight();


            positionChildLeft = parseInt(jsCoord.left.replace("px", ""));
            positionChildTop = parseInt(jsCoord.top.replace("px", ""));

            width1 = parseInt(jsCoord.width.replace("px", ""));
            height1 = parseInt(jsCoord.height.replace("px", ""));
            childWidth = width1;
            childHeight = height1;

        }
        else {
            //childWidth = $(o).outerWidth();
            //childHeight = $(o).outerHeight();
            var positionChild = $(o).position();
            positionChildLeft = positionChild.left;
            positionChildTop = positionChild.top;

            width1 = $(o).width();
            height1 = $(o).height();
            childWidth = width1;
            childHeight = height1;
        }

        var leftOverage = positionChildLeft + childWidth - parentWidth;
        var topOverage = positionChildTop + childHeight - parentHeight;

        var changed = false;
        if (leftOverage > 0) {
            changed = true;
            $(o).css('left', positionChildLeft - leftOverage);
        }

        if (topOverage > 0) {
            changed = true;
            $(o).css('top', positionChildTop - topOverage);
        }

        var thumbData = $(o).data(DATA_PDF_THUMB_DATA);
        if (changed && thumbData != null && thumbData.Thumb) {
            var coordinates = _convertToScaledJsCoordWithPosition($(o).width(), $(o).height(), positionChildTop, positionChildLeft, _thumbnailScale);
            //var coordinates = _convertToScaledJsCoord(o, _thumbnailScale);
            thumbData.Thumb.css(coordinates);
        }

        _enableTFSForPositionCalc();
    }
    function _convertToScaledJsCoordWithPosition(w, h, t, l, scale) {
        scale = scale || 1;
        return {
            width: (w * scale) + "px",
            height: (h * scale) + "px",
            top: (t * scale) + "px",
            left: (l * scale) + "px",
            position: "absolute"
        };

    }
    function _convertToScaledJsCoord(o, scale) {
        scale = scale || 1;
        return {
            width: ($(o).width() * scale) + "px",
            height: ($(o).height() * scale) + "px",
            top: ($(o).position().top * scale) + "px",
            left: ($(o).position().left * scale) + "px",
            position: "absolute"
        };
    }

    function _SetupInteractionPdfField(o) {
        var bInitialized = $(o).data('___INITIALIZED___');

        if (!!bInitialized) {
            return;
        }

        _disableTFSForPositionCalc();

        _EnforceBoundaries(o);

        _enableTFSForPositionCalc();

        if (_settings.AllowEdit() || _settings.CanEditAnnotations) {

            if ($(o).is(':not(.disable-move)')) {
                $(o).draggable({
                    cancel: '.disable-move,:input,option',
                    containment: 'parent',
                    start: function(evt, ui) {
                        $fullPanel.selectable("disable");
                        $(__pdfEditor).trigger(PAGE_MODIFIED_EVT_TYPE);
                    },
                    stop: function(evt, ui) {
                        $fullPanel.selectable("enable");
                        _EnforceBoundaries(this);
                        var data = $(this).data(DATA_PDF_THUMB_DATA);
                        if (data && data.ThumbId) {
                            var jsThumbCoord = _convertToScaledJsCoord(this, _thumbnailScale);
                            data.Thumb.css(jsThumbCoord);
                        }
                    }
                });
            }

            if ((($(o).hasClass('pdf-field-signature') == false && $(o).hasClass('pdf-field-initial') == false && $(o).hasClass('pdf-field-signature-date') == false) || _settings.OverrideResizeForSignatureFields) &&
                $(o).is(':not(.master-disable-resize)'))
            {
                var ar = $(o).hasClass('pdf-field-signature-img') ? $(o).children('img').attr('ar') : false;
                var w = $(o).attr('min-resize-width');
                var h = $(o).attr('min-resize-height');
                $(o).resize(function (e)
                {
                    e.stopPropagation();
                }); //dont propogate this event on just the field 
                $(o).resizable({
                    cancel: '.disable-resize,.master-disable-resize,:input,option',
                    autoHide: true,
                    containment: 'parent',
                    aspectRatio: ar,
                    minWidth: w,
                    minHeight: h,
                    start: function(evt, ui) {
                        $fullPanel.selectable("disable");
                        $(__pdfEditor).trigger(PAGE_MODIFIED_EVT_TYPE).
                            trigger(PDF_FIELD_DRAG_EVT_TYPE);

                    },
                    resize: function(evt, ui) {
                        var o = $(evt.target);
                        var data = o.data(DATA_PDF_FIELD);
                        if (o.is('.pdf-field-note') && data.NoteStateType == 'maximized') {
                            data.width = ui.size.width;
                            data.height = ui.size.height;
                        }
                        //                        o.find('.pdf-field-content').css(ui.size);
                    },
                    stop: function(evt, ui) {
                        $fullPanel.selectable("enable");
                        var o = $(evt.target);
                        if (o.hasClass('pdf-field-checkbox')) {
                            o.find('.pdf-field-content').width(ui.size.width).height(ui.size.height);
                        }
                        _EnforceBoundaries(o.get(0));
                        var data = o.data(DATA_PDF_FIELD);
                        if (o.is('.pdf-field-note') && data.NoteStateType == 'maximized') {
                            data.width = ui.size.width;
                            data.height = ui.size.height;
                        }
                        var data = o.data(DATA_PDF_THUMB_DATA);
                        if (ar) {
                            var newWidth = ui.size.height * ar;
                            $(o).css('width', newWidth + "px");
                            $(o).css('height', ui.size.height + "px");
                        }
                        if (data && data.ThumbId) {
                            var jsThumbCoord = _convertToScaledJsCoord(this, _thumbnailScale);
                            data.Thumb.css(jsThumbCoord);
                        }

                    }
                });
            }
        }
        $(o).data('___INITIALIZED___', true);
    }
    function _InitializePdfField(o, bForceSetupInteraction) {
        $(o).data('isActualField', true).addClass(CSS_PDF_FIELD);

        if (!!bForceSetupInteraction) {
            // 5/19/2011 dd - This method is expensive therefore only call when necessary.
            // During load time I delay the setup.
            _SetupInteractionPdfField(o);

        }

        var data_pdf = $(o).data(DATA_PDF_FIELD);
        if (null == data_pdf) {
            data_pdf = {};
            $(o).data(DATA_PDF_FIELD, data_pdf);
        }

        if (null == data_pdf.pg) {
            // Find the pg number and set valid cssClass
            var key = $(o).parents('li').data(IDX_PAGE_KEY);

            var pg = -1;
            $.each(_pageList, function(idx, v) { if (v[IDX_PAGE_KEY] == key) { pg = idx + 1; return false; } });
            data_pdf.pg = pg;
        }

        if (null == data_pdf.cssClass) {
            $.each(['pdf-field-text', 'pdf-field-checkbox', 'pdf-field-signature', 'pdf-field-initial', 'pdf-field-note', 'pdf-field-highlight', 'pdf-field-signature-img', 'pdf-field-date',
                    'pdf-field-esign-signature', 'pdf-field-esign-initial', 'pdf-field-esign-date', 'pdf-field-signature-date'], function () {
                if ($(o).hasClass(this)) {
                    data_pdf.cssClass = this;
                    return false;
                }
            });
        }
    }
    function _setupDroppableOnPage(oPageDiv) {
        var accept = '.pdf-field-template';

        //if user cannot edit the pdf then dont allow them to drop signatures/dates.
        if (oPageDiv.hasClass('cannot_add_sig')) {
            accept += ":not(.pdf-field-signature-img,.pdf-field-date)";
        }

        oPageDiv.droppable({
            accept: accept,
            drop: function(evt, ui) {
                var isActualField = !!ui.helper.data('isActualField');
                if (isActualField == false) {
                    var o = ui.helper.clone();
                    o.appendTo(this);
                    var uniqueId = _globalUniqueIdCount++;
                    o.data(DATA_PDF_FIELD_ID, uniqueId);
                    if (_settings.OnInitialDropRenderField) {
                        _settings.OnInitialDropRenderField.call(o.get(0));
                    }
                    _InitializePdfField(o, true);
                    o.offset(ui.offset);
                    _EnforceBoundaries(o.get(0));
                    $(__pdfEditor).trigger(PAGE_MODIFIED_EVT_TYPE);
                    var x = evt.clientX;
                    var y = evt.clientY;
                    $(__pdfEditor).trigger(PDF_FIELD_DROP_EVT_TYPE, [o, x, y]);
                }
            }
        });
    }

    //Javascript is a lot happier with you if you bulk append html elements to the page,
    //rather than doing them one at a time
    var bulkindex = 0;
    function _bulkAppendPngPages(parent, src, oPageList, insertAt, bDroppable, bIsThumbPanel, bInsertAtEnd, extraScaleFactor) {
        var length = oPageList.length;
        //We'll use this as a string buffer in order to avoid serial string concats
        var htmlFragmentText = [];
        //Since the actual html objects will all be created in one go, we'll store the jQuery Data
        //objects in here.
        var datas = [];

        for (var i = 0; i < length; i++) {
            var o = oPageList[i];
            var w = o.PageWidth * (o.Scale || 1) * extraScaleFactor;
            var h = o.PageHeight * (o.Scale || 1) * extraScaleFactor;

            // Generate <li class="pgid_[page key] pdf-page-parent" id="[parent id]_[page key]>
            //              <div class="pdf-page-panel">
            //                 (<span class="pdf-page-number">page # goes here</span>)? if it's a thumbnail
            //                  <div class="pdf-page">
            //                     (<div class="Magnify" />)? if it's a thumbnail
            //                      <img class='page' src='page url goes here'/>
            //                  </div>
            //              </div>
            //          </li>
            //(we don't always generate the page # span or magnify div)

            var sMainStyle = "style ='width:" + w + "px;height:" + h + "px;'";
            //We put a page key in the class, so we can get all display pages that reference the page id with $('.' + page_class_prefix + page_key)
            //We also add an id, so we can get this display page specifically with $(parent_selector + "_" + page_key)
            htmlFragmentText.push("<li " + sMainStyle + " class='" + PAGE_CLASS_PREFIX + o[IDX_PAGE_KEY] + " pdf-page-parent' id='" + parent[0].id + "_" + o[IDX_PAGE_KEY] + "'>");
            htmlFragmentText.push("<div class='" + CSS_PAGE_PANEL + "'>");

            //It would be really nice to use the <li>'s native number, but there's a period after it
            //(e.g, 1.) and there's no clean way to get rid of that in IE 7/8. So we make a placeholder here, and
            //fill it in _resyncThumbPageNumber. (the native number is moved elsewhere by CSS trickery)
            if (bIsThumbPanel) {
                htmlFragmentText.push("<span class='" + CSS_PAGE_NUMBER + "'></span>");
            }

            var cssClass = CSS_PAGE;
            if (o.CanEditSource === false) {
                cssClass += " cannot_add_sig";
            }

            htmlFragmentText.push("<div class='" + cssClass + "' >");
            if (bIsThumbPanel && i == 0 && o.Name) {
                htmlFragmentText.push("<div class='" + CSS_PAGE_OVERLAY + " overlay_" + bulkindex + "'>" + HtmlEncode(o.Name) + "</div>");
            }
            if (bIsThumbPanel) {
                htmlFragmentText.push("<div class='Magnify'> </div>");
            }
            htmlFragmentText.push("<img class='page' src='" + encodeURI(src) + "'/>");
            htmlFragmentText.push("</div>"); // </div CSS_PAGE
            htmlFragmentText.push("</div>"); // </div CSS_PAGE_PANEL
            htmlFragmentText.push("</li>"); // </li sMainStyle

            var bNeedDroppable = false;
            if ((_settings.AllowEdit() || _settings.CanEditAnnotations) && bDroppable) {
                bNeedDroppable = true;

            }
            //Store this data for later, since the element it will be associated with is just a string
            //fragment right now.
            var oData = {};
            oData[IDX_PAGE_KEY] = o[IDX_PAGE_KEY];
            oData[DATA_PDF_PAGE_NEED_DROPPABLE_SETUP] = bNeedDroppable;
            oData[DATA_CACHE_WIDTH] = w;
            oData[DATA_CACHE_HEIGHT] = h;
            datas.push(oData);
        }

        //var html = $(htmlFragmentText.join(''));
        //The above code does work very well, but unfortunately in the $(htmlFragmentText.join('')) call
        //jQuery takes a long time to ensure the html will work (at clean:)- but we don't need that.

        //This is faster, but only works because we generated moderately well-behaved HTML earlier.
        var tempDiv = document.createElement("div");
        var docFragment = document.createDocumentFragment();

        //Make it html - now we've got <div> htmlFragmentText </div>
        tempDiv.innerHTML = htmlFragmentText.join("\n");

        //Adding a single top level element to the DOM is faster than adding the element's children 
        //individually, but here it'll put an unecessary div in the page. 
        //Instead, we'll use a document fragment as the single top level element,
        //which doesn't add any html.
        //(note that document fragments don't have an innerhtml, so we can't do this in one direct step)
        var children = tempDiv.childNodes;
        while (children[0]) {
            docFragment.appendChild(children[0]);
        }
        var html = $(docFragment);

        html.children().each(function(idx, elem) {
            $(elem).data(datas[idx]);

            // because _doScroll doesn't always work, mouseover will update the thumbnails source until _doScroll's bug is fixed.
            if (bIsThumbPanel && typeof (GetThumbUrl) == "function") {
                $(elem).mouseover(function() {
                    var $img = $(this).find('img');
                    var imgSrc = $img.attr('src');
                    if (imgSrc.indexOf('pixel.gif') > 0) {
                        $img.attr('src', GetThumbUrl(_findPageObjectById($(this).data()[IDX_PAGE_KEY])));
                    }
                });
            }
        });

        if (bInsertAtEnd) {
            $(parent).append(html);
        }
        else {
            var $liColl = $("li", parent);
            var insertedPosition = insertAt;
            if (insertedPosition == $liColl.length) {
                $(parent).append(html);
            } else {
                $liColl.eq(insertedPosition).before(html);
            }
        }
    }

    function _recalculateAllHeights() {
        _recalculateHeights(_fullPanelSelector);
        _recalculateHeights(_thumbPanelSelector);
    }

    var _pageHeightCaches = {};
    function _recalculateHeights(selector) {
        var heights = [], pageIndex = [], currTop = -1, o;

        $(selector).children('li').each(function(i, oLi) {
            o = oLi.offsetTop;

            if (o == currTop) {
                return;
            }

            currTop = o;
            heights.push(o);
            pageIndex.push(i);
        });

        _pageHeightCaches[selector] = { h: heights, i: pageIndex };
        console_log('ran ' + heights.length);
    }

    var __isPdfEditorFinishLoading = false;
    function _loadPagePanel(list, insertAt, onCompletedHandle) {

        var length = list == null ? 0 : list.length;
        var timer = new Date();
        var _fullPanel = $fullPanel;
        var _thumbPanel = $thumbPanel;
        var bInsertAtEnd = insertAt == _fullPanel.children().length;
        var manageThumbnails = _settings.ManageThumbnailPanel;

        _bulkAppendPngPages(_fullPanel, _settings.EmptyImgSrc, list, insertAt, true, false, bInsertAtEnd, 1);
        if (manageThumbnails) {
            _bulkAppendPngPages(_thumbPanel, _settings.EmptyImgSrc, list, insertAt, false, true, bInsertAtEnd, _thumbnailScale);

            if (_settings.AllowEdit()) {
                _enableInteraction();
            } else {
                _disableInteraction(true/*hideDisabledClass*/);
            }
        }
        _recalculateAllHeights();
        _syncLocalPageNumbers();

        __isPdfEditorFinishLoading = true;
        timer = (new Date()) - timer;
        console_log('_loadPagePanel Part 1 in ' + timer + 'ms.');
        timer = new Date();

        _resyncThumbPageNumber();

        // 6/23/2015 - dd - Add support for initial page to start with rotation other than 0 degree.
        var bHasRotation = false;
        for (var i = 0; i < length; i++) {
            var oPage = list[i];
            if (oPage.Rotation == 0) {
                continue;
            }
            oPage.Rotation = _normalizeAngle(oPage.Rotation);
            if (typeof (oPage.ClientRotation) == "undefined" || oPage.ClientRotation == null) {
                oPage.ClientRotation = oPage.Rotation;
            }
            _applyRotation(oPage, 0);
            bHasRotation = true;

        }
        if (bHasRotation) {
            _enableTFSForPositionCalc();

            _clearPagePositionCacheData();
        }
        _hideWaiting();

        if (_settings.GoToNewDocOnAddition) {
            _onSelectedPage(_pageList[insertAt], true);
        }

        if (typeof onCompletedHandle === 'function') {
            var timerCompletedHandle = (new Date());
            onCompletedHandle();
            timerCompletedHandle = (new Date()) - timerCompletedHandle;
            console_log("_loadPagePanel - onCompleteHandle in " + timerCompletedHandle + " ms.");
        }

        timer = (new Date()) - timer;
        console_log('_loadPagePanel Part 2 in ' + timer + 'ms.');
        bulkindex += 1;

        //The pdf fields that get created in the onCompleteHandle are made drag & droppable in _scrollImpl
        //Thus, we should only call this guy (which wraps scrollImpl) after the fields have been created
        //Note that the fields will still be drag and droppable if we call these guys before the handle -
        //_scrollImpl and this function are debounced, and will wait some miliseconds to execute.
        _refreshPageImages();
    }

    function _handleError(msg) {
        _hideWaiting();
        $(__pdfEditor).trigger(ERROR_EVT_TYPE, msg);
    }

    var _thumbScrollTimeout = null;
    var _thumbScrollInterval = 100;
    var _previousPageIndex = null;
    function _onSelectedPageByIdxPageKey(idxPageKey, bIsScrollBegining) {
        var bIsFound = _pagesByID[idxPageKey] != null;

        if (bIsFound == false) {
            //This happens every time the thumb panel scrolls, because the selected page is the page which occupies
            //50% or more of the view screen height and thumbnails never do. Or at least they aren't supposed to.
            //A thumbnail of an 8.5x20ish sheet of paper would trigger it.
            console_log("Tried to select a page which doesn't exist, id is: " + idxPageKey);
            return;
        }
        console_log("Selecting page: " + idxPageKey);
        _currentPageIndex = $.inArray(_pagesByID[idxPageKey], _pageList);

        //If nothing's different, we don't need to continue
        if (_currentPageIndex == _previousPageIndex) {
            return;
        }

        //We're scrolling to the beginning of the selected page if we were told to, or if we're skipping
        //around by more than four pages. Four is arbitrary.
        bIsScrollBegining = bIsScrollBegining || (Math.abs(_currentPageIndex - _previousPageIndex) > 4)

        //Are we scrolling to the beginning of the selected page?
        if (bIsScrollBegining) {
            var fullPanel = $fullPanel;

            var currentPage = fullPanel.children().eq(_currentPageIndex);

            if (currentPage.length) {
                var t = fullPanel.scrollTop() + currentPage.position().top;
                fullPanel.scrollTop(t);
            }
        }

        if (_settings.ManageThumbnailPanel) {
            var thumbPanel = $thumbPanel;
            var thumbHeight = __panelHeight;
            var thumbChildren = thumbPanel.children();
            var pg = null;
            if (_previousPageIndex != null) {
                pg = thumbChildren.eq(_previousPageIndex);
                pg.find("." + CSS_PAGE).removeClass(CSS_PAGE_SELECTED);
            }
            pg = thumbChildren.eq(_currentPageIndex);
            var imgPg = pg.find("." + CSS_PAGE);
            imgPg.addClass(CSS_PAGE_SELECTED);


            //This is too expensive to do all the time (primarily imgPg.position()) 
            //so debounce the scroll events so it only happens once the user is done scrolling
            clearTimeout(_thumbScrollTimeout);
            _thumbScrollTimeout = setTimeout(function() {
                var p = imgPg.position();
                if (!p) return;
                var t = p.top;

                console_log("Doing thumbnail scroll");
                //User was scrolling down, hit the bottom edge of the screen - go a "page" down
                if (t > thumbHeight) {
                    imgPg[0].scrollIntoView(true);
                }

                //User was scrolling up, hit the top edge of the screen - go a "page" up
                else if (t < 0) {
                    imgPg[0].scrollIntoView(false);
                }
            }, _thumbScrollInterval);
        }
        _previousPageIndex = _currentPageIndex;
        $(__pdfEditor).trigger(PAGE_STATUS_CHANGED_EVT_TYPE, __pdfEditor);

    }
    function _onSelectedPage(o, bIsScrollBegining) {
        if (null == o) return;
        _onSelectedPageByIdxPageKey(o[IDX_PAGE_KEY], bIsScrollBegining);
    }

    function _findPageObjectByLi(li) {
        var elemId;
        //Pull it out if it's a bare object
        if (li) elemId = li.id;
        //Otherwise, maybe it's a jq object
        if (!elemId) elemId = li[0].id;

        if (!elemId) return;

        var parts = elemId.split('_');
        var pageId = parts[parts.length - 1];
        return _pagesByID[pageId];
    }

    function _findPageObjectById(id) {
        return _pagesByID[id];
    }

    function _FindInFullPanel(o) {
        return _FindByIdxPageKey(_fullPanelSelector, o);
    }

    function _FindInThumbPanel(o) {
        return _FindByIdxPageKey(_thumbPanelSelector, o);
    }

    function _FindByIdxPageKey(selector, o) {
        return $(selector + "_" + o[IDX_PAGE_KEY]);
    }

    function _SelectPages(o) {
        return $('.' + PAGE_CLASS_PREFIX + o[IDX_PAGE_KEY]);
    }

    function _refreshPages() {
        if (_pageList.length <= 0) return;


        var p0 = _pageList[0];
        var el0 = _FindInFullPanel(p0);
        var length = _pageList.length;
        for (var i = 1; i < length; i++) {
            var p1 = _pageList[i];
            var el1 = el0.next();

            if (el1.data(IDX_PAGE_KEY) === p1[IDX_PAGE_KEY]) {
                // NO-OP
                el0 = el1;
            } else {
                el1 = _FindInFullPanel(p1);
                el0.after(el1);
                el0 = el1;
            }

            p0 = p1;
        }

    }

    //This is a tricky function to name - it returns the percentage of the parent that we're currently filling.
    function _PercentOfParentView(parentHeight, myTop, myHeight) {
        return ((parentHeight < (myTop + myHeight) ? parentHeight : (myTop + myHeight)) - (myTop < 0 ? 0 : myTop < parentHeight ? myTop : parentHeight)) / parentHeight * 100;
    }

    //Gets set and reset by enable/disable thumbnail fullscreen.
    var _visibilityBuffer = 1000;
    function _CheckVisible(parentHeight, myTop, myHeight) {
        //Early exit - we're not visible if our top is "higher" than 
        //the parent's height plus a buffer (so users don't see gray loading pages unless they go fast)
        if (myTop > (parentHeight + _visibilityBuffer))
            return false;

        //We're also not visible if our bottom (top + height + visibility buffer) is less than zero
        if ((myTop + myHeight + _visibilityBuffer) < 0)
            return false;

        //Otherwise, we're visible (maybe not literally, but close)
        return true;
    }

    function _getCachePositionForScroll(oLi) {
        // 7/1/2011 dd - Even position() and height() is slow. However the method of caching the data is not reliable on
        // IE7, especially when try to cache the position data of the last element.
        var top;
        if (oLi.position() != null)
            top = oLi.position().top;
        var height = oLi.data(DATA_CACHE_HEIGHT);

        return { top: top, height: height };
    }

    function debounce(interval, fxn) {
        return function() {
            var lastPerParam = {};
            var timeoutPerParam = {};
            return function(param1) {
                var current = +new Date();
                if ((current - lastPerParam[param1]) < interval) return;
                lastPerParam[param1] = current;

                clearTimeout(timeoutPerParam[param1]);
                timeoutPerParam[param1] = setTimeout(function() { fxn(param1); }, interval);
            }
        } ();
    }

    //This only gets set on resize.
    var _maxScroll;
    //This gets set when we initialize
    var _minScroll;
    var _trackPageScrolling = false;
    var _scrollImpl = debounce(50, function(selector) {
        var selectedIdxKey = _doScroll(selector);
        if (selectedIdxKey != null && !_disableScrollToSelectedPage) _onSelectedPageByIdxPageKey(selectedIdxKey);

        if (_trackPageScrolling && selector == _fullPanelSelector) {
            var currentScroll = $fullPanel.scrollTop();
            console_log("Current scroll is: " + currentScroll + ", max scroll is " + _maxScroll);
            if (currentScroll > _maxScroll || currentScroll <= _minScroll) {
                console_log("Waiting for a scroll past the end");
                _waitForScrollPastEnd($fullPanel, currentScroll);
            }
            else {
                _cleanScrollPastEndSpacers($fullPanel);
            }
        }
    });

    function _waitForScrollPastEnd($elem, currentScroll) {
        console_log("Waiting for scroll");

        if (_spacersCleaned) {
            window.setTimeout(function() {
                $elem.prepend('<li class="WaitForScrollSpacer Start">&nbsp;</li>').append('<li class="WaitForScrollSpacer End">&nbsp;</li>');
                $elem.scrollTop($elem.scrollTop() + _minScroll);
                _spacersCleaned = false;
            }, 500);
        }
        else {
            console_log("Has spacers, checking stuff");
            if (currentScroll == 0) {
                $(__pdfEditor).trigger(SCROLLED_BEFORE_FIRST_PAGE_EVT_TYPE);
                console_log("Triggered scroll past first");
            }
            else if (currentScroll > ($elem[0].scrollHeight - $elem.height())) {
                $(__pdfEditor).trigger(SCROLLED_PAST_LAST_PAGE_EVT_TYPE);
                console_log("Triggered scroll past last");
            }
        }
    }

    var _spacersCleaned = true;
    function _cleanScrollPastEndSpacers($elem) {
        if (!_spacersCleaned) {
            $('.WaitForScrollSpacer', $elem).remove('.WaitForScrollSpacer');
            $elem.scrollTop($elem.scrollTop() - _minScroll);
            _spacersCleaned = true;
        }
    }


    //Normally people scroll forward more than they scroll back,
    //so we'll check more pages in front than we do in back.
    //This also works better with the way the thumbnail pane scrolls.

    var _recentPages = {};
    var _numLoadedPgs = 0;

    //Gets set and reset in the enable/disable FullscreenThumbnail functions
    var _loadedPageWindowBefore = 4;
    var _loadedPageWindowAfter = 10;
    var _maxPageCache = 15;

    function _doScroll(selector) {
        if (!_recentPages[selector]) {
            _recentPages[selector] = [];
        }
        var timer = new Date();
        var parent = $(selector);
        var cache = _pageHeightCaches[selector];
        var parentHeight = __panelHeight;
        var parentScrollTop = parent.scrollTop();
        var children = parent.children();

        if (!parent.is(":visible")) {
            console_log(selector + ' is not visible.');
            return;
        }

        var lastChildPosition = _getCachePositionForScroll(children.last());
        var parentTotalScrollHeight = parentScrollTop + lastChildPosition.top;
        var i = _findLineIndex(cache.h, parentScrollTop);
        var selectedId = null;
        var startingIndex = cache.i[i];

        if (startingIndex < 0 || isNaN(startingIndex)) {
            startingIndex = 0;
        }

        var SeenAPage = false;
        var iterCount = 0;
        var idx = startingIndex;
        var lookAt = children.slice(startingIndex);
        var len = Math.min(_maxPageCache, lookAt.length);

        for (var i = 0; i < len; i++) {
            var oLi = $(lookAt[i]);
            var data = oLi.data() || {}; // 5/2/2011 dd - Cache data object for better performance.
            var pageId = data[IDX_PAGE_KEY];
            if (typeof (pageId) == "undefined") continue; //Sometimes if you scroll down while dragging this happens.

            // 5/2/2011 dd - $(this).position().top & $(this).height() are expensive operations. Therefore need to cache.

            var position = _getCachePositionForScroll(oLi);
            var top = position.top;
            var height = position.height;


            var isVisible = _CheckVisible(parentHeight, top, height);
            if (!isVisible) {
                //If we've made at least one page visible before, but this page is not visible
                //That means we've gone past the visibility window and need to stop
                //If we haven't made a page visible yet, and this page is not visible
                //That means we haven't reached the visibilty window and need to keep going
                if (SeenAPage) {
                    break;
                }
                else {
                    continue;
                }

                //We could use jQuery.filter to do this, but filter will look at the entire slice
            }

            SeenAPage = true;

            var currentPageOccupies = _PercentOfParentView(parentHeight, top, height);
            if (isVisible && currentPageOccupies > 50) {
                selectedId = pageId;
            }

            var elem = oLi[0];
            //Have we seen this element recently?
            //Note that we have to pull it out of the jQuery object for this equality to work...
            if ($.inArray(elem, _recentPages[selector]) > -1) {
                //Yes, so we don't need to do anything for it
                continue;
            }

            //Otherwise, we've never seen it so set things up to display it.
            //We have to go through this because most browsers don't like having to display
            //a lot of large PNGs on the same page.
            var img = oLi.find('.pdf-page > img');

            //Mark the page as being visible
            oLi.addClass('Visible');

            var src = srcFxns[selector](_findPageObjectById(pageId));

            img.parent().css('left', '0px'); // 5/17/2011 dd - Need this in order for IE7 to work properly. Otherwise when scroll far the thumbnail will not generate properly.

            if (data[DATA_PDF_PAGE_NEED_DROPPABLE_SETUP] == true) {
                //This is where all drag & drop things get set up - they're never initialized anywhere else
                _setupDroppableOnPage(oLi.find('.' + CSS_PAGE));
                data[DATA_PDF_PAGE_NEED_DROPPABLE_SETUP] = false;
            }

            // 5/19/2011 dd - Loop through all pdf-fields and call to setup interaction pdf field
            oLi.find('div.' + CSS_PDF_FIELD).each(function() { _SetupInteractionPdfField(this); });
            _applyImgRotation(img, img.data(DATA_IMG_ROTATE_CSS));
            img.attr('src', src);

            //Remember this one
            var recentCache = _recentPages[selector];
            var idxInCache = $.inArray(elem, recentCache)
            if (idxInCache != -1) recentCache.splice(idxInCache, 1);
            _recentPages[selector].push(elem);

            //We've probably got too many pages loaded, so start kicking some out (FIFO)
            //The while should only go through once, but hey you never know.
            while (_recentPages[selector].length > _maxPageCache) {
                var $li = $(_recentPages[selector].shift());
                $li.find('.pdf-page > img').attr('src', _settings.EmptyImgSrc);
                $li.removeClass('Visible');
            }
        }
        timer = (new Date()) - timer;

        console_log('Current Visible Page : ' + startingIndex + ' Current Scroll Top ' + parentScrollTop + ' Scroll impl in ' + timer + 'ms for ' + selector);

        return selectedId;

    }

    function _onFullPanelScroll() {
        _scrollImpl(_fullPanelSelector);
    }

    function _clearPagePositionCacheData() {
        $thumbPanel.data('__TOTAL_SCROLL_HEIGHT__', null);
        $fullPanel.data('__TOTAL_SCROLL_HEIGHT__', null);

        _recalculateAllHeights()
    }

    function _onThumbPanelScroll(evt) {
        _scrollImpl(_thumbPanelSelector);
    }

    function _resyncThumbPageNumber() {
        var timer = new Date();
        $thumbPanel
            .find("." + CSS_PAGE_NUMBER)
            .each(function(idx) {
                $(this).html(idx + 1);
            }); // Re-update thumbnail page number.
        timer = (new Date()) - timer;
        console_log('Resync ThumbPage Number in ' + timer);
    }

    function _gotoPage(pg) {
        if (!__isPdfEditorFinishLoading) return;
        if (pg < 0) {
            pg = 0;
        }
        else if (pg > _pageList.length) {
            pg = _pageList.length;
        }

        _onSelectedPage(_pageList[pg], true);
    }

    function _go(d) {
        if (!__isPdfEditorFinishLoading) return;
        var i = _currentPageIndex + d;
        if (i < 0 || i >= _pageList.length)
            return;
        _onSelectedPage(_pageList[i], true);
    }
    function _rotate(from, to, d) {
        _rotatePages($(_pageList).slice(from - 1, to), d);
        _onSelectedPage(_pageList[from - 1], true);
    }

    function _rotateSelected(d) {
        var toRotate = [];
        $('.ui-selected').each(function() {
            toRotate.push(_findPageObjectByLi(this));
        });

        _rotatePages(toRotate, d);
    }

    function _rotatePages(toRotate, d) {
        if (!_settings.AllowEdit()) return;
        if (!__isPdfEditorFinishLoading) return;

        var i = _normalizeAngle(d);

        $(toRotate).each(function() {
            _rotatePageImpl(this, i);
        });

        _disableTFSForPositionCalc();

        $.each(_getFields(), function() {
            _EnforceBoundaries(this);
        });

        _enableTFSForPositionCalc();

        _clearPagePositionCacheData();


        $(__pdfEditor).trigger(PAGE_MODIFIED_EVT_TYPE);
        _refreshPageImages();
    }

    function _rotatePageImpl(oPage, rotationDistance) {
        var currentRotation = oPage.Rotation;
        oPage.Rotation += rotationDistance;
        oPage.Rotation = _normalizeAngle(oPage.Rotation);

        if (typeof (oPage.ClientRotation) == "undefined" || oPage.ClientRotation == null) {
            oPage.ClientRotation = oPage.Rotation;
        } else {
            oPage.ClientRotation += rotationDistance;
            oPage.ClientRotation = _normalizeAngle(oPage.ClientRotation);
        }

        _applyRotation(oPage, currentRotation);
    }

    function _applyRotation(oPage, currentRotation) {
        var finalRotation = oPage.ClientRotation;

        var rotationDistance = _normalizeAngle(finalRotation - currentRotation);
        //We need to resize the page if it's now sideways (rotated by a multiple of 90 degrees)
        var bNeedResize = ((rotationDistance / 90) % 2 == 1);
        var $Li = _FindInFullPanel(oPage);
        _rotateInDom($Li, finalRotation, bNeedResize);

        if (_settings.ManageThumbnailPanel) {
            $Li = _FindInThumbPanel(oPage);
            _rotateInDom($Li, finalRotation, bNeedResize, true);
        }
    }

    function _rotateInDom($li, d, bNeedResize, bIsThumb) {

        var dimensions;
        if (bNeedResize) {
            dimensions = __swapDimension($li, bIsThumb);
        }
        else {
            dimensions = _getDimension($li);
        }

        var bSideways = ((d / 90) % 2 == 1);
        if (bSideways) {
            var t = dimensions.height;
            dimensions.height = dimensions.width;
            dimensions.width = t;
        }

        if (_thumbFullScreen && bIsThumb) {
            dimensions.height *= _thumbFullScreenScale;
            dimensions.width *= _thumbFullScreenScale;
        }

        var cssClass = _getRotationClass(d);
        var $Img = $li.find('img.page');
        $Img.data(DATA_IMG_ROTATE_CSS, cssClass);
        _applyImgRotation($Img, cssClass, dimensions, bSideways);
    }

    function _applyImgRotation($Img, cssRotate, dim, bSideways) {
        if (null == cssRotate || "" == cssRotate) {
            return; // No-op.
        }
        if (typeof (bSideways) != 'undefined') {
            $Img.toggleClass('rotated', bSideways);
        }

        if ($Img.hasClass(cssRotate) == false) {
            _removeAllRotations($Img);
            $Img.addClass(cssRotate);
            if (dim) $Img.width(dim.width).height(dim.height);
        }

        var width;
        var height;

        if (dim == undefined) {
            width = $Img.width;
            height = $Img.height;
        }
        else {
            width = dim.width;
            height = dim.height;
        }

        if ((bSideways || bSideways == undefined) && hasMsTransform) {
            if (width > height) {
                var distance = (width - height) / 2;
                $Img.css("top", distance + "px").css("left", "-" + distance + "px").css("position", "relative");
            }
            else {
                var distance = (height - width) / 2;
                $Img.css("top", "-" + distance + "px").css("left", distance + "px").css("position", "relative");
            }
        }
        else {
            $Img.css("top", 0 + "px").css("left", 0 + "px").css("position", "relative");
        }

    }


    function _findLineIndex(arr, v) {
        var l = 0, u = arr.length, m;

        while (l <= u) {
            m = (l + u) >> 1;
            if (v > arr[m]) {
                l = m + 1;
            }
            else if (v != arr[m]) {
                u = m - 1;
            }
            else { //match found.
                break;
            }
        }

        //this is a BST search which is meant to find exact values. 
        //So sometimes we have the following 
        // [.,.,.,.,300,600,.,.] and were looking for 328 
        // the BST search index when it quits will be 600
        // we need the one before it though. 
        if (m > 0 && m < arr.length && arr[m] > v) {
            m -= 1;
        }

        return m;
    }

    function _removeAllRotations($img) {
        $img.removeClass(CSS_IMG_ROTATE_0);
        $img.removeClass(CSS_IMG_ROTATE_90);
        $img.removeClass(CSS_IMG_ROTATE_180);
        $img.removeClass(CSS_IMG_ROTATE_270);
    }

    function _normalizeAngle(d) {
        var i = (d || 0) % 360;
        while (i < 0) i += 360;
        return i;
    }

    function _getRotationClass(d) {
        var i = _normalizeAngle(d);

        if (i == 0) {
            return CSS_IMG_ROTATE_0;
        }
        else if (i == 90) {
            return CSS_IMG_ROTATE_90;
        }
        else if (i == 180) {
            return CSS_IMG_ROTATE_180;

        } else if (i == 270) {
            return CSS_IMG_ROTATE_270;
        }
    }

    function __swapDimension(o, bIsThumb) {
        var ret = {};
        var w = o.data(DATA_CACHE_WIDTH);
        if (w == null || isNaN(w)) {
            w = o.width();
        }

        var h = o.data(DATA_CACHE_HEIGHT);
        if (h == null || isNaN(h)) {
            h = o.height();
        }

        var applyH = h;
        var applyW = w;

        if (bIsThumb && _thumbFullScreen) {
            applyH *= _thumbFullScreenScale;
            applyW *= _thumbFullScreenScale;
        }

        o.width(applyH).height(applyW);
        o.data(DATA_CACHE_WIDTH, h);
        o.data(DATA_CACHE_HEIGHT, w);



        ret.width = h;
        ret.height = w;

        return ret;
    }

    function _getDimension(o) {
        var ret = {};
        var w = o.data(DATA_CACHE_WIDTH);
        if (w == null || isNaN(w)) {
            w = o.width();
        }

        var h = o.data(DATA_CACHE_HEIGHT);
        if (h == null || isNaN(h)) {
            h = o.height();
        }
        ret.width = w;
        ret.height = h;

        return ret;
    }

    //A wrapper around Array.splice which also handles updating the display
    function _splicePages(list, insertAt, replaceCount) {
        if (!_settings.AllowEdit() && !_settings.AddPagesWhileDisabled) {
            console_log("Cannot manipulate pages because editor does not allow editing.");
            return;
        }
        if (!__isPdfEditorFinishLoading) return;
        if (insertAt < 0 || insertAt > _pageList.length) {
            return;
        }

        //Make sure we're not replacing a null count
        replaceCount = replaceCount || 0;

        _addUniqueId(list);

        //Using the native Javascript function Array.splice would be nice, but if we just do _pageList.splice(insertAt, replaceCount, list)
        //we'll get a funky page list that contains a page list within itself.
        //So, Function.apply() to the rescue! It takes an array of args, which is pretty much what we have.
        list.unshift(replaceCount);
        list.unshift(insertAt);

        //And then this is like a call to _pageList.splice(insertAt, replaceCount, list[0], list[1], .... list[list.length-1]);
        var deletions = _pageList.splice.apply(_pageList, list);
        _removeFromUI(deletions);

        //Shift those two args off, we don't need them any more but we do need list.
        list.shift();
        list.shift();

        //Stitch these new pages into the panels, if there are any 
        //note that we can only do this after they've been added to pagelist
        if (list.length > 0) {
            _loadPagePanel(list, insertAt);
        }

        $(__pdfEditor).trigger(PAGE_MODIFIED_EVT_TYPE);

        if (_pageList.length == 0) {
            $(__pdfEditor).trigger(PAGE_STATUS_CHANGED_EVT_TYPE, __pdfEditor);
            return;
        }

        if (_settings.GoToNewDocOnAddition) {
            if ((insertAt - 1) == _pageList.length) {
                _onSelectedPage(_pageList[_pageList.length - 1], true);
            } else {
                _onSelectedPage(_pageList[insertAt - 1], true);
            }
        }
        _syncLocalPageNumbers();
        _resyncThumbPageNumber();
        _resyncFields();
        _clearPagePositionCacheData();
        _scaleThumbs();

    }

    function _syncLocalPageNumbers() {
        var len = _pageList.length;
        for (var i = 0; i < len; i++) {
            _pageList[i].LocalPage = i + 1;
        }
    }

    function _getSelectedIndexes($context, $allItems) {
        //Pulled out of this .each for perf
        /*$currentlySelected.each(function(idx, elem) {
        selectedIndexes.push($(elem).closest('li').index());
        });*/
        $context = $context || $('.ui-selected');
        var len = $context.length;
        var selectedIndexes = [];
        for (var i = 0; i < len; i++) {
            var pageObj = _findPageObjectByLi($($context[i]).closest('li'));
            var idx = pageObj.LocalPage - 1;

            if ($allItems) {
                idx = $.inArray($context[i], $allItems);
            }
            selectedIndexes.push(idx);
        }
        selectedIndexes.sort(function(a, b) { return a - b; });

        return selectedIndexes;
    }

    function _setSelectedRanges(ranges) {
        $('.ui-selected').removeClass('ui-selected');
        var pageObjects = _getSelectedPages(ranges);
        $.each(pageObjects, function(idx, obj) {
            _FindInThumbPanel(obj).addClass('ui-selected');
        });
    }

    function _getSelectedRanges() {
        if (_trackNewSelection) {
            var oldSelection = _getSelectedIndexes($('.ui-selected').not('.new-selection'));
            var newSelection = _getSelectedIndexes($('.ui-selected.new-selection'));
            var oldRanges = _indexListToRanges(oldSelection);
            var newRanges = _indexListToRanges(newSelection);

            var ret = oldRanges.concat(newRanges);
            ret["old"] = oldRanges;
            ret["new"] = newRanges;

            return ret;
        }
        else {
            var selected = _getSelectedIndexes();
            return _indexListToRanges(selected);
        }
    }

    function _indexListToRanges(list) {
        //Need to convert it into a list of {start, count} tuples
        var ranges = [];
        var rangeStart = list[0];
        var rangePrev = list[0];
        var len = list.length;
        var acc = 1;
        for (var i = 1; i < len; i++) {
            var current = list[i];
            rangePrev++;

            if (rangePrev == current) {
                acc++;
                continue;
            }

            ranges.push({
                Start: rangeStart,
                Count: acc
            });

            acc = 1;
            rangeStart = current;
            rangePrev = current;
        }

        if (typeof (rangeStart) != "undefined") {
            ranges.push({
                Start: rangeStart,
                Count: acc
            });
        }

        return ranges;
    }

    function _$rangesAsThumbnailPages(ranges) {
        var pageListObjs = _rangesAsPageListSlices(ranges);
        var len = pageListObjs.length;
        var $first = pageListObjs[0];
        for (var i = 1; i < len; i++) {
            $first.add(_FindInThumbPanel(pageListObjs[i]));
        }

        return $first;
    }

    function _getSelectedPages(ranges) {
        return _rangesAsPageListSlices(ranges || _getSelectedRanges());
    }

    //Given a list of ranges, returns an array from _pageList that represents the pages in those ranges.
    function _rangesAsPageListSlices(ranges) {
        var ret = [];
        var len = ranges.length;

        for (var i = 0; i < len; i++) {
            var temp = _pageList.slice(ranges[i].Start, ranges[i].Start + ranges[i].Count);

            //This is actually significantly (~100x) faster than ret = ret.concat(temp).
            ret.push.apply(ret, temp);
        }

        return ret;
    }

    //Removes a list of page ranges from the page list. Note that this doesn't actually update the UI.
    function _removeRangesFromPageList(ranges) {
        //Make sure it's sorted ascending, it normally will be but we need to be sure.
        ranges = ranges.sort(function(a, b) {
            return a.Start - b.Start;
        });
        //And then go backwards through it, so we don't mess up indexes of things to come (len should still be the same)
        for (var i = ranges.length - 1; i >= 0; i--) {
            temp = _pageList.splice(ranges[i].Start, ranges[i].Count);
        }
    }

    //Moves a page. Note that this works off of page numbers.
    function _movePages(moveFrom, count, insertAt) {
        _moveRanges([{ Start: moveFrom - 1, Count: count}], insertAt);
    }

    //If we're moving things around, the destination index might shift too. This calculates what it will be.
    function _moveRangeNewIdx(ranges, destIdx) {
        if (ranges.length == 0) return destIdx;

        ranges = ranges.sort(function(a, b) {
            return a.Start - b.Start;
        });

        //We need to offset the destination index based on how many pages there will have been removed before it,
        //so things end up where people expect.
        var idxOffset = 0;

        $.each(ranges, function(idx, range) {
            if (range.Start >= destIdx) return false;

            idxOffset += range.Count;
        });

        return destIdx - idxOffset;
    }

    //Moves a list of page ranges to an index in the document.
    function _moveRanges(ranges, newPageIndex) {
        if (!_settings.AllowEdit()) {
            return;
        }
        if (!__isPdfEditorFinishLoading) return;

        newPageIndex = _moveRangeNewIdx(ranges, newPageIndex);
        var toMove = _rangesAsPageListSlices(ranges, true);
        _removeRangesFromPageList(ranges);

        var firstIndexKey = toMove[0][IDX_PAGE_KEY];

        var fieldsFromMovedPages = [];
        $.each(toMove, function(idx, val) {
            var fullPanelFields = _FindInFullPanel(val).find('.' + CSS_PDF_FIELD).clone(true).removeClass('saved');
            var thumbPanelFields = _FindInThumbPanel(val).find('.' + CSS_PDF_THUMB_FIELD).clone(true);
            fieldsFromMovedPages.push({
                ToFullPage: fullPanelFields,
                ToThumbPage: thumbPanelFields
            });
        });

        _removeFromUI(toMove);

        _splicePages(toMove, newPageIndex);
        var count = toMove.length;

        for (var i = 0; i < count; i++) {
            var pageObject = toMove[i];
            var FullPage = _FindInFullPanel(pageObject).find('.' + CSS_PAGE);
            var ThumbPage = _FindInThumbPanel(pageObject).find('.' + CSS_PAGE);
            var fields = fieldsFromMovedPages[i];

            fields.ToFullPage.appendTo(FullPage);
            fields.ToThumbPage.appendTo(ThumbPage);
        }

        //Need to resync the fields before we run the OnPdfFieldDrop events
        _resyncFields();

        //Each of the fields we saved is kind of a new field, so run the on pdf field drop events on each one.
        $.each(fieldsFromMovedPages, function(idxa, fields) {
            $.each(fields.ToFullPage, function(idx, val) {
                _settings.OnPdfFieldDrop(null, val);
            });
        });

        
        //We don't necessarily want to show the new fields though, so hide them if we're in thumb full screen mode.
        $('.pdf-field-thumbnail').toggle(!_thumbFullScreen);

        _onSelectedPageByIdxPageKey(firstIndexKey, true);
        $(__pdfEditor).trigger(PAGE_MOVED_EVT_TYPE);

        var classToAdd = _getClassForNewSelections();

        $.each(toMove, function(idx, pageObj) {
            _FindInThumbPanel(pageObj).addClass(classToAdd);
        });
    }

    function _deleteSelectedPages(ranges) {
        var ranges = ranges || _getSelectedRanges();
        var numDeletions = 0;
        for (var i = 0; i < ranges.length; i++) {
            var start = ranges[i].Start;
            var count = ranges[i].Count;
            _deletePages(start - numDeletions, count);
            numDeletions += count;
        }
    }

    function _deletePages(pgStart, numOfPagesDelete) {
        _splicePages([], pgStart, numOfPagesDelete);
    }

    function _removeFromUI(toRemove) {
        $(toRemove).each(function() {
            _SelectPages(this).remove();
        });
    }

    function _getClassForNewSelections() {
        var ret = "ui-selected";
        if (_trackNewSelection) ret += " new-selection";

        return ret;
    }

    //updates the fields page number to match the pagelist order
    function _resyncFields() {
        _disableTFSForPositionCalc();

        //Grab all of the <li>s that contain main pages
        $('#' + FULL_PANEL_ID + ' li').each(function(i, o) {
            var uniqueId = $(o).data(IDX_PAGE_KEY);
            var newPage;
            var length = _pageList.length;
            var temp_pageList = _pageList;
            for (var index = 0; index < length; index++) {
                if (uniqueId === _pageList[index][IDX_PAGE_KEY]) {
                    newPage = index + 1;
                    break;
                }
            }
            $('.' + CSS_PDF_FIELD, this).each(function(j, p) {
                $(p).data(DATA_PDF_FIELD).pg = newPage;

                var thumbData = $(p).data(DATA_PDF_THUMB_DATA); //needed for ie7 or thumbs are lost on drag drop of page
                if (thumbData && thumbData.Thumb) {
                    var jsThumbCoord = _convertToScaledJsCoord(p, _thumbnailScale);
                    thumbData.Thumb.css(jsThumbCoord);
                }
            });
        });

        _enableTFSForPositionCalc();


    }

    var _defaultOptions = {
        PanelMargin: 5,
        PageBottomPadding: 80,
        AllowEdit: function () { return true; },
        SignatureFieldImgSrc: null,
        CheckboxFieldImgSrc: null,
        InitialFieldImgSrc: null,
        GenerateFullPageSrc: function(o) { return o; },
        OnPageStatusChanged: function(editor) { },
        OnRenderPdfFieldContent: function(o) { return null; },
        OnInitialdropRenderField: function() { },
        OnError: function(msg) { },
        OnPageModified: function() { },
        OnPdfFieldDrop: function(evt, o) { },
        OnPdfFieldSelectedStop: function(evt, count) { },
        CanEditAnnotations: false,
        EmptyImgSrc: '',
        OnPageMoved: function() { },
        AddPagesWhileDisabled: false,
        GoToNewDocOnAddition: true,
        OnPdfFieldDrag: function() { },
        ManageThumbnailPanel: true,
        LeftMargin: 0,
        OnScrollPastLastPage: function() { },
        OnScrollBeforeFirstPage: function() { },
        OnPageSelectionStop: function (evt, selectedIndexes) { },
        OverrideResizeForSignatureFields: false
    };

    var _settings = $.extend({}, _defaultOptions, options);
    if (!_settings.GenerateThumbPageSrc) _settings.GenerateThumbPageSrc = _settings.GenerateFullPageSrc;

    var srcFxns = {};
    srcFxns[_fullPanelSelector] = _settings.GenerateFullPageSrc;
    srcFxns[_thumbPanelSelector] = _settings.GenerateThumbPageSrc;


    var _windowDimensions;
    var __panelHeight = 0;

    function _resizeEditor(bManual) {
        var doc = $(window).dimension();
        var showThumbnails = _settings.ManageThumbnailPanel;
        var editor = $(_editorSelector);
        doc.width = doc.width - _settings.LeftMargin;
        console_log("Doc width: " + doc.width);
        var content = $("#content");
        var panelHeight;
        if (content[0] === undefined)
            panelHeight = doc.height - editor.position().top - _settings.PageBottomPadding;
        else
            panelHeight = doc.height - content.offset().top - _settings.PageBottomPadding;
        if (panelHeight < 50) {
            panelHeight = 50; // Set minimum height;
        }

        var thumbPanelWidth = thumbPanelWidth = doc.width * .15;

        if (thumbPanelWidth < 70) {
            thumbPanelWidth = 70; // Minimum width of thumbnail panel
        } else if (thumbPanelWidth > 300) {
            thumbPanelWidth = 300;
        }

        var factor = 3;
        if (!_thumbIsVisible) {
            thumbPanelWidth = 0;
            factor = 2;
        }

        var fullPanelWidth = doc.width - thumbPanelWidth - (factor * _settings.PanelMargin) - 20;
        if (fullPanelWidth < 100) {
            fullPanelWidth = 100;
        }

        $(_editorSelector).height(panelHeight + (2 * _settings.PanelMargin)).width(doc.width);
        console_log('Resize editor. ' + doc.width + 'x' + doc.height + ' panel Height:' + panelHeight);
        $fullPanel.width(fullPanelWidth).height(panelHeight);

        $thumbPanel.css({ width: '' });
        $thumbPanel.height(panelHeight);
        if (!_thumbFullScreen && _thumbIsVisible) $thumbPanel.width(thumbPanelWidth);
        __panelHeight = panelHeight;

        //This is an estimate, but shouldn't be off by more than about 20 px.
        _cleanScrollPastEndSpacers($fullPanel);
        _maxScroll = $fullPanel[0].scrollHeight - $fullPanel.height();
        _windowDimensions = doc;

        _recalculateAllHeights();

        _refreshPageImages();
    }

    function bindEvent(element, type, handler) {
            addEventHandler(element, type, handler, false);
    }

    var _pageSelectionPinned = false;
    var _trackNewSelection = false;
    function _setupPanels() {

        $(window).resize(_resizeEditor);
        var editor = $(_editorSelector), manageThumbnailPanel = _settings.ManageThumbnailPanel;
        editor.append(
                            $("<ol>",
                            { 'id': FULL_PANEL_ID, 'class': PANEL_CLASS }
                            ).css('margin', _settings.PanelMargin + 'px'));

        var obj = $("<ol>",
                            { 'id': THUMB_PANEL_ID, 'class': PANEL_CLASS + " ThumbPanelParent" }
                            ).css('margin', _settings.PanelMargin + 'px');

        if (manageThumbnailPanel) {
            editor.append(obj);
        }
        else {
            editor.prepend(obj);
        }

        $fullPanel = $(_fullPanelSelector);
        $thumbPanel = $(_thumbPanelSelector);

        $('#debugConsole').dialog({ autoOpen: false });
        _resizeEditor();


        //It would be nice to use jQuery.scroll, but that tries to fix up the event itself every single time we call it, 
        //which takes a surprising amount of time. Instead, rely on the native scroll method.
        bindEvent($fullPanel[0], 'scroll', _onFullPanelScroll);
        if (manageThumbnailPanel) {
            bindEvent($thumbPanel[0], 'scroll', _onThumbPanelScroll);
        }
        $thumbPanel.on('click', '.' + CSS_PAGE, function() { if (_thumbFullScreen) return; _onSelectedPageByIdxPageKey($(this).closest('li').data(IDX_PAGE_KEY), true); });
        $thumbPanel.on('click', '.' + CSS_PAGE + ' div.Magnify', function() { _magnifyThumbnail($(this).closest('li').data(IDX_PAGE_KEY)); });
        $thumbPanel.on('click', '.' + CSS_PAGE, function(e) {
            if (!_thumbFullScreen) return;
            if (_isInteractionDisabled()) return;

            if (!_pageSelectionPinned && !(e.ctrlKey || e.shiftKey)) $('.ui-selected', _thumbPanelSelector).removeClass('ui-selected');
            var $current = $(this).closest('li');

            var filter = '.ui-selected';
            if (_trackNewSelection) filter += ".new-selection";

            var classToAdd = _getClassForNewSelections();

            if (!_trackNewSelection && e.ctrlKey) {
                $current.toggleClass('ui-selected');
                //                if (_trackNewSelection) $current.toggleClass('new-selection', $current.is('.ui-selected'));

                $(__pdfEditor).trigger(PAGE_SELECTION_STOP_EVT_TYPE, [_getSelectedIndexes()]);
                return;
            }

            if (_trackNewSelection && $current.is(filter)) {
                $('.new-selection').removeClass('ui-selected new-selection');
                $('.old-selection').addClass('ui-selected');
                return;
            }

            $current.addClass(classToAdd);

            if (e.shiftKey || _trackNewSelection) {
                //If all .ui-selected are visible, restrict our search to .visible for performance
                if ($('.ui-selected').filter(':not(.Visible)').length == 0) {
                    _fillSelectionGaps($('.Visible' + filter, _thumbPanelSelector), $('.Visible', _thumbPanelSelector), classToAdd);
                }
                else {
                    _fillSelectionGaps($(filter, _thumbPanelSelector), $('li', _thumbPanelSelector), classToAdd);
                }
            }

            $(__pdfEditor).trigger(PAGE_SELECTION_STOP_EVT_TYPE, [_getSelectedIndexes()]);
        });
        $(_editorSelector).on('click', 'div.CloseButton', function() { $('#EdocEditorMagnifyDiv').hide(); _enableInteraction(); });


        if (_settings.AllowEdit() || _settings.CanEditAnnotations) {
            $fullPanel.on('click', '.pdf-pdf-field', function(e) {
                var $elem = $(this);

                if (!$elem.is(_selectableFilter)) return;

                var id = $elem.data(DATA_PDF_FIELD_ID);

                if (e.ctrlKey) {
                    if ($.inArray(id, g_selectedPdfFields) == -1) g_selectedPdfFields.push(id);
                }
                else {
                    $('.pdf-pdf-field.ui-selected').removeClass('ui-selected');
                    g_selectedPdfFields = [id];
                }

                $('.pdf-first-selected').removeClass('pdf-first-selected');
                $elem.addClass('pdf-first-selected ui-selected');
                $(__pdfEditor).trigger(PDF_FIELD_SELECTED_STOP_EVT_TYPE, [g_selectedPdfFields.length]);
            });

            $fullPanel.on("mousedown", function(e) {

                //This prevents clicks in the far-right region (where the scrollbar is) from triggering the selectable,
                //since we don't want people to lose their current selections if they scroll with ot.
                if (!_windowDimensions) return;
                if (e.pageX >= $fullPanel.offset().left + $fullPanel[0].clientWidth + $fullPanel[0].clientLeft
                || e.pageY >= $fullPanel.offset().top + $fullPanel[0].clientHeight + $fullPanel[0].clientTop) {
                    e.stopImmediatePropagation();
                    return false;
                }
            }).selectable(
              {
                  filter: _selectableFilter,
                  cancel: '.disable-selectable,textarea',
                  selected: function(evt, ui) {
                      var id = $(ui.selected).data(DATA_PDF_FIELD_ID);
                      if ($.inArray(id, g_selectedPdfFields) === -1) {
                          g_selectedPdfFields.push(id);
                      }
                  },
                  unselected: function(evt, ui) {
                      var id = $(ui.unselected).removeClass('pdf-first-selected').data(DATA_PDF_FIELD_ID);
                      $.each(g_selectedPdfFields, function(idx, o) { if (o === id) { g_selectedPdfFields.splice(idx, 1); return false; } });
                  },
                  stop: function(evt, ui) {
                      $('.pdf-first-selected').removeClass('pdf-first-selected');
                      $(_getSelectedPdfFields()[0]).addClass('pdf-first-selected');
                      $(__pdfEditor).trigger(PDF_FIELD_SELECTED_STOP_EVT_TYPE, [g_selectedPdfFields.length]);
                  }
              });
        }

        if (_settings.ManageThumbnailPanel) {
            var selectedRanges, originalWidth, originalHeight;
            $thumbPanel.sortable(
                {
                    placeholder: 'placeholder',
                    container: 'parent',
                    distance: 5,
                    tolerance: 'pointer',
                    handle: 'img',
                    cursorAt: { top: 5, left: 30 },
                    appendTo: $thumbPanel.parent(),
                    helper: function (e, ui) {

                        //Save these for the placeholder to use once we start sorting
                        originalHeight = $(ui).height();
                        originalWidth = $(ui).width();

                        if (!$(ui).is('.ui-selected')) {
                            $('.ui-selected').removeClass('ui-selected');
                            $(ui).addClass('ui-selected');
                        }

                        var $selected = $('.ui-selected');

                        //This shouldn't happen, but here's a fallback if it does
                        if ($selected.length == 0) return ui;

                        selectedRanges = _getSelectedRanges();
                        var $helper = $('<div class="ui-sortable-helper" ></div>');
                        var totalStackHeight = 40;
                        var minHeight = 10;

                        var stackHeight = Math.max(totalStackHeight / $selected.length, minHeight);

                        $selected.each(function (idx, elem) {
                            var $source;
                            if (ui[0].id == elem.id) {
                                //We need to leave the original ui element alone, so clone it instead
                                $source = $(elem).clone();
                            }
                            else {
                                $source = $(elem);
                            }

                            var $toAppend = $('<div>').attr('style', $source.attr('style')).append($source.children());

                            //$toAppend isn't in the DOM and has no height, so we use the source's for reference.
                            var sourceHeight = $source.height();
                            var margin = stackHeight - sourceHeight;
                            if ((margin + sourceHeight) <= 0) margin = -minHeight;

                            $source.remove();

                            $toAppend.css({ 'margin-bottom': margin + 'px' });

                            $helper.append($toAppend);
                        });

                        //Create a callback in our context that other components can use to change what happens on sort.
                        $helper.data("sort-callback", function (result) {
                            //We only want to run this once, because doing it multiple times might mess things up
                            $helper.data("sort-callback", null);

                            if (result == 'cancel') {
                                _moveRanges(selectedRanges, selectedRanges[0].Start);
                            }
                            else if (result == 'clear') {
                                _deleteSelectedPages(selectedRanges);
                            }
                        });

                        return $helper;
                    },

                    start: function (event, ui) {
                        //The ui element's original width and height are saved off when we create the helper
                        ui.placeholder.width(originalWidth).height(originalHeight);
                        startIndex = $(ui.item).index();
                        if (!selectedRanges) selectedRanges = { Start: startIndex + 1, Count: 1 };

                        ui.helper.data('selected-ranges', selectedRanges);
                    },

                    beforeStop: function (event, ui) {
                        var skipDefault = ui.helper.data('sort-skip-default');

                        if (skipDefault) {
                            return;
                        }

                        var prev = ui.item.prev();
                        //If there's no previous item we're inserting at 0
                        var endPage = 0;
                        //Otherwise use the previous page's local page number.
                        if (prev.length) endPage = _findPageObjectByLi(prev).LocalPage;

                        _moveRanges(selectedRanges, endPage);
                    }
            });

            $thumbPanel.on("mousedown", function (e) {
                //Kind of a hack - setting metaKey = true keeps Selectable from de-selecting stuff on a click in a blank area,
                //which is the behavior we want when selection is pinned
                if (_pageSelectionPinned)
                    e.metaKey = true;

                //This prevents clicks in the far-right region (where the scrollbar is) from triggering the selectable,
                //since we don't want people to lose their current selections if they scroll with ot.
                if (!_windowDimensions) return;
                if (e.pageX >= $thumbPanel.offset().left + $thumbPanel[0].clientWidth + $thumbPanel[0].clientLeft) {
                    e.stopImmediatePropagation();
                    return false;
                }
            }).selectable({
                cancel: '.pdf-page, .scrollbar-area',
                appendTo: _thumbPanelSelector,
                filter: "li.Visible",
                selecting: function (event, ui) {
                    $('.ui-selected').not(".Visible").removeClass('ui-selected');
                    $(ui.selecting).removeClass('AutoSelected').addClass('new-selection');
                    _fillSelectionGaps($('.Visible.ui-selecting', _thumbPanelSelector), $('.Visible', _thumbPanelSelector));
                    $(__pdfEditor).trigger(PAGE_SELECTION_STOP_EVT_TYPE, [_getSelectedIndexes($('.ui-selecting')), _getSelectedIndexes($('.new-selection'))]);
                },
                unselecting: function (event, ui) {
                    $(ui.unselecting).removeClass('new-selection');
                    _removeSelectionGaps($('.Visible.ui-selecting', _thumbPanelSelector), $('.Visible', _thumbPanelSelector));
                    $(__pdfEditor).trigger(PAGE_SELECTION_STOP_EVT_TYPE, [_getSelectedIndexes($('.ui-selecting')), _getSelectedIndexes($('.new-selection'))]);
                },
                stop: function () {
                    $(__pdfEditor).trigger(PAGE_SELECTION_STOP_EVT_TYPE, [_getSelectedIndexes(), _getSelectedIndexes($('.new-selection'))]);

                    if (!_trackNewSelection) {
                        _clearNewSelection();
                    }
                }
            });
        }
    }

    function _markCurrentlySelected() {
        $('.ui-selected').addClass('old-selection');
    }

    function _clearNewSelection() {
        $('.new-selection').removeClass('new-selection');
        _markCurrentlySelected();
    }

    function _magnifyThumbnail(pageKey) {
        var page = _pagesByID[pageKey];
        if (!page) {
            console_log("Invalid page id chosen to magnify: " + pageKey);
            return;
        }

        var $popup = $('#EdocEditorMagnifyDiv');

        if (!$popup.is(':visible')) _disableInteraction();

        var $editor = $(_editorSelector);
        if ($popup.length == 0) {
            $("<div id='EdocEditorMagnifyDiv'><div class='CloseButton'></div><img /></div>").appendTo($editor);
            $popup = $('#EdocEditorMagnifyDiv');
            //If we only show the popup once the image's loaded, there won't be any flickering.
            $popup.find('img').load(function() {
                $popup.show();
            }).error(function() {
                alert("Error loading page preview");
            });
        }

        var editorMargin = 20;
        var pageWidth = page.PageWidth;
        var pageHeight = page.PageHeight;
        var rotation = _normalizeAngle(page.Rotation);

        //If the page has been rotated on the client we should reflect that.
        //Since all pages are rectangles and all rotations are in 90 degree increments,
        //all this means is that we need to swap width and height if the page has been rotated by a multiple of 90.
        var isRotated90Degrees = ((rotation / 90) % 2) == 1;
        if (isRotated90Degrees) {
            var temp = pageWidth;
            pageWidth = pageHeight;
            pageHeight = temp;
        }

        var widthScale = ($editor.width() - editorMargin) / pageWidth;
        var heightScale = ($editor.height() - editorMargin) / pageHeight;

        var scaleFactor = Math.min(widthScale, heightScale);

        var popupWidth = pageWidth * scaleFactor;
        var popupHeight = pageHeight * scaleFactor
        var popupLeft = $editor.width() / 2 - popupWidth / 2;
        var thumbPanelEditorOffset = ($editor.height() - $thumbPanel.height())
        var popupTop = $editor.height() / 2 - popupHeight / 2 - thumbPanelEditorOffset / 2;


        $popup.css({ "left": popupLeft, "top": popupTop, 'width': popupWidth + "px", 'height': popupHeight + "px" });

        var $img = $popup.find('img');
        $img[0].src = _settings.GenerateFullPageSrc(page);
        var rotationCss = _getRotationClass(rotation);
        _removeAllRotations($img);
        $img.addClass(rotationCss);

        if (isRotated90Degrees) {
            //Since the image is rotated, we need to swap width and height for it.
            $img.height(popupWidth).width(popupHeight);

            if (hasMsTransform) {
                //also, for ie9+, it has to be centered in the div
                var distance = (popupHeight - popupWidth) / 2;
                $img.css("left", -distance).css("top", distance).css("position", "relative");
            }

        }
        else {
            $img.width(popupWidth).height(popupHeight);
            $img.css("left", 0).css("top", 0);
        }
        //Note that actually showing the popup is done by by the image's load handler.
    }

    var _waitingPanelId = '__WAITING';
    function _displayWaiting() {
        return;
        var top = $(document).height() / 2 - 50;
        var left = $(document).width() / 2 - 150;
        $(_editorSelector).append($('<div>', { 'id': _waitingPanelId }).text('Loading ...').addClass(CSS_LOADING_PANEL).css({ 'top': top + 'px', 'left': left + 'px' }));
    }
    function _hideWaiting() {
        $('#' + _waitingPanelId).remove();
    }
    function __init(o) {
        $(o).on(PAGE_STATUS_CHANGED_EVT_TYPE, _settings.OnPageStatusChanged)
        .on(ERROR_EVT_TYPE, _settings.OnError)
        .on(PAGE_MODIFIED_EVT_TYPE, _settings.OnPageModified)
        .on(PDF_FIELD_DROP_EVT_TYPE, _settings.OnPdfFieldDrop)
        .on(PDF_FIELD_SELECTED_STOP_EVT_TYPE, _settings.OnPdfFieldSelectedStop)
        .on(PAGE_MOVED_EVT_TYPE, _settings.OnPageMoved)
        .on(PDF_FIELD_DRAG_EVT_TYPE, _settings.OnPdfFieldDrag)
        .on(SCROLLED_PAST_LAST_PAGE_EVT_TYPE, _settings.OnScrollPastLastPage)
        .on(SCROLLED_BEFORE_FIRST_PAGE_EVT_TYPE, _settings.OnScrollBeforeFirstPage)
        .on(PAGE_SELECTION_STOP_EVT_TYPE, _settings.OnPageSelectionStop);


        $(document).keydown(function(evt) {
            if (evt.ctrlKey && evt.altKey && evt.which == 77) {
                display_console_log();
            }
        });

    }
    function _addUniqueId(list) {
        // 1/20/2010 dd - Append Unique Id to each page object
        // And put it on the list of pages by Id
        // Note that we can't just push, because pages aren't the only thing with a global unique id
        // However, they do make up the majority of things with one.
        $.each(list, function() {
            this[IDX_PAGE_KEY] = _globalUniqueIdCount;
            _pagesByID[_globalUniqueIdCount] = this;
            _globalUniqueIdCount++;
        });
    }

    function _scaleThumbs(amt, reset) {
        //Just do the right thing if we're called with no parameters.
        if (!amt && !reset) {
            amt = _thumbFullScreenScale;
            reset = !_thumbFullScreen;
        }

        var $context = $('.pdf-page-parent', _thumbPanelSelector);

        if (reset || amt == 1) {
            amt = 1;
            $context.removeClass('Rescaled');
        }
        else {
            $context = $context.not('Rescaled').addClass('Rescaled');
        }

        $context.each(function(idx, elem) {
            var data = $(elem).data();
            var h = data[DATA_CACHE_HEIGHT];
            var w = data[DATA_CACHE_WIDTH];

            $(elem).width(w * amt).height(h * amt);
            $('.' + CSS_IMG_ROTATE_0 + ', .' + CSS_IMG_ROTATE_180, elem).width(w * amt).height(h * amt);
        });

        $('.rotated', _thumbPanelSelector).each(function(idx, img) {
            var data = $(img).closest('li').data();
            var h = data[DATA_CACHE_HEIGHT];
            var w = data[DATA_CACHE_WIDTH];
            var nh = h * amt;
            var nw = w * amt;
            var distance = Math.abs(nh - nw) / 2;
            var cssOptions = {width: nh, height: nw };

            if (nh < nw) {
                cssOptions.top = -1 * distance;
                cssOptions.left = distance
            }
            else {
                cssOptions.top = distance;
                cssOptions.left = -1 * distance;
            }
            
            $(img).css(cssOptions);
        });
    }

    var _wasTFS = null;
    var _wasTFSCount = 0;
    function _disableTFSForPositionCalc() {
        if (_wasTFSCount == 0) {
            _wasTFS = _thumbFullScreen;
            if (_wasTFS == true) _disableThumbnailFullScreen();
        }

        _wasTFSCount++;
    }

    function _enableTFSForPositionCalc() {
        _wasTFSCount--;

        if (_wasTFSCount <= 0) {
            if (_wasTFS == true) _enableThumbnailFullScreen();

            _wasTFSCount = 0;
            _wasTFS = null;
        }
    }

    var _thumbFullScreen = false;
    var _thumbIsVisible = true;
    var _thumbFullScreenScale = 2.08;
    var _disableScrollToSelectedPage = false;
    function _enableThumbnailFullScreen() {
        _cleanScrollPastEndSpacers($fullPanel);
        if (!_thumbIsVisible) $thumbPanel.show();
        $(_fullPanelSelector + ", " + _thumbPanelSelector).addClass('ThumbFullScreen');
        _thumbFullScreen = true;
        _resizeEditor();
        _recalculateHeights(_thumbPanelSelector);

        _loadedPageWindowBefore = 10;
        _loadedPageWindowAfter = 30;
        _maxPageCache = 60;
        _visibilityBuffer = 200;
        _disableScrollToSelectedPage = true;

        if (_settings.AllowEdit()) {
            $thumbPanel.selectable("option", "disabled", false);
        }

        _scaleThumbs(_thumbFullScreenScale);

        _scrollImpl(_thumbPanelSelector);
    }

    function _disableThumbnailFullScreen() {
        if (!_thumbIsVisible) $thumbPanel.hide();
        $(_fullPanelSelector + ", " + _thumbPanelSelector).removeClass('ThumbFullScreen');
        _thumbFullScreen = false;
        _resizeEditor();
        _recalculateHeights(_thumbPanelSelector);

        _loadedPageWindowBefore = 4;
        _loadedPageWindowAfter = 10;
        _maxPageCache = 15;
        _visibilityBuffer = 1000;
        _disableScrollToSelectedPage = false;

        if (_settings.AllowEdit()) {
            $thumbPanel.sortable("option", "disabled", false);
            $thumbPanel.selectable("option", "disabled", true);
            //This gets added automatically but we don't want it.
            $thumbPanel.removeClass('ui-state-disabled');
        }

        _scaleThumbs(1, true);

        _scrollImpl(_thumbPanelSelector);
    }

    var _thumbPanelWasSortable = false;
    var _thumbPanelWasSelectable = false;
    var _interactionDisabledCount = 0;

    function _isInteractionDisabled() {
        return _interactionDisabledCount > 0;
    }

    function _disableInteraction(hideDisabledClass) {
        if (_isInteractionDisabled()) {
            return;
        }

        _interactionDisabledCount++;

        _thumbPanelWasSortable = $thumbPanel.sortable("option", "disabled");
        _thumbPanelWasSelectable = $thumbPanel.selectable("option", "disabled");

        $thumbPanel.sortable("option", "disabled", true);
        $thumbPanel.selectable("option", "disabled", true);

        if (!!hideDisabledClass) {
            $thumbPanel.removeClass('ui-state-disabled');
        }

        _interactionDisabled = true;
    }

    function _enableInteraction() {
        _interactionDisabledCount--;
        if (_isInteractionDisabled()) return;
        _interactionDisabledCount = 0;

        $thumbPanel.sortable("option", "disabled", _thumbPanelWasSortable);
        $thumbPanel.selectable("option", "disabled", _thumbPanelWasSelectable);
        //This gets added automatically to grey things out if something is disabled, but we never want it.
        $thumbPanel.removeClass('ui-state-disabled');


        _interactionDisabled = false;
    }

    function _fillSelectionGaps($currentlySelected, $allItems, classToAdd) {
        classToAdd = classToAdd || 'ui-selecting AutoSelected new-selection';

        var selectedIndexes = _getSelectedIndexes($currentlySelected, $allItems);

        var fillIn = [];
        var prev = selectedIndexes[0];

        for (var i = 1; i < selectedIndexes.length; i++) {
            var current = selectedIndexes[i];
            prev++;
            if (prev == current) continue;

            fillIn.push({
                start: prev,
                end: current
            });

            prev = current;
        }

        for (var i = 0; i < fillIn.length; i++) {
            var span = fillIn[i];
            var $slice = $allItems.slice(span.start, span.end);
            $slice.addClass(classToAdd);
        }
    }

    function _removeSelectionGaps($currentlySelected, $allItems) {
        $('.AutoSelected').removeClass('ui-selecting AutoSelected new-selection');

        _fillSelectionGaps($('.Visible.ui-selecting', _thumbPanelSelector), $allItems);
    }

    var _refreshPageImages = debounce(200, function() {
        //Wait for the UI to settle down some and then fire a scroll event, which will refresh whether or not images are visible.
        if (_settings.ManageThumbnailPanel) _onThumbPanelScroll();
        _onFullPanelScroll();
    });


    // Public functions

    var __pdfEditor = {
        IsInEditorViewport: _isInEditorViewport,
        GetPages: function() { return _pageList; },
        GetPdfFields: function() { return _getPdfFields(); },
        GetFields: _getFields,
        GetTotalPages: function() { return _pageList.length || 0; },
        GetCurrentPageNumber: function() { return (this.GetTotalPages() > 0) ? _currentPageIndex + 1 : 0; },
        GetSelectedRanges: _getSelectedRanges,
        SetSelectedRanges: _setSelectedRanges,
        GetSelectedPages: _getSelectedPages,
        DeleteSelectedPages: _deleteSelectedPages,
        SetCurrentPageNumber: function(pg) { },
        InsertPages: function(list, insertAt, removeCount) { return _splicePages(list, insertAt, removeCount); },
        DeletePages: function(pgStart, numOfPagesDelete) { _deletePages(pgStart, numOfPagesDelete); },
        BatchRotate: function(pgStart, pgTo, degree) { _rotate(pgStart, pgTo, degree); },
        MoveNextPage: function() { _go(1); },
        MovePrevPage: function() { _go(-1); },
        MoveToPage: function(pgnum) { _gotoPage(pgnum); },
        MovePages: function(moveFrom, count, insertAt) { return _movePages(moveFrom, count, insertAt); },
        RotateCurrentPage: function(degree) {
            var current = this.GetCurrentPageNumber();
            this.BatchRotate(current, current, degree);
        },
        RotateSelectedPages: function(degree) {
            degree = _normalizeAngle(degree);
            _rotateSelected(degree);
        },
        EnforceBoundaries: function(attachedPdfFieldElement) { _EnforceBoundaries(attachedPdfFieldElement); },
        AddPdfField: function(o, bAddToThumb) {
            var result;

            _disableTFSForPositionCalc();

            result = _addPdfField(o, bAddToThumb);

            _enableTFSForPositionCalc();

            return result;
        },
        RemovePdfField: function(o) { return _removePdfField(o); },
        GetSelectedPdfFields: function () { return _getSelectedPdfFields(); },
        ResetSelectedPdfFields: function() { g_selectedPdfFields = []; },
        AddDynamicThumbnail: function(pdfField, thumbnailContent) { _addDynamicThumbnail(pdfField, thumbnailContent); },
        EnableFullscreenThumbnail: _enableThumbnailFullScreen,
        DisableFullscreenThumbnail: _disableThumbnailFullScreen,
        EnableInteraction: _enableInteraction,
        DisableInteraction: _disableInteraction,
        Initialize: function(onCompletedHandle) {
            _isInitialized = false;
            _displayWaiting();
            _addUniqueId(_pageList);
            _setupPanels();

            //iOPM: 473935
            //non-IE browsers and Edge should be the same with IE9+
            hasMsTransform = ($fullPanel[0].style.msTransform != undefined || getBrowserVersion() > 9);

            if (hasMsTransform) {
                CSS_IMG_ROTATE_0 = "img-rotate-0-msTransform";
                CSS_IMG_ROTATE_90 = "img-rotate-90-msTransform";
                CSS_IMG_ROTATE_180 = "img-rotate-180-msTransform";
                CSS_IMG_ROTATE_270 = "img-rotate-270-msTransform";
            }

            _loadPagePanel(_pageList, 0, onCompletedHandle);



            _minScroll = GetSpacerHeight();

            _isInitialized = true;

            _refreshPageImages()
        },
        ToggleThumbnailPanel: function(show) {
            show = show || !$thumbPanel.is(':visible');

            if (_thumbIsVisible == show) {
                return;
            }
            _thumbIsVisible = show;
            $thumbPanel.toggle(show);
            _resizeEditor(true);
        },
        Refresh: function (pageList, onCompleteHandle) {
            // OPM 451519 - Set the previous index to null since we're now switching to a new doc.  
            // This will allow _onSelectedPageByIdxPageKey to complete itself.
            _previousPageIndex = null;

            _displayWaiting();
            _pageList = pageList;
            $fullPanel.empty();
            $thumbPanel.empty();
            _spacersCleaned = true;
            _addUniqueId(_pageList);
            _loadPagePanel(_pageList, 0, onCompleteHandle);
            if (_thumbFullScreen) _enableThumbnailFullScreen();

            _refreshPageImages();
        },

        GetThumbPanelSelector: function() {
            return _thumbPanelSelector;
        },

        SetLeftMargin: function(newMargin) {
            _settings.LeftMargin = newMargin;
            _resizeEditor()
        },

        BindEvent: function(eventName, callback) {
            $(__pdfEditor).on(eventName, callback);
        },

        TrackPageScrolling: function(track) {
            _trackPageScrolling = track || !_trackPageScrolling;
        },

        PinPageSelection: function(pin) {
            _pageSelectionPinned = pin;
        },

        TrackNewSelections: function(track) {
            _pageSelectionPinned = track;
            _trackNewSelection = track;

            $thumbPanel.toggleClass("TrackNew", track);
            $thumbPanel.sortable("option", "disabled", track);

            if (!track) {
                _clearNewSelection();
                $('.old-selection').removeClass('old-selection');
            }
            else {
                _markCurrentlySelected();
            }
        },

        ClearNewSelections: _clearNewSelection,

        HighlightRange: function(range) {
            $('#PdfEditorThumbPanel li.highlight').removeClass('.highlight');
            if (!range) return;
            _$rangesAsThumbnailPages([range]).addClass('highlight');
        }

    };

    function GetSpacerHeight() {
        var temp = $('<li class="WaitForScrollSpacer" />').appendTo($fullPanel);
        var height = temp.height();
        temp.remove();

        return height;
    }


    function jQueryCacheSize() {
        var element_count = 0;
        for (e in $.cache) { element_count++; }
        return element_count;
    }

    __init(__pdfEditor);
    return __pdfEditor;

};
