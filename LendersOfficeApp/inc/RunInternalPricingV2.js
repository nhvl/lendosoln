﻿
var pricingMode;

$(function () {

    if (ML.IsUlad2019) {
        initializeQualifyingBorrower(true);
        var $trueContainer = $('.true-container');
        $trueContainer.attr('class', $trueContainer.attr('class') + ' wrap');
    }
    else {
        $('.qualifying-borrower-container').remove();
    }

    var isCheckEligibility = $('#IsCheckEligibility').val() === 'True';
    var currentPricingRequestId;
    var historicalPricingRequestId;
    var pollingInterval = 1000;
    var popupTimer;
    var popupDelay = 500;

    attachEvents();
    initialize();
    initializeAngular();

    function attachEvents() {
        $("#advancedFilterBtn").click(function () {
            if (typeof OpenProductCodeFilterPopup === 'function') {
                OpenProductCodeFilterPopup();
            }
        });

        $("#advancedFilterBtn").hover(
            function (e) {
                var parent = $('#InternalPricingV2');
                popupTimer = setTimeout(function () {
                    if (typeof DisplayTooltip === 'function') {
                        DisplayTooltip(e, parent);
                    }
                }, popupDelay);
            },
            function () {
                clearTimeout(popupTimer);
                if (typeof HideTooltip === 'function') {
                     HideTooltip();
                }
            }
        );

        $("#sProdFilterRestrictResultToRegisteredProgram").change(function () {
            if (ML.IsAdvancedFiltersEnabled === true)
            {
                setDisabledAttr($("#advancedFilterBtn"), this.checked === true);
            }
        });


        $(window).on('beforeunload', function () {
            save();
        });

        $("input, select").on("change", function () {
            $("#ResultsContainer").toggleClass("mask-results", true);
        });

        $('.price-button').on('click', function () {
            submitPricingRequest();
        });

        $('#sProdFilterRestrictResultToRegisteredProgram').on('change', function () {
            refreshCalculation();
            toggleDisabledFilters();
        });

        $('#sProdFilterRestrictResultToCurrentRegistered,#sProdFilterMatchCurrentTerm').on('change', function () {
            toggleDisabledFilters();
        });

        $('.results-filter input[type="checkbox"]').on('change', function () {
            refreshLoanProgramCountAndProductCodeFilters();
        });

        $('.pricing-options input[name="pricingOption"]').on('change', function () {
            toggleDisabledPricingOptions();
            filterHistoricalSubmissions();
            refreshLoanProgramCountAndProductCodeFilters();
        });

        $('.pricing-options input[name="sProdInvestorRLckdModeT"]').on('change', function () {
            toggleDisabledInvestorPricingRow();
        });

        $('#sInvestorLockLpePriceGroupId').on('change', function () {
            refreshLoanProgramCountAndProductCodeFilters();
        });

        $('#InvestorRateLockCalendar').on('click', function () {
            displayCalendar('sProdInvestorRLckdExpiredD');
        });

        $('input[name="GetResultsUsing"]').on('change', function () {
            toggleHistoricalFields();
            toggleDisabledFilters();
        });

        $('#explainHistorical,#explainWorstCase').on('click', function (e) {
            e.preventDefault();
            return false;
        });

        $('#HistoricalSubmissions').on('change', function () {
            populateHistoricalDate();
        });

        $('.left-column').tooltip();

        $('.left-column').on('mousedown', 'select.disabled-select', function (e) {
            // See http://stackoverflow.com/a/23375267
            e.preventDefault();
            this.blur();
            window.focus();
        });

        $('.data-refresh').on('change', function () {
            refreshCalculation();
        });
    }

    function initialize() {
        $('.price-button').attr('NoHighlight', 'NoHighlight');
        toggleDisabledFilters();
        toggleDisabledPricingOptions();
        toggleHistoricalFields();
        filterHistoricalSubmissions();
        populateHistoricalDate();
        refreshLoanProgramCountAndProductCodeFilters();

        if (typeof (RegisterPostApplyProductCodesCallback) !== 'undefined')
        {
            RegisterPostApplyProductCodesCallback(refreshLoanProgramCountAndProductCodeFilters);
        }
    }

    function toggleDisabledFilters() {
        var restrictToRegisteredProgram = $('#sProdFilterRestrictResultToRegisteredProgram').prop('checked');
        var restrictToRegisteredProduct = $('#sProdFilterRestrictResultToCurrentRegistered').prop('checked');
        var restrictToCurrentAmortTerm = $('#sProdFilterMatchCurrentTerm').prop('checked');
        var isWorstCasePricing = $('#WorstCasePricing').is(':checked');

        // Reset and then disable as needed.
        $('.disable-for-registered-program,.term-amort-prod-filters input[type="checkbox"]')
            .not('.perma-disable') // perma-disable checkboxes are never supposed to be enabled.
            .prop('disabled', false);

        if (restrictToRegisteredProgram) {
            $('.disable-for-registered-program,.term-amort-prod-filters input[type="checkbox"]')
                .not('.perma-disable')
                .prop('disabled', true);
        }

        if (restrictToRegisteredProduct) {
            $('#prod-filters input')
                .prop('disabled', true);
        }

        if (restrictToCurrentAmortTerm) {
            $('#amort-filters input,#term-filters input')
                .prop('disabled', true);
        }

        if (isWorstCasePricing) {
            $('#sProdFilterDisplayrateMerge')
                .prop('checked', false)
                .prop('disabled', true);
        }
    }

    function refreshLoanProgramCountAndProductCodeFilters() {
        if (isCheckEligibility) {
            return;
        }

        var args = {
            LoanId: ML.sLId,
            PricingOption: $('input[name="pricingOption"]:checked').val(),
            sInvestorLockLpePriceGroupId: $('#sInvestorLockLpePriceGroupId').val()
        };

        $('.results-filter input[type="checkbox"]').each(function () {
            args[this.id] = this.checked;
        });

        if (typeof (RetrieveSelectedProductCodes) === 'function') {
            args['sSelectedProductCodeFilter'] = RetrieveSelectedProductCodes();
        }

        callWebMethodAsync({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: 'RunInternalPricingV2Service.aspx?method=GetNumberOfLoanPrograms',
            data: JSON.stringify(args),
            dataType: 'json',
            async: true
        }).then(function (msg) {
            var result = msg.d;
            if (typeof (result.ErrorMessage) != "undefined") {
                console.log('Got an error!');
                console.log(result.value["ErrorMessage"]);
                $('#NumLPs').text("Number of Programs: N/A");
            }
            else {
                $('#NumLPs').text("Number of Programs: " + result.NumLPs);
                $('.price-button').prop('disabled', result.NumLPs === '0');
                if(typeof(ReloadProductCodes) != 'undefined') {
                    ReloadProductCodes(result.sAvailableProductCodeFilter, result.sSelectedProductCodeFilter, result.sProductCodesByFileType);
                }
            }
        });
    }

    function toggleDisabledPricingOptions() {
        if (isCheckEligibility) {
            $('.disable-for-front-end-pricing')
                .filter('input[type!="radio"]')
                    .prop('readonly', true)
                .end()
                .filter('[type="radio"]')
                    .prop('disabled', true)
                .end()
                .filter('select')
                    .addClass('disabled-select');

            $('.disable-for-investor-pricing').prop('readonly', true);
        } else {
            var isFrontEndPricing = $('#frontEndPricing').prop('checked');
            $('.disable-for-front-end-pricing')
                .filter('input[type!="radio"]')
                    .prop('readonly', isFrontEndPricing)
                .end()
                .filter('[type="radio"]')
                    .prop('disabled', isFrontEndPricing)
                .end()
                .filter('select')
                    .toggleClass('disabled-select', isFrontEndPricing);

            $('.disable-for-investor-pricing').prop('readonly', !isFrontEndPricing);

            if (!isFrontEndPricing) {
                toggleDisabledInvestorPricingRow();
            }
        }
    }

    function toggleDisabledInvestorPricingRow() {
        var isEditRateLockPeriod = $('#editInvestorRateLockPeriod').prop('checked');
        $('#sProdInvestorRLckdDays').prop('readonly', !isEditRateLockPeriod);
        $('#sProdInvestorRLckdExpiredD').prop('readonly', isEditRateLockPeriod);
    }

    function filterHistoricalSubmissions() {
        var selectedPricingMode = $('input[name="pricingOption"]:checked').val();
        var $historicalSubmissions = $('#HistoricalSubmissions');
        var frontEndSubmissionTypes = [
            parseInt(ML.EnumsByName.E_sPricingModeT.Undefined, 10),
            parseInt(ML.EnumsByName.E_sPricingModeT.RegularPricing, 10),
            parseInt(ML.EnumsByName.E_sPricingModeT.InternalBrokerPricing, 10)
        ];
        var investorSubmissionType = parseInt(ML.EnumsByName.E_sPricingModeT.InternalInvestorPricing, 10);

        $historicalSubmissions.empty();

        for (var i = 0; i < HistoricalSubmissionOptions.length; ++i) {
            var keyValuePair = HistoricalSubmissionOptions[i];
            var description = keyValuePair.Key;
            var snapshot = JSON.parse(keyValuePair.Value);
            var includeOption = (selectedPricingMode === 'FrontEnd' && $.inArray(snapshot.SubmissionType, frontEndSubmissionTypes) !== -1)
                || (selectedPricingMode === 'Investor' && snapshot.SubmissionType === investorSubmissionType);

            if (includeOption) {
                var $option = $('<option />');
                $option.text(description).val(keyValuePair.Value);
                $historicalSubmissions.append($option);
            }
        }

        $historicalSubmissions
            .children()
            .last()
            .prop('selected', true)
            .append(document.createTextNode(" - Most Recent"))
            ;

        var noHistoricalData = $historicalSubmissions.children().length === 0;

        if (noHistoricalData) {
            $('#CurrentPricing')
                .prop('checked', true)
                .change();
        }

        $('#HistoricalPricing,#WorstCasePricing')
            .prop('disabled', noHistoricalData);
    }

    function toggleHistoricalFields() {
        var checkedOption = $('input[name="GetResultsUsing"]:checked').val();
        var isCurrentPricing = checkedOption === ML.EnumsByName.PricingResultsType.Current;
        var $historicalFields = $('#HistoricalFields');
        $historicalFields.toggle(!isCurrentPricing);
        var $historicalTopLabel = $('#HistoricalPricingTopLabel');
        var $historicalBottomLabel = $('#HistoricalPricingBottomLabel');

        if (checkedOption === ML.EnumsByName.PricingResultsType.Historical) {
            $historicalFields.detach();
            $('#HistoricalPricingRow').append($historicalFields);
            $historicalTopLabel.text('Get results from the date and time of');
            $historicalBottomLabel.hide();
        } else if (checkedOption === ML.EnumsByName.PricingResultsType.WorstCase) {
            $historicalFields.detach();
            $('#WorstCasePricingRow').append($historicalFields);
            $historicalTopLabel.text('Compare results from the date and time of');
            $historicalBottomLabel.text('to current results');
            $historicalBottomLabel.show();
        }
    }

    function populateHistoricalDate() {
        var $historicalSubmissions = $('#HistoricalSubmissions');
        if (!$historicalSubmissions.length) {
            return;
        }

        var snapshot = JSON.parse($historicalSubmissions.val());
        if (!snapshot) {
            return;
        }

        var date = new Date(snapshot.SnapshotDataLastModifiedD);
        $('#HistoricalDate').val((date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear() + ' ' + date.toLocaleTimeString());
    }

    function save() {
        if (isDirty()) {
            saveMe();
        }
    }

    function submitPricingRequest() {
        save();

        $('.price-button').prop('disabled', true);
        $('.mask-when-pricing').toggleClass('mask', true);
        $('#loadingResultsGif').toggleClass('mask', true);

        $("#ResultsContainer").toggleClass("mask-results", false);
        $("#ResultsContainer").hide();
        $("#LoadingMessage").show();

        // Set timeout to allow the mask/disable to take effect before waiting
        // for the service call to complete.
        window.setTimeout(function () {
            var args = {
                LoanId: ML.sLId,
                PricingOption: $('input[name="pricingOption"]:checked').val(),
                GetResultsUsing: $('input[name="GetResultsUsing"]:checked').val(),
                SnapshotJson: $('#HistoricalSubmissions').val(),
                IsCheckEligibility: isCheckEligibility
            };

            gService.loanedit.callAsyncSimple('SubmitInternalPricingRequest', args, function (result) {
                if (!result.error) {
                    currentPricingRequestId = result.value['CurrentRequestId'];
                    historicalPricingRequestId = result.value['HistoricalRequestId'];
                    window.setTimeout(checkForPricingResults, pollingInterval);
                }
                else {
                    alert('Unable to get pricing please try again.');
                }
            });
        }, 100);
    }

    function checkForPricingResults() {
        var args = {
            CurrentRequestId: currentPricingRequestId,
            HistoricalRequestId: historicalPricingRequestId,
            LoanId: ML.sLId,
            PricingOption: $('input[name="pricingOption"]:checked').val(),
            IsCheckEligibility: isCheckEligibility,
            GetResultsUsing: $("input[name='GetResultsUsing']:checked").val(),
            sProdFilterDisplayrateMerge: $("#sProdFilterDisplayrateMerge").prop("checked")
        };

        gService.loanedit.callAsyncSimple('IsResultReady', args, function (result) {
            if (!result.error) {
                var isReady = result.value["IsReady"] === 'True';
                if (isReady) {

                    // Retrieve the results and render.
                    var pricingSettings = {
                        showQm: ML.showQm,
                        IsDebugColumns: ML.IsDebugColumns,
                        IsBreakEvenMonths: ML.IsBreakEvenMonths,
                        isCheckEligibility: isCheckEligibility,
                        sProdFilterDisplayrateMerge: $("#sProdFilterDisplayrateMerge").prop("checked"),
                        GetResultsUsing: $("input[name='GetResultsUsing']:checked").val(),
						HasArmFilter: $("#sProdFilterFinMeth7YrsArm:checked, #sProdFilterFinMeth3YrsArm:checked, #sProdFilterFinMeth10YrsArm:checked, #sProdFilterFinMeth5YrsArm:checked").length > 0,
                        IsInvestorPricing: $("#investorPricing").prop("checked"),
                        IsUlad2019: ML.IsUlad2019
                    };

                    pricingMode = result.value.pricingMode;

                    updateResults(pricingSettings, ML.sDisclosureRegulationT, JSON.parse(result.value["PricingResults"]));

                    $('.price-button').prop('disabled', false);
                    $('.mask-when-pricing').toggleClass('mask', false);
                    $('#loadingResultsGif').toggleClass('mask', false);
                    $("#ResultsContainer").show();
                    $("#LoadingMessage").hide();
                }
                else {
                    window.setTimeout(checkForPricingResults, pollingInterval);
                }
            }
            else {
                alert('Unable to get pricing please try again.');
            }
        });
    }
});


function navigateOnSubmit() {
    var urlOption;
    switch ($("[name='pricingOption']:checked").val()) {
        case "FrontEnd":
            urlOption = 32;
            break;
        case "Investor":
            urlOption = 33;
    }

    this.self.location = ML.VirtualRoot + "/newlos/loanapp.aspx?loanid=" + ML.sLId + "&highUrl=" + urlOption;
}
