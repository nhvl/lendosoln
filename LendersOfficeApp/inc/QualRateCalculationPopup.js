﻿var wasDirty = false;
var originalQualRateValues = {};

var QualRateCalculationPopup = null;
var ShouldUseQualRate = null;
var LegacyValueName = 'Legacy Value';
var QualRateCalculationFlatValue = null;
var QualRateCalculationMaxOf = null;
var QualRateCalculationFieldT1 = null;
var QualRateCalculationFieldT2 = null;
var QualRateCalculationAdjustment1 = null;
var QualRateCalculationAdjustment2 = null;

function hasLegacyOption() {
    // This method will be called when updating the qual rate 
    // calculation dropdowns from code in QualRateCalculation.js
    return false;
}

function _postGetAllFormValues(obj) {
    setServiceArgumentValuesFromPopup(obj);
}

function _postRefreshCalculation(resultValue) {
    if (resultValue.HasQualRatePopup == "True") {
        updateQualRateFields(resultValue);
    }
}

function setServiceArgumentValuesFromPopup(obj) {
    obj.sUseQualRate = ShouldUseQualRate.checked;
    obj.sQualRateCalculationFieldT1 = $(QualRateCalculationFieldT1).val();
    obj.sQualRateCalculationAdjustment1 = $(QualRateCalculationAdjustment1).val();
    obj.sQualRateCalculationFieldT2 = $(QualRateCalculationFieldT2).val();
    obj.sQualRateCalculationAdjustment2 = $(QualRateCalculationAdjustment2).val();

    obj.sQualRateCalculationT = QualRateCalculationFlatValue.checked ? ML.FlatValue : ML.MaxOf;
}

function updateQualRateFields(resultValue) {
    ShouldUseQualRate.checked = resultValue.sUseQualRate.toLowerCase() === "true";

    $('#' + QualRateFieldIds.QualRate).val(resultValue.sQualIR);
    $(QualRateCalculationFieldT1).val(resultValue.sQualRateCalculationFieldT1);
    $(QualRateCalculationFieldT2).val(resultValue.sQualRateCalculationFieldT2);
    $(QualRateCalculationAdjustment1).val(resultValue.sQualRateCalculationAdjustment1);
    $(QualRateCalculationAdjustment2).val(resultValue.sQualRateCalculationAdjustment2);
    $('#' + QualRateFieldIds.QualRateCalculationResult1).val(resultValue.QualRateCalculationResult1);
    $('#' + QualRateFieldIds.QualRateCalculationResult2).val(resultValue.QualRateCalculationResult2);

    var isFlatValue = resultValue.sQualRateCalculationT === ML.FlatValue;
    QualRateCalculationFlatValue.checked = isFlatValue;
    QualRateCalculationMaxOf.checked = !isFlatValue;
}

function setOriginalQualRateValues() {
    setServiceArgumentValuesFromPopup(originalQualRateValues);
    originalQualRateValues.sQualIR = document.getElementById(QualRateFieldIds.QualRate).value;

    // Service page will expect "true" or "false"
    originalQualRateValues.sUseQualRate = ShouldUseQualRate.checked.toString();
}

function revertQualRateChanges() {
    updateQualRateFields(originalQualRateValues);

    if (!wasDirty && isDirty()) {
        clearDirty();
    }
}

function showQualRatePopup() {
    if ($('input[id$="sQualIRLckd"]').prop('checked')) {
        return;
    }

    wasDirty = isDirty();
    updateQualRateCalculationModel();
    setOriginalQualRateValues();

    $(QualRateCalculationPopup).dialog({
        dialogClass: "QualRateCalculationPopup",
        width: 360,
        height: 200,
        modal: true,
        title: "Qualifying Rate Calculation",
        resizable: false,
        draggable: false
    });
}

function hideQualRatePopup() {
    $(QualRateCalculationPopup).dialog("close");
}

function setQualRateLinkDisabled() {
    var isDisabled = $('input[id$="sQualIRLckd"]').prop('checked');
    $("#QualRateCalcLink").prop('disabled', isDisabled);
    $("#QualRateCalcLink").toggleClass("DisabledLink", isDisabled);
}

jQuery(function ($) {
    QualRateCalculationPopup = document.getElementById(QualRateFieldIds.QualRateCalcPopup);
    ShouldUseQualRate = document.getElementById(QualRateFieldIds.ShouldUseQualRate);
    LegacyValueName = 'Legacy Value';
    QualRateCalculationFlatValue = document.getElementById(QualRateFieldIds.QualRateCalculationFlatValue);
    QualRateCalculationMaxOf = document.getElementById(QualRateFieldIds.QualRateCalculationMaxOf);
    QualRateCalculationFieldT1 = document.getElementById(QualRateFieldIds.QualRateCalculationFieldT1);
    QualRateCalculationFieldT2 = document.getElementById(QualRateFieldIds.QualRateCalculationFieldT2);
    QualRateCalculationAdjustment1 = document.getElementById(QualRateFieldIds.QualRateCalculationAdjustment1);
    QualRateCalculationAdjustment2 = document.getElementById(QualRateFieldIds.QualRateCalculationAdjustment2);

    $('select.QualRateCalculationField, input.sQualRateCalculationT, #' + QualRateFieldIds.ShouldUseQualRate).change(function () {
        updateQualRateCalculationModel();
        refreshCalculation();
    });

    $("#QualRateCalcLink").click(showQualRatePopup);

    $('input[id$="sQualIRLckd"]').click(setQualRateLinkDisabled);
    setQualRateLinkDisabled();

    $('#' + QualRateFieldIds.QualRateCalculationPopup_Ok).click(hideQualRatePopup);

    $('#' + QualRateFieldIds.QualRateCalculationPopup_Close).click(function () {
        // The qualifying rate can impact many fields on a page. 
        // To ensure the user's changes aren't saved, revert the
        // changes made in this popup.
        revertQualRateChanges();
        refreshCalculation();
        hideQualRatePopup();
    });
});
