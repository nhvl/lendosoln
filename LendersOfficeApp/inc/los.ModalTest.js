/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 404);
/******/ })
/************************************************************************/
/******/ ({

/***/ 404:
/*!******************************!*\
  !*** ./los/ModalTest/app.ts ***!
  \******************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var SizeEnum;
(function (SizeEnum) {
    SizeEnum[SizeEnum["Oversize"] = 0] = "Oversize";
    SizeEnum[SizeEnum["Standard"] = 1] = "Standard";
})(SizeEnum || (SizeEnum = {}));
var TestEnum;
(function (TestEnum) {
    TestEnum[TestEnum["AutoResize"] = 0] = "AutoResize";
    TestEnum[TestEnum["PopupConfig"] = 1] = "PopupConfig";
    TestEnum[TestEnum["ResizeMethod"] = 2] = "ResizeMethod";
})(TestEnum || (TestEnum = {}));
function getModalSize(sizeEnum) {
    var containerWidth = window.innerWidth;
    var containerHeight = window.innerHeight;
    switch (sizeEnum) {
        case SizeEnum.Oversize:
            return { width: containerWidth * 2, height: containerHeight * 2 };
        default:
            return { width: containerWidth / 2, height: containerHeight / 2 };
    }
}
(function () {
    return __awaiter(this, void 0, void 0, function () {
        var containerWidth, containerHeight, requiredAssertions, cases, _loop_1, url, config, postShowModal, _i, cases_1, testCase, state_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    containerWidth = window.innerWidth;
                    containerHeight = window.innerHeight;
                    requiredAssertions = [NotSurpassParent];
                    cases = [
                        { modalSize: SizeEnum.Standard, testType: TestEnum.AutoResize, customAssertion: [AvoidScroller] },
                        { modalSize: SizeEnum.Oversize, testType: TestEnum.AutoResize, customAssertion: [] },
                        { modalSize: SizeEnum.Standard, testType: TestEnum.PopupConfig, customAssertion: [SizeMatchConfig] },
                        { modalSize: SizeEnum.Oversize, testType: TestEnum.PopupConfig, customAssertion: [] },
                        { modalSize: SizeEnum.Standard, testType: TestEnum.ResizeMethod, customAssertion: [SizeMatchConfig] },
                        { modalSize: SizeEnum.Oversize, testType: TestEnum.ResizeMethod, customAssertion: [] },
                    ];
                    _loop_1 = function (testCase) {
                        var modalSize, _i, _a, assertion;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    modalSize = getModalSize(testCase.modalSize);
                                    url = "/test/MockupModal.aspx?temp=1";
                                    config = {};
                                    postShowModal = noop;
                                    switch (testCase.testType) {
                                        case TestEnum.AutoResize:
                                            url += "&w=" + modalSize.width + "&h=" + modalSize.height;
                                            break;
                                        case TestEnum.PopupConfig:
                                            config = { width: modalSize.width, height: modalSize.height };
                                            break;
                                        default:
                                            postShowModal = function () { document.querySelector("#LQBPopupDiv iframe").contentWindow.resize(modalSize.width, modalSize.height); };
                                            break;
                                    }
                                    showModal(url, null, null, null, noop, config);
                                    return [4, sleep(1000)];
                                case 1:
                                    _b.sent();
                                    postShowModal();
                                    for (_i = 0, _a = testCase.customAssertion.concat(requiredAssertions); _i < _a.length; _i++) {
                                        assertion = _a[_i];
                                        if (!assertion(testCase)) {
                                            console.error(assertion.name, testCase);
                                            return [2, { value: void 0 }];
                                        }
                                    }
                                    console.info("Test passed:", testCase);
                                    return [2];
                            }
                        });
                    };
                    _i = 0, cases_1 = cases;
                    _a.label = 1;
                case 1:
                    if (!(_i < cases_1.length)) return [3, 4];
                    testCase = cases_1[_i];
                    return [5, _loop_1(testCase)];
                case 2:
                    state_1 = _a.sent();
                    if (typeof state_1 === "object")
                        return [2, state_1.value];
                    _a.label = 3;
                case 3:
                    _i++;
                    return [3, 1];
                case 4: return [2];
            }
        });
    });
})();
function AvoidScroller(testCase) {
    var mBody = document.querySelector("#LQBPopupDiv iframe").contentWindow.document.body;
    var hasHScroll = (mBody.scrollHeight > mBody.clientHeight);
    var hasVScroll = (mBody.scrollWidth > mBody.clientWidth);
    return !hasVScroll && !hasHScroll;
}
function NotSurpassParent(testCase) {
    var iframe = document.querySelector("#LQBPopupDiv iframe");
    var isOverflown = (iframe.width > window.innerWidth || iframe.height > window.innerHeight);
    return !isOverflown;
}
function SizeMatchConfig(testCase) {
    var modalButtonPadding = 25;
    var modalSize = getModalSize(testCase.modalSize);
    var mWindow = document.querySelector("#LQBPopupDiv iframe").contentWindow;
    var isFit = (mWindow.innerHeight == (modalSize.height + modalButtonPadding) && mWindow.innerWidth == modalSize.width);
    if (!isFit)
        debugger;
    return isFit;
}
function noop() { }
function sleep(m, v) {
    return new Promise(function (resolve) { setTimeout(resolve, m, v); });
}
function throwError(error) { throw error; }


/***/ })

/******/ });
//# sourceMappingURL=los.ModalTest.js.map