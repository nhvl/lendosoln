﻿$(document).ready(function () {
    // Set up this handler here instead of init() to avoid a loop
    // on calculation refresh.
    $('#sHmdaMultifamilyUnitsNotApplicable').click(setMultiFamilyUnitsValue);
    setMultiFamilyUnitsValue();
});

function _init() {
    lockField(document.getElementById('sUniversalLoanIdentifierLckd'), 'sUniversalLoanIdentifier');
    lockField(document.getElementById('sHmdaApplicationDateLckd'), 'sHmdaApplicationDate');
    lockField(document.getElementById('sHmdaTotalUnitsLckd'), 'sHmdaTotalUnits');
    lockDdl(document.getElementById('sHmdaLoanPurposeTLckd'), 'sHmdaLoanPurposeT');
    lockDdl(document.getElementById('sHmdaLoanAmountLckd'), 'sHmdaLoanAmount');

    $('.data-refresh').change(refreshCalculation);

    if (ML.LoanVersionTCurrent < 22) {
        $(".sHmdaActionTakenT").hide();
    } else {
            var combo = $(".sHmdaActionTaken");
            combo.hide();
            combo.next(".combobox_img").hide();
    }
}

function lockDdl(cb, tbID) {
    var item = document.getElementById(tbID);

    if (!cb.checked) {
        item.style.backgroundColor = "lightgrey";
        item.disabled = true;
    }
    else {
        item.style.backgroundColor = "white";
        item.disabled = false;
    }
}

function setMultiFamilyUnitsValue() {
    var sHmdaMultifamilyUnits = $('#sHmdaMultifamilyUnits');
    var sHmdaMultifamilyUnitsNotApplicableChecked = $('#sHmdaMultifamilyUnitsNotApplicable').prop('checked');

    sHmdaMultifamilyUnits.prop('readonly', sHmdaMultifamilyUnitsNotApplicableChecked);

    if (sHmdaMultifamilyUnitsNotApplicableChecked) {
        sHmdaMultifamilyUnits.val('NA').change();
    }
    else if (sHmdaMultifamilyUnits.val() === 'NA') {
        sHmdaMultifamilyUnits.val('');
    }
}
