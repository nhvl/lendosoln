// If this value set to true then value are recalculate on every field's value change.
// There is a delay when tab to next field, therefore for data entry purpose, turn this bit off.

var bIsAutoCalculate = true;
var clientID = null;

function backgroundCalculation(isAsync, callback) {

    // Check to see if there's an asynchronous ajax call.  If so, prevent re-calculating.
    if (document.getElementById("_ClientID") != null)
        clientID = document.getElementById("_ClientID").value;
    // Since recalculate does not always required every field to be pass to server.
    // Add skipMe attribute to input that does not required in calculation.    
    var args = getAllFormValues(clientID, true);
    if (typeof (_postGetAllFormValues) == "function") {
        _postGetAllFormValues(args);
    }

    for (var i = 0; i < _postGetAllFormValuesCallbacks.length; i++) {
        if (typeof (_postGetAllFormValuesCallbacks[i]) == "function") {
            _postGetAllFormValuesCallbacks[i](args);
        }
    }

    var method = "CalculateData";
    if (clientID != null && clientID != "")
        method = clientID + "_CalculateData";

    if (isAsync) {
        gService.loanedit.callAsyncSimple(method, args, function (result) { backgroundCalculationCallback(result); callback(); });
        
    }
    else {
        var result = gService.loanedit.call(method, args);
        return backgroundCalculationCallback(result);
    }
}

function performGetAllFormValuePostFunctions(args) {
    if (typeof (_postGetAllFormValues) == "function") {
        _postGetAllFormValues(args);
    }

    for (var i = 0; i < _postGetAllFormValuesCallbacks.length; i++) {
        if (typeof (_postGetAllFormValuesCallbacks[i]) == "function") {
            _postGetAllFormValuesCallbacks[i](args);
        }
    }
}

function backgroundCalculationCallback(result) {
    if (document.getElementById("_ClientID") != null)
        clientID = document.getElementById("_ClientID").value;

    if (!result.error) {
        populateForm(result.value, clientID);
        if (typeof (_postRefreshCalculation) == "function") {
            _postRefreshCalculation(result.value, clientID);
        }

        for (var i = 0; i < _postRefreshCalculationCallbacks.length; i++) {
            if (typeof (_postRefreshCalculationCallbacks[i]) == "function") {
                _postRefreshCalculationCallbacks[i](result.value, clientID);
            }
        }
    } else {

        if (result.ErrorType === 'LoanFieldWritePermissionDenied') {
            f_displayFieldWriteDenied(result.UserMessage);
            return false;
        }

        var errMsg = 'Unable to process data. Please try again.';

        if (null != result.UserMessage)
            errMsg = result.UserMessage;

        if (typeof (_onError) == "function")
            _onError(errMsg);
        else
            alert(errMsg);
        return false;
    }

    if (typeof (_init) == "function") _init();
}

function refreshCalculation() {
    if (bIsAutoCalculate == true)
        backgroundCalculation(false);
}

function refreshCalculationAsync(callback)
{
    if (bIsAutoCalculate == true) {
        backgroundCalculation(true, callback);
    }
    else {
        callback();
    }
}

function onDateAddTime(e) {
    if (e != null) {
        date_onblur(null, e);
        refreshCalculation();
    }
}

function ChecksBrokComp1Pc(ignoreIfNotChanged) {
    if (!(document.forms[0]["sBrokComp1Pc"]))
        return;
    var clientID = null;
    if (document.getElementById("_ClientID") != null)
        clientID = document.getElementById("_ClientID").value;
    var method = "ChecksBrokComp1Pc";
    var args = new Object();
    args["loanid"] = document.getElementById("loanid").value;
    args["applicationid"] = document.getElementById("applicationid").value;
    if (ignoreIfNotChanged == true) {
        args["sBrokComp1Pc"] = document.forms[0]["sBrokComp1Pc"].value;
        args["ignoreIfNotChanged"] = true;
    }
    else
        args["ignoreIfNotChanged"] = false;
    var result = gService.loanedit.call(method, args);

    if (!result.error) {
        populateForm(result.value, clientID);
        return result.value;
    }
    else
        return null;
}

var _postSaveMeCallbacks = [];
//Registers a callback that will be called post-save. Returns true if the callback was successfully registered,
//or false if the callback is already in the array. Don't use lambdas, they're never equal to each other.
function registerPostSaveMeCallback(fxn) {
    for (var i = 0; i < _postSaveMeCallbacks.length; i++) {
        if (_postSaveMeCallbacks[i] == fxn) return false;
    }
    _postSaveMeCallbacks.push(fxn);
    return true;
}

var _postRefreshCalculationCallbacks = [];


function registerPostRefreshCalculationCallback(funcToRegister) {
    for (var i = 0; i < _postRefreshCalculationCallbacks.length; i++) {
        if (_postRefreshCalculationCallbacks[i] == funcToRegister) return false;
    }
    _postRefreshCalculationCallbacks.push(funcToRegister);
    return true;
}

function saveMe(bRefreshScreen) {

    // 12/29/06 OPM 8935 db - first check to see if the sBrokComp1Pc field must be disabled because of a rate
    // lock that has occurred as a result of 2 people editing the GFE at the same time.  If so, alert the user,
    // disable the field, and continue
    if (document.forms[0]["sBrokComp1Pc"]) {
        var obj = ChecksBrokComp1Pc(true);

        if (obj && typeof (_alertUser) == "function")
            _alertUser(obj);
    }

    var bValid = true;
    if (typeof (Page_ClientValidate) == 'function' || typeof (Page_ClientValidate) == 'object')
        bValid = Page_ClientValidate();

    if (!bValid) {
        updateDirtyBit();
        return false;
    }


    var clientID = null;
    if (document.getElementById("_ClientID") != null)
        clientID = document.getElementById("_ClientID").value;

    var args = getAllFormValues(clientID);
    if (typeof (_postGetAllFormValues) == "function") {
        _postGetAllFormValues(args);
    }

    for (var i = 0; i < _postGetAllFormValuesCallbacks.length; i++) {
        if (typeof (_postGetAllFormValuesCallbacks[i]) == "function") {
            _postGetAllFormValuesCallbacks[i](args);
        }
    }

    var method = "SaveData";
    if (clientID != null && clientID != "")
        method = clientID + "_SaveData";

    var result = gService.loanedit.call(method, args);
    if (!result.error) {
        clearDirty();
        if (bRefreshScreen) {
            populateForm(result.value, clientID);
            if (null != parent.info) {
                if (parent.info.f_refreshInfoFromDB != null)
                    parent.info.f_refreshInfoFromDB(); //update loan summary
                else
                    parent.info.f_refreshInfo(); //update lead summary
            }

            if (typeof (_init) == "function") _init();

            if (typeof (_postSaveMe) == "function") _postSaveMe(result.value);

            for (var i = 0; i < _postSaveMeCallbacks.length; i++) {
                if (typeof (_postSaveMeCallbacks[i]) == "function") {
                    _postSaveMeCallbacks[i](result.value);
                }
            }
        }
        return true;
    } else {
        updateDirtyBit();
        var errMsg = 'Unable to save data. Please try again.';

        if (result.ErrorType === 'LoanFieldWritePermissionDenied') {
            f_displayFieldWriteDenied(result.UserMessage);
        }
        else if (result.ErrorType === 'VersionMismatchException') {
            f_displayVersionMismatch();
        }
        else {
            if (null != result.UserMessage)
                errMsg = result.UserMessage;

            if (typeof (_onError) == "function")
                _onError(errMsg);
            else {
                LQBPopup.ShowString(errMsg, {
                    width: 420,
                    height: 180,
                    modifyOverflow: false,
                    popupClasses: "LQBPopupAlert"
                });
            }
        }
        return false;
    }

}

function switchApplicant() {

    var bPerApp = document.getElementById("_PerApp") != null && document.getElementById("_PerApp").value == "True";

    if (bPerApp) {
        var loanID = document.getElementById("loanid").value;
        var extraArgs = document.getElementById("_PerAppExtraArgs") == null ? "" : document.getElementById("_PerAppExtraArgs").value;
        if (gIsDebug) alert("switchApplicant() url=" + self.location.pathname + "?loanid=" + loanID + "&" + extraArgs);
        f_load(self.location.pathname + "?loanid=" + loanID + "&" + extraArgs);
    }
}

function linkMe(href, extraArgs) {
    var ch = href.indexOf('?') > 0 ? '&' : '?';
    var loanID = document.getElementById("loanid").value;
    var target = (parent.body) ? parent.body : parent;
    target.f_load(href + ch + "loanid=" + loanID + (extraArgs == null ? "" : "&" + extraArgs));
}
function lockField(cb, tbID) {
    var clientID = "";
    if (document.getElementById("_ClientID") != null)
        clientID = document.getElementById("_ClientID").value + "_";

    var item = document.getElementById(clientID + tbID);
    lockFieldByElements(cb, item);
}

function lockFieldByElements(cb, item) {
    item.readOnly = !cb.checked;

    if (!cb.checked)
        item.style.backgroundColor = "lightgrey";
    else
        item.style.backgroundColor = "";
}
