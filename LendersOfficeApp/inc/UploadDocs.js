﻿var documentMetadataByName = {};
var uploadedDocs = [];
var totalFileProcessedCount = 0;

function displayUploadResults() {
    removeOverlay();
    resetDragDrop(dropzoneId);

    var $errorDiv = $('#ErrorDiv');
    if ($errorDiv.find('div').length !== 0) {
        $errorDiv.show();
    }

    if (uploadedDocs.length === 0) {
        return;
    }

    setUploadButtonDisabled(true);

    var suffix = uploadedDocs.length !== 1 ? 's' : '';
    var uploadLocation = typeof getUploadLocation === 'function' ? getUploadLocation() : 'uploaded';
    $('#UploadCountDiv').show().text(uploadedDocs.length + ' document' + suffix + ' ' + uploadLocation + '.');

    if (typeof documentUploadHandler === 'function') {
        documentUploadHandler(uploadedDocs);
        return;
    }
}

function readFileCallback(file, startTicks) {
    var args = {
        loanid: ML.sLId,
        aAppId: file.appId,
        RestrictToUcd: ML.RestrictToUcd,
        DocTypeId: file.docTypeId,
        InternalDescription: file.internalComments,
        PublicDescription: file.desc,
        StartTicks: startTicks
    };

    DocumentUploadHelper.Upload('UploadDocument', file, args, function (result) {
        if (result.error) {
            var $errorDiv = $('#ErrorDiv');
            var $errorMessage = $('<div/>').text(result.UserMessage);
            $errorDiv.append($errorMessage);
        }
        else {
            uploadedDocs.push({
                DocumentId: result.value['DocumentId'],
                DocTypeId: file.docTypeId,
                Description: args.PublicDescription,
                Comments: args.InternalDescription
            });
        }

        ++totalFileProcessedCount;
        if (totalFileProcessedCount === getFilesCount(dropzoneId)) {
            displayUploadResults();
        }
    });
}

function uploadFiles() {
    $('#ErrorDiv').empty().hide();
    $('#UploadCountDiv').hide();

    var files = retrieveFiles(dropzoneId);
    if (files.length === 0) {
        LQBPopup.ShowString('Please select file(s) to upload.');
        return;
    }

    var errors = '';
    for (var i = 0; i < files.length; ++i) {
        var file = files[i];

        if (!fileHasDocType(file)) {
            errors += 'Please select a Doc Type for document ' + file.name + '<br/>';
        }

        if (file.desc != null && file.desc.length > 200) {
            errors += 'Description is limited to 200 characters for document ' + file.name + '<br/>';
        }

        if (file.internalComments != null && file.internalComments.length > 200) {
            errors += 'Internal comments are limited to 200 characters for document ' + file.name + '<br/>';
        }
    }

    if (errors != '') {
        LQBPopup.ShowString(errors);
        return;
    }

    documentMetadataByName = {};
    uploadedDocs = [];
    totalFileProcessedCount = 0;

    triggerOverlayImmediately();
    gService.utils.callAsyncSimple('GetCurrentTicks', {}, function (result) {
        var startTicks = result.error ? null : result.value['Ticks'];

        applyCallbackFileObjects(dropzoneId, function (file) {
            readFileCallback(file, startTicks);
        });
    });
}

function setUploadButtonDisabled(disabled) {
    $('#UploadButton').prop('disabled', disabled);
}

function onDocTypeSelect() {
    var files = retrieveFiles(dropzoneId);
    var filesWithoutDocType = files.filter(function (file) { return !fileHasDocType(file); });
    setUploadButtonDisabled(filesWithoutDocType.length !== 0);
}
