﻿jQuery(function($) {
    var $Navigation = $('#BatchEditNavigation'),
        $SearchRegion = $('#BatchEditSearch'),
        $Search = $('#BatchEditSearch input'),
        resizeTimeoutId,
        pdfEditor,
        $documentList = $('ol.document-name-listing > li'),
        $folderList = $('li.folder'),
        controls = {
            PublicDescription: $('#PublicDescription'),
            InternalDescription: $('#InternalDescription'),
            RejectedReasonContainer: $('#pnlStatusDescription'),
            RejectedReason: $('#m_docStatusDescription'),
            Status: $('#m_docStatus')
        },
        CurrentDocument,
        BeforeDocSwitch = function() { return true; /*Return true to continue, false to cancel document switch*/ },
        AfterDocSwitch = function() { },
        $HeaderElement,
        HeaderPadding = 10 + $SearchRegion.height(),
        OnSplitDoc,
        EnableThumbDocHeader = false,
        SetupAnnotations = function() { },
        ViewDoc = function(docId) { alert("Document is unavailable"); };

    $Navigation.on('Initialize.BatchEdit', function(event, Settings) {
        pdfEditor = Settings.Editor;

        //pdfEditor.TrackPageScrolling();
        pdfEditor.BindEvent("PdfEditorScrolledPastLastPage.PdfEditor", function() {
            loadNextDoc();
        });

        pdfEditor.BindEvent("PdfEditorScrolledBeforeFirstPage.PdfEditor", function() {
            loadPrevDoc();
        });

        if ($.isFunction(Settings.SetupAnnotations)) {
            SetupAnnotations = Settings.SetupAnnotations;
        }

        if (Settings.DocPages && Settings.DocInfo) {
            //We don't actually need annotations to set up, so we don't check.
            setSelectedDoc(Settings.DocInfo, Settings.DocPages, Settings.DocAnnotations, Settings.ESignTags);
            if ($.isFunction(Settings.TabSwitch)) {
                Settings.TabSwitch();
            }
        }
        else if (Settings.DocId) {
            changeActiveDoc(null, Settings.DocId);
        }
        $HeaderElement = $(Settings.Header);

        if ($.isFunction(Settings.BeforeDocSwitch)) BeforeDocSwitch = Settings.BeforeDocSwitch;
        if ($.isFunction(Settings.AfterDocSwitch)) AfterDocSwitch = Settings.AfterDocSwitch;
        if ($.isFunction(Settings.ViewDoc)) ViewDoc = Settings.ViewDoc;

        $('#BatchEditSplitNewDoc').hide();
        if ($.isFunction(Settings.SplitDocs)) {
            $('#BatchEditSplitNewDoc').show();
            OnSplitDoc = Settings.SplitDocs;
        }
    });

    //Defaults to next doc, or prev doc if this is the last doc.
    $Navigation.on('AnotherDoc.BatchEdit', function(event, Settings) {
        if (!loadNextDoc()) loadPrevDoc();
    });

    $Navigation.on('GetCurrentDocumentId.BatchEdit', function () {
        return typeof CurrentDocument === 'undefined' ? null : CurrentDocument.DocumentId;
    });

    var $splitDialog = $('#BatchEditSplitDocDialog').dialog({
        autoOpen: false,
        title: "Move Pages",
        modal: true,
        resizable: false,
        width: 'auto',
        height: 100,
        open: function() {
            $(this).data('result-status', 'cancel');
        },
        close: function() {
            $(this).data('callback')($(this).data('result-status'));
        }
    });
    //Necessary if any runat="server" things are ever put in the dialog
    $splitDialog.parent().appendTo('#form1');

    $('#BatchEditAddToDoc').click(function() {
        var selectedPages = pdfEditor.GetSelectedPages($splitDialog.data('selected-ranges'));
        var destDocId = $splitDialog.data('dropped-on-info').DocumentId;

        var status = 'cancel';
        if (appendToDoc(destDocId, selectedPages)) status = 'clear';

        $splitDialog.data('result-status', status);
        $splitDialog.dialog('close');
    });

    $('#BatchEditSplitNewDoc').click(function() {
        var selected = $splitDialog.data('selected-ranges');
        var defaultDocType = $splitDialog.data('dropped-on-info');

        OnSplitDoc(selected, defaultDocType, function(isOK) {
            //We shouldn't do anything if the user clicked OK, 
            //because split docs will remove the pages on its own.
            var status = isOK ? 'nop' : 'cancel';

            $splitDialog.data('result-status', status);
            $splitDialog.dialog('close');
        });
    });

    function canAcceptDroppable() {
        if (typeof canEditDocument === 'function') {
            var dropTarget = $(this);
            return canEditDocument(dropTarget.data('docid'));
        }

        return true;
    }

    var placeholderShowTimeout;
    var droppableSettings = {
        accept: canAcceptDroppable,
        hoverClass: 'droppable-hover',
        tolerance: "pointer",
        greedy: true,
        over: function() {
            window.clearTimeout(placeholderShowTimeout);
            $('.placeholder').hide();
        },
        out: function() {
            window.clearTimeout(placeholderShowTimeout);

            //Just showing the placeholder causes flicker when the user is quickly dragging over docs
            placeholderShowTimeout = window.setTimeout(function() {
                $('.placeholder').show();
            }, 50);
        },

        drop: function(event, ui) {
            //Have to tell the sortable to not do anything this way, event.preventDefault() and return false don't work.
            ui.helper.data('sort-skip-default', true);

            var sortCallback = ui.helper.data("sort-callback");

            var folderName = $.trim($(this).closest('.folder').find('.doc-type-folder').text());
            var docType = $.trim($('.doc-type-name', this).text());
            var docId = $(this).data('docid');

            $splitDialog.data('result-status', 'cancel');
            $splitDialog.data('callback', sortCallback);
            $splitDialog.data('selected-ranges', ui.helper.data('selected-ranges'));
            $splitDialog.data('dropped-on-info', {
                FolderName: folderName,
                DocTypeName: docType,
                DocumentId: docId
            });


            var docName = folderName + ' - ' + docType;
            $('#BatchEditAddToDoc').val('Add to ' + docName);

            if ($(this).is(".selected")) {
                $('#BatchEditSplitDocDialog').removeClass('SplitOrAdd');
                $('#BatchEditAddToDoc').hide();
            }
            else {
                $('#BatchEditSplitDocDialog').addClass('SplitOrAdd');
                $('#BatchEditAddToDoc').show();
            }

            $splitDialog.dialog('open');
        }
    };

    $('.drop-target', $Navigation).droppable(droppableSettings);

    $Navigation.on('InitNewDoc.BatchEdit', function(event, documentListItem) {
        $(documentListItem).droppable(droppableSettings);
    });

    function loadNextDoc() {
        var $next;
        var $visibleDocs = $documentList.not('.Hidden');
        var len = $visibleDocs.len;
        for (var i = 0; i < len - 1; i++) {
            if ($($visibleDocs[i]).is(".selected")) {
                $next = $($visibleDocs[i + 1]);
            }
        }

        //We're at the last document
        if (!$next) return false;

        changeActiveDoc($next);
        return true;
    }
    function loadPrevDoc() {
        var $prev;
        var $visibleDocs = $documentList.not('.Hidden');
        var len = $visibleDocs.len;
        for (var i = 1; i < len; i++) {
            if ($($visibleDocs[i]).is(".selected")) {
                $prev = $($visibleDocs[i - 1]);
            }
        }

        if (!$prev) return false;

        changeActiveDoc($prev);
        return true;
    }

    var searchTimer;
    $Search.keyup(function() {
        var value = $(this).val();
        window.clearTimeout(searchTimer);
        window.setTimeout(function() {
            doSearch(value);
        }, 200);
    }).change(function(evt) {
        //Don't want to update the dirty bit when this loses focus
        evt.preventDefault();
        evt.stopImmediatePropagation();
        return false;
    });

    var $docs = $('li.document');
    var searchDataCache = [];


    function generateSearchDataCache() {
        $docs = $('li.document');
        searchDataCache = [];
        $docs.each(function() {
            var folderName = $(this).closest('.folder').find('.doc-type-folder').text().toLowerCase();
            var temp = [folderName];
            $('span, .searchable', this).not('.search-ignore').each(function() {
                temp.push($(this).text().toLowerCase());
            });
            searchDataCache.push(temp.join(' '));
        });
    }

    generateSearchDataCache();

    function doSearch(val) {
        generateSearchDataCache();
        $('.Hidden').removeClass('Hidden');
        setFolderVisibility();
        $('.search-result').removeClass('search-result');

        if ($.trim(val).length == 0) {
            return;
        }

        val = val.toLowerCase().split(' ');
        var len = searchDataCache.length;
        var val_len = val.length;

        for (var i = 0; i < len; i++) {
            var doc = $docs[i];
            var cacheData = searchDataCache[i]
            var match = true;
            for (var j = 0; j < val_len; j++) {
                if (cacheData.indexOf(val[j]) == -1) {
                    doc.className += ' Hidden';
                    match = false;
                    break;
                }
            }

            if (match) {
                doc.className += ' search-result';
            }
        }

        $folderList.addClass('Hidden');
        $folderList.has('.search-result').removeClass('Hidden');

        filterDocs();
    }

    var changingDocs = false;
    var loadingTimeout;
    function changeActiveDoc($selectedItem, documentId) {
        if (docListUpdate) {
            $documentList = $('ol.document-name-listing > li');
            docListUpdate = false;
        }

        if (!documentId && !$selectedItem) throw "changeActiveDoc needs parameters";

        if (!documentId) {
            documentId = $selectedItem.data('docid');
        }
        //if the click is a result of doc filtering, then reload the doc even if the ids match
        if (documentId == CurrentDocument.DocumentId && !filtering) {
            return; //nothing to do
        }
        if (!BeforeDocSwitch(documentId)) return;
        if (changingDocs) return;
        changingDocs = true;

        if (!$selectedItem) {
            $documentList.each(function() {
                if ($(this).data('docid') == documentId) {
                    $selectedItem = $(this);
                    return false;
                }
            });
        }

        $documentList.removeClass('selected');

        window.clearTimeout(loadingTimeout);
        loadingTimeout = window.setTimeout(function() {
            $selectedItem.addClass('selecting');
        }, 500);

        var DTO = {
            'DocId': documentId
        };
        var pages, docDetails, annotations, eSignTags;
        var success = true;

        var isSwitchSuccess = false;
        callWebMethodAsync({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: 'EditEDocService.aspx?method=GetBatchDocDetails',
            data: JSON.stringify(DTO),
            dataType: 'json',
            async: true
        }).then(
            function(msg) {
                window.clearTimeout(loadingTimeout);
                pages = JSON.parse(msg.d.PagesJSON);
                docDetails = JSON.parse(msg.d.DocDetails);
                annotations = JSON.parse(msg.d.AnnotationsJSON);
                eSignTags = JSON.parse(msg.d.ESignTags);

                if (docDetails.ImageStatus != 2) {
                    // Document is not ready in editor.
                    alert('Document is not ready. Please try again later.');
                    $documentList.removeClass('selecting');
                    $('#guid_' + CurrentDocument.DocumentId).addClass('selected')

                } else {
                    // Document has all images.
                    if (!EnableThumbDocHeader) pages[0].Name = null;

                    setSelectedDoc(docDetails, pages, annotations, eSignTags);
                    isSwitchSuccess = true
                }
            },
            function(msg) {
                alert("There was an unexpected error switching documents. Please refresh the loan file.");
            }
        ).then(function() {
            changingDocs = false;

            if (isSwitchSuccess) {
                AfterDocSwitch(docDetails, pages, annotations);
            }
        });

    }

    function appendToDoc(documentId, pages) {
        var appendInfo = {
            AppendTo: documentId,
            PagesToAppend: pages
        };
        var DTO = {
            'docsToAppend': [appendInfo]
        };
        var success = false;
        callWebMethodAsync({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: 'EditEDocService.aspx/AppendPages',
            data: JSON.stringify(DTO),
            dataType: 'json',
            async: false
        }).then(
            function(msg) {
                success = true;
            },
            function(msg) {
                alert("There was an unexpected error appending pages. Please try again later.");
            }
        );

        return success;
    }

    function setSelectedDoc(doc, pages, annotations, eSignTags) {
        CurrentDocument = doc;

        //update navigation
        var id, $item, docid = doc.DocumentId;
        $documentList.removeClass('selecting');
        $('#guid_' + docid).addClass('selected').removeClass('Hidden');

        //populate doc info
        controls.PublicDescription.val(doc.Description);
        controls.InternalDescription.val(doc.InternalDescription);
        controls.RejectedReason.val(doc.DocumentStatusReasonDescription);
        controls.Status.val(doc.DocumentStatus)

        if (doc.DocumentStatus == 3) { //rejected
            controls.RejectedReason.show();
            controls.RejectedReasonContainer.show();
        }
        else {
            controls.RejectedReason.hide();
            controls.RejectedReasonContainer.hide();
        }

        pdfEditor.Refresh(pages);
        SetupAnnotations(annotations, eSignTags);
    }

    $(window).resize(function() {
        var padding = HeaderPadding;
        if ($HeaderElement) padding += $HeaderElement.height();
        $Navigation.height($(window).height() - padding);
    });

    $Navigation.on('click', 'ol.document-name-listing > li', function(e) {
        e.stopImmediatePropagation();
        changeActiveDoc($(this));
    }).on('click', 'li.folder', function() {
        var $li = $(this);
        $li.toggleClass('close');
    }).on('click', '.open-pdf', function(e) {
        e.stopImmediatePropagation();
        ViewDoc($(this).closest('li').data('docid'));
    });
});
