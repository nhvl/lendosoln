﻿var AuthorizationDocPicker = (function () {
    var docPickerTemplate;
    var docChangeCallback;

    jQuery(function ($) {
        $.get(ML.VirtualRoot + "/newlos/Common/AuthorizationDocPicker.html", function (data) {
            docPickerTemplate = $.template(data);
        });

        $('body').on('click', '.UploadDocLink', function () {
            var docPickerRow = $(this).closest('tr');
            var appId = docPickerRow.find('.AppId').val();
            LQBPopup.Show(ML.VirtualRoot + '/newlos/ElectronicDocs/SingleDocumentUploader.aspx?loanid=' + ML.sLId + '&appid=' + appId, {
                onReturn: function (returnArgs) {
                    if (typeof (returnArgs) != 'undefined' && returnArgs != null && returnArgs.OK) {
                        UpdatePickerUI(docPickerRow[0], returnArgs.Document);
                    }
                },
                hideCloseButton: true
            });
        });

        $('body').on('click', '.SelectDocLink', function () {
            var docPickerRow = $(this).closest('tr');
            LQBPopup.Show(ML.VirtualRoot + '/newlos/ElectronicDocs/SingleDocumentPicker.aspx?nor=t&loanid=' + ML.sLId, {
                onReturn: function (returnArgs) {
                    if (typeof (returnArgs) != 'undefined' && returnArgs != null && returnArgs.OK) {
                        UpdatePickerUI(docPickerRow, returnArgs.Document);
                    }
                },
                hideCloseButton: true
            });
        });

        $('body').on('click', '.ClearDocLink', function () {
            var docPickerRow = $(this).closest('tr');
            UpdatePickerUI(docPickerRow, null);
        });

        $('body').on('click', '.ViewDocLink', function () {
            var docId = $(this).closest('tr').find('.DocId').val();
            window.open(ML.EDocsRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx?docid=" + docId, '_self');
        });

        function UpdatePickerUI(pickerRow, document) {
            if (typeof (document) == 'undefined' || document == null) {
                $(pickerRow).find('.DocId').val('');
                $(pickerRow).find('.DocDescription').val('');
                $(pickerRow).find('.DocUploaded').hide();
                $(pickerRow).find('.NoDoc').show();
            }
            else {
                var docId = document.DocumentId;
                var docDescription = document.Description;
                $(pickerRow).find('.DocId').val(docId);
                $(pickerRow).find('.DocDescription').val(docDescription);
                $(pickerRow).find('.DocUploaded').show();
                $(pickerRow).find('.NoDoc').hide();
            }

            docChangeCallback();
        }
    });

    function _createDocPicker(borrowerName, appId, borrowerType, key) {
        var data = {
            BorrowerName: borrowerName,
            AppId: appId,
            BorrowerType: borrowerType,
            Key: key
        };

        var newPicker = $.tmpl(docPickerTemplate, data);
        return newPicker;
    }

    function _removeDocPicker(key, container) {
        if (key === null || key === '') {
            $(container).find('.DocPickerRow').remove();
        }
        else {
            $(container).find('.Key[value="' + key + '"]').closest('tr').remove();
        }
    }

    function _containsDocPicker(key, container) {
        if (key === null || key === '') {
            return $(container).find('.DocPickerRow').length > 0;
        }
        else {
            return $(container).find('.Key[value="' + key + '"]').length > 0;
        }
    }

    function _retrieveAllDocPickerInfo(container) {
        var info = [];
        $(container).find('.DocPickerRow').each(function () {
            var docId = $(this).find('.DocId').val();
            var appId = $(this).find('.AppId').val();
            var borrowerType = $(this).find('.BorrowerType').val();
            info.push(docId + '|' + appId + '|' + borrowerType);
        });

        return info;
    }

    function _allPickersHaveDocs() {
        return $('.DocPickerRow .DocId').filter(function () { return $(this).val() == ''; }).length === 0;
    }

    function _registerDocChangeCallback(callback) {
        docChangeCallback = callback;
    }

    return {
        CreateDocPicker: _createDocPicker,
        RemoveDocPicker: _removeDocPicker,
        ContainsDocPicker: _containsDocPicker,
        RetrieveAllDocPickerInfo: _retrieveAllDocPickerInfo,
        AllPickersHaveDocs: _allPickersHaveDocs,
        RegisterDocChangeCallback: _registerDocChangeCallback
    };
})();



