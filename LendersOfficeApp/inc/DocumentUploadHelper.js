﻿var DocumentUploadHelper = (function ($) {
    function _upload(requestType, file, metadataDictionary, callback) {
        var formData = new FormData();
        formData.append('UploadedDocument', file, file.name);

        if (metadataDictionary && typeof metadataDictionary === 'object') {
            for (var key in metadataDictionary) {
                formData.append(key, metadataDictionary[key]);
            }
        }

        var ajaxSettings = {
            async: true,
            cache: false,
            contentType: false,
            data: formData,
            processData: false,
            type: "POST",
            url: ML.VirtualRoot + '/DocumentUpload.aspx?requestType=' + requestType
        };

        $.ajax(ajaxSettings).then(
            function (data) { _successCallback(data, callback); },
            function () { _errorCallback(callback); });
    }

    function _successCallback(data, callback) {
        var result;
        if (data) {
            result = JSON.parse(data);
        }
        else {
            result = { error: false };
        }

        callback(result);
    }

    function _errorCallback(callback) {
        var result = { error: true, UserMessage: 'System error. Please contact us if this happens again.' };
        callback(result);
    }

    return {
        Upload: _upload
    }
})(jQuery);