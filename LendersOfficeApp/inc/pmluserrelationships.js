﻿var PMLUserRelationships = (function($) {
    var EMPTY_GUID = '00000000-0000-0000-0000-000000000000';
    var bpRelationshipCleared = 'The previous Processor (External) relationship was cleared.';
    var externalPostCloserRelationshipCleared = 'The previous Post-Closer (External) relationship was cleared.';
    var $relationshipTabs;

    $(document).ready(function() {
        $relationshipTabs = $('table.pml-user-relationships');
    });

    function updateRelationships(relationshipUpdates) {
        var alertMessages = [], relationshipUpdate, $divs, i;

        for (i = 0; i < relationshipUpdates.length; i++) {
            relationshipUpdate = relationshipUpdates[i];

            $divs = $(relationshipUpdate.selector).children('td:last-child')
                .children('div');

            if ($divs.length === 0) {
                continue;
            } else {
                $divs.each(function(index, element) {
                    var addMessage, hasValidSetting, $links;
                    
                    if (relationshipUpdate.isClear) {
                        hasValidSetting = $(this).children('input:last').val() !== EMPTY_GUID;
                        addMessage = $.inArray(relationshipUpdate.clearMessage, alertMessages) === -1;

                        if (hasValidSetting && addMessage) {
                            alertMessages.push(relationshipUpdate.clearMessage);
                        }

                        onChooserFixClick(this, 'None', EMPTY_GUID, false);
                    }

                    $links = $(this).find('a');

                    $links.each(function(index, element) {
                        this.disabled = relationshipUpdate.isDisable;
                        this.style.cursor = relationshipUpdate.isDisable ? 'default' : 'hand';
                    });
                });
            }
        }

        if (alertMessages.length > 0) {
            alert('Note - ' + alertMessages.join(' '));
        }
    }

    function onOriginatingCompanyChange() {
        var relationshipsToUpdate = [];

        relationshipsToUpdate.push({
            selector: 'tr.broker-processor',
            isClear: true,
            clearMessage: bpRelationshipCleared,
            isDisable: false
        });

        relationshipsToUpdate.push({
            selector: 'tr.external-post-closer',
            isClear: true,
            clearMessage: externalPostCloserRelationshipCleared,
            isDisable: false
        });

        updateRelationships(relationshipsToUpdate);
    }

    function onRolesChange(isBrokerProcessor, isExternalPostCloser) {
        var relationshipsToUpdate = [];

        relationshipsToUpdate.push({
            selector: 'tr.broker-processor',
            isClear: isBrokerProcessor,
            clearMessage: bpRelationshipCleared,
            isDisable: isBrokerProcessor
        });

        relationshipsToUpdate.push({
            selector: 'tr.external-post-closer',
            isClear: isExternalPostCloser,
            clearMessage: externalPostCloserRelationshipCleared,
            isDisable: isExternalPostCloser
        });

        updateRelationships(relationshipsToUpdate);
    }

    function getBranchIDForMode(mode) {
        var branchID, $tab;

        $tab = $relationshipTabs.filter('.' + mode);

        if ($tab.length === 0) {
            throw 'Invalid mode';
        }

        return $('tr.branch', $tab).find('select').val();
    }

    function getPriceGroupIDForMode(mode) {
        var priceGroupID, $tab;

        $tab = $relationshipTabs.filter('.' + mode);

        if ($tab.length === 0) {
            throw 'Invalid mode';
        }

        return $('tr.price-group', $tab).find('select').val();
    }

    function prePostbackClientSideSave() {
        if (typeof saveAllLicenses === 'function') {
            saveAllLicenses();
        }
    }

    return {
        getBranchIDForMode: getBranchIDForMode,
        getPriceGroupIDForMode: getPriceGroupIDForMode,
        onOriginatingCompanyChange: onOriginatingCompanyChange,
        onRolesChange: onRolesChange,
        prePostbackClientSideSave: prePostbackClientSideSave
    };
}(jQuery));