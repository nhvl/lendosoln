﻿(function ($) {

    function toggleTextBoxReadOnly(checkbox) {
        var $textbox = $(checkbox).siblings('input[type="text"]');
        $textbox.prop('readonly', !checkbox.checked);
    }

    function clearRespaEnteredDate(fieldIdToClear, applicationId, textbox) {
        var args = {
            LoanId: ML.sLId,
            FieldIdToClear: fieldIdToClear,
            ApplicationId: applicationId,
            ClientId: ML.RespaDatesClientId
        };

        gService.utils.callAsyncSimple('ClearRespaEnteredDate', args, function (result) {
            if (result && result.error) {
                alert(result.UserMessage || 'Unable to clear entered date.');
            } else if (result.value) {
                populateForm(result.value, ML.RespaDatesClientId);
                textbox.value = '';
            }
        });
    }

    $(document).ready(function () {

        $('fieldset.RespaDates input[type="checkbox"]').click(function () {
            toggleTextBoxReadOnly(this);
            refreshCalculation();
        }).each(function(index, element) {
            // After attaching the event handler, we need to make the appropriate
            // textboxes readonly without triggering a background calculation.
            toggleTextBoxReadOnly(this);
        });

        $('fieldset.RespaDates input[type="text"][id$="CollectedD"]').change(function () {
            date_onblur(null, this);
            refreshCalculation();
        });

        $('fieldset.RespaDates input[type="button"]').click(function () {
            if (isDirty()) {
                alert('Please save changes prior to clearing the Entered date');
                return;
            }

            var $fieldToClear = $(this)
                .parent() // Containing cell.
                .parent() // Containing row.
                .find('input[type="text"][id$="EnteredD"]');
            var fieldIdToClear = $fieldToClear.attr('data-field-id');
            var applicationId = $(this).attr('data-application-id');

            clearRespaEnteredDate(fieldIdToClear, applicationId, $fieldToClear[0]);
        });
    });
})(jQuery);