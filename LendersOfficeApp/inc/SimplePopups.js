﻿var SimplePopups = (function () {
    var loadingTemplate;
    var alertTemplate;
    var errorTemplate;

    jQuery(function ($) {
        $.get(ML.VirtualRoot + "/newlos/Common/Loading.html", function (data) {
            loadingTemplate = $.template(data);
        });

        $.get(ML.VirtualRoot + "/newlos/Common/Alert.html", function (data) {
            alertTemplate = $.template(data);
        });

        $.get(ML.VirtualRoot + "/newlos/Common/Error.html", function (data) {
            errorTemplate = $.template(data);
        });

        $('body').delegate('.AdditionalDetailsLink', 'click', function () {
            $(this).parent().hide();
            $(this).closest('.ErrorDetails').find('.ErrorList').removeClass('Hidden');
        });
    });

    function _createLoadingPopup(message, header) {
        var showHeader = typeof (header) === 'string';
        return $.tmpl(loadingTemplate, { Message: message, ShowHeader: showHeader, Header: header })
            .find('img')
            .attr('src', VRoot + '/images/loading.gif')
            .end();
    }

    function _createAlertPopup(message, submessage, imageUrl, header) {
        var showHeader = typeof (header) === 'string';
        var popup = $.tmpl(alertTemplate, { Message: message, Submessage: submessage, ImageUrl: imageUrl, ShowHeader: showHeader, Header: header });
        return popup;
    }

    function _createErrorPopup(message, errors, header) {
        var showHeader = typeof (header) === 'string';
        var popup = $.tmpl(errorTemplate, { Message: message, VOARequestErrors: errors, ShowHeader: showHeader, Header: header });
        return popup;
    }

    return {
        CreateLoadingPopup: _createLoadingPopup,
        CreateAlertPopup: _createAlertPopup,
        CreateErrorPopup: _createErrorPopup
    };
})();