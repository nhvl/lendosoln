﻿if (typeof (LQBMortgagePools) == 'undefined') {
    LQBMortgagePools = {};
    alert("Base script not found, some functionality may not work");
} 
jQuery(function($) {

    if (!ML) {
        LQBMortgagePools.trace("ML not found, links won't show up");
        ML = {};
    }

    setupLoanTable();
    setupPagination();

    function getLoanId(elem) {
        return $(elem).closest('tr').find('.LoanId').val();
    }

    function setupLoanTable() {
        $('table.Loans').each(function() {
            if ($('thead th', this).length == 0) {
                $(this).hide();
                return;
            }

            LQBMortgagePools.SetupLoanTable(this, getLoanId,
            {
                SetupTablesorter: true,
                EditLoanBaseUrl: ML.LoanEditorUrl || false,
                ViewLoanBaseUrl: ML.LoanViewerUrl || false,
                NewTaskBaseUrl: ML.TaskEditorUrl || false,
                ShowSelectionCol: ML.ShowSelectionCol || false
            });
        });
    }

    function setupPagination() {
        var setPage = /([\&?]Page=)\d+/;
        $('#Pagination span.Action').click(function() {
            var result;
            var location = window.location.href;
            if (setPage.test(location)) {
                result = location.replace(setPage, "$1" + $(this).text());
            }
            else {
                var separator = '&';
                if (location.indexOf("?") == -1) separator = '?';
                result = location + separator + "Page=" + $(this).text();
            }

            if (typeof (LQBMortgagePools.OnPageSelect) == "function") {
                LQBMortgagePools.OnPageSelect(this, result);
                return;
            }

            window.location = result;
        });
    }
});


LQBMortgagePools.SetupLoanTable = (function($) {
    var contextMenuLayout = {
        'EditLoanBaseUrl': {
            Html: '<li>edit</li>',
            Fxn: editLoan
        },
        'ViewLoanBaseUrl': {
            Html: '<li>view</li>',
            Fxn: viewLoan
        },
        'NewTaskBaseUrl': {
            Html: '<li>new task</li>',
            Fxn: newTask
        }
    };

    var viewOptions = 'height=550px,width=750px,toolbar=no,menubar=no,location=no,status=no,resizable=yes';
    var taskOptions = 'width=750,height=400,center=yes,resizable=yes,scrollbars=yes,status=yes,help=no';

    return function SetupLoanTable(Table, GetLoanId, Settings) {
        if (typeof (Settings) == 'undefined') Settings = {};

        if (Settings.SetupTablesorter && $('tbody', Table).children().length != 0) {
            SetupTableSorter(Table);
        }

        var contextMenu = CreateContextMenu(Settings, GetLoanId);
        AttachContextMenu(Table, contextMenu);

        if (Settings.ShowSelectionCol) {
            $('.LoanSelectionCol input', Table).click(function() {
                var parent = $(this).closest('tr');
                parent.toggleClass("Selected", $(this).is(':checked'));
            });
            $('.LoanSelectionCol', Table).show();
        }

        $(Table).data("GetLoanId", GetLoanId);
    }

    function viewLoan(id, baseViewUrl, viewWindows) {
        var windowName = id.replace(/-/g, '');
        var url = baseViewUrl + id + '&display=t';

        openOrFocus(windowName, viewWindows, url, viewOptions);
    }

    function editLoan(id, baseEditUrl, editWindows) {
        var windowName = id.replace(/-/g, '');
        var w = screen.availWidth - 10;
        var h = screen.availHeight - 50;
        var options = 'width=' + w + 'px,height=' + h + 'px,left=0px,top=0px,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes';
        var url = baseEditUrl + id;

        openOrFocus(windowName, editWindows, url, options);
    }

    function newTask(id, baseTaskUrl, taskWindows) {
        var url = baseTaskUrl + id;
        var windowName = id.replace(/-/g, '');

        openOrFocus(windowName, taskWindows, url, taskOptions);
    }

    function openOrFocus(name, lookIn, url, options) {
        if (lookIn[name] && !lookIn[name].closed) lookIn[name].focus();
        else lookIn[name] = window.open(url, name, options);
    }

    function AttachContextMenu(Table, contextMenu) {
        //td:first is the checkbox column, we want the one after it
        $('tbody tr', Table).find('td:eq(1)').addClass('Action').append(contextMenu).click(function() {
            var contextEntries = $('.Context li', this);
            if (contextEntries.length == 1) {
                contextEntries.trigger('click');
            }
            else {
                $(this).closest('tr').toggleClass('Opened');
                $('.Context', this).toggle();
            }
        });
    }

    function CreateContextMenu(Settings, GetLoanId) {
        var contextMenu = $('<ul class="Context">');

        $.each(Settings, function(key, baseUrl) {
            if (!baseUrl) return;
            if (!key in contextMenuLayout) return;

            var menuEntry = contextMenuLayout[key];
            if (!menuEntry) return;

            var windows = {};
            contextMenu.append($(menuEntry.Html).click(function() {
                menuEntry.Fxn(GetLoanId(this), baseUrl, windows);
                return false;
            }));
        });

        contextMenu.hide();
        return contextMenu;
    }

    function SetupTableSorter(Table) {
        $.tablesorter.defaults.widgetZebra = {
            css: ["GridItem", "GridAlternatingItem"]
        };

        var sorting = [[0, 0]];
        $(Table).tablesorter(
            {
                sortList: sorting,
                widgets: ['zebra']
            });

        $('th', Table).addClass('Action');
        $(Table).on("UpdateTableSorter", function() {
            $(this).trigger("update");
            $(this).trigger("sorton", [$(this)[0].config.sortList]);
        });
    }
})(jQuery);

LQBMortgagePools.GetSelectedLoans = (function($) {
    return function GetSelectedLoans(context) {
        context = context || document;
        var selected = [];
        $('table.Loans', context).each(function() {
            var GetLoanId = $(this).data("GetLoanId");
            $('tr.Selected', this).each(function() {
                selected.push({
                    Id: GetLoanId(this),
                    Row: this
                });
            });
        });

        return selected;
    }
})(jQuery);
