﻿var loanCreationArgs = {
    isLead: false,
    isTest: false,
    purpose: ""
};

var $dialog;

function setLoanCreationArgs(isLead, isTest, purpose) {
    loanCreationArgs.isLead = isLead;
    loanCreationArgs.isTest = isTest;
    loanCreationArgs.purpose = purpose;
}

function updateTemplateText(fileType) {

    $dialog.find(".blank-link").text("Create Blank " + fileType + " file");
    $dialog.find(".template-header").text("Create " + fileType + " Using a Template: ")
}

function openTemplateDialog(fileType, isLoadingOnly) {
    $("body").addClass("prevent-scroll");
    $("#ZoomStyle").remove();

    $dialog = $(".loan-templates-dialog");

    $dialog.removeClass("show-templates");
    $dialog.addClass("loading");
    $dialog.removeClass("no-templates");

    var $window = $(window);
    if ($window.height() < 500) {
        var newHeight = "height: " + ($window.height() - 95) + "px !important;";
        $("<style id='ZoomStyle'> div.loan-templates-dialog.show-templates { " + newHeight + " } div.template-container { height: 80% !important } </style>")
            .appendTo("head");
    }

    var buttons = [];
    var title = "Loading";
    var width = 200;

    if (!isLoadingOnly) {
        updateTemplateText(fileType);
        $dialog.addClass("show-templates");
        title = fileType + " File Creation";
        width = 400;
        buttons = [{
            text: "Close",
            click: function () {
                $(this).dialog("close");
            },
            id: "closeLoanCreateBtn"
        }];
    }

    $dialog.dialog({
        width: width,
        modal: true,
        resizable: false,
        closeOnEscape: false,
        draggable: false,
        title: title,
        buttons: buttons,
        close: function () {
            $("body").removeClass("prevent-scroll");
        }
    });

    $("#closeLoanCreateBtn").blur();
}

function retrieveTemplates(args, fileType) {
    loanCreationArgs = args;
    gService.loanTemplates.callAsync("RetrieveTemplates", args, true, false, true, true, successfulTemplateRetrieval, errorTemplateRetrieval);
    openTemplateDialog(args.purpose + " " + fileType);
}

function successfulTemplateRetrieval(result) {
    $dialog.removeClass("loading");
    $dialog.removeClass("error");
    var templates = JSON.parse(result.value.templates);
    renderTemplates(templates);
}

function errorTemplateRetrieval(result) {
    $dialog.removeClass("loading");
    renderTemplates([]);
    renderError(result.UserMessage);
}

function renderError(error) {

    $(".blank-link").hide();
    $dialog.removeClass("no-templates");
    $dialog.removeClass("loading");
    $dialog.addClass("error");
    $(".template-container")[0].innerHTML = "";
    $(".template-container").text(error);
}

function renderTemplates(templates) {
    $(".blank-link").toggle(!ML.IsBlankTemplateInvisible);
    var templateHtml = "";

    if (templates.length == 0 && ML.IsBlankTemplateInvisible) {
        $dialog.addClass("no-templates");
    } else {

        for (i = 0; i < templates.length; i++) {
            templateHtml += "<a href='' data-id='" + templates[i].TemplateId + "'>" + encodeValue(templates[i].TemplateName) + "</a> <br>";
        }
    }

    $dialog.removeClass("loading");
    var $templateContainer = $(".template-container")
    $templateContainer[0].innerHTML = templateHtml;
    $templateContainer.scrollTop(0);
}

function createLoanWithArgs(args) {
    $(".ui-dialog button").prop("disabled", true);
    $(".loan-templates-dialog").prev().find(".ui-dialog-titlebar-close").hide();
    loanCreationArgs = args;
    gService.loanCreation.callAsync("CreateLoan", args, false, true, true, true, successfulLoanCreation, errorLoanCreation);
}

function createLoanFromLink(e) {
    $dialog.addClass("loading");
    var link = $(this);
    loanCreationArgs.templateId = link.data("id");

    createLoanWithArgs(loanCreationArgs);
    return false;
}

function successfulLoanCreation(result) {
    $dialog.removeClass("loading");
    $dialog.prev().find(".ui-dialog-titlebar-close").show();
    $(".ui-dialog button").prop("disabled", false);
    $dialog.dialog("close");
    var loanId = result.value.loanId;
    if (loanCreationArgs.isLead && !ML.IsEditLeadsInFullLoanEditor) {

        window.open(ML.VirtualRoot + "/los/lead/leadmainframe.aspx?loanid=" + loanId + "&isnew=t&isResize=t", "_blank", "height=400,width=400,scrollbars=yes,resizable=1,menubar=no,toolbar=no", false);
    }
    else {
        window.open(ML.VirtualRoot + "/newlos/loanapp.aspx?loanid=" + loanId + "&isnew=t&isResize=t", "_blank", "height=400,width=400,scrollbars=yes,resizable=1,menubar=no,toolbar=no", false);
    }
}

function errorLoanCreation(result) {
    $dialog.removeClass("loading");
    $dialog.prev().find(".ui-dialog-titlebar-close").show();
    $(".ui-dialog button").prop("disabled", false);
    alert(result.UserMessage || "System error.  Please contact us if this happens again.");
}
