﻿function processHiddenCmd() {
    var op = document.getElementById('m_hiddenCmd').value;
    if (op == 'selectParameter' || op == 'editParameter' || op == "saveParameterFailed" || op == "selectPathProperty" || op == "selectPathPropertyLevel") {
        Modal.ShowPopup('ParameterPickerPopup', null, null,  100, 500); // utilities.js:Modal
        document.getElementById('m_parameterPicker').style.display = 'none';
        document.getElementById('m_parameterEditor').style.display = '';
        setValueType(document.getElementById("m_hiddenEditorMode").value);
    }
    else if (op == 'pickParameter' || op == 'addParameter') {
        Modal.ShowPopup('ParameterPickerPopup', null, null,  100, 500);
        document.getElementById('m_parameterPicker').style.display = '';
        document.getElementById('m_parameterEditor').style.display = 'none';
    }
    else if (op == 'pickProtectFields') {
        document.getElementById('m_editFieldsBtn').value = "Please wait. Loading fields...";
        Modal.ShowPopup('ProtectedFieldsPopup', null, null,  100, 500);
        document.getElementById('m_editFieldsBtn').value = "Edit Fields";
    }
    else if (op == 'pickWriteFields') {
        document.getElementById('m_editFieldsBtn').value = "Please wait. Loading fields...";
        Modal.ShowPopup('WritableFieldsPopup', null, null,  100, 500);
        document.getElementById('m_editFieldsBtn').value = "Edit Fields";
    }
    else if (op == "saveConfigItem") {
        if (typeof(triggerParentPostback) == "function")
        {
            triggerParentPostback();
        }

        window.close();
    }
    else if (op == "saveWritableCollection" || "removeWritableCollection") {
        clearPathVars();
    }
}

function onEditParameter(name) {
    document.getElementById('m_hiddenParameterName').value = name;
    document.getElementById('m_hiddenCmd').value = 'editParameter';
    performPostback();
}

function onSelectPathProperty() {
    document.getElementById('m_hiddenCmd').value = 'selectPathProperty';
    performPostback();
}

function onSelectPathPropertyLevel() {
    document.getElementById('m_hiddenCmd').value = 'selectPathPropertyLevel';
    performPostback();
}

function onAddParameter() {
    document.getElementById('m_hiddenCmd').value = 'addParameter';
    performPostback();
}

function onVariableSelect(name) {
    document.getElementById('m_valueVar').innerText = name + '  ';
    document.getElementById('m_hiddenVariableName').value = name;
    
    document.getElementById('m_parameterPicker').style.display = 'none';
    document.getElementById('m_parameterEditor').style.display = '';
    setValueType('var');
}

function onSaveConfigItem() {
    document.getElementById('m_hiddenCmd').value = 'saveConfigItem';
    performPostback();
}

function onSaveParameter() {
    document.getElementById('m_hiddenCmd').value = 'saveParameter';
    performPostback();
}

function onPickWritableField() {
    document.getElementById('m_hiddenCmd').value = 'pickWriteFields';
    document.getElementById('m_editFieldsBtn').disabled = true;
    document.getElementById('m_editFieldsBtn').value = "Please wait. Loading fields...";
    performPostback();
}

function onPickWritableCollection() {
    clearPathableFieldPermissionsDialog();
    Modal.ShowPopup('pathableFieldPermissionsEditor', null, null, 100, 500);
}

function onSaveWritableFields() {
    document.getElementById('m_hiddenCmd').value = 'saveWriteFields';
    performPostback();
}

function onSaveWritableCollection() {
    document.getElementById('m_hiddenCmd').value = 'saveWritableCollection';
    performPostback();
}

function onRemoveWritableCollection(path) {
    document.getElementById('m_hiddenCmd').value = 'removeWritableCollection';
    document.getElementById('PathableFieldPermissionsEditor_Path').value = path;
    performPostback();
}

function onPickProtectedField() {
    document.getElementById('m_hiddenCmd').value = 'pickProtectFields';
    document.getElementById('m_editFieldsBtn').disabled = true;
    document.getElementById('m_editFieldsBtn').value = "Please wait. Loading fields...";
    performPostback();
}

function onSaveProtectedFields() {
    document.getElementById('m_hiddenCmd').value = 'saveProtectFields';
    performPostback();
}

function onPickVariable() {
    document.getElementById('m_hiddenCmd').value = 'pickParameter';
    performPostback();
}

function performPostback() {
    __doPostBack('', '');
}

function setValueType(type) {
    document.getElementById("m_hiddenEditorMode").value = type;
    document.getElementById('m_valueTextbox1').style.display = 'none';
    document.getElementById('m_valueTextbox2').style.display = 'none';
    document.getElementById('m_valueVar').style.display = 'none';
    document.getElementById('m_valueVarEdit').style.display = 'none';
    document.getElementById('m_valueTrueSpan').style.display = 'none';
    document.getElementById('m_valueFalseSpan').style.display = 'none';
    document.getElementById('m_valueConditionCategory').style.display = 'none';
    document.getElementById('m_valueDesc').style.display = 'none';
    document.getElementById('m_operationNDaysFromTodayOperator').style.display = 'none';
    if (type == 'textbox') {
        document.getElementById('m_valueTextboxRb').checked = 'true';
        document.getElementById('m_valueTextbox1').style.display = '';
        document.getElementById('m_valueTextbox2').style.display = document.getElementById('m_operationBetween').checked ? '' : 'none';
    } else if (type == 'var') {
        document.getElementById('m_valueVarRb').checked = 'true';
        document.getElementById('m_valueVar').style.display = '';
        document.getElementById('m_valueVarEdit').style.display = '';
    } else if (type == 'bool') {
        document.getElementById('m_valueBoolRb').checked = 'true';
        document.getElementById('m_valueTrueSpan').style.display = '';
        document.getElementById('m_valueFalseSpan').style.display = '';
    } else if (type == 'conditionCategory') {
        // make the category dropdown visible
        document.getElementById('m_valueConditionCategory').style.display = '';
    } else if (type == 'NDaysFromToday') {
        document.getElementById('m_valueTextboxRb').checked = 'true';
        document.getElementById('m_valueDesc').style.display = '';
        document.getElementById('m_valueDesc').innerHTML = 'Today';
        document.getElementById('m_operationNDaysFromTodayOperator').style.display = '';
        document.getElementById('m_valueTextbox1').style.display = '';
    }
    
    validateEditor();
}

function validateEditor() {
    var type = document.getElementById("m_hiddenEditorMode").value;
    var valid = true;
    if (type == 'textbox') {
        valid = document.getElementById('m_valueTextbox1').value != '';
        if (document.getElementById('m_operationBetween').checked)
            valid &= document.getElementById('m_valueTextbox2').value != '';
    }
    else if (type == 'var')
        valid = document.getElementById('m_valueVar').innerText != '';
    else if (type == 'bool')
        valid = document.getElementById('m_valueTrue').checked || document.getElementById('m_valueFalse').checked;
    
    document.getElementById('m_editorSaveBtn').disabled = !valid;
}


function createSelectionList($searchCriteriaInput, $categoryListDDL, $pageListDDL, $fieldListDiv) {
    return {
        $searchCriteriaInput: $searchCriteriaInput,
        $categoryListDDL: $categoryListDDL,
        $pageListDDL: $pageListDDL,
        $fieldListDiv: $fieldListDiv,
        fieldList: createFieldList($fieldListDiv.children('div'))
    };
}

function createFieldList(fieldListDivs) {
    var fieldItems = [];
    for (var i = 0; i < fieldListDivs.length; ++i) {
        var item = fieldListDivs[i];
        fieldItems.push({
            element: item,
            lowercaseValue: item.innerText.toLowerCase(),
            category: item.getAttribute('category'),
            fid: item.getAttribute('fid')
        });
    }
    return fieldItems;
}

// FIELD SEARCH
function displaySearchResults(ignoreCriteria, selectionList) {
    if (ignoreCriteria)
        selectionList.$searchCriteriaInput.val('');
    var search = selectionList.$searchCriteriaInput.val().toLowerCase();
    var list = selectionList.fieldList;
    for (var i = 0; i < list.length; i++) {
        var item = list[i];
        item.element.style.display = item.lowercaseValue.indexOf(search) != -1 ? '' : 'none';
    }
}

// FIELD CATEGORY
function defaultCategory(selectionList) {
    selectionList.$categoryListDDL.prop('selectedIndex', 0);
    selectionList.$fieldListDiv.scrollTop(0);
    selectionList.$pageListDDL.prop('selectedIndex', 0);
}

function jumpToSelected(selectionList) {
    displaySearchResults(true, selectionList);
    selectionList.$pageListDDL.prop('selectedIndex', 0);
    var selection = selectionList.$categoryListDDL.val();
    var list = selectionList.fieldList;
    for (var i = 0; i < list.length; i++) {
        var item = list[i];
        if (item.category == selection) {
            selectionList.$fieldListDiv.scrollTop(item.element.offsetTop);
            return;
        }
    }
}

// FIELD PAGE
function jumpToPage(selectionList) {
    // Clear out the other search boxes
    selectionList.$searchCriteriaInput.val('');
    selectionList.$categoryListDDL.prop('selectedIndex', 0);

    var selection = selectionList.$pageListDDL.val(); // The selected page
    var fields = PageFields[selection]; // Fields in the page

    var list = selectionList.fieldList; // The list of fields
    for (var i = 0; i < list.length; i++) {
        var field = list[i];
        // WARNING: one of the comments on the jQuery docs states that jQuery.inArray returns undefined in ie7
        if( !fields || $.inArray(field.fid, fields) > -1 ) { // Check if the fid is one of the fields in the page
            field.element.style.display = '';
        }
        else {
            field.element.style.display = 'none';
        }
    }
}

// WRITE/PROTECT COLLECTION SCRIPTS
function clearPathableFieldPermissionsDialog() {
    var collectionDropdown = $("#pathPermissionsCollectionDropdown");
    var subsetDropdown = $("#pathPermissionsSubsetDropdown");
    var recordDropdown = $("#pathPermissionsRecordDropDown");
    var propertyDropdown = $("#pathPermissionsPropertyDropdown");

    // Clear out drop down lists.
    collectionDropdown.empty();
    subsetDropdown.empty();
    recordDropdown.empty();
    propertyDropdown.empty();

    // Clear radio button selection.
    $("input[type=radio][name='pathPermissionsPropertyLevel']").prop("checked", false);

    // Hide radio list and drop downs until we select a collection.
    $("#pathPermissionsPropertyLevelRow").hide();
    $("#pathPermissionsSubsetRow").hide();
    $("#pathPermissionsRecordRow").hide();
    $("#pathPermissionsPropertyRow").hide();

    // Load collection dropdown.
    collectionDropdown.append("<option value=''></option>");
    collectionDropdown.append($.map(PathableSchemaModel, function(item) {
        return hypescriptDom("option", {value:item.Name}, item.Name);
    }));

    clearPathVars();
    setSaveEnabled();
}

function clearPathVars() {
    $("#pathPermissions_pathSoFar").text("");
    $("#PathableFieldPermissionsEditor_Path").val("");
    $("#PathableFieldPermissionsEditor_FriendlyPath").val("");
}

function setSaveEnabled() {
    var saveBtn = $("#pathPermissionsSaveBtn");
    var subsetDropdown = $("#pathPermissionsSubsetDropdown");
    var recordDropdown = $("#pathPermissionsRecordDropDown");
    var selected = $("input[type=radio][name='pathPermissionsPropertyLevel']:checked");

    // Disable save button if record or subset not selected.
    saveBtn.prop("disabled", selected.length == 0 || (selected.val() == "INDIVIDUAL" && !recordDropdown.val())
                                || (selected.val() == "SUBSET" && !subsetDropdown.val()))
}

function setupPropertyLevelRadioList(collectionModel) {
    // Set radio button list.
    var propertyLevelRow = $("#pathPermissionsPropertyLevelRow");
    var individualRadio = $("#pathPermissionsPropertyLevel_individual");
    var subsetRadio = $("#pathPermissionsPropertyLevel_subset");
    var wholeRadio = $("#pathPermissionsPropertyLevel_whole");

    var hasRecords = collectionModel.RecordTypes.length > 0;
    var hasSubsets = collectionModel.Subsets.length > 0;

    // Set visibility
    individualRadio.toggle(hasRecords);
    subsetRadio.toggle(hasSubsets);
    propertyLevelRow.toggle(hasRecords || hasSubsets);

    // Pick new selected radio if selection is not valid.
    var selected = $("input[type=radio][name='pathPermissionsPropertyLevel']:checked");
    if (selected.length == 0 || !selected.is(":visible")) {
        // Select first visible option
        if (hasRecords) {
            individualRadio.prop("checked", true);
        } else if (hasSubsets) {
            subsetRadio.prop("checked", true);
        } else {
            wholeRadio.prop("checked", true);
        }
    }

    // Set dropdown visibilty based on selected radio button.
    $("input[type=radio][name='pathPermissionsPropertyLevel']:checked").change();
}

$(document).on("change", "#pathPermissionsCollectionDropdown",function () {
    var collectionDropdown = $("#pathPermissionsCollectionDropdown");
    var subsetDropdown = $("#pathPermissionsSubsetDropdown");
    var recordDropdown = $("#pathPermissionsRecordDropDown");
    var propertyDropdown = $("#pathPermissionsPropertyDropdown");

    // Reset dialog if blank option is selected.
    if (collectionDropdown.val() == "") {
        clearPathableFieldPermissionsDialog();
        return;
    }

    // Get schema model for collection.
    var collectionModel;
    var selectedCollection = collectionDropdown.find("option:selected");
    for (var i = 0; i < PathableSchemaModel.length; i++) {
        if (PathableSchemaModel[i].Name == selectedCollection.val()) {
            collectionModel = PathableSchemaModel[i];
        }
    }

    // Clear and re-set other dropdowns.
    subsetDropdown.empty();
    subsetDropdown.append("<option value=''></option>");
    subsetDropdown.append($.map(collectionModel.Subsets, function(item) {
        return hypescriptDom("option", {value:item}, item);
    }));

    recordDropdown.empty();
    recordDropdown.append("<option value=''></option>");
    recordDropdown.append($.map(collectionModel.RecordTypes, function(item) {
        return hypescriptDom("option", {value:item.TypeId}, item.FriendlyDescription);
    }));
    
    propertyDropdown.empty();
    propertyDropdown.append("<option value=''></option>");
    propertyDropdown.append($.map(collectionModel.Fields, function(item) {
        return hypescriptDom("option", {value:item}, item);
    }));

    setupPropertyLevelRadioList(collectionModel);
});

$(document).on("change", "input[type=radio][name='pathPermissionsPropertyLevel']", function () {
    var selected = $(this);
    var subsetRow = $("#pathPermissionsSubsetRow");
    var recordRow = $("#pathPermissionsRecordRow");
    var propertyRow = $("#pathPermissionsPropertyRow");
    var propertyDescription = $("#pathPermissionsPropertyDescription");

    propertyRow.show(); // Allways show property dropdown.

    switch (selected.val()) {
        case "INDIVIDUAL":
            subsetRow.hide();
            recordRow.show();
            propertyDescription.text("Record");
            break;
        case "SUBSET":
            subsetRow.show();
            recordRow.hide();
            propertyDescription.text("Subset");
            break;
        case "WHOLE":
            subsetRow.hide();
            recordRow.hide();
            propertyDescription.text("Collection");
            break;
    }
});

$(document).on("change", ".updatePath", function () {
    var collectionVal = $("#pathPermissionsCollectionDropdown").val();
    var subsetVal = $("#pathPermissionsSubsetDropdown").val();
    var recordVal = $("#pathPermissionsRecordDropDown").val();
    var recordText = $("#pathPermissionsRecordDropDown option:selected").text();
    var propertyVal = $("#pathPermissionsPropertyDropdown").val();

    var pathSoFarSpan = $("#pathPermissions_pathSoFar");
    var selected = $("input[type=radio][name='pathPermissionsPropertyLevel']:checked");

    var pathSoFar = "";
    var friendlyPathSoFar = "";
    if (collectionVal) {
        var pathElement = "[PATH]" + collectionVal;
        pathSoFar = pathElement;
        friendlyPathSoFar = pathElement;
    }

    switch (selected.val()) {
        case "INDIVIDUAL":

            if (recordVal) {
                pathSoFar += "[" + recordVal + "]";
                friendlyPathSoFar += "[" + recordText + "]";
            }
            break;
        case "SUBSET":
            if (subsetVal) {
                var pathElement = "[SUBSET:" + subsetVal + "]";
                pathSoFar += pathElement;
                friendlyPathSoFar += pathElement;
            }
            break;
        case "WHOLE":
            var pathElement = "[ANY]";
            pathSoFar += pathElement;
            friendlyPathSoFar += pathElement;
            break;
    }

    if (propertyVal) {
        var pathElement = "." + propertyVal;
        pathSoFar += pathElement;
        friendlyPathSoFar += pathElement;
    }

    pathSoFarSpan.text(friendlyPathSoFar);
    $("#PathableFieldPermissionsEditor_FriendlyPath").val(friendlyPathSoFar);
    $("#PathableFieldPermissionsEditor_Path").val(pathSoFar);

    setSaveEnabled();
});
