﻿function postRefreshCalculationMaxDiscountPointsHandler(result) {
    $('#' + ML.MaxDiscountPointsClientId + '_DiscountPointPercentage').val(result[ML.MaxDiscountPointsResultPrefix + '_DiscountPointPercentage']);
    $('#' + ML.MaxDiscountPointsClientId + '_RepairCostsAndFees').val(result[ML.MaxDiscountPointsResultPrefix + '_RepairCostsAndFees']);
    $('#' + ML.MaxDiscountPointsClientId + '_ContingencyReserves').val(result[ML.MaxDiscountPointsResultPrefix + '_ContingencyReserves']);
    $('#' + ML.MaxDiscountPointsClientId + '_MortgagePaymentReserves').val(result[ML.MaxDiscountPointsResultPrefix + '_MortgagePaymentReserves']);
    $('#' + ML.MaxDiscountPointsClientId + '_SumRepairCostsFeesAndReserves').val(result[ML.MaxDiscountPointsResultPrefix + '_SumRepairCostsFeesAndReserves']);
    $('#' + ML.MaxDiscountPointsClientId + '_MaxFinanceableDiscountPointsAmount').val(result[ML.MaxDiscountPointsResultPrefix + '_MaxFinanceableDiscountPointsAmount']);
}
