﻿jQuery(function(jq) {
    // Want to limit the scope of selection to this portlet.
    var $main = jq('#OpenTradesPortletMain');
    function $(a) {
        return jq(a, $main);
    }

    $('.print').click(function(event) {
        var $printDiv = $('#TradeTableMain');

        var printWindow = window.open('', 'Trade_Table', 'resizable=yes,scrollbars=yes');
        var doc = printWindow.document;
        doc.open();
        doc.write('<html><head>');
        doc.write('<title>Trades</title>');
        doc.write('<link href="' + encodeURI($('.base-style-sheet').val()) + '" type="text/css" rel="stylesheet" />');
        doc.write('<link href="' + encodeURI($('.trade-report-style-sheet').val()) + '" type="text/css" rel="stylesheet" />');
        doc.write('</head><body onload="window.print(); window.close();">');
        var headerHtml = ''
        var reportTitle = $('.report-title').val();
        var logoSrc = $('.logo-src').val();
        var headerHtml = '<div class="report-header">';
        if (logoSrc.length > 0) headerHtml += '<img src="' + encodeURI(logoSrc) + '" alt="Company Logo" />';
        headerHtml += '<h1>' + reportTitle + '</h1>';
        headerHtml += '</div>';
        doc.write(headerHtml);
        doc.write($printDiv.html());
        doc.write('</body></html>');
        doc.close();
        printWindow.focus();
    });

    $('.export-pdf').click(function(event) {
        window.location.href = QueryStringUtilities.updateQueryString('toPdf', 'true');
    });

});
