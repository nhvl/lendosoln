﻿jQuery(function(jq) {
    var $main = jq('#TradePortletMain');
    //We don't want to accidentally touch anything outside the portlet,
    // so we'll restrict everything to searching within the portlet's main div
    //This should work exactly like using jquery normally as long as you don't
    // actually need to use the global jquery object (it's jq if you need it).
    function $(a) {
        return jq(a, $main);
    }

    function openTradeEditorWindow(params) {
        if (params) {
            params = '?' + params;
        }
        else {
            params = '';
        }

        var w = screen.availWidth - 10;
        var h = screen.availHeight - 50;


        var win = window.open('MortgagePools/TradeEditor.aspx' + params, 'TradeEdit', 'width=' + w + 'px,height=' + h + 'px,left=0px,top=0px,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes');
        win.focus();
    }

    $('#NewTrade').css({display:jq('#HideCreateTrade').length ? "none" : ""});


    $('#NewTrade').click(function() {
        openTradeEditorWindow('new=t');
    });
});
