﻿function hideUnfocusedDropdowns() {
    $('.dropdown-checkbox.unfocused .options-container').hide();
}

function setDropdownCheckboxText($el) {
    // Set the value in the dropdown option, updating the text.
    var $selectedOptionEl = $el.find('.selected-options option');
    var specialOptions = $el.find('input.special:checked');

    if (specialOptions.length > 0) {
        $selectedOptionEl.text(specialOptions.first().parent().text());
    }
    else {
        var numSelected = $el.find('input:checked').length;

        var text;
        if (numSelected == 0) {
            text = 'Not Applicable';
        }
        else {
            text = numSelected + ' item' + (numSelected > 1 ? 's' : '') + ' selected';
        }

        $selectedOptionEl.text(text);
    }
}

function toggleInputClasses($el) {
    $el.parent().toggleClass('selected', $el.prop('checked'));
}

function setAllInputClasses($el) {
    $el.find('input').each(function (i, val) {
        toggleInputClasses($(this));
    });
}

$(document).on('click', hideUnfocusedDropdowns);

$(document).on('mousedown', '.dropdown-checkbox .selected-options', function (e) {
    // Prevent the dropdown from opening its contents.
    e.preventDefault();
    this.blur();
    window.focus();
});

$(document).on('click', '.dropdown-checkbox .selected-options', function () {
    // Show the checkbox container.
    $(this).next().toggle();
});

$(document).on('mouseleave mouseenter', '.dropdown-checkbox, .dropdown-checkbox .options-container', function (e) {
    // If a user mouses out of the dropdown, mark it as unfocused.  If they mouse in, remove the class.
    var isMouseEnter = e.type == "mouseenter";
    $(this).toggleClass('unfocused', !isMouseEnter);
});

$(document).on('change', '.dropdown-checkbox input:not(.special)', function () {
    var $checkbox = $(this);
    var $container = $checkbox.parent().parent();

    toggleInputClasses($checkbox);
    determineAllOrNothing($container);
    setDropdownCheckboxText($container.parent());
});

$(document).on('change', '.dropdown-checkbox input.special', function () {
    var $checkbox = $(this);
    var $container = $checkbox.parent().parent();
    var $label = $checkbox.parent();

    $container.find('input.special:not([key="' + $checkbox.attr('key') + '"])').prop('checked', false);

    $container.find('input:not(.special)').prop('checked', $checkbox.prop('checked'));

    setDropdownCheckboxText($container.parent());
    setAllInputClasses($container);
});

function getDropdownCheckboxValues(selector) {
    return $(selector).find('input:checked:not(.special)').map(function () { return this.getAttribute("key"); }).get();
}

function setDropdownCheckboxValues(selector, data) {
    var $element = $(selector);
    $element.find('input').prop("checked", false);
    $.each(data, function (i, key) {
        $element.find('input[key="' + key + '"]').prop("checked", true)
    });

    determineAllOrNothing($element);
    setAllInputClasses($element);
    setDropdownCheckboxText($element);
}

function determineAllOrNothing($element) {
    var $all = $element.find('input.special[key="all"]');

    var isAll = $element.find('input:not(.special):not(:checked)').length == 0;
    $all.prop('checked', isAll).parent().toggleClass('selected', isAll);
}

function initializeDropdownCheckbox(selector, data) {
    var $element = $(selector).addClass('dropdown-checkbox');
    var $selectedOptions = $('<select class="selected-options"><option></option></select>');
    var $optionsContainer = $('<div class="options-container"></div>');

    $element.append($selectedOptions).append($optionsContainer);

    $optionsContainer.append(createDropdownOption("all", "Select All", true));
    for (var i = 0; i < data.length; i++) {
        $optionsContainer.append(createDropdownOption(data[i].Key, data[i].Value, false));
    }
}

function createDropdownOption(key, desc, isSpecial) {
    var h = hypescriptDom;
    return $(h("label", {className:"dropdown-option"}, [
        h("input", {type:"checkbox", className:(isSpecial?"special":""), attrs:{key:key}}),
        h("span", {}, desc)
    ]));
}
