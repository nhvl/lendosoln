﻿$(document).ready(function () {
    $(".hmdaCoApplicantInfo").toggle(ML.hasHmdaCoApplicant);
    $("#HmdaCoApplicantScoreLabel").text(ML.hasHmdaCoApplicant ? "HMDA Co-Applicant's Decision Score" : "No HMDA Co-Applicant");

    registerPostRefreshCalculationCallback(SetDataValues);
    SetDataValues();
    initCreditScoreAndModelNameStatus();

    if (ML.LoanVersionTCurrent >= 26) {
        initCreditScoreModelT();
    }

    if (ML.isCreditScores) {
        $("#reliedOnWithoutCreditInfo").hide();
        $("#reliedOnHeader").text("HMDA Credit Score Information");
    }

    $(".applicantCreditInfo").prop('disabled', true);
    $(".applicantCreditInfoText").prop('readonly', true);
});

function initCreditScoreModelT() {
    $('#sReliedOnCreditScoreModelT').change(function () {
        var creditModelIsOther = $(this).val() === ML.CreditScoreModelT_Other;
        var $otherDesc = $('#sReliedOnCreditScoreModelTOtherDescription');
        $otherDesc.toggle(creditModelIsOther);
    });

    $('#sReliedOnCreditScoreModelT').change();
}

function updateIndividualScoreInfo() {
    var $creditScoreMode = $("#" + ML.prefix + "sReliedOnCreditScoreModeT");

    if ($creditScoreMode.val() !== "1") { // applicant's individual score
        return;
    }
    
    var $borrowerDecisionSourceDropdown = $("#aBDecisionCreditSourceT");
    var $borrowerDecisionSourceDropdownLckd = $("#aBDecisionCreditSourceTLckd");
    var $borrowerDecisionCreditScore = $("#aBDecisionCreditScore");
    var $borrowerDecisionOtherModelName = $("#aBOtherCreditModelName");
    var $borrowerDecisionCreditModelName = $("#aBDecisionCreditModelName");
    var $borrowerDecisionModelNameIsValidHmdaModel = $("#aBIsDecisionCreditModelAValidHmdaModel");

    var $coborrowerDecisionSourceDropdown = $("#aCDecisionCreditSourceT");
    var $coborrowerDecisionSourceDropdownLckd = $("#aCDecisionCreditSourceTLckd");
    var $coborrowerDecisionCreditScore = $("#aCDecisionCreditScore");
    var $coborrowerDecisionOtherModelName = $("#aCOtherCreditModelName");
    var $coborrowerDecisionCreditModelName = $("#aCDecisionCreditModelName");
    var $coborrowerDecisionModelNameIsValidHmdaModel = $("#aCIsDecisionCreditModelAValidHmdaModel");

    var $curAppBorSource = $("#" + ML.prefix + "ApplicantCreditScores_ctl" + ML.currentAppIndex + "_aBDecisionCreditSourceT");
    var $curAppBorLckd = $("#" + ML.prefix + "ApplicantCreditScores_ctl" + ML.currentAppIndex + "_aBDecisionCreditSourceTLckd");
    var $curAppBorScore = $("#" + ML.prefix + "ApplicantCreditScores_ctl" + ML.currentAppIndex + "_aBDecisionCreditScore");
    var $curAppBorModelName = $("#" + ML.prefix + "ApplicantCreditScores_ctl" + ML.currentAppIndex + "_aBDecisionCreditModelName");
    var $curAppBorModelNameIsValidHmdaModel = $("#" + ML.prefix + "ApplicantCreditScores_ctl" + ML.currentAppIndex + "_aBIsDecisionCreditModelAValidHmdaModel");

    var $curAppCoborSource = $("#" + ML.prefix + "ApplicantCreditScores_ctl" + ML.currentAppIndex + "_aCDecisionCreditSourceT");
    var $curAppCoborLckd = $("#" + ML.prefix + "ApplicantCreditScores_ctl" + ML.currentAppIndex + "_aCDecisionCreditSourceTLckd");
    var $curAppCoborScore = $("#" + ML.prefix + "ApplicantCreditScores_ctl" + ML.currentAppIndex + "_aCDecisionCreditScore");
    var $curAppCoborModelName = $("#" + ML.prefix + "ApplicantCreditScores_ctl" + ML.currentAppIndex + "_aCDecisionCreditModelName");
    var $curAppCoborModelNameIsValidHmdaModel = $("#" + ML.prefix + "ApplicantCreditScores_ctl" + ML.currentAppIndex + "_aCIsDecisionCreditModelAValidHmdaModel");

    if ($curAppBorSource.length) {
        if (ML.isCreditScores) {
            $curAppBorSource.val($borrowerDecisionSourceDropdown.val());
            $curAppBorLckd.prop('checked', $borrowerDecisionSourceDropdownLckd.prop('checked'));
            $curAppBorScore.val($borrowerDecisionCreditScore.val());
            $curAppBorModelName.val($borrowerDecisionCreditModelName.val());
            $curAppBorModelNameIsValidHmdaModel.prop('checked', $borrowerDecisionModelNameIsValidHmdaModel.is(':checked'));
        }

        var borModelIsValidForHmda = $curAppBorModelNameIsValidHmdaModel.is(':checked');
        var borDecisionModelIsBlank = $curAppBorModelName.val() === '';
        var showBorDecisionCreditModel = !borModelIsValidForHmda && !borDecisionModelIsBlank;
        $curAppBorModelName.parent().toggle(showBorDecisionCreditModel);
    }

    if ($curAppCoborSource.length) {
        if (ML.isCreditScores) {
            $curAppCoborSource.val($coborrowerDecisionSourceDropdown.val());
            $curAppCoborLckd.prop('checked', $coborrowerDecisionSourceDropdownLckd.prop('checked'));
            $curAppCoborScore.val($coborrowerDecisionCreditScore.val());
            $curAppCoborModelName.val($coborrowerDecisionCreditModelName.val());
            $curAppCoborModelNameIsValidHmdaModel.prop('checked', $coborrowerDecisionModelNameIsValidHmdaModel.is(':checked'));
        }
        
        var coborModelIsValidForHmda = $curAppCoborModelNameIsValidHmdaModel.is(':checked');
        var coborDecisionModelIsBlank = $curAppCoborModelName.val() === '';
        var showCoborDecisionCreditModel = !coborModelIsValidForHmda && !coborDecisionModelIsBlank;
        $curAppCoborModelName.parent().toggle(showCoborDecisionCreditModel);
    }
}

function SetDataValues() {
    updateModeDdlUI();
    updateIndividualScoreInfo();

    lockCheckBoxChanged(document.getElementById(ML.prefix + "sReliedOnTotalIncomeLckd"), "sReliedOnTotalIncome");
    lockCheckBoxChanged(document.getElementById(ML.prefix + "sReliedOnDebtRatioLckd"), "sReliedOnDebtRatio");
    lockCheckBoxChanged(document.getElementById(ML.prefix + "sReliedOnCombinedLTVRatioLckd"), "sReliedOnCombinedLTVRatio");
    lockCheckBoxChanged(document.getElementById(ML.prefix + "sReliedOnPropertyValueLckd"), "sReliedOnPropertyValue");

    if (ML.LoanVersionTCurrent >= 26) {
        lockCheckBoxChanged(document.getElementById(ML.prefix + "sReliedOnCreditScoreModelTLckd"), "sReliedOnCreditScoreModelT");
        lockCheckBoxChanged(document.getElementById(ML.prefix + "sReliedOnCreditScoreModelTLckd"), "sReliedOnCreditScoreModelTOtherDescription");
    }
    else {
        lockCheckBoxChanged(document.getElementById(ML.prefix + "sReliedOnCreditScoreModelNameLckd"), "sReliedOnCreditScoreModelName");
    }
}

function updateSingleSourceDdlUI() {
    var $singleScoreSourceDropdown = $("#" + ML.prefix + "sReliedOnCreditScoreSourceT");
    var $hmdaReportAs = $("#" + ML.prefix + "HmdaReportAs");
    var $reportAsRadioListitems = $("#" + ML.prefix + "sReliedOnCreditScoreSingleHmdaReportAsT input:radio");
    var $creditScore = $("#" + ML.prefix + "sReliedOnCreditScore");

    if ($singleScoreSourceDropdown.val() === ML.CreditScoreReliedOnSourceOther) {
        $reportAsRadioListitems.prop('checked', false);
        $creditScore.val("");
        $creditScore.prop('readonly', false);
        clearCreditScoreModel();
        toggleCreditScoreModelReadOnly(false);
        toggleCreditScoreModelLocked(true);
    }
    else {
        $creditScore.prop('readonly', true);
        toggleCreditScoreModelReadOnly(true);
        toggleCreditScoreModelLocked(false);

        if ($hmdaReportAs.val() === "1") {
            $reportAsRadioListitems[0].checked = true;
        }
        else if ($hmdaReportAs.val() === "2") {
            $reportAsRadioListitems[1].checked = true;
        }
        else {
            $reportAsRadioListitems.prop('checked', false);
        }
    }
}

function clearCreditScoreModel() {
    if (ML.LoanVersionTCurrent >= 26) {
        var $modelT = $("#" + ML.prefix + "sReliedOnCreditScoreModelT");
        $modelT.val(ML.CreditScoreModelT_LeaveBlank);

        var $modelTOtherDescription = $("#" + ML.prefix + "sReliedOnCreditScoreModelTOtherDescription");
        $modelTOtherDescription.val("");
    }
    else {
        var $modelName = $("#" + ML.prefix + "sReliedOnCreditScoreModelName");
        $modelName.val("");
    }
}

function toggleCreditScoreModelLocked(isLocked) {
    if (ML.LoanVersionTCurrent >= 26) {
        var $modelLocked = $('#' + ML.prefix + "sReliedOnCreditScoreModelTLckd");
        $modelLocked.prop('checked', isLocked);
    }
    else {
        var $modelLocked = $('#' + ML.prefix + "sReliedOnCreditScoreModelNameLckd");
        $modelLocked.prop('checked', isLocked);
    }
}

function toggleCreditScoreModelReadOnly(isReadOnly) {
    if (ML.LoanVersionTCurrent >= 26) {
        var $modelT = $("#" + ML.prefix + "sReliedOnCreditScoreModelT");
        $modelT.prop('readonly', isReadOnly);

        var $modelTOtherDescription = $("#" + ML.prefix + "sReliedOnCreditScoreModelTOtherDescription");
        $modelTOtherDescription.prop('readonly', isReadOnly);
    }
    else {
        var $modelName = $("#" + ML.prefix + "sReliedOnCreditScoreModelName");
        $modelName.prop('readonly', isReadOnly);
    }
}

function initCreditScoreAndModelNameStatus() {
    var $creditScore = $("#" + ML.prefix + "sReliedOnCreditScore");
    var $singleScoreSourceDropdown = $("#" + ML.prefix + "sReliedOnCreditScoreSourceT");
    var $reportAsRadioListitems = $("#" + ML.prefix + "sReliedOnCreditScoreSingleHmdaReportAsT input:radio");

    $creditScore.prop('readonly', $singleScoreSourceDropdown.val() !== ML.CreditScoreReliedOnSourceOther);

    if ($singleScoreSourceDropdown.val() === ML.CreditScoreReliedOnSourceBlank) {
        $reportAsRadioListitems.prop('checked', false);
    }
}

function updateModeDdlUI() {
    var $creditScoreMode = $("#" + ML.prefix + "sReliedOnCreditScoreModeT");
    var $creditScore = $("#" + ML.prefix + "sReliedOnCreditScore");
    var $singleScoreSourceDropdown = $("#" + ML.prefix + "sReliedOnCreditScoreSourceT");

    switch ($creditScoreMode.val()) {
        case "0": // blank
            $("#singleScore").hide();
            $("#applicantIndividualScore").hide();
            break;
        case "1": // applicant's individual score
            $("#singleScore").hide();
            $("#applicantIndividualScore").show();
            break;
        case "2": // single score
            $("#singleScore").show();
            $("#applicantIndividualScore").hide();
            if ($singleScoreSourceDropdown.val() === ML.CreditScoreReliedOnSourceBlank) {
                $creditScore.prop('readonly', true);
                toggleCreditScoreModelReadOnly(true);
            }
            break;
    }
}

function lockCheckBoxChanged(checkBox, textBoxName) {
    if (checkBox != null){
        lockField(checkBox, ML.prefix + textBoxName);
    }
}