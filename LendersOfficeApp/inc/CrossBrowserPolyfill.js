﻿function createEvent(eventType, bubbles, cancelable)
{
    if (typeof document.CustomEvent === 'function') {
        return new document.CustomEvent(eventType);
    }
    else if (document.createEvent) {
        var event = document.createEvent('Event');
        event.initEvent(eventType, bubbles === undefined ? true : bubbles, cancelable === undefined ? true : cancelable);
        return event;
    }
    else if (document.createEventObject)
    {
        var event = document.createEventObject();
        event.eventType = eventType;
        return event;
    }
}

function dispatchEvent(dispatchSource, event, eventType) {

    if(dispatchSource.dispatchEvent)
    {
        dispatchSource.dispatchEvent(event);
    }
    else if (dispatchSource.fireEvent) {
        dispatchSource.fireEvent( "on" + (event.eventType || eventType), event);
    }
}

function triggerEvent(element, eventType, bubbles, cancelable) {
    var event = createEvent(eventType, bubbles == null ? true : bubbles, cancelable == null ? true : cancelable);
    dispatchEvent(element, event);
}

function retrieveEventTarget(event) {
    return event ? (event.srcElement || event.target) : null;
}

function addEventHandler(element, eventType, func, useCapture) {
    useCapture = useCapture || false;
    if (element.addEventListener && eventType != "propertychange") {
        element.addEventListener(eventType, func, useCapture);
    } else if(element.attachEvent) {
        if (eventType == "input") {
            eventType = "propertychange";
        }

        element.attachEvent("on" + eventType, func);
    }
}

function removeEventHandler(element, eventType, func) {
    if (element.removeEventListener) {
        element.removeEventListener(eventType, func);
    } else if(element.detachEvent) {
        element.detachEvent("on" + eventType, func);
    }
}

function hasDisabledAttr(obj) {
    if (typeof obj === 'undefined' || obj === null) {
        return false;
    }

    var unwrappedObj = obj;
    var wrappedObj = obj;

    if (obj instanceof jQuery) {
        unwrappedObj = obj[0];
    }    

    return hasAttr(unwrappedObj, 'disabled');
}

function setDisabledAttr(obj, disable) {
    if (typeof obj === 'undefined' || obj === null) {
        return;
    }

    var wrappedObj = obj;

    if (!(obj instanceof jQuery)) {
        var jQueryObj = $j || $;
        wrappedObj = jQueryObj(obj);
    }

    if (disable) {
        wrappedObj.attr('disabled', true);
    }
    else {
        wrappedObj.removeAttr('disabled');
    }
}

////  Warning.  Mutation Observers will fire whenever the attribute is set, even if the value is being set the the value it already has.
//// This means that if the callback modifies the same attribute, it can lead to an infinite loop.
function addAttributeChangeObserver(element, listOfObservedAttributes, callback) {
    if (typeof(MutationObserver) != 'undefined') {
        var mutationConfig = { attributes: true };
        listOfObservedAttributes = listOfObservedAttributes.map(function (attr) { return attr.toLowerCase();});

        var attributeChangeObserverCallback = function (mutationsList) {
            for (var i = 0; i < mutationsList.length; i++) {
                var mutation = mutationsList[i];
                if (mutation.type == 'attributes' && listOfObservedAttributes.indexOf(mutation.attributeName.toLowerCase()) > -1) {
                    callback(null, element, mutation.attributeName);
                }
            }
        };

        var mutationObserver = new MutationObserver(attributeChangeObserverCallback);
        mutationObserver.observe(element, mutationConfig);
    }
    else {
        addEventHandler(element, "propertychange", callback);
    }
}

function safeInvokeCallback(func){
    if (typeof(func) == 'function'){
        func();
    }
}

function PolyShouldShowConfirmSave(confirmFlag, postCallback , yesCallback, noCallback, cancelCallback){
    (!confirmFlag) ? postCallback(false) : PolyConfirmSave(function(confirmed) {
        safeInvokeCallback(confirmed ? yesCallback : noCallback);
        postCallback(confirmed);
    }, true, cancelCallback);
}

function PolyConfirmSave(confirmCallback, shouldShowCancel, cancelCallback){
    PolyConfirmShow("Do you want to save the changes ?", confirmCallback, shouldShowCancel, cancelCallback);
}

function PolyConfirmShow(message, confirmCallback, shouldShowCancel, cancelCallback, forceLqbPopup){
    var execScriptEnabled = (typeof execScript === 'function' || typeof execScript === 'object');
    var shouldShowConfirmPrompt = !forceLqbPopup && (execScriptEnabled || !shouldShowCancel);
    if (shouldShowConfirmPrompt){    
        if (execScriptEnabled) {
            var _vbConfirmYes = 6, _vbConfirmNo = 7, _vbConfirmCancel = 2 ;
            var result = _vbConfirmCancel;
            try
            {
                execScript('r = msgbox("' + message + '","' + (shouldShowCancel ? 3 + 48: 4 + 48) +'", "LendingQB")', "vbscript");
                result = r;
            } catch (ex){ 
                // IF IE page not in Trust Side mode, we will show ConfirmPopupPage.aspx instead.
                result = -1;
            }

            switch (result){
                case _vbConfirmNo: 
                    confirmCallback(false);
                    return;
                case _vbConfirmYes:
                    confirmCallback(true);
                    return;
                case _vbConfirmCancel:
                    safeInvokeCallback(cancelCallback);
                    return;
            }
        } else {
            confirmCallback(window.confirm(message));
            return;
        }
    }

    var confirmUrl = VRoot + '/common/ModalDlg/ConfirmPopupPage.aspx';
    var returnHandler = function(confirmValue){
        confirmValue == -1 ? safeInvokeCallback(cancelCallback) : confirmCallback(confirmValue == 1);
    }

    setTimeout(function () {
        LQBPopup.Show(confirmUrl, { width: 200, height: 70, onReturn: returnHandler, hideCloseButton: true }, { confirmMessage: message, showCancel: shouldShowCancel});
    }, 0);
}

function addOnLoadHandler(element, eventType, func, isDocument) {
    if (element.addEventListener) {
        eventType = isDocument ? 'DOMContentLoaded' : eventType;
        addEventHandler(element, eventType, func);
    }
    else {
        element.onreadystatechange = function (event) {
            var onReadyState = isDocument ? 'complete' : eventType == 'DOMContentLoaded' ? 'interactive' : 'complete';
            if (element.readyState == onReadyState) {
                func();
            }
        };
    }
}

function removeOnLoadHandler(element, eventType, func, isDocument) {
    if (element.removeEventListener) {
        eventType = isDocument ? 'DOMContentLoaded' : eventType;
        removeEventHandler(element, eventType, func);
    }
    else {
        element.onreadystatechange = null;
    }
}

function fetchFileAsync(url, mimeType, callback) {
    if (getBrowserVersion() <= 11) {
        fetchFileViaFrame(url, callback);
    }
    else {
        fetchFilesViaAjax(url, mimeType, callback);
    }
}

function fetchFilesViaAjax(url, mimeType, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.setRequestHeader('Content-Type', mimeType);
    xhr.responseType = 'blob';

    try {
        // Case 475248 
        if (!!self && !!self.origin && self.origin.indexOf('secure.lendingqb.com') != -1 && url.indexOf('edocs.lendingqb.com') != -1) {
            xhr.withCredentials = true;
        }
    } catch (e) {
        // No-op
    }

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var filename = "";
            var disposition = xhr.getResponseHeader("Content-Disposition");
            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
            var matches = filenameRegex.exec(disposition);
            if (matches != null && matches[1]) {
                filename = matches[1].replace(/['"]/g, '');
            }

            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(xhr.response, filename);
            }
            else {
                var anchor = document.createElement('a');
                anchor.download = filename;
                anchor.href = window.URL.createObjectURL(xhr.response);

                // Firefox requires the link to be added to the document before 
                // the click event will fire: https://stackoverflow.com/a/32226068.
                // Also, per this SO answer https://stackoverflow.com/a/30708820,
                // there also needs to be a timeout before removing the child
                // to ensure that the click event and file download are executed.
                document.body.appendChild(anchor);
                anchor.click();
                setTimeout(function () { document.body.removeChild(anchor); });
            }

            if (callback) {
                callback();
            }
        }
    };

    xhr.send();
}

function onFetchFileFrameLoad(event) {
    var frame = retrieveEventTarget(event);
    frame.numberStateChanges++;
    if (frame.numberStateChanges == 2 && frame.loadCallback) {
        frame.loadCallback();
    }
}

function fetchFileViaFrame(url, callback) {
    var frameId = 'downloadFileIframe';
    var isIe11 = getBrowserVersion() == 11;
    var frame = document.getElementById(frameId) || document.createElement('iframe');
    document.body.appendChild(frame);
    frame.id = 'downloadFileIframe';

    if (!isIe11) {
        frame.onreadystatechange = function () {
            if (frame.readyState === 'interactive') {
                if (callback) {
                    callback();
                }
            }
        };
    }
    else {
        frame.setAttribute('onreadystatechange', 'onFetchFileFrameLoad(event);');
        frame.loadCallback = callback;
        frame.numberStateChanges = 0;
    }

    frame.src = url;
    frame.style.display = 'none';
}

function getBrowserVersion() {
    //so 17907445
    var d = document, w = window;
    var ie = (!!w.MSInputMethodContext ? 11 : !d.all ? 99 : w.atob ? 10 :
    d.addEventListener ? 9 : d.querySelector ? 8 : w.XMLHttpRequest ? 7 :
    d.compatMode ? 6 : w.attachEvent ? 5 : 1);
    return ie;
}

//Drag & Drop resizing capability for IFrames. iOPM: 469770
// <div class="body-container">
//     <iframe src="(left panel)"></iframe>
//     <div onmousedown="triggerTreeviewResize();" class="DragBar"></div>
//     <iframe src="(right panel)"></iframe>
// </div>
// <div id='CalculationOverlay' class="Hidden"></div>
function triggerTreeviewResize() {
    function removeResizeTrigger()
    {
        var body = document.getElementsByTagName("body")[0];
        jQuery("#CalculationOverlay").addClass("Hidden");
        body.removeEventListener("mousemove", onResizeMove);
        body.removeEventListener("mouseup", removeResizeTrigger);
    }

    function onResizeMove(event) {
        changeTreeviewSize(event.clientX - 1.5);
    }

    var body = document.getElementsByTagName("body")[0];
    body.addEventListener("mousemove", onResizeMove);
    body.addEventListener("mouseup", removeResizeTrigger);
    jQuery("#CalculationOverlay").removeClass("Hidden");
}

function changeTreeviewSize(newSize) {
    var treeView = jQuery('.body-container iframe:first');
    treeView.css({
        '-ms-flex': '0 0 '+newSize+'px',
        '-webkit-flex': '0 0 '+newSize+'px',
        'flex': '0 0 '+newSize+'px'
    });
}

function getFullRef(relative, fromPath) {
    var pathName = (fromPath != null ? fromPath : window.location.pathname);
    if (relative.charAt(0) == "/"){
        return relative;
    }

    var stack = pathName.split("/");
    stack.pop();

    parts = relative.split("/");
    for (var i=0; i<parts.length; i++) {
        if (parts[i] == ".")
            continue;
        if (parts[i] == "..")
            stack.pop();
        else
            stack.push(parts[i]);
    }

    return stack.join("/");
}

// iOPM: 477482
// defaultbutton attribute in form tag scrolls dialog to bottom in non-IE browsers. 
// Need manually set default button in script.
function setDefaultFormButton(form, button) {
    var fr = document.getElementById(form);
    if(fr) {
        fr.onkeypress = function(e) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(button);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }    
    }
}

function getQueryStringVariable(variable, fallbackValue) {
	if (typeof URLSearchParams !== 'undefined') {
		var params = new URLSearchParams(window.location.search);
		if (params.has(variable)) {
			return params.get(variable);
		}
	}
	else {
		var search = window.location.search.substring(1);
		var parameters = search.split("&");
		
		for (var i = 0; i < parameters.length; ++i) {
			var pair = parameters[i].split("=");
			
			if (pair[0] === variable) {
				return pair[1];
			}
		}
    }

    if (typeof fallbackValue !== 'undefined') {
        return fallbackValue;
    }
	
	throw 'Invalid variable ' + variable;
}

try {
    // Obtained from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign#Polyfill
    if (typeof Object.assign != 'function' && typeof Object.defineProperty == 'function') {
        // Must be writable: true, enumerable: false, configurable: true
        Object.defineProperty(Object, "assign", {
            value: function assign(target, varArgs) { // .length of function is 2
                'use strict';
                if (target == null) { // TypeError if undefined or null
                    throw new TypeError('Cannot convert undefined or null to object');
                }

                var to = Object(target);

                for (var index = 1; index < arguments.length; index++) {
                    var nextSource = arguments[index];

                    if (nextSource != null) { // Skip over if undefined or null
                        for (var nextKey in nextSource) {
                            // Avoid bugs when hasOwnProperty is shadowed
                            if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                                to[nextKey] = nextSource[nextKey];
                            }
                        }
                    }
                }
                return to;
            },
            writable: true,
            configurable: true
        });
    }

    // Obtained from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find#Polyfill
    if (typeof Array.prototype.find != 'function' && typeof Object.defineProperty == 'function') {
        Object.defineProperty(Array.prototype, 'find', {
            value: function (predicate) {
                // 1. Let O be ? ToObject(this value).
                if (this == null) {
                    throw new TypeError('"this" is null or not defined');
                }

                var o = Object(this);

                // 2. Let len be ? ToLength(? Get(O, "length")).
                var len = o.length >>> 0;

                // 3. If IsCallable(predicate) is false, throw a TypeError exception.
                if (typeof predicate !== 'function') {
                    throw new TypeError('predicate must be a function');
                }

                // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
                var thisArg = arguments[1];

                // 5. Let k be 0.
                var k = 0;

                // 6. Repeat, while k < len
                while (k < len) {
                    // a. Let Pk be ! ToString(k).
                    // b. Let kValue be ? Get(O, Pk).
                    // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
                    // d. If testResult is true, return kValue.
                    var kValue = o[k];
                    if (predicate.call(thisArg, kValue, k, o)) {
                        return kValue;
                    }
                    // e. Increase k by 1.
                    k++;
                }

                // 7. Return undefined.
                return undefined;
            },
            configurable: true,
            writable: true
        });
    }

    // Obtained from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/findIndex#Polyfill
    if (typeof Array.prototype.findIndex != 'function' && typeof Object.defineProperty == 'function') {
        Object.defineProperty(Array.prototype, 'findIndex', {
            value: function (predicate) {
                // 1. Let O be ? ToObject(this value).
                if (this == null) {
                    throw new TypeError('"this" is null or not defined');
                }

                var o = Object(this);

                // 2. Let len be ? ToLength(? Get(O, "length")).
                var len = o.length >>> 0;

                // 3. If IsCallable(predicate) is false, throw a TypeError exception.
                if (typeof predicate !== 'function') {
                    throw new TypeError('predicate must be a function');
                }

                // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
                var thisArg = arguments[1];

                // 5. Let k be 0.
                var k = 0;

                // 6. Repeat, while k < len
                while (k < len) {
                    // a. Let Pk be ! ToString(k).
                    // b. Let kValue be ? Get(O, Pk).
                    // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
                    // d. If testResult is true, return k.
                    var kValue = o[k];
                    if (predicate.call(thisArg, kValue, k, o)) {
                        return k;
                    }
                    // e. Increase k by 1.
                    k++;
                }

                // 7. Return -1.
                return -1;
            },
            configurable: true,
            writable: true
        });
    }
} catch (e) {

}

if (typeof Number.MIN_SAFE_INTEGER != 'number') {
    Number['MIN_SAFE_INTEGER'] = -9007199254740991;
}
