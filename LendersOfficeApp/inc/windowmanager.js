/**
 Idea and code borrow from DecisionApp CWindowManager
*/
function WindowManager() {
  this.closeAll = wmCloseAll;
  this.close = wmClose;
  this.find = wmFind;
  this.open = wmOpen;
  this.add = wmAdd;
  
  this.hashTable = new Object();
  
  return this;
}

function wmCloseAll(fnCallback, bCancel) {
  for (p in this.hashTable) {
    var w = this.hashTable[p];
    if (null != fnCallback) {
      if (!fnCallback(w, bCancel)) return false;
    }
    if (!w.isClosed()) w.close();
  }
  this.hashTable = new Object();
  return true;
}

function wmClose(name) {
  var w = this.find(name);
  if (null != w) {
    w.close();
    delete this.hashTable[name];
  }
}

// Return win object if name existed, return null otherwise.
function wmFind(name) {

  var o = this.hashTable[name];
  if (null != o && o.isClosed()) return null;
  else return o;
 
}

// Do a window popup.
// Parameters:
//     name - unique name for window. If name already existed then url will open in current browser.
//     url 
//     isReplace - If set to false, then new URL will not be load to current open browser with same name.
//                 Default is true.
//     options - settings for new browser window. 
function wmOpen(name, url, isReplace, options) {
  // Set default value if isReplace and options are not provided.
  if (wmOpen.arguments.length < 3) isReplace = true;
  if (wmOpen.arguments.length < 4) options = "resizable=yes,scrollbars=yes,menubar=no,toolbar=no";
  
  var w = this.find(name);
  if (null == w) {
    w = new CWindow(name, url, options);
    this.hashTable[name] = w;
    isReplace = true; // Set this option so that new browser will create.
  }
  
  if (isReplace) w.open(); // Replace open browser with new url.
  
  w.focus(); 
  return w;
}

function wmAdd(name, handle) {
  var o = this.hashTable[name];

  
  if (null != o) {
    alert('Duplicate name found in hash table.');
    return;
  }
  var w = new CWindow(name, null, null);
  w.handle = handle;
  this.hashTable[name] = w;
  
}
// Define CWindow class.
function CWindow(sName, sUrl, sOptions) {
  this.name = sName;
  this.url = sUrl;
  this.options = sOptions;
  this.handle = null;
  this.open = wOpen;
  this.close = wClose;
  this.focus = wFocus;
  this.isClosed = wIsClosed;
  return this;
}

function wIsClosed() {
  return null == this.handle ? true : this.handle.closed;
}

function wOpen() {
  this.handle = window.open(this.url, this.name, this.options);
  if (null == this.handle) alert('Unable to open new window.  This may be caused by a pop-up blocker program on your computer.  Please contact Technical Support for assistance.');
  
  
}

function wClose() {

  if (this.handle && !this.handle.closed)
    this.handle.close();
}
function wFocus() {
  if (null != this.handle) 
    this.handle.focus();
}