﻿// Used to create a new manual entry in the table.
function UcdDelivery(ucdDeliveryId) {
    this.UcdDeliveryId = ucdDeliveryId;
    this.LoanId = ML.sLId;
    this.BrokerId = ML.BrokerId;
    this.UcdDeliveryType = 0;
    this.DeliveredTo = 0;
    this.FileDeliveredDocumentId = '';
    this.CaseFileId = "";
    this.DateOfDelivery = "";
    this.Note = "";
    this.IncludeInUldd = false;
    this.IsNew = true;

    this.isDirty = true; // Since it's new we know it's dirty...
    this.isDeleted = false; // and hasn't been deleted.
}

function getScope(selector) {
    var appElement = $(selector);
    return angular.element(appElement).scope();
}

function LoadUcdDeliveries() {
    var $deliveriesTableScope = getScope("#DeliveriesTableControllerDiv");
    $deliveriesTableScope.loadDeliveries();
}

function doAfterDateFormat(jsElement) {
    // Apply changes to angular model (JS code will not do this on its own).
    var angElement = angular.element($(jsElement));
    angElement.scope().$apply(function () {
        angElement.controller('ngModel').$setViewValue(angElement.val());
    }); 
}

$(function () {
    initializeAngular();
});

function _init()
{
    LoadUcdDeliveries();
}

function initializeAngular() {
    var module = angular.module('UcdDelivery', []);

    module.directive('convertToNumber', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function (val) {
                    return parseInt(val, 10);
                });
                ngModel.$formatters.push(function (val) {
                    return '' + val;
                });
            }
        };
    });

    module.directive('autoWireInput', function () {
        return {
            link: function (scope, element, attrs, ngModel) {
                element.removeAttr(gInputInitializedAttr); // Clear bit marking input as initialized (may have come from template).
                _initDynamicInput(element[0], true);
            }
        };
    });

    module.directive('autoWireCalendar', function () {
        return {
            link: function (scope, element, attrs, ngModel) {
                var newID = element.prop('id') + scope.$parent.$index;
                element.prop('id', newID);

                // Clear old binding (uses wrong ID)
                var link = element.parent().find('a');
                link.off('click');

                // Rebind handlers. I hope that's all of them.
                link.on('click', function () {
                    return displayCalendar(newID);
                });

                element.removeAttr(gInputInitializedAttr);
                _initDynamicInput(element[0], true);
            }
        };
    });

    module.directive('mustBeNonZero', function () {
        return {
            require: 'ngModel',

            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.unshift(isNonZero);     //For DOM -> model validation
                ngModel.$formatters.unshift(isNonZero);  //For model -> DOM validation

                function isNonZero(value) {
                    ngModel.$setValidity('isNonZero', value != 0);
                    return value;
                }
            }
        };
    });

    module.directive('mustHaveEdoc', function () {
        return {
            require: 'ngModel',

            link: function (scope, element, attrs, ngModel) {
                ngModel.$validators.hasEdoc = function (modelValue, viewValue) {
                    if (modelValue) {
                        return true;
                    }

                    return false;
                };
            }
        };
    });

    module.controller("DeliveriesTableController", function DeliveriesTableController($scope, $filter) {
        var Enums = {
            UcdDeliveredToEntity: {
                FannieMae: 1,
                FreddieMac: 2
            },
            UcdDeliveryTargetOption: {
                FannieMaeUcdCollectionSolution: 0
            }
        };

        $scope.deliveries = [];
        $scope.hasNonManual = false;
        $scope.newDeliveryIdCounter = -1;

        $scope.loadDeliveries = function () {
            var args = {
                LoanId: ML.sLId,
            };

            var result = gService.loanedit.call("LoadUcdDeliveries", args);
            if (result.error) {
                alert("An unexpected error occured while attempting to load delivery data:\n" + result.UserMessage);
                window.close();
            }

            $scope.updateViewModel(result.value);
        }

        $scope.saveDeliveries = function () {
            var updatedDeliveries = [];
            var deletedDeliveries = [];

            for (var i = 0; i < $scope.deliveries.length; i++) {
                delivery = $scope.deliveries[i];
                if (delivery.isDeleted && delivery.UcdDeliveryId >= 0) {
                    deletedDeliveries.push(delivery);
                } else if (!delivery.isDeleted && delivery.isDirty) {
                    updatedDeliveries.push(delivery);
                }
            }

            var args = {
                LoanId: ML.sLId,
                UpdatedDeliveries: JSON.stringify(updatedDeliveries),
                DeletedDeliveries: JSON.stringify(deletedDeliveries),
            };

            var result = gService.loanedit.call("SaveUcdDeliveries", args);
            if (result.error) {
                alert("Failed to save UCD Deliveries:\n" + result.UserMessage);
                return false;
            }

            $scope.updateViewModel(result.value);
            return true;
        }

        $scope.updateViewModel = function (values) {
            // Don't update if no update info provided.
            if (!values["UcdDeliveries"]) {
                return;
            }

            $scope.deliveries = $.parseJSON(values["UcdDeliveries"])
            $scope.hasNonManual = values["HasNonManual"] == "True";
            $scope.newDeliveryIdCounter = -1;
            $scope.HasDocMagicVendor = values.HasDocMagicVendor == "True";
            $scope.DocMagicDeliveryEnabled = values.DocMagicDeliveryEnabled == "True";
            $scope.DirectIntegrationEnabled = values.DirectIntegrationEnabled == "True";
            $scope.$apply();
            $('img[data-docmetadata]').docmetadatatooltip();
        }

        $scope.addDelivery = function () {
            $scope.deliveries.push(new UcdDelivery($scope.newDeliveryIdCounter));
            $scope.newDeliveryIdCounter--;
            updateDirtyBit();
        };

        $scope.removeDelivery = function (index) {
            var delivery = $scope.deliveries[index];
            delivery.isDeleted = true;
            $scope.markDirty(index);
            updateDirtyBit();
        };

        $scope.markDirty = function (index) {
            $scope.deliveries[index].isDirty = true;
        }

        $scope.changeDeliveredTo = function (index) {
            $scope.markDirty(index);
            var delivery = $scope.deliveries[index];
            if (delivery.DeliveredTo === Enums.UcdDeliveredToEntity.FreddieMac && delivery.IncludeInUldd) {
                delivery.IncludeInUldd = false;
            }
        };

        $scope.fixUpUlddCheckboxes = function (index) {
            $scope.markDirty(index);

            var delivery = $scope.deliveries[index];

            // Uncheck all other IncludeInUldd checkboxes if this box was checked.
            if (delivery.IncludeInUldd)
            {
                for (var i = 0; i < $scope.deliveries.length; i++) {
                    if (i != index && $scope.deliveries[i].IncludeInUldd) {
                        $scope.deliveries[i].IncludeInUldd = false;
                        $scope.markDirty(i);
                    }
                }
            }
        }

        $scope.DocMagicGenerateUCD = function () {
            $scope.openDeliveryPopup('generateOnly');
        }

        $scope.DocMagicGenerateAndDeliverUCD = function () {
            $scope.openDeliveryPopup('generateAndDeliver');
        }

        $scope.DeliverExistingUCD = function () {
            $scope.openDeliveryPopup('deliverExisting');
        }

        $scope.openDeliveryPopup = function (deliveryAction) {
            var url = ML.VirtualRoot + "/newlos/Services/GenerateUcd.aspx?loanid=" + ML.sLId + "&appid=" + ML.aAppId + "&deliveryAction=" + deliveryAction;

            LQBPopup.Show(url, {
                hideCloseButton: true,
                width: 1000,
                height: 600,
                onReturn: function (returnArgs) {
                    if (returnArgs) {
                        var json = returnArgs.ViewModelJSON;
                        var delivery = JSON.parse(json);
                        AppendDelivery(delivery);
                        return function () { ViewFindings(delivery) };
                    }
                }
            }, null);
        }

        $scope.viewFindings = function (index) {
            var delivery = $scope.deliveries[index];
            ViewFindings(delivery);
        }

        function ViewFindings(delivery) {
            LQBPopup.Show(ML.VirtualRoot + "/newlos/Finance/UcdDeliveryFindings.aspx?loanid=" + delivery.LoanId + '&deliveryId=' + delivery.UcdDeliveryId, {
                width: Math.floor($(window).width() * .9),
                height: Math.floor($(window).height() * .9)
            });
        }

        function AppendDelivery(delivery) {
            $scope.deliveries.push(delivery);
            //  We added one through generation, so this should be true now.
            $scope.hasNonManual = true;
            $scope.$apply();
        }

        $scope.uploadUcdDocument = function (index) {
            LQBPopup.Show(ML.VirtualRoot + "/newlos/ElectronicDocs/SingleDocumentUploader.aspx?loanid=" + ML.sLId + "&appid=" + ML.aAppId + "&restricttoucd=true", {
                onReturn: function (dialogArgs) {
                    if (dialogArgs.OK) {
                        if(dialogArgs.Document) {
                            var delivery = $scope.deliveries[index];
                            delivery.FileDeliveredDocumentId = dialogArgs.Document.DocumentId;
                            delivery.CanViewFileDeliveredDocument = true;
                            $scope.markDirty(index);
                            updateDirtyBit();
                            $scope.$apply();    // This callback occurs outside of ng-click, so we must apply for UI to update.
                        }
                    }
                    else {
                        alert("The document could not be uploaded. Please ensure you're uploading a valid UCD file.");
                    }
                },
                hideCloseButton: true
            });
        }

        $scope.associateUcdDocument = function (index) {
            var associatedIds = [];
            for (var i = 0; i < $scope.deliveries.length; i++) {
                if ($scope.deliveries[i].FileDeliveredDocumentId) {
                    associatedIds.push($scope.deliveries[i].FileDeliveredDocumentId);
                }
            }

            LQBPopup.Show(ML.VirtualRoot + "/newlos/ElectronicDocs/SingleDocumentPicker.aspx?loanid=" + ML.sLId  + "&restricttoucd=true" + "&associateducddocs=" + JSON.stringify(associatedIds), {
                onReturn: function (dialogArgs) {
                    if (dialogArgs.OK) {
                        var delivery = $scope.deliveries[index];
                        delivery.FileDeliveredDocumentId = dialogArgs.Document.DocumentId;
                        delivery.CanViewFileDeliveredDocument = true;
                        $scope.markDirty(index);
                        updateDirtyBit();
                        $scope.$apply();    // This callback occurs outside of ng-click, so we must apply for UI to update.
                    }
                },
                hideCloseButton: true
            });
        }

        $scope.viewUcdDocument = function (index) {
            var delivery = $scope.deliveries[index];
            window.open(ML.VirtualRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx" + '?docid=' + delivery.FileDeliveredDocumentId, '_self');
        }

        $scope.downloadUcdDocument = function (index) {
            var delivery = $scope.deliveries[index];
            window.open(ML.VirtualRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx" + '?cmd=xml&docid=' + delivery.FileDeliveredDocumentId, 'xmldoc');
        }

        $scope.clearUcdDocument = function (index) {
            var delivery = $scope.deliveries[index];

            if (confirm("Are you sure you want to clear the UCD file associated with this delivery row?")) {
                var delivery = $scope.deliveries[index];
                delivery.FileDeliveredDocumentId = '';
                delivery.FileDeliveredDocumentMetadataJSON = '';
                $scope.markDirty(index);
                updateDirtyBit();
            }
        }

        $scope.checkCanRetrieve = function (index) {
            var delivery = $scope.deliveries[index];
            return (delivery.UcdDeliveryType == 0 /*UcdDeliveryType.Manual*/ || delivery.UcdDeliveryType == 1 /*UcdDeliveryType.FannieMaeUcdDelivery*/) &&
                   delivery.DeliveredTo == Enums.UcdDeliveredToEntity.FannieMae;
        }

        $scope.IsNewOrDirty = function (index) {
            var delivery = $scope.deliveries[index];
            return delivery.IsNew || delivery.isDirty;
        }

        $scope.retrieveFindings = function (index) {
            var delivery = $scope.deliveries[index];
            if (delivery.DeliveredTo != Enums.UcdDeliveredToEntity.FannieMae) {
                return; // retrieve is only supported by Fannie
            }

            var hasAutoCredentials = CredentialDataMap[Enums.UcdDeliveryTargetOption.FannieMaeUcdCollectionSolution];
            if (hasAutoCredentials) {
                var data = {
                    AccountId: '',
                    Username: '',
                    Password: '',
                    DeliveryId: $scope.deliveries[index].UcdDeliveryId,
                    LoanId: ML.sLId,
                    AppId: ML.aAppId
                };

                RetrieveFindings(data, index);
            }
            else {
                var credentialHeader = "Fannie Mae Credentials";
                var popup = CredentialsPopup.CreateCredentialsPopup(credentialHeader, false, false, !hasAutoCredentials);
                LQBPopup.ShowElement(popup, {
                    width: 350,
                    height: 200,
                    hideCloseButton: true,
                    elementClasses: 'FullWidthHeight',
                    onReturn: function (returnArgs) {
                        if (typeof (returnArgs) != 'undefined' && returnArgs != null) {
                            var data = {
                                AccountId: returnArgs.AccountId,
                                Username: returnArgs.Username,
                                Password: returnArgs.Password,
                                DeliveryId: $scope.deliveries[index].UcdDeliveryId,
                                LoanId: ML.sLId,
                                AppId: ML.aAppId
                            };

                            return function () { RetrieveFindings(data, index); };
                        }
                    }
                });
            }
        }

        function RetrieveFindings(data, itemIndex) {
            var loadingPopup = SimplePopups.CreateLoadingPopup("Retrieving findings...");
            LQBPopup.ShowElement($('<div>').append(loadingPopup), {
                width: 350,
                height: 200,
                hideCloseButton: true,
                elementClasses: 'FullWidthHeight'
            });

            var result = gService.loanedit.callAsync("RetrieveFromFannie", data, true, false, true, true,
                   function (results) {
                       LQBPopup.Return(null);
                       HandleRetrieveRequestResults(result, itemIndex);
                   },
                   function (results) {
                       LQBPopup.Return(null);
                       alert(results.UserMessage);
                   }, null);
        }

        function HandleRetrieveRequestResults(results, itemIndex) {
            if (results.error) {
                alert(results.UserMessage);
            }
            else if (results.value.Success.toLowerCase() !== 'true') {
                var errors = JSON.parse(results.value.Errors);
                var errorPopup = SimplePopups.CreateErrorPopup("Something went wrong", errors);
                LQBPopup.ShowElement($('<div>').append(errorPopup), {
                    width: 350,
                    height: 200,
                    hideCloseButton: true,
                    elementClasses: 'FullWidthHeight'
                });
            }
            else {
                var successPopup = SimplePopups.CreateAlertPopup(results.value.Message, '', ML.VirtualRoot + '/images/success.png');
                LQBPopup.ShowElement($('<div>').append(successPopup), {
                    width: 350,
                    height: 200,
                    hideCloseButton: true,
                    elementClasses: 'FullWidthHeight',
                    onReturn: function () {
                        var json = results.value.DeliveryItem;
                        var delivery = JSON.parse(json);
                        $scope.deliveries[itemIndex] = delivery;
                        $scope.$apply();

                        return function () { ViewFindings(delivery) };
                    }
                });

                
            }
        }
    });
}