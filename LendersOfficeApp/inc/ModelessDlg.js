//
// Show modeless dialogs.  This simple api is used to
// provide a single implementation so that one change
// quickly propogates throughout the application.
// Note that we return the window, not the args.
//

var g_CurrentModelessDlg = null;

function showSoloModeless( sUrl, sOpts, sTitle, oArgs ) 
{ 
    if (null == sTitle)
      sTitle = "";
      
	  var w = null , a = oArgs || new Object;
  	
	  a.opener = window;
	  a.OK     = false;

	  if( g_CurrentModelessDlg != null )
	  {
		  g_CurrentModelessDlg.close();
		  g_CurrentModelessDlg = null;
	  }

	  if( sOpts == null )
	  {
		  w = g_CurrentModelessDlg = window.showModelessDialog
			  ( VRoot + "/Common/ModalDlg/DialogFrame.aspx?url=" + escape( VRoot + sUrl ) + "&title=" + escape(sTitle)
			  , a
			  , "dialogWidth: 200px; dialogHeight: 200px; center: yes; resizable: yes; scroll: yes; status: yes; help: no;"
			  );
	  }
	  else
	  {
		  w = g_CurrentModelessDlg = window.showModelessDialog
			  ( VRoot + "/Common/ModalDlg/DialogFrame.aspx?url=" + escape( VRoot + sUrl ) + "&title=" + escape(sTitle)
			  , a
			  , sOpts
			  );
	  }

	  w.opener = window;

	  return w;
}

function showModeless( sUrl , sOpts, sTitle, oArgs, bUsingDialogFrame ) {	
	sTitle = sTitle || "";

	if( g_CurrentModelessDlg != null ){
		g_CurrentModelessDlg.close();
		g_CurrentModelessDlg = null;
	}

	var modalUrl =  !bUsingDialogFrame ? sUrl  : VRoot + "/Common/ModalDlg/DialogFrame.aspx?url=" + escape( VRoot + sUrl ) + "&title=" + escape(sTitle);
	var modalFeatures =  sOpts || "dialogWidth: 200px; dialogHeight: 200px; center: yes; resizable: yes; scroll: yes; status: yes; help: no;";
	if (typeof showModelessDialog != "function" ){
		return openWindowWithArguments(modalUrl, sTitle, modalFeatures, oArgs);
	}

	var args = oArgs || new Object();
	args.opener = window;
	args.OK     = false;
	var w = window.showModelessDialog(modalUrl, args, modalFeatures);
	w.opener = window;

	return w;	
} 
