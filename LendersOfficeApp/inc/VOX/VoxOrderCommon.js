﻿var VOXOrderCommon = (function () {
    var RowComparer;
    var GatherRequestData;
    var serviceType;
    var ToggleOrderBtn;
    var validStatusesAfterInitialRequest = null;
    var shouldPushOutErrors = false;

    jQuery(function ($) {
        $('body').on('change', '.RequiredInput', function () {
            ToggleOrderBtn();
            $(this).siblings('.RequiredImg').toggle(this.value === '');
        });

        $('a[data-sort="true"]').click(function () {
            var link = $(this);
            var table = $(this).closest('table').eq(0);
            var rows = table.find('tr:gt(0)').toArray().sort(RowComparer(link.data('sorttarget')));
            table.find('.SortImg').hide();

            link.data('asc', !link.data('asc'));
            if (!link.data('asc')) {
                rows = rows.reverse();
                link.siblings('img').prop('src', '../../images/Tri_Desc.gif').show();
            }
            else {
                link.siblings('img').prop('src', '../../images/Tri_Asc.gif').show();
            }

            for (var i = 0; i < rows.length; i++) {
                table.append(rows[i]);
            }
        });

        $('#CancelBtn').click(function () {
            parent.LQBPopup.Return(null);
        });

        $('#OrderBtn').click(function () {
            var creditCardData = null;
            var allowPayWithCcCb = $('#AllowPayWithCreditCard');
            if (allowPayWithCcCb.is(':visible') && allowPayWithCcCb.is(':checked')) {
                LQBPopup.Show(ML.VirtualRoot + "/newlos/Services/LoginScreen.aspx?lqbp=true&mn=true", {
                    hideCloseButton: true,
                    width: 620,
                    height: 350,
                    onReturn: function (returnArgs) {
                        if (typeof (returnArgs) == 'undefined' || returnArgs == null || returnArgs.OK === false) {
                            return;
                        }
                        else {
                            creditCardData = {
                                BillingFirstName: returnArgs.BillingFirstName,
                                BillingMiddleName: returnArgs.BillingMiddleName,
                                BillingLastName: returnArgs.BillingLastName,
                                BillingCardNumber: returnArgs.BillingCardNumber,
                                BillingCVV: returnArgs.BillingCVV,
                                BillingStreetAddress: returnArgs.BillingStreetAddress,
                                BillingCity: returnArgs.BillingCity,
                                BillingState: returnArgs.BillingState,
                                BillingZipcode: returnArgs.BillingZipcode,
                                BillingExpirationMonth: returnArgs.BillingExpirationMonth,
                                BillingExpirationYear: returnArgs.BillingExpirationYear
                            };

                            return function () {
                                RunAudit(creditCardData);
                            };
                        }
                    }
                }, null);
            }
            else {
                RunAudit(creditCardData);
            }
        });

        function RunAudit(creditCardData) {
            var data = GatherRequestData();
            if (data == null) {
                return;
            }

            data.ServiceType = serviceType;
            if (creditCardData != null) {
                $.extend(data, creditCardData);
            }

            var result = gService[serviceType].call("RunAudit", data);
            if (!result.error) {
                if (result.value["Success"].toLowerCase() === 'true') {
                    var auditResults = JSON.parse(result.value["AuditResults"]);
                    var auditPassed = result.value["AuditPassed"].toLowerCase() === 'true';
                    var auditPageTitle = serviceType + " Request Data Audit";

                    var auditData = {
                        AuditPageTitle: auditPageTitle,
                        AuditResults: auditResults,
                        AuditPassed: auditPassed
                    };

                    var page = AuditPage.CreateAuditPage(auditData);
                    //iOPM: 468981
                    LQBPopup.ShowElement($('<div>').append(page), {
                        width: 650,
                        height: 550,
                        popupClass: 'EditBackground',
                        elementClasses: 'FullWidthHeight OverflowAuto',
                        hideCloseButton: true,
                        onReturn: function (returnArgs) {
                            if (returnArgs.OK == true) {
                                return function () { RunRequest(data); };
                            }

                            return null;
                        }
                    });
                }
                else {
                    alert(result.value["Errors"]);
                }
            }
            else {
                alert(result.UserMessage);
            }
        }

        function RunRequest(data) {
            var loadingPopup = SimplePopups.CreateLoadingPopup("Placing Order...", serviceType + " Request");
            LQBPopup.ShowElement($('<div>').append(loadingPopup), {
                width: 350,
                height: 200,
                hideCloseButton: true,
                elementClasses: 'FullWidthHeight'
            });

            var result = gService[serviceType].callAsync("RunRequest", data, true, false, true, true,
                function (results) {
                    HandleRequestResults(results, serviceType);
                },
                function (results) {
                    LQBPopup.Return(null);
                    alert(results.UserMessage);
                }, ML.VOXTimeoutInMilliseconds);
        }

        function HandleRequestResults(result, serviceType) {
            if (!result.error) {
                if (result.value["Success"].toLowerCase() === 'true') {
                    if (result.value.PublicJobId) {
                        var publicJobId = result.value.PublicJobId;
                        var pollingIntervalInMilliseconds = parseInt(result.value.PollingIntervalInSeconds) * 1000;
                        var attemptNumber = 1;
                        window.setTimeout(function () {
                            PollForInitialResults(publicJobId, pollingIntervalInMilliseconds, attemptNumber, result.value, serviceType);
                        }, pollingIntervalInMilliseconds);
                    }
                    else {
                        LQBPopup.Return(null);
                        HandleSuccessResult(result);
                    }
                }
                else {
                    LQBPopup.Return(null);

                    var requestErrors = JSON.parse(result.value["Errors"]);
                    var errorPopup = CreateErrorPopup(requestErrors);
                    LQBPopup.ShowElement($('<div>').append(errorPopup), {
                        width: 350,
                        height: 200,
                        hideCloseButton: true,
                        elementClasses: 'FullWidthHeight'
                    });
                }
            }
            else {
                LQBPopup.Return(null);

                alert(result.UserMessage);
            }
        }

        function PollForInitialResults(publicJobId, pollingIntervalInMilliseconds, attemptNumber, previousResult, serviceType) {
            var args = {};
            args.LoanId = ML.sLId;
            args.PublicJobId = publicJobId;
            args.AttemptNumber = attemptNumber;
            for (var key in previousResult) {
                if (previousResult.hasOwnProperty(key)) {
                    args[key] = previousResult[key];
                }
            }

            gService[serviceType].callAsyncBypassOverlay("PollForInitialResults", args,
                function (result) {
                    if (result.error) {
                        LQBPopup.Return(null);
                        alert(result.UserMessage);
                    }
                    else if (result.value["Success"].toLowerCase() === 'true') {
                        if (result.value.PublicJobId) {
                            publicJobId = result.value.PublicJobId;
                            window.setTimeout(function () {
                                PollForInitialResults(publicJobId, pollingIntervalInMilliseconds, ++attemptNumber, previousResult, serviceType);
                            }, pollingIntervalInMilliseconds);
                        }
                        else {
                            LQBPopup.Return(null);
                            HandleSuccessResult(result);
                        }
                    }
                    else {
                        LQBPopup.Return(null);

                        var requestErrors = JSON.parse(result.value["Errors"]);
                        var errorPopup = CreateErrorPopup(requestErrors);
                        LQBPopup.ShowElement($('<div>').append(errorPopup), {
                            width: 350,
                            height: 200,
                            hideCloseButton: true,
                            elementClasses: 'FullWidthHeight'
                        });
                    }
            });
        }

        function HandleSuccessResult(result) {
            var orders = JSON.parse(result.value["Orders"]);
            var errors = [];
            var successfulOrders = [];
            var completedCount = 0;
            $.each(orders, function (index, order) {
                if (validStatusesAfterInitialRequest != null && typeof (validStatusesAfterInitialRequest[order.Status]) === 'boolean') {
                    successfulOrders.push(order);

                    // The true/false value determines whether this order is considered "Completed"(true) or "Pending"(false).
                    if (validStatusesAfterInitialRequest[order.Status]) {
                        completedCount++;
                    }
                }
                else {
                    errors.push(order);
                }
            });

            if (successfulOrders.length != 0) {
                var popup;
                if (completedCount == successfulOrders.length) {
                    popup = CreateCompletePopup(result.value["Errors"] && JSON.parse(result.value["Errors"]));
                }
                else {
                    popup = CreatePendingPopup(result.value["Errors"] && JSON.parse(result.value["Errors"]));
                }

                LQBPopup.ShowElement($('<div>').append(popup), {
                    width: 350,
                    height: 200,
                    elementClasses: 'FullWidthHeight',
                    hideCloseButton: true,
                    onReturn: function () {
                        var data = {
                            StitchedCacheKey: result.value["Key"]
                        };

                        if (shouldPushOutErrors) {
                            data.Orders = successfulOrders.concat(errors);
                        }
                        else {
                            data.Orders = successfulOrders;
                        }

                        parent.LQBPopup.Return(data);
                    }
                });
            }
            else {
                var errorArray = $.map(errors, function (value, index) {
                    return value.Error;
                });

                var errorPopup = CreateErrorPopup(errorArray);
                LQBPopup.ShowElement($('<div>').append(errorPopup), {
                    width: 350,
                    height: 200,
                    hideCloseButton: true,
                    elementClasses: 'FullWidthHeight',
                    onReturn: function () {
                        if (shouldPushOutErrors) {
                            var data = {
                                Orders: errors
                            };

                            parent.LQBPopup.Return(data);
                        }
                    }
                });
            }
        }

        function CreatePendingPopup(errors) {
            if (errors && errors.length > 0) {
                return SimplePopups.CreateErrorPopup("Some orders are pending, but one or more other orders had errors:", errors, serviceType + " Request");
            }
            return SimplePopups.CreateAlertPopup("Service order is pending.", "The vendor has received your order.", null, serviceType + " Request");
        }

        function CreateCompletePopup(errors) {
            if (errors && errors.length > 0) {
                return SimplePopups.CreateErrorPopup("Partial order results have been received, but one or more other orders had errors:", errors, serviceType + " Request");
            }
            return SimplePopups.CreateAlertPopup("Order results have been received.", "", '../../images/success.png', serviceType + " Request");
        }

        function CreateErrorPopup(errorMsg) {
            return SimplePopups.CreateErrorPopup("Something went wrong", errorMsg, serviceType + " Request");
        }
    });

    function _initializeCommonFunctions(data) {
        if (typeof (data['RowComparer']) === 'function') {
            RowComparer = data['RowComparer'];
        }

        if (typeof (data['GatherRequestData']) === 'function') {
            GatherRequestData = data['GatherRequestData'];
        }

        if (typeof (data['ServiceType']) === 'string') {
            serviceType = data['ServiceType'];
        }

        if (typeof (data['ToggleOrderBtn']) === 'function') {
            ToggleOrderBtn = data['ToggleOrderBtn'];
        }

        if (typeof (data['ValidStatusesAfterInitialRequest']) === 'object') {
            validStatusesAfterInitialRequest = data['ValidStatusesAfterInitialRequest'];
        }

        if (typeof (data['ShouldPushOutErrors']) === 'boolean') {
            shouldPushOutErrors = data['ShouldPushOutErrors'];
        }
    }

    function _bindCredentialOptions(usesAccountId, requiresAccountId, credentialId, serviceCredentialHasAccountId) {
        $('.CredentialItem').show();
        $('#UsernameRow, #PasswordRow').toggle(credentialId === null);
        $('#Username, #Password').toggleClass('RequiredInput', credentialId === null);
        $('#UsernameImg, #PasswordImg').toggle(credentialId === null);
        $('#AccountIdRow').toggle(usesAccountId && !serviceCredentialHasAccountId);
        var accountIdRowHidden = !$('#AccountIdRow').is(':visible');
        $('#AccountIdImage').toggle(!accountIdRowHidden && requiresAccountId && $('#AccountId').val() === '');
        $('#AccountId').toggleClass('RequiredInput', !accountIdRowHidden && requiresAccountId);

        var hideCredentialsRow = credentialId !== null && (serviceCredentialHasAccountId || !usesAccountId);
        $('#CredentialsRow').toggle(!hideCredentialsRow);

        if (typeof (serviceCredentialId) !== 'undefined') {
            serviceCredentialId = credentialId;
        }
    }

    return {
        InitializeCommonFunctions: _initializeCommonFunctions,
        BindCredentialOptions: _bindCredentialOptions
    };
})();

