﻿var SelfEmployment = (function () {
    var selfEmploymentTemplate;
    var states = [];
    var selfEmploymentPostChangeCallback;

    jQuery(function ($) {
        $.get(ML.VirtualRoot + "/newlos/Verifications/SelfEmployment.html", function (data) {
            selfEmploymentTemplate = $.template(data);
        });

        $('body').on('change', '.TaxesSelfPrepared', function () {
            var row = $(this).closest('.SelfEmploymentRow');
            if (this.checked) {
                row.find('input').not(this).not('[type="hidden"]').val('');
                row.find('input').not(this).not('[type="hidden"]').prop('disabled', true);
                row.find('input.RequiredInput').removeClass('RequiredInput');
                row.find('select').val('');
                row.find('select').prop('disabled', true);
                row.find('.RequiredImg').hide();
            }
            else {
                row.find('input').not(this).prop('disabled', false);
                row.find('select').prop('disabled', false);
                row.find('.RequiredImg').show();
                row.find('.CompanyName').addClass('RequiredInput');
                row.find('.StreetAddress').addClass('RequiredInput');
                row.find('.PhoneNumber').addClass('RequiredInput');
            }

            if (typeof (selfEmploymentPostChangeCallback) === 'function') {
                selfEmploymentPostChangeCallback();
            }
        });

        $('body').on('change', '.City, .Zipcode, .State', function () {
            var row = $(this).closest('td');
            var city = row.find('.City').val();
            var zipcode = row.find('.Zipcode').val();
            var state = row.find('.State').val();

            $(row).find('.RequiredImg').toggle(city === '' || zipcode === '' || state === '');
            if (typeof (selfEmploymentPostChangeCallback) === 'function') {
                selfEmploymentPostChangeCallback();
            }
        });

        $('body').on('click', '.TaxPrepContactInfo', function () {
            var row = $(this).closest('.SelfEmploymentRow');
            if (row.find('.TaxesSelfPrepared').is(':checked')) {
                return;
            }

            LQBPopup.Show(ML.VirtualRoot + "/los/RolodexList.aspx?isCurrentAgentOnly=true&loanid=" + ML.sLId, {
                hideCloseButton: true,
                width: 800,
                height: 400,
                onReturn: function (returnArgs) {
                    if (typeof (returnArgs) != 'undefined' && returnArgs != null) {
                        row.find('.CompanyName').val(returnArgs.AgentCompanyName);
                        row.find('.CompanyName').siblings('.RequiredImg').toggle(returnArgs.AgentCompanyName === '');

                        row.find('.StreetAddress').val(returnArgs.AgentStreetAddr);
                        row.find('.StreetAddress').siblings('.RequiredImg').toggle(returnArgs.AgentStreetAddress === '');

                        row.find('.City').val(returnArgs.AgentCity);
                        row.find('.State').val(returnArgs.AgentState);
                        row.find('.Zipcode').val(returnArgs.AgentZip);
                        row.find('.Zipcode').siblings('.RequiredImg').toggle(returnArgs.AgentCity === '' || returnArgs.AgentZip === '' || returnArgs.AgentState === '')

                        row.find('.PhoneNumber').val(returnArgs.PhoneOfCompany);
                        row.find('.PhoneNumber').siblings('.RequiredImg').toggle(returnArgs.PhoneOfCompany === '');

                        row.find('.FaxNumber').val(returnArgs.FaxOfCompany);

                        if (typeof (selfEmploymentPostChangeCallback) === 'function') {
                            selfEmploymentPostChangeCallback();
                        }
                    }
                }
            }, null);
        });
    });

    function _createSelfEmploymentRow(borrowerName, appId, borrowerType, key) {
        var data = {
            BorrowerName: borrowerName,
            AppId: appId,
            BorrowerType: borrowerType,
            Key: key,
            States: states
        };

        var row = $.tmpl(selfEmploymentTemplate, data);
        var needToInit = $(row).find('[preset]');
        $.each(needToInit, function (index, value) {
            _initMask(value);
        });

        return row;
    }

    function _removeSelfEmploymentRow(key, container) {
        if (key === null || key === '') {
            $(container).find('.SelfEmploymentRow').remove();
        }
        else {
            $(container).find('.Key[value="' + key + '"]').closest('.SelfEmploymentRow').remove();
        }
    }

    function _containsSelfEmploymentRow(key, container) {
        if (key === null || key === '') {
            return $(container).find('.SelfEmploymentRow').length > 0;
        }
        else {
            return $(container).find('.Key[value="' + key + '"]').closest('.SelfEmploymentRow').length > 0;
        }
    }

    function _checkAllSelfEmploymentFilledOut() {
        return $('.SelfEmploymentRow:visible').filter(function () {
                    if ($(this).find('.TaxesSelfPrepared').is(':checked')) {
                        return false;
                    }

                    var city = $(this).find('.City').val();
                    var zipcode = $(this).find('.Zipcode').val();
                    var state = $(this).find('.State').val();

                    return city === '' || zipcode === '' || state === '';
                }).length === 0;
    }

    function _retrieveAllSelfEmploymentSections(container) {
        var allData = [];
        $('.SelfEmploymentRow').each(function (index, element) {
            var data = {
                AppId: $(this).find('.AppId').val(),
                BorrowerType: $(this).find('.BorrowerType').val(),
                Notes: $(this).find('.Notes').val(),
                TaxesSelfPrepared: $(this).find('.TaxesSelfPrepared').is(':checked'),
                CompanyName: $(this).find('.CompanyName').val(),
                StreetAddress: $(this).find('.StreetAddress').val(),
                City: $(this).find('.City').val(),
                State: $(this).find('.State').val(),
                Zipcode: $(this).find('.Zipcode').val(),
                PhoneNumber: $(this).find('.PhoneNumber').val(),
                FaxNumber: $(this).find('.FaxNumber').val()
            };

            allData.push(data);
        });

        return allData;
    }

    return {
        CreateSelfEmploymentRow: _createSelfEmploymentRow,
        RemoveSelfEmploymentRow: _removeSelfEmploymentRow,
        ContainsSelfEmploymentRow: _containsSelfEmploymentRow,
        RegisterStates: function (stateList) { states = stateList; },
        RegisterPostChangeCallback: function (callback) { selfEmploymentPostChangeCallback = callback; },
        CheckAllSelfEmploymentFilledOut: _checkAllSelfEmploymentFilledOut,
        RetrieveAllSelfEmploymentSections: _retrieveAllSelfEmploymentSections
    };
})();
