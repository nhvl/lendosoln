﻿var SalaryKey = (function () {
    var salaryKeyTemplate;

    jQuery(function ($) {
        $.get(ML.VirtualRoot + "/newlos/verifications/SalaryKey.html", function (data) {
            salaryKeyTemplate = $.template(data);
        });
    });

    function _createSalaryKeyRow(borrowerName, appId, borrowerType, key) {
        var data = {
            BorrowerName: borrowerName,
            AppId: appId,
            BorrowerType: borrowerType,
            Key: key
        };

        return $.tmpl(salaryKeyTemplate, data);
    }

    function _removeSalaryKey(key, container) {
        if (key === null || key === '') {
            $(container).find('.SalaryKeyRow').remove();
        }
        else {
            $(container).find('.Key[value="' + key + '"]').closest('tr').remove();
        }
    }

    function _containsSalaryKey(key, container) {
        if (key === null || key === '') {
            return $(container).find('.SalaryKeyRow').length > 0;
        }
        else {
            return $(container).find('.Key[value="' + key + '"]').closest('tr').length > 0;
        }
    }

    function _retrieveAllSalaryKeys(container) {
        var allData = [];
        $(container).find('.SalaryKeyRow').each(function(index, element) {
            var data = {
                SalaryKey: $(this).find('.SalaryKey').val(),
                AppId: $(this).find('.AppId').val(),
                BorrowerType: $(this).find('.BorrowerType').val()
            };

            allData.push(data);
        });

        return allData;
    }

    return {
        CreateSalaryKeyRow: _createSalaryKeyRow,
        RemoveSalaryKey: _removeSalaryKey,
        ContainsSalaryKey: _containsSalaryKey,
        RetrieveAllSalaryKeys: _retrieveAllSalaryKeys
    };
})();
