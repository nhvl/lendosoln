﻿var VOAOrderRow = (function () {
    var voaRowTemplate;
    var voaPostTemplateLoadCallback;

    jQuery(function ($) {
        $.get(ML.VirtualRoot + "/newlos/verifications/voaorderrow.html", function (data) {
            voaRowTemplate = $.template(data);
            if (typeof (voaPostTemplateLoadCallback) == 'function') {
                voaPostTemplateLoadCallback();
            }
        });
    });

    function _createVoaRow(data) {
        return $.tmpl(voaRowTemplate, data);
    }

    return {
        CreateVoaRow: _createVoaRow,
        PostTemplateLoadCallback: function (callback) { voaPostTemplateLoadCallback = callback; }
    };
})();

