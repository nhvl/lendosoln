﻿var SSA89OrderRow = (function () {
    var ssa89RowTemplate;
    var ssa89PostTemplateLoadCallback;

    jQuery(function ($) {
        $.get(ML.VirtualRoot + "/newlos/verifications/SSA89OrderRow.html", function (data) {
            ssa89RowTemplate = $.template(data);
            if (typeof (ssa89PostTemplateLoadCallback) == 'function') {
                ssa89PostTemplateLoadCallback();
            }
        });

        $('body').on('click', '.StatusLink', function () {
            var popup = $('#SubProductStatusPopup');
            var statusLink = $(this);
            popup.find('.DmStatus').text(statusLink.siblings('.DmStatus').val());
            popup.find('.SsaStatus').text(statusLink.siblings('.SsaStatus').val());
            popup.find('.OfacStatus').text(statusLink.siblings('.OfacStatus').val());
            popup.find('.ChStatus').text(statusLink.siblings('.ChStatus').val());
            popup.find('.OrderNumber').text(statusLink.closest('.OrderRow').find('.OrderNumber').text());
            LQBPopup.ShowElement(popup, {
                width: 300,
                height: 200,
                hideCloseButton: true,
                elementClasses: 'FullWidthHeight'
            });
        });

        $('body').on('click', '.BackSubStatusBtn', function () {
            LQBPopup.Return(null);
        });
    });

    function _createSsa89Row(data) {
        return $.tmpl(ssa89RowTemplate, data);
    }

    return {
        CreateSsa89Row: _createSsa89Row,
        PostTemplateLoadCallback: function (callback) { ssa89PostTemplateLoadCallback = callback; }
    };
})();
