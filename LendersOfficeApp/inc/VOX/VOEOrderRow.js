﻿var VOEOrderRow = (function () {
    var voeRowTemplate;
    var voePostTemplateLoadCallback;

    jQuery(function ($) {
        $.get(ML.VirtualRoot + "/newlos/verifications/VOEOrderRow.html", function (data) {
            voeRowTemplate = $.template(data);
            if (typeof (voePostTemplateLoadCallback) == 'function') {
                voePostTemplateLoadCallback();
            }
        });
    });

    function _createVoeRow(data) {
        return $.tmpl(voeRowTemplate, data);
    }

    return {
        CreateVOERow: _createVoeRow,
        PostTemplateLoadCallback: function (callback) { voePostTemplateLoadCallback = callback; }
    };
})();

