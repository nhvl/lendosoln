  const vbConfirmNo = 7
  const vbConfirmYes = 6
  const vbConfirmCancel = 2
  const vbScriptEnableFlag = 1

  function ConfirmSaveNoCancel
    ' Display Yes, No with Exclaimation icon alert box.
    ' Ret = 6 - Yes, 7 - No, 2 - Cancel
    ConfirmSaveNoCancel = MsgBox("Do you want to save the changes?",4 + 48,"LendingQB")
  end function
  
  function ConfirmSave

    ' Display Yes, No, Cancel with Exclaimation icon alert box.
    ' Ret = 6 - Yes, 7 - No, 2 - Cancel
    ConfirmSave = MsgBox("Do you want to save the changes?",3 + 48,"LendingQB")
  end function
  function ConfirmSaveWithMessage(ByVal MessageText)
    ' Display Yes, No, Cancel with Exclaimation icon alert box.
    ' Ret = 6 - Yes, 7 - No, 2 - Cancel
    ConfirmSaveWithMessage = MsgBox(MessageText,3 + 48,"LendingQB")
  end function
  function ConfirmSaveWithMessageNoCancel(ByVal MessageText)
    ' Display Yes, No, with Exclaimation icon alert box.
    ' Ret = 6 - Yes, 7 - No
    ConfirmSaveWithMessageNoCancel = MsgBox(MessageText,4 + 48,"LendingQB")
  end function 
  function ConfirmAdd(ByVal MessageText)
    ' Display Yes, No, Cancel with Question icon alert box.
    ' Ret = 6 - Yes, 7 - No, 2 - Cancel
    ConfirmAdd = MsgBox(MessageText, 32 + 3 , "LendingQB")
  end function