﻿var LQBPopup = (function($) {
    var buttonPadding = 37.74;
    var $elementParent = null;
    var $elementChild = null;
    var finalDefaltWidth = 450;
    var finalDefaultHeight = 350;
    var $popup = null, $iframe, $element, $contentContainer, $header, $headerText, returnCallback = null, cancelCallback = null, _args = {}, popupClasses, elementClasses = null;
    var defaultSettings = {
        width           : 100,
        height          : 100,
        closeText       : 'Close',
        onClose         : null,
        onReturn        : null,
        hideCloseButton : false,
        modifyOverflow  : true,
        popupClasses    : null,
        elementClasses  : null,
        marginLeft      : null,
        marginTop       : null,
        context         : null,
        targetOffset    : null,
        isCopyHTML      : true,
        dialogClasses   : null
    };

    var $dialog = null;

    var originalHeight = 0;
    var originalWidth = 0;
    var hasBeenReverted;

    var context = null;

    function isCrossDomain() {
        try {
            if ($iframe.get(0).contentWindow.document != null) {
                return false;
            }
        }
        catch (e) {
            return true;
        }
    }

    function highestLqbPopupParent() {
        var highestParent = window;
        while (highestParent != highestParent.parent && isLqbPopup(highestParent)) {
            highestParent = highestParent.parent;
        }

        return highestParent;
    }

    function limitDimensionsToWindow(width, height) {
        var highestParent = highestLqbPopupParent();
        var dimensions = f_getWindowDimensions(highestParent);
        if (height.toString().indexOf('%') == -1) {
            height = Number(height) + (buttonPadding);
            var calculatedHeight = Math.min(dimensions.height - buttonPadding, height);
            height = calculatedHeight < 0 ? height : calculatedHeight;
        }

        if (width.toString().indexOf('%') == -1) {
            width = Number(width);
            var calculatedWidth = Math.min(dimensions.width - buttonPadding, width);
            width = calculatedWidth < 0 ? width : calculatedWidth;
        }

        return {
            height: height,
            width: width
        };
    }

    function determineDimensionSettings(settings) {
        settings = settings || {};

        var limitedDimensions = limitDimensionsToWindow(settings.width, settings.height);
        settings.height = limitedDimensions.height;
        settings.width = limitedDimensions.width;

        // position:fixed isn't supported by IE below 7
        var browserVersion = getBrowserVersion();
        if (browserVersion <= 7) {
            var dimensions = f_getWindowDimensions(window);
            settings.marginTop = determineMargin(dimensions.top , settings.height);
            settings.marginLeft = determineMargin(dimensions.left, settings.width);
        }

        if (settings.targetOffset != null) {
            settings.marginTop = 0;
            settings.marginLeft = 0;
        }

        return settings;
    }

    function determineMargin(scrollValue, settingValue) {
        if (settingValue.toString().indexOf('%') == -1) {
            return scrollValue - (settingValue / 2);
        }
        return "calc(" + scrollValue + "px - " + settingValue + "%)";
    }

    function _createPopup() {
        jQuery('body').first().append([
            '<div id="LQBPopupDiv">',

                '<div class="lqbpopup-content-container">',
                    '<div id="LQBPopupElement"></div>',
                    '<iframe id="LQBPopupDivFrame" scrolling="auto" src="about:blank" border="0" frameBorder="0"></iframe>',
                '</div>',
            '</div>'
        ].join(""));

        $popup = jQuery('#LQBPopupDiv');
        $iframe = $popup.find('iframe');
        $element = $popup.find('#LQBPopupElement');

        $contentContainer = $popup.children(".lqbpopup-content-container");

        if ($('.ui-widget-overlay').length == 0) {
            $('body').append($('<div class="ui-widget-overlay"></div>').css('visibility', 'hidden'));
        }

        $dialog = $popup.dialog({
            resizable: true,
            draggable: true,
            modal: true,
            close: function (event, ui) {
                if (!hasBeenReverted) {
                    _cancel();
                }
            },
            open: function(event, ui) {
                $popup.dialog('widget').position({ my: 'center', at: 'center', of: '.ui-widget-overlay', collision: 'fit' });
            }
        }); 
    }
    
    function _show(url, settings) {
        if (typeof (settings) == 'undefined') {
            settings = {};
        }

        settings.hideCloseButton = false;
        if ($popup == null) _createPopup();
        $popup.dialog('close');

        $iframe.show();
        settings = $.extend({isUsingDefaultWidth: settings.width == null, isUsingDefaultHeight: settings.height == null}, defaultSettings, settings);
        context = settings.context;

        popupClasses = settings.popupClasses;
        elementClasses = settings.elementClasses;

        $popup.addClass(popupClasses);
        $element.addClass(elementClasses);

        function onLoadResize() {
            $popup.show();
            removeOnLoadHandler($iframe.get(0), 'load', onLoadHandler, false);
            addOnLoadHandler($iframe.get(0), 'load', onLoadHeaderTitle);
            if (isCrossDomain() || !$iframe.get(0).contentWindow.hasBeenResized) {
                _resize(settings.width, settings.height, settings);
            }
        }

        function onLoadHeaderTitle(){
            if (!isCrossDomain()) {
                var $doc = $($iframe.get(0).contentWindow.document);
                var pageHeader = retrievePopupHeader($doc, "h4.page-header, body > div, body > div > div, body > table, body > div > table, form > div, form > div > div, form > table, form > div > table");

                if (pageHeader) {
                    pageHeader.hide();
                    updateDialogTitle(pageHeader.html());
                }
            }
        }

        function onLoadHandler() {
            $popup.dialog('open');
            onLoadResize();
            onLoadHeaderTitle();
        };
        
        addOnLoadHandler($iframe.get(0), 'load', onLoadHandler);
        $iframe.prop('src', url);

        $popup.addClass(popupClasses);
        $popup.dialog('widget').addClass(settings.dialogClasses);
        $element.addClass(elementClasses);
        if (settings.modifyOverflow) { jQuery(document.body).css("overflow", "hidden"); }
        hasBeenReverted = false;
    }

    function updateDialogTitle(html) {
        html = html == '' ? '&nbsp;' : html;
        $popup.dialog('widget').find('span.ui-dialog-title').html(html);
    }

    function _updateHeaderText(headerText) {
        headerText = headerText == '' ? '&nbsp;' : headerText;
        $popup.dialog('widget').find('span.ui-dialog-title').text(headerText); // sanatizes html.
    }

    function retrievePopupHeader($element, viableHeaderSelector) {
        var $viableHeaders = $element.find(viableHeaderSelector);
        if ($viableHeaders.length > 0) {
            for (var i = 0; i < $viableHeaders.length && i < 5; i++) {
                var $el = $($viableHeaders[i]);
                if ($el.is("h4.page-header")) {
                    return $el;
                }
                if ($el.is("div.FormTableHeader, div.MainRightHeader, div.topHeader, div.LoanFormHeader")) {
                    return $el;
                }
                if ($el.is("table")) {
                    if ($el.children("tr.FormTableHeader").length > 0) {
                        return $el.children("tr.FormTableHeader").children("td").first();
                    }
                    if ($el.children("tr.MainRightHeader").length > 0) {
                        return $el.children("tr.MainRightHeader").children("td").first();
                    }
                    if ($el.find("tr, tbody:first-child tr").first().children("td.MainRightHeader").length > 0) {
                        return $el.find("tr, tbody:first-child tr").first().children("td.MainRightHeader");
                    }
                    if ($el.find("tr, tbody:first-child tr").first().children("td.FormTableHeader").length > 0) {
                        return $el.find("tr, tbody:first-child tr").first().children("td.FormTableHeader");
                    }
                }
            }
        }

        return null;
    }

    function _showElement($el, settings) // show an element (such as a div)
    {
        if (typeof (settings) == 'undefined') {
            settings = {};
        }

        settings.hideCloseButton = false;
        if ($popup == null) _createPopup();

        settings = $.extend({ isUsingDefaultWidth: settings.width == null, isUsingDefaultHeight: settings.height == null }, defaultSettings, settings);

        var tabbableSelector = 'a, link, button, input, select, textarea';

        $el.find(tabbableSelector).each(function (i, el) {
            var $child = $(el);
            $child.attr('current-tabIndex', $child.prop('tabIndex'));
        });

        $element.show();
        $popup.dialog('open');

        if (settings.isCopyHTML) {
            $element.html($el.html());
            $elementParent = null;
        }
        else {
            $elementParent = $el.parent();
            $elementChild = $el;
            $element.empty().append($el);
        }

        setTimeout(function () {
            $element.find('input[tabindex], select[tabIndex]').each(function (i, el) {
                var $child = $(el);
                $child.attr('tabIndex', $child.attr('current-tabIndex'));
            });
        });
        $iframe.hide();

        popupClasses = settings.popupClasses;
        elementClasses = settings.elementClasses;

        $popup.addClass(popupClasses);
        $popup.dialog('widget').addClass(settings.dialogClasses);
        $element.addClass(elementClasses);

        if (!settings.hideCloseButton) {
            var pageHeader = retrievePopupHeader($element, "h4.page-header, .MainRightHeader, .FormTableHeader");

            if (pageHeader) {
                pageHeader.hide();
                updateDialogTitle(pageHeader.html());
            }
        }

        if (settings.modifyOverflow) { jQuery(document.body).css("overflow", "hidden"); }
        $popup.show();
        _resize(settings.width, settings.height, settings);
        hasBeenReverted = false;
    }

    function _hide(callback) {
        updateDialogTitle('');
        if ($iframe) {
            if (isCrossDomain()) {
                $iframe.prop('src', 'about:blank');
                _hideAndResetPopup(callback);
            } else {
                var iframeWindow = $iframe.get(0).contentWindow;
                var iframeBody = iframeWindow.document.getElementsByTagName('Body')[0];
                iframeWindow.location.replace('about:blank');
                $iframe.removeAttr('style');

                if ($elementParent) {
                    $elementParent.append($elementChild);
                }

                if ((iframeWindow && iframeWindow.onbeforeunload) || (iframeBody && iframeBody.onbeforeunload)) {
                    setTimeout(function () {
                        if (iframeWindow.location.href == 'about:blank') {
                            _hideAndResetPopup(callback);
                        }
                    });
                }
                else {
                    _hideAndResetPopup(callback);
                }
            }
        }
        else {
            _hideAndResetPopup(callback);
        }
    }

    function _hideAndResetPopup(callback) {
        _revertParentToOriginal();
        $popup.removeClass(popupClasses);
        $element.removeClass(elementClasses);
        $element.hide();
        $popup.removeClass('ChildVisible');
        returnCallback = null;
        cancelCallback = null;
        jQuery(document.body).css("overflow", "auto");

        if (typeof (callback) == 'function') {
            callback();
        }

        $popup.dialog('close');
    }

    function _showPopup(url, settings, args) {
        setTimeout(function () {
            _show(url, settings);
            if (settings && typeof (settings.onClose) === 'function') {
                cancelCallback = settings.onClose;
            }
            if (settings && typeof (settings.onReturn) === 'function') {
                returnCallback = settings.onReturn;
            }
            _args = (args != null) ? args : null;
        });
    }

    function _showPopupElement(url, settings, args) {
        setTimeout(function () {
            _showElement(url, settings);
            if (settings && typeof (settings.onClose) === 'function') {
                cancelCallback = settings.onClose;
            }
            if (settings && typeof (settings.onReturn) === 'function') {
                returnCallback = settings.onReturn;
            }
            _args = (args != null) ? args : null;
        });
    }

    function _cancel() {
        var postHideFunction = null;
        if (typeof (cancelCallback) === 'function') {
            var args = [].slice.call(arguments);
            postHideFunction = cancelCallback.apply(context || this, args);
        }

        _hide(postHideFunction);
    }

    function _return() {
        var postHideFunction = null;
        if (typeof (returnCallback) === 'function') {
            var args = [].slice.call(arguments);
            postHideFunction = returnCallback.apply(context || this, args);
        }

        _hide(postHideFunction);
    }

    function _resize(width, height, settings) {
        height = isNaN(height) ? height : Number(height) + buttonPadding;
        width = isNaN(width) ? width : Number(width);

        if ($popup == null) { return; }

        if (!isCrossDomain() && $iframe.length > 0) {
            $iframe[0].contentWindow.hasBeenResized = true;
        }

        settings = settings || {};
        if (!isCrossDomain() && (settings.isUsingDefaultWidth || settings.isUsingDefaultHeight)) {
            $popup.dialog('option', { 'width': defaultSettings.width, 'height': defaultSettings.height });
            $popup.show();
            var iFrameHref = $iframe[0].contentWindow.location.href;
            var isIframePopup = iFrameHref.length > 0 && iFrameHref !== 'about:blank';
            if (settings.isUsingDefaultWidth){
                width = isIframePopup ?  $iframe.contents().width() : $element.children().get(0).offsetWidth;
            }

            if (settings.isUsingDefaultHeight){
                height = (isIframePopup ?  $iframe.contents().height() : $element.children().get(0).offsetHeight) + buttonPadding;
            }

            width = width < finalDefaltWidth ? finalDefaltWidth : width;
            height = height < finalDefaultHeight ? finalDefaultHeight : height;
        }
        
        var parentLimitedSettings = _resizeLqbPopupParent(width, height);
        settings.height = isNaN(height) ? height : Math.min(height, parentLimitedSettings.height);
        settings.width = isNaN(width) ? width : Math.min(width, parentLimitedSettings.width);

        settings = determineDimensionSettings(settings);

        // Height should be fixed with the calc operator once the site is fully CB.
        $iframe.css({ 'width': '100%', 'height': '100%' });
        
        $popup.dialog('option', { 'width': settings.width, 'height': settings.height });

        settings = $.extend({}, defaultSettings, settings);
        if (settings.targetOffset != null) {
            var popupOffset = $popup.offset();
            w = settings.targetOffset.left - popupOffset.left;
            h = settings.targetOffset.top - popupOffset.top;

            $popup.dialog('widget').position({ my: 'center', at: 'center' + formatMarginForPosition(w) + ' center' + formatMarginForPosition(h + buttonPadding), of: '.ui-widget-overlay', collision: 'fit' });
        }
        else {
            $popup.dialog('widget').position({ my: 'center', at: 'center' + formatMarginForPosition(settings.marginLeft) + ' center' + formatMarginForPosition(settings.marginTop), of: '.ui-widget-overlay', collision: 'fit' });
        }

        return { width: settings.width - buttonPadding, height: settings.height - 2 * buttonPadding };
    }

    function formatMarginForPosition(marginVal) {
        if (!marginVal || marginVal == 0) {
            return ''
        }

        if (marginVal < 0) {
            return marginVal;
        }

        return '+' + marginVal;
    }

    function _resizeLqbPopupParent(width, height) {
        if (window != window.parent) {
            var parentLqbPopup = jQuery('#LQBPopupDiv:visible', window.parent.document).parent();
            if (parentLqbPopup.length > 0 && isLqbPopup(window)) {
                var parentWidth = parentLqbPopup.width();
                var parentHeight = parentLqbPopup.height();
                var newWidth = parentWidth;
                var newHeight = limitedParentHeight;
                var limitedParentHeight = parentHeight - buttonPadding;

                if (!isNaN(width)) {
                    width = parentWidth - width < buttonPadding ? width + buttonPadding : width;
                    newWidth = width > parentWidth ? width : parentWidth;
                }

                if (!isNaN(height)) {
                    height = limitedParentHeight - height < buttonPadding ? height + buttonPadding : height;
                    newHeight = height > limitedParentHeight ? height : limitedParentHeight;
                }

                if (newWidth != parentWidth || newHeight != limitedParentHeight) {
                    if (originalHeight == 0 && originalWidth == 0) {
                        originalHeight = parentHeight;
                        originalWidth = parentWidth;
                    }

                    return parent.LQBPopup.Resize(newWidth, newHeight);
                }
            }
        }

        var limitedDimensions = f_getWindowDimensions(window);
        return {
            width: !isNaN(width) ? Math.min(width, limitedDimensions.width - buttonPadding) : limitedDimensions.width - buttonPadding,
            height: !isNaN(height) ?  Math.min(height, limitedDimensions.height - buttonPadding) : limitedDimensions.height - buttonPadding
        };
    }

    function _revertParentToOriginal() {
        hasBeenReverted = true;
        var parentLqbPopup = jQuery('#LQBPopupDiv:visible', window.parent.document).parent();
        if (parentLqbPopup.length > 0 && isLqbPopup(window) && originalHeight != 0 && originalWidth != 0) {
            parent.LQBPopup.Resize(originalWidth, originalHeight);
            originalHeight = 0;
            originalWidth = 0;
        }
    }

    function _getCurrentlyDisplayedFilename() {
        if ($iframe == null || isCrossDomain()) {
            return '';
        }

        return $iframe.get(0).contentWindow.location.pathname.split('/').pop();
    }

    function _showPopupString(s, settings, args)
    {
        _showPopupElement($('<div>').html(s), settings, args);
    }

    return {
        Show: _showPopup,
        ShowElement: _showPopupElement,
        ShowString: _showPopupString,
        Hide: _cancel,
        Return: _return,
        GetArguments: function() { return _args; },
        Resize: _resize,
        GetCurrentlyDisplayedFilename: _getCurrentlyDisplayedFilename,
        UpdateHeaderText: _updateHeaderText
    };
})(jQuery);

function isLqbPopup(window) {
    return (
        window.frameElement != null &&
        jQuery(window.frameElement).closest("#LQBPopupDiv").length > 0
    );
}
