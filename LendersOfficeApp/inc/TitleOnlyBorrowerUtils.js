﻿function _postGetAllFormValues(args) {
    serializeTitleOnlyBorrowerData();
    args['TitleOnlyBorrowerJSON'] = $('#TitleOnlyBorrowerJSON').val();
}

function _postRefreshCalculation(serviceResult) {
    var titleOnlyBorrowers = JSON.parse(serviceResult['TitleOnlyBorrowerJSON']);
    var titleOnlyBorrowersById = titleOnlyBorrowers.reduce(function (map, titleOnlyBorrower) {
        map[titleOnlyBorrower.Id] = titleOnlyBorrower;
        return map;
    }, {});

    $('#TitleOnylBorrowerTable li').each(function (index, listItem) {
        var $listItem = $(listItem);

        var titleOnlyBorrowerId = $listItem.attr('data-title-only-borrower-id');
        if (typeof titleOnlyBorrowerId === 'undefined') {
            // Template row
            return true;
        }

        var titleOnlyBorrower = titleOnlyBorrowersById[titleOnlyBorrowerId];

        $listItem.find('input.TitleNameOnCurrentTitle').val(titleOnlyBorrower.NameOnCurrentTitle);
        $listItem.find('input.TitleWillBeHeldInWhatName').val(titleOnlyBorrower.TitleWillBeHeldInWhatName);
    });
}

function serializeTitleOnlyBorrowerData() {
    var titleInfoString = generateTitleOnlyBorrowerJson();
    $('#TitleOnlyBorrowerJSON').val(titleInfoString);
}

function generateTitleOnlyBorrowerJson() {
    var titleInfo = [];
    $('#TitleOnylBorrowerTable li').each(function (i, o) {
        if (i === 0) {
            return;
        }
        var aliasString = $('input[id$=TitleAliases]', o).val();
        titleInfo.push({
            FirstNm: $('input.TitleFirstNm', o).val(),
            MidNm: $('input.TitleMidNm', o).val(),
            LastNm: $('input.TitleLastNm', o).val(),
            SSN: $('input.TitleSSN', o).val(),
            HPhone: $('input.TitleHPhone', o).val(),
            DOB: $('.TitleDOB', o).val(),
            Email: $('input.TitleEmail', o).val(),
            Address: $('input.TitleAddress', o).val(),
            City: $('input.TitleCity', o).val(),
            State: $('select.TitleState', o).val(),
            Zip: $('input.TitleZip', o).val(),
            RelationshipTitleT: $('select.TitleRelationshipTitleT', o).val(),
            RelationshipTitleTOtherDesc: $('input.TitleRelationshipTitleTOtherDesc', o).val(),
            AssociatedApplicationId: $('select.TitleAssociatedApplicationId', o).val(),
            Id: $('input.TitleId', o).val(),
            POA: $('input.TitlePOA', o).val(),
            Aliases: JSON.parse(aliasString),
            CurrentlyHoldsTitle: $('input.TitleCurrentlyHoldsTitle', o).prop('checked'),
            WillHoldTitle: $('input.TitleWillHoldTitle', o).prop('checked'),
            NameOnCurrentTitle: $('input.TitleNameOnCurrentTitle', o).val(),
            NameOnCurrentTitleLckd: $('input.TitleNameOnCurrentTitleLckd', o).prop('checked'),
            TitleWillBeHeldInWhatName: $('input.TitleWillBeHeldInWhatName', o).val(),
            TitleWillBeHeldInWhatNameLckd: $('input.TitleWillBeHeldInWhatNameLckd', o).prop('checked')
        });

    });
    return JSON.stringify(titleInfo);
}