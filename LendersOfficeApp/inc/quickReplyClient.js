﻿// assumes existence of top.frames['treeview']

function QuickReplyClient() {
    var targetHead = window.document.getElementsByTagName('head')[0];
    var treeviewFrame = top.frames['treeview'];
    var basicInfo = treeviewFrame.QRI.getBasicInfo();
    var instance = this;
    instance.loanId = basicInfo.loanId;
    instance.version = basicInfo.version;
    instance.virtualRoot = basicInfo.virtualRoot;

    addNecessaryScripts();

    function createConversationLogDiv() {
        if (window.document.getElementById('conversationLogDiv') !== null) {
            return;
        }

        var width = 700;
        var height = 150;

        var div = document.createElement('div');
        div.id = 'conversationLogDiv';
        div.style.width = width + 'px';
        div.style.overflowY = 'hidden';
        div.backgroundColor = 'white';
        div.innerHTML = '<iframe name="convLogFrame" src="' + virtualRoot + '/newlos/ConversationLogNewPost.aspx?loanid=' + encodeURIComponent(loanId) + '" style="width:' + width + 'px;height:' + height + 'px;border:0px" frameBorder="0"/>';
        document.body.appendChild(div);

        var divWidth = width + 30;
        var $conversationLogDialog = $("#conversationLogDiv").dialog(
            {
                title: "Conversation Log",
                width: divWidth,
                height: height,
                resizable: false,
                close: function (ui, event) {
                    var obj = treeviewFrame.QRI.getObject();
                    obj.divIsOpen = false;
                },
                open: function (ui, event) {
                    var obj = treeviewFrame.QRI.getObject();
                    obj.divIsOpen = true;
                }
            }).parent().css(
                {
                    position: "fixed",
                    border: "solid 1px black",
                    backgroundImage: "none"
            });

        $conversationLogDialog.find(".ui-dialog-titlebar").attr('style', 'background-color:Maroon; background-image:none; border:none')
            .find('.ui-dialog-title').attr('style', 'color:white;')

        $conversationLogDialog.find('.ui-dialog-titlebar-close').css("display", "inline-block");

        setConversationLogPosition();
        var obj = treeviewFrame.QRI.getObject();
        obj.divIsOpen = true;


        $(window).on('unload', function (e) {
            registerConversationLogPosition();
        });
    }

    function addNecessaryScripts() {
        injectLinkedScript('jquery-ui-1.11.4.min.js', createConversationLogDiv);
        injectLink({
            href: virtualRoot + '/css/jquery-ui-1.11.custom.css' + '?v=' + version,
            type: 'text/css',
            rel: 'stylesheet'
        });
    }

    function injectLink(obj) {
        var link = window.document.createElement('link');
        link.href = obj.href;
        link.type = obj.type;
        link.rel = obj.rel;
        targetHead.appendChild(link);
    }

    function injectLinkedScript(scriptUrl, onLoadHandler) {
        var script = window.document.createElement('script');
        script.src = virtualRoot + '/inc/' + scriptUrl + '?v=' + version;
        if (typeof onLoadHandler === 'function') {
            addOnLoadHandler(script, "load", onLoadHandler);
        }
        targetHead.appendChild(script);
    }

    function injectActualScript(scriptText) {
        var script = window.document.createElement('script');
        script.type = "text/javascript";
        script.text = scriptText;
        targetHead.appendChild(script);
    }

    function openConversationLog() {
        $("#conversationLogDiv").dialog("open");
    }
    function closeConversationLog() {
        $("#conversationLogDiv").dialog("close");
    }

    function registerConversationLogPosition() {
        var uiposition = $("#conversationLogDiv").dialog("option", "position");
        var obj = treeviewFrame.QRI.getObject();
        obj.position = uiposition;
    }

    function setConversationLogPosition() {
        var obj = treeviewFrame.QRI.getObject();
        var uiposition = obj.position || [0, 0];
        $("#conversationLogDiv").dialog("option", "position", uiposition);
    }

    function expandDialog(expand) {
        var height = expand ? 'auto' : 150;
        $('#conversationLogDiv').dialog("option", "height", height);
    }

    return {
        openConversationLog: openConversationLog,
        closeConversationLog: closeConversationLog,
        expandDialog: expandDialog
    };
}

var QRC = QuickReplyClient();
