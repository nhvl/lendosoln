﻿var pageTree;
var landingPageTree;
var favoriteTree;
var deletedFolders = [];
var addedFolders = [];
var dummyPage;
var dummyFolder;
var isFavoritesModified = false;

function setIsDirty(isDirty) {
    isFavoritesModified = isDirty;
    $('.favorite-apply').prop('disabled', !isDirty);
}

function initializeFolderEditor() {
    initializeDropdownCheckbox('.loan-purpose-dropdown-checkbox', $.parseJSON(ML.loanPurposes));
    initializeDropdownCheckbox('.loan-type-dropdown-checkbox', $.parseJSON(ML.loanTypes));

    if (ML.isAdmin) {
        initializeDropdownCheckbox('.role-dropdown-checkbox', $.parseJSON(ML.rolesInfo));
        initializeDropdownCheckbox('.employee-group-dropdown-checkbox', $.parseJSON(ML.employeeGroups));
    }
    else {
        $('.role-section, .group-section').hide();
    }
}

function InitializeFolderLinks(node, nodeSpan) {
    if (node.data.isFolder) {
        var $nodeSpan = $(nodeSpan);
        var h = hypescriptDom;
        $nodeSpan.after(
            h("span", {className:"link-span", attrs:{key: node.data.key}}, [
                h("a", {className:"add-subfolder"}, "add a subfolder"),
                h("a", {className:"edit-folder"}, "edit"),
                h("a", {className:"select-pages"}, "select pages"),
                (node.parent.parent != null) &&
                    h("a", {className:"delete-folder"}, "delete"),
            ])
        );
    }
}

function convertNodeToDynaModel(data) {
    var isFolder = data.PageId == undefined;
    if (isFolder) {
        data.title = data.Folder.InternalName;
        data.key = data.Folder.FolderId;
        data.isFolder = true;
        data.expand = true;

        data.Folder.RoleIds = data.Folder.RoleIds.$values;
        data.Folder.GroupIds = data.Folder.GroupIds.$values;
        data.Folder.LoanTypes = data.Folder.LoanTypes.$values;
        data.Folder.LoanPurposes = data.Folder.LoanPurposes.$values;

        if (data.Children && data.Children.$values && data.Children.$values.length) {
            data.children = data.Children.$values;
            for (var i = 0; i < data.Children.$values.length; i++) {
                convertNodeToDynaModel(data.Children.$values[i]);
            }
        }
    }
    else {
        data.key = data.PageId;
        data.title = data.LinkText;
    }
}

function convertDynaToNode(dynaNode) {
    var isFolder = dynaNode.data.isFolder;
    if (isFolder) {
        var folderNode = { };
        var folder = dynaNode.data.Folder;
        folderNode.isModified = true;
        folderNode.children = [];
        folderNode.Folder = folder;
        folderNode.SortOrder = dynaNode.data.SortOrder ? dynaNode.data.SortOrder : 0;
        folderNode.$type = dummyFolder.$type;

        if (dynaNode.childList) {
            for (var i = 0; i < dynaNode.childList.length; i++) {
                dynaNode.childList[i].data.SortOrder = i;
                folderNode.children.push(convertDynaToNode(dynaNode.childList[i], false));
            }
        }

        return folderNode;
    }
    else {
        var page = {};
        page.PageId = dynaNode.data.key;
        page.$type = dummyPage.$type;
        page.SortOrder = dynaNode.data.SortOrder;
        return page;
    }
}

function initializeFavoriteTree() {
    var rootNode = $.parseJSON(ML.treeRoot);
    convertNodeToDynaModel(rootNode);

    var args = {
        children: [rootNode],
        title: "Custom Folder Navigation",
        onCreate: InitializeFolderLinks,
        clickFolderMode: 1,
        rootVisible: true,
        dnd: {
            preventVoidMoves: true,
            onDragStart: function (node) {
                return true;
            },
            onDragEnter: function (node, sourceNode) {

                if (node.parent != sourceNode.parent || node.data.isFolder != sourceNode.data.isFolder) {
                    return false;
                }

                return ["before", "after"];
            },
            onDrop: function (node, sourceNode, hitMode, ui, draggable) {
                sourceNode.move(node, hitMode);
                setIsDirty(true);
            },
            onDragStop: function (sourceNode) {
                $.each(sourceNode.parent.childList, function (index, val) { val.data.SortOrder = index });
            }
        }
    };

    favoriteTree = $("#PageTree").dynatree(args).dynatree("getTree");
}

var alreadySelecting = false;
function selectPageFromPageSelection(selected, node) {
    if (!alreadySelecting) {
        alreadySelecting = true;

        $('.page-selection').find('a[key="' + node.data.key + '"]').each(function (index, el) {
            var duplicateNode = $.ui.dynatree.getNode(el);
            duplicateNode.select(selected);
        });

        alreadySelecting = false;
    }
}

function renderPageTreeSelectionNode(node, nodeSpan) {
    $(nodeSpan).find('a').attr('key', node.data.key);
}

function initializePageSelectionTree() {
    var pageTreeModel = $.parseJSON(ML.pageTreeModel);
    pageTreeModel.expand = true;
    pageTreeModel.checkbox = true;
    pageTreeModel.onSelect = selectPageFromPageSelection;
    pageTreeModel.onRender = renderPageTreeSelectionNode;

    pageTree = $('.page-selection').dynatree(pageTreeModel).dynatree("getTree");
}

function initializeLandingPageSelectionTree() {
    $('#LandingPageId').val(ML.landingPageId);
    var landingPageNode = pageTree.getNodeByKey(ML.landingPageId);
    if (landingPageNode) {
        $('#LandingPageDescription').text(landingPageNode.data.title);
    }

    var landingPageTreeModel = $.parseJSON(ML.landingPageTreeModel);
    landingPageTreeModel.expand = true;
    landingPageTreeModel.checkbox = true;
    landingPageTreeModel.selectMode = 1;
    landingPageTreeModel.classNames = {checkbox: 'dynatree-radio'};

    landingPageTree = $('.landing-page-selection').dynatree(landingPageTreeModel).dynatree("getTree");
}

$(function () {
    setIsDirty(false);
    $('#ButtonContainer').toggle(ML.isAdmin);
    initializeFolderEditor();
    initializeFavoriteTree();
    initializePageSelectionTree();
    if (!ML.isAdmin) {
        initializeLandingPageSelectionTree();
    }

    addHandlers();

    dummyFolder = $.parseJSON(ML.dummyFolder);
    dummyPage = $.parseJSON(ML.dummyPage);
});

function resetFolderEditor(node) {
    $(".internal-name").val(node ? node.data.Folder.InternalName : '');
    $(".external-name").val(node ? node.data.Folder.ExternalName : '');
    $(".lead-loan-status").val(node ? node.data.Folder.LeadLoanStatus : 0);
    setDropdownCheckboxValues(".role-dropdown-checkbox", node ? node.data.Folder.RoleIds : []);
    setDropdownCheckboxValues(".loan-type-dropdown-checkbox", node ? node.data.Folder.LoanTypes : []);
    setDropdownCheckboxValues(".loan-purpose-dropdown-checkbox", node ? node.data.Folder.LoanPurposes : []);
    setDropdownCheckboxValues(".employee-group-dropdown-checkbox", node ? node.data.Folder.GroupIds : []);
    validateFolderEditor();
}

function validateFolderEditor() {
    var isValid = $.trim($(".internal-name").val()).length > 0;
    $('.folder-editor-buttons .ok').prop('disabled', !isValid);

    var url = VRoot + '/images/' + (isValid ? 'pass.png' : 'fail.png');
    $('#InternalNameValidationImg').prop('src', url);
}

function getFolderEditorData(isAdd) {
    var data =
    {
        title: $(".internal-name").val(),
        isFolder: true,
        isModified: true,
        Folder: {
            InternalName: $(".internal-name").val(),
            ExternalName: $(".external-name").val(),
            LeadLoanStatus: $(".lead-loan-status").val(),
            RoleIds: getDropdownCheckboxValues(".role-dropdown-checkbox"),
            LoanTypes: getDropdownCheckboxValues(".loan-type-dropdown-checkbox"),
            LoanPurposes: getDropdownCheckboxValues(".loan-purpose-dropdown-checkbox"),
            GroupIds: getDropdownCheckboxValues(".employee-group-dropdown-checkbox")
        }
    };

    if (isAdd) {
        data.Folder.FolderId = -1;
    }

    return data;
}

function saveFavorites(isClose) {
    if (isFavoritesModified) {
        var data = convertDynaToNode(favoriteTree.getRoot().childList[0]);
        var landingPageId = ML.isAdmin ? '' : $('#LandingPageId').val();

        var args = { treeJSON: JSON.stringify(data), isAdmin: ML.isAdmin, landingPageId: landingPageId };
        gService.folderNavigation.callAsyncSimple("SaveTree", args, function () {
            setIsDirty(false);

            if (isClose) {
                closePage();
            }
        }, function (result) {
            if (result.error && result.UserMessage) {
                alert(result.UserMessage);
            }
            else {
                alert("Failed to save updates.  Please try again.");
            }
        });
    }
    else {
        if (isClose) {
            closePage();
        }
    }
}

function openFolderPageSelector() {
    var key = this.parentElement.getAttribute("key");
    var folderNode = favoriteTree.getNodeByKey(key);

    //reset the page values
    var pages = pageTree.getSelectedNodes();
    $.each(pages, function (i, node) {
        node.select(false);
    });

    $.each(folderNode.childList, function (i, node) {
        selectPageFromPageSelection(true, node);
    });

    //Set the pages
    LQBPopup.ShowElement($(".page-selection-container"), {
        isCopyHTML: false,
        height: 600,
        width: 600,
        onReturn: function () { updateFolderSelectedPages(folderNode) }
    });
}

function updateFolderSelectedPages(folderNode) {
    setIsDirty(true);
    folderNode.data.pages = [];
    var selectedPages = pageTree.getSelectedNodes();
    var children = folderNode.childList ? folderNode.childList : [];
    var firstFolder;
    var childrenIds = $.map(children, function (node, i) {
        firstFolder = firstFolder == null && node.data.isFolder ? node : firstFolder;
        return node.data.key;
    });
    var selectedPagesHash= {};
    $.each(selectedPages, function (i, node) {
        selectedPagesHash[node.data.key] = node;
    });

    var selectedIds = Object.keys(selectedPagesHash);

    var toBeDeleted = [];

    for (var i = 0; i < childrenIds.length; i++) {
        var child = children[i];
        if (!child.data.isFolder && selectedIds.indexOf(child.data.key) == -1) {
            toBeDeleted.push(child);
        }
    }

    $.each(toBeDeleted, function () {
        this.remove();
    });

    for (var i = 0; i < selectedIds.length; i++) {
        var node = selectedPagesHash[selectedIds[i]];
        if (childrenIds.indexOf(node.data.key) == -1) {
            folderNode.addChild(node.data, firstFolder);
        }
    }
}

function closePage() {
    window.parent.LQBPopup.Return();
}

function addHandlers() {
    $(document).on('click', '.favorite-ok', function () { saveFavorites(true); });
    $(document).on('click', '.favorite-apply', function () { saveFavorites(false); });
    $(document).on('click', '.favorite-cancel', closePage );
    $(document).on('click', '.cancel', function () { LQBPopup.Hide(); });
    $(document).on('click', '.ok', function () { LQBPopup.Return(); });
    $(document).on('input', '.internal-name', validateFolderEditor);

    $(document).on("click", "a.add-subfolder", function (e) {
        var key = this.parentElement.getAttribute("key");
        var node = favoriteTree.getNodeByKey(key);
        $('.folder-editor-header').text('Add Subfolder');
        resetFolderEditor(null);

        LQBPopup.ShowElement($(".folder-editor"), {
            popupClasses: "folder-editor EditBackground", onReturn: function (args) {
                setIsDirty(true);
                var data = getFolderEditorData(true);
                data.Folder.ParentFolderId = node.data.Folder.FolderId;
                node.addChild(data);
            },
            width: 600,
            height: ML.isAdmin ? 400 : 350,
            isCopyHTML: false
        });
    });

    $(document).on("click", "a.select-pages", openFolderPageSelector);

    $(document).on("click", ".landing-page a.change-link", function (e) {

        //reset the page values
        var pages = landingPageTree.getSelectedNodes();
        $.each(pages, function (i, node) {
            node.select(false);
        });

        landingPageTree.selectKey($('#LandingPageId').val());

        //Set the pages
        LQBPopup.ShowElement($(".landing-page-selection-container"), {
            isCopyHTML: false,
            height: 600,
            width: 600,
            onReturn: function () {
                setIsDirty(true);
                var selectedPages = landingPageTree.getSelectedNodes();
                if (selectedPages.length > 0) {
                    $('#LandingPageDescription').text(selectedPages[0].data.title);
                    $('#LandingPageId').val(selectedPages[0].data.key);
                }
            }
        });
    });

    $(document).on('click', '.landing-page .default-link', function () {
        setIsDirty(true);
        $("#LandingPageId").val("");
        $("#LandingPageDescription").text("Default");
    });

    $(document).on("click", "a.edit-folder", function (e) {
        var key = this.parentElement.getAttribute("key");
        var node = favoriteTree.getNodeByKey(key);

        $('.folder-editor-header').text('Edit Subfolder');
        resetFolderEditor(node);

        LQBPopup.ShowElement($(".folder-editor"), {
            popupClasses: "folder-editor EditBackground",
            isCopyHTML: false,
            onReturn: function (args) {
                setIsDirty(true);
                var newData = getFolderEditorData(false);
                var oldFolderData = node.data.Folder;
                var updatedFolderData = $.extend(node.data.Folder, newData.Folder);
                $.extend(node.data, newData);
                node.data.Folder = updatedFolderData;
                node.render();
            },
            width: 600,
            height: ML.isAdmin ? 400 : 350
        });
    });

    $(document).on("click", "a.delete-folder", function (e) {
        setIsDirty(true);
        var key = $(this).parent().attr("key");
        deletedFolders.push(key);
        favoriteTree.getNodeByKey(key).remove();
    });
}
