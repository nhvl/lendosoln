﻿// This is intended for use when jQuery is added to the page via EnableJQuery.  Do not use this otherwise.

if (typeof ($j) == "undefined")
{
    // This prevents jQuery from taking the $ variable, and instead stores it within $j.
    window.$j = $.noConflict();
    window.$ = $j;
}
else {
    // If $j already exists, then there's no need to override the value.
    //$.noConflict();
}