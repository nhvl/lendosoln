// This script file is for class inherit from BaseLoanPage.
function f_addToHistory(url, title, pageid) {
  // parent.info is null when this loan page is inside another frame.
  // ex: Liability list / Asset list.
  if (null != parent.info) {
    
    parent.info.f_pushHistory(url, title);
    if (!isDirty())
      parent.info.f_enableSave(false);
  }
  if (null != parent.treeview && typeof parent.treeview.selectPageID === 'function') {

    parent.treeview.selectPageID(pageid);
  }
}
function f_refreshLoanSummary() {
  if (parent.info != null)
    parent.info.f_refreshInfo();
}
function f_setCurrentApplicationID(id, pageid) {
  parent.info.f_setCurrentApplicationID(id);
  if (null != pageid && null != parent.treeview) {
    parent.treeview.selectPageID(pageid);

  }
}
function f_accessDenied(errmsg) {
    setTimeout("parent.close()", 1000);
    alert(errmsg);
}

function f_displayAlertMessage(msg) {
    LQBPopup.ShowString(msg, {
        width: 420,
        height: 180,
        modifyOverflow: false,
        popupClasses: "LQBPopupAlert"
    });
}

var g_bPageProcessing = false;
function f_smartConfirm(normalHandler, postbackHandler, bCancel, postSaveHandler) {
    if (g_bPageProcessing)
        return false; 
    
    if (!isDirty()){
        if (null != normalHandler){
            normalHandler();
        }
        return true;
    }
    
    // OPM 6295 - If input range fail then don't move to different page.
    if (!gIsInputRangeValid) return false;

    var showCancel = (bCancel == null || bCancel);
    PolyConfirmSave(function(confirmed){
        var bSuccess = true;

        if (confirmed) {
            g_bPageProcessing = true;
            if (typeof (saveAndRedirect) == 'function') {
                saveAndRedirect(postbackHandler);
                return true;
            }
            else if (typeof (saveMe) == 'function') {
                bSuccess = saveMe(false); // User will redirect to different screen. Don't reload data for old screen after save.
                g_bPageProcessing = false;
            }
            else {
                if (null != postbackHandler)
                    postbackHandler();
                return true;
            }

            if(postSaveHandler) postSaveHandler();
        }

        if (bSuccess) {
            clearDirty();
        }

        if (null != normalHandler && bSuccess)
        normalHandler();
    }, showCancel);       
    
    return true;
}
function f_goBackDesktop() {
  f_smartConfirm(function() {
                              if (parent.opener != null && parent.opener.parent.frmCode != null) {
                                parent.opener.parent.focus(); // Focus parent. 
                              } else {
                                parent.location = gVirtualRoot + '/los/main.aspx'; 
                              }
                            },
                 function() { __doPostBack('', 'gotodesktop'); }
                 );
}
function f_exportLoan(loanid) {
  f_smartConfirm(function() { window.open(gVirtualRoot + '/LegacyLink/LoanExportDlg.aspx?loanid=' + loanid, 'export', 'resizable=yes,scrollbars=no,menubar=no,toolbar=no,status=yes,height=300px,width=400px'); },
                 function() { __doPostBack('', 'exportloan'); }
                 );
}

function __duplicateLoan() {
    clearDirty();
    if (confirm('Do you want to duplicate this loan?')) {

        CreateNewLoanFileFromLoan("CreateDuplicateLoan");
        // __doPostBack('', 'duplicateloan');
    }
}

function CreateNewLoanFileFromLoan(method) {
    gService.utils.callAsyncSimple(method, { LoanID: ML.sLId }, function (result) {
        if (!result.error) {

            if (result.value['Error'] == 'True') {
                alert(result.value['UserMessage']);
            }

            f_openLoan(result.value['NewLoanID'], result.value['LoanName']);
        }
    });    
}

function f_duplicateLoan() {
  f_smartConfirm(__duplicateLoan, __duplicateLoan);
}

function __createTest() {
    clearDirty();
    if (confirm('Do you want to create a test file?')) {

        CreateNewLoanFileFromLoan("CreateTestLoan");
        // __doPostBack('', 'createtest');
    }
}


function f_createTest() {
    f_smartConfirm(__createTest, __createTest);
}

function __createSandbox() {
    clearDirty();
    if (confirm('Do you want to create a sandbox file?')) {
        CreateNewLoanFileFromLoan('CreateSandboxLoan');
        // __doPostBack('', 'createsandbox');
    }
}

function f_createSandbox(){
    f_smartConfirm(__createSandbox, __createSandbox);
}

function __createLoanTemplate() {
    clearDirty();
    if (confirm('Do you want to create template from this loan?')) {
        CreateNewLoanFileFromLoan("CreateTemplateFromLoan");
        // __doPostBack('', 'createloantemplate');
    }
    
}
function f_createLoanTemplate() {
  f_smartConfirm(__createLoanTemplate, __createLoanTemplate);
}
function f_openLead(loanid, loanname) {
    var w = window.parent.opener;
    if (null != w && !w.closed) {
        var frmCode = retrieveFrame(w.parent, "frmCode");

        if (frmCode != null) {
            frmCode.editLead(loanid, loanname);
            return;
        }
    }
    // 8/2/2004 dd - No Pipeline existed
    f_openLeadWindowNoPipeline(loanid, loanname);

}

function f_openLeadWindowNoPipeline(loanid, loanname) {
    var bMaximize = true;
    var bCenter = false;

    var w = 0;
    var h = 0;
    var left = 0;
    var top = 0;

    if (!bMaximize) {
        var preferedWidth = 1024;
        var preferedHeight = 768;
        w = preferedWidth < screen.width ? preferedWidth : screen.width - 10;
        h = preferedHeight < screen.height ? preferedHeight : screen.height - 60;
    } else {
        w = screen.availWidth - 10;
        h = screen.availHeight - 50;
    }

    if (bCenter) {
        left = (screen.width - w) / 2;
        top = (screen.height - h - 50) / 2;
    }

    var url = gVirtualRoot + "/los/lead/leadmainframe.aspx";
    var options = "height=" + h + ",width=" + w + ",left=" + left + ",top=" + top + ",location=no,menubar=no,resizable=yes,status=yes,toolbar=no";
    var gWindow = window.open(url + '?loanid=' + loanid, 'LoanEdit_' + loanid.replace(/-/gi, ""), options, false);
    gWindow.focus();

}

function f_openLoan(loanid, loanname) {
  var w = window.parent.opener;
  if (null != w && !w.closed) {
    var frmCode = retrieveFrame(w.parent, "frmCode");
    if (frmCode != null) {
      frmCode.editLoan(loanid, loanname);
      return;
    }
  }
  // 8/2/2004 dd - No Pipeline existed
  f_openLoanWindowNoPipeline(loanid, loanname);
  
}
function f_openLoanWindowNoPipeline(loanid, loanname) {
    var bMaximize = true;
    var bCenter = false;
    
    var w = 0;
    var h = 0;
    var left = 0;
    var top = 0;
    
    if (!bMaximize) {
      var preferedWidth = 1024;
      var preferedHeight = 768;
      w = preferedWidth < screen.width ? preferedWidth : screen.width - 10;
      h = preferedHeight < screen.height ? preferedHeight : screen.height - 60;
    } else {
      w = screen.availWidth - 10;
      h = screen.availHeight - 50;
    }
    
    if (bCenter) {
      left = (screen.width - w) / 2;
      top = (screen.height - h - 50) / 2;
    }
  
    var url = gVirtualRoot + "/newlos/loanapp.aspx";
    var options = "height=" + h + ",width=" + w + ",left=" + left + ",top=" + top + ",location=no,menubar=no,resizable=yes,status=yes,toolbar=no";
    var gWindow = window.open(url + '?loanid=' + loanid, 'LoanEdit_' + loanid.replace(/-/gi,""), options, false);
    gWindow.focus();

}
function f_logout() {
  f_smartConfirm(function() { parent.location = gVirtualRoot + '/logout.aspx'; },
                 function() { __doPostBack('', 'logout'); }
                );
}
function f_closeMe(bCancel) {
  return f_smartConfirm(function() {
                              var w = window.parent.opener;
                              if (null != w && !w.closed) {
                                w.focus(); 
                              } 
                              window.parent.close(); 
                            },
                 function() { __doPostBack('', 'closewindow'); },
                 bCancel
                );

}

function f_smartLoad(href, useFullEditor) {
    var fullHref = getFullRef(href);
    var handler = function () {
        if (useFullEditor) {
            window.top.location.href = fullHref;
        } else {
            self.location = fullHref;
        }
    };

    f_smartConfirm(handler, handler);
}


function f_load(href, useFullEditor) {
  // Load but get current selected application.
  
  f_smartLoad(href + "&appid=" + encodeURIComponent(parent.info.f_getCurrentApplicationID()), useFullEditor);
}

function f_loadFromHistory(href) {
  f_smartLoad(href + "&history=t");
}
// User press save explicitly.
function f_saveMe() {
  var bSuccess = true;
  if (typeof(saveMe) == 'function'){
      bSuccess = saveMe(true);
  }
  else __doPostBack('', '');
  return bSuccess;
}

// Temporary hack. dd 2/11/04
function f_getPrintID() {
  return (document.getElementById("PrintID") != null) ? document.getElementById("PrintID").value : "";
}

function f_displayVersionMismatch()
{
    showModal('/newlos/VersionMismatchDialog.aspx', null, null, null, function (args) {
        if (args.Action === 'Print') {
            var printFrame = window.frames['mainframeset'] || window;
            lqbPrintByFrame(printFrame);
        }
        location.href = location.href;
    }, { hideCloseButton: true });
}

function f_displayFieldWriteDenied(msg) {
    showModal('/newlos/LoanSaveDenial.aspx?msg=' + encodeURI(msg), null, null, null, function (args) {
        if (args.Action === 'Print') {
            var printFrame = window.frames['mainframeset'] || window;
            lqbPrintByFrame(printFrame);
        }
        location.href = location.href;
    }, { hideCloseButton: true });
}
function f_disableLeadOnlyLinksAndButtons() {
    var links = document.getElementsByTagName('a'); 
    for(var i = 0; i < links.length; i++)
    {
        var obj = links[i];
        if(obj.getAttribute('disableforlead') == null)
        {
            continue;
        }

        obj.onclick = function() { return false; };
        obj.style.color = "Gray";
        obj.style.textDecoration = "none"; ;
                
    } 
      
    var inputs = document.getElementsByTagName('input'); 
    for(var i = 0; i < inputs.length; i++)
    {
        var obj = inputs[i];
        if(obj.getAttribute('disableforlead') == null)
        {
            continue;
        }
        if(obj.type == 'button')
        {
            obj.disabled = true;
        }
    }
}
function f_addsSpAddrValidatorForCE() {
    var sSpAddr = document.getElementById('sSpAddr') || document.getElementById(getAllFormValues('sSpAddr')._ClientID + '_sSpAddr');
    if(sSpAddr != null) {
        oldsSpAddrVal = sSpAddr.value;
        gService.register(ML.VirtualRoot + '/newlos/ComplianceEasesSpAddrValidationService.aspx', 'complianceeasesspaddrvalidator');
        addEventHandler(sSpAddr, 'blur', f_validate_sSpAddrForCE, false);
    }
}
function f_validate_sSpAddrForCE() {
    var sSpAddr = retrieveEventTarget(arguments[0]);
    if (oldsSpAddrVal != sSpAddr.value) {
        var args = {};
        args.sLId = ML.sLId;
        args.sSpAddr = sSpAddr.value;
        gService.complianceeasesspaddrvalidator.callAsyncSimple('ValidatesSpAddr', args, function (result) {
            if (!result.error) {
                if (result.value['sSpAddrIsNotValid'] === 'true') {
                    alert('The property street address is using an invalid format and cannot be sent to ComplianceEase.' +
                    '  To ensure accurate compliance results, please try entering the address in a different format.');
                }
            }

            oldsSpAddrVal = sSpAddr.value;
        });        
    }
    else {
        oldsSpAddrVal = sSpAddr.value;
    }
}
