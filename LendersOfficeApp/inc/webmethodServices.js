﻿var pageJQuery = ($ && $.fn && $.fn.jquery) ? $ : ($j || jQuery);

var defaultWebMethodArgs = {
    type: 'POST',
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',
    displayOverlay: true
};

var overlayBufferTime = Number.MAX_SAFE_INTEGER;
var giveUpTimer = 3000;
var highestParent = getHighestParentSameDomain();
var $body;
var $focusedElement;
var asyncIntervalSet = setInterval(function () {
    giveUpTimer = -500;
    if (typeof (ML) != 'undefined') {
        // If the overlayBufferTime has not been set in the ConstStage, it will default to -1.  If the value is -1, 
        // then we allow the mask buffer to be a high value, so that it never appears.
        overlayBufferTime = (typeof (ML.AsyncOverlayTimeBuffer) != 'undefined' && ML.AsyncOverlayTimeBuffer != -1) ? ML.AsyncOverlayTimeBuffer : overlayBufferTime;
        clearInterval(asyncIntervalSet);
    }

    if (giveUpTimer <= 0) {
        clearInterval(asyncIntervalSet);
    }
}, 500);

function getHighestParentSameDomain() {
    var frame = window;
    try {
        while (frame.parent.document !== frame.document && window.frameElement != null) {
            frame = frame.parent;
        }
    } catch (e) { }

    return frame;
}

function callWebMethodAsync(settings) {
    var completeSettings = pageJQuery.extend({}, defaultWebMethodArgs, settings);
    if (completeSettings.async && completeSettings.displayOverlay) {
        triggerOverlay();
        if (completeSettings.complete) {
            var completeFunc = completeSettings.complete;
            completeSettings.complete = function (jqXHR, textStatus) { removeOverlay(); completeFunc(jqXHR, textStatus) };
        }
        else {
            completeSettings.complete = removeOverlay;
        }
    }

    return pageJQuery.ajax(completeSettings);
}


var isOverlayAdded = false;
sessionStorage.setItem("isOverlayAdded", false);

function addOverlay() {
    if (isOverlayAdded || (sessionStorage && sessionStorage.getItem("isOverlayAdded") == "true")) {
        var overlay = $body.find("#CalculationOverlay");
        overlay.addClass("darkened");
    }
}

var addOverlayTimeout;

function triggerOverlay() {
    setTimeout(triggerOverlayCode, 0);
}

function triggerOverlayImmediately() {
    triggerOverlayCode();
    addOverlay();
}

function triggerOverlayCode() {
    if (ML && ML.AsyncOverlayTimeBuffer != -1) {
        $body = $body || pageJQuery(highestParent.document.getElementsByTagName("body")[0])
        if (isAsyncCallsFinished()) {
            $body.find("#CalculationOverlay").removeClass("Hidden");
        }

        updateNumAsyncCalls(true);
        enableInterceptEvents();
        isOverlayAdded = true;
        if (sessionStorage) {
            sessionStorage.setItem("isOverlayAdded", true);
        }

        addOverlayTimeout = setTimeout(addOverlay, overlayBufferTime);
    }
}

function removeOverlay() {
    isOverlayAdded = false;

    if (sessionStorage) {
        sessionStorage.setItem("isOverlayAdded", false);
    }

    updateNumAsyncCalls(false);

	if (isAsyncCallsFinished()) {
		$body = $body || pageJQuery(highestParent.document.getElementsByTagName("body")[0])
		$body.find("#CalculationOverlay").addClass("Hidden");

        disableInterceptEvents();
    }

    clearTimeout(addOverlayTimeout);
}

function disableInterceptEvents() {
    var frames = highestParent.frames;
    if (frames.length > 0) {
        for (var i = 0; i < frames.length; i++) {
            var frame = frames[i];
            if (frame.removeInterceptEvents) {
                frame.removeInterceptEvents();
            }
        }
    }
    else {
        removeInterceptEvents();
    }
}

function removeInterceptEvents() {
    if ($focusedElement) {
        $focusedElement.resumeEvents();
        $focusedElement = null;
    }
}

function enableInterceptEvents() {
    var frames = highestParent.frames;
    if (frames.length > 0) {
        for (var i = 0; i < frames.length; i++) {
            var frame = frames[i];
            if (frame.setInterceptEvent) {
                frame.setInterceptEvent();
            }
        }
    }
    else {
        setInterceptEvent();
    }
}

function setInterceptEvent() {
    setTimeout(function () {
        if (!isAsyncCallsFinished()) {
            $focusedElement = pageJQuery(getActiveElement());
            $focusedElement.suspendEvents();
        }
    }, 0);
}

function getActiveElement() {
    return document.activeElement;
}

function interceptEvent(event) {
    event.preventDefault();
    event.stopPropagation();
}

function updateNumAsyncCalls(isAdd) {
    var currentCalls = Number(sessionStorage.getItem("numAsyncCalls")) || 0;
    sessionStorage.setItem("numAsyncCalls", currentCalls + (isAdd ? 1 : -1));
}

function isAsyncCallsFinished() {
    var currentCalls = Number(sessionStorage.getItem("numAsyncCalls")) || 0;
    return currentCalls == 0;
}

// https://stackoverflow.com/questions/17164609/how-to-temporarily-disable-all-event-handlers-attached-to-a-control
pageJQuery.fn.preBind = function (type, data, fn) {
    this.each(function () {
        var jQuerythis = pageJQuery(this);

        jQuerythis.bind(type, data, fn);

        pageJQuery.each(type.split(/ +/), function () {
            var currentBindings = pageJQuery._data ? pageJQuery._data(jQuerythis.get(0), "events")[this] : jQuerythis.data('events')[this];
            if (pageJQuery.isArray(currentBindings)) {
                currentBindings.unshift(currentBindings.pop());
            }
        });
    });
    return this;
};

pageJQuery.fn.suspendEvents = function () {
    this.preBind("keydown keyup keypress mouseover mouseenter mouseout mouseleave mousedown mouseup mousemove scroll", null, blockEvents);
}

pageJQuery.fn.resumeEvents = function () {
    var _this = this;
    pageJQuery.each("keydown keyup keypress mouseover mouseenter mouseout mouseleave mousedown mouseup mousemove scroll".split(/ +/), function (index, eventType) {
        _this.unbind(eventType, blockEvents); 
    });
}

function blockEvents(e) {
    e.stopImmediatePropagation();
    e.preventDefault();
}