﻿var backupModel;
var isQualifyingBorrowerReadonly;
function populateQualifyingBorrower(model) {
    var keys = Object.keys(model);
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        if (key == '$$hashKey') {
            continue;
        }

        var value = model[key];
        var $element = $('.true-container #' + key + ', .true-container input[data-field-id="' + key + '"], .true-container span[data-field-id="' + key + '"]');

        if ($element.length > 0) {
            if ($element[0].tagName.toLowerCase() == 'input') {
                var type = $element.attr('type');
                if (type != undefined) {
                    if ($element.attr('type').toLowerCase() == 'text') {
                        $element.val(value);
                    }

                    if ($element.attr('type').toLowerCase() == 'checkbox') {
                        $element.prop('checked', (typeof(value) == 'boolean' && value) || (typeof(value) == 'string' && value.toLowerCase() == 'true'));
                    }
                }
            }
            else {
                $element.text(value);
            }
        }
    }

    postPopulationQb(model);
}

function postPopulationQb(model) {
    setAdditionalCheckboxLogic(model);
    $('.true-container input[type="checkbox"]').each(function (index, element) {
        updateLockCheckbox(null, $(element), model);
    });
}

function setAdditionalCheckboxLogic(model) {
    $('#sAltCostLckd').attr('additionalDisableStatus', model['sAltCostLckdDisabled']);
}

function retrieveQualifyingBorrowerModelForUpdate() {
    return {
        sAltCostLckd: document.getElementById('sAltCostLckd').checked,
        sTotEstPp1003Lckd: document.getElementById('sTotEstPp1003Lckd').checked,
        sTotCcPbsLocked: document.getElementById('sTotCcPbsLocked').checked,
        sTotEstCc1003Lckd: document.getElementById('sTotEstCc1003Lckd').checked,
        sLDiscnt1003Lckd: document.getElementById('sLDiscnt1003Lckd').checked,
        sAltCost : document.getElementById("sAltCost").value,
        sTotEstCcNoDiscnt1003 : document.getElementById("sTotEstCcNoDiscnt1003").value,
        sTotEstPp1003 : document.getElementById("sTotEstPp1003").value,
        sTotCcPbs : document.getElementById("sTotCcPbs").value,
        sLDiscnt1003 : document.getElementById("sLDiscnt1003").value
    };
}

var qbLockMap = {
    sAltCostLckd: 'sAltCost',
    sLDiscnt1003Lckd: 'sLDiscnt1003',
    sTotEstCc1003Lckd: 'sTotEstCcNoDiscnt1003',
    sTotEstPp1003Lckd: 'sTotEstPp1003',
    sTotCcPbsLocked: 'sTotCcPbs'
};

function setQualifyingBorrowerLockCheckboxesState(model) {
    setAdditionalCheckboxLogic(model);
    var keys = Object.keys(qbLockMap);
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        updateLockCheckbox(null, $(".true-container #" + key));
    }
}

function updateLockCheckbox(e, $element) {
    var $checkbox = $element != null ? $element : $(retrieveEventTarget(e));

    var additionalDisableStatus = $checkbox.attr('additionalDisableStatus');
    additionalDisableStatus = additionalDisableStatus ? additionalDisableStatus.toLowerCase() : '';
    if (additionalDisableStatus == 'true') {
        $checkbox.prop('disabled', true);
    }

    var correspondingId = qbLockMap[$checkbox.prop('id')];
    if (isQualifyingBorrowerReadonly) {
        return;
    }

    $('#' + correspondingId).attr('readonly', $checkbox.prop('checked') ? null : '');
}

function revertModel() {
    populateQualifyingBorrower(backupModel);
}

function initializeQualifyingBorrower(isReadOnly, refreshCalculationCallback, removeHeader) {
    isQualifyingBorrowerReadonly = isReadOnly;
    if (removeHeader) {
        $('.qualifying-borrower-header').remove();
    }

    if (isReadOnly) {
        $('.true-container input[type="text"]').attr('readonly', '');
        $('.true-container input[type="checkbox"]').prop('disabled', 'true');
        $('.true-container .calculator').hide();
        return;
    }

    $('.qm-container .calculator')
        .prop('src', VRoot + '/images/Calculator.svg')
        .on('click', function (event) {
            backupModel = retrieveQualifyingBorrowerModelForUpdate();
            var calcIcon = $(retrieveEventTarget(event));
            var calcId = calcIcon.data('calcid');

            var args = {
                width: 600,
                height: 100,
                isCopyHTML: false,
                onClose: function () {
                    return function () {
                        revertModel();
                    };
                },
                onReturn: function () {
                    return function () {
                        if (refreshCalculationCallback) {
                            refreshCalculationCallback(true);
                        }
                    }
                }
            };

            LQBPopup.ShowElement($('.calculator-popup[data-CalcId="' + calcId + '"] .popup-content'), args);
        });

    $('.true-container input')
        .each(function (index, element) {
            _initMask(element);
        });

    $('.qm-container input').on('change', function () { if (refreshCalculationCallback) { refreshCalculationCallback(true); } })

    $('.true-container .calculator-popup input').off('change');

    $('.true-container .ok').on('click', function () { LQBPopup.Return(); });
    $('.true-container .cancel').on('click', function () { LQBPopup.Hide(); });

    var lockCheckboxIds = Object.keys(qbLockMap);
    var lockCheckboxSelector = '#' + lockCheckboxIds.join(', #');

    $(lockCheckboxSelector).on('change', updateLockCheckbox);
}