﻿var docuSignEnvelopeModule = angular.module('DocuSignEnvelope', ['ngRoute', 'LqbForms'])

.service('LQBPopupCreate', function ($window) {
    this.Show = $window.LQBPopup.Show;
    this.Hide = $window.LQBPopup.Hide;
    this.ShowElement = $window.LQBPopup.ShowElement;
})

.controller('DocuSignEnvelopeController', ['$scope', '$route', '$routeParams', '$location',
    function DocuSignEnvelopeController($scope, $route, $routeParams, $location) {
        this.$route = $route;
        this.$location = $location;
        this.$routeParams = $routeParams;
        $scope.$on('$routeChangeStart', function (event, next, current) {
            $scope.loaded = false;
            $scope.loadSuccess = false;
            $scope.errorMessage = 'Unknown Error.';
        });
        $scope.$on('$routeChangeSuccess', function (event, next, current) {
            $scope.loaded = true;
            $scope.loadSuccess = true;
            $scope.errorMessage = '';
        });
        $scope.$on('$routeChangeError', function (event, next, current, response) {
            $scope.loaded = true;
            $scope.loadSuccess = false;
            if (response.data && response.data.UserMessage && response.data.ErrorReferenceNumber)
            {
                $scope.errorMessage = response.data.UserMessage + ' Reference Number: ' + response.data.ErrorReferenceNumber;
            }
        });
    }])

.config(function ($compileProvider, $routeProvider, $locationProvider, LqbUrl) {
    $locationProvider.hashPrefix('');

    $routeProvider
    .when('/config', {
        templateUrl: LqbUrl.root + '/newlos/Services/DocuSign/DocuSignEnvelopeConfiguration.html',
        controller: 'EnvelopeConfigurationController',
        resolve: {
            pageName: function ($rootScope) { $rootScope.pageTitle = 'DocuSign Envelope Configuration'; return $rootScope.pageTitle; }
        }
    })

    .otherwise({ redirectTo: '/config' });
})

// Adapted from ngPattern source code
.directive('patternIfRequired', function () {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, elem, attr, ngModel) {
            if (!ngModel) { return; }

            var regexp, patternExp = attr.ngPattern || attr.pattern;
            attr.$observe('patternIfRequired', function (regex) {
                if (angular.isString(regex) && regex.length > 0) {
                    regex = new RegExp('^' + regex + '$');
                }

                if (regex && !regex.test) {
                    throw 'patternIfRequired: ' + 'Expected ' + patternExp + ' to be a RegExp but was ' + regex + '. Element: ' + elem;
                }

                regexp = regex || undefined;
                ngModel.$validate();
            });

            ngModel.$validators.pattern = function (modelValue, viewValue) {
                if(!attr.required)
                    return true;
                else
                    return ngModel.$isEmpty(viewValue) || angular.isUndefined(regexp) || regexp.test(viewValue);
            };
        }
    };
})

.controller('EnvelopeConfigurationController', function ($scope, $window, $location, $http, $filter, LQBPopupCreate, LqbUrl, ML, gService) {
    // Shouldn't load these from the global scope for testing reasons; use API loading or constant provider instead.
    $scope.edocsInfoList = edocsInfoList;
    $scope.enabledMfaOptions = enabledMfaOptions;
    $scope.recipientIdCounter = recipients.length;
    $scope.recipients = window.recipients;
    $scope.emailRecipientsStartIndex = 0; // 0 => All recipients are email recipients; recipients.length => All recipients are in-person
    $scope.inPersonOnly = false;
    $scope.submitting = false;

    $scope.emailMessagePlaceHolder = function() {
        if ($scope.recipients.length > 0 && $scope.emailRecipientsStartIndex < $scope.recipients.length && $scope.edocsInfoList.length > 0)
            return $scope.recipients[$scope.emailRecipientsStartIndex].name + ',\r\n\r\nPlease DocuSign ' + $scope.edocsInfoList[0].folder + ' - ' + $scope.edocsInfoList[0].docType + '\r\n\r\nThank You, ' + ML.docuSignUserName;
        else
            return '<Recipient Name>,\r\n\r\nPlease DocuSign <Document folder> - <Document Type>\r\n\r\nThank You, ' + ML.docuSignUserName;
    };

    $scope.recipientsWithoutRoles = function() {
        return $scope.recipients.filter(function (recipient) { return recipient.roles.length === 0; });
    };

    $scope.hasAnyPredefinedPhones = function(phone) {
        return phone.workPhone
            || phone.homePhone
            || phone.agentPhone
            || phone.companyPhone
            || phone.cellPhone;
    };

    (function() {
        if (typeof $scope.enabledMfaOptions.NoMfa !== 'undefined') {
            $scope.emailMfaOptions = {};
            var emailOptionCount = 0;
            // Note that $scope.enabledMfaOptions is an object, not an array.
            for (var option in $scope.enabledMfaOptions) {
                if (option !== 'NoMfa') {
                    $scope.emailMfaOptions[option] = $scope.enabledMfaOptions[option];
                    emailOptionCount++;
                }
            }
            if (emailOptionCount === 0) {
                ML.defaultMfaValue = $scope.enabledMfaOptions.NoMfa.value;
                for (var i = 0; i < $scope.recipients.length; i++) {
                    $scope.recipients[i].mfaChosen = $scope.enabledMfaOptions.NoMfa.value;
                    $scope.recipients[i].signingInPerson = true;
                }
                $scope.inPersonOnly = true;
                $scope.emailRecipientsStartIndex = $scope.recipients.length;
            }
            else
            {
                $scope.emailRecipientsStartIndex = 0;
            }
        }
        else {
            $scope.emailMfaOptions = $scope.enabledMfaOptions;
        }

        // Normalize recipient phones
        for (var recipientIndex = 0; recipientIndex < $scope.recipients.length; recipientIndex++ ) {
            var recipient = $scope.recipients[recipientIndex];
            if (!recipient.phone || !$scope.hasAnyPredefinedPhones(recipient.phone)) {
                recipient.phone = {
                    choosing: true,
                    choice: 'chosen',
                    chosenPhone: '',
                };
            }
        }
    })();

    $scope.adjustPlaceHolders = function() {
        angular.element('body').css('margin-top', angular.element('.float-top').outerHeight() + 'px')
            .css('margin-bottom', angular.element('.submit-page').outerHeight() + 'px');
    };
    $scope.$watch('adjustPlaceHolders()'); // This will be called on every $digest call 

    $scope.closeWindow = function() {
        $window.close();
    };

    $scope.submitDisabled = function() {
        return (!$scope.fullPageForm.$valid || $scope.recipients.length === 0 || $scope.edocsInfoList.length === 0 || $scope.submitting || $scope.unpreppedDocs().length > 0);
    };

    $scope.unpreppedDocs = function() {
        return $scope.edocsInfoList.filter(function(edocInfo) {return !edocInfo.isPreppedForEditing; }).map(function(edocInfo) { return edocInfo.folder + ' - ' + edocInfo.docType; });
    };

    $scope.changeLocationAbsolute = function(newHref) {
        $window.location.href = newHref;
    };

    $scope.getEnabledMfaOptions = function (inPerson) {
        return inPerson ? $scope.enabledMfaOptions : $scope.emailMfaOptions;
    };

    $scope.mfaOptionHasAccessCode = function (optionValue) {
        return ($scope.enabledMfaOptions.AccessCode && optionValue === $scope.enabledMfaOptions.AccessCode.value)
            || ($scope.enabledMfaOptions.AccessCodeAndIdCheck && optionValue === $scope.enabledMfaOptions.AccessCodeAndIdCheck.value);
    };

    $scope.mfaOptionIsPhone = function (optionValue) {
        return $scope.enabledMfaOptions.Phone && optionValue === $scope.enabledMfaOptions.Phone.value;
    };

    $scope.mfaOptionIsSms = function (optionValue) {
        return $scope.enabledMfaOptions.Sms && optionValue === $scope.enabledMfaOptions.Sms.value;
    };

    $scope.recipientNameAndEmail = function (recipient) {
        var nameAndEmail = '';
        if (recipient.name)
            nameAndEmail += recipient.name + ' ';
        else
            nameAndEmail += '<NO NAME> ';
        if (recipient.email)
            nameAndEmail += '(' + recipient.email + ')';
        else
            nameAndEmail += '(<NO EMAIL>)';
        return nameAndEmail;
    };

    $scope.edocSortOrder = [];

    // Sort cycle: [No sort] => [Ascending] => [Descending] => [No sort] ...
    $scope.cycleSort = function (sortArray, categoryName) {
        var ascendingIndex = sortArray.indexOf('+' + categoryName);
        var descendingIndex = sortArray.indexOf('-' + categoryName);
        if (ascendingIndex !== -1) {
            // Ascending: switch to descending, same sort order.
            sortArray[ascendingIndex] = '-' + categoryName;
            return sortArray;
        }
        else if (descendingIndex !== -1) {
            // Descending: remove sort.
            sortArray.splice(descendingIndex, 1);
            return sortArray;
        }
        else {
            // No sort: Add ascending sort as primary sort.
            sortArray.splice(0, 0, '+' + categoryName);
            return;
        }
    };

    $scope.moveRecipientUp = function (recipient) {
        if ($scope.recipients.indexOf(recipient) === $scope.emailRecipientsStartIndex && $scope.emailRecipientsStartIndex < $scope.recipients.length) {
            $scope.emailRecipientsStartIndex++;
            correctSigningOrder();
        }
        else
            $scope.moveRecipient(recipient, recipient.signingOrder - 2);
    };

    $scope.moveRecipientDown = function (recipient) {
        if ($scope.recipients.indexOf(recipient) === $scope.emailRecipientsStartIndex - 1 && $scope.emailRecipientsStartIndex > 0) {
            $scope.emailRecipientsStartIndex--;
            correctSigningOrder();
        }
        else
            $scope.moveRecipient(recipient, recipient.signingOrder);
    };

    $scope.moveRecipientToMiddle = function (recipient) {
        var index = $scope.recipients.indexOf(recipient);

        if (index < $scope.emailRecipientsStartIndex) {
            $scope.moveRecipient(recipient, $scope.emailRecipientsStartIndex - 1);
            $scope.emailRecipientsStartIndex--;
        }
        else {
            $scope.moveRecipient(recipient, $scope.emailRecipientsStartIndex);
            $scope.emailRecipientsStartIndex++;
        }
        correctSigningOrder();
    };

    $scope.signingOrderBlur = function(recipient, newPosition) {
        var index = $scope.recipients.indexOf(recipient);
        if (typeof newPosition !== 'number') {
            recipient.signingOrder = index + 1;
            correctSigningOrder();
        }
        else {
            var newIndex = newPosition - 1;
            if (index !== newIndex)
                $scope.moveRecipient(recipient, newIndex);
        }
    };

    $scope.moveRecipient = function (recipientToMove, newIndex)
    {
        var oldIndex = $scope.recipients.indexOf(recipientToMove);
        if (oldIndex == -1 || typeof recipientToMove.signingOrder !== 'number') return;

        if (typeof newIndex !== 'number') {
            newIndex = recipientToMove.signingOrder - 1;
        }

        if (newIndex === oldIndex) return;

        if (newIndex < $scope.emailRecipientsStartIndex && oldIndex >= $scope.emailRecipientsStartIndex) {
            $scope.emailRecipientsStartIndex++;
        }
        if (newIndex >= $scope.emailRecipientsStartIndex && oldIndex < $scope.emailRecipientsStartIndex) {
            $scope.emailRecipientsStartIndex--;
        }

        if (newIndex < 0) newIndex = 0;
        // remove from list
        $scope.recipients.splice(oldIndex, 1);
        // Add to list
        $scope.recipients.splice(newIndex, 0, recipientToMove);

        correctSigningOrder();
    };

    $scope.removeRecipient = function (recipientToRemove) {
        var indexToRemove = $scope.recipients.indexOf(recipientToRemove);
        if (indexToRemove === -1) 
            return;
        if (indexToRemove < $scope.emailRecipientsStartIndex) 
            $scope.emailRecipientsStartIndex--;
        $scope.recipients.splice(indexToRemove, 1);
        correctSigningOrder();
    };

    function correctSigningOrder()
    {
        for (var i = 0; i < $scope.recipients.length; i++) {
            $scope.recipients[i].signingOrder = i + 1;
            $scope.recipients[i].signingInPerson = i < $scope.emailRecipientsStartIndex;
        }
    }

    $scope.inPersonRecipient = function(value, index, array) {
        return $scope.recipients.indexOf(value) < $scope.emailRecipientsStartIndex;
    };

    $scope.emailRecipient = function(value, index, array) {
        return $scope.recipients.indexOf(value) >= $scope.emailRecipientsStartIndex;
    };

    $scope.recipientPhone = function (phoneObj, mfaOption) {
        return phoneObj[phoneObj.choice + 'Phone'];
    };

    $scope.disablePhoneChoosing = function(recipient) {
        return (!recipient.phone || !$scope.hasAnyPredefinedPhones(recipient.phone))
            || ($scope.mfaOptionIsSms(recipient.mfaChosen) && !recipient.phone.cellPhone);
    };

    $scope.choosePhone = function (phoneObj) {
        phoneObj.choosing = !phoneObj.choosing;
        if (!$scope.hasAnyPredefinedPhones(phoneObj)) {
            phoneObj.choice = 'chosen';
        }
    };

    $scope.chooseText = function (choosing) {
        return choosing ? 'collapse' : 'choose';
    };

    $scope.borrowerSelectDialogCancel = function() {
        $scope.showBorrowerSelectDialog = false;
    };

    $scope.addNewRecipient = function(newRecipient) {
        newRecipient.id = $scope.recipientIdCounter++;
        newRecipient.mfaChosen = ML.defaultMfaValue;
        newRecipient.signingInPerson = ML.defaultMfaValue === 32; // No MFA
        newRecipient.signingOrder = $scope.recipients.length + 1;
        newRecipient.phone.choosing = !$scope.hasAnyPredefinedPhones(newRecipient.phone);
        for (var i = 0; i < newRecipient.roles.length; i++) {
            var newRole = newRecipient.roles[i];
            for (var j = 0; j < $scope.activeSigningRoles.length; j++) {
                if ($scope.activeSigningRoles[j].name === newRole) {
                    $scope.activeSigningRoles[j].recipient = newRecipient;
                    break;
                }
            }
        }

        $scope.recipients.push(newRecipient);
    };

    $scope.addContactsRecipient = function () {
        LQBPopupCreate.Show(LqbUrl.root + '/los/RolodexList.aspx?loanid=' + ML.sLId,
            {
                onReturn: function (result) {
                    $scope.$apply(function () {
                        if (result.OK) {
                            var newRecipient = {
                                name: result.AgentName || result.AgentCompanyName,
                                email: result.AgentEmail,
                                roles: [],
                                isAgent: true,
                                phone: {
                                    agentPhone: result.AgentPhone,
                                    companyPhone: result.PhoneOfCompany,
                                    cellPhone: result.AgentAltPhone,
                                    isAgent: true
                                }
                            };
                            $scope.addNewRecipient(newRecipient);
                        }
                    });
                },
                height: 400,
                width: 800,
                hideCloseButton: true
            });
    };

    $scope.addBorrowerRecipients = function () {
        $scope.showBorrowerSelectDialog = true;
        $scope.borrowersLoaded = false;
        gService.main.callAsyncSimple('GetBorrowers', { loanid: ML.sLId }, 
            function successCallback(result) {
                $scope.$apply(function() {
                    $scope.borrowersLoaded = true;
                    var recipients = angular.fromJson(result.value.recipients);
                    $scope.borrowersToAdd = recipients
                        .filter(function(recipient) { return !$scope.recipients.some(function(existingRecipient) { return existingRecipient.appId === recipient.appId && existingRecipient.borrowerMode === recipient.borrowerMode; } ); })
                        .map(function(recipient) { return { shouldAdd: false, recipient: recipient };});
                });
            },
            handleStandardError);
    };

    $scope.addSelectedBorrowers = function(borrowersToAdd)
    {
        for (var i = 0; i < borrowersToAdd.length; i++) {
            if (borrowersToAdd[i].shouldAdd) {
                $scope.addNewRecipient(borrowersToAdd[i].recipient);
            }
        }
        $scope.showBorrowerSelectDialog = false;
    };

    /**
     *  Combine the list of all recipients ($scope.recipients) with the given recipients.
     */
    $scope.combineRecipients = function(recipientsToAdd) {
        for (var addIndex = 0; addIndex < recipientsToAdd.length; addIndex++) {
            var addedRecipient = recipientsToAdd[addIndex];
            var recipientAdded = false;
            for (var recipIndex = 0; recipIndex < $scope.recipients.length; recipIndex++) {
                var existingRecipient = $scope.recipients[recipIndex];
                // Determine if both recipients are the same borrower
                if (addedRecipient.appId && existingRecipient.appId && addedRecipient.appId === existingRecipient.appId 
                    && addedRecipient.borrowerMode && existingRecipient.borrowerMode && addedRecipient.borrowerMode === existingRecipient.borrowerMode) {
                    // Remove all existing borrower/coborrower roles from the existing recipient.
                    for (var roleIndex = existingRecipient.roles.length - 1; roleIndex >= 0; roleIndex--) { // Go backward so we can remove as we go
                        if (existingRecipient.roles[roleIndex].search(/(co)?borrower/i) > -1) existingRecipient.roles.splice(roleIndex, 1);
                    }
                    existingRecipient.roles = existingRecipient.roles.concat(addedRecipient.roles);
                    recipientAdded = true;
                }
                else if (addedRecipient.name === existingRecipient.name && addedRecipient.email === existingRecipient.email)
                {
                    // Same name & email => add all roles from backend and ensure distinct roles.
                    existingRecipient.roles = existingRecipient.roles.concat(addedRecipient.roles).filter(function (value, index, array) { return array.indexOf(value) === index; });
                    recipientAdded = true;
                }
            }
            if (!recipientAdded) {
                $scope.addNewRecipient(addedRecipient);
            }
        }
    };

    $scope.addBlankRecipient = function () {
        var newRecipient = {
            name: '',
            email: '',
            roles: [],
            phone: { choice: 'chosen', chosenPhone: '' }
        };
        $scope.addNewRecipient(newRecipient);
        return newRecipient;
    };

    $scope.editDoc = function (docId) {
        // Doc list has a window map to keep track of opened eDoc edit windows
        var windowMap = null;
        if (window.opener) {
            windowMap = $window.opener.windowMap;
        }
        var mapId = 'a' + docId.replace(/-/g, '');

        var createdWindow = $window.open(LqbUrl.root + '/newlos/ElectronicDocs/EditEdocV2.aspx?loanid=' + ML.sLId + '&docid=' + docId, mapId, 'height=864,width=1024,menubar=no,scrollbars=no,resizable=yes');
        if (windowMap && !windowMap[mapId]) {
            windowMap[mapId] = createdWindow;
        }
        createdWindow.focus();
    };

    $scope.viewDoc = function (docId) {
        $window.open(LqbUrl.root + '/newlos/ElectronicDocs/ViewEdocPdf.aspx?docid=' + docId, docId, 'height=0,width=0,menubar=no,scrollbars=no,resizable=no');
    };

    $scope.removeDoc = function (index) {
        return $scope.edocsInfoList.splice(index, 1)[0];
    };

    $scope.addDocFromPicker = function (doc) {
        for (var i = 0; i < $scope.edocsInfoList.length; i++) {
            if ($scope.edocsInfoList[i].docId == doc.DocumentId) {
                return;
            }
        }

        var newDoc = {
            docId: doc.DocumentId,
            folder: doc.FolderName,
            docType: doc.DocTypeName,
            description: doc.Description,
            lastModified: doc.LastModifiedDate,
            signingRoles: []
        };

        // Called in $scope.$apply to ensure the view is updated, since LQBPopup return isn't an event recognized by AngularJS.
        $scope.$apply(function () { $scope.edocsInfoList.push(newDoc); });

        //Update other data (mostly signing roles, but might as well update everything) with ajax call
        gService.main.callAsyncSimple('GetDocInfo', 
            {
                loanid: ML.sLId,
                docIds: angular.toJson($scope.edocsInfoList.map(function (edoc) { return edoc.docId; }), false),
                roles: angular.toJson($scope.getDistinctEdocSigningRoles($scope.edocsInfoList))
            },
            function successCallback(result) {
                $scope.$apply(function() {
                    var responseData = angular.fromJson(result.value.docsAndRecipients);
                    $scope.edocsInfoList = responseData.docInfo;
                    $scope.combineRecipients(responseData.recipientList);
                });
            },
            handleStandardError);
    };

    $scope.addEDoc = function () {
        LQBPopupCreate.Show(LqbUrl.root + '/newlos/ElectronicDocs/SingleDocumentPicker.aspx?esignonly=true'
            + '&loanid=' + ML.sLId
            + '&exclude=' + $scope.edocsInfoList.map(function (edoc) { return edoc.docId; }).join(','),
            {
                onReturn: function (result) {
                    if (result.OK)
                        $scope.addDocFromPicker(result.Document);
                },
                width: 600,
                height: 500,
                hideCloseButton: true
            });
    };

    $scope.getDistinctEdocSigningRoles = function (edocList) {
        var allRoles = edocList.map(function (edoc) { return edoc.signingRoles; })
            .reduce(function (accumulator, currentValue) {
                for (var i = 0; i < currentValue.length; i++) {
                    accumulator.push(currentValue[i]);
                }
                return accumulator;
            }, []);
        var allRolesDistinct = allRoles.filter(function (value, index, array) { return array.indexOf(value) === index; });
        return allRolesDistinct;
    };

    $scope.initEdocSigningRoles = function (edocList) {
        var allRolesDistinct = $scope.getDistinctEdocSigningRoles(edocList);
        // Need to make it into an object in order to be the model for an ngRepeat.
        var roleMap = {};
        for (var i = 0; i < allRolesDistinct.length; i++)
        {
            var role = allRolesDistinct[i];
            for (var j = 0; j < $scope.recipients.length; j++) {
                if ($scope.recipients[j].roles.indexOf(role) >= 0) {
                    roleMap[role] = { name: role, recipient: $scope.recipients[j] };
                    break;
                }
            }
            if (typeof roleMap[role] === 'undefined') {
                roleMap[role] = { name: role, recipient: undefined };
            }
        }
        return roleMap;
    };

    $scope.allEdocSigningRolesMap = $scope.initEdocSigningRoles($scope.edocsInfoList);

    $scope.updateEdocSigningRoles = function (edocList) {
        var allRolesDistinct = $scope.getDistinctEdocSigningRoles(edocList);
        var activeRoleObjects = [];
        for (var i = 0; i < allRolesDistinct.length; i++) {
            var role = allRolesDistinct[i];
            if (typeof $scope.allEdocSigningRolesMap[role] === 'undefined') {
                $scope.allEdocSigningRolesMap[role] = { name: role };
            }
            activeRoleObjects.push($scope.allEdocSigningRolesMap[role]);
            if (typeof $scope.allEdocSigningRolesMap[role].recipient === 'undefined') {
                for (var j = 0; j < $scope.recipients.length; j++) {
                    if ($scope.recipients[j].roles.indexOf($scope.allEdocSigningRolesMap[role].name) > -1) {
                        $scope.allEdocSigningRolesMap[role].recipient = $scope.recipients[j];
                        break;
                    }
                }
            }
        }

        return activeRoleObjects;
    };

    $scope.$watchCollection('edocsInfoList', function (newVal, oldVal) {
        $scope.activeSigningRoles = $scope.updateEdocSigningRoles(newVal);
    });

    $scope.roleInActiveRoles = function(role) {
        return $scope.activeSigningRoles.some(function(roleObj) { return roleObj.name === role; });
    };

    $scope.$watchCollection('activeSigningRoles', function (newVal, oldVal) {
        // Remove roles not present anymore from recipients
        for (var recipientIndex = 0; recipientIndex < $scope.recipients.length; recipientIndex++) {
            var recipientRoles = $scope.recipients[recipientIndex].roles;
            var newRoleList = [];
            for (var roleNameIndex = 0; roleNameIndex < recipientRoles.length; roleNameIndex++) {
                if (newVal.some(function(roleObj) { return recipientRoles[roleNameIndex] === roleObj.name; })) {
                    newRoleList.push(recipientRoles[roleNameIndex]);
                }
            }
            $scope.recipients[recipientIndex].roles = newRoleList;
        }
    });

    function submitSuccess(response) {
        $scope.$apply(function() {
            $scope.submitting = false;
            if (response.value && response.value.UserMessage && response.value.ErrorReferenceNumber) {
                $scope.errorStatus = response.value.UserMessage + '\r\nError Reference Number: ' + response.value.ErrorReferenceNumber;
            }
            else if (response.value && response.value.ErrorMessage) {
                alert(response.value.ErrorMessage);
            }
            else { // Success.
                $scope.showCloseDialog = true;
                if (response.value && response.value.RecipientViewUrl)
                {
                    $scope.continueUri = response.value.RecipientViewUrl;
                    $scope.changeLocationAbsolute($scope.continueUri);
                }
            }
        });
    }

    function submitError(response) {
        $scope.submitting = false;
        handleStandardError(response);
    }

    function handleStandardError(response) {
        $scope.$apply(function() {
            var errorWindow = $window.open(response.url, 'Error', 'height=400,width=500,menubar=no,toolbar=no,location=no,status=no');
            errorWindow.focus();
        });
    }

    $scope.submitForm = function() {
        for (var i = 0; i < $scope.recipients.length; i++) {
            $scope.recipients[i].phone.chosenPhone = $filter('phone')($scope.recipients[i].phone.chosenPhone);
        }
        var args = {
                documentIds: angular.toJson($scope.edocsInfoList.map(function (doc) { return doc.docId; })),
                recipients: angular.toJson($scope.recipients),
                roles: angular.toJson($scope.activeSigningRoles.map(function(role) {return { Name: role.name, RecipientSigningOrder: role.recipient.signingOrder};})),
                emailMessage: $scope.emailMessage,
                loanId: ML.sLId
            };

        $scope.submitting = true;
        gService.main.callAsyncSimple('SubmitEnvelope', args, submitSuccess, submitError);
    };
});

function recipientRowController ($scope, $attrs) {
    $scope.$watch('recipient.mfaChosen', function (newVal, oldVal, $scope) {
        var phoneObj = $scope.recipient.phone;
        if ($scope.$parent.enabledMfaOptions.Phone && newVal === $scope.$parent.enabledMfaOptions.Phone.value && !phoneObj.choosing) {
            if (phoneObj.homePhone) {
                phoneObj.choice = 'home';
            }
            else if (phoneObj.workPhone) {
                phoneObj.choice = 'work';
            }
            else if (phoneObj.agentPhone) {
                phoneObj.choice = 'agent';
            }
            else if (phoneObj.companyPhone) {
                phoneObj.choice = 'company';
            }
            else if (phoneObj.cellPhone) {
                phoneObj.choice = 'cell';
            }
            else {
                phoneObj.choice = 'chosen';
            }
        } else if ( $scope.$parent.enabledMfaOptions.Sms 
                    && newVal === $scope.$parent.enabledMfaOptions.Sms.value
                    && phoneObj.choice !== 'cell'
                    && phoneObj.choice !== 'chosen') {
            if (phoneObj.cellPhone) {
                phoneObj.choice = 'cell';
            }
            else {
                phoneObj.choice = 'chosen';
                phoneObj.choosing = true;
            }
        } else if (phoneObj.choosing && $scope.disablePhoneChoosing($scope.recipient)) {
            phoneObj.choice = 'chosen';
        }
    });
}

docuSignEnvelopeModule.directive('recipientRow', function(LqbUrl){
    return {
        templateUrl: LqbUrl.root + '/newlos/Services/DocuSign/RecipientRow.html',
        controller: recipientRowController,
        restrict: 'A'
};})

.controller('RoleRowController', function ($scope) {
    $scope.$watch('role', function (newVal, oldVal, $scope) {
        if (typeof oldVal !== 'undefined'
            && typeof oldVal.recipient !== 'undefined'
            && oldVal.recipient) {
            // The oldVal.recipient !== the actual recipient object that we want to modify.
            // Need to find the equivalent in that list to modify.
            var oldRecipient = null;
            for (var i = 0; i < $scope.recipients.length; i++) {
                if ($scope.recipients[i].id === oldVal.recipient.id) {
                    oldRecipient = $scope.recipients[i];
                }
            }

            if (oldRecipient) {
                // Remove this role from old recipient
                var oldIndex = oldRecipient.roles.indexOf(oldVal.name);
                if (oldIndex >= 0) {
                    oldRecipient.roles.splice(oldIndex, 1);
                }
            }
        }

        // Add this role to new recipient
        if (typeof newVal.recipient !== 'undefined'
            && newVal.recipient
            && newVal.recipient.roles.indexOf(newVal.name) === - 1)
        {
            var newRecipient = null;
            for (var j = 0; j < $scope.recipients.length; j++) {
                if ($scope.recipients[j].id === newVal.recipient.id) {
                    newRecipient = $scope.recipients[j];
                }
            }

            newRecipient.roles.push(newVal.name);
        }
    }, true);
});