// This script should only be use in Asset/Liability/Employment/REO edit screen.

//---- Start ListIFrame js methods
var list_oTable = null;
var gHighlightBackgroundRow = '#000080';
var gHighlightForegroundRow = 'white';

function __sortMe(arg) {
    if (callFrameMethod(parent, "SingleIFrame", "single_sort", [arg])) {
        self.location = self.location;
    }
}
  
function list_getRowCount() {
  return null == list_oTable ? 0 : list_oTable.rows.length;
}  
function list_selectRow(index) {
  parent.PolyShouldShowConfirmSave(parent.isDirty(), function(confirmed){
      // Unhighlight previous row.
      var previousIndex = parent.parent_getCurrentIndex();
      if (previousIndex > 0 && previousIndex < list_oTable.rows.length) {
        list_oTable.rows[previousIndex].style.backgroundColor = '';
        list_oTable.rows[previousIndex].style.color = '';
      }

      // Highlight new row.
      var recordID = null;
      if (index > 0 && index <= list_oTable.rows.length && list_oTable.rows.length > 1) {
        if (index == list_oTable.rows.length)
          index = index - 1; // Last record get delete.
        list_oTable.rows[index].style.backgroundColor = gHighlightBackgroundRow;
        list_oTable.rows[index].style.color = gHighlightForegroundRow;
        parent.parent_setCurrentIndex(index);
        
        recordID = list_oTable.rows[index].getAttribute("recordid");
      } else {
        parent.parent_setCurrentIndex(0);
      }

      // Retrieve recordID to load.
      if (confirmed) {
        var result = callFrameMethod(parent, "SingleIFrame", "invokeMethod", ["SaveData", recordID]);
        if (result != null && result.error) {
            clearDirty();
        } else {
            // Update previous row with new values.
            callFrameMethod(parent, "SingleIFrame", "updateListIFrame", [previousIndex]);
        }
      }
      callFrameMethod(parent, "SingleIFrame", "invokeMethod", ["LoadData", recordID]);
  } );
}

function list_getRecordID(index) {
  if (index < 0 || index >= list_oTable.rows.length) return null;
    
  return list_oTable.rows[index].getAttribute("recordid");    
} 
// Move record forward or backward.
// d = -1  Move backward
// d = 1 Move forward    
function list_go(d) {
  var previousIndex = parent.parent_getCurrentIndex();
  var index = previousIndex;
  if (d == -1 && previousIndex > 1) {
    index--;
  } else if (d == 1 && previousIndex < list_oTable.rows.length - 1) {
    index++;
  } else {
    return;
  }
    
  if (index > 0 && index < list_oTable.rows.length) {
    list_selectRow(index);
  }
}

// Move record up or down.
// d = -1 Move down.
// d = 1 Move up.
// Return true if move successfully.
function list_move(d) {
  if (d != -1 && d != 1) return; // Need to be either -1 or 1.
  var previousIndex = parent.parent_getCurrentIndex();

  if (d == 1 && previousIndex <= 1) return; // Can't move up.
  if (d == -1 && previousIndex >= list_oTable.rows.length - 1) return; // Can't move down.

  parent.PolyShouldShowConfirmSave(parent.isDirty(), function(confirmed){
    var methodName = confirmed ? "SaveDataAndMoveRecord" : "MoveRecord";

    var updateListIFrame = retrieveFrameProperty(parent, "SingleIFrame", "updateListIFrame");
    var setDirection = retrieveFrameProperty(parent, "SingleIFrame", "setDirection");
    var invokeMethod = retrieveFrameProperty(parent, "SingleIFrame", "invokeMethod");

    if (confirmed  && updateListIFrame) // Update previous row with new values.
    {
        updateListIFrame(previousIndex);
    }
        
    if(setDirection) {
        setDirection(d);
    }

    if(invokeMethod) {
        invokeMethod(methodName, "00000000-0000-0000-0000-000000000000");
    }
    
    swapNodePoly(list_oTable.rows[previousIndex], list_oTable.rows[previousIndex - d]);

    parent.parent_setCurrentIndex(previousIndex - d);
  });
}

// First argument is rowIndex, second argument is recordid, the rest is value in column.
function list_updateRow() {
  var len = list_updateRow.arguments.length;
  if (len > 2) {
    var iRowIndex = list_updateRow.arguments[0];
    list_oTable.rows[iRowIndex].setAttribute("recordid", list_updateRow.arguments[1]); // Retrieve recordid
    for (var i = 2; i < len; i++) 
      list_oTable.rows[iRowIndex].cells[i - 2].innerText = list_updateRow.arguments[i];
  }
}

function list_addRow() {
  list_insertRow(list_oTable.rows.length);
}

function list_deleteRecord() {
  var currentIndex = parent.parent_getCurrentIndex();
  if (currentIndex < 1) return;
  
  if (!confirm('Do you want to delete this record?')) return;
  
  parent.PolyShouldShowConfirmSave(parent.isDirty(), function(confirmed){
    var methodName = confirmed ? "SaveDataAndDeleteRecord" : "DeleteRecord";

    // Get next record id to load.
    // Always get recordid after the delete row. If the deleted record is the last row then retrieve a row before.
    var recordID = null;

    if (currentIndex == list_oTable.rows.length - 1 && currentIndex != 1) {
      recordID = list_oTable.rows[currentIndex - 1].getAttribute('recordid'); 
    } else if (currentIndex < list_oTable.rows.length - 1) {
      recordID = list_oTable.rows[currentIndex + 1].getAttribute('recordid');
    }

    var result = callFrameMethod(parent, "SingleIFrame", "invokeMethod", [methodName, recordID]);

    if (result && !result.error) {
      var index = currentIndex;
      if (currentIndex == list_oTable.rows.length - 1)
        index = currentIndex - 1;  
      // Deleterow in table.
      list_oTable.deleteRow(currentIndex);

      // Highlight new row.
      if (recordID != null ) {

        
        list_oTable.rows[index].style.backgroundColor = gHighlightBackgroundRow;
        list_oTable.rows[index].style.color = gHighlightForegroundRow;
        parent.parent_setCurrentIndex(index);    
      } else {
        parent.parent_setCurrentIndex(0);
      }
    }
  });
}

function list_insertRow(index) {
  parent.PolyShouldShowConfirmSave(parent.isDirty(), function(confirmed){
    var methodName = confirmed ? "SaveDataAndInsertRecord" : "InsertRecord";
    callFrameMethod(parent, "SingleIFrame", "single_hideEdit", [false]);

    // Clear out the highlight of previous selected row.
    var previousIndex = parent.parent_getCurrentIndex();
    if (previousIndex > 0 && previousIndex < list_oTable.rows.length) {      
      list_oTable.rows[previousIndex].style.backgroundColor = '';
      list_oTable.rows[previousIndex].style.color = '';
      if (confirmed) // Update previous row with new values.
        callFrameMethod(parent, "SingleIFrame", "updateListIFrame", [previousIndex]);
    }
        
    if (index == 0) index = 1; // Insert correctly on empty result list.
    callFrameMethod(parent, "SingleIFrame", "setRowIndex", [index]);
    
    // Invoke server method to insert new record.
    var result = callFrameMethod(parent, "SingleIFrame", "invokeMethod", [methodName, "00000000-0000-0000-0000-000000000000"]);
    if (!result.error) {
      var tr = list_oTable.insertRow(index);
      tr.className = "GridItem";
      tr.onclick = function () { list_selectRow(this.rowIndex); if (typeof (refreshCalculation) == 'function') refreshCalculation(); };
      tr.setAttribute("recordid", result.value["RecordID"]);
      if (typeof(constructEmptyRow) != "function") {
        alert("Need to define constructEmptyRow method.");
        return ;
      }
      
      constructEmptyRow(tr); // Need to define constructEmptyRow in each list.

      // Highlight new row.
      list_oTable.rows[index].style.backgroundColor = gHighlightBackgroundRow;
      list_oTable.rows[index].style.color = gHighlightForegroundRow;
      parent.parent_setCurrentIndex(index);  
      callFrameMethod(parent, "SingleIFrame", "updateListIFrame", [index]);
    }
    
    callFrameMethod(parent, "SingleIFrame", "statusDdlChange", []);
  });

  return true;
}
//---- End ListIFrame js methods

//---- Parent frame js methods.
var parent_iCurrentIndex = 1;
var parent_bSingleFrameReady = false;
var parent_bListFrameReady = false;

function parent_resetValues() {
  parent_iCurrentIndex = 1;
  parent_bSingleFrameReady = false;
  parent_bListFrameReady = false;
}

function parent_setRowIndex(index) {
    parent_iCurrentIndex = index
}
//ui = 1 if this method is call from Single edit screen.
//ui = 2 if this method is call from List screen.
function parent_initScreen(ui) {

  if (ui == 1) parent_bSingleFrameReady = true;
  if (ui == 2) parent_bListFrameReady = true;
  
  if (parent_bSingleFrameReady && parent_bListFrameReady) {
      // Load record when both list and single frame are load completely.
      callFrameMethod(window, "ListIFrame", "list_selectRow", [parent_iCurrentIndex]);
  }
}

function parent_disableSaveBtn(b) {
  var btnSave = document.getElementById("btnSave");
  btnSave.disabled = b;
  btnSave.style.backgroundColor=''; // Clear out highlight
  if (!b)  {
    if (!isDirty()) updateDirtyBit(); // Prevent infinite loop.
  } else clearDirty();
}
function parent_refreshButtonsUI() {
    var bIsReadOnly = isReadOnly();
    var rowCount = callFrameMethod(window, "ListIFrame", "list_getRowCount");

    document.getElementById("btnPrev").disabled = bIsReadOnly || parent_iCurrentIndex <= 1;
    document.getElementById("btnNext").disabled = bIsReadOnly || parent_iCurrentIndex == rowCount - 1;
    document.getElementById("btnUp").disabled= bIsReadOnly || parent_iCurrentIndex <= 1;
    document.getElementById("btnDown").disabled= bIsReadOnly || parent_iCurrentIndex == rowCount - 1;
    document.getElementById("btnDelete").disabled = bIsReadOnly || parent_iCurrentIndex < 1;
    if (typeof(_customRefreshButtonsUI) == "function") _customRefreshButtonsUI();
}

function parent_getCurrentIndex() { return parent_iCurrentIndex; }
function parent_setCurrentIndex(index) { 
  parent_iCurrentIndex = index; 
  parent_refreshButtonsUI();  
}

function parent_onInsert() {
    callFrameMethod(window, "ListIFrame", "list_insertRow", [parent_iCurrentIndex]);
}

function parent_onAdd() {
    callFrameMethod(window, "ListIFrame", "list_addRow", [parent_iCurrentIndex]);
} 

//----- End Paren frame js methods

//----- SingleIFrame js methods
// recordID - next record to be load. If recordID is null then no record to be load, hide edit fields.
function invokeMethod(methodName, recordID) {
  if (gIsDebug) alert("singledit2.js:invokeMethod('" + methodName + "', recordid='" + recordID + "')");
  var args = getAllFormValues();
  if (null != recordID) args["nextrecordid"] = recordID;
  else single_hideEdit(true);
  
  // Get special values.
  if (typeof (getSpecialValues) == "function") getSpecialValues(args); // Define by each edit page.
  args.sFileVersion = document.getElementById("sFileVersion").value;
  var result = gService.singleedit.call(methodName, args);
  var isFocus = true;
  if (!result.error)
  {
      if (result.value["sFileVersion"] != null)
      {
          var o = document.getElementById("sFileVersion");
          if (null != o) o.value = result.value["sFileVersion"];
      }
    var mainEdit = document.getElementById("MainEdit");
    if (null != recordID && null != mainEdit && mainEdit.style.display != "none") {
      populateForm(result.value);
      if (typeof(postPopulateForm) == "function") postPopulateForm(result.value);
    }
    
    // Set special values.
    if (typeof(setSpecialValues) == "function") setSpecialValues(result.value); // Define by each edit page.
    // Do not clear dirty bit if methodName = CalculateData.
    if (methodName != "CalculateData") parent.parent_disableSaveBtn(true); 
    else isFocus = false; // Don't reset new focus on background calculation operation.

} 
else 
{
    var errMsg = result.UserMessage || 'System error.  Please contact us if this happens again.';
    if (result.ErrorType === 'VersionMismatchException')
    {
        f_displayVersionMismatchSingleEdit();
    }
    else if (result.ErrorType === 'LoanFieldWritePermissionDenied') {
        f_displayFieldWriteDeniedSingleEdit(result.UserMessage);
    }
    else
    {
        alert(errMsg);
    }
}
  
  if (typeof(refreshUI) == 'function') refreshUI(isFocus);

  return result;
  
}
function f_displayVersionMismatchSingleEdit()
{
    showModal('/newlos/VersionMismatchDialog.aspx', null, null, null, function (args) {
        if (args.Action === 'Print') {
            lqbPrintByFrame(parent);
        }

        parent.location.href = parent.location.href;
    }, { hideCloseButton: true });
}
function f_displayFieldWriteDeniedSingleEdit(msg) {
    showModal('/newlos/LoanSaveDenial.aspx?msg=' + msg, null, null, null, function (args) {
        if (args.Action === 'Print') {
            lqbPrintByFrame(parent);
        }
        location.href = location.href;
    }, { hideCloseButton: true });
}
function single_saveMe() {
    result = invokeMethod("SaveDataAndLoadData", document.getElementById("RecordID").value);    
    if (!result.error) {
        if (null != document.getElementById("MainEdit") && document.getElementById("MainEdit").style.display != "none") {
            updateListIFrame(parent.parent_getCurrentIndex());
        }
        parent.parent_disableSaveBtn(true);
    }
    return result;
}

function setRowIndex(index) {
  document.getElementById("RowIndex").value = index - 1;
}
function setDirection(d) {
  document.getElementById("Direction").value = '' + d;
} 
function single_hideEdit(b) {
  var mainEdit = document.getElementById("MainEdit");
  if (null != mainEdit) {
    mainEdit.style.display= b ? 'none' : '';
    if (b) document.getElementById("RecordID").value = '00000000-0000-0000-0000-000000000000'; // Clear out RecordID if no record available.
  }
}
function single_sort(arg) {
  var previousSortExpression = document.getElementById("PreviousSortExpression").value;
  var parts = previousSortExpression.split(" ");
  var sortExpression = arg + ((parts.length == 2 && parts[0] == arg && parts[1] == "ASC") ? " DESC" : " ASC");
  
  document.getElementById("SortExpression").value = sortExpression;
  var methodName = "Sort";
  var ret = vbConfirmNo;
  if (parent.isDirty()) {
    ret = ConfirmSave();
    if (ret == vbConfirmCancel) return false;
    else if (ret == vbConfirmYes) methodName = "SaveDataAndSort";
  }  
  invokeMethod(methodName, "00000000-0000-0000-0000-000000000000");
  document.getElementById("PreviousSortExpression").value = sortExpression;
  return true;
}

function runListIFrameMethod(methodName, arg) {
      var method = callFrameMethod(window, "ListIFrame", methodName, [arg]);
  }
//----- End SingleIFrame js methods