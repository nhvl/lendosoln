﻿function enableDisableDflp(ClientID) {
    var poc = document.getElementById(ClientID + '_Poc_chk');
    var dflp = document.getElementById(ClientID + '_Dflp_chk');
    if (dflp != null && poc != null) {
        if (poc.checked) {
            dflp.checked = false;
            dflp.disabled = true;
        }
        else {
            dflp.disabled = false;
        }
    }
}

function OnPaidChecking(ClientID) {
    var poc = document.getElementById(ClientID + '_Poc_chk');
    var paid = document.getElementById(ClientID + '_Paid_chk');
    if (paid.checked && poc != null)
        poc.checked = true;
    enableDisableDflp(ClientID);
}

function OnPocCheck(ClientID) {
    var poc = document.getElementById(ClientID + '_Poc_chk');
    var paid = document.getElementById(ClientID + '_Paid_chk');
    if (!poc.checked && paid != null)
        paid.checked = false;
    enableDisableDflp(ClientID);
}

function OnTpCheck(ClientID) {
    var tp = document.getElementById(ClientID + '_TrdPty_chk');
    var aff = document.getElementById(ClientID + '_Aff_chk');
    if (tp != null && aff != null) {
        if (!tp.checked)
            aff.checked = false;
    }
}

function OnAffCheck(ClientID) {
    var tp = document.getElementById(ClientID + '_TrdPty_chk');
    var aff = document.getElementById(ClientID + '_Aff_chk');
    if (tp != null && aff != null) {
        if (aff.checked)
            tp.checked = true;
    }
}

function HideAgentPicker(ClientID) {
    var tb = document.getElementById(ClientID + '_PaidTo_tb');
    var nextSibling = tb.nextSibling;
    while (nextSibling && nextSibling.nodeType != 1) {
        nextSibling = nextSibling.nextSibling
    }
    nextSibling.style.display = 'none';
}

function PickAgent(ClientID, LegacyType) {
    var tb = document.getElementById(ClientID + '_PaidTo_tb');
    var ap = document.getElementById(ClientID + '_AgentPicker');

    if (tb.readOnly && tb.disablePicker == true) {
        return false;
    }

    var url = '/newlos/Status/Agents.aspx?loanid=' + ML.sLId + '&appid=' + ML.aAppId + '&picker=Y';
    showModal(url, null, null, null, function(result){
        if (result.OK == true) {
            var tb = document.getElementById(ClientID + '_PaidTo_tb');
            tb.value = result.compName;
    
            if (result.id == 'CLEAR') {
                // Clear Flags
                var tp = document.getElementById(ClientID + '_TrdPty_hdn');
                var aff = document.getElementById(ClientID + '_Aff_hdn');
                var qmWarn = document.getElementById(ClientID + '_QmWarn_hdn');
    
                tp.value = 'N';
                aff.value = 'N';
                qmWarn.value = 'N';
    
                if (typeof _clearAgent === 'function') {
                    _clearAgent(ClientID, LegacyType);
                }
            }
            else if (typeof _agentPicked === 'function') {
                // Call service that sets TP & AFF properties
                _agentPicked(ClientID, result.id, LegacyType);
            }
    
            updateDirtyBit();
        }
     })
}

function EnableAgentPicker(ClientID) {
    var tb = document.getElementById(ClientID + '_PaidTo_tb');
    var ap = document.getElementById(ClientID + '_AgentPicker');

    tb.disablePicker = false;
    ap.src = '../../images/contacts.png';
}

function DisableAgentPicker(ClientID) {
    var tb = document.getElementById(ClientID + '_PaidTo_tb');
    var ap = document.getElementById(ClientID + '_AgentPicker');

    tb.disablePicker = true;
    ap.src = '../../images/contacts_disabled.png';
}

function OnAgentHover(ClientID) {
    var tb = document.getElementById(ClientID + '_PaidTo_tb');
    var ap = document.getElementById(ClientID + '_AgentPicker');

    if (!tb.disablePicker) {
        ap.src = '../../images/contacts_clicked.png';
    }
}

function OnAgentHoverOut(ClientID) {
    var tb = document.getElementById(ClientID + '_PaidTo_tb');
    var ap = document.getElementById(ClientID + '_AgentPicker');
    
    if (!tb.disablePicker) {
        ap.src = '../../images/contacts.png';
    }
}

function SetWarning(ClientID) {
    // Skip sMipPiaProps since it doesn't make use of TP & AFF bits
    if (ClientID == 'sMipPiaProps_ctrl') {
        return;
    }

    var tb = document.getElementById(ClientID + '_PaidTo_tb');
    var ap = document.getElementById(ClientID + '_AgentPicker');
    var qmWarn = document.getElementById(ClientID + '_QmWarn_hdn');

    var bWarn = tb.value && tb.value.replace(/^\s+|\s+$/g, '') !== '';
    qmWarn.value = bWarn ? 'Y' : 'N';
}