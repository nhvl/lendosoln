﻿//! assumes we are in the left tree frame.
//! assumes that $ arguments are passed in with jquery.
/*
call like this:
var QRI = QuickReplyInjector(virtualRoot, loanId, version,  $injectorElements, targetFrame);
*/
function QuickReplyInjector(virtualRoot, loanId, version, $injectorElements, targetFrame) {
    var targetHead;
    this.obj = {};
    var instance = this;
    this.isInjecting = false;

    $injectorElements.click(onClickQuickReply);

    function onClickQuickReply() {
        targetHead = targetFrame.document.getElementsByTagName('head')[0];
        // if the targetFrame is conv log, do nothing.
        if (targetIsConvLog()) {
            return;
        }

        if (typeof targetFrame.QRC !== 'undefined' && targetFrame.QRC.openConversationLog !== 'undefined') {
            targetFrame.QRC.openConversationLog();
            return;
        }

        // inject the necessaary scripts to the target frame.
        injectQuickReplyClientScript();
    }

    function targetIsConvLog() {
        // return true iff target frame is the conversation log page.
        return targetFrame.location.href.indexOf("ConversationLog.aspx") > 0;
    }

    function injectQuickReplyClientScript() {
        injectLinkedScript('quickReplyClient.js');
    }

    function injectLinkedScript(scriptUrl, onLoadHandler) {
        var script = targetFrame.document.createElement('script');
        script.src = virtualRoot + '/inc/' + scriptUrl + '?v=' + version;
        if (typeof onLoadHandler !== 'undefined')
        {
            addOnLoadHandler(script, "load", onLoadHandler);
        }
        targetHead.appendChild(script);
    }

    function getObject()
    {
        return instance.obj;
    }


    function injectWhenIframeReady(isRecursive) {
        //! to this add something that checks if href is different than expected. (to prevent double clicking mattering.)
        //! possibly add something if the count > 100 or some arbitrary #.

        if (instance.obj.divIsOpen !== true) {
            return;
        }

        if (instance.isInjecting === true && false == isRecursive) {
            return;
        }
        instance.isInjecting = true;
        

        var iframe = targetFrame;

        var iframeDoc = iframe.document;
        if (typeof iframeDoc === 'undefined') {
            window.setTimeout(function () { injectWhenIframeReady(true) }, 100);
            return;
        }

        // Check if loading is complete
        if (iframeDoc.readyState != 'complete') {
            window.setTimeout(function () { injectWhenIframeReady(true) }, 100);
            return;
        }

        onClickQuickReply();
        instance.isInjecting = false;
    }

    return {
        getObject: getObject,
        injectWhenIframeReady: function () { injectWhenIframeReady(false); },
        getBasicInfo: function () { return { loanId: loanId, version: version, virtualRoot: virtualRoot } }
    };
}