﻿var ConditionDocumentUploadHelper = (function ($) {
    var loanId = null;
    var taskId = null;
    var dropzoneId = null;
    var $errorDiv = null;

    var droppedFileCount = 0;
    var totalFileProcessedCount = 0;
    var uploadedDocuments = [];

    function _uploadDocs(dropzone, loanIdForUpload, taskIdForUpload, isPml) {
        loanId = loanIdForUpload;
        taskId = taskIdForUpload;
        dropzoneId = dropzone.id;

        if (getFilesCount(dropzoneId) === 0) {
            displayAlert('Please select file(s) to upload.');
            return;
        }

        var filenames = retrieveFiles(dropzone.id).map(function (file) { return file.name; });

        gService.utils.callAsyncSimple('StoreIdListToCache', { IdList: JSON.stringify(filenames) }, function (result) {
            if (result.error) {
                displayAlert(result.UserMessage);
                return;
            }

            var args = {
                loanid: loanId,
                TaskId: taskId,
                CacheKey: result.value['CacheKey']
            };

            var queryString = _generateQueryString(args);

            if (isPml) {
                IframePopup.show(gVirtualRoot + '/main/EDocs/UploadDocToTask.aspx?' + queryString, {}).then(function (pair) {
                    var error = pair[0];
                    var result = pair[1];
                    if (error != null) {
                        console.error(error);
                        simpleDialog.alert(typeof error == "string" ? error : JSON.stringify(error), "ERROR");
                        return;
                    }

                    if (result == null) {
                        resetDragDrop(dropzone.id);
                        return;
                    }

                    _onUploadDocsPopupClosed(result.value);
                });
            }
            else {
                var settings = {
                    onReturn: _onUploadDocsPopupClosed,
                    onClose: function () { resetDragDrop(dropzone.id); }
                };

                LQBPopup.Show(gVirtualRoot + '/newlos/ElectronicDocs/UploadDocToTask.aspx?' + queryString, settings);
            }
        });
    }

    function _generateQueryString(obj) {
        var o = obj || '';
        var str = '';
        for (var p in o) {
            if (o.hasOwnProperty(p)) {
                str += p + '=' + encodeURIComponent(o[p]) + '&';
            }
        }
        return str;
    }

    function _onUploadDocsPopupClosed(result) {
        if (!result.OK) {
            resetDragDrop(dropzoneId);
            return;
        }

        $errorDiv = $('#' + dropzoneId).parent().parent().find('.drag-drop-error-div');
        $errorDiv.empty();

        var allDocsArgs = {
            loanid: loanId
        };

        var allFiles = retrieveFiles(dropzoneId);
        var removedFiles = allFiles.filter(function (file) {
            return typeof result.fileDataByName[file.name] === 'undefined'
        });

        for (var i = 0; i < removedFiles.length; ++i) {
            removeFile(dropzoneId, removedFiles[i]);
        }

        droppedFileCount = getFilesCount(dropzoneId);
        uploadedDocuments = [];
        totalFileProcessedCount = 0;
        triggerOverlayImmediately();

        applyCallbackFileObjects(dropzoneId, function (file) {
            var selectedDocArgs = result.fileDataByName[file.name];
            var docArgs = $.extend({}, allDocsArgs, selectedDocArgs);

            DocumentUploadHelper.Upload('UploadDocument', file, docArgs, function (serviceResult) {
                _uploadDocumentCallback(file.name, selectedDocArgs.DocTypeId, serviceResult);
            });
        });
    }

    function _uploadDocumentCallback(filename, docTypeId, serviceResult) {
        if (serviceResult.error || serviceResult.value['Error'] === 'True') {
            var $errorMessage = $('<div/>').text(filename + " upload failed: " + (serviceResult.UserMessage || serviceResult.value['UserMessage']));
            $errorDiv.append($errorMessage);
        }
        else {
            uploadedDocuments.push({
                DocumentId: serviceResult.value['DocumentId'],
                DocTypeId: docTypeId
            });
        }

        ++totalFileProcessedCount;
        if (totalFileProcessedCount === droppedFileCount) {
            _associateDocuments();
        }
    }

    function _associateDocuments() {
        removeOverlay();
        resetDragDrop(dropzoneId);

        if (uploadedDocuments.length === 0) {
            _postAssociateCallback();
            return;
        }

        if (typeof documentUploadPostUploadCallback === 'function') {
            documentUploadPostUploadCallback(dropzoneId, taskId, uploadedDocuments);
            return;
        }

        var args = {
            TaskId: taskId,
            DocumentIdJson: JSON.stringify(uploadedDocuments.map(function (doc) { return doc.DocumentId; }))
        };

        gService.utils.callAsyncSimple('AssociateDocsWithTask', args, function (associateResult) {
            if (associateResult.error) {
                var $errorMessage = $('<div/>').text("Documents uploaded but failed to associate, please associate manually. Error: " + associateResult.UserMessage);
                $errorDiv.append($errorMessage);
            }

            _postAssociateCallback();
        });
    }

    function _postAssociateCallback() {
        if ($errorDiv.find('div').length !== 0) {
            var $uploadNotice = $('<div class="upload-notice" />').text('All other documents uploaded and associated.');
            $errorDiv.append($uploadNotice).show();
        }
        else if (typeof documentUploadPostAssociationCallback === 'function') {
            documentUploadPostAssociationCallback();
        }
    }

    function displayAlert(text) {
        LQBPopup.ShowString(text, {
            width: 250,
            height: 25,
            popupClasses: 'condition-upload-complete',
            onClose: function () { location.href = location.href; }
        });
    }

    return {
        UploadDocs: _uploadDocs
    };
})(jQuery);