﻿
var AdjustmentTable = {

    isOptionArm: false,
    currentIndex: 0,
    alternate: false,
    tableData: null,
    table: null,
    showDeleteCheckbox: false,
    selectAllCheckbox: null,
    deleteAllButton: null,
    isReadOnly: false,
    showHiddenCB: true,
    showLenderCB: false,
    showPersistCB: false,

    InstantiateTable: function(tableId, data, isOptionArm, isReadOnly, renderVisibleHiddenCB, showLenderCB, showPersistCB) {
        this.table = document.getElementById(tableId);
        this.tableData = this.table.tBodies[0];
        this.isOptionArm = isOptionArm;
        this.isReadOnly = isReadOnly;
        this.showHiddenCB = renderVisibleHiddenCB;
        this.showLenderCB = !!showLenderCB;
        this.showPersistCB = !!showPersistCB;

        if (!data || data.length < 1) {
            return;
        }

        for (var i = 0; i < data.length; i++) {
            this.__createAndAppendAdjustmentRow(i, data[i]);
        }

        this.currentIndex = data.length - 1;
    },


    EnableDeleteEntries: function(selectAllId, deleteAllButtonId) {
        this.showDelete = true;
        this.selectAllCheckbox = document.getElementById(selectAllId);
        this.deleteAllButton = document.getElementById(deleteAllButtonId);
        this.deleteAllButton.disabled = true;
        addEventHandler(this.selectAllCheckbox, 'click', function() { AdjustmentTable.__selectAllRows(); });
        addEventHandler(this.deleteAllButton, 'click', function() { AdjustmentTable.__deleteSelectedRows(); });
    },



    __selectAllRows: function() {
        if (this.isReadOnly) {
            return;
        }
        var checked = this.selectAllCheckbox.checked;
        var btn = this.deleteAllButton;

        for (var i = 0; i < this.tableData.rows.length; i++) {
            var index = this.tableData.rows[i].getAttribute("AdjustmentIndex");
            document.getElementById('DeleteEntry' + index).checked = checked;
        }

        if (this.tableData.rows.length > 0 && checked) {
            btn.disabled = false;
        }
        else {
            btn.disabled = true;
        }

    },

    __deleteSelectedRows: function() {
        var rowDeleted;
        var rowLength = this.tableData.rows.length - 1;
        for (var i = rowLength; i >= 0; i--) {
            var row = this.tableData.rows[i];
            var index = row.getAttribute("AdjustmentIndex");
            var item = document.getElementById('DeleteEntry' + index);

            if (item.checked) {
                this.tableData.deleteRow(i);
                rowDeleted = true;
            }
        }

        if (typeof (f_adjustmentChange) === 'function' && rowDeleted) {
            f_adjustmentChange('delete');
        }

        this.deleteAllButton.disabled = true;
    },

    __updateDeleteButton: function() {
        var enable = true;
        for (var i = 0; i < this.tableData.rows.length; i++) {
            var index = this.tableData.rows[i].getAttribute("AdjustmentIndex");
            var item = document.getElementById('DeleteEntry' + index);
            if (item.checked) {
                enable = false;
                break;
            }
        }

        this.deleteAllButton.disabled = enable;
    },


    __createAndAppendElement: function(oParent, tag, attributes, className) {
        var item = document.createElement(tag);

        for (var attr in attributes) {
            if (attributes.hasOwnProperty(attr)) {
                if (item[attr] !== undefined) {
                    if (attr === "class" || attr === "className") {
                        if (item[attr] === "")
                            item[attr] = attributes[attr];
                        else
                            item[attr] = item[attr] + " " + attributes[attr];
                    }
                    else
                        item[attr] = attributes[attr];
                }
                else
                    item.setAttribute(attr, attributes[attr]);
            }
        }

        if (className) {
            item.className = className;
        }
        oParent.appendChild(item);

        if (tag === 'input') {
            _initDynamicInput(item);
        }

        return item;
    },

    __adjustmentTableInputOnChange: function(update) {
        if (typeof (f_adjustmentChange) === 'function') {
            f_adjustmentChange(update);
        }

    },

    __handlePriceFeeUpdate: function(sourceId, destinationId, sourceType) {
        f_updateAdjustmentPriceFee(sourceId, destinationId, sourceType);
    },

    __createAndAppendAdjustmentRow: function(index, data) {

        if (typeof (readOnly) === 'undefined') {
            readOnly = false;
        }

        var oTBody, oTR, oTD, oInput;
        oTBody = this.tableData;
        var cssClass = this.alternate ? 'GridAlternatingItem' : 'GridItem';
        this.alternate = !this.alternate;
        oTR = this.__createAndAppendElement(oTBody, 'tr', { 'AdjustmentIndex': index }, cssClass);

        if (this.showDelete) {
            /*DeleteButton*/
            oTD = this.__createAndAppendElement(oTR, 'td', {});
            var oInput = this.__createAndAppendElement(oTD, 'input', {
                'SkipMe': 'SkipMe',
                'id': 'DeleteEntry' + index,
                'name': 'DeleteEntry' + index,
                'type': 'checkbox',
                'AdjustmentDeleteEntry': index
            });

            addEventHandler(oInput, 'click', function() { AdjustmentTable.__updateDeleteButton(); });

            oInput.disabled = this.isReadOnly;

        } /*end if show delete*/



        /*Description*/
        oTD = this.__createAndAppendElement(oTR, 'td', {});
        oInput = this.__createAndAppendElement(oTD, 'input', {
            'SkipMe': 'SkipMe',
            'id': 'Description' + index,
            'name': 'Description' + index,
            'value': data.Description,
            'type': 'text'
        }, 'descriptionField');

        oInput.readOnly = this.isReadOnly

        addEventHandler(oInput, 'change', function() { AdjustmentTable.__adjustmentTableInputOnChange('description'); });

        /*IsLenderAdjustment*/
        if (!this.showLenderCB) {
            this.__createAndAppendElement(oTD, 'input', {
                'SkipMe': 'SkipMe',
                'id': 'IsLenderAdjustment' + index,
                'name': 'IsLenderAdjustment' + index,
                'type': 'hidden',
                'value': data.IsLenderAdjustment
            });
        }
        /*IsSRPAdjustment*/
        this.__createAndAppendElement(oTD, 'input', {
            'SkipMe': 'SkipMe',
            'id': 'IsSRPAdjustment' + index,
            'name': 'IsSRPAdjustment' + index,
            'type': 'hidden',
            'value': data.IsSRPAdjustment
        });
        /*IsPersist*/
        if (!this.showPersistCB) {
            this.__createAndAppendElement(oTD, 'input', {
                'SkipMe': 'SkipMe',
                'id': 'IsPersist' + index,
                'name': 'IsPersist' + index,
                'type': 'hidden',
                'value': data.IsPersist
            });
        }
        if (!this.showHiddenCB) {
            this.__createAndAppendElement(oTD, 'input', {
                'SkipMe': 'SkipMe',
                'id': 'Hidden' + index,
                'name': 'Hidden' + index,
                'type': 'hidden',
                'value': data.IsHidden ? 'true' : 'false'
            });
        }

        /*Rate*/
        oTD = this.__createAndAppendElement(oTR, 'td', {});
        oInput = this.__createAndAppendElement(oTD, 'input', {
            'SkipMe': 'SkipMe',
            'id': 'Rate' + index,
            'name': 'Rate' + index,
            'value': data.Rate,
            'type': 'text',
            'preset': 'percent'
        }, 'rateField');

        addEventHandler(oInput, 'change', function() { AdjustmentTable.__adjustmentTableInputOnChange('Rate'); });
        oInput.readOnly = this.isReadOnly;

        /*Price*/
        oTD = this.__createAndAppendElement(oTR, 'td', {});
        oInput = this.__createAndAppendElement(oTD, 'input', {
            'SkipMe': 'SkipMe',
            'id': 'Price' + index,
            'name': 'Price' + index,
            'value': data.Price,
            'type': 'text'
        }, 'rateField');

        oInput.readOnly = this.isReadOnly;

        var feeId = 'Fee' + index;

        addEventHandler(oInput, 'change', (function(id, priceid) {
            return function() {
                AdjustmentTable.__handlePriceFeeUpdate(id, feeId, 'P');
                AdjustmentTable.__adjustmentTableInputOnChange('fee');
            }
        })(oInput.id, feeId));
       
        /*Fee*/
        oTD = this.__createAndAppendElement(oTR, 'td', {});
        oInput = this.__createAndAppendElement(oTD, 'input', {
            'SkipMe': 'SkipMe',
            'id': 'Fee' + index,
            'name': 'Fee' + index,
            'value': data.Fee,
            'type': 'text'
        }, 'rateField');

        oInput.readOnly = this.isReadOnly;

        var priceId = 'Price' + index;

        addEventHandler(oInput, 'change', (function(id, priceid) {
            return function() {
                AdjustmentTable.__handlePriceFeeUpdate(id, priceId, 'F');
                AdjustmentTable.__adjustmentTableInputOnChange('price');
            }
        })(oInput.id, priceId));

        /*Margin*/
        oTD = this.__createAndAppendElement(oTR, 'td', {});
        oInput = this.__createAndAppendElement(oTD, 'input', {
            'SkipMe': 'SkipMe',
            'id': 'Margin' + index,
            'name': 'Margin' + index,
            'value': data.Margin,
            'type': 'text',
            'preset': 'percent'

        }, 'rateField');

        oInput.readOnly = this.isReadOnly;

        addEventHandler(oInput, 'change', function() { AdjustmentTable.__adjustmentTableInputOnChange('Margin'); });

        /*T Rate */
        oTD = this.__createAndAppendElement(oTR, 'td', {});
        oInput = this.__createAndAppendElement(oTD, 'input', {
            'SkipMe': 'SkipMe',
            'id': 'TeaserRate' + index,
            'name': 'TeaserRate' + index,
            'value': data.TeaserRate,
            'type': 'text',
            'preset': 'percent'
        }, 'rateField');

        if (!this.isOptionArm) {
            oInput.readOnly = true;
        }
        else {
            oInput.readOnly = this.isReadOnly;
        }

        addEventHandler(oInput, 'change', function () { AdjustmentTable.__adjustmentTableInputOnChange('TeaserRate'); });

        if (this.showHiddenCB) {
            /*Hidden*/
            oTD = this.__createAndAppendElement(oTR, 'td', { 'align': 'center' });
            oInput = this.__createAndAppendElement(oTD, 'input', {
                'SkipMe': 'SkipMe',
                'id': 'Hidden' + index,
                'name': 'Hidden' + index,
                'type': 'checkbox'
            });

            addEventHandler(oInput, 'click', function() { AdjustmentTable.__adjustmentTableInputOnChange('IsHidden'); });

            if (data.IsHidden) {
                oInput.checked = 'checked';
            }
            oInput.disabled = this.isReadOnly;
        }

        if (this.showLenderCB) {
            oTD = this.__createAndAppendElement(oTR, 'td', { 'align': 'center' });
            oInput = this.__createAndAppendElement(oTD, 'input', {
                'SkipMe': 'SkipMe',
                'id': 'IsLenderAdjustment' + index,
                'name': 'IsLenderAdjustment' + index,
                'type': 'checkbox'
            });

            if (data.IsLenderAdjustment) {
                oInput.checked = 'checked';
            }

            oInput.disabled = this.isReadOnly;

            addEventHandler(oInput, 'click', function() { AdjustmentTable.__adjustmentTableInputOnChange('IsLenderAdjustment'); });
        }

        if (this.showPersistCB) {
            oTD = this.__createAndAppendElement(oTR, 'td', { 'align': 'center' });
            oInput = this.__createAndAppendElement(oTD, 'input', {
                'SkipMe': 'SkipMe',
                'id': 'IsPersist' + index,
                'name': 'IsPersist' + index,
                'type': 'checkbox'
            });

            if (data.IsPersist) {
                oInput.checked = 'checked';
            }
            oInput.disabled = this.isReadOnly;

            addEventHandler(oInput, 'click', function () { AdjustmentTable.__adjustmentTableInputOnChange('IsPersist'); });
        }
    },

    __IE8NullFix: function(v) {
        return v === "" ? "" : v; //http://blogs.msdn.com/jscript/archive/2009/06/23/serializing-the-value-of-empty-dom-elements-using-native-json-in-ie8.aspx
    },

    AddAdjustment: function(data) {
        this.currentIndex++;
        this.__createAndAppendAdjustmentRow(this.currentIndex, data);
    },

    Serialize: function(data) {
        var g = function(id) { return document.getElementById(id); };
        var adjustments = [];
        for (var i = 0; i < this.tableData.rows.length; i++) {
            var index = this.tableData.rows[i].getAttribute("AdjustmentIndex");
            var adjustment = {
                Description: this.__IE8NullFix(g('Description' + index).value),
                Rate: this.__IE8NullFix(g('Rate' + index).value),
                Price: this.__IE8NullFix(g('Price' + index).value),
                Fee: this.__IE8NullFix(g('Fee' + index).value),
                Margin: this.__IE8NullFix(g('Margin' + index).value),
                TeaserRate: this.__IE8NullFix(g('TeaserRate' + index).value),
                IsHidden: this.showHiddenCB ? g('Hidden' + index).checked : this.__IE8NullFix(g('Hidden' + index).value),
                IsSRPAdjustment: g('IsSRPAdjustment' + index).value,
                IsLenderAdjustment: this.showLenderCB ? g('IsLenderAdjustment' + index).checked : g('IsLenderAdjustment' + index).value,
                IsPersist: this.showPersistCB ? g('IsPersist' + index).checked : g('IsPersist' + index).value

            };

            adjustments.push(adjustment);
        }
        return JSON.stringify(adjustments);
    },

    AddEmptyAdjustment: function() {
        var data = {
            Description: '',
            Rate: '0.000%',
            Price: '0.000%',
            Fee: '0.000%',
            Margin: '0.000%',
            TeaserRate: '0.000%',
            Hidden: false,
            IsLenderAdjustment: false,
            IsSRPAdjustment: false,
            IsPersist: false
        };

        this.AddAdjustment(data);
        if (typeof (updateDirtyBit) === 'function') {
            updateDirtyBit();
        }
    },



    SetReadOnly: function(isReadOnly) {
        this.isReadOnly = isReadOnly;
        for (var i = 0; i < this.tableData.rows.length; i++) {
            var row = this.tableData.rows[i];
            var inputs = row.getElementsByTagName('input');

            for (var x = 0; x < inputs.length; x++) {
                var input = inputs[x];
                if (input.type === 'hidden') {
                    continue;
                }
                if (!this.isOptionArm && input.id.indexOf('TeaserRate') !== -1) {
                    continue;
                }
                if (input.type === 'checkbox') {
                    input.disabled = isReadOnly;
                }
                else {
                    input.readOnly = isReadOnly;
                }

                if (input.id.indexOf('Delete') !== -1) {
                    input.checked = false;
                }
            }
        }

        this.selectAllCheckbox.disabled = isReadOnly;
    }
};