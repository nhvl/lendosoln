// This script file is for navigate single record edit screen.
// This script should only be use with page inherit from BaseSingleEditPage. Use these
// function by itself will result in error. Consider yourself warn.

// 9/24/2003 -dd -  Liability / Assets UI had change quite dramaticly. 
//   Each section has 2 frames, list frame and single edit frame.
//   To consolidate js script, I pull all script in list frame and single edit frame here.

// 08/02/06 mf - Made the single record editors honor validators
//               so we don't save invalid data.

//-------- List Frame js methods.
var list_oTable = null;

function list_deleteRow(index) {
  list_oTable.deleteRow(index);
  list_selectRow(index);
}

function list_selectRow(index) {
  PolyShouldShowConfirmSave(parent.isDirty(), function(){
    var previousIndex = parent.parent_getCurrentIndex();
    if (previousIndex > 0 && previousIndex < list_oTable.rows.length) {
      list_oTable.rows[previousIndex].style.backgroundColor = '';
      list_oTable.rows[previousIndex].style.color = '';
    }
    if (index > 0 && index <= list_oTable.rows.length && list_oTable.rows.length > 1) {
      if (index == list_oTable.rows.length)
          index = index - 1; // Last record get delete.
      list_oTable.rows[index].style.backgroundColor = 'blue';
      list_oTable.rows[index].style.color = 'white';
      parent.parent_setCurrentIndex(index);
      var recordID = list_oTable.rows[index].getAttribute("recordid");
      if (recordID != '00000000-0000-0000-0000-000000000000') {
          callFrameMethod(parent, "SingleIFrame", "loadData", [recordID]);// Load data to edit screen.  
      } else {
          callFrameMethod(parent, "SingleIFrame", "resetForm", [false]);
      }
    } else {
      parent.parent_setCurrentIndex(0);
      callFrameMethod(parent, "SingleIFrame", "hideEdit", [true]);
    }
      
    parent.parent_disableSaveBtn(true);
  }, function(){ parent.saveMe() }, function() { parent.clearDirty(); } );
}
  
function list_getRecordID(index) {
  if (index < 0 || index >= list_oTable.rows.length) return null;
    
  return list_oTable.rows[index].getAttribute("recordid");    
}    
function list_setCurrentRecordID(id) {
  // set recordid for current row. This method only use when new record is added.
  list_oTable.rows[parent.parent_getCurrentIndex()].recordid = id;
}  
// Move record forward or backward.
// d = -1  Move backward
// d = 1 Move forward    
function list_go(d) {
  var previousIndex = parent.parent_getCurrentIndex();
  var index = previousIndex;
  if (d == -1 && previousIndex > 1) {
    index--;
  } else if (d == 1 && previousIndex < list_oTable.rows.length - 1) {
    index++;
  }
    
  if (index > 0 && index < list_oTable.rows.length) {
    list_selectRow(index);
  }
}
    
// Move record up or down.
// d = -1 Move down.
// d = 1 Move up.
// Return true if move successfully.
function list_move(d) {
  if (d != -1 && d != 1) return; // Need to be either 0 or 1.
  var previousIndex = parent.parent_getCurrentIndex();

  if (d == 1 && previousIndex <= 1) return; // Can't move up.
  if (d == -1 && previousIndex >= list_oTable.rows.length - 1) return; // Can't move down.

  PolyShouldShowConfirmSave(parent.isDirty(), function(){
    swapNodePoly(list_oTable.rows[previousIndex], list_oTable.rows[previousIndex - d]);
    list_selectRow(previousIndex - d);
    callFrameMethod(parent, "SingleIFrame", "moveRecord", [d]);
  }, function(){ parent.saveMe() }, function() { parent.clearDirty(); } );
}
    
// First argument is rowIndex, second argument is recordid, the rest is value in column.
function list_updateRow() {
  var len = list_updateRow.arguments.length;
  if (len > 2) {
    var iRowIndex = list_updateRow.arguments[0];
    list_oTable.rows[iRowIndex].setAttribute("recordid", list_updateRow.arguments[1]); // Retrieve recordid
    for (var i = 2; i < len; i++) 
      list_oTable.rows[iRowIndex].cells[i - 2].innerText = list_updateRow.arguments[i];
  }
}

function list_addRow() {
  list_insertRow(list_oTable.rows.length);
}

function list_insertRow(index) {
  PolyShouldShowConfirmSave(parent.isDirty(), function(confirmed){
    // Clear out the highlight of previous selected row.
    var previousIndex = parent.parent_getCurrentIndex();
    if (previousIndex > 0 && previousIndex < list_oTable.rows.length) {      
      list_oTable.rows[previousIndex].style.backgroundColor = '';
      list_oTable.rows[previousIndex].style.color = '';
    }
        
    if (index == 0) index = 1; // Insert correctly on empty result list.
    var tr = list_oTable.insertRow(index);
    tr.className='GridItem';
    tr.onclick =  function() { list_selectRow(this.rowIndex); };
    tr.setAttribute("recordid", "00000000-0000-0000-0000-000000000000");
    
    if (typeof(constructEmptyRow) != "function") {
      alert("Need to define constructEmptyRow method.");
    }
      
    constructEmptyRow(tr); // Need to define constructEmptyRow in each list.
    
    list_selectRow(index);
    callFrameMethod(parent, "SingleIFrame", "setRowIndex", [index]);
    callFrameMethod(parent, "SingleIFrame", "createNew");

    return;
  }, function(){ parent.saveMe() }, function() { parent.clearDirty(); } );
}

//------- End List Frame js methods.

//------- Parent frame js methods.
var parent_iCurrentIndex = 1;
var bSingleEditDirtyBit = false;
var bSpecialRecordDirtyBit = false;

var bSingleFrameReady = false;
var bListFrameReady = false;

function parent_specialDirtyBit() {
  bSpecialRecordDirtyBit = true; 
  parent_disableSaveBtn(false); // Special record now enable individual save btn. dd 9/30/03
}

//ui = 1 if this method is call from Single edit screen.
//ui = 2 if this method is call from List screen.
function parent_initScreen(ui) {
    if (ui == 1) bSingleFrameReady = true;
    if (ui == 2) bListFrameReady = true;

    if (bSingleFrameReady && bListFrameReady) {
        // Load record when both list and single frame are load completely.
        callFrameMethod(window, "ListIFrame", "list_selectRow", [parent_iCurrentIndex]);
    }
}

//When record is insert or add, the list will not refresh correctly when user decide to 
//click on move up/down and delete btn before saving. To avoid the problem, I just disabled these buttons.
function parent_disableBtnWhenNew(b) {
    if (document.getElementById("btnUp") != null) {
        document.getElementById("btnUp").disabled = b;
        document.getElementById("btnDown").disabled = b;
        document.getElementById("btnDelete").disabled = b;
        document.getElementById("btnInsert").disabled = b;
        document.getElementById("btnInsert").style.backgroundColor = '';
        document.getElementById("btnAdd").disabled = b;
        document.getElementById("btnAdd").style.backgroundColor = '';
    }
}
function parent_clearDirty() {
  bSingleEditDirtyBit = false;
    //bSpecialRecordDirtyBit = false;
  callFrameMethod(parent, "SingleIFrame", "clearDirty");
  parent_disableBtnWhenNew(false);
}
function parent_onInsert() {
    callFrameMethod(window, "ListIFrame", "list_insertRow", [parent_iCurrentIndex]);
    parent_disableBtnWhenNew(true);
}

function parent_onAdd() {
  if (callFrameMethod(window, "ListIFrame", "list_addRow"))
    parent_disableBtnWhenNew(true);
}  
    
function parent_getCurrentIndex() {
  return parent_iCurrentIndex;
}
  
function parent_setCurrentIndex(index) {
  parent_iCurrentIndex = index;
}

function parent_disableSaveBtn(b) {

  var btn = document.getElementById("btnSave");
  if (btn == null && parent.window.frames != null)
  {
    for (var i = 0; i<parent.window.frames.length; i++)
    {
      btn = parent.window.frames[i].document.getElementById("btnSave");
      if (btn != null)
        break;
    }
  }
  if (btn != null)
  {
  btn.disabled = b;
  btn.style.backgroundColor = '';
  bSingleEditDirtyBit = !btn.disabled;
  }
  if (!bSpecialRecordDirtyBit) {
    if (bSingleEditDirtyBit) 
      updateDirtyBit();
    else
      clearDirty();
  } 
}
//-------- End Parent frame js methods


var currentIndex = 0;

function _initEdit() {
  if (typeof(document.getElementById("currentindex")) == "undefined") return;
  if (typeof(refreshUI) == 'function') refreshUI();
  currentIndex = parseInt(document.getElementById("currentindex").value);
  updateButtons();
}

function switchApplicant() {
  goToList();
}      

function saveMe(href) {
  if (null == gService.singleedit) return false;
  if (isPageValid() == false) return false;
  var args = getAllFormValues();
  if (typeof (postGetAllFormValues) == "function") postGetAllFormValues(args);
  var result = invokeService("SaveData", args, true);
  if (!result.error) {
      document.getElementById('IndexLabel').style.visibility = 'visible';
      if (typeof (parent.parent_disableBtnWhenNew) == "function") parent.parent_disableBtnWhenNew(false);
      parent_disableSaveBtn(true);
  }
  return !result.error;
}

function updateButtons() {
  if (document.getElementById("IndexLabel") != null)
    document.getElementById("IndexLabel").innerText = "(" + (currentIndex + 1) + " of " + list.length + ")";
  if (document.getElementById("OwnerNameLabel") != null && document.getElementById("OwnerName") != null)
    document.getElementById("OwnerNameLabel").innerText = document.getElementById("OwnerName").value;

  
  if (null != document.getElementById("btnPrevious")) {  
    document.getElementById("btnPrevious").disabled = currentIndex == 0;
    document.getElementById("btnPrevious").style.backgroundColor = "";
  }
  if (null != document.getElementById("btnNext")) {
    document.getElementById("btnNext").disabled = currentIndex >= (list.length - 1);
    document.getElementById("btnNext").style.backgroundColor = "";
  }
}

function _createNewRecord(){
  if (document.getElementById("RecordID").value == '00000000-0000-0000-0000-000000000000'){
    return;
  }

  document.getElementById("btnNext").disabled = true;
  document.getElementById("btnNext").style.backgroundColor = "";
	document.getElementById("RecordID").value = '00000000-0000-0000-0000-000000000000';
	if (typeof(resetForm) == 'function'){
    resetForm(true);
  } 

  currentIndex++;

  if (document.getElementById("IndexLabel") != null){
		document.getElementById("IndexLabel").innerText = "(" + (currentIndex + 1) + " of " + list.length + ")";
  }
}

function saveAndCreateNew() {
  if (isDirty() && !saveMe()){
    return;
  }

  _createNewRecord();
}

function createNew() {
  PolyShouldShowConfirmSave(isDirty(), function(confirmed){
    if (confirmed && !saveMe()){
      return;
    }
    
    _createNewRecord(); 
  });
}

// Helper function.
// A reason why I should avoid short name variable. Have no idea what b is for :-) dd 9/9/03
function invokeService(name, args, b) {
  var result = gService.singleedit.call(name, args);
  if (!result.error)
  {
      populateForm(result.value);
      if (typeof (postPopulateForm) == "function") postPopulateForm(result.value);
      clearDirty();
      if (b)
      {
          // If the record is new then append the id to the ids list for navigation.
          var isNewID = true;
          var recordID = result.value["PrevRecordID"];


          for (var i = 0; i < list.length; i++)
          {
              if (recordID == list[i])
              {
                  isNewID = false;
                  break;
              }
          } // end for
          if (isNewID && recordID != null)
          {
              //alert('Set new recordid to ' + recordID);
              list[list.length] = recordID;
              document.getElementById("RecordID").value = recordID;
              callFrameMethod(parent, "ListIFrame", "list_setCurrentRecordID", [recordID]);
              currentIndex = list.length - 1;
          }
      }

  } 
  else
  {
      var errMsg = result.UserMessage || 'Unable to save data. Please try again.';
      if (result.ErrorType === 'VersionMismatchException')
      {
          if (typeof (f_displayVersionMismatch) == 'function')
          {
              f_displayVersionMismatch();
          }
      }
      else if (result.ErrorType === 'LoanFieldWritePermissionDenied') {
          if (typeof (f_displayFieldWriteDenied) == 'function') {
              f_displayFieldWriteDenied(result.UserMessage);
          }
      } 
      else
      {
          alert(errMsg);
      }
  }
  if (typeof(refreshUI) == 'function') refreshUI();
  updateButtons();
  
  return result;
}
      
function goToList() {
  PolyShouldShowConfirmSave(isDirty(), function(confirmed){
    if (confirmed && !saveMe()){
      return;
    }

    location.href= document.getElementById("ListLocation").value + '&appid=' + encodeURIComponent(parent.info.f_getCurrentApplicationID());
  });
}

function saveDataAndLoadNext() {
  if (isPageValid() == false) return false;
  var args = getAllFormValues();
  if (typeof (postGetAllFormValues) == "function") postGetAllFormValues(args);  
  
  args["nextrecordid"] = list[currentIndex];
  invokeService("SaveDataAndLoadNext", args, true);
  return true;
}
function loadNext() {
  var args = getAllFormValues();
  if (typeof (postGetAllFormValues) == "function") postGetAllFormValues(args);  
  
  if (typeof args !== 'undefined') {
      if (typeof args.RecordID !== 'undefined') {
          args.RecordID = list[currentIndex];
      }
      else {
          args.recordid = list[currentIndex];
      }
  } 

  invokeService("LoadData", args, false);
}
function loadData(id) {
  document.getElementById("RecordID").value = id;
  
  if (id == '00000000-0000-0000-0000-000000000000') return; // Don't load if new record.
  hideEdit(false);

  var args = getAllFormValues();
  if (typeof (postGetAllFormValues) == "function") postGetAllFormValues(args);  
  
  var result = invokeService("LoadData", args,  false);
  
  // Allow each page to be customize after each load.
  if (!result.error && typeof(onLoadSuccessful) == "function") onLoadSuccessful(result);
    
}
function navigate(isNext) {
  PolyShouldShowConfirmSave(isDirty(), function(confirmed){
    isNext? currentIndex++ :currentIndex--;    

    var result = confirmed ? saveDataAndLoadNext() : loadNext();
    if ( result === false ){
      isNext? currentIndex-- :currentIndex++;    
    }
  });
}

function hideEdit(b) {
  if (null != document.getElementById("MainEdit")) {
    document.getElementById("MainEdit").style.display= b ? 'none' : '';
    if (b) document.getElementById("RecordID").value = '00000000-0000-0000-0000-000000000000';
  }
}
      
function deleteRecord() {
  var args = getAllFormValues();
  if (typeof (postGetAllFormValues) == "function") postGetAllFormValues(args);  
  
  var result = invokeService('DeleteRecord', args, false);
  if (!result.error) document.getElementById("RecordID").value = '00000000-0000-0000-0000-000000000000';
}
function moveRecord(d) {
  var args = getAllFormValues();
  if (typeof (postGetAllFormValues) == "function") postGetAllFormValues(args);  
  
  args["direction"] = '' + d; // Need to convert int to string
  invokeService("MoveRecord", args, false);
}
      
      
function goPrevious() {
  if (currentIndex > 0)
  {
    document.getElementById('IndexLabel').style.visibility = 'visible';
    navigate(false);
  }
}

function goNext() {
  if (currentIndex < list.length - 1)
  {
    document.getElementById('IndexLabel').style.visibility = 'visible';
    navigate(true);
  }
}

function setRowIndex(index) {
  document.getElementById("RowIndex").value = index - 1;
}

function isPageValid()
{
  var bValid = true;
  if (typeof(Page_ClientValidate) == 'function' || typeof(Page_ClientValidate) == 'object')
    bValid = Page_ClientValidate();
  return bValid;
}    
