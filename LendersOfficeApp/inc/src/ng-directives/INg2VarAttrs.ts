/*

% Author: Vien Luong

Usage
=====

1. In definition of a directive

    ```typescript
    someDirective.$inject = [ "$parse" ];
    export function someDirective($parse: ng.IParseService): ng.IDirective {
        return {
            controller      : SomeController.ngName,
            link(scope: ng.IScope, $e: ng.IAugmentedJQuery, attrs: INg2VarAttrs, ctrl: DocTypePickerController) {
                $parse(attrs.ng2Var).assign(scope.$parent, ctrl);
            },
        };
    }
    ```

2. When this `someDirective` is used
        
    ```html
    <some-directive ng2-var="vm.someDirective"></some-directive>
    ```

    - `ng2-var`
        Assign the `SomeController` to the name specified in this attribute.
        For above example, the `vm.someDirective` will be assign to this directive's controller.

*/

export interface INg2VarAttrs extends ng.IAttributes {
    ng2Var: string;
}