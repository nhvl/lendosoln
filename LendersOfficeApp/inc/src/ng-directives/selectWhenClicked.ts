export function selectWhenClicked() {
    return {
        link(scope: ng.IScope, element: JQuery): void {

            element.on("click.selectWhenClicked", function (this:HTMLElement) {
                if ((document as any).selection) {
                    const range = (document.body as any).createTextRange();
                    range.moveToElementText(this);
                    range.select();
                } else if (window.getSelection) {
                    const range = document.createRange();
                    range.selectNode(this);
                    const selection = window.getSelection();
                    if (selection.toString()) return; // Skip event fired when selection
                    selection.addRange(range);
                }
            });

            element.on("$destroy", () => {
                element.off("click.selectWhenClicked");
            });
        }
    };
}
