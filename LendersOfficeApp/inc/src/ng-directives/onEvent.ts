﻿// poly fill Angular 2 (event)="statement"
// <div on-event="event:statement" />

interface IOnEventAttributes extends  ng.IAttributes {
    onEvent : string;
}

const forceAsyncEvents = <{[event:string]:boolean}> {
    'blur': true,
    'focus': true
};

onEvent.$inject = ["$parse", "$rootScope"];
export function onEvent($parse: ng.IParseService, $rootScope:ng.IRootScopeService) {
    return {
        restrict: "A",
        compile($element:JQuery, attr:IOnEventAttributes) {

            var eventName = attr.onEvent.split(":", 1)[0].trim();
            const statement = attr.onEvent.slice(eventName.length + 1).trim();

            var fn = (<any>$parse) (statement, /* interceptorFn */ null, /* expensiveChecks */ true);
            return function ngEventHandler(scope: ng.IScope, element: JQuery) {
                console.debug("directive[onEvent]: Register event[%s] as [%s]", eventName, statement);
                element.on(eventName, (event:JQueryEventObject) => {
                    var callback = () => { fn(scope, { $event: event }); };

                    if (forceAsyncEvents[eventName] && $rootScope.$$phase) {
                        scope.$evalAsync(callback);
                    } else {
                        scope.$apply(callback);
                    }
                });
            };
        }
    };
}
