// https://github.com/tushariscoolster/angular-nicescroll

import {IRootScopeService, IParseService, IScope, IAttributes, IController, IDirective} from "angular";
import {INiceScrollOptions, INiceScrollObject} from "../dt-local/jquery.nicescroll/index.d";

interface INgNicescrollAttributes extends IAttributes {
    niceOption       : string;
    niceScrollObject : string;
    niceScrollEnd    : string;
    niceScrollTopEnd : string;
}

ngNicescroll.ngName = "ngNicescroll";
ngNicescroll.$inject = ['$rootScope','$parse'];
export function ngNicescroll($rootScope: IRootScopeService, $parse: IParseService): IDirective {
    return {
        link(scope:IScope, element:JQuery, attrs:INgNicescrollAttributes, controller:IController) {

            const niceOption:INiceScrollOptions = scope.$eval(attrs.niceOption);

            const niceScroll = element.niceScroll(niceOption);
            var nice = element.getNiceScroll();

            if (attrs.niceScrollObject) $parse(attrs.niceScrollObject).assign(scope, nice);

            // on scroll end
            niceScroll.onscrollend = function(data: {end: {y:number}}) {
                if (this.newscrolly >= this.page.maxh) {
                    if (attrs.niceScrollEnd) scope.$evalAsync(attrs.niceScrollEnd);

                }
                if (data.end.y <= 0) {
                    // at top
                    if (attrs.niceScrollTopEnd) scope.$evalAsync(attrs.niceScrollTopEnd);
                }
            };

            scope.$on('$destroy', () => {
                if (angular.isDefined(niceScroll.version)) {
                    niceScroll.remove();
                }
            });
        },
    }
}
