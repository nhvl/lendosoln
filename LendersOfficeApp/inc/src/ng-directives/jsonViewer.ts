import "./jquery.jsonview";
import "./jquery.jsonview.css";

interface IJsonViewerAttributes extends ng.IAttributes {
    jsonViewer : string;
}

jsonViewer.ngName = "jsonViewer";
export function jsonViewer() {
    return {
        link(scope: ng.IScope, element: JQuery, attrs: IJsonViewerAttributes): void {
            scope.$watch(attrs.jsonViewer, (data: any) => {
                const json = JSON.stringify(data) || "null";
                (<any> element).JSONView(json);
            });
        }
    };
}
