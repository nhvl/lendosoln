﻿// <div re-ref="vm.elediv = $ref" />

import { IAttributes, IDirective, IScope, IParseService, IRootScopeService } from "angular";

interface IReRefAttributes extends IAttributes {
    reRef: string; // <T>(ref: T): void;
}

reRefDirective.ngName = "reRef";
reRefDirective.$inject = ['$parse', '$rootScope'];
export function reRefDirective($parse: IParseService, $rootScope: IRootScopeService): IDirective {
    return {
        restrict: 'A',
        scope: false,
        compile($element: JQuery, attr: IReRefAttributes) {
            // copy from ngEventDirectives
            const fn = $parse(attr.reRef);
            return (scope: IScope, element: JQuery) => {
                const callback = function () {
                    fn(scope, { $ref: element[0] });
                };
                if ($rootScope.$$phase) {
                    scope.$evalAsync(callback);
                } else {
                    scope.$apply(callback);
                }
            };
        }
    };
}
