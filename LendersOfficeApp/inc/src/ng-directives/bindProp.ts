﻿// poly fill Angular 2 [property]="expression"
// <div bind-prop="property:expression" />

interface IBindPropAttributes extends  ng.IAttributes {
    bindProp : string;
}

export function bindProp() {
    return {
        restrict: "AC",
        compile() {
            return (scope: ng.IScope, element: JQuery, attr: IBindPropAttributes) => {
                const ele : {[prop:string]:any} = <any> element[0];

                var prop = attr.bindProp.split(":", 1)[0].trim();
                const expr = attr.bindProp.slice(prop.length + 1).trim();

                console.debug("directive[bindProp]: [%s]='%s'", prop, expr, ele);
                scope.$watch(expr, (value: any) => {
                    console.debug("directive[bindProp] on [%s] = [%s]", expr, value);
                    if (value == undefined) {}
                    ele[prop] = value;
                });
            };
        },
    };
}
