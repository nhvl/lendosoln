import { IDirective, IDirectiveLinkFn } from "angular";
interface IBsPopoverAttrs extends ng.IAttributes {
    content : string;
    showText: string;
    hideText: string;
    wrapper : string;
}

export function bsPopover(): IDirective {
    return {
        restrict: "E",
        link: ((scope, ele, attrs: IBsPopoverAttrs) => {
            const $a = $('<a role="button" tabindex="0">');
            $a.text(attrs.showText);
            ele.append($a);

            const wrapper = attrs.wrapper;
            const html = (!!wrapper);
            $a.popover({ html: true });
            scope.$watch(attrs.content,
                (content: string) => {
                    if (!content) return;
                    if (html) content = $(`<${wrapper}>`).text(content).wrap("<div>").parent().html();
                    $a.attr({ "data-content": content });
                });

            $a.on("show.bs.popover", () => { $a.text(attrs.hideText); });
            $a.on("hide.bs.popover", () => { $a.text(attrs.showText); });
        }) as IDirectiveLinkFn,
    };
}
