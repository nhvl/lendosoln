﻿// poly fill Angular 2 <input #ref />
// Usage: <div ng2-var="ref" />

interface INg2VarAttributes extends  ng.IAttributes {
    ng2Var : string;
}

ng2Var.ngName = "ng2Var";
export function ng2Var() {
    return {
        link(scope: ng.IScope, element: JQuery, attrs: INg2VarAttributes ): void {
            (scope as any)[attrs.ng2Var] = element[0];
        }
    };
}
