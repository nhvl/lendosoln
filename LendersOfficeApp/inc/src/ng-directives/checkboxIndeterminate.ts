﻿interface CbIndAttributes extends  ng.IAttributes {
    checkboxIndeterminate : string;
}

export function checkboxIndeterminate(): ng.IDirective {
    return {
        link(scope: ng.IScope, element: JQuery, attrs: CbIndAttributes): void {

            const cb = element[0] as HTMLInputElement;
            scope.$watch(attrs.checkboxIndeterminate, (value: number) => {
                if (value <= 0) {
                    cb.checked = false;
                    cb.indeterminate = false;

                } else if (1 <= value) {
                    cb.checked = true;
                    cb.indeterminate = false;
                } else {
                    cb.indeterminate = true;
                }
            });
        }
    } as any;
}
