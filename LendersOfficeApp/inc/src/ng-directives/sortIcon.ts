﻿export class OrderSpec {
    by: string;
    desc: boolean;
    constructor(d?: OrderSpec) {
        this.by   = "";
        this.desc = false;

        if (d != null) { Object.assign(this, d); }
    }
}

interface ISortHandleAttrs extends ng.IAttributes {
    key: string;
    sortHandle: string;
    onChange: string;
}

// Usage: <a sort-handle="vm.entrySort" key="Id"></span>
//        where vm.entrySort : OrderSpec
//              key          : string
sortHandle.$inject = ["$timeout"];
export function sortHandle($timeout:ng.ITimeoutService) {
    const ascClass = "glyphicon-sort-by-attributes";
    const descClass = "glyphicon-sort-by-attributes-alt";

    return <ng.IDirective>{
        restrict: "A",
        link(scope: ng.IScope, element: JQuery, attrs: ISortHandleAttrs) {
            const $icon = $('<span class="glyphicon">');
            element.append($icon);

            $icon.addClass("glyphicon").addClass(ascClass);

            scope.$watch(attrs.sortHandle, (spec: OrderSpec) => {
                const by = attrs.key.trim();
                if (spec.by !== by) {
                    $icon.addClass("hidden");
                } else {
                    $icon.removeClass("hidden");
                    if (spec.desc) {
                        $icon.removeClass(ascClass).addClass(descClass);
                    } else {
                        $icon.removeClass(descClass).addClass(ascClass);
                    }
                }
            }, true);

            element.on("click", () => { $timeout(update); });

            function update() {
                const spec = <OrderSpec> scope.$eval(attrs.sortHandle);
                const by = attrs.key.trim();
                if (spec.by === by) spec.desc = !(spec.desc);
                else {
                    spec.by = by;
                    spec.desc = false;
                }

                if (!!attrs.onChange) scope.$eval(attrs.onChange);
            }
        }
    }
}
