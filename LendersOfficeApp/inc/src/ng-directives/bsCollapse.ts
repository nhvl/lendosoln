﻿// <div bs-collapse="vm.isHide" />

import { IAttributes, IDirective, IScope, IDirectiveLinkFn, } from "angular";

interface IBsCollapseAttributes extends IAttributes {
    bsCollapse: string;
}

bsCollapseDirective.ngName = "bsCollapse";
// bsCollapseDirective.$inject = [];
export function bsCollapseDirective(): IDirective {
    return {
        restrict: 'A',
        scope: false,
        link: ((scope: IScope, element: JQuery, attrs: IBsCollapseAttributes) => {
            const cb = element[0] as HTMLInputElement;
            scope.$watch(attrs.bsCollapse, (value: boolean) => {
                if (typeof value == "boolean")
                    element.collapse(value ? 'hide' : 'show');
            });
        }) as IDirectiveLinkFn,
    };
}
