﻿export function formValidate(form: HTMLFormElement): boolean {
    let x : JQuery;
    let title: string = "";

    if ((x = $(form).find(".ng-invalid-required").not(":disabled").first()).length > 0) {
        title = "This field is required.";
    } else if ((x = $(form).find(".ng-invalid-url").not(":disabled").first()).length > 0) {
        title = "Invalid URL.";
    } else if ((x = $(form).find(".ng-invalid-email").not(":disabled").first()).length > 0) {
        title = "Invalid email.";
    } else if ((x = $(form).find(".ng-invalid").not(":disabled").first()).length > 0) {
        title = "This field is invalid.";
    }

    if (x.length > 0) {
        x.tooltip({ title })
            .focus()
            .one("change blur input", function(this:HTMLElement) { $(this).tooltip("destroy"); });
        return false;
    }

    return true;
}
