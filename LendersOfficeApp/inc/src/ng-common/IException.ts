export interface IException {
    ExceptionType: string;
    Message      : string;
    StackTrace   : string;
}