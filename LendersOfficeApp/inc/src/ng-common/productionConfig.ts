// Usage: app.config(productionConfig)
productionConfig.$inject = ["$compileProvider"]
export function productionConfig($compileProvider: ng.ICompileProvider) {
    $compileProvider.debugInfoEnabled(false);
}