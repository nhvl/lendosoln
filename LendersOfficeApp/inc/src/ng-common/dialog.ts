import { IQService, IPromise, IQResolveReject } from "angular";

export interface IDialog {
    alert(message: string, title?: string, okBtn?: string): IPromise<boolean>;
    confirm(message: string, title?: string, okBtn?: string, cancelBtn?: string): IPromise<boolean>;
    ync(message: string, tilte?: string): IPromise<number>;
}

let $q: IQService;

export class Dialog implements IDialog {
    static ngName = "dialog";

    private promiseResolve: IQResolveReject<any> | any;

    private $modal    : JQuery;
    private $header   : JQuery;
    private $title    : JQuery;
    private $footer   : JQuery;
    private $message  : JQuery;

    static $inject = ["$q"];
    constructor(_$q: IQService) {
        $q = _$q;

        this.$modal   = $(Dialog.template);
        this.$header  = this.$modal.find(".modal-header");
        this.$title   = this.$header.find(".modal-title");
        this.$footer  = this.$modal.find(".modal-footer");
        this.$message = this.$modal.find(".dialog-message");

        this.$modal.appendTo("body");
        this.$modal.modal({
            // backdrop: "static",
            show: false,
        });
    }

    show<T>(message: string, title: string = "", buttons: {label:string, value:T, className?:string}[]): IPromise<T> {
        if (this.promiseResolve != null) this.promiseResolve(false);

        this.$header.toggleClass("hidden", !title);
        this.$title.text(title);

        this.$message.text(message);

        this.$footer.off();
        this.$footer.html(
            buttons.map((b,i) =>
                `<span class="clipper">
                    <a class="btn ${b.className || "btn-default"}" data-index="${i}">${b.label}</a>
                </span>`)
            .join(" ")
        );
        this.$footer.on("click", "a.btn", (e: JQueryEventObject) => {
            const i: number = Number((e.target as HTMLElement).dataset["index"]);
            const v = (buttons[i].value);

            this.$modal.one("hidden.bs.modal", () => {
                if (this.promiseResolve != null) {
                    this.promiseResolve(v);
                    this.promiseResolve = null;
                }
            });
            this.$modal.modal("hide");
        });

        this.$modal.modal("show");

        return $q<T>((resolve:IQResolveReject<T>) => { this.promiseResolve = resolve; });
    }

    alert(message: string, title: string = "", okBtn: string = "OK"): IPromise<boolean> {
        return this.show<boolean>(message, title, [
            {label: okBtn, value: true, className:"btn-primary"},
        ]);
    }

    confirm(message: string, title: string = "", okBtn: string = "OK", cancelBtn: string = "Cancel"): IPromise<boolean> {
        return this.show<boolean>(message, title, [
            {label: okBtn    , value: true, className:"btn-primary"},
            {label: cancelBtn, value: false                        },
        ]);
    }

    ync(message: string, title: string = ""): IPromise<number> {
        return this.show<number>(message, title, [
            {label: "Yes"   , value:  1, className:"btn-primary"},
            {label: "No"    , value:  0,                        },
            {label: "Cancel", value: -1,                        },
        ]);
    }

    static template = `
        <div class="modal v-middle fade"><div class="modal-dialog"><div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Title</h4>
            </div>
            <div class="modal-body"><p class="text-center dialog-message">Message</p></div>
            <div class="modal-footer"></div>
        </div></div></div>
    `;
}
