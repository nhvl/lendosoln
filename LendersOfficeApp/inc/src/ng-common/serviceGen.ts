import {
    IPromise,
    IHttpService,
    IQService,
    IHttpPromiseCallbackArg,
} from "angular";
import {IException} from "./IException";

export interface IService {
    <T>(method: string, args ?: {}, config ?: {}): IPromise<T>;
}

export interface IServiceGen {
    (url ?: string): IService;
}

serviceGen.$inject = ["$http", "$q"];
export function serviceGen($http: IHttpService, $q: IQService): IServiceGen {
    const defaultConfig = {
        //transformRequest
        transformResponse(text:string):any {
            try {
                const r = JSON.parse(text,
                (key:string, value:any) => {
                    if (typeof value == "string") {
                        // parse ASP.NET DateTime type
                        {
                            const result = /\/Date\((-?[0-9]+)(?:[a-zA-Z]|(?:\+|-)[0-9]{4})?\)\//g.exec(value);
                            if (!!result) { return new Date(parseFloat(result[1])); }
                        }
                    }
                    return value;
                });
                return (typeof r.d === "undefined") ? r : r.d;
            }
            catch (e) { return text; }
        },
    };

    return function<T>(url: string = window.location.pathname): IService {
        return function<T>(method: string, args?: {}, config?: {}): IPromise<T> {
            args = args || {};

            return new $q<T>((resolve, reject) => {
                $http.post<T>(`${url}/${method}`, args, angular.extend({}, defaultConfig, config))
                    .then(
                        (response: IHttpPromiseCallbackArg<T>) => { resolve(response.data as any); },
                        (response: IHttpPromiseCallbackArg<IException>) => {
                            if (response.status === 403) {
                                const url = response.headers("Location");
                                if (!!url) window.location.href = url;
                            }
                            const contentType = response.headers("Content-Type");
                            if (contentType != null && contentType.includes("text/html")) {
                                reject(response.data);
                                return;
                            }
                            const e: IException = response.data;
                            if (e != null && e.Message != null) {
                                reject(e.Message);
                            } else {
                                reject("Service Error");
                            }
                        }
                    );
            });
        };
    };
}

