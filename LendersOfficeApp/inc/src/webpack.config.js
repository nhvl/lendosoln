"use strict";

const path = require('path');
const {fromPairs} = require('lodash');

const webpack = require('webpack');
const { ModuleConcatenationPlugin, UglifyJsPlugin } = webpack.optimize;
const { DefinePlugin, IgnorePlugin, ProvidePlugin, LoaderOptionsPlugin, ContextReplacementPlugin } = webpack;
// const CompressionPlugin = require("compression-webpack-plugin");
const { CheckerPlugin } = require('awesome-typescript-loader');

const useBabel = false;
const useAwesomeTypescriptLoader = false;

const entryFolders = [
  "LOAdmin/Main"                               ,
  "LOAdmin/Broker/BrokerList"                  ,
  "LOAdmin/Broker/DeletedLoans"                ,
  "LOAdmin/Broker/FindUser"                    ,
  "LOAdmin/Broker/WorkflowOperations"          ,
  "LOAdmin/BrowserUsageStatistics"             ,
  "LOAdmin/CacheTableRequestErrors"            ,
  "LOAdmin/Internal/AdminList"                 ,
  "LOAdmin/Internal/ChangePassword"            ,
  "LOAdmin/Internal/QueueBacklogAutoEscalation",
  "LOAdmin/Internal/StageConfigAdmin"          ,
  "LOAdmin/Manage/GenericFrameworkVendors"     ,
  "LOAdmin/Manage/ManageDocumentVendors"       ,
  "LOAdmin/Manage/SecurityQuestions"           ,
  "LOAdmin/SAE/DownloadList"                   ,
  "LOAdmin/SAE/EmailDownloadList"              ,
  "LOAdmin/SAE/InvestorXlsFileList"            ,
  "LOAdmin/SAE/OutputFiles"                    ,
  "LOAdmin/SAE/PlanCodeEditor"                 ,
  "LOAdmin/SAE/RateSheetVersion"               ,
  "LOAdmin/Test/TestCenter"                    ,
  "LOAdmin/Author.LoanProgramLookUp"           ,
  "test/ModalTest"                              ,
  "Ulad/ApplicationManagement",
  "Ulad/Assets",
  "Ulad/Declarations",
  "Ulad/EditAssociations",
  "Ulad/HomeownerCounseling",
  "Ulad/Liabilities",
  "Ulad/Owners",
  "Ulad/RealProperties",
  "Ulad/IncomeSources",
  "Ulad/Verifications/Vod",
  "Ulad/Verifications/Vol",
  "Ulad/Verifications/Vom",
  
  // "LoanEditor"                                 ,
  // "LoanEditor/Postman"                         ,
  // "Pipeline"                                   ,
  // "TradesAndPools"                             ,
];

module.exports = (env = {}) => {
  const __DEV__ = !(env.production === true || (process.argv.slice(2).indexOf("--watch") < 0));
	
  const plugins = (
    [
      new DefinePlugin({
        __DEV__: JSON.stringify(__DEV__),
        "process.env.NODE_ENV": JSON.stringify(__DEV__ ? "development" : "production"),
      }),
      new IgnorePlugin(/^\.\/locale$/, /moment$/),  // http://stackoverflow.com/a/25426019/1101570
      new LoaderOptionsPlugin({
        options: { context: __dirname },
      }),
      !useAwesomeTypescriptLoader ? null : new CheckerPlugin(), // check at https://github.com/s-panferov/awesome-typescript-loader - `CheckerPlugin` is optional. Use it if you want async error reporting. We need this plugin to detect a `--watch` mode. It may be removed later after https://github.com/webpack/webpack/issues/3460 will be resolved.
    ]
    .concat(__DEV__ ? [] : [
      new ModuleConcatenationPlugin(),
      new UglifyJsPlugin({ sourceMap:false, parallel:true, comments:false }),
      new LoaderOptionsPlugin({ minimize: true }),
      // new CompressionPlugin({
      //   asset: "[path].gz[query]",
      //   algorithm: "gzip",
      //   test: /\.js$|\.html$/,
      //   threshold: 10240,
      //   minRatio: 0.8
      // }),
    ])
    .filter(p => p != null)
  );

  const awesomeTypescriptLoader = ({
    loader: 'awesome-typescript-loader',
    options: {
      transpileOnly: !__DEV__,
      forceIsolatedModules: true,
      useCache: true,
    }
  });
  const tsLoader = ({
    loader: 'ts-loader',
    options: {
      transpileOnly: !__DEV__,
      onlyCompileBundledFiles: true,
    }
  });
  const babelLoader = ({
    loader: 'babel-loader',
    options: {
      presets: [
        "@babel/preset-env",
        "@babel/preset-stage-3",
        "@babel/preset-typescript",
        // { modules: false, },
      ],
      // plugins: [],
    }
  })


  return {
    entry: fromPairs(entryFolders.map(e => [
      e.split("/").join("."),
      `${__dirname}/${e}/app.ts`
    ])),
    output: {
      path           : `${__dirname}/../`,
      filename       : `[name].js`,
      // publicPath     : `/js/`
      pathinfo: __DEV__,
    },
    context: __dirname,
    devtool: __DEV__ ? "source-map" : "", // https://webpack.js.org/configuration/devtool/
    // bail: !__DEV__, // https://webpack.js.org/configuration/other-options/#bail
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          exclude: /(node_modules|bower_components)/,
          use: (
            useBabel                   ? babelLoader : (
            useAwesomeTypescriptLoader ? awesomeTypescriptLoader : (
                                         tsLoader
          ))),
          // include: [
          //   `${__dirname}/src`,
          // ],
        },
        // {
        //   test: /(\.jsx|\.js)$/,
        //   use: 'babel-loader',
        //   exclude: /(node_modules|bower_components)/
        //   options: {
        //     presets: [
        //       // "es2015-webpack", "stage-2",
        //       "latest", { "modules": false },
        //     ],
        //     compact: true,
        //   }
        // },

        { test: /\.css$/, use: ["style-loader", "css-loader"] },
        // { test: /\.scss$/, use: "style-loader!css-loader|sass-loader" },

        // { test: /\.html$/, use: "html-loader" },
        { test: /\.html$/, use: ["layout-loader?l", { loader: "html-loader", options:{removeAttributeQuotes:false}}, "layout-loader?r"] },
        { test: /\.svg$/, use: "html-loader" },
      ],
    },
    resolve: {
      modules: [
        "node_modules",
        path.resolve(__dirname)
        // path.resolve(__dirname, './'),
      ],
      extensions: ['.ts', '.tsx', '.js', '.jsx'],
    },
    target: "web", // https://webpack.js.org/configuration/target/
    plugins,
  };
};
