declare function showModal(url:string, arg:any, 
    feature:string|null, forceEdge:boolean|null, 
    callback:((result:any) => void)|null, 
    settings?:{
        hideCloseButton?:boolean,
        width           ?: number,
        height           ?: number,
    }|null): void;

interface Window {
    resize(width: number, height: number): void;
}

type AssertionType = (testCase: TestCase) => void;

enum SizeEnum{
    Oversize,
    Standard,
}

enum TestEnum{
    AutoResize, 
    PopupConfig,
    ResizeMethod
}

interface TestCase{
    modalSize: SizeEnum;
    testType: TestEnum;
    customAssertion: AssertionType[];
}


interface Size{
    width: number;
    height: number;
}



function getModalSize(sizeEnum: SizeEnum): Size{
    const containerWidth = window.innerWidth;
    const containerHeight = window.innerHeight;

    switch (sizeEnum){
        case SizeEnum.Oversize:
            return {width: containerWidth*2, height: containerHeight*2};
        default:
            return {width: containerWidth/2, height: containerHeight/2};
    }
}

(async function () {
    const containerWidth = window.innerWidth;
    const containerHeight = window.innerHeight;
    const requiredAssertions = [NotSurpassParent];
    const cases: TestCase[] = [
        { modalSize: SizeEnum.Standard, testType: TestEnum.AutoResize, customAssertion: [AvoidScroller]},
        { modalSize: SizeEnum.Oversize, testType: TestEnum.AutoResize, customAssertion: []},
        { modalSize: SizeEnum.Standard, testType: TestEnum.PopupConfig, customAssertion: [SizeMatchConfig]},
        { modalSize: SizeEnum.Oversize, testType: TestEnum.PopupConfig, customAssertion: []},
        { modalSize: SizeEnum.Standard, testType: TestEnum.ResizeMethod, customAssertion: [SizeMatchConfig]},
        { modalSize: SizeEnum.Oversize, testType: TestEnum.ResizeMethod, customAssertion: []},
    ];
    
    for (const testCase of cases) {
        console.info(testCase);
        const modalSize = getModalSize(testCase.modalSize);
        var url =  `/test/MockupModal.aspx?temp=1`;
        var config = {};
        var postShowModal = noop;
        switch (testCase.testType){
            case TestEnum.AutoResize:
                url += `&w=${modalSize.width}&h=${modalSize.height}`;
                break;
            case TestEnum.PopupConfig:
                config = { width:modalSize.width, height:modalSize.height };
                break;
            case TestEnum.ResizeMethod:
                postShowModal = function(){ document.querySelector<HTMLFrameElement>("#LQBPopupDiv iframe")!.contentWindow!.resize(modalSize.width, modalSize.height);}
                break;
            default:
                throw new Error("Unknown TestEnum value " + testCase.testType);
        }

        showModal(url, null, null, null, noop, config);
        await sleep(1000);
        postShowModal();
        var testPassed = true;
        for (const assertion of [ ...testCase.customAssertion, ...requiredAssertions]) {
            if (!assertion(testCase)) {
                testPassed = false;
            }
        }

        var result = "Test " + testPassed ? "passed" : "failed";
        console.info(result);
    }
})();


function AvoidScroller(testCase: TestCase){
    const mBody = document.querySelector<HTMLFrameElement>("#LQBPopupDiv iframe")!.contentWindow!.document.body;
    const hasHScroll = (mBody.scrollHeight > mBody.clientHeight);
    const hasVScroll = (mBody.scrollWidth > mBody.clientWidth);
    var haveNoScroller = !hasVScroll && !hasHScroll;
    if (!haveNoScroller) console.error("Page has scroller.");
    return haveNoScroller;
}

function NotSurpassParent(testCase: TestCase){
    const iframe = document.querySelector<HTMLFrameElement>("#LQBPopupDiv iframe")!;
    const isOverflown = (iframe.width > window.innerWidth || iframe.height > window.innerHeight);
    if (isOverflown) console.error("Size is bigger than parrent.");
    return !isOverflown;
}

function SizeMatchConfig(testCase: TestCase) {
    const modalButtonPadding = 25;
    const modalSize = getModalSize(testCase.modalSize);
    const mWindow = document.querySelector<HTMLFrameElement>("#LQBPopupDiv iframe")!.contentWindow!;
    const isFit = (mWindow.innerHeight == (modalSize.height + modalButtonPadding) && mWindow.innerWidth == modalSize.width);
    if (!isFit) console.error("Size does not match with config.");
    return isFit;
}

function noop(){}
function sleep<T = void>(m:number, v?:T) {
    return new Promise(function(resolve) { setTimeout(resolve, m, v) });
}
function throwError(error:Error) {  throw error; }
