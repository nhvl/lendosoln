﻿import * as moment from "moment";
export { IConsumerPortalUser, ConsumerPortalUser };

interface IConsumerPortalUser {
    Id                         : number ;
    BrokerId                   : string ; // Guid
    FirstName                  : string ;
    LastName                   : string ;
    Email                      : string ;
    Phone                      : string ;
    LastLoginD                 : Date   ;
    PasswordResetCode          : string ;
    PasswordResetCodeRequestD  : Date   ;
    LoginAttempts              : number ;
    IsTemporaryPassword        : boolean;
    IsNotifyOnStatusChanges    : boolean;
    IsNotifyOnNewDocRequests   : boolean;
    SecurityPin                : string ;
}

class ConsumerPortalUser implements IConsumerPortalUser {
    Id                         : number ;
    BrokerId                   : string ; // Guid
    FirstName                  : string ;
    LastName                   : string ;
    Email                      : string ;
    Phone                      : string ;
    LastLoginD                 : Date   ;
    PasswordResetCode          : string ;
    PasswordResetCodeRequestD  : Date   ;
    LoginAttempts              : number ;
    IsTemporaryPassword        : boolean;
    IsNotifyOnStatusChanges    : boolean;
    IsNotifyOnNewDocRequests   : boolean;
    SecurityPin                : string ;

    mLastLoginD                : moment.Moment;
    mPasswordResetCodeRequestD : moment.Moment;

    constructor(d?: IConsumerPortalUser){
        if (d == null) {
            d = {
                Id                         : null,
                BrokerId                   : null,
                FirstName                  : "",
                LastName                   : "",
                Email                      : "",
                Phone                      : "",
                LastLoginD                 : null,
                PasswordResetCode          : "",
                PasswordResetCodeRequestD  : null,
                LoginAttempts              : 0,
                IsTemporaryPassword        : false,
                IsNotifyOnStatusChanges    : false,
                IsNotifyOnNewDocRequests   : false,
                SecurityPin                : "",
            };
        }
        $.extend(this, d);

        this.mLastLoginD                = moment(this.LastLoginD);
        this.mPasswordResetCodeRequestD = moment(this.mPasswordResetCodeRequestD);
    }
}
