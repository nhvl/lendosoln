export enum E_sGseSpT {
	LeaveBlank = 0,
	Attached = 1,
	Condominium = 2,
	Cooperative = 3,
	Detached = 4,
	DetachedCondominium = 5,
	HighRiseCondominium = 6,
	ManufacturedHomeCondominium = 7,
	ManufacturedHomeMultiwide = 8,
	ManufacturedHousing = 9,
	ManufacturedHousingSingleWide = 10,
	Modular = 11,
	PUD = 12
}