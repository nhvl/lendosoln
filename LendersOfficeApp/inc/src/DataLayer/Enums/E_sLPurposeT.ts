export enum E_sLPurposeT
{
    Purchase                = 0,
    Refin                   = 1,
    RefinCashout            = 2,
    Construct               = 3,
    ConstructPerm           = 4,
    Other                   = 5,
    FhaStreamlinedRefinance = 6,
    VaIrrrl                 = 7,
    HomeEquity              = 8, // 9/9/2013 dd - OPM 136658 Add Home Equity Option.
}
