export enum E_aAddrMailSourceT {
    PresentAddress         = 0,
    SubjectPropertyAddress = 1,
    Other                  = 2,
}
