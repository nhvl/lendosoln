export enum E_CustomPmlFieldType {
    Blank = 0,
    Dropdown = 1,
    NumericGeneric = 2,
    NumericMoney = 3,
    NumericPercent = 4
}