export enum E_AgentSourceT {
    NotYetDefined   = 0,
    ManuallyEntered = 1,
    CorporateList   = 2
}
