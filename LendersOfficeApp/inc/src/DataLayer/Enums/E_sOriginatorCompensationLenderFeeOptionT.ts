export enum E_sOriginatorCompensationLenderFeeOptionT {
    IncludedInLenderFees = 0,
    InAdditionToLenderFees = 1,
}
