export enum E_CreditLenderPaidItemT {
    None = 0,
    AllLenderPaidItems = 1,
    OriginatorCompensationOnly = 2
}
