export enum E_PercentBaseT {
    LoanAmount                = 0,
    SalesPrice                = 1,
    AppraisalValue            = 2,
    OriginalCost              = 3,
    TotalLoanAmount           = 4,
    AverageOutstandingBalance = 5,
    AllYSP                    = 6,
    DecliningRenewalsMonthly  = 7,
    DecliningRenewalsAnnually = 8,
}
