export enum E_aBAddrT {
    Own            = 0,
    Rent           = 1,
    LeaveBlank     = 2,
    LivingRentFree = 3,
}
