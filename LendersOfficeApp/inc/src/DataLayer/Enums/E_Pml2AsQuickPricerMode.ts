export enum E_Pml2AsQuickPricerMode {
    Disabled = 0, // no user can enable it.
    PerUser = 1,  // up to admins to set the bit per user.
    Enabled = 2   // all users at this lender have it enabled.
}
