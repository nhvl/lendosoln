export enum E_RateLockExpirationWeekendHolidayBehavior {
    PrecedingBusinessDay = 0,
    AllowWeekendHoliday = 1,
    FollowingBusinessDay = 2
}
