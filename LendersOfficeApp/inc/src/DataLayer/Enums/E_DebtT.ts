export enum E_DebtT {
    Alimony = 0,
    Installment = 1,
    JobRelatedExpense = 2,
    Mortgage = 3,
    Open = 4,
    Revolving = 5,
    Other = 6
}