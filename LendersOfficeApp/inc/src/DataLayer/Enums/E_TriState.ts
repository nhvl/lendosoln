export enum E_TriState {
    Blank = 0,
    Yes   = 1,
    No    = 2,
}
