// ~/LendersOfficeLib/DataAccess/RateLockHistoryItem.cs
export enum E_RateLockAuditActionT {
    BreakRateLock       ,
    LockRate            ,
    ExtendRateLock      ,
    ClearRateLockRequest,
    LockRequested       ,
    FloatDownRate       ,
    ReLockRate          ,
    UNDEFINED           ,
    SuspendLock         ,
    ResumeLock          ,
}

// ~/LendersOfficeLib/DataAccess/RateLockHistoryItem.cs/GetRateLockHistoryItemFrom
const mn = new Map<string, { Action: string, sRateLockAuditActionT: E_RateLockAuditActionT}>([
    ["BreakLockEvent"          , { Action: "Break Rate Lock"            , sRateLockAuditActionT: E_RateLockAuditActionT.BreakRateLock       }],
    ["LockEvent"               , { Action: "Rate Locked"                , sRateLockAuditActionT: E_RateLockAuditActionT.LockRate            }],
    ["ExtendLockEvent"         , { Action: "Extend Lock Expiration Date", sRateLockAuditActionT: E_RateLockAuditActionT.ExtendRateLock      }],
    ["RemoveRequestedRateEvent", { Action: "Clear Rate Lock Request"    , sRateLockAuditActionT: E_RateLockAuditActionT.ClearRateLockRequest}],
    ["LockRequestedEvent"      , { Action: "Lock Requested"             , sRateLockAuditActionT: E_RateLockAuditActionT.LockRequested       }],
    ["FloatDownEvent"          , { Action: "Rate Float Down"            , sRateLockAuditActionT: E_RateLockAuditActionT.FloatDownRate       }],
    ["ReLockRateEvent"         , { Action: "Re-Lock Rate"               , sRateLockAuditActionT: E_RateLockAuditActionT.ReLockRate          }],
    ["SuspendLockEvent"        , { Action: "Lock Suspended"             , sRateLockAuditActionT: E_RateLockAuditActionT.SuspendLock         }],
    ["ResumeLockEvent"         , { Action: "Lock Modified"              , sRateLockAuditActionT: E_RateLockAuditActionT.ResumeLock          }],

    ["BreakInvestorLockEvent"  , {Action: "Break Rate Lock"             , sRateLockAuditActionT: E_RateLockAuditActionT.BreakRateLock       }],
    ["InvestorLockEvent"       , {Action: "Rate Locked"                 , sRateLockAuditActionT: E_RateLockAuditActionT.LockRate            }],
    ["ExtendInvestorLockEvent" , {Action: "Extend Lock Expiration Date" , sRateLockAuditActionT: E_RateLockAuditActionT.ExtendRateLock      }],
    ["InvestorResumeLockEvent" , {Action: "Lock Modified"               , sRateLockAuditActionT: E_RateLockAuditActionT.ResumeLock          }],
    ["SuspendInvestorLockEvent", {Action: "Lock Suspended"              , sRateLockAuditActionT: E_RateLockAuditActionT.SuspendLock         }],
    ["default"                 , {Action: "????"                        , sRateLockAuditActionT: E_RateLockAuditActionT.UNDEFINED           }],
] as [string, { Action: string, sRateLockAuditActionT: E_RateLockAuditActionT}][]);

export function eventType2Action(event: string) {
    return (mn.has(event) ? mn.get(event) : mn.get("default"));
}
