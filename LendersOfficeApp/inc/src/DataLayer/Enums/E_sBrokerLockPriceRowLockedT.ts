export enum E_sBrokerLockPriceRowLockedT {
    Base            = 0,
    BrokerBase      = 1, // aka. Origintor Base
    BrokerFinal     = 2, // aka.  Final Price
    OriginatorPrice = 3
}