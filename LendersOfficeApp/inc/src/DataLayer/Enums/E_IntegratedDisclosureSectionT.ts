export enum E_IntegratedDisclosureSectionT
{
    LeaveBlank = 0,
    SectionA = 1,
    SectionB = 2,
    SectionC = 3,
    SectionD = 4,
    SectionE = 5,
    SectionF = 6,
    SectionG = 7,
    SectionH = 8,
    SectionBorC = 9
}
