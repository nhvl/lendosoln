export enum E_AssetT 
    {
        Auto = 0,
        Bonds = 1,
        Business = 2,
        Checking = 3,
        GiftFunds = 4,
        
        LifeInsurance = 5,
        
        Retirement = 6,
        Savings = 7,
        Stocks = 8,
        OtherIlliquidAsset = 9,
        
        CashDeposit = 10,
        OtherLiquidAsset = 11,
        PendingNetSaleProceedsFromRealEstateAssets = 12, // 5/12/2008 dd - Add to support Loan Prospector integration.
        GiftEquity = 13, // 8/8/2008 dd - OPM 23660 - Add "Gift of Equity" asset type
        // 11/1/10 vm - OPM 29578 - Added assets 14 to 19 to match with DU
        CertificateOfDeposit = 14,
        MoneyMarketFund = 15,
        MutualFunds = 16,
        SecuredBorrowedFundsNotDeposit = 17,
        BridgeLoanNotDeposited = 18,
        TrustFunds = 19
        
        //BE SURE TO SEARCH AND ADD ANY NEW TYPES TO ALL SWITCH STATEMENTS - av 2 2011 
        //THERES CODE THAT THROWS EXCEPTION IF IT ENCOUNTERS A NEW TYPE
    }