export enum E_sClosingCostSetType {
    Fees                  = 0,
    LenderFeeSetup        = 1,
    SellerResponsibleFees = 2,
    BorrowerAndSellerFees = 3,
}
