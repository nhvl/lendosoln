export enum E_ShowCredcoOptionT {
    DisplayBoth = 0,
    DisplayOnlyDirect = 1,
    DisplayOnlyFNMA = 2
}
