export enum E_sClosingCostAutomationUpdateT {
    UpdateOnConditionChange = 0,
    PreserveFeesOnLoan = 1,
    UpdateUnconditionally = 2
}
