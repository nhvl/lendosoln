export enum E_GfeProviderChoiceT {
    LeaveBlank    = 0,
    Borrower      = 1,
    Lender        = 2,
    NotApplicable = 3
}
