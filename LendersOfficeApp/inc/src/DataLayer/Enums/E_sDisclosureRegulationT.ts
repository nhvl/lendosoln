﻿export enum E_sDisclosureRegulationT {
    Blank    = 0,
    GFE2010  = 1,
    TRID2015 = 2,
}
