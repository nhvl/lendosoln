export enum E_aAddrPostSourceT {
    PresentAddress         = 0,
    MailingAddress         = 1,
    SubjectPropertyAddress = 2,
    Other                  = 3,
}
