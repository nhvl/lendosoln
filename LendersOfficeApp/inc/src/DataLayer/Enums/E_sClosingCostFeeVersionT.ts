export enum E_sClosingCostFeeVersionT {
    Legacy = 0,
    LegacyButMigrated = 1,
    ClosingCostFee2015 = 2
}
