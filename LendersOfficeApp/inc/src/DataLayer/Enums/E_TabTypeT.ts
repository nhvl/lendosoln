export enum E_TabTypeT
{
    PipelinePortlet = 0,
    DisclosurePortlet = 1,
    LoanPoolPortlet = 2,
    TradeMasterPortlet = 3,
    MyTasksPortlet = 4,
    RemindersPortlet = 5,
    DisctrackPortlet = 6,
    MyLoanRemindersPortlet = 7,
    //Not necessary once we let clients adjust to new tab system
    UnassignedLoanPortlet = 8,
    MyLeadPortlet = 9,
    UnassignLeadPortlet = 10,
    TestPortlet = 11
}
