export enum E_MIVendorBrokerSettingStatusT {
    Disabled   = 0,
    Test       = 1,
    Production = 2
}