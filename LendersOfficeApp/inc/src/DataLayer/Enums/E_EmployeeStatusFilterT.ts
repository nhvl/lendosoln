export enum E_EmployeeStatusFilterT {
    All = 0,
    ActiveOnly = 1,
    InactiveOnly = 2
}
