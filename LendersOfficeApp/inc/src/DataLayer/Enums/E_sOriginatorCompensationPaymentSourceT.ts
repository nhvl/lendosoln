export enum E_sOriginatorCompensationPaymentSourceT {
    SourceNotSpecified = 0,
    BorrowerPaid       = 1,
    LenderPaid         = 2,
}
