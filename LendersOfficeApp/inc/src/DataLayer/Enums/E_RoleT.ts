export enum E_RoleT {
    Accountant = 0,
    Administrator = 1,
    CallCenterAgent = 2,
    Closer = 3,
    Consumer = 4,
    Funder = 5,
    LenderAccountExecutive = 6,
    LoanOfficer = 7,
    LoanOpener = 8,
    LockDesk = 9,
    Manager = 10,
    Processor = 11,
    RealEstateAgent = 12,
    Shipper = 13,
    Underwriter = 14,
    PostCloser = 15,
    Insuring = 16,
    CollateralAgent = 17,
    DocDrawer = 18,
    CreditAuditor = 19,
    DisclosureDesk = 20,
    JuniorProcessor = 21,
    JuniorUnderwriter = 22,
    LegalAuditor = 23,
    LoanOfficerAssistant = 24,
    Purchaser = 25,
    QCCompliance = 26,
    Secondary = 27,
    Servicing = 28,

    Pml_LoanOfficer = 50, // This role is same as loan officer. Except it is only available for "P" user.
    Pml_BrokerProcessor = 51,
    Pml_Administrator = 52, // This role is same as admin. Except it is only available for "P" user.
    Pml_Secondary = 70,
    Pml_PostCloser = 71
}

const role_map_string = new Map<E_RoleT, string>([
    [E_RoleT.Accountant            , "Accountant"],
    [E_RoleT.Administrator         , "Administrator"],
    [E_RoleT.CallCenterAgent       , "Telemarketer"],
    [E_RoleT.Closer                , "Closer"],
    [E_RoleT.Consumer              , "Consumer"],// Consumer Portal project
    [E_RoleT.Funder                , "Funder"],
    [E_RoleT.LenderAccountExecutive, "LenderAccountExec"],
    [E_RoleT.LoanOfficer           , "Agent"],
    [E_RoleT.LoanOpener            , "LoanOpener"],
    [E_RoleT.LockDesk              , "LockDesk"],
    [E_RoleT.Manager               , "Manager"],
    [E_RoleT.Processor             , "Processor"],
    [E_RoleT.RealEstateAgent       , "RealEstateAgent"],
    [E_RoleT.Shipper               , "Shipper"],// EDocs project
    [E_RoleT.Underwriter           , "Underwriter"],
    [E_RoleT.PostCloser            , "PostCloser"], // OPM 108148 gf start
    [E_RoleT.Insuring              , "Insuring"],
    [E_RoleT.CollateralAgent       , "CollateralAgent"],
    [E_RoleT.DocDrawer             , "DocDrawer"], // OPM 108148 gf end
    [E_RoleT.CreditAuditor         , "CreditAuditor"], // 11/19/2013 gf - OPM 145015 start
    [E_RoleT.DisclosureDesk        , "DisclosureDesk"],
    [E_RoleT.JuniorProcessor       , "JuniorProcessor"],
    [E_RoleT.JuniorUnderwriter     , "JuniorUnderwriter"],
    [E_RoleT.LegalAuditor          , "LegalAuditor"],
    [E_RoleT.LoanOfficerAssistant  , "LoanOfficerAssistant"],
    [E_RoleT.Purchaser             , "Purchaser"],
    [E_RoleT.QCCompliance          , "QCCompliance"],
    [E_RoleT.Secondary             , "Secondary"],
    [E_RoleT.Servicing             , "Servicing"], // 11/19/2013 gf - OPM 145015 end
    [E_RoleT.Pml_LoanOfficer       , "Agent"], // This role is same as loan officer. Except it is only available for "P" user.
    [E_RoleT.Pml_BrokerProcessor   , "BrokerProcessor"], // Workflow project
    [E_RoleT.Pml_Administrator     , ""], // This role is same as admin. Except it is only available for "P" user.
    [E_RoleT.Pml_Secondary         , "ExternalSecondary"],
    [E_RoleT.Pml_PostCloser        , "ExternalPostCloser"],
]);

export function GetRole(r: E_RoleT): string {
    return role_map_string.has(r) ? role_map_string.get(r) : "";
}
