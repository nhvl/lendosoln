export enum E_GfeResponsiblePartyT {
    LeaveBlank = 0,
    Buyer      = 1,
    Seller     = 2,
    Lender     = 3,
}
