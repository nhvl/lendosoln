export enum E_ReoStatusT {
    Residence = 0,
    Sale = 1,
    PendingSale = 2,
    Rental = 3
}