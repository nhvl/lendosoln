export enum E_CustomPmlFieldVisibilityT {
    AllLoans = 0,
    PurchaseLoans = 1,
    RefinanceLoans = 2,
    HomeEquityLoans = 3
}