export enum E_BranchChannelT {
    Blank         = 0,
    Retail        = 1,
    Wholesale     = 2,
    Correspondent = 3,
    Broker        = 4,
}
