export enum E_DeliveryMethodT
{
    LeaveEmpty = 0,
    Email      = 1,
    Fax        = 2,
    InPerson   = 3,
    Mail       = 4,
    Overnight  = 5,
}
