export enum BrokerSuiteType {
    Blank = 0,
	ClientLqb = 1,
	ClientPmlOnly = 2,
	ClientIls = 3,
	InternalDemo = 4,
	InternalTest = 5,
	LoadTest = 6,
	VendorTest = 7
}