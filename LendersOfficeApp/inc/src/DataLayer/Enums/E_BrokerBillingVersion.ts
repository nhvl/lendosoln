export enum E_BrokerBillingVersion {
    NoBilling = 0,
    Classic = 1,
    New = 2,
    PerTransaction = 3
}
