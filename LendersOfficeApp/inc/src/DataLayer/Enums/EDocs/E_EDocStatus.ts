export enum E_EDocStatus {
    Blank = 0,
    Obsolete,
    Approved,
    Rejected,
    Screened
}
