export enum E_EDocumentSource {
    FromIntegration,
    UserUpload,
    AcceptedFromConsumer,
    Fax,
    SplitDoc,
    GeneratedDocs,
    CBCDrive,
    FloodService,
    ESignPOST,
    UserCopy, // If using UserCopy, should also set m_copiedFromFileName
    NotApplicable, // this is default
    DeletedPages, // m_copiedFromFileName also required
}
