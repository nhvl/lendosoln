export enum E_LiaOwnerT 
    {
        Borrower = 0,
        CoBorrower = 1,
        Joint = 2
    }
