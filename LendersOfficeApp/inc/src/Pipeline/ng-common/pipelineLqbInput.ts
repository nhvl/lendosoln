import {lqbInputGen       } from "../../LoanEditor/ng-common/lqbInput/lqbInputGen";
import {PipelineModels    } from "./PipelineModels";

export const lqbInput = lqbInputGen(getPipelineFromScope);
lqbInput.ngName = "lqbInput";
lqbInput.$inject= ["$timeout"];

function getPipelineFromScope(scope: ng.IScope): PipelineModels {
    let pipeline: PipelineModels;
    let leScope = scope;
    while (leScope != null && (pipeline = leScope.$eval("vm.pipeline")) == null)
        leScope = leScope.$parent;
    if (pipeline == null) throw "Please define `vm.pipeline`";
    return pipeline;
}
