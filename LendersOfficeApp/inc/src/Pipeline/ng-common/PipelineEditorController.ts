type StrGuid = string;

import {PipelineModels   } from "./PipelineModels";
import {LoadingIndicator } from "../../LoanEditor/ng-common/loadingIndicator";
import {IDialog          } from "../../ng-common/dialog";
import {ServiceFactory   } from "./../../LoanEditor/ng-common/service";

import {IApplicant       } from "../../LoanEditor/ng-common/IApplicant";
import {ILoanSummary     } from "../../LoanEditor/ng-common/ILoanSummary";

let $timeout         : ng.ITimeoutService;
let loadingIndicator : LoadingIndicator;
let $q               : ng.IQService;

export class PipelineEditorController { static ngName = "PipelineEditorController";
    pipeline             : PipelineModels;
    disabledSave         : boolean;
    public windows       : Window[];

    private ref               : (_: {$ref  : PipelineEditorController}) => void;
    protected onSummaryChanged: (x:{ $summary: ILoanSummary, $applicants: IApplicant[] }) => void;
    protected onStateChanged  : (x: { $pageStateId: string }) => void;
    public isSaveAvailable : boolean = true;

    protected fieldList(): string[] { throw Error("Not implemented"); };

    static $inject = ["$element", "$q", "$timeout", "dialog", ServiceFactory.ngName, "$scope", LoadingIndicator.ngName, PipelineModels.ngName];
    constructor(protected $element: ng.IAugmentedJQuery,
                _$q: ng.IQService,
                _$timeout: ng.ITimeoutService,
                protected dialog: IDialog,
                protected service: ServiceFactory,
                private   $scope: ng.IScope,
                _loadingIndicator: LoadingIndicator) {
        $q               = _$q              ;
        $timeout         = _$timeout        ;
        loadingIndicator = _loadingIndicator;

        this.init();
    }

    protected init(){}

    private $onInit(){
        if (!!this.ref) this.ref({ $ref: this }); else debugger;
    }

    protected $postLink() {
        if (this.fieldList != null) {
            loadingIndicator.show();
            PipelineModels.get((this.fieldList()))
                .then((pipeline) => { this.onLoaded(pipeline) },
                      () => false)
                .finally(() => { loadingIndicator.hide(); });

            this.disabledSave = true;
            this.$element.one("change", ":input", () => { this.disabledSave = false; });
        } else {
            console.error("fieldList is not defined");
            debugger;
        }
    }

    protected apply(){
        this.$scope.$apply();
    }

    protected OpenWindow(url: string, windows: Window[]) : void{
        // We are emitting this event so that it is caught by pipelineApp.ts.  This is because we only want windows to be closed when the Pipeline page is closed,
        // but switching between pages in the pipeline shouldn't.  Also, since the pipelineApp has to close the pages, it must also be the one to open them, as
        // scripts can only close pages if they are the ones that opened them.
        this.$scope.$emit('OpenWindow', {url: url});
    }

    protected onLoaded(pipeline:PipelineModels) : boolean {
        this.pipeline = pipeline;

        return true;
    }

    save(): ng.IPromise<void> {
        const {dialog} = this;

        if (this.pipeline.isDirty()) {
            const message = this.pipeline.validateToSave();
            if (message != null) return dialog.alert(message, "Invalid").then(() => $q.reject(message));
        }
        loadingIndicator.show("Saving...");

        return $q<void>((resolve) => {
            if (document.activeElement) {
                $(document.activeElement).trigger("change");
                $timeout(() => {
                    this.pipeline.save().then(() => {
                        loadingIndicator.hide();
                        resolve();
                    });
                });
            } else {
                this.pipeline.save().then(() => {
                    loadingIndicator.hide();
                    resolve();
                });
            }
        });
    }

    $onDestroy(){
        this.$element.find("*").off();
    }
}
