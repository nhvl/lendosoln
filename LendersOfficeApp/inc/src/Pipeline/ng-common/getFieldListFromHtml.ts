export function getFieldListFromHtml(html: string):string[] {

    const $pipelineEditorTemplate = $(`<div>${html}</div>`);
    const fieldList:string[] = Array.from(new Set<string>([].concat(
        $pipelineEditorTemplate.find("lqb-input[lqb-model]").map((i, m) => lqbModelBindExpression_2_fieldId(m)).toArray(),
        $pipelineEditorTemplate.find("lqb-lockable-input[lqb-model]").map((i, m) => lqbModelBindExpression_2_fieldId(m)).toArray(),
        $pipelineEditorTemplate.find("lqb-lockable-input[lockable-model]").map((i, m) => lqbModelBindExpression_2_fieldId(m, "lockable-model")).toArray(),
        $pipelineEditorTemplate.find("[ng-repeat]").map((i, m) => ngRepeateExpression_2_fieldId(m)).toArray(),
        ...$pipelineEditorTemplate.find("div[lqb-load-fields]").toArray().map((m) => $(m).attr("lqb-load-fields").split(" ").map((s:string) => s.trim()))
        ).filter(f => !!f)
    ));

    return (
        fieldList
    );
}

function lqbModelBindExpression_2_fieldId(m: Element, attrName: string = "lqb-model") {
    const expr = $(m).attr(attrName).trim();
    let match =  expr.match(/vm\.pipeline\.(\S+)/)
    if (expr.match(/vm\.pipeline\.fairLendNoticeAddress.*/)) return "fairLendNoticeAddress";
    return match == null ? null : match[1];
}

function ngRepeateExpression_2_fieldId(m: Element) {
    const expr = $(m).attr("ng-repeat").trim();

    if (expr.match(/vm\.pipeline\.ComLicenses.*/)) return "ComLicenses";

    if (expr.match(/vm\.pipeline\.IpWhiteList.*/)) return "IpWhiteList";

    let match = expr.match(/vm\.pipeline\.app\.aOtherIncomeList/); // vm.pipeline.app.aOtherIncomeList
    if (match != null) return "aOtherIncomeList";

    if (expr.match(/vm\.pipeline\.creditAgents.*/)) return "creditAgents";

    match = expr.match(/vm\.pipeline\.sSellerCollection/); // vm.pipeline.sSellerCollection
    if (match != null) return "sSellerCollection";

    match = expr.match(/vm\.pipeline\.sTitleBorrowers/); // vm.pipeline.sTitleBorrowers
    if (match != null) return "sTitleBorrowers";

    return null;
}
