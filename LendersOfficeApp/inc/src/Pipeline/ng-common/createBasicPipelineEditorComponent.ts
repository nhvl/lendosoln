import {IComponentOptions} from "angular";

import {PipelineEditorController} from "./PipelineEditorController";
import {getFieldListFromHtml} from "./getFieldListFromHtml";


export function createBasicPipelineEditorComponent(name:string, appHtml: string, Controller = PipelineEditorController): IComponentOptions {
    const fieldList = getFieldListFromHtml(appHtml);
    class ExtController extends Controller {
        protected fieldList() {
            return fieldList;
        }
    }

    return {
        ngName                : name,
        bindings              : {
            onSummaryChanged  : "&",
            onStateChanged    : "&",
            ref               : "&",
        },
        controllerAs          : "vm",
        template              : appHtml,
        controller            : ExtController as any,
    };
}
