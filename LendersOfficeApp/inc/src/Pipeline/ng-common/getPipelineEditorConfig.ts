
import {getQueryStringValue} from "../../utils/getQueryStringValue";

const $ = jQuery;

const qsk_aAppId = "appId";

export function getApplicantId(){ return getQueryStringValue(qsk_aAppId) || ""; }

