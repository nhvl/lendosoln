import "core-js/modules/es7.array.includes";
import isEqual = require("lodash/isEqual");
import omit = require("lodash/omit");
import * as events from "events";

import {ILqbDataModel, ILqbDataModelG,
    LqbInputType, ILqbDataModelRaw, updateFromRaw                } from "../../LoanEditor/ng-common/ILqbDataModel";
import {ServiceFactory                                           } from  "./../../LoanEditor/ng-common/service";
import {toastrOptions                                            } from  "../../LoanEditor/ng-common/toastrOptions";
import {recursiveTraversal, recursiveUpdate                      } from "./../../utils/recursiveUpdate";
import {isLqbDataModel, isValidLqbInputModel                     } from  "./../../LoanEditor/ng-common/ILqbDataModel";

import {StrGuid} from "../../utils/guid";

export const WarnSaveMessage = "Your change is not save. Do you really want to exit?";

export interface IDataModelsRaw {

    fields: {
        [key: string]: ILqbDataModelRaw,
    },

    IpWhiteList?: any[];
    [key: string]: any,
}

let service: ServiceFactory;
let $q: ng.IQService;

export class PipelineModels { static ngName = "PipelineModels";

    fields: ILoanFields;
	broker: ILoanFields;
	brokerAddress: ILoanFields;
	PipelineView: IPipelineView;
	TableInfo: any;

    flnAddress: string[];
    pipelineId: string;
    PasswordOption: any;
    creditAgents: {Key: string, Value: boolean}[];
    HiddenCras: {Key: string, Value: any}[];
    CraList: ILqbDataModel;
    IpWhiteList: any;
    IpWhiteListTemplate: any;
    skipDirtyCheck1Time: boolean;

    namingOptions: any;
    ComLicenses: ILoanFields[];
    CompanyLicenseTemplate: ILoanFields;

    private eventEmitter: events.EventEmitter;

    constructor(dm?: IDataModelsRaw, pipelineId?: string ) {
        this.fields = {};

        this.pipelineId = pipelineId;

        this.skipDirtyCheck1Time = false;

        if (dm != null) this.update(dm);

        this.isCalculating = false;
        this.deferChangedFieldsForCalculate = [];

        this._cacheValue = this.getCacheValue();

        this.eventEmitter = new events.EventEmitter();

        window.onbeforeunload = () => {
            if (this.skipDirtyCheck1Time) {
                this.skipDirtyCheck1Time = false;
                return;
            }

            $(document.activeElement).trigger("change");
            if (this.isDirty()) return WarnSaveMessage;
        }
    }

    validateToSave(): string {
        let message: string = null;
        recursiveTraversal(this, (x: any, paths: (string|number)[]) => {
            if (!isLqbDataModel(x)) return { handled: false, terminate: false };

            const path = paths.join(".");
            let terminate = !isValidLqbInputModel(x, path);
            if (terminate) {
                console.log(path + " required but empty");
                message = "Please fill in all required fields before saving data.";
            }
            return {handled: true, terminate};
        });
        return message;
    }

    private update(dm: IDataModelsRaw) {
        if (dm == null) { console.error("dm is null"); return; }
        updateFromRaw(dm, this);
    }

    public isCalculating: boolean;
    private deferChangedFieldsForCalculate: ILqbDataModel[];
    private calcPromise: ng.IPromise<PipelineModels>;

    public on(event: "preCalc" | "postCalc", f:Function) {
        this.eventEmitter.on(event, f);
    }

    calculate(dm?: ILqbDataModel): ng.IPromise<PipelineModels> {
        if (this.isCalculating) { this.deferChangedFieldsForCalculate.push(dm); return; }

        // fire event onPreCalcEvents
        this.eventEmitter.emit("preCalc", dm);

        this.deferChangedFieldsForCalculate = [];
        this.isCalculating = true;

        toastr.info(`onCalculate: ${(!dm) ? "all" : JSON.stringify(dm.v)}`, null, toastrOptions);
        const p: ng.IPromise<PipelineModels> = this.calcPromise =
            service.pipeline.calculate(this).then((dm) => {
                this.update(dm);
                this.isCalculating = false;
                if (this.deferChangedFieldsForCalculate.length > 0) {
                    return this.calculate(this.deferChangedFieldsForCalculate[0]);
                }
                this.calcPromise = null;
                return this;
            });
        p.catch(() => { this.isCalculating = false });
        return p;
    }

    calculateZip(dm?: ILqbDataModel): ng.IPromise<PipelineModels> {
        if (this.isCalculating) { this.deferChangedFieldsForCalculate.push(dm); return; }

        this.deferChangedFieldsForCalculate = [];
        this.isCalculating = true;

        toastr.info(`onCalculate: ${(!dm) ? "all" : JSON.stringify(dm.v)}`, null, toastrOptions);
        const p: ng.IPromise<PipelineModels> = service.pipeline.calculateZipcode(this)
            .then((dm) => {
                this.update(dm);
                if (this.deferChangedFieldsForCalculate.length > 0) {
                    return this.calculate(this.deferChangedFieldsForCalculate[0]);
                }
                this.isCalculating = false;
                return this;
            });
        p.catch(() => { this.isCalculating = false });
        return p;
    }

    save() {
        const calcPromise = (this.isCalculating && this.calcPromise) ? this.calcPromise : $q.when(null);
        return calcPromise.then(() => {
            this.deferChangedFieldsForCalculate = [];

            const p: ng.IPromise<PipelineModels> = service.pipeline.save(this)
                .then((dm) => {
                    this.update(dm);
                    this._cacheValue = this.getCacheValue();
                    return this;
                });
            return p;
        });
    }

    private _cacheValue: any;
    private getCacheValue(){
        return recursiveUpdate(
            omit(this, ["isCalculating", "deferChangedFieldsForCalculate", "_cacheValue", "eventEmitter"]),
            updateFunc);

        function updateFunc(thing: any): [boolean, any] {
            if (isLqbDateModel(thing)) return [true, thing.v];

            if (thing != null && typeof thing === "object") {
                const angularTempKeys = Object.keys(thing).filter(key => key.startsWith("$$"));
                if (angularTempKeys.length > 0) {
                    return [true, recursiveUpdate(omit(thing, angularTempKeys), updateFunc)];
                }
            }
            return [false, undefined];
        }

        function isLqbDateModel(modelObject: any): modelObject is ILqbDataModel {
            return (modelObject != null && (typeof modelObject === "object") && modelObject.hasOwnProperty("v"));
        }
    }

    isDirty(){
        return !isEqual(this._cacheValue, this.getCacheValue());
    }


    static get(fieldList: string[]):ng.IPromise<PipelineModels> {
        return service.pipeline.load(fieldList)
            .then((dm: IDataModelsRaw) => new PipelineModels(dm));
    }


    static ngFactory(_service: ServiceFactory, _$q:ng.IQService) {
        service = _service;
        $q = _$q;

        return PipelineModels;
    }
}
PipelineModels.ngFactory.$inject = [ServiceFactory.ngName, "$q"];

export interface ILoanFields {
    [key: string]: ILqbDataModel;
}

export interface IPipelineView {
	Index: string;
	Active: any;
	Tabs: any;
}
