/// <reference path="../dt-local/tsd.d.ts" />

import "core-js/es6/object";
import "core-js/es6/number";
import "core-js/es6/string";
import "core-js/es6/array";
import "core-js/es7/array";
import "core-js/es6/promise";
import "core-js/es6/map";
import "core-js/es6/set";

import {Dialog} from "../ng-common/dialog";

import {ngServiceRunner} from "../LoanEditor/ng-common/ngServices";
import {lqModal                             } from "../LoanEditor/components/lqModal";
import {hiddenCrasModal                     } from "../LoanEditor/components/HiddenCrasModal";
import {branchPrefixModal                     } from "../LoanEditor/components/BranchPrefixModal";
import {licenseList                         } from "../LoanEditor/components/LicenseList";

import {ServiceFactory                      } from "../LoanEditor/ng-common/service";
import {PipelineModels                      } from "./ng-common/PipelineModels";
import {lqbInput                            } from "./ng-common/pipelineLqbInput";
import {PipelineEditorController            } from "./ng-common/PipelineEditorController";
import {LoadingIndicator                    } from "../LoanEditor/ng-common/loadingIndicator";

import {pipelineAppDirective                 } from "./pipelineApp";
import {pipelineTableDirective               } from "./PipelineTable";
import {brokerInfoComponent                  } from "./BrokerInfo";
import {companyInfoDirective                 } from "./BrokerInfo/CompanyInfo";
import {passwordOptionsDirective             } from "./BrokerInfo/PasswordOptions";
import {ipRestrictionsDirective              } from "./BrokerInfo/IpRestrictions";
import {optionsDirective                     } from "./BrokerInfo/Options";
import {lockPoliciesDirective                } from "./BrokerInfo/LockPolicies";
import {legalAddressDirective                } from "./BrokerInfo/LegalAddress";
import {loanNamingDirective                  } from "./BrokerInfo/LoanNaming";
import {creditReportDirective                } from "./BrokerInfo/CreditReport";
import {companyLicensesDirective             } from "./BrokerInfo/CompanyLicenses";

stateConfig.$inject = ["$stateProvider", "$urlRouterProvider", "$rootScopeProvider"];
function stateConfig($stateProvider: angular.ui.IStateProvider, $urlRouterProvider: angular.ui.IUrlRouterProvider, $rootScopeProvider: any):void {
    $rootScopeProvider.digestTtl(20);

    $urlRouterProvider.otherwise("/pipelineTable");

    $stateProvider
		.state("pipelineTable"             , { url: "/pipelineTable"  , template: getTemplate(pipelineTableDirective          ) })
		.state("brokerInfo"                , { url: "/brokerInfo"     , template: getTemplate(brokerInfoComponent             ) })
		.state("brokerInfo.companyInfo"    , { url: "/companyInfo"    , template: getTemplate(companyInfoDirective            ) })
        .state("brokerInfo.ipRestrictions" , { url: "/ipRestrictions" , template: getTemplate(ipRestrictionsDirective         ) })
		.state("brokerInfo.options"        , { url: "/options"        , template: getTemplate(optionsDirective                ) })
        .state("brokerInfo.passwordOptions", { url: "/passwordOptions", template: getTemplate(passwordOptionsDirective        ) })
		.state("brokerInfo.lockPolicies"   , { url: "/lockPolicies"   , template: getTemplate(lockPoliciesDirective           ) })
        .state("brokerInfo.legalAddress"   , { url: "/legalAddress"   , template: getTemplate(legalAddressDirective           ) })
        .state("brokerInfo.loanNaming"     , { url: "/loanNaming"     , template: getTemplate(loanNamingDirective             ) })
        .state("brokerInfo.creditReport"   , { url: "/creditReport"   , template: getTemplate(creditReportDirective           ) })
        .state("brokerInfo.companyLicenses", { url: "/companyLicenses", template: getTemplate(companyLicensesDirective        ) })
        .state("TODO"                      , { url: "/TODO"           , template: '<p>Invalid page or not yet implemented</p>'  })
    ;
}

export const app = angular.module("app", ["ngSanitize", "ui.router", "ui.select", "dragularModule"])
    .service  (LoadingIndicator                 .ngName, LoadingIndicator                   )
    .service  ("service"                               , ServiceFactory                     )
    .service  ("dialog"                                , Dialog                             )
    .service  (PipelineModels                   .ngName, PipelineModels.ngFactory           )
    .directive(lqbInput                         .ngName, lqbInput                           )
    .config(stateConfig)
    .component(lqModal.ngName, lqModal);

[
    pipelineAppDirective    ,
	lockPoliciesDirective   ,
].forEach(d => { app.directive(d.ngName, d) });

[
    pipelineTableDirective  ,
    brokerInfoComponent     ,
    companyInfoDirective    ,
    ipRestrictionsDirective ,
    passwordOptionsDirective,
    legalAddressDirective   ,
    loanNamingDirective     ,
    creditReportDirective   ,
    companyLicensesDirective,
    optionsDirective        ,
    hiddenCrasModal         ,
    branchPrefixModal       ,
    licenseList             ,
].forEach(c => { app.component(c.ngName, c) });


function getTemplate(directive: ng.IComponentOptions) {
    const directiveTag = directive.ngName.replace(/([a-z])([A-Z])/g, "$1-$2").toLowerCase();
    return `<${directiveTag}
        ref="vm.setPipelineEditor($ref)"
        on-summary-changed="vm.onSummaryChanged($summary, $applicants)"
        on-state-changed="vm.navToPage($pageStateId)"
    >`;
}
