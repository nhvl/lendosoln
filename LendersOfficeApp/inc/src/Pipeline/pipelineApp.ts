import {PipelineEditorController} from "./ng-common/PipelineEditorController";
import {IApplicant      } from "../LoanEditor/ng-common/IApplicant";
import {WarnSaveMessage } from "../LoanEditor/ng-common/DataModels";
import {ILoanSummary} from "../LoanEditor/ng-common/ILoanSummary";
import {getApplicantId} from "./ng-common/getPipelineEditorConfig";
import {Dialog, IDialog} from "../ng-common/dialog";
import {sublMatch} from "../utils/sublMatch";

const appHtml = require("./pipelineApp.html");

interface IPage {
    name     : string;
    route   ?: string;
    children?: IPage[];
    isExpand?: boolean;
}

const nav: IPage[] = require("./portlets.json");

const fav: string[] = [
    "Front-end Rate Lock",
    "This Loan Info",
    "Other Financing",
    "Upfront MIP / FF",
];
let favorites: IPage[] = [];
function IsPageInList(pageName: string, pageList: IPage[]) {
    return pageList.some((page:IPage) => page.name === pageName);
}

const descPages: { desc:string, page:IPage }[] = [];
initDescPages("", nav);
function initDescPages(prefix: string, ps: IPage[]) {
    ps.forEach(page => {
        if (fav.includes(page.name) && !IsPageInList(page.name, favorites)) favorites.push(page);

        const desc = ((!prefix) ? page.name : `${prefix} / ${page.name}`);
        if (page.children != null) {
            initDescPages(desc, page.children);
        } else {
            descPages.push({ desc, page });
        }
    });
}

const route2Page = (() => {
    return registerRoute2Map(new Map<string, IPage[]>(), nav, []);

    function registerRoute2Map(route2Page: Map<string, IPage[]>, ps: IPage[], path: IPage[]) {
        ps.forEach(p => {
            const newPath = path.concat(p);
            if (p.children != null) {
                registerRoute2Map(route2Page, p.children, newPath);
            } else {
                route2Page.set(p.route, newPath);
            }
        });
        return route2Page;
    }
})();

let dialog: IDialog;

class PipelineAppController {
    private loanSummary: ILoanSummary;

    private nav: typeof nav;
    private favorites: IPage[];
    private descPages: { desc: string, page: IPage }[];
    private searchQuery: string;

    private selected1: IPage;
    private selected2: IPage;
    private selected3: IPage;
    private selectedPage : IPage;

    private applicants: IApplicant[];
    private selectedApplicant: IApplicant;

    private pipelineEditor: PipelineEditorController;

    private theme: any;

    private currentYear:number = new Date().getFullYear();

    static $inject = ["$rootScope", "$element", "$state", "$q", "dialog", "$scope"];
    constructor($rootScope:ng.IRootScopeService,
                private $element:ng.IAugmentedJQuery,
                private $state:ng.ui.IStateService,
                private $q: ng.IQService,
                _dialog: IDialog,
                $scope: any
    ){
        dialog = _dialog;

        this.loanSummary = null;

        this.nav = nav;
        this.favorites = favorites;
        this.descPages = descPages;
        this.searchQuery = "";

        this.applicants = null;
        this.selectedApplicant = null;

        $scope.$on('OpenWindow', function (e : any, args: any){
            var childWindow = window.open(args["url"], "_blank");

            $(window).unload(function(){
                childWindow.close();
            });
        });

        $rootScope.$on("$stateChangeStart", (event: ng.IAngularEvent, toState: ng.ui.IState) => {
            const route = toState.name;
            if (route2Page.has(route)) {
                const xs = route2Page.get(route);
                [this.selected1, this.selected2, this.selected3] = xs;
                this.selectedPage = xs[xs.length - 1];
                if (!!this.selected1) this.selected1.isExpand = true;
                if (!!this.selected2) this.selected2.isExpand = true;
            }

            $(".content").scrollTop(0);
        });

        $scope.log = function(){
        console.log(parent);
        console.log($scope);
        console.log($scope.parent);
        };

        $scope.IsPageInList = IsPageInList;

        setTimeout(setContainerDimensions, 0);
        $(window).resize(setContainerDimensions);

        $element.find(".lightbulb").on("click", function() {
            const themeCSS = $("#Theme");
            themeCSS.attr("href",
                themeCSS.attr("href").indexOf("Dark") > -1
                    ? "../css/Prototype/SASS/PipelineTheme.css"
                    : "../css/Prototype/SASS/DarkPipelineTheme.css");
        });

        $('.header-main, .content').on('click', function() { $('.override').removeClass('override'); });

        $("body").css("overflow", "hidden");
        $("#LoanSummaryToggle, .nav-toggle-small, .nav-toggle-normal").click(() => { setTimeout(setContainerDimensions, 10); });

        $(window).on('blur', () => { $('.dropdown-toggle').parent().removeClass('open'); });

        setTimeout(() => {
            let $sb = $element.find(".sidebar-navigation");
            (<any>$sb.find(".navbar:not(.user-settings)").css("height", "100%")).niceScroll({
                cursorcolor: "rgba(255, 255, 255, .13)",
                cursorborder: "rgba(255, 255, 255, .13)",
                autohidemode: "leave",
                cursorwidth: ".5rem"
            });

            // hide <- / -> buttons and expend search textbox when focus
            let $sg = $sb.find(".search-group");
            let $sg1 = $sg.find(".search-group-first");

            $sg.on("focus blur", "input.ui-select-search", (event) => {
                $sg1.toggleClass("covered", event.type === "focusin");
                $sg .toggleClass("expand" , event.type === "focusin");
            });

            //
            const $navHeader = $element.find(".navbar.header");
            const $lst = $navHeader.find("#LoanSummaryToggle").addClass("collapsed");
            $lst.click(() => { $navHeader.toggleClass("hide-shadow", $lst.hasClass("collapsed")); });

            //
            const applicantsDdlBtn = $element.find(".applicants-ddl-btn");
            const itemWidth = $element.find(".applicants-ddl-items").outerWidth();
            if (applicantsDdlBtn.outerWidth() < itemWidth) {
                applicantsDdlBtn.width(itemWidth);
            }
        });

        function setContainerDimensions() {
            var sideBar = $(".sidebar-navigation");
            var scrollBars = (<any> sideBar.find('.navbar')).getNiceScroll();
            if ((sideBar.is(":visible") && sideBar.css("visibility") != "collapse") || sideBar.hasClass("override")) {
                scrollBars.show();
                setTimeout(() => { scrollBars.resize(); }, 300);
            } else { scrollBars.hide(); }
        }

        on_Ctrl_S(() => { this.save(); return false; });
    }

    private searchFilterComparator(x:string, y:string){
        return (typeof x === "string" && typeof y === "string" && sublMatch(y, x));
    }

    private toggleFavorite(pageName: string) {
        const {descPages, favorites} = this;

        const page = descPages.find(({page}) => page.name === pageName).page;
        const index = favorites.indexOf(page);
        if (index > -1) favorites.splice(index, 1);
        else favorites.push(page);
    }

    private navClick(xs: IPage[], $event: ng.IAngularEvent) {
        const x = xs[xs.length - 1];
        if (x.children != null) {
            x.isExpand = !x.isExpand;
            $($event.currentTarget).next("ul").collapse("toggle");
        } else {
            const go = () => {
                [this.selected1, this.selected2, this.selected3] = xs;
                this.selectedPage = x;
                this.$state.go(x.route);
            };

            this.askSave().then(() => { go() });
        }
    }
    private navToPage(pageStateId: string) {
        this.askSave().then(() => { this.$state.go(pageStateId); });
    }
    private askSave() {
        const {$q} = this;

        return (
            (this.pipelineEditor == null || (!this.pipelineEditor.pipeline.isDirty()) || (!this.pipelineEditor.isSaveAvailable))
            ? $q.when()
            : dialog.ync("Do you want to save the changes?", "Save?").then(o => {
                switch (o) {
                    case  1: return this.pipelineEditor.save();
                    case  0: this.pipelineEditor.pipeline.skipDirtyCheck1Time = true; return;
                    case -1: return $q.reject();
                    default: console.error(o); return $q.reject();
                }
            })
        );
    }

    private searchSelect(page: IPage){
        [this.selected1, this.selected2, this.selected3] = route2Page.get(page.route);
        this.selectedPage = page;
        this.$state.go(page.route);
    }

    private navBack   () { this._historyGo(-1); }
    private navForward() { this._historyGo( 1); }
    private _historyGo(i: number){
        if (this.pipelineEditor.pipeline.isDirty()) {
            dialog.ync("Do you want to save the changes?", "Save?")
                .then(o => {
                    switch (o) {
                        case  1: this.pipelineEditor.save().then(() => { window.history.go(i) }); return;
                        case  0: this.pipelineEditor.pipeline.skipDirtyCheck1Time = true; window.history.go(i); return;
                        case -1: return;
                        default: console.error(o); return;
                    }
                });
        } else window.history.go(i);
    }

    private save() {
        this.pipelineEditor.save();
    }

    private print() { window.print(); }

    private close(){
        if (this.pipelineEditor.pipeline.isDirty()) {
            dialog.ync("Do you want to save the changes?", "Save?")
                .then(o => {
                    switch (o) {
                        case  1: this.pipelineEditor.save().then(() => { window.close(); }); return;
                        case  0: this.pipelineEditor.pipeline.skipDirtyCheck1Time = true; window.close(); return;
                        case -1: return;
                        default: console.error(o); return;
                    }
                });
        } else window.close();
    }

    private oldUiClick()
    {
        window.location.href = "../los/main.aspx";
    }

    private onSummaryChanged(summary: ILoanSummary, applicants: IApplicant[]) {
        this.loanSummary = summary;
        this.applicants = applicants;

        const applicantId = getApplicantId();

        if (this.selectedApplicant == null) {
            this.selectedApplicant = !applicantId
                ? this.applicants[0]
                : this.applicants.find(x => x.aAppId == applicantId)
            ;
        }
    }

    private setPipelineEditor(pipelineEditor: PipelineEditorController) {
        this.pipelineEditor = pipelineEditor;
    }

    private selectApplicant(applicant: IApplicant){
        this.selectedApplicant = applicant;
    }

    private checkToggle() {
        const nav = this.$element.find(".sidebar-navigation");
        nav.toggleClass("override in height", !nav.hasClass("override"))
    }

    private removeOverride() {
        const nav = this.$element.find(".sidebar-navigation");
        nav.removeClass("override");
    }
}

pipelineAppDirective.ngName = "pipelineApp";
export function pipelineAppDirective(): ng.IDirective {
    return {
        restrict        : "E",
        scope           : {},
        controller      : PipelineAppController,
        controllerAs    : "vm",
        template        : appHtml,
    };
}

function on_Ctrl_S(func: () => boolean) {
    const keys_S = 83;

    $(document).on("keydown.shortkey", (event: JQueryKeyEventObject) => {
        const is_Ctrl_S = (event.ctrlKey && !event.altKey && event.keyCode === keys_S);
        if (!is_Ctrl_S) return true;

        return func();
    });
}
