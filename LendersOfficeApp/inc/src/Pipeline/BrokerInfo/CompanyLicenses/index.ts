import {LoadingIndicator                  } from "../../../LoanEditor/ng-common/loadingIndicator";
import {PipelineEditorController          } from "../../ng-common/PipelineEditorController";
import {PipelineModels, ILoanFields       } from "../../ng-common/PipelineModels";
import {createBasicPipelineEditorComponent} from "../../ng-common/createBasicPipelineEditorComponent";
import {LicenseListController             } from "./../../../LoanEditor/components/LicenseList/LicenseList";

import * as appHtml from "./index.html";

class CompanyLicensesController extends PipelineEditorController {
    private licenseList: LicenseListController;

    onLoaded(pipeline:PipelineModels) : boolean {
        this.licenseList.nmLsIdentifier = pipeline.broker["NmLsIdentifier"];
        this.licenseList.licenses = pipeline.ComLicenses;
        this.licenseList.licenseTemplate = angular.merge({}, pipeline.CompanyLicenseTemplate);

        return super.onLoaded(pipeline);
    }
}

export const companyLicensesDirective = createBasicPipelineEditorComponent("companyLicenses", appHtml, CompanyLicensesController);
