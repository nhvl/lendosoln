import {LoadingIndicator              } from "../../../LoanEditor/ng-common/loadingIndicator";
import {PipelineEditorController              } from "../../ng-common/PipelineEditorController";
import {PipelineModels                    } from "../../ng-common/PipelineModels";
import {createBasicPipelineEditorComponent} from "../../ng-common/createBasicPipelineEditorComponent";

import * as appHtml from "./index.html";

class LegalAddressController extends PipelineEditorController {
}

export const legalAddressDirective = createBasicPipelineEditorComponent("legalAddress", appHtml, LegalAddressController);
