import {LoadingIndicator                   } from "../../../LoanEditor/ng-common/loadingIndicator";
import {PipelineEditorController           } from "../../ng-common/PipelineEditorController";
import {PipelineModels                     } from "../../ng-common/PipelineModels";
import {createBasicPipelineEditorComponent } from "../../ng-common/createBasicPipelineEditorComponent";
import {LqModalController, ModalStatus     } from "../../../LoanEditor/components/lqModal";

import * as appHtml from "./index.html";

class IpRestrictionsController extends PipelineEditorController {
    workingIpItem: any = {};
    shouldShowInputError:boolean = false;
    modal: LqModalController;
    init(){
         this.isSaveAvailable = false;
    }

    get isEdit():Boolean {
        return this.workingIpItem.workingIndex != null;
    }

    deleteItem(index: number){
        this.dialog.confirm('Do you want to delete this ' + this.pipeline.IpWhiteList[index].IpAddress.v + '?')
        .then((isConfirm:boolean) => {
            if (isConfirm){
                this.pipeline.IpWhiteList.splice(index, 1);
                this.pipeline.calculate();
            }
        });
    }

    editItem(index: number){
        const ipItem = this.pipeline.IpWhiteList[index];

        this.workingIpItem = {};
        this.workingIpItem.workingIndex = index;
        this.workingIpItem.IpAddress = ipItem.IpAddress.v;
        this.workingIpItem.Description = ipItem.Description.v;

        this.checkIpField();
        this.showModal();
    }

    addItem(){
        this.workingIpItem = {};
        this.workingIpItem.IpAddress = String();
        this.checkIpField();
        this.showModal();
    }

    showModal(){
        this.modal.showWithOKCancel(
            ((promiseValue) =>{
                switch(promiseValue){
                    case ModalStatus.Save:
                        this.applyModal();
                        break;
                    case ModalStatus.Discard:
                        this.modal.close();
                        break;
                }
            }), (() => {
                if (this.isEdit){
                    const ipItem = this.pipeline.IpWhiteList[this.workingIpItem.workingIndex];
                    return ipItem.IpAddress.v !== this.workingIpItem.IpAddress
                    || ipItem.Description.v !== this.workingIpItem.Description;
                } else {
                    return this.workingIpItem.IpAddress || this.workingIpItem.Description
                }
            })
        );
    }

    applyModal(){
        if (this.shouldShowInputError){
            this.dialog.alert("IP Address is invalid format.", "Message from webpage");
            return;
        }

        if (this.isEdit){
            const ipItem = this.pipeline.IpWhiteList[this.workingIpItem.workingIndex];
            ipItem.IpAddress.v = this.workingIpItem.IpAddress;
            ipItem.Description.v = this.workingIpItem.Description;
        } else {
            if (this.pipeline.IpWhiteList == null){
                this.pipeline.IpWhiteList = [];
            }

            const ipItem = JSON.parse(JSON.stringify(this.pipeline.IpWhiteListTemplate));
            ipItem.IpAddress.v = this.workingIpItem.IpAddress;
            ipItem.Description.v = this.workingIpItem.Description;
            this.pipeline.IpWhiteList.push(ipItem);
        }

        this.pipeline.calculate();
        this.modal.close();
    }

     checkIpField(){
        let ipAddrText = this.workingIpItem.IpAddress;
        var isIpAddrFormat = /^((\*|\d{1,3})\.(\*|\d{1,3})\.(\*|\d{1,3})\.(\*|\d{1,3}))$/.test(ipAddrText);
        if (isIpAddrFormat) {
            for (var addrByteString of ipAddrText.split(".")){
                let addrByte = addrByteString as number;
                console.log(addrByte);
                if (addrByte < 0 || addrByte > 255){
                    this.shouldShowInputError = true;
                    return;
                }
            }
        } else {
            this.shouldShowInputError = true;
            return;
        }
        this.shouldShowInputError = false;
     }
}

export const ipRestrictionsDirective = createBasicPipelineEditorComponent("ipRestrictions", appHtml, IpRestrictionsController);
