
import * as appHtml from "./index.html";

interface IMyScope extends ng.IScope
{
	tableInfo: any;
}

class lockPoliciesController {
	private json: string;

	static $inject = ["$rootScope", "$element", "$state", "$scope", "$http"];
    constructor($rootScope:ng.IRootScopeService, private $element:ng.IAugmentedJQuery, private $state:ng.ui.IStateService, $scope: IMyScope, $http: ng.IHttpService ) {
	}
}

lockPoliciesDirective.ngName = "lockPolicies";
export function lockPoliciesDirective(): ng.IDirective {
    return {
        restrict        : "E",
        scope           : {},
        controller      : lockPoliciesController,
        controllerAs    : "pt",
        template        : appHtml,
    };
}
