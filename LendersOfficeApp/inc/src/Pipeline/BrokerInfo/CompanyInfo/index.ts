import {PipelineEditorController          } from "../../ng-common/PipelineEditorController";
import {isValidLqbInputModel              } from "./../../../LoanEditor/ng-common/ILqbDataModel";
import {createBasicPipelineEditorComponent} from "../../ng-common/createBasicPipelineEditorComponent";

import * as appHtml from "./index.html";

class CompanyInfoController extends PipelineEditorController {

    init(){
        angular.element('[data-toggle="popover"]').popover();
    }
    shouldShowFhaLenderIdError(): boolean{
        let fhaLenderIdInput = this.pipeline.broker["FhaLenderId"];
        let isHiddenFhaInput = this.pipeline.broker["IsHideFhaLenderIdInTotalScorecard"];

        let isValidField = isValidLqbInputModel(fhaLenderIdInput,'broker.FhaLenderId');

        if (isValidField && String(fhaLenderIdInput.v).length > 0){
            isHiddenFhaInput.r = false;
        } else {
            isHiddenFhaInput.r = true;
            isHiddenFhaInput.v = false;
        }

        return !isValidField;
    }
}

export const companyInfoDirective = createBasicPipelineEditorComponent("companyInfo", appHtml, CompanyInfoController);
