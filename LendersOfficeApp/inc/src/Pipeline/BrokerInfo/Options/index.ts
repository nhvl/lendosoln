
import * as appHtml from "./index.html";

interface IMyScope extends ng.IScope
{
	tableInfo: any;
}

class optionsController {
	private json: string;

	static $inject = ["$rootScope", "$element", "$state", "$scope", "$http"];
    constructor($rootScope:ng.IRootScopeService, private $element:ng.IAugmentedJQuery, private $state:ng.ui.IStateService, $scope: IMyScope, $http: ng.IHttpService ) {
	}
}

optionsDirective.ngName = "options";
export function optionsDirective(): ng.IDirective {
    return {
        restrict        : "E",
        scope           : {},
        controller      : optionsController,
        controllerAs    : "pt",
        template        : appHtml,
    };
}
