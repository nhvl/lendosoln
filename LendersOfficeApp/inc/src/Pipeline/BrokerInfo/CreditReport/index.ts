import {LoadingIndicator                    } from "../../../LoanEditor/ng-common/loadingIndicator";
import {PipelineEditorController            } from "../../ng-common/PipelineEditorController";
import {PipelineModels                      } from "../../ng-common/PipelineModels";
import {createBasicPipelineEditorComponent} from "../../ng-common/createBasicPipelineEditorComponent";
import {HiddenCrasModalController           } from "../../../LoanEditor/components/HiddenCrasModal";

import * as appHtml from "./index.html";
const emptyGuid = "00000000-0000-0000-0000-000000000000";

class CreditReportController extends PipelineEditorController {
    modal: HiddenCrasModalController;

    clearAgent(agent: any){
        agent.id.v = emptyGuid;
        this.pipeline.calculate();
    }
}

export const creditReportDirective = createBasicPipelineEditorComponent("creditReport", appHtml, CreditReportController);
