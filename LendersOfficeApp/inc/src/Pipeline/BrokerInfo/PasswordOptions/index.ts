import {LoadingIndicator              } from "../../../LoanEditor/ng-common/loadingIndicator";
import {PipelineEditorController              } from "../../ng-common/PipelineEditorController";
import {PipelineModels                    } from "../../ng-common/PipelineModels";
import {createBasicPipelineEditorComponent} from "../../ng-common/createBasicPipelineEditorComponent";
import * as appHtml from "./index.html";
type KeyValuePair = {Key: string, Value: string};

class PasswordOptionsController extends PipelineEditorController {
    applySuccess:boolean = false;
    errorString: string = null;

    init(){
         this.isSaveAvailable = false;
    }
    apply() {
        const f = () => {
            this.errorString = null;
            this.applySuccess = false;
            var option: string;
            if (this.pipeline.PasswordOption.branch.v == null || this.pipeline.PasswordOption.branch.v == 0){
                this.pipeline.PasswordOption.branch.v = "";
            }

            if (this.pipeline.PasswordOption.expireDate.v == null){
                this.pipeline.PasswordOption.expireDate.v = undefined;
            }

            switch(this.pipeline.PasswordOption.option.v){
                case 0:
                    option = 'NextL';
                    break;
                case 1:
                    option = 'Never';
                    break;
                case 2:
                    option = 'Dated';
                    break;
            }

            this.service.brokerInfo.ApplyPasswordOptions({
                branch: this.pipeline.PasswordOption.branch.v,
                option: option,
                expirationD: this.pipeline.PasswordOption.expireDate.v,
                cycleExpire: this.pipeline.PasswordOption.cycleExpire.v,
                expirePeriod: String(this.pipeline.PasswordOption.expirePeriod.v)
            }).then((errorMessage) => {
                if (errorMessage){
                    this.applySuccess = false;
                    this.errorString = errorMessage;
                } else {
                    this.applySuccess = true;
                }
            }).catch((e: any) => {
                console.log(e);
            });
        };
        this.executeClick(f);
    }

    executeClick(f: Function): void {
        if (this.pipeline.isCalculating) this.pipeline.on("postCalc", f);
        else f();
    }
}

export const passwordOptionsDirective = createBasicPipelineEditorComponent("passwordOptions", appHtml, PasswordOptionsController);
