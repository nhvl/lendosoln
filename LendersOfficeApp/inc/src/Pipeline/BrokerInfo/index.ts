import {PipelineEditorController          } from "../ng-common/PipelineEditorController";
import {ILoanSummary                      } from "../../LoanEditor/ng-common/ILoanSummary";
import {IApplicant                        } from "../../LoanEditor/ng-common/IApplicant";

class BrokerInfoController {
    private currentTab: string;

    static $inject = ["$rootScope", "$state"];
    constructor($rootScope: ng.IRootScopeService, $state: ng.ui.IStateService){
        if ($state.is("brokerInfo")) {
            this.goTo("brokerInfo.companyInfo");
        }

        $rootScope.$on("$stateChangeStart", (event: ng.IAngularEvent, toState: ng.ui.IState) => {
            const route = toState.name;
            try {
                const [, currentTab] = route.split(".");
                this.currentTab = currentTab;
            } catch(e) { debugger; }
        });
    }

    goTo(pageStateId:string):void {
        this._c_onStateChanged({$pageStateId:pageStateId});
    }

    protected _c_onSummaryChanged: (x:{ $summary: ILoanSummary, $applicants: IApplicant[] }) => void;
    private onSummaryChanged($summary: ILoanSummary, $applicants: IApplicant[]) {
        this._c_onSummaryChanged({$summary, $applicants});
    }

    protected _c_onStateChanged: (x: { $pageStateId: string }) => void;
    private onStateChanged($pageStateId: string) {
        this._c_onStateChanged({ $pageStateId });
    }

    private _c_ref       : (_: {$ref  : PipelineEditorController}) => void;
    private setPipelineEditor($ref: any) {
        this._c_ref({ $ref });
    }
}

import * as appHtml from "./index.html";

export const brokerInfoComponent : ng.IComponentOptions = {
    ngName                 : "brokerInfo",
    bindings               : {
        _c_onSummaryChanged: "&onSummaryChanged",
        _c_onStateChanged  : "&onStateChanged"  ,
        _c_ref             : "&ref"             ,
    },
    controllerAs           : "vm",
    template               : appHtml,
    controller             : BrokerInfoController,
};
