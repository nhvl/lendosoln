import {PipelineEditorController                } from "../../ng-common/PipelineEditorController";
import {createBasicPipelineEditorComponent      } from "../../ng-common/createBasicPipelineEditorComponent";
import {BranchPrefixModalController             } from "../../../LoanEditor/components/BranchPrefixModal";

import * as appHtml from "./index.html";

class LoanNamingController extends PipelineEditorController {
    modal: BranchPrefixModalController;
    init(){
        angular.element('[data-toggle="popover"]').popover();
    }
}

export const loanNamingDirective = createBasicPipelineEditorComponent("loanNaming", appHtml, LoanNamingController);
