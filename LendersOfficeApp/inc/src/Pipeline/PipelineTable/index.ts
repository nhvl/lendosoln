import {ILqbDataModel, LqbInputType,E_CalculationType} from "../../LoanEditor/ng-common/ILqbDataModel";
import {$timeout                                     } from "../../LoanEditor/ng-common/ngServices";
import {PipelineEditorController                     } from "../ng-common/PipelineEditorController";
import {PipelineModels                               } from "../ng-common/PipelineModels";
import {createBasicPipelineEditorComponent           } from "../ng-common/createBasicPipelineEditorComponent"
import {E_TabTypeT                                   } from "../../DataLayer/Enums/E_TabTypeT";

import * as appHtml from "./index.html";

class PipelineTableController extends PipelineEditorController {
    private sortKey: string;
    private isAsc: boolean;
    private selectAllState:boolean;
    private pipelineLocal : {
        [key: string]: ILqbDataModel
    }
    private SelectedPipeline: string;

    protected init() {
        this.SelectedPipeline = "LoanPipeline";
        this.windows = [];
        this.pipelineLocal = {};
    }

    protected onLoaded(pipeline:PipelineModels) : boolean {
        super.onLoaded(pipeline);

        this.pipelineLocal["selectAll"]={
            t:LqbInputType.Checkbox,
            v:false,
            r:false,
            h:false,
            calcAction:E_CalculationType.NoCalculate
        };
        this.selectAllState = false;
        for(var i = 0; i < this.pipeline.TableInfo.Rows.length;i++){
            this.pipelineLocal["checkbox"+i]={
                t:LqbInputType.Checkbox,
                v:false,
                r:false,
                h:false,
                calcAction:E_CalculationType.NoCalculate
            };
        }

        this.SwitchPipeline('0');
        window.setInterval(()=>{
            if(this.pipelineLocal != null && this.pipeline != null && this.selectAllState != this.pipelineLocal["selectAll"].v){
                this.selectAllState = <boolean>this.pipelineLocal["selectAll"].v;
                for(var i = 0; i < this.pipeline.TableInfo.Rows.length;i++){
                    this.pipelineLocal["checkbox"+i].v = this.selectAllState;
                }
                this.pipeline.calculate();
            }
        },10);
        return true;
    }

    protected SwitchPipeline(index : string){
        this.pipeline.PipelineView.Index = ' ' + index + ' ';
        this.pipeline.TableInfo = null;
        this.pipeline.PipelineView.Active = this.pipeline.PipelineView.Tabs[Number(index)];
        this.pipeline.calculate();
    }

    private OpenLoan(sLID: string) {
        this.OpenWindow(`../LoanEditor/LoanEditor.aspx?loanid=${sLID}`, this.windows);
        return false;
    }

    private IsEnableButtons(){
        if(this.pipelineLocal != null && this.pipeline != null && this.pipeline.TableInfo != null){
            for(var i = 0; i < this.pipeline.TableInfo.Rows.length;i++){
                    if(this.pipelineLocal["checkbox"+i]!=null && this.pipelineLocal["checkbox"+i].v)
                        return true;
                }
        }
        return false;
    }
    private GetTabColoring(type:E_TabTypeT){
        if(type == null){
            type = E_TabTypeT.PipelinePortlet;
        }
        switch (type)
        {
            case E_TabTypeT.PipelinePortlet:
                //return "#3b50ce";
                return "PipelinePortlet";
            case E_TabTypeT.DisclosurePortlet:
                //return "#dd191d";
                return "DisclosurePortlet";
            case E_TabTypeT.LoanPoolPortlet:
            case E_TabTypeT.TradeMasterPortlet:
                //return "#43A047";
                return "TradePoolPortlet";
            case E_TabTypeT.MyTasksPortlet:
            case E_TabTypeT.RemindersPortlet:
            case E_TabTypeT.DisctrackPortlet:
            case E_TabTypeT.MyLoanRemindersPortlet:
                //return "#FB8C00";
                return "MyTaskPortlet";
            case E_TabTypeT.UnassignedLoanPortlet:
            case E_TabTypeT.MyLeadPortlet:
            case E_TabTypeT.UnassignLeadPortlet:
                //return "#424242";
                return "MyLeadPortlet";
            case E_TabTypeT.TestPortlet:
                //return "#FDD835";
                return "TestPortlet";
            default:
                //throw CBaseException.GenericException("Unsupported tab type: " + type);
                //return "#ffffff";
                return "NoType";
        }
    }

    private sortRegulars(by: string) {
        if (this.pipeline.isCalculating) return;
        if (by == this.sortKey) this.isAsc = !this.isAsc;
        else {
            this.sortKey = by;
            this.isAsc = true;
        }
        var property = this.GetHeaderID(this.sortKey);
        var sortOrder = -1;
        if (this.isAsc) sortOrder = 1;
        //http://stackoverflow.com/questions/8829765/regular-expression-for-dollar-amount-in-javascript
        var moneyRegex = /^\$?\d+(,\d{3})*\.?[0-9]?[0-9]?$/;
        this.pipeline.TableInfo.Rows = this.pipeline.TableInfo.Rows.sort(function (a:any, b:any) {
            var result = 0;
            if(moneyRegex.test(a[property]) && moneyRegex.test(b[property])){
                let valA = Number(a[property].replace(/[^0-9\.]+/g,""));
                let valB = Number(b[property].replace(/[^0-9\.]+/g,""));
                result = (valA < valB) ? -1 : (valA > valB) ? 1 : 0;
            }
            else
            {
                if((typeof a[property]) == (typeof b[property]) && (typeof a[property]) != "string")
                        result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                else
                    result = (String(a[property]).toLowerCase() < String(b[property]).toLowerCase())
                            ? -1
                            : (String(a[property]).toLowerCase() > String(b[property]).toLowerCase())
                            ? 1 : 0;
            }
            return result * sortOrder;
        });
    }
    private GetHeaderID(key:string):string{
        switch (key) {
            case this.pipeline.TableInfo.Headers[0]: return "sLNm";
            case this.pipeline.TableInfo.Headers[1]: return "sLAmtCalc";
            case this.pipeline.TableInfo.Headers[2]: return "sLPurposeT";
            case this.pipeline.TableInfo.Headers[3]: return "sRateLockStatusT";
            case this.pipeline.TableInfo.Headers[4]: return "sInitialClosingDisclosureCreatedD";
            case this.pipeline.TableInfo.Headers[5]: return "sLRefNm";
            default: return null;
        }
    }
}

export const pipelineTableDirective = createBasicPipelineEditorComponent("pipelineTable", appHtml, PipelineTableController)
