﻿import * as angular from "angular";
import "angular-messages";

import {serviceGen, IService} from "../../../ng-common/serviceGen";
import {formValidate} from "../../../ng-common/formValidate";
//import {sortHandle, OrderSpec} from "../../../ng-directives/sortIcon";

const $ = jQuery;

class DJobConfig {
    JobID: number;
    JobXmlContent: string;
    JobStatus: string;
    JobPriority: string;
    JobSubmitD: string;
    JobStartD: string;
    JobNotifyEmailAddr: string;
    JobUserLabel: string;
    JobUserNotes: string;
    UserLoginNm: string;

    ErrorText: string;
    IsShowError: boolean;

    constructor(d?: DJobConfig) {
        if (d != null) $.extend(this, d);
        return this;
    }
    JobCommand(command:string): ng.IPromise<DJobConfig[]> {
        const service = DJobConfig.service;
        const $q = DJobConfig.$q;
        return service<DJobConfig[]>("JobCommand", { jobId: this.JobID, command: command })
            .then((error) => (error == null) ? $q.when(null) : $q.reject(error));
    }
    static getList(): ng.IPromise<DJobConfig[]> {
        const service = DJobConfig.service;
        return service<DJobConfig[]>("Get")
            .then(
            vs => vs.map(v => new DJobConfig(v)));
    }

    static $q :ng.IQService;
    static service<T>(method: string, args?: {}): ng.IPromise<T> {
        console.error("Have not assgin [service] function");
        return null;
    }
}

DJobConfigFactory.$inject = ["serviceGen", "$q"];
function DJobConfigFactory(serviceGen:(url?: string) => IService, $q: ng.IQService): typeof DJobConfig {
    DJobConfig.service = <any> serviceGen();
    DJobConfig.$q = $q;
    return DJobConfig;
}

interface DJobManagerScope extends ng.IScope{
}

class DJobManager{
    DJobs: DJobConfig[];
    isLoading: boolean;
    //entrySort: OrderSpec;
    command: string;//cancel or enqueue
    confirmDJ: DJobConfig;

    hasCancel: boolean;
    hasEnqueue: boolean;

    static $inject = ["$scope", "DJobConfig"];
    constructor(private $scope: DJobManagerScope) {
        this.isLoading = false;
        //this.command = "cancel";
        this.DJobs = null;
        //this.entrySort = new OrderSpec({ by: "brokerNm", desc: false });

        this.reload();
    }
    toogleError(DJ: DJobConfig) {
        DJ.IsShowError = !DJ.IsShowError;
    }
    SetJobCommand(DJ: DJobConfig, command:string) {
        this.confirmDJ = new DJobConfig(DJ);
        this.command = command;
    }
    ExecuteJobCommand() {
        this.confirmDJ.JobCommand(this.command)
            .then(
            () => {
                    $("#confirmModal").modal("hide");
                    this.reload();
                },
                (error: DJobConfig) => {
                    alert("Error");
                });
    }
    reload() {
        this.isLoading = true;
        DJobConfig.getList()
            .then((xs) => {
                this.DJobs = xs;
                for (var index in this.DJobs) {
                    switch (this.DJobs[index].JobStatus) {
                        case "Error":
                            this.hasEnqueue = true;
                        case "Pending":
                            this.hasCancel = true;
                            break;
                    }
                }
                this.isLoading = false;
            });
    }
    //sort(by: string) {
    //    if (this.entrySort.by === by) this.entrySort.desc = !this.entrySort.desc;
    //    else { this.entrySort.by = by; this.entrySort.desc = false; }
    //}
}

angular.module("app", ["ngMessages"])
    .factory("serviceGen", serviceGen)
    .factory("DJobConfig", DJobConfigFactory)
    //.directive("sortHandle", sortHandle)
    .controller("DJobManager", DJobManager)
;
