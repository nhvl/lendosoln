import { module, IPromise, IQService, IScope, IAngularEvent, IDirective, IDirectiveLinkFn} from "angular";

import {StrGuid} from "../../../utils/guid";

import {serviceGen, IService, IServiceGen} from "../../../ng-common/serviceGen";
import {formValidate} from "../../../ng-common/formValidate";

const $ = jQuery;

class VendorConfig {
    VendorId                   : StrGuid;
    VendorName                 : string;
    TestAccountId              : string;
    LiveAccountId              : string;

    PrimaryExportPath          : string;
    BackupExportPath           : string;
    PreviewExportPath          : string;
    TestingExportPath          : string;

    PackageDataListJSON        : string;
    ParameterName              : string;
    ExtraParameters            : string;

    UsesAccountId              : boolean;
    UsesPassword               : boolean;
    UsesUsername               : boolean;

    SupportsOnlineInterface    : boolean;
    SupportsBarcodes           : boolean;
    DisableManualFulfilmentAddr: boolean;
    EnableConnectionTest       : boolean;
    EnableMismo34              : boolean;

    Platform                   : number;
    IsTestVendor               : boolean;

    constructor(d?: VendorConfig) {
        if (d != null) $.extend(this, d);
    }

    update(): IPromise<string[]> {
        const service = VendorConfig.service;
        const $q      = VendorConfig.$q;

        return service<string[]>("Update", { vendor: this });
    }

    static get(vendorId:string):IPromise<VendorConfig> {
        const service = VendorConfig.service;
        return service<VendorConfig>("GetDetail", { vendorId })
            .then((v: VendorConfig) => new VendorConfig(v));
    }

    static getList(): IPromise<VendorConfig[]> {
        const service = VendorConfig.service;
        return service<VendorConfig[]>("Get")
            .then((vs: VendorConfig[]) => vs.map(v => new VendorConfig(v)));
    }

    static $q      : IQService;
    static service : IService;

    static ngName = "VendorConfig";
    static ngFactory(serviceGen: IServiceGen, $q: IQService): typeof VendorConfig {
        VendorConfig.service = serviceGen();
        VendorConfig.$q      = $q;
        return VendorConfig;
    }
}
VendorConfig.ngFactory.$inject = ["serviceGen", "$q"];

class VendorManagerController {
    eVendor  : VendorConfig; // editing vendor object
    vendors  : VendorConfig[];
    isLoading: boolean;
    errors: string[];
    platforms: any[];

    static ngName = "VendorManagerController";
    static $inject = ["$scope", VendorConfig.ngName];
    constructor(private $scope: IScope) {
        this.eVendor   = new VendorConfig();
        this.isLoading = false;

        this.reload();

        this.platforms = [
            { name: "Docutech", value: 1 },
            { name: "Docs On Demand", value: 2 },
            { name: "IDS", value: 3 },
            { name: "DocMagic", value: 4 },
            { name: "ClosingXpress", value: 6 },
            { name: "MRG", value: 7 },
            { name: "Signia Documents", value: 8 }
        ];
    }
    edit(vendor?: VendorConfig) {
        this.resetError();
        this.eVendor = new VendorConfig(vendor);
    }
    save($event: IAngularEvent) {
        if (!formValidate(<HTMLFormElement> $event.currentTarget)) return;

        this.resetError();

        this.eVendor.update().then(
            (errorMessages: string[]) => {
                if (errorMessages !== null && errorMessages.length !== 0) {
                    this.errors = errorMessages;
                    return;
                }

                $("#editVendorModal").modal("hide");
                this.reload();
            },
            (errorMessage: string) => {
                this.errors = [errorMessage];
            });
    }
    resetError() {
        this.errors = [];
    }
    reload() {
        this.isLoading = true;
        VendorConfig.getList()
            .then((xs) => {
                this.vendors = xs;
                this.isLoading = false;
            });
    }
}

import * as appHtml from "./app.html";

vendorManagerDirective.ngName = "vendorManager";
function vendorManagerDirective(): IDirective {
    return {
        restrict    : "E",
        scope       : true,

        controller  : VendorManagerController,
        controllerAs: "vm",
        template    : appHtml,
    }
}

module("app", [])
    .factory("serviceGen", serviceGen)
    .factory(VendorConfig.ngName, VendorConfig.ngFactory)
    .directive(vendorManagerDirective.ngName, vendorManagerDirective);
