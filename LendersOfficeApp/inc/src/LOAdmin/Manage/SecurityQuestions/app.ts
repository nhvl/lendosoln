import Ractive from "ractive";
import { ContextEvent } from "../../../Ractive/misc";

import {serviceGen          } from "../../../utils/serviceGen";
import {formValidate        } from "../../../utils/formValidate";
import {documentReadyPromise} from "../../../utils/documentReadyPromise";

import * as appHtml from "./app.html";

const $ = jQuery;

// services
const service = serviceGen();

//#region Model
    interface IQuestionData {
        QuestionId: number;
        Question  : string;
        IsObsolete: boolean;
    }
    class Question implements IQuestionData {
        QuestionId: number;
        Question  : string;
        IsObsolete: boolean;

        constructor(d?: IQuestionData) {
            d = (d != null) ? d : {
                QuestionId: -1,
                Question  : "",
                IsObsolete: false,
            };

            this.QuestionId = d.QuestionId;
            this.Question   = d.Question  ;
            this.IsObsolete = d.IsObsolete;
        }

        isValid() {
            if (!this.Question) return false;
            return true;
        }

        update(): Promise<void> {
            if (this.QuestionId != null)
                return service<void>(
                    "UpdateQuestion", {
                        questionId: this.QuestionId,
                        isObsolete: this.IsObsolete,
                    });
            return Question.create(this.Question).then((q) => {
                    this.QuestionId = q.QuestionId;
                });
        }

        static get(): Promise<Question[]> {
            return service<Question[]|string>("GetSecureQuestions").
                then((result) =>
                    (typeof result === "string") ? Promise.reject<Question[]>(result) :
                        result.map(d => new Question(d))
                );
        }

        static create(question: string): Promise<Question> {
            return service<number>("CreateQuestion", { question })
                .then((questionId: number) => new Question(<IQuestionData>{ QuestionId: questionId, Question: question, IsObsolete: false }));
        }
    }
//#endregion


//#region RactiveJS Components
    interface IMainData {
        questions      ?: Question[],
        editingQuestion ?: Question,
    }

    const Main = Ractive.extend({
        template: appHtml,
        data(): IMainData {
            return {
                questions      : undefined,
                editingQuestion: undefined,
            }
        },
        onrender(this: Ractive.Ractive) {
            const $editModal = $("#editModal");
            this.on({
                addNew(this: Ractive.Ractive) {
                    this.set({
                        editingQuestion: new Question()
                    } as IMainData);
                    $editModal.modal("show");

                    return false;
                },
                edit(this: Ractive.Ractive, context: ContextEvent, question:IQuestionData) {
                    this.set({
                        editingQuestion: new Question(question)
                    } as IMainData);
                    console.debug("edit vendor", question);
                    $editModal.modal("show");

                    return false;
                },
                save(this: Ractive.Ractive, context: ContextEvent) {
                    if (!(formValidate(context.node as HTMLFormElement))) {
                        return false;
                    }

                    const question: Question = this.get("editingQuestion");

                    question.update()
                        .then((v) => {
                            $editModal.modal("hide");
                            Question.get().then((questions: Question[]) => this.set(<IMainData>{ questions }));
                        }, (error) => {
                            window.alert(`The following questions failed to save: ${error}`);
                            return Promise.reject(false);
                        });

                    return false;
                },
            });
        }
    });
//#endregion

init();
async function init() {
    const questions = await Question.get();
    await documentReadyPromise;
    new Main({
        el: "container",
        data: { questions } as IMainData,
        //lazy: true,
    });
}
