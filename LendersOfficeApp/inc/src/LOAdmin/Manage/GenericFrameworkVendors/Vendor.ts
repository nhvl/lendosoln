import "../../../utils/Array.orderBy";
import {IQService, IPromise} from "angular";

import {IService, IServiceGen} from "../../../ng-common/serviceGen";
import {regexPattern         } from "../../../utils/regexPattern";

import {TypeOfService, serviceTypeListFactory} from "./TypeOfService";
import {TypeOfLoanIdentifier, loanIdentifierTypeListFactory} from "./TypeOfLoanIdentifier";

export interface IVendorData {
    CredentialXML           : string;
    IncludeUsername         : boolean;
    LaunchURL               : string;
    Name                    : string;
    ProviderID              : string;
    ServiceType             : TypeOfService;
    LoanIdentifierTypeToSend: TypeOfLoanIdentifier;
}

export class FormError {
    LaunchURL   : string;
    ServiceType :string;

    constructor(d?: {}) {
        Object.assign(this, d);
    }
}

interface SaveResult { ProviderID: string; Error: string; }

export class Vendor implements IVendorData {
    static ngName = "Vendor";

    CredentialXML           : string;
    IncludeUsername         : boolean;
    LaunchURL               : string;
    Name                    : string;
    ProviderID              : string;
    ServiceType             : TypeOfService;
    LoanIdentifierTypeToSend: TypeOfLoanIdentifier;

    get serviceTypeName(): string {
        return Vendor.typeOfService2String[this.ServiceType] || "loading&hellip;" ;
    }
    get loanIdentifierTypeName(): string {
        return Vendor.typeOfLoanIdentifier2String[this.LoanIdentifierTypeToSend] || "loading&hellip;" ;
    }

    constructor(d?: IVendorData) {
        if (d == null)
            d = {
                CredentialXML           : "",
                IncludeUsername         : false,
                LaunchURL               : "",
                Name                    : "",
                ProviderID              : "",
                ServiceType             : null as any,
                LoanIdentifierTypeToSend: TypeOfLoanIdentifier.LoanReferenceNumber,
            };

        this.CredentialXML               = d.CredentialXML  ;
        this.IncludeUsername             = d.IncludeUsername;
        this.LaunchURL                   = d.LaunchURL      ;
        this.Name                        = d.Name           ;
        this.ProviderID                  = d.ProviderID     ;
        this.ServiceType                 = d.ServiceType    ;
        this.LoanIdentifierTypeToSend    = d.LoanIdentifierTypeToSend    ;
    }

    isValid(): (FormError|null) {
        const formError = new FormError();
        let hasError = false;

        if (!this.LaunchURL) {
            formError.LaunchURL = "This field is required.";
            hasError = true;
        }
        else if (!(regexPattern.URL_REGEXP.test(this.LaunchURL))) {
            formError.LaunchURL = "Invalid URL.";
            hasError = true;
        }

        if (this.ServiceType == null) {
            formError.ServiceType = "This field is required.";
            hasError = true;
        }

        return (hasError ? formError : null);
    }

    save(): IPromise<this> {
        const $q = Vendor.$q;
        const service = Vendor.service;

        return service<SaveResult>("SaveVendor",
            {
                providerID              : this.ProviderID,
                name                    : this.Name,
                launchURL               : this.LaunchURL,
                lenderCredential        : this.CredentialXML,
                includeUsername         : this.IncludeUsername,
                serviceType             : this.ServiceType,
                loanIdentifierTypeToSend: this.LoanIdentifierTypeToSend,
            })
            .then((result: SaveResult) => {
                if (!!(result.Error)) throw result.Error;
                this.ProviderID = result.ProviderID;
                return this;
            });
    }

    static get(): IPromise<Vendor[]> {
        const $q = Vendor.$q;
        const service = Vendor.service;

        return service<IVendorData[]|string>("GetVendors").
            then((result: (IVendorData[]|string)) =>
                (typeof result === "string") ? $q.reject(result) as any :
                    result.map(d => new Vendor(d))
                        //.orderBy("ProviderID")
            );
    }

    static service :IService;
    static $q      :IQService;
    static typeOfService2String       : { [key: number]: string } = {};
    static typeOfLoanIdentifier2String: { [key: number]: string } = {};

    static ngFactory(serviceGen: IServiceGen,  $q: IQService,
        serviceTypeList: IPromise<{ Text: string, Value: TypeOfService }[]>,
        loanIdentifierTypeList: IPromise<{ Text: string, Value: TypeOfLoanIdentifier }[]>,
    ): typeof Vendor {
        Vendor.$q = $q;
        Vendor.service = serviceGen();
        serviceTypeList.then((xs) => {
            Vendor.typeOfService2String = xs.toDictionary(x => x.Value.toString(), x => x.Text);
        });
        loanIdentifierTypeList.then((xs) => {
            Vendor.typeOfLoanIdentifier2String = xs.toDictionary(x => x.Value.toString(), x => x.Text);
        });

        return Vendor;
    }
}
Vendor.ngFactory.$inject = ["serviceGen", "$q", serviceTypeListFactory.ngName!, loanIdentifierTypeListFactory.ngName!];
