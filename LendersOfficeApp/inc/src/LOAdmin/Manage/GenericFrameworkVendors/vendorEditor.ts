import { IQService, IPromise, IAngularEvent, IParseService, IDirective, IScope, IDirectiveLinkFn} from "angular";

import {Vendor, FormError} from "./Vendor";
import {TypeOfService, serviceTypeListFactory} from "./TypeOfService";
import {TypeOfLoanIdentifier, loanIdentifierTypeListFactory} from "./TypeOfLoanIdentifier";

import {formValidate                    } from "../../../ng-common/formValidate";
import {IDialog                         } from "../../../ng-common/dialog";
import {INg2VarAttrs                    } from "../../../ng-directives/INg2VarAttrs";

export class VendorEditorCtrl {
    private entry                    : Vendor;
    private serviceTypeList          : { Text: string, Value: TypeOfService }[];
    private loanIdentifierTypeList   : { Text: string, Value: TypeOfLoanIdentifier }[];

    private formError            : FormError|null;

    private $editModal           : JQuery;

    static $inject = ["$element", "$q", "dialog", serviceTypeListFactory.ngName, loanIdentifierTypeListFactory.ngName, "Vendor"];
    constructor($element: JQuery,
        private $q: IQService, private dialog: IDialog,
        serviceTypeList: IPromise<{ Text: string, Value: TypeOfService }[]>,
        loanIdentifierTypeList: IPromise<{ Text: string, Value: TypeOfLoanIdentifier }[]>,

    ) {

        this.entry          = new Vendor();
        this.formError      = null;
        this.$editModal     = $element.find(".modal"); console.assert(this.$editModal.length === 1);

        serviceTypeList.then(xs => this.serviceTypeList = xs);
        loanIdentifierTypeList.then(xs => this.loanIdentifierTypeList = xs);
    }

    show(vendor: Vendor):void {
        this.entry = (vendor == null) ? new Vendor() : new Vendor(vendor);
        this.formError = null;

        this.$editModal.modal("show");
    }
    save(event: IAngularEvent) {
        const $q = this.$q;

        const vendor = this.entry;

        if (!(formValidate(event.currentTarget as HTMLFormElement))) return false;

        { // Validate
            const formError = vendor.isValid();
            this.formError = formError;
            if (formError != null) return false;
        }

        vendor.save()
            .then((v) => {
                this.$editModal.modal("hide");
                this.onClose();
            }, (error: string) => {
                this.dialog.alert(`The following vendors failed to save: ${error}`);
                return $q.reject(false);
            });

        return false;
    }

    private onClose: () => void;

    static ngName = "VendorEditorCtrl";
}

import * as vendorEditorHtml from "./vendorEditor.html";

vendorEditor.ngName = "vendorEditor";
vendorEditor.$inject = ("$parse").split(" ");
export function vendorEditor($parse: IParseService): IDirective {
    return {
        restrict: "E",
        scope   : { onClose: "&" },
        link: ((scope: IScope, $e: JQuery, attrs: INg2VarAttrs, ctrl: VendorEditorCtrl) => {
            $parse(attrs.ng2Var).assign(scope.$parent, ctrl);
        }) as IDirectiveLinkFn,
        template        : vendorEditorHtml,
        controller      : VendorEditorCtrl,
        controllerAs    : "vm",
        bindToController: true,
    };
}
