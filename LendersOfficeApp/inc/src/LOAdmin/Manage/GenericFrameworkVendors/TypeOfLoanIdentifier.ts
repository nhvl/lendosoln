import "../../../utils/Array.orderBy";
import {IRootScopeService, IPromise} from "angular";
import {IService, IServiceGen} from "../../../ng-common/serviceGen";

export enum TypeOfLoanIdentifier {
    LoanNumber                           = 1 << 0,
    LoanReferenceNumber                  = 1 << 1,
    BothLoanNumberAndLoanReferenceNumber = LoanNumber | LoanReferenceNumber,
}

interface IModel { Text: string, Value: TypeOfLoanIdentifier }

loanIdentifierTypeListFactory.ngName = "loanIdentifierTypeList";
loanIdentifierTypeListFactory.$inject = ("serviceGen $rootScope").split(" ");
export function loanIdentifierTypeListFactory(serviceGen: IServiceGen): IPromise<IModel[]>{
    const service = serviceGen();

    return service<IModel[]>("GetLoanIdentifierTypeList")
        .then((xs: IModel[]) => xs.orderBy(o => o.Text));
}

typeOfLoanIdentifierFilter.ngName = "typeOfLoanIdentifier";
typeOfLoanIdentifierFilter.$inject = [loanIdentifierTypeListFactory.ngName, "$rootScope"];
export function typeOfLoanIdentifierFilter(list: IPromise<IModel[]>, $rootScope:IRootScopeService) {
    let TypeOfLoanIdentifier2String: { [k: number]: string } = {};
    list.then(xs => {
        TypeOfLoanIdentifier2String = xs.toDictionary(x => x.Value.toString(), x => x.Text);
        if (!$rootScope.$$phase) $rootScope.$apply();
    });

    return (s: TypeOfLoanIdentifier) => (TypeOfLoanIdentifier2String[s] || "loading&hellip;");
}
