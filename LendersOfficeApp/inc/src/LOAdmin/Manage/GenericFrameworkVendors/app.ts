/// <reference path="../../../dt-local/tsd.d.ts" />
import {IDirective, module} from "angular";

import "../../../utils/Array.toDictionary";

import {serviceGen           } from "../../../ng-common/serviceGen";
import {Dialog               } from "../../../ng-common/dialog";
import {selectWhenClicked    } from "../../../ng-directives/selectWhenClicked";
import {sortHandle, OrderSpec} from "../../../ng-directives/sortIcon";

import {Vendor} from "./Vendor";
import {serviceTypeListFactory, typeOfServiceFilter} from "./TypeOfService";
import {loanIdentifierTypeListFactory, typeOfLoanIdentifierFilter} from "./TypeOfLoanIdentifier";
import {VendorEditorCtrl, vendorEditor} from "./vendorEditor";

class VendorManagerCtrl {
    static ngName = "VendorManagerCtrl";

    vendors              : Vendor[];
    entrySort            : OrderSpec;

    static $inject = [Vendor.ngName];
    constructor() {
        this.entrySort      = new OrderSpec({ by:"ProviderID", desc:false});

        this.reload();
    }

    reload() {
        Vendor.get().then((vs: Vendor[]) => this.vendors = vs);
    }

    vendorEditor: VendorEditorCtrl;
    edit(vendor: Vendor) {
        this.vendorEditor.show(vendor);
        return false;
    }
}

import * as appHtml from "./app.html";
import "./app.css";

vendorManagerDirective.ngName = "vendorManager";
function vendorManagerDirective() : IDirective {
    return {
        restrict    : "E",
        scope       : true,
        controller  : VendorManagerCtrl,
        controllerAs: "vm",
        template    : appHtml,
    }
}

const app = module("app", [] as string[])
    .factory   ("serviceGen"                         , serviceGen            )
    .service   ("dialog"                             , Dialog                )
    .directive ("selectWhenClicked"                  , selectWhenClicked     )
    .directive ("sortHandle"                         , sortHandle            )

    .factory   (serviceTypeListFactory        .ngName!, serviceTypeListFactory)
    .filter    (typeOfServiceFilter           .ngName!, typeOfServiceFilter   )

    .factory   (loanIdentifierTypeListFactory .ngName!, loanIdentifierTypeListFactory)
    .filter    (typeOfLoanIdentifierFilter    .ngName!, typeOfLoanIdentifierFilter   )


    .factory   (Vendor                        .ngName, Vendor.ngFactory      )

    .directive(vendorEditor                   .ngName!, vendorEditor          )
    .directive(vendorManagerDirective         .ngName, vendorManagerDirective)
    ;
