import "../../../utils/Array.orderBy";
import {IRootScopeService, IPromise} from "angular";
import {IService, IServiceGen} from "../../../ng-common/serviceGen";

export enum TypeOfService {
    Multiple                    =  0,
    Appraisal                   =  1,
    AVM                         =  2,
    Compliance                  =  3,
    Credit                      =  4,
    BusinessCreditReport        =  5,
    CreditAnalyzer              =  6,
    CreditAssure                =  7,
    CreditSupplements           =  8,
    CriminalRecordReports       =  9,
    EvictionReports             = 10,
    Flood                       = 11,
    Fraud                       = 12,
    HMDA                        = 13,
    LDPAndGSAReports            = 14,
    MERSReport                  = 15,
    MortgageInsurance           = 16,
    MortgageParticipantsReport  = 17,
    SSA89                       = 18,
    Title                       = 19,
    VerificationOfDeposits      = 20,
    VerificationOfEmployment    = 21,
    TaxReturnVerification4506T  = 22,
    TrailingDocs                = 23,
    UndisclosedDebtNotification = 24,
    USPSCheck                   = 25,
    WhatIfSimulator             = 26,
    OCR                         = 27,
}

//typeOfService2StringFactory.$inject = ("serviceGen $rootScope").split(" ");
//export function typeOfService2StringFactory(serviceGen: IServiceGen, $rootScope: IRootScopeService) {
//    const service = serviceGen();
//    const typeOfService2String: {
//        [key: number]   : string,
//        serviceTypeList ?: { Text: string, Value: TypeOfService }[],
//    } = <string[]> [];

//    service("GetServiceTypeList")
//        .then((serviceTypeList: { Text: string, Value: TypeOfService }[]) => {
//            typeOfService2String.serviceTypeList = serviceTypeList.orderBy(o => o.Text);

//            serviceTypeList.forEach((p) => { typeOfService2String[p.Value] = p.Text; });

//            if (!$rootScope.$$phase) $rootScope.$apply();
//        });

//    return typeOfService2String;
//}

interface IModel {
    Text: string, Value: TypeOfService
}

serviceTypeListFactory.ngName = "serviceTypeList";
serviceTypeListFactory.$inject = ("serviceGen $rootScope").split(" ");
export function serviceTypeListFactory(serviceGen: IServiceGen): IPromise<IModel[]> {
    const service = serviceGen();

    return service<IModel[]>("GetServiceTypeList")
        .then((serviceTypeList: IModel[]) => serviceTypeList.orderBy(o => o.Text));
}

typeOfServiceFilter.$inject = [serviceTypeListFactory.ngName, "$rootScope"];
export function typeOfServiceFilter(serviceTypeList: IPromise<IModel[]>, $rootScope:IRootScopeService) {
    let typeOfService2String: { [k: number]: string } = {};
    serviceTypeList.then(xs => {
        typeOfService2String = xs.toDictionary(x => x.Value.toString(), x => x.Text);
        if (!$rootScope.$$phase) $rootScope.$apply();
    });

    return (s: TypeOfService) => (typeOfService2String[s] || "loading&hellip;");
}
