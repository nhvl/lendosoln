﻿import * as angular from "angular";
import "angular-messages";

import {serviceGen, IService} from "../../../ng-common/serviceGen";
import {formValidate} from "../../../ng-common/formValidate";
import {sortHandle, OrderSpec} from "../../../ng-directives/sortIcon";

const $ = jQuery;

class DocConfig {
    XPath: string;//id
    DefaultURL: string;
    AltURL: string;
    URLDependsOnFieldId: string;
    FieldId: string;

    constructor(d?: DocConfig) {
        if (d != null) $.extend(this, d);
        return this;
    }
    static getList(): ng.IPromise<DocConfig[]> {
        const service = DocConfig.service;
        return service<DocConfig[]>("Get")
            .then(
            vs => vs.map(v => new DocConfig(v)));
    }
    static Export(): ng.IPromise<DocConfig[]> {
        const service = DocConfig.service;
        const $q = DocConfig.$q;
        return service<DocConfig[]>("Export")
            .then((error) => (error == null) ? $q.when(null) : $q.reject(error));
    }
    static $q :ng.IQService;
    static service<T>(method: string, args?: {}): ng.IPromise<T> {
        console.error("Have not assgin [service] function");
        return null;
    }
}

DocConfigFactory.$inject = ["serviceGen", "$q"];
function DocConfigFactory(serviceGen:(url?: string) => IService, $q: ng.IQService): typeof DocConfig {
    DocConfig.service = <any> serviceGen();
    DocConfig.$q = $q;
    return DocConfig;
}

interface DocManagerScope extends ng.IScope{
}

class DocManager{
    Docs: DocConfig[];
    isLoading: boolean;
    isShow: boolean;//show more
    entrySort: OrderSpec;

    static $inject = ["$scope", "DocConfig"];
    constructor(private $scope: DocManagerScope) {
        this.isLoading = false;
        this.Docs = null;
        this.isShow = false;
        this.entrySort = new OrderSpec({ by: "XPath", desc: false });
        this.reload();
    }
    reload() {
        this.isLoading = true;
        DocConfig.getList()
            .then((xs) => {
                this.Docs = xs;
                this.isLoading = false;
            });
    }
    sort(by: string) {
        if (this.entrySort.by === by) this.entrySort.desc = !this.entrySort.desc;
        else { this.entrySort.by = by; this.entrySort.desc = false; }
    }
    show() {
        this.isShow = !this.isShow;
    }
    export() {
        DocConfig.Export().then(
            () => {},
            (error: DocConfig) => {
                alert("Error");
            });
    }
}

angular.module("app", ["ngMessages"])
    .factory("serviceGen", serviceGen)
    .factory("DocConfig", DocConfigFactory)
    .directive("sortHandle", sortHandle)
    .controller("DocManager", DocManager)
;
