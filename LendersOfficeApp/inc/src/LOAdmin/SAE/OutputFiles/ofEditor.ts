import { IScope, IAugmentedJQuery, IAngularEvent, IParseService, IDirective, IDirectiveLinkFn } from "angular"
import {formValidate} from "../../../ng-common/formValidate";
import {INg2VarAttrs} from "../../../ng-directives/INg2VarAttrs";
import {IDialog     } from "../../../ng-common/dialog";

import {OutputFile  } from "./OutputFile";

export interface IOfEditorScope extends IScope{
    editEntryForm:  {
        Url: {
            $error: {
                url: boolean,
            }
        },
    }
}

class BatchEditToogle {
    FileName            : boolean;
    ExportType          : boolean;
    Worksheet           : boolean;
    Enabled             : boolean;
    Dirty               : boolean;
    UpdateMonitorEmails : boolean;

    constructor() {
        this.FileName            = false;
        this.ExportType          = false;
        this.Worksheet           = false;
        this.Enabled             = false;
        this.Dirty               = false;
        this.UpdateMonitorEmails = false;
    }
}

export class OfEditorController {
    static ngName = "OfEditorController";

    private eEntry       : OutputFile; // editting vendor object
    private entries      : OutputFile[]; // batch editting entries
    private isBatchEdit  : boolean;
    private batToggle    : BatchEditToogle;
    private clientCode   : string;
    private $modal       : JQuery;

    static $inject = ["$scope", "$element", "dialog", OutputFile.ngName];
    constructor(private $scope: IOfEditorScope, $element:IAugmentedJQuery, private dialog: IDialog) {
        this.eEntry         = new OutputFile();
        this.isBatchEdit    = false;
        this.batToggle      = new BatchEditToogle();
        this.clientCode     = "";
        this.$modal         = $element.find(".modal"); console.assert(this.$modal.length === 1);
    }

    private onSaved:()=>void;
    private save($event: IAngularEvent) {
        if (!formValidate(<HTMLFormElement> $event.currentTarget)) {
            console.warn("Form Invalid");
            return;
        }

        this.resetError();
        if (this.isBatchEdit) {
            const inEntry = new OutputFile(this.eEntry);

            const isNothingSelect = Object.keys(this.batToggle).every((p: string) => !(<any> this.batToggle)[p]);
            if (isNothingSelect) {
                console.warn("Nothing select for Batch update");
                return;
            }

            Object.keys(this.batToggle).forEach((p: string) => {
                if (!((<any> this.batToggle)[p])) (<any>inEntry)[p] = null;
            });
            OutputFile.batchUpdate(this.entries, inEntry)
                .then(() => {
                        this.$modal.modal("hide");
                        this.onSaved();
                    },
                    (error: string) => { this.dialog.alert(error, "Update FAIL"); });
        } else {
            this.eEntry.update()
                .then(
                    () => {
                        this.$modal.modal("hide");
                        this.onSaved();
                    },
                    (error: string) => { this.dialog.alert(error, "Update FAIL"); });
        }
    }
    private resetError() {
        const editEntryForm = this.$scope.editEntryForm;
        //editEntryForm.Url.$error.url = false;
    }

    startEdit(entry?: OutputFile) { // null for add new
        this.resetError();
        this.isBatchEdit = false;
        this.eEntry = new OutputFile(entry);
        this.$modal.modal("show");
    }
    startBatchEdit(entries: OutputFile[]) { // null for add new
        this.entries = entries;

        this.resetError();
        this.isBatchEdit = true;
        this.eEntry = new OutputFile();
        this.batToggle = new BatchEditToogle();
        this.$modal.modal("show");
    }
}

import * as ofEditorHtml from "./ofEditor.html";

ofEditorDirective.ngName = "ofEditor";
ofEditorDirective.$inject = ["$parse"];
export function ofEditorDirective($parse: IParseService) : IDirective {
    return {
        restrict         : "E",
        scope            : { onSaved: "&" },
        template         : ofEditorHtml,
        controller       : OfEditorController,
        controllerAs     : "vm",
        bindToController : true,
        link             : ((scope, $e, attrs: INg2VarAttrs, ctrl: OfEditorController) => {
                                $parse(attrs.ng2Var).assign(scope.$parent, ctrl);
                            }) as IDirectiveLinkFn,
    };
}
