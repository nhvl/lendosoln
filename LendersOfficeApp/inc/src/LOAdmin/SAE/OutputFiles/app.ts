import * as angular from "angular";
import "angular-messages";

import {serviceGen,          } from "../../../ng-common/serviceGen";
import {Dialog, IDialog      } from "../../../ng-common/dialog";
import {selectWhenClicked    } from "../../../ng-directives/selectWhenClicked";
import {checkboxIndeterminate} from "../../../ng-directives/checkboxIndeterminate";
import {sortHandle, OrderSpec} from "../../../ng-directives/sortIcon";

import {SearchForm  } from "./SearchForm";
import {OutputFile  } from "./OutputFile";
import {OfEditorController, ofEditorDirective} from "./ofEditor";

import * as appHtml from "./app.html";
class OfManagerController {
    entries      : OutputFile[]|null;
    entrySort    : OrderSpec;

    isLoading    : boolean;

    searchForm   : SearchForm;
    yesNoOptions : { v: boolean|null, t: string }[];

    selectedPercent: number;

    ofEditor: OfEditorController;

    static $inject = ["dialog", OutputFile.ngName];
    constructor(private dialog: IDialog) {
        this.entries = null;
        this.entrySort = new OrderSpec({ by: "FileName", desc:false });

        this.isLoading = false;

        this.searchForm = new SearchForm();
        this.yesNoOptions = SearchForm.yesNoOptions;

        this.selectedPercent = 0;

        this.reload();
    }

    reload() {
        this.isLoading = true;
        OutputFile.get(this.searchForm)
            .then((xs) => {
                this.entries = xs;
                this.selectedPercent = 0;
            })
            .finally(() => this.isLoading = false);
    }
    delete(entry: OutputFile) {
        this.dialog.confirm(entry.deleteMessage(), "Confirm")
            .then((isConfirmed) => {
                if (!isConfirmed) return false;
                entry.delete().then(
                    () => { this.reload(); },
                    (error) => { this.dialog.alert(error, "Delete FAIL") });
                return true;
            });
    }
    edit(entry?: OutputFile) { // null for add new
        this.ofEditor.startEdit(entry);
    }
    batchEdit() {
        if (this.entries == null) return;
        this.ofEditor.startBatchEdit(this.entries.filter(e => e.isSelected));
    }

    onEntrySelect() {
        console.debug("event[onEntrySelect]");
        if (this.entries == null || this.entries.length < 1) return;

        const numberOfSelected = this.entries.reduce((c: number, e: OutputFile) => c + (e.isSelected ? 1 : 0), 0);
        this.selectedPercent = numberOfSelected / this.entries.length;
    }
    onSelectAll($event: ng.IAngularEvent) {
        if (this.entries == null) return;

        const isSelect = (($event.currentTarget as HTMLInputElement).checked);
        for (let e of this.entries) { e.isSelected = isSelect; }
        this.selectedPercent = isSelect ? 1 : 0;
    }
}


import "./style.css";

angular.module("app", ["ngMessages"])
    .factory  ("serviceGen"              , serviceGen           )
    .directive("selectWhenClicked"       , selectWhenClicked    )
    .directive("checkboxIndeterminate"   , checkboxIndeterminate)
    .service  ("dialog"                  , Dialog               )

    .directive ("sortHandle"             , sortHandle           )

    .factory   (OutputFile.ngName        , OutputFile.ngFactory )

    .directive (ofEditorDirective.ngName!, ofEditorDirective    )
    .component("ofManager", { controller: OfManagerController, controllerAs: "vm", template: appHtml })
;
