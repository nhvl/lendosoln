import { IQService, IPromise, } from "angular";
import {IServiceGen, IService } from "../../../ng-common/serviceGen";

import {SearchForm} from "./SearchForm";

let $q      : IQService;
let service : IService;

export class OutputFile { static ngName = "OutputFile";

    ID                  : number|null;
    FileName            : string ;
    ExportType          : string ;
    Worksheet           : string ;
    Enabled             : boolean;
    Dirty               : boolean;
    dUpdated            : Date|null;
    LastUlResult        : string ;
    ErrorCount          : number ;
    UpdateMonitorEmails : string ;

    isSelected          : boolean;

    constructor(d?: OutputFile) {
        this.ID                  =  null;
        this.FileName            =    "";
        this.ExportType          =    ""; // TODO: Rate Options
        this.Worksheet           =    "";
        this.Enabled             =  true;
        this.Dirty               = false;
        this.dUpdated            =  null;
        this.LastUlResult        =    "";
        this.ErrorCount          =     0;
        this.UpdateMonitorEmails =    "";

        this.isSelected          = false;

        if (d != null) Object.assign(this, d);
        return this;
    }

    update(): IPromise<void> {
        return service<string>("Update", { entry: this })
            .then((error) => (!error) ? null : $q.reject(error) as any)
            ;
    }

    delete(): IPromise<void> {
        return service<string>("Delete", { id: this.ID })
            .then((error) => !error ? null : $q.reject(error) as any);
    }

    deleteMessage() {
        return `Are you sure you want to delete entry ${this.ID}, ${this.FileName}?`;
    }

    static get(searchForm:SearchForm): IPromise<OutputFile[]> {
        return service<OutputFile[]>("Get", searchForm)
            .then((es: OutputFile[]) => es.map(e => new OutputFile(e)));
    }
    static batchUpdate(entries: OutputFile[], inEntry: OutputFile): IPromise<void> {
        return service<void>("BatchUpdate", { ids: entries.map(e => e.ID), inEntry })
            .then((error) => (!error) ? null : $q.reject(error) as any)
            ;
    }


    static ngFactory(serviceGen: IServiceGen, _$q: IQService): typeof OutputFile {
        service = serviceGen();
        $q      = _$q;
        return OutputFile;
    }
}
OutputFile.ngFactory.$inject = ["serviceGen", "$q"];
