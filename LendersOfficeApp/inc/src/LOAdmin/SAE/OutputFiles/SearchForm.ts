export class SearchForm {
    FileName            : string ;
    Enabled             : boolean|null;
    Dirty               : boolean|null;

    constructor() {
        this.FileName    = "";
        this.Enabled     = null;
        this.Dirty       = null;
    }

    static yesNoOptions = [
        { v:  null, t: "All" },
        { v:  true, t: "Yes" },
        { v: false, t: "No"  },
    ];
}
