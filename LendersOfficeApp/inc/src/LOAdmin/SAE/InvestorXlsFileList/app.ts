import {module} from "angular";

import {initialiseAgGridWithAngular1, GridOptions} from "ag-grid";
initialiseAgGridWithAngular1(angular);
import "ag-grid/dist/styles/ag-grid.css";
import "ag-grid/dist/styles/theme-fresh.css";

import "../panels-with-nav-tabs.css";

import {serviceGen,                   } from "../../../ng-common/serviceGen";
import {Dialog, IDialog               } from "../../../ng-common/dialog";
import {InvestorXlsFile               } from "./InvestorXlsFile";
import {IrFileEditorCtrl, irFileEditor} from "./irFileEditor";

let $filter: ng.IFilterService;
let $timeout: ng.ITimeoutService;

import * as appHtml from "./app.html";
class XlsListCtrl {
    private filter      : string;
    private gridOptions : GridOptions;
    private id2File     : Map<number, InvestorXlsFile>;

    static $inject = ["$filter", "dialog", "$element", "$timeout", InvestorXlsFile.ngName];
    constructor(_$filter: ng.IFilterService, private dialog: IDialog, private $element: ng.IAugmentedJQuery, private _$timeout:ng.ITimeoutService) {
        $filter = _$filter;
        $timeout = _$timeout;

        this.filter = "";
        this.id2File = new Map();

        const $filter_date = $filter("date");
        this.gridOptions = {
            columnDefs: [
                { width:40, headerName:"", field: "InvestorXlsFileId",
                    cellRenderer(p){
                        return `<a class="investor-xls-file-edit" data-id="${p.value}">edit</a>`;
                    }
                },
                { width:50, headerName:"", field: "InvestorXlsFileId",
                    cellRenderer(p){
                        return `<a class="investor-xls-file-delete" data-id="${p.value}">delete</a>`;
                    }
                },
                {headerName: "ID"                   , field: "InvestorXlsFileId"             ,                                   filter:"number", width: 60, },
                {headerName: "Rate Sheet Name"      , field: "InvestorXlsFileName"           ,                                   filter:"text"  ,            },
                {headerName: "Effective DateTime"   , children: [
                {headerName: "WorksheetName"        , field: "EffectiveDateTimeWorksheetName", filter:"text"  ,            },
                {headerName: "LandMarkText"         , field: "EffectiveDateTimeLandMarkText" , filter:"text"  , width:120, },
                {headerName: "Row Offset"           , field: "EffectiveDateTimeRowOffset"    , filter:"number", width:120, },
                {headerName: "Column Offset"        , field: "EffectiveDateTimeColumnOffset" , filter:"number", width:120, },
                ]},
                {headerName: "File Version DateTime", field: "FileVersionDateTime"           ,
                    //valueGetter(p){ return toShortDate(p.data) },
                    comparator: dateComparator,
                    cellRenderer(p){ return $filter_date(p.value, "short"); },
                },
				{headerName: "Exclude from InvPricingWarnings?", field: "ExcludeFromInvPricingWarnings" , filter:"text", width:300, },
            ],
            enableSorting: true,
            enableFilter: true,
            enableColResize: true,
        };

        $element.on("click", "a.investor-xls-file-edit", (event: Event) => {
            $timeout(() => {
                const id = Number(getXfId(<HTMLElement>event.currentTarget));
                const f = this.id2File.get(id);
                if (f == null) { return; }
                this.edit(f);
            });
        });

        $element.on("click", "a.investor-xls-file-delete", (event: Event) => {
            $timeout(() => {
                const id = Number(getXfId(<HTMLElement>event.currentTarget));
                const f = this.id2File.get(id);
                if (f == null) { return; }
                this.delete(f);
            });
        });

        function getXfId(e: HTMLElement) {
            return e.dataset ? e.dataset["id"] : e.getAttribute("data-id");
        }


        this.reload();
    }

    //#region File list
    reload() {
        InvestorXlsFile.get().then((xs:InvestorXlsFile[]) => {
            if (this.gridOptions.api == null) return;

            this.gridOptions.api.setRowData(xs);
            this.gridOptions.api.sizeColumnsToFit();

            this.id2File.clear();
            xs.forEach(x => { this.id2File.set(x.InvestorXlsFileId!, x); });

            this.onFilterChanged();
        });
    }

    delete(f: InvestorXlsFile) {
        const m = `Are you sure you want to delete Investor Xls File: ${f.InvestorXlsFileName}, id: ${f.InvestorXlsFileId}?`;
        this.dialog.confirm(m, "Confirm")
            .then((isConfirmed) => {
                if (!isConfirmed) return false;
                f.delete().then(
                    () => { this.reload(); },
                    (e: string) => this.dialog.alert(e, "Drop File Fail"));
                return true;
            });
    }

    fileEditor:IrFileEditorCtrl;
    edit(f: InvestorXlsFile) { // null for add new
        this.fileEditor.show(f);
    }
    //#endregion

    private onFilterChanged(){
        if (this.gridOptions.api == null) return;

        const fa: any = this.gridOptions.api.getFilterInstance("InvestorXlsFileName");
        fa.setType("contains");
        fa.setFilter(this.filter);
        this.gridOptions.api.onFilterChanged();
    }
}

import "./app.css";

function dateComparator(x:Date, y:Date) { return x.getTime() - y.getTime(); }

module("app", ["agGrid"])
    .factory   ("serviceGen"           , serviceGen            )
    .service   ("dialog"               , Dialog                )

    .factory   (InvestorXlsFile.ngName , InvestorXlsFile.ngFactory)

    .directive (irFileEditor.ngName!    , irFileEditor          )

    .component("xlsList", { controller: XlsListCtrl, controllerAs: "vm", template: appHtml})
;



