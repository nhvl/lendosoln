﻿import {IAngularEvent, IParseService, IDirective, IScope, IDirectiveLinkFn } from "angular";
import {InvestorXlsFile  } from "./InvestorXlsFile";
import {formValidate     } from "../../../ng-common/formValidate";
import {IDialog          } from "../../../ng-common/dialog";
import {INg2VarAttrs     } from "../../../ng-directives/INg2VarAttrs";

// Invertor Ratesheet File Editor
export class IrFileEditorCtrl {
    private entry: InvestorXlsFile;

    private $modal: JQuery;
    private onSaved: () => void;

    private deploymentTypes: string[];
    private investorXslFiles: InvestorXlsFile[];

    private isPending: boolean;

    static $inject = ["$element", "dialog", InvestorXlsFile.ngName];
    constructor($element: JQuery, private dialog: IDialog) {
        this.$modal = $element.find(".modal");

        this.isPending = false;

        console.assert(this.$modal.length === 1);
    }

    show(entry: InvestorXlsFile) {
        this.resetError();

        this.entry = new InvestorXlsFile(entry);
        this.isPending = false;

        this.$modal.modal("show");
    }

    private save($event: IAngularEvent) {
        if (this.isPending) return;

        if (!formValidate(<HTMLFormElement> $event.currentTarget)) {
            console.warn("Form Invalid");
            return;
        }

        this.resetError();

        this.isPending = true;
        this.entry.save().then(
            () => {
                this.$modal.modal("hide");
                this.onSaved();
            },
            (error: string) => {
                const message = typeof error === "string" ? error : JSON.stringify(error, null, 2);
                this.dialog.alert(message, "Save File Fail");
            })
            .finally(() => { this.isPending = false; });
    }

    private resetError() {
        // Nothing to implement
    }
}

import * as irFileEditorHtml from "./irFileEditor.html";

irFileEditor.ngName = "irFileEditor";
irFileEditor.$inject = ["$parse"];
export function irFileEditor($parse: IParseService) : IDirective {
    return {
        restrict        : "E",
        scope           : { onSaved: "&", },
        link            : ((scope: IScope, $e: JQuery, attrs: INg2VarAttrs, ctrl: IrFileEditorCtrl) => {
                                $parse(attrs.ng2Var).assign(scope.$parent, ctrl);
                            }) as IDirectiveLinkFn,
        template        : irFileEditorHtml,
        controller      : IrFileEditorCtrl,
        controllerAs    : "vm",
        bindToController: true,
    };
}
