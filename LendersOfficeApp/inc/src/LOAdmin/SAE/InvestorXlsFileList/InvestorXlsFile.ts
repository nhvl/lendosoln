import {IService   } from "../../../ng-common/serviceGen";
import { IPromise, IQService } from "angular";
import * as _ from "lodash";

let $q     : IQService;
let service: IService;

export class InvestorXlsFile { static ngName = "InvestorXlsFile";

    InvestorXlsFileId             : number|null;
    InvestorXlsFileName           : string;
    EffectiveDateTimeWorksheetName: string;
    EffectiveDateTimeLandMarkText : string;
    EffectiveDateTimeRowOffset    : number|null;
    EffectiveDateTimeColumnOffset : number|null;
    FileVersionDateTime           : Date|null;
	ExcludeFromInvPricingWarnings : boolean;

    get DisplayName() :string {
        return `${this.InvestorXlsFileName} [${this.InvestorXlsFileId}]`;
    }

    constructor(d?: InvestorXlsFile) {
        this.InvestorXlsFileId              = null;
        this.InvestorXlsFileName            = "";
        this.InvestorXlsFileName            = "";
        this.EffectiveDateTimeWorksheetName = "";
        this.EffectiveDateTimeLandMarkText  = "";
        this.EffectiveDateTimeRowOffset     = null;
        this.EffectiveDateTimeColumnOffset  = null;
        this.FileVersionDateTime            = null;
		this.ExcludeFromInvPricingWarnings  = false;

        if (d != null) _.assign(this, d);
    }

    save(): ng.IPromise<void> {
        return service<string>("Save", { file: this })
            .then((error: string) => (!error) ? null : $q.reject(error) as any)
            ;
    }

    delete(): ng.IPromise<void> {
        return service<string>("Delete", { fileId: this.InvestorXlsFileId })
            .then((error: string) => (!error) ? null : $q.reject(error) as any);
    }

    static get() {
        return service<InvestorXlsFile[]>("Get")
            .then((es: InvestorXlsFile[]) => es.map(e => new InvestorXlsFile(e)));
    }
    static getById(fileId: number): IPromise<InvestorXlsFile> {
        return service<InvestorXlsFile>("GetById", {fileId})
            .then((f: InvestorXlsFile) => new InvestorXlsFile(f));
    }

    static ngFactory(serviceGen: (url?: string) => IService, _$q: ng.IQService): typeof InvestorXlsFile
    {
        service = serviceGen("InvestorXlsFileList.aspx");
        console.assert(/LOAdmin\/SAE\/.*.aspx/.test(window.location.pathname), "service may be mis-config");
        $q = _$q;
        return InvestorXlsFile;
    }
}

InvestorXlsFile.ngFactory.$inject = ["serviceGen", "$q"];
