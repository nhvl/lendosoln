import * as angular from "angular";
import "angular-messages";

import {serviceGen,          } from "../../../ng-common/serviceGen";
import {Dialog, IDialog      } from "../../../ng-common/dialog";
import {selectWhenClicked    } from "../../../ng-directives/selectWhenClicked";
import {checkboxIndeterminate} from "../../../ng-directives/checkboxIndeterminate";
import {sortHandle, OrderSpec} from "../../../ng-directives/sortIcon";

import {EdlEntry   } from "./EdlEntry";
import {SearchForm} from "./SearchForm";
import {EdlEditorController, edlEditorDirective} from "./edlEditor";
let $timeout: ng.ITimeoutService;

class EdlManager {
    entries      : EdlEntry[]|null;
    entrySort    : OrderSpec;

    isLoading    : boolean;

    searchForm   : SearchForm;
    activeOptions: { v: boolean|null, t: string }[];

    selectedPercent: number;

    edlEditor: EdlEditorController;

    static $inject = ["dialog", "$timeout", EdlEntry.ngName];
    constructor(private dialog: IDialog, private _$timeout:ng.ITimeoutService) {
        $timeout = _$timeout;
        this.entries = null;
        this.entrySort = new OrderSpec({ by: "TargetFileName", desc:false });

        this.isLoading = false;

        this.searchForm = new SearchForm();
        this.activeOptions = SearchForm.activeOptions;

        this.selectedPercent = 0;
        $(window).resize(() => { this.resizeTable() });
        this.reload();
    }

    resizeTable():void{
        $('.table-responsive .table tbody').height(window.innerHeight - 380);
        var width = $('.table-responsive .table tbody').width() as number;
        $('.col-id'            ).css("width",width * 0.03);
        $('.col-status'        ).css("width",width * 0.04);
        $('.col-file-name'     ).css("width",width * 0.12);
        $('.col-last-updated'  ).css("width",width * 0.075);
        $('.col-sender'        ).css("width",width * 0.07);
        $('.col-subject'       ).css("width",width * 0.07);
        $('.col-mail-file-name').css("width",width * 0.07);
        $('.col-body'          ).css("width",width * 0.07);
        $('.col-description'   ).css("width",width * 0.07);
    }
    
    autocomplete():void {
        EdlEntry.bodyOptions = Array.from(new Set(this.entries!.map(e => e.Body)));
        EdlEntry.mailFileNameOptions = Array.from(new Set(this.entries!.map(e => e.MailFileName)));
        EdlEntry.descriptionOptions = Array.from(new Set(this.entries!.map(e => e.Description)));
        
        EdlEntry.senderOptions = Array.from(new Set(this.entries!.map(e => e.Sender)));
        (<any>$('[ng-model="vm.searchForm.sender"]')).autocomplete({
            source: EdlEntry.senderOptions
        });

        EdlEntry.targetFileNameOptions = Array.from(new Set(this.entries!.map(e => e.TargetFileName)));
        (<any>$('[ng-model="vm.searchForm.targetFileName"]')).autocomplete({
            source: EdlEntry.targetFileNameOptions
        });

        EdlEntry.botTypeOptions = Array.from(new Set(this.entries!.map(e => e.BotType)));
        (<any>$('[ng-model="vm.searchForm.botType"]')).autocomplete({
            source: EdlEntry.botTypeOptions
        });

        EdlEntry.subjectOptions = Array.from(new Set(([] as string[]).concat(...(this.entries!.map(e => e.subjects())))));
        (<any>$('[ng-model="vm.searchForm.subject"]')).autocomplete({
            source: EdlEntry.subjectOptions
        });
    }
    reload() {
        this.isLoading = true;
        EdlEntry.get(this.searchForm)
            .then(([error, entries]) => {
                if(error != null){
                    alert(error);
                    this.entries = [];
                    return;
                }
                this.entries = entries;
                this.selectedPercent = 0;
                $timeout(() => {
                    this.autocomplete();
                    this.resizeTable();
                });
            })
            .finally(() => this.isLoading = false);
    }
    delete(entry: EdlEntry) {
        this.dialog.confirm(entry.deleteMessage(), "Confirm")
            .then((isConfirmed) => {
                if (!isConfirmed) return false;
                entry.delete().then(([error]) => {
                    if(error != null){
                        alert(error);
                    }
                    this.reload(); 
                });
                return true;
            });
    }
    batchDelete(){
        if (this.entries == null) return;
        //this.entries.filter(e => e.isSelected)
        //TODO: Case 244406
        this.dialog.confirm(EdlEntry.deleteBatchMessage(), "Confirm")
            .then((isConfirmed) => {
                    if (!isConfirmed) return false;
                    EdlEntry.batchDelete(this.entries!.filter(e => e.isSelected)).then(([error]) => {
                        if(error != null){
                            alert(error);
                        }
                        this.reload(); 
                    });
                    return true;
                });
    }
    edit(entry?: EdlEntry) { // null for add new
        this.edlEditor.startEdit(entry);
    }
    batchEdit() {
        if (this.entries == null) return;
        this.edlEditor.startBatchEdit(this.entries.filter(e => e.isSelected));
    }

    onEntrySelect() {
        console.debug("event[onEntrySelect]");
        if (this.entries == null || this.entries.length < 1) return;

        const numberOfSelected = this.entries.reduce((c: number, e: EdlEntry) => c + (e.isSelected ? 1 : 0), 0);
        this.selectedPercent = numberOfSelected / this.entries.length;
    }
    onSelectAll($event: ng.IAngularEvent) {
        if (this.entries == null) return;

        const isSelect = (($event.currentTarget as HTMLInputElement).checked);
        for (let e of this.entries) e.isSelected = isSelect;
        this.selectedPercent = isSelect ? 1 : 0;
    }
}

import "./app.css";
import * as template from "./app.html";

angular.module("app", ["ngMessages"])
    .factory  ("serviceGen"              , serviceGen)
    .directive("selectWhenClicked"       , selectWhenClicked)
    .directive("checkboxIndeterminate"   , checkboxIndeterminate)
    .service  ("dialog"                  , Dialog)

    .directive ("sortHandle"             , sortHandle)

    .factory   (EdlEntry.ngName           , EdlEntry.ngFactory)

    .directive (edlEditorDirective.ngName! , edlEditorDirective)

    .component("emailDownloadManager", { controller: EdlManager, controllerAs: "vm", template })
;
