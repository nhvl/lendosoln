import { IParseService, IDirective, IScope, IAngularEvent, IDirectiveLinkFn} from "angular";
import {formValidate} from "../../../ng-common/formValidate";
import {INg2VarAttrs} from "../../../ng-directives/INg2VarAttrs";

import {EdlEntry     } from "./EdlEntry";

export interface EdlEditorScope extends IScope{
    editEntryForm:  {
        Url: {
            $error: {
                url: boolean,
            }
        },
    }
}

class BatchEditToogle {
    Sender          : boolean;
    Body            : boolean;
    MailFileName    : boolean;
    TargetFileName  : boolean;
    Active          : boolean;
    BotType         : boolean;
    Subject         : boolean;
    Description     : boolean;
    Note            : boolean;
    [field:string]:boolean;

    constructor() {
        this.Sender         = false;
        this.Body           = false;
        this.MailFileName   = false;
        this.TargetFileName = false;
        this.Active         = false;
        this.BotType        = false;
        this.Subject        = false;
        this.Description    = false;
        this.Note           = false;
    }
}

export class EdlEditorController {
    private eEntry       : EdlEntry; // editting vendor object
    private entries      : EdlEntry[]; // batch editting entries
    private isBatchEdit  : boolean;
    private batToggle    : BatchEditToogle;
    private clientCode   : string;
    private $modal       : JQuery;
    static $inject = ("$scope $element EdlEntry").split(" ");
    constructor(private $scope: EdlEditorScope, $element:JQLite) {
        this.eEntry = new EdlEntry();
        this.isBatchEdit = false;
        this.batToggle = new BatchEditToogle();
        this.clientCode = "";
        this.$modal = $element.find(".modal"); console.assert(this.$modal.length === 1);
    }
    autocomplete():void {
        (<any>$('[ng-model="vm.eEntry.Sender"]')).autocomplete({
            appendTo:"#editEntryForm",
            source: EdlEntry.senderOptions
        });
        (<any>$('[ng-model="vm.eEntry.Body"]')).autocomplete({
            appendTo:"#editEntryForm",
            source: EdlEntry.bodyOptions
        });
        (<any>$('[ng-model="vm.eEntry.MailFileName"]')).autocomplete({
            appendTo:"#editEntryForm",
            source: EdlEntry.mailFileNameOptions
        });
        (<any>$('[ng-model="vm.eEntry.Description"]')).autocomplete({
            appendTo:"#editEntryForm",
            source: EdlEntry.descriptionOptions
        });
        (<any>$('[ng-model="vm.eEntry.TargetFileName"]')).autocomplete({
            appendTo:"#editEntryForm",
            source: EdlEntry.targetFileNameOptions
        });
        (<any>$('[ng-model="vm.eEntry.BotType"]')).autocomplete({
            appendTo:"#editEntryForm",
            source: EdlEntry.botTypeOptions
        });
        (<any>$('[ng-model^="vm.eEntry.Subject"]')).autocomplete({
            appendTo:"#editEntryForm",
            source: EdlEntry.subjectOptions
        });
    }
    private onSaved:()=>void;
    private save($event: IAngularEvent) {
        const editEntryForm = this.$scope.editEntryForm;

        if (!formValidate($event.currentTarget as HTMLFormElement)) {
            console.warn("Form Invalid");
            return false;
        }

        this.resetError();
        if (this.isBatchEdit) {
            const isNothingSelect = Object.keys(this.batToggle).every((p: string) => !this.batToggle[p]);
            if (isNothingSelect) {
                console.warn("Nothing select for Batch update");
                return false;
            }
            this.entries = this.entries.map(e =>{
                if (this.batToggle.Sender        ) e.Sender        = this.eEntry.Sender        ;
                if (this.batToggle.Body          ) e.Body          = this.eEntry.Body          ;
                if (this.batToggle.MailFileName  ) e.MailFileName  = this.eEntry.MailFileName  ;
                if (this.batToggle.TargetFileName) e.TargetFileName= this.eEntry.TargetFileName;
                if (this.batToggle.Active        ) e.Active        = this.eEntry.Active        ;
                if (this.batToggle.BotType       ) e.BotType       = this.eEntry.BotType       ;
                if (this.batToggle.Description   ) e.Description   = this.eEntry.Description   ;
                if (this.batToggle.Note          ) e.Note          = this.eEntry.Note          ;
                if (this.batToggle.Subject       ) e.Subject       = this.eEntry.Subject       ;
                if (this.batToggle.Subject       ) e.Subject1      = this.eEntry.Subject1      ;
                if (this.batToggle.Subject       ) e.Subject2      = this.eEntry.Subject2      ;
                if (this.batToggle.Subject       ) e.Subject3      = this.eEntry.Subject3      ;
                if (this.batToggle.Subject       ) e.Subject4      = this.eEntry.Subject4      ;
                if (this.batToggle.Subject       ) e.Subject5      = this.eEntry.Subject5      ;
                if (this.batToggle.Subject       ) e.Subject6      = this.eEntry.Subject6      ;
                if (this.batToggle.Subject       ) e.Subject7      = this.eEntry.Subject7      ;
                if (this.batToggle.Subject       ) e.Subject8      = this.eEntry.Subject8      ;
                if (this.batToggle.Subject       ) e.Subject9      = this.eEntry.Subject9      ;
                return e;
            });
        }
        EdlEntry.save(this.isBatchEdit ? this.entries : [this.eEntry]).then(([error])=>{
            if (error) {
                alert(error);
            } else {
                this.$modal.modal("hide");
                this.onSaved();
            }
        });

        return false;
    }
    private resetError() {
        const editEntryForm = this.$scope.editEntryForm;
        //editEntryForm.Url.$error.url = false;
    }

    startEdit(entry?: EdlEntry) { // null for add new
        this.resetError();
        this.isBatchEdit = false;
        this.eEntry = new EdlEntry(entry);
        this.$modal.modal("show");
        this.autocomplete();
    }
    startBatchEdit(entries: EdlEntry[]) { // null for add new
        this.entries = entries;

        this.resetError();
        this.isBatchEdit = true;
        this.eEntry = new EdlEntry();
        this.batToggle = new BatchEditToogle();
        this.$modal.modal("show");
        this.autocomplete();
    }
}

import * as edlEditorHtml from "./edlEditor.html";

edlEditorDirective.ngName = "edlEditor";
edlEditorDirective.$inject = ("$parse").split(" ");
export function edlEditorDirective($parse: IParseService): IDirective {
    return {
        restrict         : "E",
        scope            : { onSaved: "&" },
        template         : edlEditorHtml,
        controller       : EdlEditorController,
        controllerAs     : "vm",
        bindToController : true,
        link             : ((scope, $e, attrs: INg2VarAttrs, ctrl: EdlEditorController) => {
                                $parse(attrs.ng2Var).assign(scope.$parent, ctrl);
                            }) as IDirectiveLinkFn,
    };
}
