export class SearchForm {
    active        : boolean|null;
    sender        : string;
    targetFileName: string;
    botType       : string;
    subject       : string;

    constructor() {
        this.active         = null;
        this.sender         = "";
        this.targetFileName = "";
        this.botType        = "";
        this.subject        = "";
    }

    static activeOptions = [
        { v:           null, t: "All"           },
        { v:           true, t: "Active Only"   },
        { v:          false, t: "Inactive Only" },
    ];
}
