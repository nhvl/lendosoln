import { IQService, IPromise} from "angular";
import {IServiceGen, IService } from "../../../ng-common/serviceGen";

import {SearchForm} from "./SearchForm";

let $q: IQService;
let service: <T>(method: string, args ?: {}, config ?: {}) => IPromise<T>;

export class EdlEntry {
    static ngName = "EdlEntry";

    ID            : number|null; // int
    Sender        : string;
    Body          : string  ;
    MailFileName  : string  ;
    TargetFileName: string  ;
    Active        : boolean ;
    BotType       : string  ;
    Description   : string ;
    Note          : string  ;
    Subject       : string  ;
    Subject1      : string  ;
    Subject2      : string  ;
    Subject3      : string  ;
    Subject4      : string  ;
    Subject5      : string  ;
    Subject6      : string  ;
    Subject7      : string  ;
    Subject8      : string  ;
    Subject9      : string  ;
    dLastUpdated  : Date|null;

    isSelected    : boolean ;

    constructor(d?: EdlEntry) {
        this.ID        = null;
    this.Sender        = "";
    this.Body          = "";
    this.MailFileName  = "";
    this.TargetFileName= "";
    this.Active        = true;
    this.BotType       = "";
    this.Description   = "";
    this.Note          = "";
    this.Subject       = "";
    this.Subject1      = "";
    this.Subject2      = "";
    this.Subject3      = "";
    this.Subject4      = "";
    this.Subject5      = "";
    this.Subject6      = "";
    this.Subject7      = "";
    this.Subject8      = "";
    this.Subject9      = "";
    this.dLastUpdated  = null;

        this.isSelected             = false;

        if (d != null) Object.assign(this, d);
        return this;
    }
    static bodyOptions: string[];
    static senderOptions: string[];
    static descriptionOptions: string[];
    static mailFileNameOptions: string[];
    static targetFileNameOptions:string[];
    static botTypeOptions:string[];
    static subjectOptions:string[];

    subjects():string[] {
        return [
            this.Subject ,
            this.Subject1,
            this.Subject2,
            this.Subject3,
            this.Subject4,
            this.Subject5,
            this.Subject6,
            this.Subject7,
            this.Subject8,
            this.Subject9,
        ].filter(s => !!s);
    }

    delete(): IPromise<[string|null, number|null]> {
        return EdlEntry.batchDelete([this]);
    }

    deleteMessage() {
        return `Are you sure you want to delete entry ${this.ID}, ${this.Description}?`;
    }
    static deleteBatchMessage(){
        return `Are you sure you want to delete selected entries?`;
    }

    static get(searchForm:SearchForm): IPromise<[string|null, EdlEntry[]|null]> {
        return service<[string|null, EdlEntry[]|null]>("Get", {})
            .then((es: [string|null, EdlEntry[]|null]) => 
                [es[0],(es[1] != null ? es[1]!.map(e => new EdlEntry(e)).filter(ex=>EdlEntry.isMatch(ex,searchForm)) : null)] as [string|null, EdlEntry[]|null]);
    }
    private static isMatch(e:EdlEntry,searchForm:SearchForm): boolean{
        if(searchForm.active != null && searchForm.active != e.Active) return false;
        if(searchForm.sender.trim() != "" && !e.Sender.toLowerCase().trim().includes(searchForm.sender.toLowerCase().trim())) return false;
        if(searchForm.targetFileName.trim() != "" && !e.TargetFileName.toLowerCase().trim().includes(searchForm.targetFileName.toLowerCase().trim())) return false;
        if(searchForm.botType.trim() != "" && !e.BotType.toLowerCase().trim().includes(searchForm.botType.toLowerCase().trim())) return false;
        if(searchForm.subject.trim() != "" 
            && !e.Subject.toLowerCase().trim().includes(searchForm.subject.toLowerCase().trim())
            && !e.Subject1.toLowerCase().trim().includes(searchForm.subject.toLowerCase().trim())
            && !e.Subject2.toLowerCase().trim().includes(searchForm.subject.toLowerCase().trim())
            && !e.Subject3.toLowerCase().trim().includes(searchForm.subject.toLowerCase().trim())
            && !e.Subject4.toLowerCase().trim().includes(searchForm.subject.toLowerCase().trim())
            && !e.Subject5.toLowerCase().trim().includes(searchForm.subject.toLowerCase().trim())
            && !e.Subject6.toLowerCase().trim().includes(searchForm.subject.toLowerCase().trim())
            && !e.Subject7.toLowerCase().trim().includes(searchForm.subject.toLowerCase().trim())
            && !e.Subject8.toLowerCase().trim().includes(searchForm.subject.toLowerCase().trim())
            && !e.Subject9.toLowerCase().trim().includes(searchForm.subject.toLowerCase().trim())
           ) 
            return false;

        return true;
    }

    static save(entries: EdlEntry[]):IPromise<[string|null, void|null]> {
        return service<[string|null, void|null]>("Save", { entries:entries});
    }

    static batchDelete(entries: EdlEntry[]): IPromise<[string|null, number|null]> {
        return service<[string|null, number|null]>("Delete", { ids: entries.map(e => e.ID)});
    }

    static ngFactory(serviceGen: IServiceGen, _$q: IQService): typeof EdlEntry {
        service = serviceGen() as any;
        $q = _$q;
        return EdlEntry;
    }
}
EdlEntry.ngFactory.$inject = ["serviceGen", "$q"];
