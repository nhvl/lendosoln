﻿
import { IDirective, IAttributes, IParseService, IScope, IAugmentedJQuery, IAngularEvent, IDirectiveLinkFn} from "angular";
import {FileVersion } from "./FileVersion";
import {formValidate} from "../../../ng-common/formValidate";
import {IDialog     } from "../../../ng-common/dialog";

export class FvEditorCtrl {
    private entry: FileVersion|null;
    private $modal: JQuery;
    private onClose: () => void;

    static $inject = ("$element dialog FileVersion").split(" ");
    constructor($element: IAugmentedJQuery, private dialog: IDialog) {
        this.entry = null;
        this.$modal = $element.find(".modal");
    }

    show(entry: FileVersion) {
        this.entry = entry;
        this.$modal.modal("show");
    }

    private saveFv($event: IAngularEvent) {
        if (this.entry == null) return;

        if (!formValidate($event.currentTarget as HTMLFormElement)) {
            console.warn("Form Invalid");
            return;
        }

        this.entry.save().then(
            () => {
                this.$modal.modal("hide");
                this.onClose();
            },
            (error: string) => {
                this.dialog.alert(error, "Save File Version Fail");
            });
    }
}

interface IFvEdittorAttrs extends IAttributes {
    ng2Var : string;
}

import * as fvEditorHtml from "./fvEditor.html";

fvEditor.ngName = "fvEditor";
fvEditor.$inject = ("$parse").split(" ");
export function fvEditor($parse:IParseService): IDirective {
    return {
        restrict: "E",
        scope: {
            onClose: "&",
        },
        link: ((scope: IScope, $e: JQuery, attrs: IFvEdittorAttrs, ctrl: any) => {
            $parse(attrs.ng2Var).assign(scope.$parent, ctrl);
        }) as IDirectiveLinkFn,
        template: fvEditorHtml,
        controller: FvEditorCtrl,
        controllerAs: "vm",
        bindToController: true,
    };
}
