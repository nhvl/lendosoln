﻿import {IDirective, IAttributes, IParseService, IScope, IAugmentedJQuery, IAngularEvent, ITimeoutService, IDirectiveLinkFn} from "angular";

import {RsFile            } from "./RsFile";
import {FileVersion       } from "./FileVersion";
import {IDialog           } from "../../../ng-common/dialog";
import {FvEditorCtrl      } from "./fvEditor";
import {sortHandle, OrderSpec} from "../../../ng-directives/sortIcon";

export class FvListCtrl { static ngName = "FvListCtrl";
    private entry: RsFile;
    private $modal: JQuery;
    private onFvChange: () => void;

    private fvSort: OrderSpec;

    static $inject = ("$element $timeout dialog FileVersion").split(" ");
    constructor($element: IAugmentedJQuery, private $timeout: ITimeoutService, private dialog: IDialog) {
        this.entry = null as any;
        this.$modal = $element.find(">div.modal");

        this.fvSort = new OrderSpec({ by: "LpeAcceptableRsFileId", desc: false });
    }

    show(entry: RsFile) {
        this.entry = entry;
        entry.getFileVersionAsync().then(() => {
            this.$modal.modal("show");
        });
    }

    deleteFv(fv: FileVersion) {
        const m = `Are you sure you want to delete Acceptable Ratesheet File Version: ${fv.LpeAcceptableRsFileId}, version: ${fv.VersionNumber}?`;
        this.dialog.confirm(m, "Confirm?")
            .then((isConfirmed) => {
                if (!isConfirmed) return false;
                fv.delete().then(
                    () => { this.show(this.entry); },
                    (e: string) => this.dialog.alert(e, "Drop File Version Fail"));
                return true;
            });
    }

    toggleExpiration(fv: FileVersion, event: IAngularEvent){
        fv.updateExpiration(fv.IsExpirationIssuedByInvestor).then(() => {
            this.entry.calcExpiration();
            this.onFvChange();
        });
        return false;
    }

    fvEditor: FvEditorCtrl;
    editFv(fv?: FileVersion) {
        this.fvEditor.show(new FileVersion(fv || { LpeAcceptableRsFileId: this.entry.LpeAcceptableRsFileId } as any));
    }
    onFvEditComplete() {
        if (this.entry == null) return;
        this.entry.getFileVersionAsync().then(() => this.onFvChange());
    }
}

interface IFvEdittorAttrs extends IAttributes {
    ng2Var: string;
}

import * as fvListHtml from "./fvList.html";

fvList.ngName = "fvList";
fvList.$inject = ["$parse"];
export function fvList($parse: IParseService): IDirective {
    return {
        restrict         : "E",
        scope            : {
            onFvChange   : "&",
        },
        link             : ((scope: IScope, $e: JQuery, attrs: IFvEdittorAttrs, ctrl: FvListCtrl) => {
                                $parse(attrs.ng2Var).assign(scope.$parent, ctrl);
                            }) as IDirectiveLinkFn,
        template        : fvListHtml,
        controller      : FvListCtrl,
        controllerAs    : "vm",
        bindToController: true,
    };
}
