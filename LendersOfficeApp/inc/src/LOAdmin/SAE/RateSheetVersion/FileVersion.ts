import {IService } from "../../../ng-common/serviceGen";

let $q :ng.IQService;
let service: IService;

export interface IFileVersion {
    LpeAcceptableRsFileId        : string ;
    VersionNumber                : number|null;
    FirstEffectiveDateTime       : Date|null;
    LatestEffectiveDateTime      : Date|null;
    IsExpirationIssuedByInvestor : boolean;
    EntryCreatedD                : Date|null;
    ModifiedD                    : Date|null;
}

export class FileVersion implements IFileVersion {
    LpeAcceptableRsFileId        : string ;
    VersionNumber                : number;
    FirstEffectiveDateTime       : Date|null;
    LatestEffectiveDateTime      : Date|null;
    IsExpirationIssuedByInvestor : boolean;
    EntryCreatedD                : Date|null;
    ModifiedD                    : Date|null;

    constructor(d?: IFileVersion){
        this.LpeAcceptableRsFileId        = ""  ;
        this.VersionNumber                = -1;
        this.FirstEffectiveDateTime       = null;
        this.LatestEffectiveDateTime      = null;
        this.IsExpirationIssuedByInvestor = false;
        this.EntryCreatedD                = null;
        this.ModifiedD                    = null;

        if (d != null) Object.assign(this, d);
    }

    save(): ng.IPromise<void> {
        return service<string>("SaveFileVersion", { fileVersion: this })
            .then((error: string) => (!error) ? $q.when() : $q.reject(error))
            ;
    }

    delete(): ng.IPromise<void> {

        return service<string>("DeleteFileVersion", { fileId: this.LpeAcceptableRsFileId, version:this.VersionNumber })
            .then((error: string) => (!error) ? $q.when() : $q.reject(error));
    }

    updateExpiration(IsExpirationIssuedByInvestor:boolean){
        return service<string>("SaveFileVersion", { fileVersion: Object.assign({}, this, {IsExpirationIssuedByInvestor}) })
            .then((error: string) => {
                if (!!error) {
                    return $q.reject(error);
                }
                this.IsExpirationIssuedByInvestor = IsExpirationIssuedByInvestor;
                return null;
            });
        ;
    }

    static get(fileId:string): ng.IPromise<FileVersion[]> {
        return service<IFileVersion[]>("GetFileVersions", { fileId })
            .then((es: IFileVersion[]) => es.map(e => new FileVersion(e)));
    }


}

fileVersionFactory.$inject = ["serviceGen", "$q"];
export function fileVersionFactory(serviceGen: (url?: string) => IService, _$q: ng.IQService): typeof FileVersion {
    service = serviceGen() as any;
    $q = _$q;
    return FileVersion;
}
