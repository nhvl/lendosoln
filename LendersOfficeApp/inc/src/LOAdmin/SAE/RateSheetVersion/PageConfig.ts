﻿import {InvestorXlsFile} from "../InvestorXlsFileList/InvestorXlsFile";
import {IService       } from "../../../ng-common/serviceGen";
import {RsFile         } from "./RsFile";

let service: IService

export class PageConfig {
    InvestorXslFiles     : InvestorXlsFile[];
    DeploymentTypes      : string[];
    DefaultDeploymentType: string;
    defaultFile: RsFile;

    constructor(d ?: PageConfig) {
        if (d != null) {
            this.InvestorXslFiles = d.InvestorXslFiles.map(e => new InvestorXlsFile(e));
            this.DeploymentTypes = d.DeploymentTypes;
            this.defaultFile = new RsFile({
                DeploymentType          : d.DefaultDeploymentType,
                InvestorXlsFileId       : this.InvestorXslFiles[0].InvestorXlsFileId,
                IsBothRsMapAndBotWorking: true,
            });
        }
    }
};

pageConfigFactory.$inject = ("serviceGen InvestorXlsFile RsFile").split(" ");
export function pageConfigFactory(serviceGen: (url?: string) => IService) : ng.IPromise<PageConfig> {
    service = serviceGen();
    return service<PageConfig>("GetConfig")
        .then((o: PageConfig) => o = new PageConfig(o));
}