﻿/// <reference path="../../../dt-local/tsd.d.ts" />

import "../../../utils/Array.toDictionary";

import * as angular from "angular";

import {GridOptions, ColDef, initialiseAgGridWithAngular1} from "ag-grid";
import "ag-grid/dist/styles/ag-grid.css";
import "ag-grid/dist/styles/theme-fresh.css";
initialiseAgGridWithAngular1(angular);

import {serviceGen,                     } from "../../../ng-common/serviceGen";
import {Dialog, IDialog                 } from "../../../ng-common/dialog";
import {selectWhenClicked               } from "../../../ng-directives/selectWhenClicked";
import {checkboxIndeterminate           } from "../../../ng-directives/checkboxIndeterminate";
import {sortHandle, OrderSpec           } from "../../../ng-directives/sortIcon";
import {FvListCtrl, fvList              } from "./fvList";
import {fvEditor                        } from "./fvEditor";
import {FileEditorCtrl, fileEditor      } from "./fileEditor";
import {pageConfigFactory               } from "./pageConfig";
import {fileVersionFactory              } from "./FileVersion";
import {RsFile, rsFileFactory           } from "./RsFile";
import {InvestorXlsFile                 } from "../InvestorXlsFileList/InvestorXlsFile";
import {IrFileEditorCtrl, irFileEditor  } from "../InvestorXlsFileList/irFileEditor";

declare const importError: string;

class SearchSpec {
    LpeAcceptableRsFileId: string;

    constructor() {
        this.LpeAcceptableRsFileId = "";
    }
}

interface ICellRendererParam {
    data: RsFile,
    api: GridApi,
    colDef: ColDef,
    column: Column,
    value: any,
    valueFormatted: any,
}

class SelectAllHeaderComponent {
    init() { }
    getGui() {
        var div = document.createElement('div');
        div.innerHTML = `<input type="checkbox" class="rs-file-select-all">`;
        return div.firstChild;
    }
}

class DlManager {
    entries        : RsFile[]|null; private setEntries(entries: RsFile[]|null) { this.entries = entries; this.filterEntries(); }
    filteredEntries: RsFile[]|null; private filterEntries() { this.filteredEntries = !this.entries || !this.searchSpec.LpeAcceptableRsFileId ? this.entries : this.entries.filter(e => e.LpeAcceptableRsFileId.toLowerCase().includes(this.searchSpec.LpeAcceptableRsFileId.toLowerCase())) }
    entrySort      : OrderSpec;
    searchSpec     : SearchSpec;

    importError    : string|null;
    gridOptions    : GridOptions;

    static $inject = ("$element $filter $timeout dialog RsFile FileVersion InvestorXlsFile").split(" ");
    constructor($element: ng.IAugmentedJQuery, _$filter: ng.IFilterService, $timeout:ng.ITimeoutService, private dialog: IDialog,) {
        const $filter_date = _$filter("date");

        this.setEntries(null);
        this.entrySort = new OrderSpec({ by: "LpeAcceptableRsFileId", desc: false });
        this.searchSpec = new SearchSpec();

        this.importError = null;
        if (typeof importError !== "undefined") { this.importError = importError; }



        this.selectedPercent = 0;
        this.reload();
        this.gridOptions = {
            columnDefs: [
                {field: "LpeAcceptableRsFileId"       , headerComponent: SelectAllHeaderComponent, width: 40, cellRenderer:({data}:ICellRendererParam) => `<input type="checkbox" ${data.isSelected ? "checked" : ""} class="rs-file-select" data-id="${data.LpeAcceptableRsFileId}">`, suppressSorting:true, suppressFilter:true, suppressResize:true, }, // checkbox
                {field: "LpeAcceptableRsFileId"       , headerName: ""                           , width: 40, cellRenderer:p => `<a class="rs-file-edit   " data-id="${p.value}">edit   </a>`, suppressSorting:true, suppressFilter:true, suppressResize:true, },
                {field: "LpeAcceptableRsFileId"       , headerName: ""                           , width: 50, cellRenderer:p => `<a class="rs-file-delete " data-id="${p.value}">delete </a>`, suppressSorting:true, suppressFilter:true, suppressResize:true, },
                {field: "LpeAcceptableRsFileId"       , headerName: ""                           , width: 60, cellRenderer:p => `<a class="rs-file-version" data-id="${p.value}">version</a>`, suppressSorting:true, suppressFilter:true, suppressResize:true, },
                {field: "LpeAcceptableRsFileId"       , headerName: "Output Filename"            , width:500, filter:"text", },
                {field: "DeploymentType"              , headerName: "Deployment Type"            , width:120, filter:"text", },
                {field: "IsBothRsMapAndBotWorking"    , headerName: "Both Map & Bot are OK"      , width:170,                cellRenderer:p => p.value ? `<div class="text-center text-success">✓</div>` : `<div class="text-center text-danger">✗</div>` },
                {field: "LatestEffectiveDateTime"     , headerName: "Latest Effective DateTime"  , width:180, filter:"date", cellRenderer:p => $filter_date(p.value, "short") },
                {field: "IsExpirationIssuedByInvestor", headerName: "Latest Expired?"            , width:120,                cellRenderer:p => p.value ? `<span class="text-danger">Expired</span>` : "" },
            ],
            enableSorting: true,
            enableFilter: true,
            enableColResize: true,
        };

        $element.on("click", "a.rs-file-edit", (event: Event) => {
            if (this.entries == null) return;

            const id = (event.currentTarget as HTMLAnchorElement).dataset["id"];
            const f = this.entries.find(f => f.LpeAcceptableRsFileId == id);
            if (f == null) { console.error("file not found, id = ", id); return; }
            $timeout(() => this.edit(f));
        });

        $element.on("click", "a.rs-file-delete", (event: Event) => {
            if (this.entries == null) return;

            const id = (event.currentTarget as HTMLAnchorElement).dataset["id"];
            const f = this.entries.find(f => f.LpeAcceptableRsFileId == id);
            if (f == null) { console.error("file not found, id = ", id); return; }
            $timeout(() => this.deleteFile(f));
        });

        $element.on("click", "a.rs-file-version", (event: Event) => {
            if (this.entries == null) return;

            const id = (event.currentTarget as HTMLAnchorElement).dataset["id"];
            const f = this.entries.find(f => f.LpeAcceptableRsFileId == id);
            if (f == null) { console.error("file not found, id = ", id); return; }
            $timeout(() => this.loadVersions(f));
        });

        $element.on("change", "input.rs-file-select", (event: Event) => {
            if (this.entries == null) return;

            const cb = (event.currentTarget as HTMLInputElement)
            const id = cb.dataset["id"];
            const f = this.entries.find(f => f.LpeAcceptableRsFileId == id);
            if (f == null) { console.error("file not found, id = ", id); return; }

            $timeout(() => this.selectEntry(f, cb.checked));
        });

        $element.on("change", "input.rs-file-select-all", (event: Event) => {
            $timeout(() => {
                const isSelected = (event.currentTarget as HTMLInputElement).checked;
                this.selectAllEntries(isSelected);
            });
        });

        window.addEventListener("optimizedResize", () => {
            if (this.gridOptions.api == null) return;
            this.gridOptions.api.sizeColumnsToFit();
        });
    }

    //#region File list
    private async reload() {
        this.selectedPercent = 0;
        var entries = await RsFile.get();
        this.setEntries(entries);

        if (this.gridOptions.api == null) return;
        this.gridOptions.api.setRowData(this.filteredEntries!);
        this.onFilterChange();
        this.gridOptions.api.sizeColumnsToFit();
    }

    fileEditor: FileEditorCtrl;
    create() {
        this.fileEditor.create();
    }
    edit(f: RsFile) { // null for add new
        console.assert(f != null);
        this.fileEditor.edit(f);
    }
    duplicate() {
        if (this.entries == null) return;

        const f = this.entries.find(f => f.isSelected);
        if (f == null) {
            this.dialog.alert("Please select one entry.");
        } else {
            this.fileEditor.create(f);
        }
    }
    deleteFile(f: RsFile) {
        const m = `Are you sure you want to delete Acceptable Ratesheet File: ${f.LpeAcceptableRsFileId}?`;
        this.dialog.confirm(m, "Confirm")
            .then((isConfirmed) => {
                if (!isConfirmed) return false;
                f.delete().then(
                    () => { this.reload(); },
                    (e: string) => this.dialog.alert(e, "Delete File Fail"));
                return true;
            });
    }

    numberOfSelected: number;
    selectedPercent : number;
    selectEntry(entry: RsFile, isSelected: boolean) {
        entry.isSelected = isSelected;
        this.onEntrySelected();
    }
    onEntrySelected() {
        if (this.filteredEntries == null || this.filteredEntries.length < 1) return;

        const numberOfSelected = this.filteredEntries.reduce((c: number, e: RsFile) => c + (e.isSelected ? 1 : 0), 0);
        this.numberOfSelected = numberOfSelected;
        this.selectedPercent = numberOfSelected / this.filteredEntries.length;

        const allCb = $("input.rs-file-select-all")[0] as HTMLInputElement;
        allCb.checked = !(this.selectedPercent < 1);
        allCb.indeterminate = 0 < this.selectedPercent && this.selectedPercent < 1;
    }
    selectAllEntries(isSelected:boolean) {
        // if (this.entries == null) { debugger; return; }
        // if (isSelected) this.entries.forEach(e => e.isSelected = false); // only de-select when trigger check-all = true

        if (this.filteredEntries == null) { debugger; return; }
        this.filteredEntries.forEach(e => e.isSelected = isSelected);

        this.selectedPercent = isSelected ? 1 : 0;
        this.gridOptions.api!.redrawRows();
    }

    batchEdit() {
        if (this.entries == null) { debugger; return; }

        const xs = this.entries.filter(x => x.isSelected);
        if (xs.length < 1) { debugger; return; }

        this.fileEditor.batchEdit(xs);
    }
    batchDelete() {
        if (this.entries == null) { debugger; return; }

        const fs = this.entries.filter(f => f.isSelected);
        if (fs.length < 1) {
            const m = `Please select at least 1 Acceptable Ratesheet File to delete?`;
            this.dialog.alert(m);
            return false;
        }
        const m = `Are you sure you want to delete selected Acceptable Ratesheet File(s)?`;
        this.dialog.confirm(m, "Confirm").then((isConfirmed) => {
            if (!isConfirmed) return false;
            RsFile.delete(fs).then(
                () => { this.reload(); },
                (e: string) => this.dialog.alert(e, "Delete File(s) Fail"));
            return true;
        });
        return false;
    }
    //#endregion

    private onFilterChange(){
        // if (this.gridOptions.api == null) return;

        // const fa = this.gridOptions.api.getFilterInstance("LpeAcceptableRsFileId");
        // fa.setModel({
        //     type:'contains',
        //     filter:this.searchSpec.LpeAcceptableRsFileId,
        // });
        // this.gridOptions.api.onFilterChanged();

        this.filterEntries();
        this.onEntrySelected();
        if (this.gridOptions.api == null) return;
        this.gridOptions.api.setRowData(this.filteredEntries!);

    }

    //#region FileVersion
    fvList: FvListCtrl;
    loadVersions(f: RsFile) {
        this.fvList.show(f);
    }
    onFvListFvChange(){
        if (this.gridOptions.api == null) return;
        this.gridOptions.api.refreshView();
    }
    //#endregion

    irFileEditor: IrFileEditorCtrl;
    editInvestorXlsFile(f: RsFile) {
        if (f.InvestorXlsFileId == null) return;

        InvestorXlsFile.getById(f.InvestorXlsFileId)
            .then(irf => { this.irFileEditor.show(irf); });
    }
}
import {ng2Var} from "../../../ng-directives/ng2Var";
import { GridApi } from "ag-grid/dist/lib/gridApi";
import { Column } from "ag-grid/dist/lib/entities/column";
angular.module("app", ["agGrid"])
    .factory   ("serviceGen"           , serviceGen                )
    .directive ("selectWhenClicked"    , selectWhenClicked         )
    .directive ("checkboxIndeterminate", checkboxIndeterminate     )

    .service   ("dialog"               , Dialog                    )
    .directive ("sortHandle"           , sortHandle                )

    .factory   ("RsFile"               , rsFileFactory             )
    .factory   ("FileVersion"          , fileVersionFactory        )
    .factory   (InvestorXlsFile.ngName , InvestorXlsFile.ngFactory )
    .factory   ("pageConfig"           , pageConfigFactory         )

    .controller("DlManager"            , DlManager                 )

    .directive(fvEditor        .ngName!, fvEditor                  )
    .directive(fvList          .ngName!, fvList                    )
    .directive(fileEditor      .ngName!, fileEditor                )
    .directive(irFileEditor    .ngName!, irFileEditor              )
;

// https://developer.mozilla.org/en-US/docs/Web/Events/resize
(function() {
    var throttle = function(type:string, name:string, obj:Window = window) {
        var running = false;
        var func = function() {
            if (running) { return; }
            running = true;
            requestAnimationFrame(function() {
                obj.dispatchEvent(new CustomEvent(name));
                running = false;
            });
        };
        obj.addEventListener(type, func);
    };

    /* init - you can init any event */
    throttle("resize", "optimizedResize");
})();
