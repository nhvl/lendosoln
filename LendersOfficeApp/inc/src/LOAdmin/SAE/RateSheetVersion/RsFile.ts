import { IPromise, IQService } from "angular";
import {IService   } from "../../../ng-common/serviceGen";
import {FileVersion} from "./FileVersion";

let $q     : IQService;
let service: IService;

export interface IRsFile {
    LpeAcceptableRsFileId        ?: string ;
    DeploymentType               ?: string ;
    IsBothRsMapAndBotWorking     ?: boolean;
    InvestorXlsFileId            ?: number|null;
    IsExpirationIssuedByInvestor ?: boolean|null;

    LatestEffectiveDateTime      ?: Date|null;
    XlsFileName                  ?: string;
}

export class RsFile implements IRsFile {
    LpeAcceptableRsFileId       : string ;
    DeploymentType              : string ;
    IsBothRsMapAndBotWorking    : boolean;
    InvestorXlsFileId           : number|null;
    IsExpirationIssuedByInvestor: boolean|null;

    LatestEffectiveDateTime     : Date|null;
    XlsFileName                 : string;

    isSelected                  : boolean;
    fileVersions                : FileVersion[]|null;

    constructor(d?: IRsFile){
        this.LpeAcceptableRsFileId        = "";
        this.DeploymentType               = "UseVersion";
        this.IsBothRsMapAndBotWorking     = true;
        this.InvestorXlsFileId            = null;
        this.IsExpirationIssuedByInvestor = null;

        this.LatestEffectiveDateTime      = null;
        this.XlsFileName                  = "";

        this.isSelected                   = false;
        this.fileVersions                 = null;

        if (d != null) Object.assign(this, d);
    }

    create() {
        return service<string>("CreateFile", { file: this })
            .then((error: string) => (!error) ? $q.when(error) : $q.reject(error))
            ;
    }

    update(): IPromise<string> {
        return service<string>("UpdateFile", { file: this })
            .then((error: string) => (!error) ? $q.when(error) : $q.reject(error))
            ;
    }

    delete(): IPromise<string> {
        return service<string>("DeleteFiles", { ids: [this.LpeAcceptableRsFileId] })
            .then((error: string) => (!error) ? $q.when(error) : $q.reject(error));
    }

    getFileVersionAsync(): IPromise<FileVersion[]> {
        return FileVersion.get(this.LpeAcceptableRsFileId)
            .then((vs:FileVersion[]) => {
                this.fileVersions = vs;
                this.calcExpiration();
                return vs;
            })
            ;
    }
    calcExpiration(){
        if (this.fileVersions == null || this.fileVersions.length < 1) return;
        this.IsExpirationIssuedByInvestor = this.fileVersions.reduce((latest, fv) => fv.VersionNumber > latest.VersionNumber ? fv : latest).IsExpirationIssuedByInvestor;
    }

    static get(): IPromise<RsFile[]> {
        return service<RsFile[]>("GetFiles")
            .then((es: RsFile[]) => es.map(e => new RsFile(e)));
    }

    static batchEdit(fs: RsFile[], f:RsFile) : IPromise<string> {
        return service<string>("BatchEditFile", { ids: fs.map(f => f.LpeAcceptableRsFileId), file: f })
            .then((error: string) => (!error) ? $q.when(error) : $q.reject(error));
    }
    static delete(fs: RsFile[]): IPromise<string> {
        return service<string>("DeleteFiles", { ids: fs.map(f => f.LpeAcceptableRsFileId) })
            .then((error: string) => (!error) ? $q.when(error) : $q.reject(error));
    }
}

rsFileFactory.$inject = ("serviceGen $q").split(" ");
export function rsFileFactory(serviceGen: (url?: string) => IService, _$q: IQService): typeof RsFile {
    service = serviceGen();
    $q      = _$q;
    return RsFile;
}
