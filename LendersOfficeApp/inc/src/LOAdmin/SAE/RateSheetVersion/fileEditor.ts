﻿import { IPromise, IAngularEvent, IAttributes, IParseService, IDirective, IScope, IController, IDirectiveLinkFn } from "angular";
import {RsFile           } from "./RsFile";
import {InvestorXlsFile  } from "../InvestorXlsFileList/InvestorXlsFile";
import {formValidate     } from "../../../ng-common/formValidate";
import {IDialog          } from "../../../ng-common/dialog";
import {PageConfig       } from "./pageConfig";

class BatToggle {
    DeploymentType              : boolean = false;
    IsBothRsMapAndBotWorking    : boolean = false;
    InvestorXlsFileId           : boolean = false;
    IsExpirationIssuedByInvestor: boolean = false;
}

export class FileEditorCtrl { static ngName = "FileEditorCtrl";
    private entry     : RsFile|null;
    private isCreating: boolean;

    private isBatchEdit: boolean;
    private batToggle: BatToggle;

    private $modal: JQuery;
    private onClose: () => void;

    private defaultFile: RsFile;
    private deploymentTypes: string[];
    private investorXslFiles: InvestorXlsFile[];

    static $inject = ("$element dialog pageConfig FileVersion").split(" ");
    constructor($element: JQuery, private dialog: IDialog, pageConfig: IPromise<PageConfig>) {
        this.entry = null;
        this.$modal = $element.find(".modal");

        console.assert(this.$modal.length === 1);

        pageConfig.then((c: PageConfig) => {
            this.defaultFile = c.defaultFile;
            this.deploymentTypes = c.DeploymentTypes;
            this.investorXslFiles = c.InvestorXslFiles;
        });
    }

    private batchEditEntries : RsFile[];
    edit(entry: RsFile) {
        this.resetError();

        this.isBatchEdit = false;

        console.assert(entry != null);
        this.isCreating = false;
        this.entry = new RsFile(entry);

        this.$modal.modal("show");
    }
    create(entry ?: RsFile) {
        this.resetError();

        this.isBatchEdit = false;

        this.isCreating = true;
        this.entry = new RsFile(entry || this.defaultFile);

        this.$modal.modal("show");
    }
    batchEdit(entries: RsFile[]) {
        this.batchEditEntries = entries;

        this.resetError();

        this.isBatchEdit = true;

        this.entry = new RsFile(this.defaultFile);

        this.batToggle = new BatToggle();

        this.$modal.modal("show");
    }

    private saveFile($event: IAngularEvent) {
        if (this.entry == null) return;

        if (!formValidate($event.currentTarget as HTMLFormElement)) {
            console.warn("Form Invalid");
            return;
        }

        this.resetError();
        if (this.isBatchEdit) {
            Object.keys(this.batToggle).forEach(k => {
                if (!!(this.batToggle as any)[k]) return;
                (this.entry as any)[k] = null;
            });
            this.entry.IsExpirationIssuedByInvestor = this.batToggle.IsExpirationIssuedByInvestor ? !!this.entry.IsExpirationIssuedByInvestor : null;

            RsFile.batchEdit(this.batchEditEntries, this.entry)
            .then(() => {
                    this.$modal.modal("hide");
                    this.onClose();
                },
                (e:string) => this.dialog.alert(e, "Batch edit error"));
        } else {
            const saveProcess = (this.isCreating ? this.entry.create() : this.entry.update());
            saveProcess.then<void, void>(
                () => {
                    this.$modal.modal("hide");
                    this.onClose();
                },
                (error: string) => {
                    this.dialog.alert(error, "Save File Fail")
                });
        }
    }

    private resetError() {
        // Nothing to implement
    }
}

interface IFvEdittorAttrs extends IAttributes {
    ng2Var: string;
}

import * as fileEditorHtml from "./fileEditor.html";

fileEditor.ngName = "fileEditor";
fileEditor.$inject = ("$parse").split(" ");
export function fileEditor($parse: IParseService): IDirective {
    return {
        restrict: "E",
        scope   : {
            onClose: "&",
        },
        link: ((scope: IScope, $e: JQuery, attrs: IFvEdittorAttrs, ctrl: IController) => {
            $parse(attrs.ng2Var).assign(scope.$parent, ctrl);
        }) as IDirectiveLinkFn,
        template        : fileEditorHtml,
        controller      : FileEditorCtrl,
        controllerAs    : "vm",
        bindToController: true,
    };
}
