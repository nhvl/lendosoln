export class SearchForm {
    active     : boolean|null;
    login      : string;
    description: string;
    fileName   : string;

    constructor() {
        this.active      = null;
        this.login       = "";
        this.description = "";
        this.fileName    = "";
    }

    static activeOptions = [
        { v:           null, t: "All"           },
        { v:           true, t: "Active Only"   },
        { v:          false, t: "Inactive Only" },
    ];
}
