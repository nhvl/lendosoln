﻿import { IParseService, IDirective, IScope, IAngularEvent, IDirectiveLinkFn} from "angular";
import {formValidate} from "../../../ng-common/formValidate";
import {INg2VarAttrs} from "../../../ng-directives/INg2VarAttrs";

import {DlEntry     } from "./DlEntry";

export interface DlEditorScope extends IScope{
    editEntryForm:  {
        Url: {
            $error: {
                url: boolean,
            }
        },
    }
}

class BatchEditToogle {
    Description            : boolean;
    Url                    : boolean;
    FileName               : boolean;
    Active                 : boolean;
    Conversion             : boolean;
    BotType                : boolean;
    Login                  : boolean;
    Password               : boolean;
    LastRatesheetTimestamp : boolean;
    Token                  : boolean;
    [field:string]:boolean;

    constructor() {
        this.Description            = false;
        this.Url                    = false;
        this.FileName               = false;
        this.Active                 = false;
        this.Conversion             = false;
        this.BotType                = false;
        this.Login                  = false;
        this.Password               = false;
        this.LastRatesheetTimestamp = false;
        this.Token                  = false;
    }
}

export class DlEditorController {
    private eEntry       : DlEntry; // editting vendor object
    private entries      : DlEntry[]; // batch editting entries
    private isBatchEdit  : boolean;
    private batToggle    : BatchEditToogle;
    private clientCode   : string;
    private $modal       : JQuery;
    static $inject = ("$scope $element DlEntry").split(" ");
    constructor(private $scope: DlEditorScope, $element:JQLite) {
        this.eEntry = new DlEntry();
        this.isBatchEdit = false;
        this.batToggle = new BatchEditToogle();
        this.clientCode = "";
        this.$modal = $element.find(".modal"); console.assert(this.$modal.length === 1);
    }

    private onSaved:()=>void;
    private save($event: IAngularEvent) {
        const editEntryForm = this.$scope.editEntryForm;

        if (!formValidate($event.currentTarget as HTMLFormElement)) {
            console.warn("Form Invalid");
            return false;
        }

        this.resetError();
        if (this.isBatchEdit) {
            const isNothingSelect = Object.keys(this.batToggle).every((p: string) => !this.batToggle[p]);
            if (isNothingSelect) {
                console.warn("Nothing select for Batch update");
                return false;
            }

            const inEntry = new DlEntry(this.eEntry);
            Object.keys(this.batToggle).forEach((p: string) => {
                if (!(this.batToggle[p])) (inEntry as any)[p] = null;
            });

            DlEntry.batchUpdate(this.entries, inEntry)
                .then(() => {
                    this.$modal.modal("hide");
                    this.onSaved();
                });
        } else {
            this.eEntry.update()
                .then(
                    () => {
                        this.$modal.modal("hide");
                        this.onSaved();
                    },
                    (error: string) => { alert(error); });
        }

        return false;
    }
    private populateFields() {
        this.eEntry.populate(this.clientCode);
    }
    private resetError() {
        const editEntryForm = this.$scope.editEntryForm;
        //editEntryForm.Url.$error.url = false;
    }

    startEdit(entry?: DlEntry) { // null for add new
        this.resetError();
        this.isBatchEdit = false;
        this.eEntry = new DlEntry(entry);
        this.$modal.modal("show");
    }
    startBatchEdit(entries: DlEntry[]) { // null for add new
        this.entries = entries;

        this.resetError();
        this.isBatchEdit = true;
        this.eEntry = new DlEntry();
        this.batToggle = new BatchEditToogle();
        this.$modal.modal("show");
    }
}

import * as dlEditorHtml from "./dlEditor.html";

dlEditorDirective.ngName = "dlEditor";
dlEditorDirective.$inject = ("$parse").split(" ");
export function dlEditorDirective($parse: IParseService): IDirective {
    return {
        restrict         : "E",
        scope            : { onSaved: "&" },
        template         : dlEditorHtml,
        controller       : DlEditorController,
        controllerAs     : "vm",
        bindToController : true,
        link             : ((scope, $e, attrs: INg2VarAttrs, ctrl: DlEditorController) => {
                                $parse(attrs.ng2Var).assign(scope.$parent, ctrl);
                            }) as IDirectiveLinkFn,
    };
}
