import { IQService, IPromise} from "angular";
import {IServiceGen, IService } from "../../../ng-common/serviceGen";

import {SearchForm} from "./SearchForm";

let $q: IQService;
let service: <T>(method: string, args ?: {}, config ?: {}) => IPromise<T>;

export class DlEntry {
    static ngName = "DlEntry";

    Id                     : number|null; // int
    Description            : string ;
    Url                    : string ;
    FileName               : string ;
    dLastUpdated           : Date|null;
    dLastSuccessfulDL      : Date|null;
    Active                 : boolean;
    LastDlResult           : string ;
    ErrorCount             : number ; // int
    Conversion             : string ;
    BotType                : string ;
    Login                  : string ;
    Password               : string ;
    LastRatesheetTimestamp : Date|null;
    Token                  : string ;
    Note                   : string ;

    isSelected: boolean;

    constructor(d?: DlEntry) {
        this.Id                     = null;
        this.Description            = ""  ;
        this.Url                    = ""  ;
        this.FileName               = ""  ;
        this.dLastUpdated           = null;
        this.dLastSuccessfulDL      = null;
        this.Active                 = true;
        this.LastDlResult           = ""  ;
        this.ErrorCount             = 0   ;
        this.Conversion             = ""  ;
        this.BotType                = ""  ;
        this.Login                  = ""  ;
        this.Password               = ""  ;
        this.LastRatesheetTimestamp = null;
        this.Token                  = ""  ;
        this.Note                   = ""  ;

        this.isSelected             = false;

        if (d != null) Object.assign(this, d);
        return this;
    }

    populate(clientCode: string): void {
        this.Description = `CHASE_${clientCode}`;
        this.Url         = "https://www.chasecorrespondentchaselock.chase.com/NetOxygen/fw_chase_portal/portal_frame.asp";
        this.FileName    = `CHASE_${clientCode}.xls`;
        this.Conversion  = "STRIP_DATETIME_FROM_EXCEL";
        this.BotType     = "CHASE";
    }

    update(): IPromise<number> {
        return service<number>("Update", { entry: this })
            .then((result: number | string) => (typeof result === "string")
                ? $q.reject(result as string)
                : $q.when(result as number))
            ;
    }

    delete(): IPromise<number> {
        return service<number>("Delete", { id: this.Id });
    }

    deleteMessage() {
        return `Are you sure you want to delete entry ${this.Id}, ${this.Description}?`;
    }

    static get(searchForm:SearchForm): IPromise<DlEntry[]> {
        return service<DlEntry[]>("Get", searchForm)
            .then((es: DlEntry[]) => es.map(e => new DlEntry(e)));
    }
    static batchUpdate(entries: DlEntry[], inEntry: DlEntry): IPromise<void> {
        return service<void>("BatchUpdate", { ids: entries.map(e => e.Id), inEntry });
    }

    static ngFactory(serviceGen: IServiceGen, _$q: IQService): typeof DlEntry {
        service = serviceGen() as any;
        $q = _$q;
        return DlEntry;
    }
}
DlEntry.ngFactory.$inject = ["serviceGen", "$q"];
