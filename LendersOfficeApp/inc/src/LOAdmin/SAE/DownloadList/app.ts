import * as angular from "angular";
import "angular-messages";

import {serviceGen,          } from "../../../ng-common/serviceGen";
import {Dialog, IDialog      } from "../../../ng-common/dialog";
import {selectWhenClicked    } from "../../../ng-directives/selectWhenClicked";
import {checkboxIndeterminate} from "../../../ng-directives/checkboxIndeterminate";
import {sortHandle, OrderSpec} from "../../../ng-directives/sortIcon";

import {DlEntry   } from "./DlEntry";
import {SearchForm} from "./SearchForm";
import {DlEditorController, dlEditorDirective} from "./dlEditor";
let $timeout: ng.ITimeoutService;

class DlManager {
    entries      : DlEntry[]|null;
    entrySort    : OrderSpec;

    isLoading    : boolean;

    searchForm   : SearchForm;
    activeOptions: { v: boolean|null, t: string }[];

    selectedPercent: number;

    dlEditor: DlEditorController;

    static $inject = ["dialog", "$timeout", DlEntry.ngName];
    constructor(private dialog: IDialog, private _$timeout:ng.ITimeoutService) {
        $timeout = _$timeout;
        this.entries = null;
        this.entrySort = new OrderSpec({ by: "FileName", desc:false });

        this.isLoading = false;

        this.searchForm = new SearchForm();
        this.activeOptions = SearchForm.activeOptions;

        this.selectedPercent = 0;
        $(window).resize(() => { this.resizeTable() });
        this.reload();
    }

    resizeTable():void{
        $('.table-responsive .table tbody').height(window.innerHeight - 380);
        var width = $('.table-responsive .table tbody').width() as number;
        $('.id'             ).css("width",width * 0.03);
        $('.active-col'     ).css("width",width * 0.04);
        $('.description'    ).css("width",width * 0.07);
        $('.url'            ).css("width",width * 0.11);
        $('.file-name'      ).css("width",width * 0.12);
        $('.last-updated'   ).css("width",width * 0.06);
        $('.last-successful').css("width",width * 0.085);
        $('.last-result'    ).css("width",width * 0.06);
        $('.conversion'     ).css("width",width * 0.065);
        $('.pass'           ).css("width",width * 0.055);
    }

    reload() {
        this.isLoading = true;
        DlEntry.get(this.searchForm)
            .then((xs) => {
                this.entries = xs;
                this.selectedPercent = 0;
                $timeout(() => {
                    this.resizeTable();
                });
            })
            .finally(() => this.isLoading = false);
    }
    delete(entry: DlEntry) {
        this.dialog.confirm(entry.deleteMessage(), "Confirm")
            .then((isConfirmed) => {
                if (!isConfirmed) return false;
                entry.delete().then(() => { this.reload(); });
                return true;
            });
    }
    edit(entry?: DlEntry) { // null for add new
        this.dlEditor.startEdit(entry);
    }
    batchEdit() {
        if (this.entries == null) return;
        this.dlEditor.startBatchEdit(this.entries.filter(e => e.isSelected));
    }

    onEntrySelect() {
        console.debug("event[onEntrySelect]");
        if (this.entries == null || this.entries.length < 1) return;

        const numberOfSelected = this.entries.reduce((c: number, e: DlEntry) => c + (e.isSelected ? 1 : 0), 0);
        this.selectedPercent = numberOfSelected / this.entries.length;
    }
    onSelectAll($event: ng.IAngularEvent) {
        if (this.entries == null) return;

        const isSelect = (($event.currentTarget as HTMLInputElement).checked);
        for (let e of this.entries) e.isSelected = isSelect;
        this.selectedPercent = isSelect ? 1 : 0;
    }
}

import "./app.css";
import * as template from "./app.html";

angular.module("app", ["ngMessages"])
    .factory  ("serviceGen"              , serviceGen)
    .directive("selectWhenClicked"       , selectWhenClicked)
    .directive("checkboxIndeterminate"   , checkboxIndeterminate)
    .service  ("dialog"                  , Dialog)

    .directive ("sortHandle"             , sortHandle)

    .factory   (DlEntry.ngName           , DlEntry.ngFactory)

    .directive (dlEditorDirective.ngName! , dlEditorDirective)

    .component("downloadManager", { controller: DlManager, controllerAs: "vm", template })
;
