import {module, IPromise, IQService, IScope, ITimeoutService, IAngularEvent} from "angular";
import {serviceGen, IService } from "../../../ng-common/serviceGen";
import {formValidate         } from "../../../ng-common/formValidate";
import {Dialog, IDialog      } from "../../../ng-common/dialog";
import {selectWhenClicked    } from "../../../ng-directives/selectWhenClicked";
import {sortHandle, OrderSpec} from "../../../ng-directives/sortIcon";
import {guidEmpty, isGuid, StrGuid} from "../../../utils/guid";

import "../../../utils/Array.toDictionary";

const $ = jQuery;

interface IPceRepsone {
    importErrors         : PlanCodeError[];// Errors of importing plan codes
    saveErrors           : PlanCodeError[];// Errors while save plan codes
    newPlanCodes         : PlanCode[]     ;// only PlanCodeId - PlanCodeNm
    planCodes            : PlanCode[]     ;// The Plan code on DB or importing file
    isImporting          : boolean        ;// indicate that is middle of importing
    investorNameSynonyms:  string         ;
}
declare const pceData: IPceRepsone;

class FilterSpec {
    PlanCodeNm    : string;
    PlanCodeDesc  : string;
    InvestorNm    : string;

    constructor() {
        this.PlanCodeNm   = "";
        this.PlanCodeDesc = "";
        this.InvestorNm   = "";
    }
}

class PlanCode {
    PlanCodeId    : StrGuid;
    PlanCodeNm    : string;
    PlanCodeDesc  : string;
    InvestorNm    : string;
    RuleXmlContent: string;

    HasError      : boolean;

    isSelected    : boolean;

    constructor(d?: PlanCode) {
        this.PlanCodeId     = guidEmpty;
        this.PlanCodeNm     = ""  ;
        this.PlanCodeDesc   = ""  ;
        this.InvestorNm     = ""  ;
        this.RuleXmlContent = ""  ;

        this.HasError = false;

        this.isSelected = false;

        if (d != null) Object.assign(this, d);

        // Validate
        console.assert(
            isGuid(this.PlanCodeId),
            "Plan code [%s] has an invalid PlanCodeID [%s]", this.PlanCodeNm, this.PlanCodeId, this);
    }

    update(): IPromise<void> {
        const service = PlanCode.service;
        const $q = PlanCode.$q;

        return service<string>("Update", { planCode: this })
            .then((error:string) => (!error) ? $q.when() : $q.reject(error))
            ;
    }

    delete(): IPromise<number> {
        const service = PlanCode.service;

        return service<number>("Delete", { id: this.PlanCodeId });
    }

    deleteMessage() {
        return `Are you sure you want to delete Plan code [${this.PlanCodeNm}]?`;
    }

    static get(): IPromise<PlanCode[]> {
        throw new Error("Not implemented");
        const service = PlanCode.service;
        return service<PlanCode[]>("Get")
            .then(es => es.map(e => new PlanCode(e)));
    }

    static import(plans: PlanCode[]) {
        const service = PlanCode.service;
        return service<IPceRepsone>("Import", { plans })
            ;
    }

    static $q : IQService;
    static service: <T>(method: string, args?: {}) => IPromise<T>;
}

class PlanCodeError {
    PlanCodeNm: string;
    LineNum   : number|null;
    Messages  : string[];

    constructor(d?: PlanCodeError) {
        this.PlanCodeNm = "";
        this.LineNum    = null;
        this.Messages   = [];

        if (d != null) $.extend(this, d);
    }

    show() {
        if (!!this.PlanCodeNm) {
            return [this.PlanCodeNm].concat(this.Messages);
        } else if (this.LineNum != null) {
            return [`Line number ${this.LineNum}: ${this.Messages[0]}`];
        } else {
            return ["An unspecified error occurred"];
        }
    }
}

DlEntryFactory.$inject = ["serviceGen", "$q"];
function DlEntryFactory(serviceGen:(url?: string) => IService, $q: IQService): typeof PlanCode {
    PlanCode.service = serviceGen();
    PlanCode.$q = $q;
    return PlanCode;
}

interface DlManagerScope extends IScope{
    editEntryForm:  {
        Url: {
            $error: {
                url: boolean,
            }
        },
    }
}

class DlManager {
    eEntry       : PlanCode;   // editting vendor object

    entries      : PlanCode[];
    // sort and filter
    entrySort    : OrderSpec;
    investors    : {t:string, v:string}[];
    filterSpec   : FilterSpec;

    // import
    isImporting  : boolean;
    importErrors : PlanCodeError[];
    saveErrors   : PlanCodeError[];

    investorNameSynonyms: string;

    isLoading    : {import:boolean};

    static $inject = ("$scope dialog $timeout DlEntry").split(" ");
    constructor(private $scope: DlManagerScope, private dialog: IDialog, private $timeout:ITimeoutService) {
        this.eEntry = new PlanCode();
        this.entrySort = new OrderSpec({ by: "", desc:false });

        this.isImporting = false;
        this.importErrors = [];
        this.saveErrors = [];

        this.isLoading = {import: false};

        this.filterSpec = new FilterSpec();
        this.investors = [{ t: "All", v: "" }];

        this.reload();
    }

    reload() {
        if (typeof pceData === "undefined") {
            this.$timeout(() => {
                if (typeof pceData === "undefined") {
                    alert("Error: No data found (pceData). Please contact us.");
                    debugger;
                    return;
                } else {
                    this._syncRespone(pceData);
                }
            }, 300);
        } else {
            this._syncRespone(pceData);
        }
    }
    delete(entry: PlanCode) {
        this.dialog.confirm(entry.deleteMessage(), "Confirm")
            .then((isConfirmed) => {
                if (!isConfirmed) return false;
                entry.delete().then(() => {
                    window.location.reload(false);
                    this.reload();
                });
                return true;
            });
    }
    edit(entry?: PlanCode) { // null for add new
        this.resetError();

        this.eEntry = new PlanCode(entry);
    }
    save($event: IAngularEvent) {
        const editEntryForm = this.$scope.editEntryForm;

        if (!formValidate(<HTMLFormElement> $event.currentTarget)) {
            console.warn("Form Invalid");
            return;
        }

        this.resetError();

        if (this.isImporting) {
            const i = this.entries.findIndex(e => this.eEntry.PlanCodeNm === e.PlanCodeNm);
            this.entries[i] = this.eEntry;

            $("#editEntryModal").modal("hide");
            return;
        }

        this.eEntry.update().then(
            () => {
                const i = this.entries.findIndex(e => this.eEntry.PlanCodeId === e.PlanCodeId);
                if (i < 0) {
                    console.error("Index out of range");
                    return;
                }
                this.entries[i] = this.eEntry;

                $("#editEntryModal").modal("hide");
            },
            (error: string[]) => {
                this.dialog.alert(error.join("<br/>"), "Update Plan Code Fail");
            });
    }
    continueImport() {
        if (!this.isImporting) { console.error("Cannot continueImport() when is not importing"); return; }
        if (!!this.isLoading.import) { console.error("Cannot continueImport() when loading..."); return; }

        this.isLoading.import = true;
        PlanCode.import(this.entries)
            .then((r: IPceRepsone) => {
                if (r.saveErrors == null || r.saveErrors.length < 1) { // No error
                    // Force refresh not only for update the data in the view
                    // (We can merge the data without hard refresh)
                    // but also prevent user to repost the POSTDATA when they do refresh (F5)
                    window.location.replace(window.location.href);
                }

                this._syncRespone(r);

                console.assert(!this.isImporting);

                console.info("%d new plan codes created.", r.newPlanCodes.length, r.newPlanCodes);
            })
            .finally(() => { this.isLoading.import = false; });
        return false;
    }
    private _syncRespone(r: IPceRepsone) {
        if (r.investorNameSynonyms) this.investorNameSynonyms = r.investorNameSynonyms;
        if (r.planCodes) {
            this.entries = r.planCodes.map(c => new PlanCode(c));
            this.investors = [{ t: "All", v: "" }].concat(
                Array.from<string>(new Set<string>(this.entries.map(e => e.InvestorNm))).map(s => { return { t: s, v: s } }));
        }
        this.isImporting = !!(r.isImporting);
        if (r.saveErrors) this.saveErrors = r.saveErrors.map(c => new PlanCodeError(c));
        if (r.importErrors) this.importErrors = r.importErrors.map(c => new PlanCodeError(c));

        {
            var seDict = (this.saveErrors || <PlanCodeError[]>[]).toDictionary(e => e.PlanCodeNm, e => true);
            var ieDict = (this.importErrors || <PlanCodeError[]>[]).toDictionary(e => e.PlanCodeNm, e => true);
            this.entries.forEach(c => c.HasError = (seDict[c.PlanCodeNm] || ieDict[c.PlanCodeNm]));
        }

        if (r.newPlanCodes) {
            const name2Id = r.newPlanCodes.toDictionary(e => e.PlanCodeNm, e => e.PlanCodeId);
            this.entries.forEach(c => {
                const name = c.PlanCodeNm;
                const id = name2Id[name];
                console.assert(c.PlanCodeId === guidEmpty, "New plan code should have empty PlanCodeId", c);
                if (!!id) { c.PlanCodeId = id; }
            });
        }
    }

    resetError() {
        const editEntryForm = this.$scope.editEntryForm;
        //editEntryForm.Url.$error.url = false;
    }
}

module("app", [])
    .factory   ("serviceGen"           , serviceGen           )
    .service   ("dialog"               , Dialog               )

    .directive ("selectWhenClicked"    , selectWhenClicked    )
    .directive ("sortHandle"           , sortHandle           )

    .factory   ("DlEntry"              , DlEntryFactory       )
    .controller("DlManager"            , DlManager            )
;
