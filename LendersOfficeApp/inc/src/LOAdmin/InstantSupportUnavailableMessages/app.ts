﻿/// <reference path="../../dt-local/tsd.d.ts" />

import * as angular from "angular";
import "angular-messages";
import "angular-sanitize";

import {serviceGen, IService} from "../../ng-common/serviceGen";
import {formValidate} from "../../ng-common/formValidate";

const $ = jQuery;
class Holiday {
    Name: string;
    Date: string;
}
class ISUMConfig {
    MsgId: string;
    UserMsg: string;
    EffectiveD: string;
    EndD: string;
    IsDefault: boolean;
    IsAdd: boolean;

    WindowsX: number;
    WindowsY: number;

    Holidays: Holiday[];

    ErrMsg: string;

    constructor(d?: ISUMConfig) {
        if (d != null) $.extend(this, d);
        return this;
    }
    setDefault(currentDefault:string): ng.IPromise<ISUMConfig> {
        const service = ISUMConfig.service;
        const $q = ISUMConfig.$q;

        return service<ISUMConfig>("SetDefaultMessage", { defaultId: currentDefault, MsgId: this.MsgId })
            .then((error) => (error === null || error.hasOwnProperty("d")) ? $q.when(null) : $q.reject(error));
    }
    getPreview(): ng.IPromise<ISUMConfig> {
        const service = ISUMConfig.service;
        const $q = ISUMConfig.$q;

        return service<ISUMConfig>("Preview", { MsgId: this.MsgId })
            .then(v => (v.ErrMsg == '') ? $q.when(new ISUMConfig(v)) : $q.reject(v));
    }
    update(): ng.IPromise<ISUMConfig> {
        const service = ISUMConfig.service;
        const $q = ISUMConfig.$q;

        return service<ISUMConfig>("Update", { data: this })
            .then((error) => (error === null || error.hasOwnProperty("d")) ?
                $q.when(null) :
                $q.reject(error));
    }

    static get(ISUMId:string):ng.IPromise<ISUMConfig> {
        const service = ISUMConfig.service;
        const $q = ISUMConfig.$q;

        return service<ISUMConfig>("GetDetail", { MsgId: ISUMId })
            .then(v => (v.ErrMsg == '') ? $q.when(new ISUMConfig(v)) : $q.reject(v));
    }

    static getList(): ng.IPromise<ISUMConfig[]> {
        const service = ISUMConfig.service;
        return service<ISUMConfig[]>("Get")
            .then(
            vs => vs.map(v => new ISUMConfig(v)));
    }
    delete(): ng.IPromise<ISUMConfig> {
        const service = ISUMConfig.service;
        const $q = ISUMConfig.$q;

        return service<ISUMConfig>("DeleteMessage", { MsgId: this.MsgId })
            .then((error) => (error === null || error.hasOwnProperty("d")) ? $q.when(null) : $q.reject(error));
    }
    static $q :ng.IQService;
    static service<T>(method: string, args?: {}): ng.IPromise<T> {
        console.error("Have not assgin [service] function");
        return null;
    }
}

ISUMConfigFactory.$inject = ["serviceGen", "$q"];
function ISUMConfigFactory(serviceGen:(url?: string) => IService, $q: ng.IQService): typeof ISUMConfig {
    ISUMConfig.service = <any> serviceGen();
    ISUMConfig.$q = $q;
    return ISUMConfig;
}

interface ISUMManagerScope extends ng.IScope{
    editISUMForm:  {
        PackageDataListJSON: {
            $error: {
                json: boolean,
            }
        },
    }
}
class DatePickerItem {
    //https://angular-ui.github.io/bootstrap/
    date: Date;
    minDate: Date;
    isOpen: boolean;
    format: string;
    constructor(d?: string) {
        if (d != null && d != "") {
            this.date = new Date(d);
            this.minDate = new Date(d);
        }
        else {
            this.date = null;
            this.minDate = new Date();
        }
        this.isOpen = false;

        this.format = 'M/d/yyyy';
    }
    open() {
        this.isOpen = true;
        //return false;
    }
}
class ISUMManager{
    eISUM: ISUMConfig; // editting or deleting ISUM object
    ISUMs: ISUMConfig[];
    isLoading: boolean;
    defaultMsgId: string;
    currentDefaultMsgId: string;
    previewMessage: ISUMConfig;

    //use for edit or add new
    effDate: DatePickerItem;
    endDate: DatePickerItem;

    static $inject = ["$scope", "ISUMConfig"];
    constructor(private $scope: ISUMManagerScope) {
        this.eISUM = new ISUMConfig();
        this.isLoading = false;
        this.ISUMs = null;
        this.defaultMsgId = null;
        this.currentDefaultMsgId = null;

        //show multiple modals
        //http://stackoverflow.com/questions/19305821/multiple-modals-overlay
        $(document).on('show.bs.modal', '.modal', function (event) {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            setTimeout(function () {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 1000);
        });

        this.reload();
    }

    selectChange(item: string) {
        //event when select tag change in edit and add new dialog
        this.effDate = new DatePickerItem(item);
        this.endDate = new DatePickerItem(item);
    }

    delete(ISUM: ISUMConfig) {
        this.eISUM = ISUM;
    }
    saveDelete() {
        this.eISUM.delete()
            .then(
            () => {
                if (this.eISUM.MsgId == this.defaultMsgId) $('#btnDefault').attr('disabled', 'disabled');
                $("#deleteISUMModal").modal("hide");
                this.reload();
            },
            (error: ISUMConfig) => {
                if (error.ErrMsg != '')
                    alert(error.ErrMsg);
            });
    }
    edit(ISUM?: ISUMConfig) {
        var val = (ISUM != null) ? ISUM.MsgId : '';
        ISUMConfig.get(val).then(
            (xs) => {
                this.eISUM = xs;
                $("#editISUMModal").find("select").prop('selectedIndex', 0);
                this.effDate = new DatePickerItem(xs.EffectiveD);
                this.endDate = new DatePickerItem(xs.EndD);
                $("#editISUMModal").modal("show");
            },
            (error: ISUMConfig) => {
                if (error.ErrMsg != '')
                    alert(error.ErrMsg);
            });
    }
    check(ISUM: ISUMConfig, $event: ng.IAngularEvent) {
        const checkbox = $event.currentTarget as HTMLInputElement;
        var status = checkbox.checked;
        if (status) {
            $('#btnDefault').removeAttr('disabled');
            $("input:checkbox").prop("checked", false);
            checkbox.checked = status;
            this.defaultMsgId = ISUM.MsgId;
        }
        else {
            $('#btnDefault').attr('disabled', 'disabled');
            this.defaultMsgId = null;
        }
    }
    setDefault($event: ng.IAngularEvent) {
        for (var i in this.ISUMs) {
            if (this.ISUMs[i].MsgId == this.defaultMsgId) {
                this.ISUMs[i].setDefault(this.currentDefaultMsgId)
                    .then(
                    () => {
                        $('#btnDefault').attr('disabled', 'disabled');
                        $("#defaultISUMModal").modal("hide");
                        this.reload();
                    },
                    (error: ISUMConfig) => {
                        if (error.ErrMsg!='')
                            alert(error.ErrMsg);
                    });
            }
        }

    }
    preview(ISUM: ISUMConfig) {
        ISUM.getPreview().then(
            (xs) => {
                this.previewMessage = xs;

                $("#previewISUMModal .modal-content")
                    .css("height", this.previewMessage.WindowsY)
                    .css("width", this.previewMessage.WindowsX);
                $("#previewISUMModal")
                    .css("left", (window.innerWidth - this.previewMessage.WindowsX) / 2)
                    .css("bottom", "auto");
                $("#previewISUMModal").modal("show");
            },
            (error: ISUMConfig) => {
                if (error.ErrMsg != '')
                    alert(error.ErrMsg);
            });
    }
    previewTemp() {
        this.previewMessage = this.eISUM;
        $("#previewISUMModal .modal-content")
            .css("height", this.previewMessage.WindowsY)
            .css("width", this.previewMessage.WindowsX);
        $("#previewISUMModal")
            .css("left", (window.innerWidth - this.previewMessage.WindowsX) / 2)
            .css("bottom", "auto");
        $("#previewISUMModal").modal("show");
    }

    save($event: ng.IAngularEvent) {
        const editISUMForm = this.$scope.editISUMForm;

        if (!formValidate(<HTMLFormElement> $event.currentTarget)) return;


        //this.resetError();
        if (this.effDate.date != null) this.eISUM.EffectiveD = this.effDate.date.toDateString();
        else this.eISUM.EffectiveD = null;
        if (this.endDate.date != null) this.eISUM.EndD = this.endDate.date.toDateString();
        else this.eISUM.EndD = null;


        this.eISUM.update()
            .then(
                () => {
                    $("#editISUMModal").modal("hide");
                    this.reload();
                },
                (error: ISUMConfig) => {
                    if (error.ErrMsg != '')
                        alert(error.ErrMsg);
                });
    }
    resetError() {
        const editISUMForm = this.$scope.editISUMForm;
        //editISUMForm.PrimaryExportPath.$error.url = false;
        editISUMForm.PackageDataListJSON.$error.json = false;
    }
    reload() {
        this.isLoading = true;
        ISUMConfig.getList()
            .then((xs) => {
                this.ISUMs = xs;
                for (var i in this.ISUMs) {
                    if (this.ISUMs[i].IsDefault) {
                        this.currentDefaultMsgId = this.ISUMs[i].MsgId;
                        break;
                    }
                }
                this.isLoading = false;
            });
    }
}

angular.module("app", ["ngMessages", "ngSanitize","ui.bootstrap"])
    .factory("serviceGen", serviceGen)
    .factory("ISUMConfig", ISUMConfigFactory)
    .controller("ISUMManager", ISUMManager)
;
