import * as angular from "angular";
import { IPromise } from "angular";

import {serviceGen, IService, IServiceGen} from "../../ng-common/serviceGen";
import {Dialog, IDialog               } from "../../ng-common/dialog";
import {selectWhenClicked             } from "../../ng-directives/selectWhenClicked";
import {sortHandle, OrderSpec         } from "../../ng-directives/sortIcon";

class BrowserUsage {
    Version: string;
    Users  : number;
    Percent: number;

    constructor(d?: BrowserUsage) {
        this.Version = "";
        this.Users   = 0;
        this.Percent = 0;
        Object.assign(this, d);
    }
}
class OsUsage {
    Os       : string;
    userCount: number;
    constructor(d?: OsUsage) {
        this.Os        = "";
        this.userCount = 0;
        Object.assign(this, d);
    }
}
class BrowserUsageStat {
    totalInactive: number;
    totalActive  : number;
    versionUsage : BrowserUsage;
    osUsage      : OsUsage;
    constructor(d?: BrowserUsageStat) {
        this.totalInactive = 0;
        this.totalActive   = 0;
        this.versionUsage  = new BrowserUsage();
        this.osUsage       = new OsUsage();

        Object.assign(this, d);
    }

    static get(): IPromise<BrowserUsageStat> {
        return BrowserUsageStat.service<BrowserUsageStat>("Get")
            .then((s: BrowserUsageStat) => new BrowserUsageStat(s));
    }

    private static service: IService;

    static ngName = "BrowserUsageStat";
    static ngFactory(serviceGen: IServiceGen):typeof BrowserUsageStat {
        BrowserUsageStat.service = serviceGen();
        return BrowserUsageStat;
    }
}
BrowserUsageStat.ngFactory.$inject = ("serviceGen").split(" ");

import * as appHtml from "./app.html";
class BusController {
    entry       : BrowserUsageStat;

    browserUSort: OrderSpec;
    osUSort     : OrderSpec;

    static $inject = ("dialog BrowserUsageStat").split(" ");
    constructor(private dialog: IDialog) {
        this.entry        = new BrowserUsageStat();
        this.browserUSort = new OrderSpec({by : "Version", desc: false});
        this.osUSort      = new OrderSpec({ by: "os", desc     : false });

        this.reload();
    }

    reload() {
        BrowserUsageStat.get().then(e => this.entry = e);
    }

    static ngName = "BrowserUsageStat";
}

angular.module("app", [])
    .directive ("selectWhenClicked"    , selectWhenClicked           )
    .service   ("dialog"               , Dialog                      )
    .directive ("sortHandle"           , sortHandle                  )

    .factory   ("serviceGen"           , serviceGen                  )

    .factory   (BrowserUsageStat.ngName, BrowserUsageStat.ngFactory  )

    .component("browserUsageStatistics", { controller: BusController, controllerAs: "vm", template: appHtml })
;
