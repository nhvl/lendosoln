import "../../../utils/Array.orderBy";

import Ractive from "ractive";
import "../../../Ractive/misc";

import * as moment from "moment";

import {StrGuid} from "../../../utils/guid";

import {serviceGen          } from "../../../utils/serviceGen";
import {documentReadyPromise} from "../../../utils/documentReadyPromise";
import {autofocusDecorator  } from "../../../Ractive/decorators/autofocus";
import {Permission          } from "../Permission";

import * as appHtml from "./app.html";

declare const VRoot: string;
const $ = jQuery;

//#region Models
const service = serviceGen();

class User {
    EmployeeId   : StrGuid;
    BrokerId     : StrGuid;
    UserId       : StrGuid;
    LoginNm      : string;
    FullName     : string;
    Email        : string;
    BrokerNm     : string;
    BranchNm     : string;
    RecentLoginD : Date;
    Type         : string;
    IsActive     : boolean;
    CanLogin     : boolean;
	EnableRetailTpoPortalMode : boolean;

    mRecentLoginD: moment.Moment;

    constructor(d: User){
        this.EmployeeId     = d.EmployeeId;
        this.BrokerId       = d.BrokerId;
        this.UserId         = d.UserId;
        this.LoginNm        = d.LoginNm;
        this.FullName       = d.FullName;
        this.Email          = d.Email;
        this.BrokerNm       = d.BrokerNm;
        this.BranchNm       = d.BranchNm;
        this.RecentLoginD   = d.RecentLoginD;
        this.Type           = d.Type;
        this.IsActive       = d.IsActive;
        this.CanLogin       = d.CanLogin;
		this.EnableRetailTpoPortalMode = d.EnableRetailTpoPortalMode;

        this.mRecentLoginD = moment(this.RecentLoginD);
    }
    become() {
        return (service<BecomeRequestResult>("BecomeUser",
            { brokerId: this.BrokerId, userId: this.UserId })
            .then((r: BecomeRequestResult) => ((r.IsSuccess) ? r.RedirectUrl : Promise.reject<string>(r.Message))));
    }
    becomePml(){
        return (service<BecomeRequestResult>("BecomePml", {userId: this.UserId}).
            then((r: BecomeRequestResult) => ((r.IsSuccess) ? r.RedirectUrl : Promise.reject<string>(r.Message))));
    }
	becomeRetail() {
		return (service<BecomeRequestResult>("BecomeRetail",
            { brokerId: this.BrokerId, userId: this.UserId })
            .then((r: BecomeRequestResult) => ((r.IsSuccess) ? r.RedirectUrl : Promise.reject<string>(r.Message))));
	}

    static search(form: SearchForm) {
        return (
            service<User[]|string>("Search", form)
            .then((xs: (User[]|string)) => ((typeof xs === "string") ?
                    Promise.reject<User[]>(xs) :
                    (xs).map(x => new User(x)))));
    }
}

class BecomeRequestResult {
    IsSuccess  : boolean;
    Message    : string;
    RedirectUrl: string;
}

class SearchForm {
    login         : string;
    employeeName  : string;
    email         : string;
    broker        : string;
    userId        : string;
    status        : string;
    type          : string;
    employeeId    : string;
    brokerStatus  : boolean;

    error         : {
        userId    : boolean,
        employeeId: boolean,
    }

    constructor(){
        this.login          = "";
        this.employeeName   = "";
        this.email          = "";
        this.broker         = "";
        this.userId         = "";
        this.status         = "active";
        this.type           = "B";
        this.employeeId     = "";
        this.brokerStatus   = true;

        this.error = {
            userId    : false,
            employeeId: false,
        };
    }
}
//#endregion

//#region RactiveJS Components
const Main = Ractive.extend({
    template: appHtml,
    data() {
        return {
            users       : null as (User[]|null),
            isSubmitting: false,
            permission  : Permission.Default,
            sForm       : new SearchForm(),
            errorMessage: "",
            overflow    : false,
            sort : {
                by : "",
                asc: true,
            },
        }
    },
    onrender(this:Ractive.Ractive) {
        this.on({
            resetForm(this: Ractive.Ractive, event:Ractive.Event) {
                const sForm = new SearchForm();
                this.set({sForm, errorMessage:null});
            },
            search(this: Ractive.Ractive, event: Ractive.Event) {
                try {
                    if (this.get("isSubmitting")) return false;

                    this.set({
                        "isSubmitting"          : true,
                        "overflow"              : false,
                        "users"                 : null,
                        "sForm.error.employeeId": false,
                        "sForm.error.userId"    : false,
                        "errorMessage"          : "",
                    });

                    User.search(this.get("sForm")).
                    then((rs:User[]) => {
                        this.set({
                            "isSubmitting": false,
                            "errorMessage": "",
                            "users"       : rs.map(u => new User(u)),
                            "overflow"    : rs.length > 99,
                        });
                    }, (errorMessage:string) => {
                        this.set({ "isSubmitting": false });
                        switch (errorMessage){
                            case "Invalid EmployeeId.":
                                this.set("sForm.error.employeeId", true);
                                break;
                            case "Invalid UserId.":
                                this.set("sForm.error.userId", true);
                                break;
                            default:
                                this.set({errorMessage});
                                break;
                        }
                        setTimeout(() => { $("form .has-error input").focus(); }, 100);
                    });
                } catch(e) { console.error(e); }

                return false;
            },
            sort(this: Ractive.Ractive, event:Ractive.Event, sortByProp:string) {
                const sort = this.get("sort");
                if (sort.by === sortByProp) sort.asc = !sort.asc;
                else sort.by = sortByProp;

                const users = (this.get("users") as User[]).orderBy(sort.by, !(sort.asc));
                this.set({ sort, users });
            },

            editEmployee(event: Ractive.Event, user:User) {
                try {
                    window.open(`${VRoot}/LOAdmin/Broker/EditEmployee.aspx?employeeid=${user.EmployeeId}&brokerid=${user.BrokerId}`);
                }
                catch(e){
                    window.status = e.message;
                }
                return false;
            },
            editUser(event: Ractive.Event, user:User) {
            try {
                    window.open(`${VRoot}/LOAdmin/Broker/EditUser.aspx?userid=${user.UserId}&brokerid=${user.BrokerId}`);
                }
                catch(e){
                    window.status = e.message;
                }
                return false;
            },
            showHistory(event: Ractive.Event, user:User) {
                alert('This feature is no longer available.');
                return false;
            },
            editBroker(this: Ractive.Ractive, event: Ractive.Event, user:User) {
                try {
                    const result: any = window.open(`${VRoot}/LOAdmin/Broker/BrokerEdit.aspx?cmd=edit&brokerid=${user.BrokerId}`);

                    if (result.OK == true && result.commandName != null && result.commandArgs != null) {
                        const eventToProcess = result.commandName + "*" + result.commandArgs;
                        (document.getElementsByName("m_EventToProcess")[0] as HTMLInputElement).value = eventToProcess;

                        location.reload(false); // or we can trigger on("search") but how?
                    }
                } catch( e ) {
                    window.status = e.message;
                }
                return false;
            },

            becomeUser(this: Ractive.Ractive, event: Ractive.Event, user:User) {
                if (this.get("isSubmitting")) return false;

                this.set("isSubmitting", true);
                user.become().then(
                    (url:string) => { if (!!url) window.location.href = url; },
                    (errorMessage:string) => { this.set({errorMessage}); }
                );

                return false;
            },
            becomePmlUser(this: Ractive.Ractive, event: Ractive.Event, user:User) {
                if (this.get("isSubmitting")) return false;

                this.set("isSubmitting", true);
                user.becomePml().then(
                    (url: string) => { if (!!url) window.location.href = url; },
                    (errorMessage: string) => { this.set({ errorMessage }); }
                );

                return false;
            },
            becomeRetail(this: Ractive.Ractive, event: Ractive.Event, user:User) {
                if (this.get("isSubmitting")) return false;

                this.set("isSubmitting", true);
                user.becomeRetail().then(
                    (url:string) => { if (!!url) window.location.href = url; },
                    (errorMessage:string) => { this.set({errorMessage}); }
                );

                return false;
            },
        });

        try {
            const m_ErrorMessage = (document.getElementsByName("m_EventToProcess")[0] as HTMLInputElement);
            // Look for error and report if found.
            if ((m_ErrorMessage != null) && !(m_ErrorMessage.value))
                alert(`Error: ${m_ErrorMessage.value}`);
        } catch (e) {
            window.status = e.message;
        }
    },
    decorators: {
        autofocus: autofocusDecorator,
    },
    sanitize: true,
});
//#endregion

init();
async function init(){
    const p = Permission.Get();
    await documentReadyPromise;

    const permission = await p;

    new Main({
        el: "container",
        lazy: true,
        data: { permission },
    });
}
