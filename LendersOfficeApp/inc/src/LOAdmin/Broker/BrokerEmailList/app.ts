import "../../../Ractive/misc";

import {serviceGen          } from "../../../utils/serviceGen";
import {getQueryStringValue } from "../../../utils/getQueryStringValue";
import {copyToClipboard,
        ICopyFunc           } from "../../../utils/copyToClipboard";
import {documentReadyPromise} from "../../../utils/documentReadyPromise";

import * as appHtml from "./app.html";


const $ = jQuery;

// Helper functions
    function emailsToList(emails:string){
        return emails.split(/;|,/).map(e => e.trim()).filter(e => !!e);
    }
    function flatten<T>(xss:T[][]){
        return (<T[]> []).concat(...xss);
    }
    function isArraySame<T>(xs: T[], ys:T[]){
        if (xs.length !== ys.length) return false;
        return xs.every((x,i) => x == ys[i]);
    }

// Models
    const service = serviceGen();

    interface IBrokerData {
        BrokerId: string;
        BrokerNm: string;
        Emails: string[];
        IsInclude: boolean;
    }

    class Broker implements IBrokerData {
        BrokerId : string;
        BrokerNm : string;
        Emails   : string[];
        IsInclude: boolean;

        emails: string;
        saveError:boolean;

        constructor(d?: IBrokerData) {
            if (d == null)
                d = <IBrokerData> {
                    BrokerId : "",
                    BrokerNm : "",
                    Emails   : <string[]> [],
                    IsInclude: false,
                };

            this.BrokerId      = d.BrokerId;
            this.BrokerNm      = d.BrokerNm;
            this.Emails        = d.Emails;
            this.IsInclude     = d.IsInclude;

            if (this.Emails == null) this.Emails = <string[]>[];
            this.emails = this.Emails.join("; ");

            this.saveError = false;
        }

        static get(){
            return (service<IBrokerData[]>("Get")
                .then((xs: IBrokerData[]) => xs.map(x => new Broker(x)))
            );
        }
        static save(xs: Broker[]):Promise<string[]> {
            let changedXs = xs;

            changedXs.forEach(x => x.Emails = emailsToList(x.emails));

            if (changedXs.length < 1) {
                // return Promise.resolve([]);
                changedXs = xs;
            }

            return (service("Save", {"brokers": changedXs.map(x => {return {BrokerId:x.BrokerId, Emails:x.Emails, IsInclude:x.IsInclude}; })})
                //.then(rs => isSuccess ? true : Promise.reject())
            );
        }
    }
// ------

// RactiveJS Components
    interface IMainData {
        brokers ?: Broker[],
        isSubmitting ?: boolean,

        saveSuccess ?: boolean,
        errorMessage ?: string,

        sort ?: {
            by: string,
            asc: boolean,
        },

        validateEmails?: (emails: string) => boolean,
    }

    const Main = Ractive.extend({
        template: appHtml,
        data(): IMainData{ return{
            brokers: null,
            isSubmitting: false,

            saveSuccess: false,
            errorMessage: null,

            sort : {
                by: null,
                asc: true,
            },

            validateEmails(emails:string) {
                const EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
                const s = emails.trim();
                if (s.length < 1) return true;
                return (emailsToList(s).every(e => EMAIL_REGEXP.test(e)));
            },
        }},
        onrender(){
            var copy: ICopyFunc = null;

            this.on({
                save(event: Ractive.Event) {
                    const brokers = this.get("brokers");

                    this.set({errorMessage:null, saveSuccess:false, isSubmitting:true});

                    Broker.save(brokers).then((xs) => {
                        this.set({isSubmitting:false});

                        if (xs.length < 1) {
                            this.set({saveSuccess:true});
                            return;
                        }

                        const brokers = <Broker[]> this.get("brokers");
                        const dict: { [id: string]: Broker } = brokers.reduce((d: { [bId: string]: any }, b: Broker) => { d[b.BrokerId] = b; return d; }, <{ [bId: string]: any }> {});
                        this.set(<IMainData>{errorMessage: `Fail to save for broker(s): ${xs.map((id:string) => dict[id].BrokerNm).join(", ")}.`});
                    });
                },
                generate(event:Ractive.Event) {
                    const brokers = <Broker[]> this.get("brokers");
                    const s = emailsToList(brokers.filter(b => b.IsInclude).map(b => b.emails).join(";")).join("; ");

                    if (copy == null) copy = copyToClipboard();
                    copy(s, event.original, "Email list has been copied to the clipboard.");
                },
            });
        },
    });
// --------------------

function init(brokers:Broker[]){
    new Main({
        el: "container",
        //lazy: true,
        data: { brokers },
    });
}

Promise.all([
    <Promise<Broker[]>> Broker.get(),
    documentReadyPromise,
]).then(([brokers]: any[]) => { init(brokers); });
