import "core-js/modules/es6.object.assign";

import {SearchForm} from "./SearchForm";
import {IServiceGen, IService } from "../../../ng-common/serviceGen";

type Guid = string;
type int = number;

let $q: ng.IQService;
let service: IService;

export class Loan {
    static ngName = "Loan";

    sLId                 : Guid   ;
    sOldLNm              : string ;
    DeletedD             : Date|null;
    LoanStatus           : string ;
    sPrimBorrowerFullNm  : string ;
    aBSsn                : string ;
    FullAddr             : string ;
    sEmployeeLoanRepName : string ;
    sLienPosT            : int|null;
    IsTemplate           : boolean|null;
    sLoanFileT           : string ;

    constructor(d?: Loan) {
        this.sLId                 = "";
        this.sOldLNm              = "";
        this.DeletedD             = null;
        this.LoanStatus           = "";
        this.sPrimBorrowerFullNm  = "";
        this.aBSsn                = "";
        this.FullAddr             = "";
        this.sEmployeeLoanRepName = "";
        this.sLienPosT            = null;
        this.IsTemplate           = null;
        this.sLoanFileT           = "";

        if (d != null) Object.assign(this, d);
        return this;
    }

    undelete(restoreUserFirstName: string, restoreUserLastName: string): ng.IPromise<boolean> {
        return service<boolean>("UndeleteLoan", { sLId: this.sLId, restoreUserFirstName, restoreUserLastName });
    }

    static get(searchForm: SearchForm): ng.IPromise<Loan[]> {
        return service<Loan[]|string>("Search", searchForm)
            .then((es:(Loan[]|string)) => (typeof es === "string") ? $q.reject(es) as any : es.map(e => new Loan(e)));
    }

    static ngFactory(serviceGen: IServiceGen, _$q: ng.IQService): typeof Loan {
        service = serviceGen();
        $q = _$q;
        return Loan;
    }
}
Loan.ngFactory.$inject = ["serviceGen", "$q"];


