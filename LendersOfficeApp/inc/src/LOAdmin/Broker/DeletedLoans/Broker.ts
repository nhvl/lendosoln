import "core-js/modules/es6.object.assign";

import {IServiceGen, IService } from "../../../ng-common/serviceGen";

type Guid = string;
export class Broker {
    static ngName = "Broker";

    BrokerId : Guid  ;
    BrokerNm : string;

    constructor(d?: Broker) {
        this.BrokerId = "";
        this.BrokerNm = "";

        if (d != null) Object.assign(this, d);
        return this;
    }
    
    checkValid(restoreUserFirstName: string, restoreUserLastName: string): ng.IPromise<boolean> {
        const service = Broker.service;

        return service<boolean>("CheckValid", {
            brokerId: this.BrokerId, brokerName: this.BrokerNm,
            restoreUserFirstName, restoreUserLastName
        });
    }

    static get(): ng.IPromise<Broker[]> {
        const service = Broker.service;

        return service<Broker[]>("GetBrokers")
            .then((bs:(Broker[])) => bs.map(b => new Broker(b)));
    }

    static $q :ng.IQService;
    static service: IService;

    static ngFactory(serviceGen: IServiceGen, $q: ng.IQService): typeof Broker {
        Broker.service = serviceGen();
        Broker.$q = $q;
        return Broker;
    }
}
Broker.ngFactory.$inject = ["serviceGen", "$q"];


