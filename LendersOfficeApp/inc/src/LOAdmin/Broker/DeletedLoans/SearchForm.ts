
type Guid = string;
type int = number;
type Ssn = string;

export class SearchForm {
    sBrokerID        : Guid;
    daysSinceDeletion: int;
    borrowerName     : string;
    aBSsn            : string;
    sOldLNm          : string;
    sLId             : Guid;

    constructor() {
        this.sBrokerID         = "";
        this.daysSinceDeletion =  1;
        this.borrowerName      = "";
        this.aBSsn             = "";
        this.sOldLNm           = "";
        this.sLId              = "";
    }

    static daysSinceDeletionOptions = [
        { v:   1, t: "1 day    " },
        { v:   2, t: "2 days   " },
        { v:   3, t: "3 days   " },
        { v:   4, t: "4 days   " },
        { v:   5, t: "5 days   " },
        { v:   6, t: "6 days   " },
        { v:   7, t: "Week     " },
        { v: 7*2, t: "2 Weeks  " },
        { v: 7*3, t: "3 Weeks  " },
        { v: 7*4, t: "4 Weeks  " },
        { v:  30, t: "30 Days  " },
        { v:  60, t: "60 Days  " },
        { v:  -2, t: "> 60 Days" },
    ];
}
