import * as angular from "angular";
import {IDirective} from "angular";
import "angular-messages";

import {StrGuid} from "../../../utils/guid";

import {serviceGen,          } from "../../../ng-common/serviceGen";
import {Dialog, IDialog      } from "../../../ng-common/dialog";
import {selectWhenClicked    } from "../../../ng-directives/selectWhenClicked";
import {sortHandle, OrderSpec} from "../../../ng-directives/sortIcon";

import {SearchForm} from "./SearchForm";
import {Loan      } from "./Loan";
import {Broker    } from "./Broker";

import * as appHtml from "./app.html";
class DlManagerController {
    entries      : Loan[]|null;
    entrySort    : OrderSpec;


    isLoading    : boolean;

    searchForm   : SearchForm;
    brokerOptions: Broker[];
    daysSinceDeletionOptions: { v: number, t: string }[]; // typeof SearchForm.daysSinceDeletionOptions

    restoreUserFirstName: string;
    restoreUserLastName : string;

    static $inject = ["dialog", Loan.ngName, Broker.ngName];
    constructor(private dialog: IDialog) {
        this.entrySort = new OrderSpec({ by: "FileName", desc:false });

        this.isLoading = false;

        this.searchForm = new SearchForm();
        this.daysSinceDeletionOptions = SearchForm.daysSinceDeletionOptions;

        this.brokerOptions = [];
        Broker.get().then(bs => {
            this.brokerOptions = bs;
            this.searchForm.sBrokerID = bs[0].BrokerId;
        });

        this.entries = null;
        this.broker = null;
    }

    private broker:Broker|null;
    search() {
        this.isLoading = true;
        Loan.get(this.searchForm)
            .then(
                (xs: Loan[]) => {
                    this.entries = xs;
                    this.broker = this.brokerOptions.find(b => this.searchForm.sBrokerID === b.BrokerId)!;
                    console.assert(this.broker != null, "Can not found broker???");
                },
                (m: string) => { this.dialog.alert(m, "Error"); })
            .finally(() => this.isLoading = false);
    }

    undelete(entry: Loan) {
        if (!this.restoreUserFirstName || !this.restoreUserLastName) {
            this.dialog.alert("Please insert the user name that requested the undelete in the Requested By field");
            return;
        }
        if (this.broker == null) { console.error("missing broker"); return; }
        this.broker.checkValid(this.restoreUserFirstName, this.restoreUserLastName)
            .then((isValid: boolean) => {
                if (isValid) return true;
                return this.dialog.confirm("The given user name was not recognized as a user. Do you wish to use this user name anyway?");
            })
            .then((isConfirmed) => {
                if (!isConfirmed) return;
                entry.undelete(this.restoreUserFirstName, this.restoreUserLastName)
                    .then((isSuccess) => {
                        if (!isSuccess) {
                            this.dialog.alert("Failed to restore loan.", "Fail");
                            return;
                        }

                        this.search();

                        this.dialog.alert(`Successfully restored loan with id: ${entry.sLId}.`, "Success");
                    });
            });
    }
    viewAuditDetail(loan: Loan) {
        window.open(`AuditHistoryList.aspx?loanid=${loan.sLId}`, "AuditHistory", "toolbar=no,menubar=no,location=no,status=no,resizable=yes, scrollbars=yes");
    }
}

function sLienPosT2string() {
    return (sLienPosT: string) => sLienPosT == "0" ? "1st" : "2nd";
}

function isTemplate2string() {
    return (isTemplate: boolean) => isTemplate ? "Yes" : "No";
}

angular.module("app", ["ngMessages"])
    .factory  ("serviceGen"              , serviceGen)
    .directive("selectWhenClicked"       , selectWhenClicked)
    .service  ("dialog"                  , Dialog)

    .directive("sortHandle"             , sortHandle)

    .filter("sLienPosT2string", sLienPosT2string)
    .filter("isTemplate2string", isTemplate2string)

    .factory(Loan  .ngName, Loan  .ngFactory)
    .factory(Broker.ngName, Broker.ngFactory)

    .component("dlManager", {controller: DlManagerController, controllerAs: "vm", template: appHtml})
;
