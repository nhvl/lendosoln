import {serviceGen} from "../../utils/serviceGen";

const service = serviceGen("FindUser.aspx");

export class Permission {
    EditEmployee : boolean;
    BecomeUser   : boolean;
    CreateBroker : boolean;
    EditBroker   : boolean;
	ViewActivity : boolean;
	ViewBrokers  : boolean;

    constructor(d?:Permission){
        if (d != null) {
            this.EditEmployee = !!d.EditEmployee;
            this.BecomeUser   = !!d.BecomeUser  ;
            this.CreateBroker = !!d.CreateBroker;
            this.EditBroker   = !!d.EditBroker  ;
			this.ViewActivity = !!d.ViewActivity;
			this.ViewBrokers  = !!d.ViewBrokers ;
        }
    }

    static Get() {
        return service<Permission>("GetPermission").
            then((p:Permission) => new Permission(p),
                 () => Permission.Default);
    }
    static Default = new Permission();
}
