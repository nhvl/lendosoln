import Ractive from "ractive";
import {ContextEvent} from "../../../Ractive/misc";

import "../../../utils/Array.orderBy";
import {serviceGen          } from "../../../utils/serviceGen";
import {documentReadyPromise} from "../../../utils/documentReadyPromise";
import {autofocusDecorator  } from "../../../Ractive/decorators/autofocus";
import {Permission          } from "../Permission";
import {BrokerSuiteType     } from "../../../DataLayer/Enums/BrokerSuiteType";

import * as appHtml from "./app.html";

const $       = jQuery;
declare const VRoot: string;

//#region Models
const service = serviceGen();

class Broker {
    BrokerNm               : string;
    Notes                  : string;
    BrokerId               : string|null;
    BrokerAddress          : string;
    BrokerPhone            : string;
    BrokerFax              : string;
    CustomerCode           : string;
    CreditMornetPlusUserId : string;
    IsActive               : boolean;
	SuiteType			   : BrokerSuiteType;
	SuiteTypeRep		   : string;

    showNotes              : boolean;
    constructor(b?: any){
        if (b == null)
            b =  {
                BrokerNm              : "",
                Notes                 : "",
                BrokerId              : null,
                BrokerAddress         : "",
                BrokerPhone           : "",
                BrokerFax             : "",
                CustomerCode          : "",
                CreditMornetPlusUserId: "",
                IsActive              : true,
				SuiteType			  : BrokerSuiteType.Blank
            };
			
		b.SuiteTypeRep = this.getSuiteTypeRep(b.SuiteType);
        Object.assign(this, b);

        this.showNotes               = false;
    }
	
	getSuiteTypeRep(suiteType:BrokerSuiteType) : string {
		switch (suiteType) {
			case BrokerSuiteType.Blank:
				return "";
			case BrokerSuiteType.ClientLqb:
				return "Client - LQB";
			case BrokerSuiteType.ClientPmlOnly:
				return "Client - PML Only";
			case BrokerSuiteType.ClientIls:
				return "Client - ILS";
			case BrokerSuiteType.InternalDemo:
				return "Internal Demo";
			case BrokerSuiteType.InternalTest:
				return "Internal Test";
			case BrokerSuiteType.LoadTest:
				return "Load Test";
			default:
				return "Vendor Test";
		}
	}

    static search(form: SearchForm): Promise<Broker[]> {
        return service<Broker[]|string>("Search", form)
            .then((xs: (Broker[]|string)) => ((typeof xs === "string") ?
                    Promise.reject<Broker[]>(xs) :
                    xs.map(x => new Broker(x))));
    }
}

class SearchForm {
    company      : string;
    isActive     : boolean;
    customerCode : string;
    duUserId     : string;
    brokerId     : string;
	suiteType    : BrokerSuiteType|null;

    error        : {
        brokerId :boolean,
    };

    constructor(){
        this.company        = "";
        this.isActive       = true;
        this.customerCode   = "";
        this.duUserId       = "";
        this.brokerId       = "";
		this.suiteType      = null;

        this.error = {
            brokerId: false,
        };
    }
}
// ------

// RactiveJS Components
interface IMainData {
    brokers      ?: Broker[],
    permission   ?: Permission,
    sForm        ?: SearchForm,
    errorMessage ?: string,
    sort         ?: {
        by       ?: string,
        asc      ?: boolean,
    },
}

const Main = Ractive.extend({
    data(): IMainData {
        return {
            brokers     : undefined,
            permission  : Permission.Default,
            sForm       : new SearchForm(),
            errorMessage: undefined,
            sort        : {
                by      : undefined,
                asc     : true,
            }
        };
    },
    onrender(this: Ractive.Ractive){
        this.on({
            resetForm(this: Ractive.Ractive, context:ContextEvent) {
                this.set({
                    sForm       : new SearchForm(),
                    errorMessage: null,
                });

                return false;
            },
            search(this:Ractive.Ractive, context: ContextEvent) {
                try {
                    if (this.get("isSubmitting")) return false;

                    this.set({
                        isSubmitting: true,
                        brokers     : undefined,
                    } as IMainData);

                    Broker.search(this.get("sForm"))
                    .then((xs:Broker[]) => {
                        this.set({
                            isSubmitting          : false,
                            "sForm.error.brokerId": false,
                            errorMessage          : undefined,
                            brokers               : xs,
                        } as IMainData);
                    }, (errorMessage:string) => {
                        this.set({
                            isSubmitting: false,
                            errorMessage,
                        } as IMainData);
                        switch (errorMessage){
                            case "Invalid EmployeeId.":
                                this.set("sForm.error.employeeId", true);
                                break;
                            case "Invalid UserId.":
                                this.set("sForm.error.userId", true);
                                break;
                        }
                        setTimeout(() => {
                            const input = document.querySelector("form .has-error input") as HTMLInputElement;
                            if (input == null) return;
                            input.focus();
                        }, 50);
                    });
                } catch(e) { console.error(e); }

                return false;
            },
            sort(this: Ractive.Ractive, context: ContextEvent, sortByProp:string){
                const sort = this.get("sort");
                if (sort.by === sortByProp) sort.asc = !sort.asc;
                else sort.by = sortByProp;

                const brokers = (this.get("brokers") as Broker[]).orderBy(sort.by, !(sort.asc));
                this.set({ sort, brokers });
            },

            addNewBroker(context: ContextEvent){
                try {
                    const result: any = window.open(`${VRoot}/LOAdmin/Broker/BrokerEdit.aspx?cmd=add`);
                    if (result.OK === true) document.location.reload(false);
                }
                catch(e) {
                    window.status = e.message;
                }
                return false;
            },
            editBroker(context: ContextEvent, broker:Broker) {
                try {
                    if ((context.event as MouseEvent).shiftKey) {
                        window.open(`${VRoot}/LOAdmin/Broker/BrokerEdit.aspx?cmd=edit&brokerid=${broker.BrokerId}`);
                        return false;
                    }
                    const result: any = window.open(`${VRoot}/LOAdmin/Broker/BrokerEdit.aspx?cmd=edit&brokerid=${broker.BrokerId}`);

                    if( result.OK === true && result.commandName != null && result.commandArgs != null )
                    {
                        const eventToProcess = result.commandName + "*" + result.commandArgs;
                        (document.getElementsByName("m_EventToProcess")[0] as HTMLInputElement).value = eventToProcess;

                        document.location.reload(false);
                    }
                    return false;
                } catch( e ) {
                    console.error(e);
                    window.status = e.message;
                }

                return false;
            },
            showNotes(this: Ractive.Ractive, event: ContextEvent, brokerIndex:number) {
                this.toggle(`brokers[${brokerIndex}].showNotes`);
            },
        });

        try {
            const m_ErrorMessage = document.getElementsByName("m_EventToProcess")[0] as HTMLInputElement;
            // Look for error and report if found.
            if ( (m_ErrorMessage != null) && !(m_ErrorMessage.value))
                alert( `Error: ${m_ErrorMessage.value}` );
        } catch( e ) {
            window.status = e.message;
        }
    },
    decorators: {
        autofocus: autofocusDecorator,
    },
    sanitize: true,
    template: appHtml,
});
// --------------------

init();
async function init(){
    const permissionPromise = Permission.Get();
    await documentReadyPromise;

    const permission = await permissionPromise;

    new Main({
        el: "container",
        lazy: true,
        data: { permission },
    });
}
