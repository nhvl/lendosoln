import * as angular from "angular";

import {serviceGen, IService, IServiceGen} from "../../../ng-common/serviceGen";

import * as appHtml from "./app.html";

declare var validateOperations: any;

class OperationItem
{
	public Id         : string;
	public Description: string;
	public ParentId   : string;
	public Focused    : boolean;
	public Children   : OperationItem[];
	public Disabled   : boolean;
	public IsParent   : boolean;

	constructor(id: string, description: string, parentId: string = "", children: OperationItem[] = [], disabled: boolean = false, isParent = false) {
		this.Id          = id;
		this.Description = description;
		this.ParentId    = parentId;
		this.Focused     = false;
		this.Children    = children;
		this.Disabled    = disabled;
		this.IsParent    = isParent;
	}
}

class WorkflowOperationsController {

	AvailableOperations: OperationItem[];
	SelectedOperations: OperationItem[];
	IsEnforceExclusive: boolean;

	static service: any;

	constructor()
	{
		let dataSource: any = angular.fromJson(angular.fromJson((document.getElementById("DataSourceJson") as HTMLInputElement).value));
		this.AvailableOperations = dataSource.AvailableOperations;
		this.SelectedOperations = dataSource.SelectedOperations;
		this.IsEnforceExclusive = dataSource.IsEnforceExclusive;
		this.runValidation(false);
	}

	focusOperation(operation: OperationItem)
	{
		if(operation.Disabled || operation.IsParent)
		{
			return;
		}
		else if(operation.ParentId == undefined || operation.ParentId == "")
		{
			operation.Focused = !operation.Focused;
		}
		else
		{
			// This is an exclusive operation.  Ensure another child isn't selected.
			// Disable the siblings once one is selected.
			let parentOperation = this.retrieveOperation(this.AvailableOperations, operation.ParentId);
			if (parentOperation == null) { console.error("parentOperation is null"); return; }

			if(this.IsEnforceExclusive)
			{
				parentOperation.Children.forEach((item: OperationItem) => {
					if(item.Id != operation.Id) {
						item.Focused = false;
					}
				});
			}

			operation.Focused = !operation.Focused;
			operation.Disabled = false;
		}
	}

	retrieveOperation(list: OperationItem[], id: string)	{
		for(let i = 0; i < list.length; i++)
		{
			if(list[i].Id == id)
			{
				return list[i];
			}
		}

		return null;
	}

	createOperationCopy(operation: OperationItem)
	{
		let copyOperation: OperationItem = new OperationItem(operation.Id, operation.Description, operation.ParentId, operation.Children, operation.Disabled, operation.IsParent);
		return copyOperation;
	}

	selectOperations(selectAll: boolean)
	{
		let operationsToSelect: OperationItem[] = [];
		let parentsToRemove: OperationItem[] = [];

		for(let i = 0; i < this.AvailableOperations.length; i++)
		{
			let item: OperationItem = this.AvailableOperations[i];

			if((item.Focused || selectAll) && !item.IsParent)
			{
				operationsToSelect.push(item);
				item.Focused = false;
			}
			else if(item.IsParent)
			{
				// We cannot select exclusive operations when selecting all.
				let focusedChild: OperationItem[] = item.Children.filter(function(operation: OperationItem){ return operation.Focused; });
				if(focusedChild.length > 0 || (selectAll && !this.IsEnforceExclusive))
				{
					let selectedParent = this.retrieveOperation(this.SelectedOperations, item.Id);
					if(selectedParent == null)
					{
						selectedParent = this.createOperationCopy(item);
						selectedParent.Children = [];
						this.SelectedOperations.push(selectedParent);
					}

					if(selectAll && !this.IsEnforceExclusive)
					{
						selectedParent.Children = selectedParent.Children.concat(item.Children);
						parentsToRemove.push(item);
					}
					else
					{
						focusedChild.forEach(function(operation: OperationItem){ operation.Focused = false; });
						selectedParent.Children = selectedParent.Children.concat(focusedChild);
						item.Children = this.excludeArray(item.Children, focusedChild);
					}

					if(item.Children.length === 0)
					{
						parentsToRemove.push(item);
					}
				}

				if(this.IsEnforceExclusive && focusedChild.length > 0)
				{
					item.Children.forEach(function(operation: OperationItem){ operation.Disabled = true; });
				}
			}
		}

		this.SelectedOperations = this.SelectedOperations.concat(operationsToSelect);
		this.AvailableOperations = this.excludeArray(this.AvailableOperations, operationsToSelect);
		this.AvailableOperations = this.excludeArray(this.AvailableOperations, parentsToRemove);

		(document.getElementById("DataSourceJson") as HTMLInputElement).value = angular.toJson(this);

		this.runValidation(true);
	}

	//  EditWorkflowConfigItem.aspx relies on calling the validateOperations method to make the Writeable Fields section appear.
	// If the method exists, we call it.
	runValidation(doPostback: boolean)
	{
		if(typeof(validateOperations) != "undefined")
		{
			let writeLoan = this.retrieveOperation(this.SelectedOperations, "WriteField");
			let LoanStatusChange = this.retrieveOperation(this.SelectedOperations, "LoanStatusChange");
			validateOperations(doPostback, writeLoan != null, LoanStatusChange != null);
		}
	}

	removeOperations(selectAll: boolean)
	{
		let parentsToMerge: OperationItem[] = [];
		let itemsToRemove: OperationItem[] = [];

		for(let i = 0; i < this.SelectedOperations.length; i++)
		{
			let item: OperationItem = this.SelectedOperations[i];

			if((item.Focused || selectAll) && !item.IsParent)
			{
				this.AvailableOperations.push(item);
				item.Focused = false;
				itemsToRemove.push(item);
			}
			else if(item.IsParent)
			{
				let focusedChild: OperationItem[] = item.Children.filter(function(operation: OperationItem){ return operation.Focused; });

				if(focusedChild.length > 0 || selectAll)
				{
					let availableParent = this.retrieveOperation(this.AvailableOperations, item.Id);
					if(availableParent == null) {
						availableParent = this.createOperationCopy(item);
						availableParent.Children = [];
						this.AvailableOperations.push(availableParent);
					}

					if(selectAll)
					{
						availableParent.Children = availableParent.Children.concat(item.Children);
						itemsToRemove.push(item);
					}
					else
					{
						let isRemove: boolean = true;
						item.Children.forEach(function(child){
							if(child.Focused)
							{
								availableParent!.Children.push(child);
							}

							isRemove = isRemove && child.Focused;
						});

						focusedChild.forEach(function(operation: OperationItem){ operation.Focused = false; });
						item.Children = this.excludeArray(item.Children, focusedChild);

						if(isRemove)
						{
							itemsToRemove.push(item);
						}
					}

					availableParent.Children.forEach(function(item: OperationItem){ item.Disabled = false; });
				}
			}
		}

		this.SelectedOperations = this.excludeArray(this.SelectedOperations, itemsToRemove);
		this.SelectedOperations = this.excludeArray(this.SelectedOperations, parentsToMerge);

		(document.getElementById("DataSourceJson") as HTMLInputElement).value = angular.toJson(this);

		this.runValidation(true);
	}

	excludeArray(mainList: any[], toBeRemoved: any)
	{
		return mainList.filter((item) => toBeRemoved.indexOf(item) < 0);
	}

	static ngName = "WorkflowOperationsController";
}


function WorkflowOperations() : ng.IDirective {
	return {
		restrict: "E",
		scope: {},
		template: appHtml,
		controller: WorkflowOperationsController,
		controllerAs: "vm",
		bindToController: true,
	};
}

angular.module("app", [])
	.factory   ("serviceGen"        , serviceGen          )
	.directive ("workflowOperations", WorkflowOperations  )
;
