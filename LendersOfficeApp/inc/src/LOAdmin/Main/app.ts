import "../../utils/Array.orderBy";
import {serviceGen          } from "../../utils/serviceGen";
import { documentReadyPromise } from "../../utils/documentReadyPromise";

declare const VRoot: string;
const $ = jQuery;

//#region Models
const service = serviceGen();

class QueueItem {
    id   : number; // int
    name : string;
    count: number; // init

    constructor(d?: QueueItem) {
        if (d != null) {
            this.id    = d.id;
            this.name  = d.name;
            this.count = d.count;
        }
    }
    static get(): Promise<QueueItem[]> {
        return service<QueueItem[]>("GetQueueStatus").then((xs: QueueItem[]) => xs.map(x => new QueueItem(x)));
    }
}
//#endregion

import Ractive from "ractive";
import { ContextEvent } from "../../Ractive/misc";
//#region RactiveJS Components
interface ISortSpec {
    by: string;
    asc: boolean;
}
interface IQueueStatusData {
    items   ?: QueueItem[];
    sort    ?: ISortSpec;
    headers ?: {key:string, value:string}[];
}

import * as queueStatusHtml from "./QueueStatus.html";

const QueueStatusRactive = Ractive.extend({
    data: {
        items    : [],
        sort     : {
            by   : "",
            asc  : true,
        },
        headers  : [
            { key: "id", value   : "Id" },
            { key: "name", value : "Name" },
            { key: "count", value: "Count" },
        ],
    },
    on: {
        sort(this: Ractive.Ractive, context: ContextEvent, sortByProp: string) {
            const sort: ISortSpec = this.get("sort");
            if (sort.by === sortByProp) {
                sort.asc = !sort.asc;
            } else {
                sort.by = sortByProp;
            }

            const items = (this.get("items") as QueueItem[]).orderBy(sort.by, !(sort.asc));
            this.set({ sort, items } as IQueueStatusData);
        }
    },
    sanitize: true,
    template: queueStatusHtml,
});
//#endregion

init();
async function init(){
    const p = QueueItem.get();
    await documentReadyPromise;
    doMasonryLayout();

    const items: QueueItem[] = await p;

    new QueueStatusRactive({
        el      : "#loansTable",
        data    : { items } as IQueueStatusData,
    });

    doLabel();
    doMasonryLayout();
}

function doMasonryLayout() {
    ($(".row-masonry") as any).masonry({
        itemSelector: ".masonry-item",
        //columnWidth: 200,
        columnWidth: ".grid-sizer",
    });
}

function doLabel(){
    var xs = $("a.list-group-item");
    xs.filter(".rp-working").each(function(){
        $(this).append(' <span class="label label-danger">working&hellip;</span>');
    });

    xs.filter(".rp-new").each(function() {
        $(this).append(' <span class="label label-info">new</span>');
    });
}
