
import * as angular from "angular";
import "angular-messages";
import { IDirective } from "angular";

import {serviceGen,          } from "../../../ng-common/serviceGen";
import {Dialog, IDialog      } from "../../../ng-common/dialog";
import {selectWhenClicked    } from "../../../ng-directives/selectWhenClicked";
import {sortHandle, OrderSpec} from "../../../ng-directives/sortIcon";

import {StageConstant} from "./StageConstant";
import {StageConstantEditorCtrl, stageConstantEditor} from "./stageConstantEditor";

type Guid = string;

import * as appHtml from "./app.html";
class SCManagerController {
    entries     : StageConstant[]|null;
    entrySort   : OrderSpec;
    isLoading   : boolean;
    errorMessage: string;

    static $inject = ["dialog", StageConstant.ngName];
    constructor(private dialog: IDialog) {
        this.entrySort = new OrderSpec({ by:"Key", desc:false });

        this.isLoading = false;
        this.entries = null;
        this.errorMessage = "";

        StageConstant.get()
            .then(
                (xs: StageConstant[]) => {
                    this.entries = xs;
                },
                (m: string) => { this.dialog.alert(m, "Error"); })
            .finally(() => this.isLoading = false);

    }

    delete(f: StageConstant) {
        const m = `Are you sure you want to delete constant ${f.Key}?`;
        this.dialog.confirm(m, "Confirm")
            .then((isConfirmed) => {
                if (!isConfirmed) return false;
                f.delete(f.Key).then(
                    (e: boolean) => {
                        if (e) { this.reload(); }
                        else { this.dialog.alert("Can not delete."); }
                    });
                return true;
            });
    }

    stageEditor: StageConstantEditorCtrl;

    edit(f: StageConstant) {
        this.stageEditor.show(f);
        return false;
    }

    add(){
        this.stageEditor.show();
        return false;
    }

    reload() {
        StageConstant.get()
            .then(
            (xs: StageConstant[]) => {
                this.entries = xs;
            },
            (m: string) => { this.dialog.alert(m, "Error"); })
            .finally(() => this.isLoading = false);
    }

    closeError() {
        this.errorMessage = "";
    }
}

import "./style.css";

angular.module("app", ["ngMessages"])

    .factory("serviceGen", serviceGen)
    .factory(StageConstant.ngName, StageConstant.ngFactory)

    .service("dialog", Dialog)
    .directive("sortHandle", sortHandle)
    .directive(stageConstantEditor.ngName!, stageConstantEditor)

    .component("scManager", { controller: SCManagerController, controllerAs: "vm", template: appHtml })
;
