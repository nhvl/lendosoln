import { IPromise, IQService } from "angular";

import {IServiceGen, IService } from "../../../ng-common/serviceGen";

type int = number;
export class StageConstant {
    static ngName = "StageConstant";

    Integer    : int;
    Key        : string
    Desc       : string;
    Owner      : string;
    CreatedDate: Date|null;

    constructor(d?: StageConstant) {
        this.Integer     = 0;
        this.Key         = "";
        this.Desc        = "";
        this.Owner       = "";
        this.CreatedDate = null;

        if (d != null) Object.assign(this, d);
        return this;
    }

    static get(): IPromise<StageConstant[]> {
        const service = StageConstant.service;
        return service<StageConstant[]>("GetConstants")
            .then((bs: (StageConstant[])) => bs.map(b => new StageConstant(b)));
    }

    delete(f: string): IPromise<boolean> {
        const service = StageConstant.service;
        const $q = StageConstant.$q;
        return service<boolean>("DeleteConstant", { constantKey: f });
    }

    edit(): IPromise<string> {
        const service = StageConstant.service;
        const $q = StageConstant.$q;
        return service<string>("EditConstant", { constantKey: this.Key, newString: this.Desc, newInt : this.Integer, newOwner : this.Owner })
            ;
    }

    add(): IPromise<string> {
        this.Key = this.Key.replace(/\s/g, "");
        const service = StageConstant.service;
        const $q = StageConstant.$q;
        return service<string>("AddConstant", { constantKey: this.Key, newString: this.Desc, newInt: this.Integer, newOwner: this.Owner })
            ;
    }

    static $q :IQService;
    static service: IService;

    static ngFactory(serviceGen: IServiceGen, $q: IQService): typeof StageConstant {
        StageConstant.service = serviceGen();
        StageConstant.$q = $q;
        return StageConstant;
    }
}
StageConstant.ngFactory.$inject = ["serviceGen", "$q"];
