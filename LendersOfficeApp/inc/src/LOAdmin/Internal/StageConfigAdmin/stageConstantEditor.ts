import { IAngularEvent, IParseService, IDirective, IScope, IDirectiveLinkFn } from "angular";
import {StageConstant  } from "./StageConstant";
import {formValidate     } from "../../../ng-common/formValidate";
import {IDialog          } from "../../../ng-common/dialog";
import {INg2VarAttrs     } from "../../../ng-directives/INg2VarAttrs";

// Invertor Ratesheet File Editor
export class StageConstantEditorCtrl {
    private entry: StageConstant;
    private $modal: JQuery;
    private onSaved: () => void;

    private deploymentTypes: string[];

    private isPending: boolean;
    private editMode: boolean;

    static $inject = ["$element", "dialog", StageConstant.ngName];
    constructor($element: JQLite, private dialog: IDialog) {
        this.$modal = $element.find(".modal");

        this.isPending = false;
        console.assert(this.$modal.length === 1);
    }

    show(entry?: StageConstant) {
        this.resetError();

        this.entry = new StageConstant(entry);
        this.isPending = false;
        this.editMode = (!!this.entry.Key && this.entry.Key.length > 0);
        this.$modal.modal("show");
    }

    private save($event: IAngularEvent) {
        if (this.isPending) return;

        this.resetError();

        this.isPending = true;

        if (this.editMode) {
            this.entry.edit().then(
                () => {
                    this.$modal.modal("hide");
                    this.onSaved();
                },
                (error: string) => {
                    if (error) {
                        const message = typeof error === "string" ? error : JSON.stringify(error, null, 2);
                        this.dialog.alert(message, "Edit failed.");
                    }
                })
                .finally(() => { this.isPending = false; });
        } else {
            this.entry.add().then(
                () => {
                    this.$modal.modal("hide");
                    this.onSaved();
                },
                (error: string) => {
                    if (error) {
                        const message = typeof error === "string" ? error : JSON.stringify(error, null, 2);
                        this.dialog.alert(message, "Add failed.");
                    }
                })
                .finally(() => { this.isPending = false; });
        }

    }

    static ngName = "StageConstantEditorCtrl";
    private resetError() {
        // Nothing to implement
    }
}

import * as stageEditorHtml from "./stageConstantEditor.html";

stageConstantEditor.ngName = "stageConstantEditor";
stageConstantEditor.$inject = ["$parse"];
export function stageConstantEditor($parse: IParseService): IDirective {
    return {
        restrict        : "E",
        scope           : { onSaved: "&", },
        link            : ((scope  : IScope, $e: JQLite, attrs: INg2VarAttrs, ctrl: StageConstantEditorCtrl) => {
                                $parse(attrs.ng2Var).assign(scope.$parent, ctrl);
                            }) as IDirectiveLinkFn,
        template        : stageEditorHtml,
        controller      : StageConstantEditorCtrl,
        controllerAs    : "vm",
        bindToController: true,
    };
}
