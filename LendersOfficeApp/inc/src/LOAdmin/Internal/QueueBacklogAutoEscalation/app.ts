import Ractive from "ractive";
import {ContextEvent} from "../../../Ractive/misc";

import "../../../utils/Array.orderBy";
import {serviceGen          } from "../../../utils/serviceGen";
import {formValidate        } from "../../../utils/formValidate";
import {documentReadyPromise} from "../../../utils/documentReadyPromise";
import {getDialog, IDialog  } from "../../../Ractive/DialogRactive";

import * as appHtml from "./app.html";

const $       = jQuery;

// services
const service = serviceGen();

// Model
// -----
interface IQueueMonitorData {
	ID              						: string;
	Name 									: string;
	Description 							: string;
	Threshold1QueueLength              		: string;
	Threshold1QueueLengthDurationMinutes    : string;
	Threshold1OldestItemMinutes             : string;
	Threshold2QueueLength              		: string;
	Threshold2QueueLengthDurationMinutes  	: string;
	Threshold2OldestItemMinutes             : string;
}

class QueueMonitorData implements IQueueMonitorData {
	ID              						: string;
	Name 									: string;
	Description 							: string;
	Threshold1QueueLength              		: string;
	Threshold1QueueLengthDurationMinutes    : string;
	Threshold1OldestItemMinutes             : string;
	Threshold2QueueLength              		: string;
	Threshold2QueueLengthDurationMinutes  	: string;
	Threshold2OldestItemMinutes             : string;

	constructor(other?: IQueueMonitorData) {
		this.ID                                   = "0";
		this.Name                                 = "";
		this.Description                          = "";
		this.Threshold1QueueLength                = "";
		this.Threshold1QueueLengthDurationMinutes = "";
		this.Threshold1OldestItemMinutes          = "15"; // We default to 15 minutes when keeping track of the oldest item in the queue.
		this.Threshold2QueueLength                = "";
		this.Threshold2QueueLengthDurationMinutes = "";
		this.Threshold2OldestItemMinutes          = "15";

		if (other != null) Object.assign(this, other);
	}

	static get(): Promise<QueueMonitorData[]> {
		return service<QueueMonitorData[]>("RetrieveAll")
			.then((result) => result.map(data => new QueueMonitorData(data)).orderBy("Name"));
	}

	loadDetail() {
		return service<QueueMonitorData[]>("GetQueueMonitoringData", { id: this.ID })
				.then((result) => $.extend(this, result));
	}

	add(): Promise<boolean|IFormError> {
		if(!this.validate()) Promise.reject("Please complete all required fields before adding new monitoring.");

		return service<boolean | IFormError>("AddNewQueueMonitoringData", { data: this })
				.then((res) => res === true ? res : Promise.reject(res) as any);
	}

	edit(): Promise<boolean|IFormError> {
		if(!this.validate()) Promise.reject("Please complete all required fields before editing monitoring.");

		return service<boolean | IFormError>("ModifyExistingQueueMonitoringData", { data: this })
				.then((res) => res === true ? res : Promise.reject(res) as any);
	}

	remove(): Promise<string> {
		return service("RemoveQueueFromMonitoring", { id: this.ID })
				.then((res) => res === true ? res : Promise.reject(res) as any);
	}

	validate() : boolean {
		return 	this.isValid(this.ID) &&
				this.isValid(this.Name) &&
				this.isValid(this.Description) &&
				this.isValid(this.Threshold1QueueLength) &&
				this.isValid(this.Threshold1QueueLengthDurationMinutes) &&
				this.isValid(this.Threshold1OldestItemMinutes) &&
				this.isValid(this.Threshold2QueueLength) &&
				this.isValid(this.Threshold2QueueLengthDurationMinutes) &&
				this.isValid(this.Threshold2OldestItemMinutes);
	}

	isValid(field : string) : boolean {
		return field !== null && field !== "";
	}
}
// -----


// RactiveJS Components
// -----
interface ISortParam {
		by 	: string,
		asc	: boolean,
}
interface IFormError {
	ID              						: string;
	Name 									: string;
	Description 							: string;
	Threshold1QueueLength              		: string;
	Threshold1QueueLengthDurationMinutes    : string;
	Threshold1OldestItemMinutes             : string;
	Threshold2QueueLength              		: string;
	Threshold2QueueLengthDurationMinutes  	: string;
	Threshold2OldestItemMinutes             : string;
}
interface IMainData {
	queues		  ?: QueueMonitorData[],
	sort          ?: ISortParam,
	currentQueue  ?: QueueMonitorData,
	formError     ?: IFormError,
}
interface IMainRactive extends Ractive.Ractive {
	dialog: IDialog;
}

const Main = Ractive.extend({
	template: appHtml,
	data(): IMainData{ return {
		queues         	: undefined,
		currentQueue   	: undefined,
		sort         	: {
			by : "",
			asc: true,
		},
		formError 		: {
			ID 										: "",
			Name 									: "",
			Description 							: "",
			Threshold1QueueLength              		: "",
			Threshold1QueueLengthDurationMinutes    : "",
			Threshold1OldestItemMinutes             : "",
			Threshold2QueueLength              		: "",
			Threshold2QueueLengthDurationMinutes  	: "",
			Threshold2OldestItemMinutes             : "",
		},
	}},
	onrender(this: IMainRactive) {
		const $editModal = $("#editModal");
		this.on({
			addNew(): boolean {
				this.set({
					currentQueue : new QueueMonitorData(),
				} as IMainData);

				$("#queue-title").text("Add New Queue");

				$editModal.modal("show");

				return false;
			},
			edit(this: IMainRactive, context: ContextEvent, queue: QueueMonitorData): boolean {
				this.set({
					currentQueue : new QueueMonitorData(queue)
				} as IMainData);

				$("#queue-title").text("Edit Queue");

				$editModal.modal("show");

				return false;
			},
			save(this: IMainRactive, context: ContextEvent) {
				if (!(formValidate(context.node as HTMLFormElement))) {
					return false;
				}

				const queue: QueueMonitorData = this.get("currentQueue");

				if (!validate(this, queue)) {
					setTimeout(() => { $("form").find(".has-error input").first().focus(); }, 200);

					return false;
				}

				let isAdd = ($("#queue-title").text() === "Add New Queue");

				if(isAdd) {
					queue.add()
						.then((v) => {
							$editModal.modal("hide");
							this.fire("reload");
						}, (formError: (boolean|IFormError)) => {
								if (formError === false) {
									window.alert("Save failed! Check the PB logs for more information.");
								} else {
									this.set("formError", $.extend(this.get("formError"), formError));
								}
							}
						);
				}
				else {
					queue.edit()
						.then((v) => {
							$editModal.modal("hide");
							this.fire("reload");
						}, (formError: (boolean|IFormError)) => {
								if (formError === false) {
									window.alert("Save failed! Check the PB logs for more information.");
								} else {
									this.set("formError", $.extend(this.get("formError"), formError));
								}
							}
						);
				}

				return false;

				function validate(ractive: Ractive.Ractive, queue: QueueMonitorData) {
					let ret = true;

					ractive.set("formError.*", null);

					if (queue.Name === null || queue.Name === "") {
						ractive.set("formError.Name", "This field is required.");
						ret = false;
					}
					if (queue.Description === null || queue.Description === "") {
						ractive.set("formError.Description", "This field is required.");
						ret = false;
					}
					if (isInvalidNumberString(queue.Threshold1QueueLength)) {
						ractive.set("formError.Threshold1QueueLength", "This field must be a number greater than or equal to zero.");
						ret = false;
					}
					if (isInvalidNumberString(queue.Threshold1QueueLengthDurationMinutes)) {
						ractive.set("formError.Threshold1QueueLengthDurationMinutes", "This field must be a number greater than or equal to zero.");
						ret = false;
					}
					if (isInvalidNumberString(queue.Threshold1OldestItemMinutes)) {
						ractive.set("formError.Threshold1OldestItemMinutes", "This field must be a number greater than or equal to zero.");
						ret = false;
					}
					if (isInvalidNumberString(queue.Threshold2QueueLength)) {
						ractive.set("formError.Threshold2QueueLength", "This field must be a number greater than or equal to zero.");
						ret = false;
					}
					if (isInvalidNumberString(queue.Threshold2QueueLengthDurationMinutes)) {
						ractive.set("formError.Threshold2QueueLengthDurationMinutes", "This field must be a number greater than or equal to zero.");
						ret = false;
					}
					if (isInvalidNumberString(queue.Threshold2OldestItemMinutes)) {
						ractive.set("formError.Threshold2OldestItemMinutes", "This field must be a number greater than or equal to zero.");
						ret = false;
					}

					return ret;
				}

				function isInvalidNumberString(input: string) {
					return input === null || input === "" || isNaN(parseInt(input, 10)) || (parseInt(input, 10) < 0);
				}
			},
			remove(this: IMainRactive, context: ContextEvent, queue: QueueMonitorData): boolean {
				this.dialog.confirm("Remove monitoring for queue '" + queue.Name + "'?")
					.then((isConfirm:boolean) => {
						if (!isConfirm) return;

						queue.remove().then(() => { this.fire("reload"); });
					});


				return false;
			},
			sort(this: IMainRactive, context: ContextEvent, sortByProp: string) {
				const sort = this.get("sort");

				if (sort.by === sortByProp) sort.asc = !sort.asc;
				else sort.by = sortByProp;

				const queues = (this.get("queues") as QueueMonitorData[]).orderBy(sort.by, !(sort.asc));

				this.set({ sort, queues } as IMainData);
			},
			reload(this: IMainRactive) {
				QueueMonitorData.get().then((queues: QueueMonitorData[]) => this.set({ queues } as IMainData));
			}
		});
	},
});
// -----

// Page initialization
// -----
init();
async function init() {
	const queues = await QueueMonitorData.get();
	await documentReadyPromise;

	const dialog = getDialog();
    new Main({
        el: "container",
		data: { queues } as IMainData,
        dialog: dialog,
    } as Ractive.NewOptions);
}
