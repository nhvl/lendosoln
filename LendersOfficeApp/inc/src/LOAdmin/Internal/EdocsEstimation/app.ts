﻿import * as angular from "angular";
import "angular-messages";

import {serviceGen, IService} from "../../../ng-common/serviceGen";
import {formValidate} from "../../../ng-common/formValidate";
import {sortHandle, OrderSpec} from "../../../ng-directives/sortIcon";

const $ = jQuery;

class EdocsConfig {
    brokerNm: string;
    DocumentId: string;
    description: string;
    sLid: string;

    CreatedDate: string;
    TimeImageCreationEnqueued: string;
    TimeImageCreationStarted: string;
    TimeImageCreationCompleted: string;
    CreatedDateSort: number;
    TimeImageCreationEnqueuedSort: number;//use number instead of Date to sort
    TimeImageCreationStartedSort: number;
    TimeImageCreationCompletedSort: number;

    IsUploadedByPmlUser: boolean;
    NumPages: number;
    DisplayedEstimatedWaitTimeInSeconds: number;
    ActualProcessingTimeFromEnqueuingInSeconds: number;

    constructor(d?: EdocsConfig) {
        if (d != null) $.extend(this, d);
        return this;
    }
    static getList(): ng.IPromise<EdocsConfig[]> {
        const service = EdocsConfig.service;
        return service<EdocsConfig[]>("Get")
            .then(
            vs => vs.map(v => new EdocsConfig(v)));
    }

    static $q :ng.IQService;
    static service<T>(method: string, args?: {}): ng.IPromise<T> {
        console.error("Have not assgin [service] function");
        return null;
    }
}

EdocsConfigFactory.$inject = ["serviceGen", "$q"];
function EdocsConfigFactory(serviceGen:(url?: string) => IService, $q: ng.IQService): typeof EdocsConfig {
    EdocsConfig.service = <any> serviceGen();
    EdocsConfig.$q = $q;
    return EdocsConfig;
}

interface EdocsManagerScope extends ng.IScope{
}

class EdocsManager{
    Edocss: EdocsConfig[];
    isLoading: boolean;
    entrySort: OrderSpec;

    static $inject = ["$scope", "EdocsConfig"];
    constructor(private $scope: EdocsManagerScope) {
        this.isLoading = false;
        this.Edocss = null;
        //this.entrySort = new OrderSpec({ by: "brokerNm", desc: false });
        this.entrySort = new OrderSpec({ by: "", desc: false });

        this.reload();
    }
    reload() {
        this.isLoading = true;
        EdocsConfig.getList()
            .then((xs) => {
                this.Edocss = xs;
                this.isLoading = false;
            });
    }
    sort(by: string) {
        if (this.entrySort.by === by) this.entrySort.desc = !this.entrySort.desc;
        else { this.entrySort.by = by; this.entrySort.desc = false; }
    }
}

angular.module("app", ["ngMessages"])
    .factory("serviceGen", serviceGen)
    .factory("EdocsConfig", EdocsConfigFactory)
    .directive("sortHandle", sortHandle)
    .controller("EdocsManager", EdocsManager)
;
