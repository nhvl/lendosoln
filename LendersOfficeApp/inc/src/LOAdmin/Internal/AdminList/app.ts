import Ractive from "ractive";
import { ContextEvent } from "../../../Ractive/misc";

import "../../../utils/Array.orderBy";
import {serviceGen          } from "../../../utils/serviceGen";
import {formValidate        } from "../../../utils/formValidate";
import {documentReadyPromise} from "../../../utils/documentReadyPromise";
import {getDialog, IDialog  } from "../../../Ractive/DialogRactive";

import * as appHtml from "./app.html";

const $ = jQuery;

// services
const service = serviceGen();

//#region Model
interface IUserData {
    LoginNm              : string;
    UserID               : string;
    AdminNm              : string;
    IsActive             : boolean;

    FirstName            : string;
    LastName             : string;
    Permissions          : string;

    IsSystemAdmin        : boolean;
    IsDevelopment        : boolean;
    ViewBrokers          : boolean;
    EditBroker           : boolean;
    CreateBroker         : boolean;
    ViewEmployees        : boolean;
    EditEmployee         : boolean;
    BecomeUser           : boolean;
    ViewActivity         : boolean;
    ViewInvoices         : boolean;
    GenerateInvoices     : boolean;
    ViewLenders          : boolean;
    IsSAE                : boolean;
    UpdateStageConstants : boolean;
    HasBillingAccess     : boolean;
    EditIntegrationSetup : boolean;
	FeatureAdoption  : boolean;
}
class User implements IUserData {
        LoginNm              : string;
        UserID               : string;
        AdminNm              : string;
        IsActive             : boolean;

        FirstName            : string;
        LastName             : string;
        Permissions          : string;

        IsSystemAdmin        : boolean;
        IsDevelopment        : boolean;
        ViewBrokers          : boolean;
        EditBroker           : boolean;
        CreateBroker         : boolean;
        ViewEmployees        : boolean;
        EditEmployee         : boolean;
        BecomeUser           : boolean;
        ViewActivity         : boolean;
        ViewInvoices         : boolean;
        GenerateInvoices     : boolean;
        ViewLenders          : boolean;
        IsSAE                : boolean;
        UpdateStageConstants : boolean;
        HasBillingAccess     : boolean;
        EditIntegrationSetup : boolean;
		FeatureAdoption  : boolean;

        isSelected           : boolean;

        constructor(d?: IUserData) {
            if (d == null)
                d = {
                    LoginNm              : "",
                    UserID               : "",
                    AdminNm              : "",
                    IsActive             : true,

                    FirstName            : "",
                    LastName             : "",
                    Permissions          : "",

                    IsSystemAdmin        : false,
                    IsDevelopment        : false,
                    ViewBrokers          : false,
                    EditBroker           : false,
                    CreateBroker         : false,
                    ViewEmployees        : false,
                    EditEmployee         : false,
                    BecomeUser           : false,
                    ViewActivity         : false,
                    ViewInvoices         : false,
                    GenerateInvoices     : false,
                    ViewLenders          : false,
                    IsSAE                : false,
                    UpdateStageConstants : false,
                    HasBillingAccess     : false,
                    EditIntegrationSetup : false,
					FeatureAdoption  : false,
                };

            $.extend(this, d);

            this.isSelected = false;
        }

        isValid() {
            if (!this.LoginNm) return false;
            return true;
        }

        save(password: string, changePasswordNextLogin:boolean): Promise<boolean|IFormError> {
            return service<boolean|IFormError>("EditAdmin", { data: this, password, changePasswordNextLogin })
                .then((res) =>  (res === true) ? res : Promise.reject(res) as any);
        }

        loadDetail() {
            return service<IUserData[]>("GetAdminDetail", { userId: this.UserID }).
                then((result) => $.extend(this, result));
        }

        static get(): Promise<User[]> {
            return service<IUserData[]>("GetAdmins").
                then((result) => result.map(d => new User(d)).orderBy("AdminNm")
                );
        }

        static forcePasswordChange(xs: User[]): Promise<{ invalidUserIds: string[], failedUserIds: string[] }> {
            if (xs == null || xs.length < 1) return Promise.reject<{ invalidUserIds: string[], failedUserIds: string[] }>("Argument Null");
            return service("ForcePasswordChange", { userIds: xs.map(x => x.UserID) });
        }
    }
//#endregion

//#region RactiveJS Components
interface ISortParam {
    by : string,
    asc: boolean,
}
interface IFormError {
    LoginNm   : string,
    password  : string,
    passwordRe: string,
    FirstName : string,
    LastName  : string,
}
interface IMainData {
    users                   ?: User[],

    sort                    ?: ISortParam,
    selectedState           ?: number,
    isLoading               ?: boolean,

    leftPermissionFields    ?: { key: string, text: string }[],
    centerPermissionFields  ?: { key: string, text: string }[],
    rightPermissionFields   ?: { key: string, text: string }[],

    edittingUser            ?: User,
    password                ?: string,
    passwordRe              ?: string,
    changePassNext          ?:boolean,
    formError               ?: IFormError,
}

const Main = Ractive.extend({
    template: appHtml,
    data(): IMainData{
        return {
            users        : undefined,

            sort         : {
                by : "",
                asc: true,
            },
            selectedState: 0,
            isLoading    : false,
            leftPermissionFields: [
                { key: "IsSystemAdmin"       , text: "Is System Admin" },
                { key: "IsDevelopment"       , text: "Is Development"},
                { key: "ViewBrokers"         , text: "View Brokers"},
                { key: "EditBroker"          , text: "Edit Broker"},
                { key: "CreateBroker"        , text: "Create Broker"},
                { key: "ViewEmployees"       , text: "View Employees"},
				{ key: "ExportBroker", text: "Export Broker"},
				{ key: "ImportBroker", text: "Import Broker"},
            ],
            centerPermissionFields: [
                { key: "EditEmployee"        , text: "Edit Employee"},
                { key: "BecomeUser"          , text: "Become User"},
                { key: "ViewActivity"        , text: "View Activity"},
                { key: "ViewInvoices"        , text: "View Invoices"},
                { key: "GenerateInvoices"    , text: "Generate Invoices"},
                { key: "ViewLenders"         , text: "View Lenders"},
            ],
            rightPermissionFields: [
                { key: "IsSAE"               , text: "Is SAE"},
                { key: "UpdateStageConstants", text: "Update Stage Constants"},
                { key: "HasBillingAccess"    , text: "Has Billing 2.0 Access"},
                { key: "EditIntegrationSetup", text: "Allow editing integration setup"},
				{ key: "FeatureAdoption",  text: "Feature Adoption"},
				{ key: "UploadReplicationWhitelist", text: "Upload Replication Whitelist"},
				{ key: "DownloadReplicationWhitelist", text: "Download Replication Whitelist"},				
            ],

            edittingUser: undefined,
            password    : "",
            passwordRe  : "",
            changePassNext:false,
            formError : {
                LoginNm   : "",
                password  : "",
                passwordRe: "",
                FirstName : "",
                LastName  : "",
            },
        }
    },
    onrender(this: Ractive.Ractive) {
        const $editModal = $("#editModal");
        this.on({
            addNew(context: ContextEvent) {
                this.set({
                    edittingUser  : new User(),
                    password      : "",
                    passwordRe    : "",
                    changePassNext: false,
                } as IMainData);
                this.set("formError.*", null);

                $editModal.modal("show");

                return false;
            },
            edit(this: Ractive.Ractive, context: ContextEvent, user: User) {
                this.set({
                    isLoading : true,
                    password  : "",
                    passwordRe: "",
                    changePassNext: false,
                } as IMainData);
                this.set("formError.*", null);

                user.loadDetail().then(() => {
                    this.set({
                        edittingUser: new User(user),
                        isLoading   : false,
                    } as IMainData);
                });

                $editModal.modal("show");

                return false;
            },
            forceChange(this:Ractive.Ractive&{dialog:IDialog}, context: ContextEvent) {
                // OPM 24342
                // pops up confirmation for forced password changes
                this.dialog.confirm("Force passsword changes for the selected users?", "Confirm Force Password Change", "Force Change")
                .then((isConfirm: boolean) => {
                    if (!isConfirm) { return; }

                    console.debug("is confirm", isConfirm);

                    const xs = (<User[]> this.get("users")).filter(x => x.isSelected);
                    if (xs.length < 1) { return; }

                    User.forcePasswordChange(xs).then(() => {
                        this.dialog.alert("Forced passsword changes.", "Success")
                    });
                });

                return false;
            },
            save(this: Ractive.Ractive, context: ContextEvent) {
                if (!(formValidate(context.node as HTMLFormElement))) { return false; }

                const user          : User    = this.get("edittingUser");
                const password      : string  = this.get("password");
                const changePassNext: boolean = this.get("changePassNext");

                if (!validate(this, user, password, this.get("passwordRe"))) {
                    setTimeout(() => { $("form").find(".has-error input").first().focus(); }, 200);
                    return false;
                }

                user.save(password, changePassNext)
                    .then((v) => {
                        $editModal.modal("hide");
                        User.get().then((users: User[]) => this.set({ users } as IMainData));
                    }, (formError: (boolean|IFormError)) => {
                        if (formError === false) {
                            window.alert("Save FAILED!");
                        } else {
                            //this.merge("formError", formError);
                            this.set("formError", $.extend(this.get("formError"),formError));
                        }
                    });

                return false;

                function validate(ractive: Ractive.Ractive, user: User, password: string, passwordRe: string) {
                    // Login name already exists.
                    let r = true;

                    ractive.set("formError.*", null);

                    if (!user.LoginNm) {
                        ractive.set("formError.LoginNm", "This field is required.");
                        r = false;
                    }
                    if (!user.FirstName) {
                        ractive.set("formError.FirstName", "This field is required.");
                        r = false;
                    }
                    if (!user.LastName) {
                        ractive.set("formError.LastName", "This field is required.");
                        r = false;
                    }
                    if (!user.UserID) {
                        if (!password) {
                            ractive.set("formError.password", "This field is required.");
                            r = false;
                        }
                        if (!passwordRe) {
                            ractive.set("formError.passwordRe", "This field is required.");
                            r = false;
                        }
                    }

                    if (!!password) {
                        if (!(/\d/.test(password) && /[a-zA-Z]/.test(password))) {
                            ractive.set("formError.password", "At least one number and one character is required.");
                            r = false;
                        } else if (password.length < 6) {
                            ractive.set("formError.password", "Password must be at least 6 characters in length.");
                            r = false;
                        }

                        if (!passwordRe) {
                            ractive.set("formError.passwordRe", "This field is required.");
                            r = false;
                        } else {
                            if (password != passwordRe) {
                                ractive.set("formError.passwordRe", "Password does not match.");
                                r = false;
                            }
                        }
                    }

                    return r;
                }
            },
            sort(context: ContextEvent, sortByProp: string) {
                const sort = this.get("sort");
                if (sort.by === sortByProp) sort.asc = !sort.asc;
                else sort.by = sortByProp;

                const users = (this.get("users") as User[]).orderBy(sort.by, !(sort.asc));
                this.set({ sort, users } as IMainData);
            },
        });

        //#region batch select feature
        const $batchSelect = this.find("#batchSelect") as HTMLInputElement;
        this.observe("users.*.isSelected", (newValue:boolean, oldValue:boolean, keypath:string) => {
            const xs: User[] = this.get("users");

            let s: number;
            if (!(xs.some(x => x.isSelected))) s = 0;
            else if (!(xs.every(x => x.isSelected))) s = 1;
            else s = 2;

            this.set({ selectedState: s } as IMainData);
            $batchSelect.indeterminate = (s == 1);
        });
        this.on({
            selectAllUsers(context: ContextEvent) {
                let s: number = this.get("selectedState");
                if (s == 2) s = 0;
                else s = 2;
                this.set({ selectedState: s } as IMainData);

                const xs: User[] = this.get("users");
                this.set("users.*.isSelected", (s == 2));
                //xs.forEach(x => x.isSelected = (s == 2));

                $batchSelect.indeterminate = false;
            }
        });
        //#endregion
    }
});
//#endregion

init();
async function init() {
    const getUserPromise = User.get();

    await documentReadyPromise;

    let users: User[];
    try {
        users = await getUserPromise;
    } catch (errors) {
        window.alert("Service error");
        console.error(errors);
        return;
    }

    const dialog = getDialog();

    new Main({
        el      : "container",
        data    : { users } as IMainData,
        dialog  : dialog,
    } as Ractive.NewOptions);
}
