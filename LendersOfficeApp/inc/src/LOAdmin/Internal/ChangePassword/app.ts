import Ractive from "ractive";
import "../../../Ractive/misc";

import {serviceGen          } from "../../../utils/serviceGen";
import {getQueryStringValue } from "../../../utils/getQueryStringValue";
import {documentReadyPromise} from "../../../utils/documentReadyPromise";

import * as appHtml from "./app.html";

const $ = jQuery;

// services
const service = serviceGen();
function GetUserInfo(): Promise<User & {PasswordGuidelines:string}> {
    return service("GetUserInfo").then((u:{}) => new User(u) as any);
}
function TryChangePassword(currentPassword:string, newPassword:string){
    return service<boolean|{error?:string}>("TryChangePassword", {currentPassword, newPassword})
        .then(
            (d: (boolean|{error?:string})) => {
                if (d === true) return (<boolean> d);
                console.info("TryChangePassword error: ", <{}> d);
                return Promise.reject<boolean>((<{error?:string}>d).error || "Service error");
            },
            (e:string) => {
                console.error("TryChangePassword error:", e);
                return Promise.reject("Service error.");
            });
}
//------------

// Model
class User {
    DisplayName: string;
    LoginName  : string;

    constructor(d?: {}) {
        if (d == null) d = { DisplayName: "", LoginName: "", };

        Object.assign(this, d);
    }
}
//-----------

// RactiveJS Components
const Main = Ractive.extend({
    template: appHtml,
    data(){ return {
        user: null as (User|null),
        // OPM 24342
        //     check to see if this was sent from the RequestHelper for a forced password change
        //     or if it was through the Change Password link on the main page
        passwordChangeRequired: (getQueryStringValue("requirepwchange") == "true"),
        serverError: "",
        submitSuccess: false,
        isSubmitting: false,
        form: {
            currentPassword: {
                value: "",
                pristine: true,
                error: { required:() => true, has:() => true, } // this will be replace
            },
            newPassword: {
                value: "",
                pristine: true,
                error: { required: () => true, has: () => true, } // this will be replace
            },
            confirmPassword: {
                value: "",
                pristine: true,
                error: { required: () => true, has: () => true, } // this will be replace
            },
            hasRequiredNotFill(){
                try {
                    return (this.currentPassword.error.required() ||
                            this.newPassword.error.required() ||
                            this.confirmPassword.error.required());
                } catch(e){
                    return true;
                }
            },
            hasError(){
                try {
                    return (this.currentPassword.error.has() ||
                        this.newPassword.error.has() ||
                        this.confirmPassword.error.has());
                } catch(e){
                    return true;
                }
            },
        },
    }},
    onrender(this:Ractive.Ractive&any) {
        this.set({
            "form.currentPassword.error": {
                required: () => !this.get("form.currentPassword.value"),
                has() { return this.required() },
            },
            "form.newPassword.error": {
                required: () => !this.get("form.newPassword.value"),
                notAlphanumeric: () => {
                    const v = this.get("form.newPassword.value");
                    return !(/\d/.test(v) && /[a-zA-Z]/.test(v));
                },
                minLength: () => this.get("form.newPassword.value").trim().length < 6,
                has(){
                    return this.required() || this.notAlphanumeric() || this.minLength();
                },
            },
            "form.confirmPassword.error": {
                required: () =>!this.get("form.confirmPassword.value"),
                notMatched:() => this.get("form.newPassword.value") != this.get("form.confirmPassword.value"),
                has() {
                    return this.required() || this.notMatched();
                },
            },
        });
    },
    on: {
        submitChange(this: Ractive.Ractive) {
            if (this.get("isSubmitting")) return false;
            //|| this.get("form").hasRequiredNotFill()

            try {
                this.set({
                    "form.currentPassword.pristine": false,
                    "form.newPassword.pristine"    : false,
                    "form.confirmPassword.pristine": false,
                });

                {
                    const form = this.get("form");
                    const hasError = (form.currentPassword.error.has() ||
                        form.newPassword.error.has() ||
                        form.confirmPassword.error.has());
                    if (hasError) {
                        console.error("Cannot submitChange: form.hasError()");
                        setTimeout(() => {
                            const el = $(this.find("form .has-error input"));

                            if (
                                //!Modernizr.input.required &&
                                !el.val()
                            ) {
                                el.tooltip({ title: "This field is required." });
                                el.one("change", () => { el.tooltip("destroy"); });
                            }

                            $(el).focus();
                        }, 100);

                        return false;
                    }
                }

                this.set({
                    isSubmitting : true,
                    submitSuccess: false,
                    serverError  : "",
                });

                TryChangePassword(
                    this.get("form.currentPassword.value"),
                    this.get("form.newPassword.value")
                ).then(
                    () => {
                        this.set({
                            isSubmitting: false,
                            submitSuccess: true,
                        });
                    },
                    (serverError: string) => {
                        this.set({
                            isSubmitting: false,
                            serverError,
                        });
                    }
                    );
            } catch (e) { console.error(e) }

            return false;
        }
    }
});
// -----------------------

init();
async function init(){
    const p = GetUserInfo();
    await documentReadyPromise;
    const {PasswordGuidelines, ...user} = await p;
    new Main({
        el: "container",
        data: { user },
        lazy: true,
    });

    new Ractive({
        el: document.querySelector("#passwordRulesModal .modal-body"),
        template: PasswordGuidelines,
        sanitize: true,
    });
}
