﻿import * as angular from "angular";
import "angular-messages";

import {serviceGen, IService} from "../../ng-common/serviceGen";
import {formValidate} from "../../ng-common/formValidate";
import {sortHandle, OrderSpec} from "../../ng-directives/sortIcon";

const $ = jQuery;

class LSConfig {
    sFieldId: string;
    sDescription: string;
    LastEditedD: string;
    LastEditedDSort: number;
    bLockDeskPerm: boolean;
    bCloserPerm: boolean;
    bAccountantPerm: boolean;
    bInvestorInfoPerm: boolean;
    constructor(d?: LSConfig) {
        if (d != null) $.extend(this, d);
        else {
            this.sFieldId = this.sDescription = this.LastEditedD = "";
            this.LastEditedDSort = 0;
            this.bAccountantPerm = this.bCloserPerm = this.bInvestorInfoPerm = this.bLockDeskPerm = false;
        }
        return this;
    }
    addNew(): ng.IPromise<LSConfig> {
        const service = LSConfig.service;
        const $q = LSConfig.$q;

        return service<LSConfig>("AddNew", { lfs: this })
            .then((error) => (error == null) ? $q.when(null) : $q.reject(error));
    }
    delete(): ng.IPromise<LSConfig> {
        const service = LSConfig.service;
        const $q = LSConfig.$q;

        return service<LSConfig>("Delete", { fieldId: this.sFieldId })
            .then((error) => (error == null) ? $q.when(null) : $q.reject(error));
    }
    static parse(text:string): ng.IPromise<LSConfig[]> {
        const service = LSConfig.service;
        const $q = LSConfig.$q;

        return service<LSConfig>("ParseMultiple", { text: text })
            .then((error) => (error == null) ? $q.when(null) : $q.reject(error));
    }
    static getList(): ng.IPromise<LSConfig[]> {
        const service = LSConfig.service;
        return service<LSConfig[]>("Get")
            .then(
            vs => vs.map(v => new LSConfig(v)));
    }

    static $q :ng.IQService;
    static service<T>(method: string, args?: {}): ng.IPromise<T> {
        console.error("Have not assgin [service] function");
        return null;
    }
}

LSConfigFactory.$inject = ["serviceGen", "$q"];
function LSConfigFactory(serviceGen:(url?: string) => IService, $q: ng.IQService): typeof LSConfig {
    LSConfig.service = <any> serviceGen();
    LSConfig.$q = $q;
    return LSConfig;
}

interface LSManagerScope extends ng.IScope{

}

class LSManager{
    activeLS: LSConfig;
    LSs: LSConfig[];
    isLoading: boolean;
    entrySort: OrderSpec;
    parseMult: string;

    static $inject = ["$scope", "LSConfig"];
    constructor(private $scope: LSManagerScope) {
        this.isLoading = false;
        this.LSs = null;
        this.entrySort = new OrderSpec({ by: "LastEditedDSort", desc: true });

        this.reload();
    }
    OnAddNew() {
        this.activeLS = new LSConfig();
    }
    Add($event: ng.IAngularEvent) {

        if (!formValidate(<HTMLFormElement> $event.currentTarget)) return;

        this.activeLS.addNew()
            .then(
                () => {
                    $("#addModal").modal("hide");
                    $("div[role='alert']").addClass("hide");
                    this.reload();
                },
                (error: LSConfig) => {
                    $("#addModal").modal("hide");
                    this.showError(error.sFieldId);
                });
    }
    OnDelete(LS: LSConfig) {
        this.activeLS = new LSConfig(LS);
    }
    Delete() {
        this.activeLS.delete()
            .then(
                () => {
                    $("#deleteModal").modal("hide");
                    $("div[role='alert']").addClass("hide");
                    this.reload();
                },
                (error: LSConfig) => {
                    $("#deleteModal").modal("hide");
                    this.showError(error.sFieldId);
                });
    }
    Parse($event: ng.IAngularEvent) {
        if (!formValidate(<HTMLFormElement> $event.currentTarget)) return;
        LSConfig.parse(this.parseMult).then(
            () => {
                $("#parseModal").modal("hide");
                $("div[role='alert']").addClass("hide");
                this.parseMult = '';
                this.reload();
            },
            (error: LSConfig) => {
                $("#parseModal").modal("hide");
                this.showError(error.sFieldId);
            });
    }
    showError(message:string) {
        $("div[role='alert']").removeClass("hide");
        $("[id$='ErrMsg']").html(message);
    }

    reload() {
        this.isLoading = true;
        LSConfig.getList()
            .then((xs) => {
                this.LSs = xs;
                this.isLoading = false;
            });
    }
    sort(by: string) {
        if (this.entrySort.by === by) this.entrySort.desc = !this.entrySort.desc;
        else { this.entrySort.by = by; this.entrySort.desc = false; }
    }
}

angular.module("app", ["ngMessages"])
    .factory("serviceGen", serviceGen)
    .factory("LSConfig", LSConfigFactory)
    .directive("sortHandle", sortHandle)
    .controller("LSManager", LSManager)
;
