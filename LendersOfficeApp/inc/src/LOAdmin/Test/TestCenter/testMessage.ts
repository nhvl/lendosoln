import { IScope, IComponentOptions, IComponentController, IOnChangesObject } from "angular";
import * as testMessageHtml from "./testMessage.html";

export class TestMessageController implements IComponentController {
    static ngName = "TestMessageController";

    summary    : string;
    isMultiline: boolean;
    message    : string;

    $onChanges(onChangesObj: IOnChangesObject){
        if (onChangesObj.message != null)  {
            this.init(onChangesObj.message.currentValue);
        }
    }

    init(message: string = "") {
        const MAX = 70;

        const summary = message.split(/\r\n/, 1)[0];
        this.summary = ((summary.length > MAX) ? (summary.slice(0, MAX) + "\u2026") : summary);
        this.isMultiline = /\n/.test(message);
        this.message = message;
    }
}

export const testMessageComponent: IComponentOptions= {
    controller  : TestMessageController,
    controllerAs: "ctrl",
    template    : testMessageHtml,
    bindings    : { message: "<" },
};