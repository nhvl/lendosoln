import "../../../console";

import * as angular from "angular";
import * as JSZip   from "jszip";

import {serviceGen, IService } from "../../../ng-common/serviceGen";
import {bsPopover            } from "../../../ng-directives/bsPopover";
import {selectWhenClicked    } from "../../../ng-directives/selectWhenClicked";
import {saveAs               } from "file-saver";
import {saveTextAs           } from "../../../utils/FileSaver/saveTextAs";

import {TestMessageController, testMessageComponent} from "./testMessage";

type int = number;
enum ResultStatus {
    Ignore  = 0,
    Fail    = 1,
    Success = 2,
}

interface ITestMethodData {
    Name        : string;
    FriendlyName: string;
}
interface IFixtureData {
    Id         : number;
    Name       : string;
    Namespace  : string;
    FullName   : string;
    TestMethods: ITestMethodData[];
}
interface ITestMethodResult {
    MethodId    : int;
    ResultStatus: ResultStatus;
    Message     : string;
}
interface ITestClassResult {
    FixtureId        : int;
    ResultStatus     : ResultStatus;
    Message          : string;
    TestMethodResults: ITestMethodResult[];
}

class TestMethod implements ITestMethodData {
    Name        : string;
    FriendlyName: string;

    Message     : string;
    ResultStatus: ResultStatus|null;

    isRunning   :boolean;

    constructor(d:ITestMethodData){
        this.Name         = d.Name;
        this.FriendlyName = d.FriendlyName;

        this.Message      = " ";
        this.ResultStatus = null;

        this.isRunning = false;
    }

    run() {
        const fixtureId = 0;
        throw new Error("TODO: fixtureId methodId");
        return TestMethod.service("RunTestMethod", { fixtureId: null, methodId:null });
    }

    static service: <T>(method: string, args?: {}) => ng.IPromise<T>;
}

class Fixture implements IFixtureData {
    Id          : number;
    Name        : string;
    Namespace   : string;
    FullName    : string;
    TestMethods : TestMethod[];

    ResultStatus: ResultStatus;
    Message     : string;

    isRunning   : boolean;

    constructor(d: IFixtureData) {
        this.Id          = d.Id;
        this.Name        = d.Name;
        this.Namespace   = d.Namespace;
        this.FullName    = d.FullName;
        this.TestMethods = (d.TestMethods || <ITestMethodData[]> []).map( m => new TestMethod(m));

        this.isRunning   = false;
    }

    run(): ng.IPromise<ITestClassResult[]> {
        return Fixture.service<ITestClassResult[]>("RunTestFixture", { fixtureId: this.Id });
    }

    runTestMethod(methodId: number): ng.IPromise<ITestClassResult[]> {
        return Fixture.service<ITestClassResult[]>("RunTestMethod", { fixtureId: this.Id, methodId });
    }

    static get(): ng.IPromise<Fixture[]> {
        return Fixture.service<IFixtureData[]>("GetTestManager")
            .then((fs: IFixtureData[]) => fs.map(f => new Fixture(f)));
    }

    static service: <T>(method: string, args?: {}) => ng.IPromise<T>;

    static ngName = "Fixture";
    static ngFactory(serviceGen: (url?: string) => IService): typeof Fixture {
        Fixture.service = serviceGen();
        return Fixture;
    }
}
Fixture.ngFactory.$inject = ["serviceGen"];

class TestCenterCtrl {
    fixtures     : Fixture[];
    isRunning    : boolean;
    isHidePassed : boolean;
    summary      : {
        passFixtureIds: number[],
        failFixureIds : number[],
    };

    static $inject = ["$q", Fixture.ngName];
    constructor(private $q:ng.IQService) {
        this.fixtures = <Fixture[]> [];
        this.isRunning = false;
        this.isHidePassed = false;
        this.summary = {
            passFixtureIds: <number[]> [],
            failFixureIds : <number[]> [],
        };

        Fixture.get().then((fs: Fixture[]) => { this.fixtures = fs; });
    }

    resultStatusToCssClass(o: (Fixture|TestMethod)): string {
        if (o.isRunning) return "";
        const d = ["warning", "danger", "success"];

        const s = o.ResultStatus!;
        return (s < 0 || d.length <= s) ? "unknown" : d[s];
    }

    runAll() {
        if (this.isRunning) {
            console.error("runAll: Not start. Process still running.");
            return;
        }
        const $q = this.$q;

        this.isRunning = true;
        const fixtures = this.fixtures;
        const rullAll =
            fixtures.reduce(
                ((promise: ng.IPromise<void>, fixture: Fixture) =>
                    fixture.TestMethods.reduce(
                        ((promise: ng.IPromise<void>, method: TestMethod, methodId: number) =>
                            promise.then(() => this.sRunMethod(fixture.Id, methodId))),
                        promise)
                ), $q.when());

        rullAll.finally(() => this.isRunning = false);
    }
    rerun() {
        if (this.isRunning) {
            console.error("rerun: Not start. Process still running.");
            return;
        }

        const $q = this.$q;

        this.isRunning = true;
        const allFixtures = this.fixtures;
        const fixtures = this.summary.failFixureIds.map(id => allFixtures[id]);
        const rullAll =
            fixtures.reduce(
                ((promise: ng.IPromise<void>, fixture: Fixture) =>
                    fixture.TestMethods.reduce(
                        ((promise: ng.IPromise<void>, method: TestMethod, methodId: number) =>
                            promise.then(() => this.sRunMethod(fixture.Id, methodId))),
                        promise)
                    ), $q.when());

        rullAll.finally(() => this.isRunning = false);
    }
    runFixture(fixtureId: number) {
        if (this.isRunning) {
            console.error("runFixture: Not start. Process still running.");
            return;
        }
        const $q = this.$q;

        const fixture = this.fixtures[fixtureId];
        if (fixture == null) { console.error("fixture(%d) is null", fixtureId); return; }
        if (fixture.Id !== fixtureId) { console.error("incorrect query fixture by id"); return; }

        this.isRunning = true;
        const runFixture = fixture.TestMethods.reduce(
            ((p: ng.IPromise<void>, m: TestMethod, i: number) => p.then(() => this.sRunMethod(fixtureId, i))),
            $q.when());

        runFixture.then(() => this.isRunning = false);
    }
    runFixture2(fixtureId: number) {
        if (this.isRunning) {
            console.error("runFixture2: Not start. Process still running.");
            return;
        }

        this.isRunning = true;
        this.fixtures[fixtureId].run()
            .then((report: ITestClassResult[]) => this.updateFixturesTestResult(report))
            .finally(() => this.isRunning = false);
    }
    runMethod(fixtureId: number, methodId: number) {
        if (this.isRunning) {
            console.error("runMethod: Not start. Process still running.");
            return;
        }

        this.isRunning = true;
        this.sRunMethod(fixtureId, methodId)
            .then(() => this.isRunning = false);
    }

    export() {
        const fixtures = this.fixtures;
        const ids = this.summary.failFixureIds;

        var xs = ids.map((id) => {
            const f = fixtures[id];
            return {
                n: f.FullName,
                c: ((f.Message !== "fail") ? [f.Message] : [])
                    .concat(
                        f.TestMethods
                        .filter(m => m.ResultStatus === ResultStatus.Fail)
                        .filter(m => m.Message !== "fail")
                        .map(m => m.Message)
                    )
                    .join("\r\n\r\n\r\n")
            };
        });

        let isFileSaverSupported = false;
        try {
            isFileSaverSupported = !!new Blob;
        } catch (e) { }

        if (isFileSaverSupported) {
            var zip = new JSZip();
            xs.forEach(x => { zip.file(x.n + ".txt", x.c); });
            zip.generateAsync({ type: "blob" }).then(data => saveAs(data as Blob, `FailResult_${(new Date()).toJSON().slice(0, 16)}.zip`));
        } else {
            let fc = xs.map(x => `################ ${x.n} ################\r\n\r\n${x.c}`)
                .join("\r\n\r\n\r\n----------------------------------------------------\r\n\r\n\r\n");
            saveTextAs(fc, `FailResult_${new Date().toJSON().slice(0, 16)}.txt`);
        }
    }

    private sRunMethod(fixtureId: number, methodId: number): ng.IPromise<void> {
        const $q = this.$q;

        console.debug("runMethod(%d, %d)", fixtureId, methodId);

        const fixture = this.fixtures[fixtureId];
        if (fixture == null) { return $q.reject(`fixture(${fixtureId}) is null`); }
        if (fixture.Id !== fixtureId) { return $q.reject("Incorrect query fixture by id"); }
        if ((methodId < 0) || (fixture.TestMethods.length <= methodId)) {
            return $q.reject(`Index out of range: methodId = ${methodId}`);
        }

        const method = fixture.TestMethods[methodId];
        if (method == null) { return $q.reject(`fixture(${fixtureId}).method(${methodId}) is null`); }

        fixture.isRunning =
        method .isRunning = true;

        return (
            fixture.runTestMethod(methodId).
                then((r) => this.updateFixturesTestResult(r),
                    (errorThrown: string) => {
                        this.updateFixturesTestResult([{
                            FixtureId   : fixtureId,
                            ResultStatus: ResultStatus.Fail,
                            Message     : "",
                            TestMethodResults: [{
                                MethodId: methodId,
                                ResultStatus: ResultStatus.Fail,
                                Message: errorThrown,
                            }],
                        }]);
                    })
            );
    }
    private updateFixturesTestResult(report: ITestClassResult[]):void {
        const fixtures = this.fixtures;
        report.forEach(r => {
            var { FixtureId: id, TestMethodResults: testMethodResults, ResultStatus: fixtureResultStatus, Message: fixtureMessage } = r;
            const fixture = fixtures[id];
            if (fixture.Id !== id) {
                console.error("incorrect query fixture by id");
                return;
            }

            fixture.isRunning = false;

            testMethodResults.forEach((methodReport: ITestMethodResult) => {
                const i = methodReport.MethodId;
                if ((i < 0) || (fixture.TestMethods.length <= i)) {
                    console.error("index out of range: methodReport.MethodId", methodReport);
                    return;
                }

                const testMethod = fixture.TestMethods[i];
                testMethod.isRunning    = false;
                testMethod.ResultStatus = methodReport.ResultStatus;
                testMethod.Message      = methodReport.Message;
            });

            const fixtureFailed = (
                fixtureResultStatus  === ResultStatus.Fail ||
                    fixture.ResultStatus !== ResultStatus.Fail ||
                    fixture.TestMethods.every(m => (m.ResultStatus !== ResultStatus.Fail))
            );
            if (fixtureFailed) {
                fixture.ResultStatus = fixtureResultStatus;
                if (!!fixtureMessage) fixture.Message = fixtureMessage;
            }
        });

        this.summary.passFixtureIds = fixtures
            .filter(fixture => (
                (fixture.ResultStatus === ResultStatus.Success) &&
                    fixture.TestMethods.every(m => ((m.ResultStatus != null) && (m.ResultStatus != ResultStatus.Fail)))
                ))
            .map(fixture => fixture.Id);
        this.summary.failFixureIds = fixtures
            .filter(fixture => fixture.ResultStatus === ResultStatus.Fail)
            .map(fixture => fixture.Id);
    }

    static ngName = "TestCenterCtrl";
}

import * as appHtml from "./app.html";


angular.module("app", [])
    .factory   ("serviceGen"                , serviceGen           )
    .factory   (Fixture.ngName              , Fixture.ngFactory    )

    .directive("selectWhenClicked"          , selectWhenClicked    )
    .directive("bsPopover"                  , bsPopover            )

    .component ("testMessage"               , testMessageComponent )
    .component("testCenter", { controller: TestCenterCtrl, controllerAs: "vm", template: appHtml}  )
;
