import "../../Ractive/misc";

import {serviceGen          } from "../../utils/serviceGen";
import {documentReadyPromise} from "../../utils/documentReadyPromise";

const service = serviceGen();

//#region Models
interface ILookUpResult   {
    corporate ?: string;
    fullPath  ?: string;
    message   ?: string;
}
class LookUpResult implements ILookUpResult {
    corporate ?: string;
    fullPath  ?: string;
    message   ?: string;

    constructor(d?: ILookUpResult) {
        if (d != null) {
            this.corporate = d.corporate;
            this.fullPath  = d.fullPath;
            this.message   = d.message;
        }
    }

    static lookUp(productId:string): Promise<LookUpResult> {
        return (
            (service<ILookUpResult>("LookUp", { productId })
                .then((d: ILookUpResult) => new LookUpResult(d),
                      () => new LookUpResult({ message: "Service error." }))));
    }
}
//#endregion

//#region RactiveJS Components
const Main = Ractive.extend({
    data() { return {
        serverError  :"",
        submitSuccess: false,
        isSubmitting : false,
        productId    : "",
        corporate    : "",
        fullPath     : "",
        message      : "",
    }; },
    onrender(this: Ractive.Ractive) {
        this.on({
            submitChange(event:Ractive.Event) {
                try {
                    const form = $(event.original.target as any);
                    const inputs = form.find(":input:visible");
                    inputs.tooltip("destroy");

                    const productId:string = this.get("productId");
                    if (!productId) {
                        $(inputs.filter((i:any,x:any) => !($(x).val()))[0]).tooltip().focus();
                        return false;
                    }

                    this.set({isSubmitting: true});
                    LookUpResult.lookUp(productId).then((r) => {
                        this.set({isSubmitting: false});
                        this.set(r);
                    });
                } catch(e) { console.error("LookUp.submit", e); }

                return false;
            },
        });
    },
});
//#endregion

function init(){
    const $container = $("#container");
    const template = $container.html();
    $container.removeClass("hidden");

    new Main({
        template: template,
        el: "#container",
    });
}

documentReadyPromise.then(init);

/*
watchify "LOAdmin.Author.LoanProgramLookUp.js" --outfile "../LOAdmin.Author.LoanProgramLookUp.js" --debug - v


*/
