﻿import { IDirective, IParseService, IScope, IAugmentedJQuery, IDirectiveLinkFn } from "angular";
import {LcuRequest      } from "./LcuRequest";
import {INg2VarAttrs     } from "../../ng-directives/INg2VarAttrs";

export class ErrorInfoController {
    entry: LcuRequest|null;
    $modal: JQuery;

    static $inject = ("$element").split(" ");
    constructor($element: IAugmentedJQuery) {
        this.entry = null;

        this.$modal = $element.find(".modal");
        console.assert(this.$modal.length === 1);
    }

    show(e: LcuRequest) {
        this.entry = e;
        this.$modal.modal("show");
    }

    static ngName = "ErrorInfoController";
}

import * as errorInfoHtml from "./errorInfo.html";

errorInfo.$inject = ("$parse").split(" ");
export function errorInfo($parse: IParseService): IDirective {
    return {
        restrict    : "E",
        scope       : {},
        link        : ((scope: IScope, $e: JQLite, attrs: INg2VarAttrs, ctrl: ErrorInfoController) => {
                            $parse(attrs.ng2Var).assign(scope.$parent, ctrl);
                        }) as IDirectiveLinkFn,
        template    : errorInfoHtml,
        controller  : ErrorInfoController,
        controllerAs: "vm",
    };
}

