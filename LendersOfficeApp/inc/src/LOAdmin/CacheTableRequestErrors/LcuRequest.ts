import {IPromise, IQService} from "angular"
import {IService   } from "../../ng-common/serviceGen";

export interface IRunResponse {
    successes  : number;
    failures   : number;
    redirectUrl: string;
}

export class LcuRequest {
    RequestId             : string; // Guid
    sLId                  : string; // Guid
    UserId                : string;
    UserLoginNm           : string;
    PageName              : string;
    AffectedCachedFields  : string;
    ErrorInfo             : string;
    FirstOccurD           : Date|null;
    LastRetryD            : string; // Date
    WhoRetriedLastLoginNm : string;
    sLNm                  : string;
    CustomerCode          : string;

    isSelected            : boolean;

    constructor(d?: LcuRequest) {
        this.RequestId             = "";
        this.sLId                  = "";
        this.UserId                = "";
        this.UserLoginNm           = "";
        this.PageName              = "";
        this.AffectedCachedFields  = "";
        this.ErrorInfo             = "";
        this.FirstOccurD           = null;
        this.LastRetryD            = "";
        this.WhoRetriedLastLoginNm = "";
        this.sLNm                  = "";
        this.CustomerCode          = "";

        if (d != null) Object.assign(this, d);

        this.isSelected = false;
    }

    static get() {
        const service = LcuRequest.service;
        return service<LcuRequest[]>("Get")
            .then((es: LcuRequest[]) => es.map(e => new LcuRequest(e)));
    }
    static delete(rs: LcuRequest[]): IPromise<true> {
        const service = LcuRequest.service;
        const $q      = LcuRequest.$q;

        return service<string>("Delete", {rs})
            .then((error:string) => (!error) ? true : $q.reject(error) as any);
    }
    static run(rs: LcuRequest[], numErrorsBefore: number): IPromise<IRunResponse> {
        const service = LcuRequest.service;
        const $q      = LcuRequest.$q;

        return service<IRunResponse>("Run", { rs, numErrorsBefore });
    }
    static runAll(): IPromise<IRunResponse> {
        const service = LcuRequest.service;
        const $q = LcuRequest.$q;

        return service<IRunResponse>("RunAll");
    }

    static $q     : IQService;
    static service: IService;

    static ngName = "LcuRequest";
    static ngFactory(serviceGen: (url?: string) => IService, $q: IQService): typeof LcuRequest {
        LcuRequest.service = <any> serviceGen();
        console.assert(/LOAdmin\/CacheTableRequestErrors.aspx/.test(window.location.pathname), "serivce may be mis-config");
        LcuRequest.$q = $q;
        return LcuRequest;
    }
}
LcuRequest.ngFactory.$inject = ("serviceGen $q").split(" ");
