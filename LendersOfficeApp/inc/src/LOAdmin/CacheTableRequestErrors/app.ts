import "core-js/modules/es6.object.assign";

import * as angular from "angular";
import { IDirective } from "angular";

import {serviceGen,                   } from "../../ng-common/serviceGen";
import {Dialog, IDialog               } from "../../ng-common/dialog";
import {selectWhenClicked             } from "../../ng-directives/selectWhenClicked";
import {checkboxIndeterminate         } from "../../ng-directives/checkboxIndeterminate";
import {sortHandle, OrderSpec         } from "../../ng-directives/sortIcon";
import {LcuRequest, IRunResponse      } from "./LcuRequest";
import {ErrorInfoController, errorInfo} from "./errorInfo";

class SearchSpec {
    // TODO:
    UserLoginNm : string;
    PageName    : string;
    CustomerCode: string;

    constructor() {
        this.UserLoginNm  = "";
        this.PageName     = "";
        this.CustomerCode = "";
    }
}

import * as appHtml from "./app.html";
class RequestListController {
    entries      : LcuRequest[]|null;
    entrySort    : OrderSpec;
    searchSpec   : SearchSpec;
    selectedPercent: number;

    static $inject = ("dialog LcuRequest").split(" ");
    constructor(private dialog: IDialog) {
        this.entries = null;
        this.searchSpec = new SearchSpec();
        this.entrySort = new OrderSpec({ by: "", desc: false });
        this.selectedPercent = 0;

        this.reload();
    }

    reload() {
        this.selectedPercent = 0;
        LcuRequest.get().then(xs => this.entries = xs);
    }

    delete() {
        if (this.entries == null) return;

        const rs = this.entries.filter(r => r.isSelected);
        if (rs.length < 0) return;

        const m = `Are you sure you want to delete selected requests?`;
        this.dialog.confirm(m, "Confirm")
            .then((isConfirmed) => {
                if (!isConfirmed) return false;
                LcuRequest.delete(rs).then(
                    () => { this.reload(); },
                    (e: string) => this.dialog.alert(e, "Delete Fail"));
                return true;
            });
    }

    run() {
        if (this.entries == null) return;

        const rs = this.entries.filter(r => r.isSelected);
        if (rs.length < 0) return false;

        LcuRequest.run(rs, this.entries.length).then(
            (r) => { window.location.assign(r.redirectUrl) },
            (e: string) => this.dialog.alert(e, "Run Fail"));

        return false;
    }

    runAll() {
        LcuRequest.runAll().then(
            (r) => { window.location.assign(r.redirectUrl) },
            (e: string) => this.dialog.alert(e, "Run All Fail"));

        return false;
    }

    errorInfoModal: ErrorInfoController;
    showErrorInfo(e: LcuRequest) {
        this.errorInfoModal.show(e);
    }

    onEntrySelect() {
        console.debug("event[onEntrySelect]");
        if (this.entries == null || this.entries.length < 1) return;

        const numberOfSelected = this.entries.reduce((c: number, e: LcuRequest) => c + (e.isSelected ? 1 : 0), 0);
        this.selectedPercent = numberOfSelected / this.entries.length;
    }
    onSelectAll($event: ng.IAngularEvent) {
        if (this.entries == null) return;

        const isSelect = (($event.currentTarget as HTMLInputElement).checked);
        for (let e of this.entries) {
            e.isSelected = isSelect;
        }
        this.selectedPercent = isSelect ? 1 : 0;
    }
}

import "./style.css";

angular.module("app", [])
    .factory   ("serviceGen"                , serviceGen            )
    .directive ("selectWhenClicked"         , selectWhenClicked     )
    .directive ("checkboxIndeterminate"     , checkboxIndeterminate )

    .service   ("dialog"                    , Dialog                )
    .directive ("sortHandle"                , sortHandle            )

    .factory   (LcuRequest.ngName           , LcuRequest.ngFactory  )

    .directive ("errorInfo"                 , errorInfo             )
    .component("requestList"                , { controller: RequestListController, controllerAs: "vm", template: appHtml })
;
