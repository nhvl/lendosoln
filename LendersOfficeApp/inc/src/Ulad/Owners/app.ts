import * as angular from 'angular';
import { ITimeoutService } from 'angular';

import ConsumerDefinition from '../Common/ConsumerDefinition';
import Dictionary from '../../api/Dictionary';
import LqbDataServiceRequestFormatProvider from '../../api/LqbDataServiceRequestFormatProvider';
import LqbDataServiceRequestParameterFactory from '../../api/LqbDataServiceRequestParameterFactory';
import LqbDataServiceResponseHandler from '../../api/LqbDataServiceResponseHandler';
import LqbDataServiceOperationType from '../../api/LqbDataServiceOperationType';
import LqbDataServiceRequestType from '../../api/LqbDataServiceRequestType';

import BooleanLqbDataServiceResponseConverter from '../../api/BooleanLqbDataServiceResponseConverter';
import EditOwnerPopupHelper from '../Common/EditOwnerPopupHelper';
import UladAssociationHelper from '../Common/UladAssociationHelper';

import getConsumerName from '../Common/Utils/getConsumerName';
import sendWebRequest from '../Common/Utils/sendWebRequest';

class OwnersPageConsumerDefinition extends ConsumerDefinition {
    FirstName: string = '';
    LastName: string = '';
}

declare global {
    interface Window { LQBPopup: any; }
}

declare function getQueryStringVariable(variable: string) : string;
declare var VRoot: string;
declare var LQBPopup: any;

const $ = jQuery;
const uladAssociationHelper = new UladAssociationHelper();
const parameterFactory = new LqbDataServiceRequestParameterFactory()
const booleanValueConverter = new BooleanLqbDataServiceResponseConverter();

angular
.module('Owners', [])
.controller('OwnersController', ['$scope', '$timeout', function OwnersController($scope, $timeout: ITimeoutService) {
	$scope.selectedRecordId = getQueryStringVariable('id');
    $scope.selectedOwners = new Array<Dictionary<string>>();

    $scope.needsPrimary = function (): boolean {
        const entityType = getQueryStringVariable('t');
        return entityType != 'CounselingEvent';
    }

    $scope.getTitle = function (): string {
        const entityType = getQueryStringVariable('t');
        switch (entityType) {
            case 'CounselingEvent':
                return 'Select Attendees';
            default:
                return 'Select Owners';
        }
    }

    $scope.getOwnerLabel = function (): string {
        const entityType = getQueryStringVariable('t');
        switch (entityType) {
            case 'CounselingEvent':
                return 'Attendee';
            default:
                return 'Owner';
        }
    }

    $scope.getConsumerName = function (consumer: Dictionary<string>) : string {
        return getConsumerName([consumer], consumer['Id']);
    }
	
    $scope.setOwnerSelectedState = function (consumer: Dictionary<any>) {
        // This method just sets the primary consumer.
        // So, it does not need to do anything if a primary is not needed.
        if (!$scope.needsPrimary()) {
            return;
        }

        if (consumer['IsSelected']) {
            // If there's no primary yet, set this consumer as the primary owner.
            const existingPrimaryOwner = $scope.vm.loan.Consumers.find((consumer: Dictionary<any>) => consumer['IsSelected'] && consumer['IsPrimary']);
            if (existingPrimaryOwner == null) {
                consumer['IsPrimary'] = true;
            }
        }
        else if (consumer['IsPrimary']) {
            // Set the new primary owner as the top-most selected consumer.
            consumer['IsPrimary'] = false;

            for (let i = 0; i < $scope.vm.loan.Consumers.length; ++i) {
                let otherConsumer: Dictionary<any> = $scope.vm.loan.Consumers[i];
                if (otherConsumer['Id'] != consumer['Id'] && otherConsumer['IsSelected']) {
                    otherConsumer['IsPrimary'] = true;
                    break;
                }
            }
        }
    }

    $scope.setPrimaryOwnerState = function (consumer: Dictionary<any>) {
        $.each($scope.vm.loan.Consumers, (index: number, existingConsumer: Dictionary<any>) => {
            existingConsumer['IsPrimary'] = existingConsumer['Id'] === consumer['Id'];
        });
    }
	
	$scope.processResult = function(response: Dictionary<any>) {
		for (const consumer of response['Consumers']) {
			var matchingOwner = $scope.selectedOwners.find((owner: Dictionary<string>) => owner['ConsumerId'] === consumer['Id']);
            consumer.IsSelected = matchingOwner != null;

            if ($scope.needsPrimary()) {
                consumer.IsPrimary = consumer.IsSelected && matchingOwner['IsPrimary'];
            }
		}
	}
	
	$scope.applyResult = function(data: Dictionary<any>) {
		$scope.$apply(function() {
			const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
			const response = handler.getResponseByKey(LqbDataServiceRequestType.Get, 'loan');		

			$scope.processResult(response);
            $scope.vm.loan = Object.assign($scope.vm.loan, response);

            $timeout(() => {
                // Resize to fit content once the table has been bound
                // with the consumer data.
                parent.LQBPopup.Resize(NaN/*width*/, NaN/*height*/, {
                    isUsingDefaultWidth: true,
                    isUsingDefaultHeight: true
                });

                parent.LQBPopup.UpdateHeaderText($scope.getTitle());
            });
		});
	}
	
    $scope.loadData = function() {
        const args = parameterFactory.createIndividualGetParameter([OwnersPageConsumerDefinition]);
		
		let provider = new LqbDataServiceRequestFormatProvider();
        provider.addRequest(LqbDataServiceRequestType.Get, args);

        sendWebRequest(
            LqbDataServiceOperationType.Load,
            provider.toRequest(),
            (data: Dictionary<any>) => { $scope.applyResult(data); });
	}
	
	$scope.onEditOwner = function() {
        let returnArgs = new Dictionary<any>();

        const selectedOwners = $scope.vm.loan.Consumers.filter((consumer: Dictionary<string>) => consumer['IsSelected']);
        if (selectedOwners.length === 0) {
            alert('Please select at least one owner.');
            return;
        }

        returnArgs['Id'] = $scope.selectedRecordId;

        if ($scope.needsPrimary()) {
            const primaryOwner = selectedOwners.find((consumer: Dictionary<string>) => consumer['IsPrimary']);
            if (primaryOwner == null) {
                alert('Please select a primary owner.');
                return;
            }

            const additionalOwners = selectedOwners.filter((consumer: Dictionary<string>) => consumer['Id'] !== primaryOwner['Id']);

            returnArgs['PrimaryOwner'] = primaryOwner;
            returnArgs['AdditionalOwners'] = additionalOwners;
        } else {
            returnArgs['Attendees'] = selectedOwners;
        }
		
		parent.LQBPopup.Return(returnArgs);
	}
	
	$scope.init = function() {
		$scope.vm = new Dictionary<Dictionary<any>>();
		$scope.vm.loan = new Dictionary<any>();
		
		const ownerIdCacheKey = getQueryStringVariable('s');
	
        var helper = new EditOwnerPopupHelper();
        helper.retrieveEntityOwnerIdsFromCache(ownerIdCacheKey, (result: Dictionary<any>) => {
            if (result.error) {
                alert(result.UserMessage);
                return;
            }

            const ownersJson = result.value['IdList'];
            $scope.selectedOwners = JSON.parse(ownersJson);
            $scope.loadData();
        });
	}

    // Allow init scripts like service registration to complete
    // before running the controller's initialization.
    $timeout(() => {
        $scope.init();
    });
}]);