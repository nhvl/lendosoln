import * as angular from 'angular';
import { ITimeoutService } from 'angular';

import { AllApplicationsFilter, LegacyApplicationsFilter, UladApplicationsFilter, BorrowerFilter, ApplicationLabelsByValue } from '../Common/ApplicationFilters';
import ConsumerRealPropertiesDefinition from '../Common/ConsumerRealPropertiesDefinition';
import EditOwnerPopupHelper from '../Common/EditOwnerPopupHelper';
import LegacyApplicationConsumersDefinition from '../Common/LegacyApplicationConsumersDefinition';
import LegacyApplicationDefinition from '../Common/LegacyApplicationDefinition';
import LoanDefinition from '../Common/LoanDefinition';
import RealPropertyLiabilitiesDefinition from '../Common/RealPropertyLiabilitiesDefinition';
import TotalsType from '../Common/TotalsType';
import UladApplicationDefinition from '../Common/UladApplicationDefinition';
import UladApplicationConsumersDefinition from '../Common/UladApplicationConsumersDefinition';
import UladAssociationHelper from '../Common/UladAssociationHelper';

import BooleanLqbDataServiceResponseConverter from '../../api/BooleanLqbDataServiceResponseConverter';
import Dictionary from '../../api/Dictionary';
import LqbDataServiceRequestFormatProvider from '../../api/LqbDataServiceRequestFormatProvider';
import LqbDataServiceRequestParameterFactory from '../../api/LqbDataServiceRequestParameterFactory';
import LqbDataServiceResponseHandler from '../../api/LqbDataServiceResponseHandler';
import LqbDataServiceOperationType from '../../api/LqbDataServiceOperationType';
import LqbDataServiceRequestType from '../../api/LqbDataServiceRequestType';

import RealPropertyAddRecordHelper from './RealPropertyAddRecordHelper';
import RealPropertySortHelper from './RealPropertySortHelper';
import {
    RealPropertiesPageSubjectPropertyDefinition,
    RealPropertiesPagePresentAddressDefinition,
    RealPropertiesPageReoDefinition,
    RealPropertiesPageLiabilityDefinition,
    RealPropertiesPageConsumerDefinition
} from './RealPropertyDefinitions';

import getAppId from '../Common/Utils/getAppId';
import getAppName from '../Common/Utils/getAppName';
import getConsumerName from '../Common/Utils/getConsumerName';
import getFullRealPropertyAddress from '../Common/Utils/getFullRealPropertyAddress';
import getLoanId from '../Common/Utils/getLoanId';
import getOwnersForNewEntity from '../Common/Utils/getOwnersForNewEntity';
import getTotalArgs from '../Common/Utils/getTotalArgs';
import normalizeSelectedApplications from '../Common/Utils/normalizeSelectedApplications';
import orderConsumerNames from '../Common/Utils/orderConsumerNames';
import sendWebRequest from '../Common/Utils/sendWebRequest';

declare function clearDirty(): void;
declare function updateDirtyBit(): void;
declare function isDirty(): boolean;
declare function _initInput(): void;
declare function PolyConfirmSave(confirmCallback: (shouldSave: boolean) => void, shouldShowCancel: boolean, cancelCallback?: () => void): void;

declare var LQBPopup: any;
declare var VRoot: string;
declare var ReoStatusByEnumConstantValue: Dictionary<string>;

const parameterFactory = new LqbDataServiceRequestParameterFactory();
const uladAssociationHelper = new UladAssociationHelper();
const booleanValueConverter = new BooleanLqbDataServiceResponseConverter();

angular
.module('RealProperties', [])
.controller('RealPropertiesController', ['$scope', '$timeout', function RealPropertiesController($scope, $timeout: ITimeoutService) {
    const moneyFieldIds = [
        'NetRentIncData',
        'MarketValue',
        'MtgAmt',
        'GrossRentInc',
        'MtgPmt',
        'HExp'
    ];

    const DefaultRealPropertyType = '0'; // Blank
    const DefaultRealPropertyStatus = '0'; // Retained

    $scope.applicationLabelsByValue = ApplicationLabelsByValue;

    $scope.sortKey = null;
    $scope.sortDesc = false;

    $scope.realPropertyFilterModel = AllApplicationsFilter;
    $scope.selectedApplicationIdModel = null;

    $scope.selectedRealProperty = null;
    $scope.originalSelectedRealProperty = null;

    $scope.selectedApplications = new Array<string>();
    $scope.selectedRecordIndex = 0;

    $scope.isDirty = isDirty;
    $scope.updateDirtyBit = updateDirtyBit;

    $scope.getFriendlyReoStatusName = function (reoStatus: string): string {
        return ReoStatusByEnumConstantValue[reoStatus];
    }

    $scope.getFullRealPropertyAddress = getFullRealPropertyAddress;

    $scope.onFormatMoney = function (input: HTMLInputElement) {
        if (moneyFieldIds.indexOf(input.id) !== -1) {
            // Ensure the real property's money fields are formatted 
            // in the model as expected, which happens after the initial 
            // model update.
            $scope.$apply(() => {
                $scope.selectedRealProperty[input.id] = input.value;
            });
        }
    }

    $scope.onSmartZipcode = function () {
        $scope.$apply(() => {
            // The changes to the inputs happen asynchronously and
            // will not update the model's value during the normal
            // digest cycle.
            $scope.selectedRealProperty['City'] = (document.getElementById('City') as HTMLInputElement).value;
            $scope.selectedRealProperty['State'] = $('#State').val() as string;
        });
    }

    $scope.refreshCalculation = function () {
        // Allow other scripts to finish running before calling
        // for a data refresh, e.g. money formatting.
        $timeout(() => {
            $scope.saveData(null/*callback*/, true/*refreshOnly*/);
        });
    }

    $scope.revertRealPropertyChanges = function (backupRealProperty: Dictionary<any>) {
        if ($scope.vm.loan == null || $scope.vm.loan.RealProperties == null) {
            return;
        }

        const backupId = backupRealProperty['Id'];
        for (let i = 0; i < $scope.vm.loan.RealProperties.length; ++i) {
            const realProperty = $scope.vm.loan.RealProperties[i];

            if (realProperty['Id'] === backupId) {
                $scope.vm.loan.RealProperties[i] = backupRealProperty;
                return;
            }
        }
    }

    $scope.checkForSave = function (postCallback: () => void, cancelCallback?: () => void, updateTotalsOnSave: boolean = true) {
        if (isDirty()) {
            PolyConfirmSave(
                (shouldSave: boolean) => {
                    if (shouldSave) {
                        $scope.saveData(postCallback, false/*refreshOnly*/, updateTotalsOnSave);
                    }
                    else {
                        clearDirty();

                        $timeout(() => {
                            $scope.revertRealPropertyChanges($scope.originalSelectedRealProperty);
                            $scope.setSelectedRealProperty($scope.originalSelectedRealProperty);
                            $scope.refreshCalculation();

                            postCallback();
                        });
                    }
                },
                true/*shouldShowCancel*/,
                () => {
                    if (cancelCallback != null) {
                        $timeout(cancelCallback);
                    }
                });
        }
        else {
            postCallback();
        }
    }

    $scope.getLinkedLiabilities = function (realPropertyId: string): Array<Dictionary<string>> {
        const liabilityAssociations = $scope.vm.loan.RealPropertyLiabilities
            .filter((association: Dictionary<string>) => association['RealPropertyId'] == realPropertyId);

        return $scope.vm.loan.Liabilities.filter((liability: Dictionary<string>) => {
            const liabilityId = liability['Id'];
            const matchingAssociation = liabilityAssociations.find((association: Dictionary<string>) => association['LiabilityId'] === liabilityId);
            return matchingAssociation != null;
        });
    }

    $scope.goToLinkedLiability = function (liabilityId: string) {
        liabilityId = encodeURIComponent(liabilityId);
        window.location.href = `${VRoot}/newlos/Ulad/Liabilities.aspx?loanid=${getLoanId()}&liabilityId=${liabilityId}`;
    }

    $scope.sortRealProperties = function(realProperty1: Dictionary<any>, realProperty2: Dictionary<any>): number {
        var helper = new RealPropertySortHelper($scope.sortKey, $scope.sortDesc, $scope.getConsumerName, $scope.getFullRealPropertyAddress);
        return helper.sort(realProperty1, realProperty2);
    }

    $scope.orderBy = function (key: string) {
        $scope.checkForSave(() => {
            if (key === $scope.sortKey) {
                $scope.sortDesc = !$scope.sortDesc;
            }
            else {
                $scope.sortKey = key;
                $scope.sortDesc = false;
            }

            if ($scope.vm.loan == null ||
                $scope.vm.loan.RealProperties == null ||
                $scope.vm.loan.RealProperties.length === 0) {
                return;
            }

            $scope.vm.loan.RealProperties.sort($scope.sortRealProperties);
            $scope.setRecordOrder($scope.vm.loan.RealProperties);

            // Update the selected record index to account for the new
            // order based on the sorting result.
            const selectedRealPropertyId = $scope.selectedRealProperty['Id'];
            $scope.selectedRecordIndex = $scope.vm.loan.RealProperties.findIndex((realProperty: Dictionary<any>) => {
                return realProperty['Id'] == selectedRealPropertyId;
            });
        });
    }

    $scope.setSelectedRealProperty = function (realProperty: Dictionary<any>) {
        // Normalize the type to blank if it was previously undefined.
        if (realProperty['Type'] == null || realProperty['Type'].trim() === "") {
            realProperty['Type'] = DefaultRealPropertyType;
        }

        // Normalize the status to 'Retained' if it was previously undefined
        // to match old REO editor.
        if (realProperty['Status'] == null || realProperty['Status'].trim() === "") {
            realProperty['Status'] = DefaultRealPropertyStatus;
        }

        // Default to false to match the old REO editor.
        if (realProperty['NetRentIncLocked'] == null || realProperty['NetRentIncLocked'] == '') {
            realProperty['NetRentIncLocked'] = false;
        }

        $scope.selectedRealProperty = realProperty;

        const backupRealProperty: Dictionary<any> = Object.assign(new Dictionary<any>(), realProperty);
        $scope.originalSelectedRealProperty = backupRealProperty;
    }

    $scope.moveToPreviousRecord = function () {
        if ($scope.vm.loan.RealProperties == null || $scope.vm.loan.RealProperties.length === 0) {
            return;
        }

        $scope.checkForSave(() => {
            const startIndex = Math.max($scope.selectedRecordIndex - 1, 0);
            let nextIndex = -1;

            for (let i = startIndex; i >= 0; --i) {
                const realPropertyAtIndex = $scope.vm.loan.RealProperties[i];
                if ($scope.realPropertyMatchesFilter(realPropertyAtIndex)) {
                    nextIndex = i;
                    break;
                }
            }

            if (nextIndex >= 0) {
                $scope.selectedRecordIndex = nextIndex;
                $scope.setSelectedRealProperty($scope.vm.loan.RealProperties[$scope.selectedRecordIndex]);
            }
            else if ($scope.selectedRecordIndex === 0) {
                $scope.moveToNextRecord();
            }
        });
    }

    $scope.moveToNextRecord = function () {
        if ($scope.vm.loan.RealProperties == null || $scope.vm.loan.RealProperties.length === 0) {
            return;
        }

        $scope.checkForSave(() => {
            const startIndex = Math.min($scope.selectedRecordIndex + 1, $scope.vm.loan.RealProperties.length - 1);
            let nextIndex = -1;

            for (let i = startIndex; i < $scope.vm.loan.RealProperties.length; ++i) {
                const realPropertyAtIndex = $scope.vm.loan.RealProperties[i];
                if ($scope.realPropertyMatchesFilter(realPropertyAtIndex)) {
                    nextIndex = i;
                    break;
                }
            }

            if (nextIndex >= 0) {
                $scope.selectedRecordIndex = nextIndex;
                $scope.setSelectedRealProperty($scope.vm.loan.RealProperties[$scope.selectedRecordIndex]);
            }
        });
    }

    $scope.selectRealProperty = function (realProperty: Dictionary<string>) {
        $scope.checkForSave(() => {
            const realPropertyId = realProperty['Id'];

            for (let i = 0; i < $scope.vm.loan.RealProperties.length; ++i) {
                const realPropertyAtIndex = $scope.vm.loan.RealProperties[i];

                if (realPropertyId === realPropertyAtIndex['Id']) {
                    $scope.selectedRecordIndex = i;
                    $scope.setSelectedRealProperty($scope.vm.loan.RealProperties[$scope.selectedRecordIndex]);
                    break;
                }
            }
        });
    }

    $scope.insertRecord = function () {
        $scope.checkForSave(() => {
            const insertIndex = Math.max(0, $scope.selectedRecordIndex);
            $scope.addRealProperty(insertIndex);
        });
    }

    $scope.addRecord = function () {
        $scope.checkForSave(() => {
            $scope.addRealProperty();
        });
    }

    $scope.addRealProperty = function (insertIndex?: number) {
        const hadActiveRealProperty = $scope.vm.loan.RealProperties.filter($scope.realPropertyMatchesFilter).length !== 0;

        const owners = getOwnersForNewEntity(
            $scope.selectedApplications,
            $scope.selectedApplicationIdModel,
            $scope.vm.loan.UladApplications,
            $scope.vm.loan.UladApplicationConsumers);

        const helper = new RealPropertyAddRecordHelper(owners, parameterFactory, insertIndex);
        const provider = helper.generateRequestFormatProvider();

        sendWebRequest(
            LqbDataServiceOperationType.Save,
            provider.toRequest(),
            (data: Dictionary<any>) => {
                $scope.addRealPropertyCallback(data, helper, hadActiveRealProperty, insertIndex);
            });
    }

    $scope.addRealPropertyCallback = function (data: Dictionary<any>, helper: RealPropertyAddRecordHelper, hadActiveRealProperty: boolean, insertIndex?: number) {
        const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
        if (handler.hasError()) {
            alert(handler.getErrorMessage());
            return;
        }

        $scope.$apply(function () {
            const response = handler.getResponseByKey(LqbDataServiceRequestType.Add, 'loan');

            var realProperty = new Dictionary<any>();
            realProperty['Id'] = response['id'];
            realProperty['Owners'] = [helper.getPrimaryOwner(), ...helper.getAdditionalOwners()];

            if (insertIndex == null) {
                const realPropertiesLength = $scope.vm.loan.RealProperties.push(realProperty);
                $scope.selectedRecordIndex = realPropertiesLength - 1;
            }
            else {
                $scope.vm.loan.RealProperties.splice(insertIndex, 0/*No entries before: only adding the real property*/, realProperty);
                $scope.selectedRecordIndex = insertIndex;
            }

            orderConsumerNames([realProperty], $scope.getConsumerName);
            $scope.setSelectedRealProperty(realProperty);

            if (!hadActiveRealProperty) {
                // No inputs are in the DOM until there is an active real property.
                // Once we've added a real property, initialize the new inputs.
                window.setTimeout(_initInput, 0);
            }
        });
    }

    $scope.realPropertyMatchesFilter = function (realProperty: Dictionary<any>) {
        if ($scope.vm.loan.ConsumerRealProperties == null) {
            return true;
        }

        if ($scope.selectedApplications == null || $scope.realPropertyFilterModel === AllApplicationsFilter) {
            return true;
        }

        const applicableApps = $scope.selectedApplications.filter((app: Dictionary<any>) => app['key'] === $scope.selectedApplicationIdModel)
            .map((app: Dictionary<any>) => app['values'])
            .reduce((a: Array<Dictionary<any>>, b: Array<Dictionary<any>>) => a.concat(b), new Array<Dictionary<any>>());

        if (applicableApps.length === 0) {
            return false;
        }

        const matchingOwner = realProperty['Owners'].find(function (realPropertyOwner: Dictionary<boolean|string>) {
            // Ignore IsPrimary when searching at the borrower level.
            if ($scope.realPropertyFilterModel !== BorrowerFilter && !realPropertyOwner['IsPrimary']) {
                return false;
            }

            return applicableApps.find((app: Dictionary<string>) => app['ConsumerId'] === realPropertyOwner['ConsumerId']) != null;
        });

        return matchingOwner != null;
    }

    $scope.getGetRealPropertyTotalArgs = function () {
        return getTotalArgs(parameterFactory, $scope.realPropertyFilterModel, $scope.selectedApplicationIdModel);
    }

    $scope.updateRealPropertyTotalsForCurrentApplication = function () {
        const getRealPropertyTotalArgs = $scope.getGetRealPropertyTotalArgs();
        $scope.updateRealPropertyTotals(getRealPropertyTotalArgs);
    }

    $scope.updateRealPropertyTotals = function (getTotalArgs: Dictionary<string>) {
        let provider = new LqbDataServiceRequestFormatProvider();
        provider.addRequest(LqbDataServiceRequestType.GetRealPropertyTotals, getTotalArgs);

        sendWebRequest(
            LqbDataServiceOperationType.Load,
            provider.toRequest(),
            (data: Dictionary<any>) => {
                const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
                if (handler.hasError()) {
                    alert(handler.getErrorMessage());
                    return;
                }

                $scope.$apply(() => {
                    $scope.vm.reoTotals = handler.getResponseByKey(LqbDataServiceRequestType.GetRealPropertyTotals, 'loan');
                });
            });
    }

    $scope.deleteRecord = function () {
        const recordId = $scope.selectedRealProperty['Id'];

        const deleteArgs = parameterFactory.createRemoveParameter(RealPropertiesPageReoDefinition, recordId);
        const getTotalArgs = $scope.getReoTotalArgs();

        let provider = new LqbDataServiceRequestFormatProvider();
        provider.addRequest(LqbDataServiceRequestType.Remove, deleteArgs);
        provider.addRequest(LqbDataServiceRequestType.GetRealPropertyTotals, getTotalArgs);

        sendWebRequest(
            LqbDataServiceOperationType.Save,
            provider.toRequest(),
            (data: Dictionary<any>) => {
                const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
                if (handler.hasError()) {
                    alert(handler.getErrorMessage());
                    return;
                }

                $scope.$apply(function () {
                    $scope.vm.reoTotals = handler.getResponseByKey(LqbDataServiceRequestType.GetRealPropertyTotals, 'loan');

                    if ($scope.vm.loan.RealProperties.length === 1) {
                        $scope.vm.loan.RealProperties = new Array<Dictionary<string>>();
                        $scope.selectedRecordIndex = 0;
                        $scope.selectedRealProperty = null;
                    }
                    else {
                        $scope.vm.loan.RealProperties.splice($scope.selectedRecordIndex, 1);
                        $scope.moveToPreviousRecord();
                    }
                });
            });
    }

    $scope.moveRecordUp = function () {
        if ($scope.selectedRecordIndex === 0) {
            return;
        }

        $scope.checkForSave(() => {
            const realProperties = $scope.vm.loan.RealProperties;
            const temp = realProperties[$scope.selectedRecordIndex];
            realProperties[$scope.selectedRecordIndex] = realProperties[$scope.selectedRecordIndex - 1];
            realProperties[$scope.selectedRecordIndex - 1] = temp;

            $scope.setRecordOrder(realProperties, () => { $scope.selectedRecordIndex-- });
        });
    }

    $scope.moveRecordDown = function () {
        if ($scope.selectedRecordIndex === $scope.vm.loan.RealProperties.length - 1) {
            return;
        }

        $scope.checkForSave(() => {
            const realProperties = $scope.vm.loan.RealProperties;
            const temp = realProperties[$scope.selectedRecordIndex];
            realProperties[$scope.selectedRecordIndex] = realProperties[$scope.selectedRecordIndex + 1];
            realProperties[$scope.selectedRecordIndex + 1] = temp;

            $scope.setRecordOrder(realProperties, () => { $scope.selectedRecordIndex++ });
        });
    }

    $scope.setRecordOrder = function (realProperties: Array<Dictionary<string>>, callback?: () => void) {
        const realPropertyIds = realProperties.map((realProperty: Dictionary<string>) => realProperty['Id']);
        const args = parameterFactory.createSetOrderParameter(RealPropertiesPageReoDefinition, realPropertyIds);

        let provider = new LqbDataServiceRequestFormatProvider();
        provider.addRequest(LqbDataServiceRequestType.SetOrder, args);

        sendWebRequest(
            LqbDataServiceOperationType.Save,
            provider.toRequest(),
            (data: Dictionary<any>) => {
                const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
                if (handler.hasError()) {
                    alert(handler.getErrorMessage());
                    return;
                }

                $scope.$apply(() => {
                    $scope.vm.loan.RealProperties = realProperties;

                    if (typeof callback === 'function') {
                        callback();
                    }
                });
            });
    }

    $scope.editRealPropertyOwners = function (realProperty: Dictionary<any>) {
        $scope.checkForSave(() => {
            const selectedOwners = realProperty['Owners'];

            var helper = new EditOwnerPopupHelper();
            helper.storeEntityOwnerIdsToCache(selectedOwners, (result: Dictionary<any>) => {
                if (result.error) {
                    alert(result.UserMessage);
                    return;
                }

                const cacheKey = result.value['CacheKey'];
                const url = `${VRoot}/newlos/Ulad/EditOwner.aspx?loanid=${getLoanId()}&appid=${getAppId()}&id=${realProperty['Id']}&s=${cacheKey}`;
                const settings = {
                    onReturn: $scope.onOwnerPopupClose
                };
                LQBPopup.Show(url, settings);
            });
        });
    }

    $scope.onOwnerPopupClose = function (returnArgs: Dictionary<any>) {
        const primaryOwner: Dictionary<string> = returnArgs['PrimaryOwner'];
        const additionalOwners: Array<Dictionary<string>> = returnArgs['AdditionalOwners'];

        const getRealPropertyTotalArgs = $scope.getReoTotalArgs();
        const setOwnerArgs = parameterFactory.createSetOwnerParameter(
            ConsumerRealPropertiesDefinition,
            $scope.selectedRealProperty['Id'],
            primaryOwner['Id'],
            additionalOwners.map((owner: Dictionary<string>) => owner['Id']));

        let provider = new LqbDataServiceRequestFormatProvider();
        provider.addRequest(LqbDataServiceRequestType.SetOwner, setOwnerArgs);
        provider.addRequest(LqbDataServiceRequestType.GetRealPropertyTotals, getRealPropertyTotalArgs);

        sendWebRequest(
            LqbDataServiceOperationType.Save,
            provider.toRequest(),
            (data: Dictionary<any>) => {
                $scope.setOwnerCallback(data);
            });
    }

    $scope.setOwnerCallback = function (data: Dictionary<any>) {
        const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
        if (handler.hasError()) {
            alert(handler.getErrorMessage());
            return;
        }

        LQBPopup.Hide();

        const provider = $scope.createInitProvider();
        $scope.loadData(provider.toRequest(), () => {
            $scope.vm.reoTotals = handler.getResponseByKey(LqbDataServiceRequestType.GetRealPropertyTotals, 'loan');

            // Ensure that the editor is hidden when the real property
            // no longer matches the application filter. For the "All"
            // view, this logic is ignored.
            if ($scope.realPropertyFilterModel === AllApplicationsFilter) {
                $scope.setSelectedRealProperty($scope.vm.loan.RealProperties[$scope.selectedRecordIndex]);
                return;
            }

            const selectedApplication: Dictionary<Array<Dictionary<string>>> = $scope.selectedApplications.find((app: Dictionary<string>) => {
                return app['key'] === $scope.selectedApplicationIdModel;
            });

            const updatedSelectedRealProperty = $scope.vm.loan.RealProperties.find((realProperty: Dictionary<string>) => realProperty['Id'] === $scope.selectedRealProperty['Id']);
            const selectedRealPropertyNewOwners: Array<Dictionary<string>> = updatedSelectedRealProperty['Owners'];
            const selectedRealPropertyOwners: Array<Dictionary<string>> = selectedApplication['values'];

            const selectedRealPropertyOwnerInCurrentApp = selectedRealPropertyNewOwners.find((realPropertyOwner: Dictionary<string>) => {
                return selectedRealPropertyOwners.find((appOwner: Dictionary<string>) => appOwner['ConsumerId'] === realPropertyOwner['ConsumerId']) != null;
            });

            if (selectedRealPropertyOwnerInCurrentApp != null) {
                $scope.setSelectedRealProperty($scope.vm.loan.RealProperties[$scope.selectedRecordIndex]);
            }
            else {
                $scope.selectedRecordIndex = -1;
                $scope.selectedRealProperty = null;
                $scope.moveToNextRecord();
            }
        });
    }

    $scope.getReoTotalArgs = function () {
        return getTotalArgs(parameterFactory, $scope.realPropertyFilterModel, $scope.selectedApplicationIdModel);
    }

    $scope.copyFromBorrowerPresentAddress = function () {
        const selectedRealPropertyOwners: Array<Dictionary<string>> = $scope.selectedRealProperty['Owners'];
        const primaryOwnerConsumerId = selectedRealPropertyOwners[0]['ConsumerId'];

        $scope.copyReoAddressFromSource(
            RealPropertiesPagePresentAddressDefinition,
            (response: Dictionary<Array<Dictionary<string>>>) => {
                const consumers = response['Consumers'];
                if (consumers == null || consumers.length === 0) {
                    alert('System error. Please contact us if this happens again.');
                    return;
                }

                const primaryConsumer = consumers[0];

                $scope.selectedRealProperty['StreetAddress'] = primaryConsumer['Address'];
                $scope.selectedRealProperty['City'] = primaryConsumer['City'];
                $scope.selectedRealProperty['State'] = primaryConsumer['State'];
                $scope.selectedRealProperty['Zip'] = primaryConsumer['Zip'];
                $scope.selectedRealProperty['IsPrimaryResidence'] = true;

                $scope.updateCashFlowCalculationCheckbox();
                updateDirtyBit();
            },
            primaryOwnerConsumerId);
    }

    $scope.copyFromSubjectProperty = function () {
        $scope.copyReoAddressFromSource(
            RealPropertiesPageSubjectPropertyDefinition,
            (response: Dictionary<string>) => {
                $scope.selectedRealProperty['StreetAddress'] = response['sSpAddr'];
                $scope.selectedRealProperty['City'] = response['sSpCity'];
                $scope.selectedRealProperty['State'] = response['sSpState'];
                $scope.selectedRealProperty['Zip'] = response['sSpZip'];
                $scope.selectedRealProperty['IsSubjectProp'] = true;

                $scope.updateCashFlowCalculationCheckbox();
                updateDirtyBit(); });
    }

    $scope.updateCashFlowCalculationCheckbox = function () {
        if ($scope.selectedRealProperty['IsPrimaryResidence'] || $scope.selectedRealProperty['IsSubjectProp']) {
            $scope.selectedRealProperty['IsForceCalcNetRentalInc'] = false;
        }

        $scope.refreshCalculation();
    }

    $scope.onStatusChange = function () {
        switch ($scope.selectedRealProperty.Status) {
            case "1": // S, Sale/Sold
                $scope.selectedRealProperty['IsForceCalcNetRentalInc'] = false;
                break;

            case "2": // PS, Pending Sale
                $scope.selectedRealProperty['IsForceCalcNetRentalInc'] = true;
                break;

            case "3": // R, Rental
                $scope.selectedRealProperty['OccR'] = "75.000%";
                $scope.selectedRealProperty['IsForceCalcNetRentalInc'] = false;
                break;

            case "0": // "", Residence
                $scope.selectedRealProperty['IsForceCalcNetRentalInc'] = true;
                break;

            default:
                throw `Unhandled REO status ${$scope.selectedRealProperty.Status}`;
        }

        $scope.updateCashFlowCalculationCheckbox();
    }

    $scope.copyReoAddressFromSource = function (definition: typeof LoanDefinition, callback: (response: Dictionary<string>) => void, recordId?: string) {
        const getArgs = parameterFactory.createIndividualGetParameter([definition], recordId);

        let provider = new LqbDataServiceRequestFormatProvider();
        provider.addRequest(LqbDataServiceRequestType.Get, getArgs);

        sendWebRequest(
            LqbDataServiceOperationType.Load,
            provider.toRequest(),
            (data: Dictionary<any>) => {
                const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
                if (handler.hasError()) {
                    alert(handler.getErrorMessage());
                    return;
                }

                $scope.$apply(() => {
                    const response = handler.getResponseByKey(LqbDataServiceRequestType.Get, 'loan');
                    callback(response);
                });
            });
    }

    $scope.getConsumerName = (consumerId: string) => getConsumerName($scope.vm.loan.Consumers, consumerId);

    $scope.getAppName = (appIdConsumerIdPairs: Array<Dictionary<string>>) => getAppName(appIdConsumerIdPairs, $scope.getConsumerName);

    $scope.saveData = function (callback: () => void, refreshOnly: boolean = false, updateTotalsOnSave: boolean = true) {
        let provider = new LqbDataServiceRequestFormatProvider();
        let setters = new Array<Dictionary<any>>();

        if ($scope.selectedRealProperty != null) {
            setters.push(parameterFactory.createIndividualSetParameter(
                RealPropertiesPageReoDefinition,
                $scope.selectedRealProperty,
                $scope.selectedRealProperty['Id']));
        }

        const setArgs = parameterFactory.createAggregateSetParameter(setters);
        provider.addRequest(LqbDataServiceRequestType.Set, setArgs);

        if (updateTotalsOnSave) {
            const getReoTotalArgs = $scope.getReoTotalArgs();
            provider.addRequest(LqbDataServiceRequestType.GetRealPropertyTotals, getReoTotalArgs);
        }

        let refreshDataOpId = '';
        if (refreshOnly) {
            const refreshDataArgs = parameterFactory.createIndividualGetParameter([RealPropertiesPageReoDefinition], $scope.selectedRealProperty['Id']);
            refreshDataOpId = provider.addRequest(LqbDataServiceRequestType.Get, refreshDataArgs);
        }

        sendWebRequest(
            refreshOnly ? LqbDataServiceOperationType.LoadNoOverlay : LqbDataServiceOperationType.Save,
            provider.toRequest(),
            (data: Dictionary<any>) => {
                const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
                if (handler.hasError()) {
                    alert(handler.getErrorMessage());
                    return;
                }

                $scope.$apply(() => {
                    if (updateTotalsOnSave) {
                        $scope.vm.reoTotals = handler.getResponseByKey(LqbDataServiceRequestType.GetRealPropertyTotals, 'loan');
                    }

                    if (refreshOnly) {
                        const getResponse = handler.getResponseByKey(LqbDataServiceRequestType.Get, 'loan', refreshDataOpId);
                        const realPropertyArray = getResponse['RealProperties'];
                        const updatedRealProperty = realPropertyArray[0];

                        // Avoid using Object.assign since Angular adds tracking
                        // metadata for the selected real property as part of applying
                        // the model to the editor.
                        for (const key of Object.keys(updatedRealProperty)) {
                            $scope.selectedRealProperty[key] = updatedRealProperty[key];
                        }
                    }
                    else {
                        clearDirty();
                    }

                    if (typeof callback === 'function') {
                        callback();
                    }
                });
            });
    }

    $scope.applyResult = function (data: Dictionary<any>, callback?: any) {
        $scope.$apply(function () {
            const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
            if (handler.hasError()) {
                alert(handler.getErrorMessage());
                return;
            }

            const getResponse = handler.getResponseByKey(LqbDataServiceRequestType.Get, 'loan');
            $scope.vm.loan = Object.assign($scope.vm.loan, getResponse);
            orderConsumerNames($scope.vm.loan.RealProperties, $scope.getConsumerName);

            const reoTotalsResponse = handler.getResponseByKey(LqbDataServiceRequestType.GetRealPropertyTotals, 'loan');
            $scope.vm.reoTotals = reoTotalsResponse;

            if (callback != null) {
                callback();
            }
        });
    }

    $scope.loadData = function (request: string, callback?: any) {
        sendWebRequest(
            LqbDataServiceOperationType.Load,
            request,
            (data: Dictionary<any>) => { $scope.applyResult(data, callback); });
    }

    $scope.displayAllRealProperties = function () {
        $scope.init();
    }

    $scope.displayLegacyAppRealProperties = function () {
        $scope.selectedApplications = normalizeSelectedApplications(
            $scope.vm.loan.LegacyApplicationConsumers,
            'LegacyApplicationId',
            'ConsumerId',
            (app: Dictionary<boolean>) => app['IsPrimary']);

        $scope.postNormalizePropertyFilterChange();
    }

    $scope.displayUladAppRealProperties = function () {
        $scope.selectedApplications = normalizeSelectedApplications(
            $scope.vm.loan.UladApplicationConsumers,
            'UladApplicationId',
            'ConsumerId',
            (app: Dictionary<boolean>) => app['IsPrimary']);

        $scope.postNormalizePropertyFilterChange();
    }

    $scope.displayBorrowerRealProperties = function () {
        $scope.selectedApplications = normalizeSelectedApplications(
            $scope.vm.loan.Consumers,
            'Id',
            'Id',
            (app: Dictionary<boolean>) => true);

        $scope.postNormalizePropertyFilterChange();
    }

    $scope.postNormalizePropertyFilterChange = function () {
        $scope.selectedApplicationIdModel = $scope.selectedApplications[0]['key'];

        $scope.selectedRecordIndex = -1;
        $scope.moveToNextRecord();

        $scope.updateRealPropertyTotalsForCurrentApplication();

        // Give the filtering time to complete before initializing inputs.
        $timeout(_initInput, 500);
    }

    $scope.onRealPropertyFilterChange = function (oldRealPropertyFilterModel: string) {
        $scope.checkForSave(
            () => {
                $scope.selectedApplicationIdModel = null;
                $scope.selectedApplications = null;
                $scope.selectedRealProperty = null;

                switch ($scope.realPropertyFilterModel) {
                    case AllApplicationsFilter:
                        $scope.displayAllRealProperties();
                        break;
                    case LegacyApplicationsFilter:
                        $scope.displayLegacyAppRealProperties();
                        break;
                    case UladApplicationsFilter:
                        $scope.displayUladAppRealProperties();
                        break;
                    case BorrowerFilter:
                        $scope.displayBorrowerRealProperties();
                        break;
                    default:
                        throw `Unhandled real property filter value ${$scope.realPropertyFilterModel}.`;
                }
            },
            () => {
                $scope.realPropertyFilterModel = oldRealPropertyFilterModel;
            },
            false/*updateTotalsOnSave*/);
    }

    $scope.onApplicationFilterChange = function (newSelectedApplicationIdModel: string) {
        const oldSelectedApplicationIdModel: string = $scope.selectedApplicationIdModel;

        $scope.checkForSave(
            () => {
                $scope.selectedApplicationIdModel = newSelectedApplicationIdModel;

                $scope.selectedRecordIndex = -1;
                $scope.selectedRealProperty = null;

                $scope.moveToNextRecord();
                $scope.updateRealPropertyTotalsForCurrentApplication();

                // Give the filtering time to complete before initializing inputs.
                $timeout(_initInput, 500);
            },
            () => {
                $('#ApplicationFilter').val(oldSelectedApplicationIdModel);
                $scope.selectedApplicationIdModel = oldSelectedApplicationIdModel;
            },
            false/*updateTotalsOnSave*/);
    }

    $scope.onNetRentIncLockedClick = function () {
        // Sync calculated and user-input fields when locking field.
        if ($scope.selectedRealProperty['NetRentIncLocked']) {
            $scope.selectedRealProperty['NetRentIncData'] = $scope.selectedRealProperty['NetRentInc'];
        }

        $scope.refreshCalculation();
    }

    $scope.init = function () {
        $scope.vm = new Dictionary<Dictionary<any>>();
        $scope.vm.loan = new Dictionary<any>();

        const provider = $scope.createInitProvider();
        $scope.loadData(provider.toRequest(), function () {
            $scope.moveToPreviousRecord();
            window.setTimeout(_initInput, 0);
        });
    }

    $scope.createInitProvider = function (): LqbDataServiceRequestFormatProvider {
        let provider = new LqbDataServiceRequestFormatProvider();

        const getArgs = parameterFactory.createIndividualGetParameter([
            ConsumerRealPropertiesDefinition,
            LoanDefinition,
            LegacyApplicationConsumersDefinition,
            RealPropertyLiabilitiesDefinition,
            RealPropertiesPageConsumerDefinition,
            RealPropertiesPageLiabilityDefinition,
            RealPropertiesPageReoDefinition,
            UladApplicationDefinition,
            UladApplicationConsumersDefinition]);

        const getReoTotalArgs = parameterFactory.createGetTotalsParameter(LoanDefinition, TotalsType.Loan);

        provider.addRequest(LqbDataServiceRequestType.Get, getArgs);
        provider.addRequest(LqbDataServiceRequestType.GetRealPropertyTotals, getReoTotalArgs);
        return provider;
    }

    $scope.init();
}]);
