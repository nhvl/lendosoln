import Dictionary from '../../api/Dictionary';

declare var ReoStatusByEnumConstantValue: Dictionary<string>;
declare function parseMoneyFloat(str: string): number;

export default class RealPropertySortHelper {
    constructor(
        private sortKey: string,
        private sortDesc: boolean,
        private getConsumerName: (consumerId: string) => string,
        private getFullRealPropertyAddress: (realProperty: Dictionary<any>) => string) { }

    public sort(realProperty1: Dictionary<any>, realProperty2: Dictionary<any>): number {
        const direction = this.sortDesc ? -1 : 1;

        switch (this.sortKey) {
            case 'Owner':
                const firstRealPropertyOwners: Array<Dictionary<any>> = realProperty1['Owners'];
                const secondRealPropertyOwners: Array<Dictionary<any>> = realProperty2['Owners'];

                const firstRealPropertyPrimaryOwner = firstRealPropertyOwners[0];
                const secondRealPropertyPrimaryOwner = secondRealPropertyOwners[0];

                const firstAssetPrimaryOwnerName = this.getConsumerName(firstRealPropertyPrimaryOwner['ConsumerId']);
                const secondAssetPrimaryOwnerName = this.getConsumerName(secondRealPropertyPrimaryOwner['ConsumerId']);

                return direction * firstAssetPrimaryOwnerName.localeCompare(secondAssetPrimaryOwnerName);

            case 'Address':
                const firstRealPropertyAddress = this.getFullRealPropertyAddress(realProperty1);
                const secondRealPropertyAddress = this.getFullRealPropertyAddress(realProperty2);

                return direction * firstRealPropertyAddress.localeCompare(secondRealPropertyAddress);

            case 'Status':
                const firstRealPropertyStatus = realProperty1['Status'];
                const secondRealPropertyStatus = realProperty2['Status'];

                if (firstRealPropertyStatus == null || firstRealPropertyStatus.trim().length === 0) {
                    if (secondRealPropertyStatus == null || secondRealPropertyStatus.trim().length === 0) {
                        return 0;
                    }

                    return direction * -1;
                }

                if (secondRealPropertyStatus == null || secondRealPropertyStatus.trim().length === 0) {
                    if (firstRealPropertyStatus == null || firstRealPropertyStatus.trim().length === 0) {
                        return 0;
                    }

                    return direction * 1;
                }

                const firstRealPropertyStatusDescription = ReoStatusByEnumConstantValue[firstRealPropertyStatus];
                const secondRealPropertyStatusDescription = ReoStatusByEnumConstantValue[secondRealPropertyStatus];

                return direction * firstRealPropertyStatusDescription.localeCompare(secondRealPropertyStatusDescription);

            case 'NetRentInc':
                const firstNetRentIncLocked = realProperty1['NetRentIncLocked'];
                const secondNetRentIncLocked = realProperty2['NetRentIncLocked'];

                let firstRealPropertyCashFlow = realProperty1[firstNetRentIncLocked ? 'NetRentIncData' : 'NetRentInc'];
                let secondRealPropertyCashFlow = realProperty2[secondNetRentIncLocked ? 'NetRentIncData' : 'NetRentInc'];

                if (firstRealPropertyCashFlow == null || firstRealPropertyCashFlow.trim().length === 0) {
                    firstRealPropertyCashFlow = Number.MIN_SAFE_INTEGER.toString();
                }

                if (secondRealPropertyCashFlow == null || secondRealPropertyCashFlow.trim().length === 0) {
                    secondRealPropertyCashFlow = Number.MIN_SAFE_INTEGER.toString();
                }

                const firstRealPropertyCashFlowParsed = parseMoneyFloat(firstRealPropertyCashFlow);
                const secondRealPropertyCashFlowParsed = parseMoneyFloat(secondRealPropertyCashFlow);

                if (firstRealPropertyCashFlowParsed === secondRealPropertyCashFlowParsed) {
                    return 0;
                }

                return direction * (firstRealPropertyCashFlowParsed > secondRealPropertyCashFlowParsed ? -1 : 1);

            default:
                throw 'Unhandled sort key ' + this.sortKey;
        }
    }
}