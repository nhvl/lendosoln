import Dictionary from "../../api/Dictionary";
import LqbDataServiceRequestFormatProvider from "../../api/LqbDataServiceRequestFormatProvider";
import LqbDataServiceRequestParameterFactory from "../../api/LqbDataServiceRequestParameterFactory";
import LqbDataServiceRequestType from "../../api/LqbDataServiceRequestType";
import { RealPropertiesPageReoDefinition } from "./RealPropertyDefinitions";

export default class RealPropertyAddRecordHelper {
    private primaryOwner: Dictionary<string | boolean>;
    private additionalOwners: Array<Dictionary<string | boolean>>;

    constructor(
        private owners: Array<Dictionary<string | boolean>>,
        private parameterFactory: LqbDataServiceRequestParameterFactory,
        private insertIndex?: number) { }

    public generateRequestFormatProvider() : LqbDataServiceRequestFormatProvider {
        const primaryOwner = this.owners.find((owner: Dictionary<string | boolean>) => owner['IsPrimary'] as boolean);
        if (primaryOwner == null) {
            throw 'Unable to add primary ownership for new real property.';
        }

        this.primaryOwner = primaryOwner;
        this.additionalOwners = this.owners.filter((owner: Dictionary<string | boolean>) => owner['ConsumerId'] !== this.primaryOwner['ConsumerId']);
        const additionalOwnerIds = this.additionalOwners.map((owner: Dictionary<string | boolean>) => owner['ConsumerId'] as string);

        const primaryConsumerId = this.primaryOwner['ConsumerId'] as string;
        const args = this.parameterFactory.createAddParameter(RealPropertiesPageReoDefinition, primaryConsumerId, additionalOwnerIds, this.insertIndex);

        let provider = new LqbDataServiceRequestFormatProvider();
        provider.addRequest(LqbDataServiceRequestType.Add, args);

        return provider;
    }

    public getPrimaryOwner(): Dictionary<string | boolean> {
        return this.primaryOwner;
    }

    public getAdditionalOwners(): Array<Dictionary<string | boolean>> {
        return this.additionalOwners;
    }
}