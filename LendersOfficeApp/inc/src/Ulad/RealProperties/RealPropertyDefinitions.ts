import ConsumerDefinition from '../Common/ConsumerDefinition';
import LoanDefinition from '../Common/LoanDefinition';
import ReoDefinition from '../Common/ReoDefinition';
import LiabilityDefinition from '../Common/LiabilityDefinition';

export class RealPropertiesPageSubjectPropertyDefinition extends LoanDefinition {
    sSpAddr: string = '';
    sSpCity: string = '';
    sSpState: string = '';
    sSpZip: string = '';
}

export class RealPropertiesPagePresentAddressDefinition extends ConsumerDefinition {
    Address: string = '';
    City: string = '';
    State: string = '';
    Zip: string = '';
}

export class RealPropertiesPageReoDefinition extends ReoDefinition {
    City: string = '';
    GrossRentInc: string = '';
    HExp: string = '';
    IsForceCalcNetRentalInc: string = '';
    IsPrimaryResidence: string = '';
    IsSubjectProp: string = '';
    MarketValue: string = '';
    MtgAmt: string = '';
    MtgPmt: string = '';
    NetRentInc: string = '';
    NetRentIncData: string = '';
    NetRentIncLocked: string = '';
    OccR: string = '';
    State: string = '';
    Status: string = '';
    StreetAddress: string = '';
    Type: string = '';
    Zip: string = '';

    isReadonly(field: string) {
        return field === 'NetRentInc'
    }
}

export class RealPropertiesPageLiabilityDefinition extends LiabilityDefinition {
    CompanyName: string = '';
    Bal: string = '';
    Pmt: string = '';
}

export class RealPropertiesPageConsumerDefinition extends ConsumerDefinition {
    FirstName: string = '';
    LastName: string = '';
}