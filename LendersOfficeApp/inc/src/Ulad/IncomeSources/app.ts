import * as angular from 'angular';
import { ITimeoutService } from 'angular';

import { AllApplicationsFilter, LegacyApplicationsFilter, UladApplicationsFilter, BorrowerFilter, ApplicationLabelsByValue } from '../Common/ApplicationFilters';
import IncomeSourceDefinition from '../Common/IncomeSourceDefinition';
import IncomeSourceEmploymentRecordsDefinition from '../Common/IncomeSourceEmploymentRecordsDefinition';
import ConsumerDefinition from '../Common/ConsumerDefinition';
import ConsumerIncomeSourceDefinition from '../Common/ConsumerIncomeSourceDefinition';
import EditAssociationPopupHelper from '../Common/EditAssociationPopupHelper';
import EditOwnerPopupHelper from '../Common/EditOwnerPopupHelper';
import EmploymentRecordDefinition from '../Common/EmploymentRecordDefinition';
import LegacyApplicationConsumersDefinition from '../Common/LegacyApplicationConsumersDefinition';
import LegacyApplicationDefinition from '../Common/LegacyApplicationDefinition';
import LoanDefinition from '../Common/LoanDefinition';
import TotalsType from '../Common/TotalsType';
import UladApplicationDefinition from '../Common/UladApplicationDefinition';
import UladApplicationConsumersDefinition from '../Common/UladApplicationConsumersDefinition';
import UladAssociationHelper from '../Common/UladAssociationHelper';

import BooleanLqbDataServiceResponseConverter from '../../api/BooleanLqbDataServiceResponseConverter';
import Dictionary from '../../api/Dictionary';
import LqbDataServiceRequestFormatProvider from '../../api/LqbDataServiceRequestFormatProvider';
import LqbDataServiceRequestParameterFactory from '../../api/LqbDataServiceRequestParameterFactory';
import LqbDataServiceResponseHandler from '../../api/LqbDataServiceResponseHandler';
import LqbDataServiceOperationType from '../../api/LqbDataServiceOperationType';
import LqbDataServiceRequestType from '../../api/LqbDataServiceRequestType';

import getAppId from '../Common/Utils/getAppId';
import getAppName from '../Common/Utils/getAppName';
import getConsumerName from '../Common/Utils/getConsumerName';
import getEmploymentToDate from '../Common/Utils/getEmploymentToDate';
import getLoanId from '../Common/Utils/getLoanId';
import getOwnersForNewEntity from '../Common/Utils/getOwnersForNewEntity';
import getTotalArgs from '../Common/Utils/getTotalArgs';
import groupBy from '../../utils/groupBy';
import normalizeSelectedApplications from '../Common/Utils/normalizeSelectedApplications';
import sendWebRequest from '../Common/Utils/sendWebRequest';
import orderConsumerNames from '../Common/Utils/orderConsumerNames';

class IncomeSourcesPageEmploymentRecordDefinition extends EmploymentRecordDefinition {
    EmployerName: string = '';
    MonthlyIncome: string = '';
    EmploymentStartDate: string = '';
    EmploymentEndDate: string = '';
    IsCurrent: string = '';
    EmploymentStatusType: string = '';
}

class IncomeSourcesPageIncomeSourceDefinition extends IncomeSourceDefinition {
	IncomeType: string = '';
	Description: string = '';
	MonthlyAmountData: string = '';	
}

class IncomeSourcesPageConsumerDefinition extends ConsumerDefinition {
	FirstName: string = '';
	LastName: string = '';
}

declare function parseMoneyFloat(str: string): number;
declare function callWebMethodAsync(params: Dictionary<any>): void;
declare function getQueryStringVariable(variable: string): string;
declare function clearDirty(): void;
declare function updateDirtyBit(): void;
declare function isDirty(): boolean;
declare function _initInput(): void;
declare function PolyConfirmSave(confirmCallback: (shouldSave: boolean) => void, shouldShowCancel: boolean, cancelCallback?: () => void): void;

declare var LQBPopup: any;
declare var VRoot: string;
declare var IncomeSourceDescriptionsByValue: Dictionary<string>;

const $ = jQuery;
const parameterFactory = new LqbDataServiceRequestParameterFactory();
const uladAssociationHelper = new UladAssociationHelper();
const booleanValueConverter = new BooleanLqbDataServiceResponseConverter();

angular.module('IncomeSources', []).controller('IncomeSourcesController', ['$scope', '$timeout', function IncomeSourcesController($scope, $timeout: ITimeoutService) {	
	$scope.sortKey = null;
	$scope.sortDesc = false;

	$scope.incomeSourceFilterModel = AllApplicationsFilter;
	$scope.selectedApplicationIdModel = null;

	$scope.selectedApplications = new Array<string>();
	$scope.selectedRecordIndex = 0;

	$scope.selectedIncomeSource = null;
	$scope.originalSelectedIncomeSource = null;

	$scope.isDirty = isDirty;
	$scope.updateDirtyBit = updateDirtyBit;

	$scope.totalOpId = null;
	$scope.subtotalOpId = null;

	// format
	$scope.onFormatMoney = function (input: HTMLInputElement) {
		if (input.id === 'SelectedIncomeSourceMonthlyAmountData') {
			// Ensure the income source's monthly amount data is 
			// formatted in the listing table as expected, which 
			// happens after the initial model update.
			$scope.$apply(() => {
				$scope.selectedIncomeSource['MonthlyAmountData'] = input.value;
			});
		}
	}

	$scope.refreshCalculation = function () {
		// Allow other scripts to finish running before calling
		// for a data refresh, e.g. money formatting.
		$timeout(() => {
			$scope.saveData(null/*callback*/, true/*refreshOnly*/);
		});
	}

	$scope.revertIncomeSourceChanges = function (backupIncomeSource: Dictionary<any>) {
		if ($scope.vm.loan == null || $scope.vm.loan.IncomeSources == null) {
			return;
		}

		const backupId = backupIncomeSource['Id'];
		for (let i = 0; i < $scope.vm.loan.IncomeSources.length; ++i) {
			const incomeSource = $scope.vm.loan.IncomeSources[i];

			if (incomeSource['Id'] === backupId) {
				$scope.vm.loan.IncomeSources[i] = backupIncomeSource;
				return;
			}
		}
	}

	$scope.setSelectedIncomeSource = function (incomeSource: Dictionary<any>) {
		$scope.selectedIncomeSource = incomeSource;

		const backupIncomeSource: Dictionary<any> = Object.assign(new Dictionary<any>(), incomeSource);
		$scope.originalSelectedIncomeSource = backupIncomeSource;
	}

	$scope.checkForSave = function (postCallback: () => void, cancelCallback?: () => void, updateTotalsOnSave: boolean = true) {
		if (isDirty()) {
			PolyConfirmSave(
				(shouldSave: boolean) => {
					if (shouldSave) {
						$scope.saveData(postCallback, false/*refreshOnly*/, updateTotalsOnSave);
					}
					else {
						clearDirty();

						$timeout(() => {
							$scope.revertIncomeSourceChanges($scope.originalSelectedIncomeSource);
							$scope.setSelectedIncomeSource($scope.originalSelectedIncomeSource);
							$scope.refreshCalculation();

							postCallback();
						});
					}
				},
				true/*shouldShowCancel*/,
				() => {
					if (cancelCallback != null) {
						$timeout(cancelCallback);
					}
				});
		}
		else {
			postCallback();
		}
	}

	$scope.saveData = function (callback: () => void, refreshOnly: boolean = false, updateTotalsOnSave: boolean = true) {
		let provider = new LqbDataServiceRequestFormatProvider();
		let setters = new Array<Dictionary<any>>();

		if ($scope.selectedIncomeSource != null) {
			setters.push(parameterFactory.createIndividualSetParameter(
				IncomeSourcesPageIncomeSourceDefinition,
				$scope.selectedIncomeSource,
				$scope.selectedIncomeSource['Id']));
		}		

		const setArgs = parameterFactory.createAggregateSetParameter(setters);

		if (updateTotalsOnSave) {
			const getIncomeSourceTotalArgs = $scope.getIncomeSourceTotalArgs();
			const getIncomeSourceSubtotalArgs = $scope.getIncomeSourceSubtotalArgs();

			$scope.totalOpId = provider.addRequest(LqbDataServiceRequestType.GetIncomeSourceTotals, getIncomeSourceTotalArgs);			
			$scope.subtotalOpId = provider.addRequest(LqbDataServiceRequestType.GetIncomeSourceTotals, getIncomeSourceSubtotalArgs);
		}		

		provider.addRequest(LqbDataServiceRequestType.Set, setArgs);		

		sendWebRequest(
			refreshOnly ? LqbDataServiceOperationType.Load : LqbDataServiceOperationType.Save,
			provider.toRequest(),
			(data: Dictionary<any>) => {
				const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
				if (handler.hasError()) {
					alert(handler.getErrorMessage());
					return;
				}

				$scope.$apply(() => {
					if (updateTotalsOnSave) {
						$scope.vm.incomeSourceTotals = handler.getResponseByKey(LqbDataServiceRequestType.GetIncomeSourceTotals, 'loan', $scope.totalOpId);						
						$scope.vm.incomeSourceSubtotals = handler.getResponseByKey(LqbDataServiceRequestType.GetIncomeSourceTotals, 'loan', $scope.subtotalOpId);
					}
					
					if (!refreshOnly) {
						clearDirty();
						$scope.originalSelectedIncomeSource = $scope.selectedIncomeSource;
					}

					if (typeof callback === 'function') {
						callback();
					}
				});
			});
	}

	$scope.getConsumerName = (consumerId: string) => getConsumerName($scope.vm.loan.Consumers, consumerId);

	$scope.getAppName = (appIdConsumerIdPairs: Array<Dictionary<string>>) => getAppName(appIdConsumerIdPairs, $scope.getConsumerName);

	$scope.getFriendlyIncomeSourceTypeName = function (incomeSourceTypeValue: string): string {
		return IncomeSourceDescriptionsByValue[incomeSourceTypeValue] as string;
    }

    $scope.getEmploymentRecords = function (incomeSourceId: string): Array<Dictionary<string>> {
        const incomeSourceEmploymentAssociations = $scope.vm.loan.IncomeSourceEmploymentRecords
            .filter((association: Dictionary<string>) => association['IncomeSourceId'] == incomeSourceId);

        return $scope.vm.loan.EmploymentRecords.filter((employmentRecord: Dictionary<string>) => {
            const employmentRecordId = employmentRecord['Id'];
            const matchingAssociation = incomeSourceEmploymentAssociations.find((association: Dictionary<string>) => association['EmploymentRecordId'] === employmentRecordId);
            return matchingAssociation != null;
        });
    }

    $scope.getEmploymentToDate = function (employmentRecord: Dictionary<string | boolean>) : string {
        return getEmploymentToDate(
            employmentRecord['EmploymentEndDate'] as string,
            employmentRecord['IsCurrent'] as boolean,
            employmentRecord['EmploymentStatusType'] as string)
    }

	$scope.updateIncomeSourceSubtotalsForCurrentApplication = function () {
		const getIncomeSourceSubtotalArgs = $scope.getIncomeSourceSubtotalArgs();
		$scope.updateIncomeSourceSubtotals(getIncomeSourceSubtotalArgs);
	}

	$scope.updateIncomeSourceSubtotals = function (getIncomeSourceSubtotalArgs: Dictionary<string>) {
		let provider = new LqbDataServiceRequestFormatProvider();
		provider.addRequest(LqbDataServiceRequestType.GetIncomeSourceTotals, getIncomeSourceSubtotalArgs);

		sendWebRequest(
			LqbDataServiceOperationType.Load,
			provider.toRequest(),
			(data: Dictionary<any>) => {
				const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
				if (handler.hasError()) {
					alert(handler.getErrorMessage());
					return;
				}

				$scope.$apply(() => {
					$scope.vm.incomeSourceSubtotals = handler.getResponseByKey(LqbDataServiceRequestType.GetIncomeSourceTotals, 'loan');
				});
			});
	}

	$scope.applyResult = function (data: Dictionary<any>, callback?: any) {
		$scope.$apply(function () {
			const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
			if (handler.hasError()) {
				alert(handler.getErrorMessage());
				return;
			}

			const getResponse = handler.getResponseByKey(LqbDataServiceRequestType.Get, 'loan');
			$scope.vm.loan = Object.assign($scope.vm.loan, getResponse);
			orderConsumerNames($scope.vm.loan.IncomeSources, $scope.getConsumerName);

			const incomeSourceTotalsResponse = handler.getResponseByKey(LqbDataServiceRequestType.GetIncomeSourceTotals, 'loan', $scope.totalOpId);
			$scope.vm.incomeSourceTotals = incomeSourceTotalsResponse;

			const incomeSourceSubtotalsResponse = handler.getResponseByKey(LqbDataServiceRequestType.GetIncomeSourceTotals, 'loan', $scope.subtotalOpId);
			$scope.vm.incomeSourceSubtotals = incomeSourceSubtotalsResponse;

			if (callback != null) {
				callback();
			}
		});
	}

	$scope.loadData = function (request: string, callback?: any) {
		sendWebRequest(
			LqbDataServiceOperationType.Load,
			request,
			(data: Dictionary<any>) => { $scope.applyResult(data, callback); });
	}

	// filter
	$scope.getIncomeSourceTotalArgs = function () {
		return getTotalArgs(parameterFactory, AllApplicationsFilter, "" /* selected application id model doesn't get used when we're getting the total */);
	}

	$scope.getIncomeSourceSubtotalArgs = function () {
		return getTotalArgs(parameterFactory, $scope.incomeSourceFilterModel, $scope.selectedApplicationIdModel);
	}

	$scope.displayAllIncomeSources = function () {
		$scope.init();
	}

	$scope.displayLegacyAppIncomeSources = function () {
		$scope.selectedApplications = normalizeSelectedApplications(
			$scope.vm.loan.LegacyApplicationConsumers,
			'LegacyApplicationId',
			'ConsumerId',
			(app: Dictionary<boolean>) => app['IsPrimary']);

		$scope.selectedApplicationIdModel = $scope.selectedApplications[0]['key'];

		$scope.selectedRecordIndex = -1;
		$scope.moveToNextRecord();

		const getIncomeSourceSubtotalArgs = parameterFactory.createGetTotalsParameter(
			LoanDefinition,
			TotalsType.LegacyApplication,
			$scope.selectedApplicationIdModel);

		$scope.updateIncomeSourceSubtotals(getIncomeSourceSubtotalArgs);
	}

	$scope.displayUladAppIncomeSources = function () {
		$scope.selectedApplications = normalizeSelectedApplications(
			$scope.vm.loan.UladApplicationConsumers,
			'UladApplicationId',
			'ConsumerId',
			(app: Dictionary<boolean>) => app['IsPrimary']);

		$scope.selectedApplicationIdModel = $scope.selectedApplications[0]['key'];

		$scope.selectedRecordIndex = -1;
		$scope.moveToNextRecord();

		const getIncomeSourceSubtotalArgs = parameterFactory.createGetTotalsParameter(
			LoanDefinition,
			TotalsType.UladApplication,
			$scope.selectedApplicationIdModel);

		$scope.updateIncomeSourceSubtotals(getIncomeSourceSubtotalArgs);
	}

	$scope.displayBorrowerIncomeSources = function () {
		$scope.selectedApplications = normalizeSelectedApplications(
			$scope.vm.loan.Consumers,
			'Id',
			'Id',
			(app: Dictionary<string>) => true);

		$scope.selectedApplicationIdModel = $scope.selectedApplications[0]['key'];

		$scope.selectedRecordIndex = -1;
		$scope.moveToNextRecord();

		const getIncomeSourceSubtotalArgs = parameterFactory.createGetTotalsParameter(
			LoanDefinition,
			TotalsType.Consumer,
			$scope.selectedApplicationIdModel);

		$scope.updateIncomeSourceSubtotals(getIncomeSourceSubtotalArgs);
	}

	$scope.onIncomeSourceFilterChange = function (oldIncomeSourceFilterModel: string) {
		$scope.checkForSave(
			() => {
				$scope.selectedApplicationIdModel = null;
				$scope.selectedApplications = null;
				$scope.selectedIncomeSource = null;

				switch ($scope.incomeSourceFilterModel) {
					case AllApplicationsFilter:
						$scope.displayAllIncomeSources();
						break;
					case LegacyApplicationsFilter:
						$scope.displayLegacyAppIncomeSources();
						break;
					case UladApplicationsFilter:
						$scope.displayUladAppIncomeSources();
						break;
					case BorrowerFilter:
						$scope.displayBorrowerIncomeSources();
						break;
					default:
						throw `Unhandled income source filter value ${$scope.incomeSourceFilterModel}.`;
				}
			},
			() => {
				$scope.incomeSourceFilterModel = oldIncomeSourceFilterModel;
			},
			false/*updateTotalsOnSave*/);
	}

	$scope.onApplicationFilterChange = function (newSelectedApplicationIdModel: string) {
		const oldSelectedApplicationIdModel: string = $scope.selectedApplicationIdModel;

		$scope.checkForSave(
			() => {
				$scope.selectedApplicationIdModel = newSelectedApplicationIdModel;

				$scope.selectedRecordIndex = -1;
				$scope.selectedIncomeSource = null;

				$scope.moveToNextRecord();
				$scope.updateIncomeSourceSubtotalsForCurrentApplication();
			},
			() => {
				$('#ApplicationFilter').val(oldSelectedApplicationIdModel);
				$scope.selectedApplicationIdModel = oldSelectedApplicationIdModel;
			},
		false/*updateTotalsOnSave*/);
	}

	$scope.incomeSourceMatchesFilter = function (incomeSource: Dictionary<any>) {
		if ($scope.vm.loan.ConsumerIncomeSources == null) {
			return true;
		}

		if ($scope.selectedApplications == null || $scope.incomeSourceFilterModel === AllApplicationsFilter) {
			return true;
		}

		const applicableApps = $scope.selectedApplications.filter((app: Dictionary<any>) => app['key'] === $scope.selectedApplicationIdModel)
			.map((app: Dictionary<any>) => app['values'])
			.reduce((a: Array<Dictionary<any>>, b: Array<Dictionary<any>>) => a.concat(b), new Array<Dictionary<any>>());

		if (applicableApps.length === 0) {
			return false;
		}

		const matchingOwner = incomeSource['Owners'].find(function (incomeSourceOwner: Dictionary<string>) {
			// Ignore IsPrimary when searching at the borrower level.
			if ($scope.incomeSourceFilterModel !== BorrowerFilter && !incomeSourceOwner['IsPrimary']) {
				return false;
			}

			return applicableApps.find((app: Dictionary<string>) => app['ConsumerId'] === incomeSourceOwner['ConsumerId']) != null;
		});

		return matchingOwner != null;
	}

	$scope.init = function () {
		$scope.vm = new Dictionary<Dictionary<any>>();
		$scope.vm.loan = new Dictionary<any>();

		const provider = $scope.createInitProvider();
		$scope.loadData(provider.toRequest(), function () {
			$scope.moveToPreviousRecord();
			window.setTimeout(_initInput, 0);
		});
	}

	$scope.createInitProvider = function (): LqbDataServiceRequestFormatProvider {
		let provider = new LqbDataServiceRequestFormatProvider();

        const getArgs = parameterFactory.createIndividualGetParameter([
            ConsumerIncomeSourceDefinition,
            IncomeSourceEmploymentRecordsDefinition,
            IncomeSourcesPageEmploymentRecordDefinition,
			IncomeSourcesPageIncomeSourceDefinition,
			IncomeSourcesPageConsumerDefinition,
			LegacyApplicationDefinition,
			LoanDefinition,
			LegacyApplicationConsumersDefinition,
			UladApplicationDefinition,
			UladApplicationConsumersDefinition]);

		const getIncomeSourceTotalArgs = parameterFactory.createGetTotalsParameter(LoanDefinition, TotalsType.Loan);
		const getIncomeSourceSubtotalArgs = $scope.getIncomeSourceSubtotalArgs();

		provider.addRequest(LqbDataServiceRequestType.Get, getArgs);
		$scope.totalOpId = provider.addRequest(LqbDataServiceRequestType.GetIncomeSourceTotals, getIncomeSourceTotalArgs);
		$scope.subtotalOpId = provider.addRequest(LqbDataServiceRequestType.GetIncomeSourceTotals, getIncomeSourceSubtotalArgs);
		return provider;
	}

	// record related functions
	$scope.moveToPreviousRecord = function () {
        if ($scope.vm.loan.IncomeSources == null || $scope.vm.loan.IncomeSources.length === 0) {
            return;
        }

        $scope.checkForSave(() => {
            const startIndex = Math.max($scope.selectedRecordIndex - 1, 0);
            let nextIndex = -1;

            for (let i = startIndex; i >= 0; --i) {
                const incomeSourceAtIndex = $scope.vm.loan.IncomeSources[i];
                if ($scope.incomeSourceMatchesFilter(incomeSourceAtIndex)) {
                    nextIndex = i;
                    break;
                }
            }

            if (nextIndex >= 0) {
                $scope.selectedRecordIndex = nextIndex;
                $scope.setSelectedIncomeSource($scope.vm.loan.IncomeSources[$scope.selectedRecordIndex]);
            }
            else if ($scope.selectedRecordIndex === 0) {
                $scope.moveToNextRecord();
            }
        });
	}

	$scope.moveToNextRecord = function () {
		if ($scope.vm.loan.IncomeSources == null || $scope.vm.loan.IncomeSources.length === 0) {
			return;
		}

		$scope.checkForSave(() => {
			const startIndex = Math.min($scope.selectedRecordIndex + 1, $scope.vm.loan.IncomeSources.length - 1);
			let nextIndex = -1;

			for (let i = startIndex; i < $scope.vm.loan.IncomeSources.length; ++i) {
				const incomeSourceAtIndex = $scope.vm.loan.IncomeSources[i];
				if ($scope.incomeSourceMatchesFilter(incomeSourceAtIndex)) {
					nextIndex = i;
					break;
				}
			}

			if (nextIndex >= 0) {
				$scope.selectedRecordIndex = nextIndex;
				$scope.setSelectedIncomeSource($scope.vm.loan.IncomeSources[$scope.selectedRecordIndex]);
			}
		});
	}

	$scope.selectIncomeSource = function (incomeSource: Dictionary<string>) {
		$scope.checkForSave(() => {
			const incomeSourceId = incomeSource['Id'];

			for (let i = 0; i < $scope.vm.loan.IncomeSources.length; ++i) {
				const incomeSourceAtIndex = $scope.vm.loan.IncomeSources[i];

				if (incomeSourceId === incomeSourceAtIndex['Id']) {
					$scope.selectedRecordIndex = i;
					$scope.setSelectedIncomeSource($scope.vm.loan.IncomeSources[$scope.selectedRecordIndex]);
					break;
				}
			}
		});
	}

	$scope.insertRecord = function () {
		$scope.checkForSave(() => {
			const insertIndex = Math.max(0, $scope.selectedRecordIndex);
			$scope.addIncomeSource(insertIndex);
		});
	}

	$scope.addRecord = function () {
		$scope.checkForSave(() => {
			$scope.addIncomeSource();
		});
	}

	$scope.addIncomeSource = function (insertIndex?: number) {
		const hadActiveIncomeSource = $scope.vm.loan.IncomeSources.filter($scope.incomeSourceMatchesFilter).length !== 0;

		const owners = getOwnersForNewEntity(
			$scope.selectedApplications,
			$scope.selectedApplicationIdModel,
			$scope.vm.loan.UladApplications,
			$scope.vm.loan.UladApplicationConsumers);

		const primaryOwner = owners.find((owner: Dictionary<string | boolean>) => owner['IsPrimary'] as boolean);
		if (primaryOwner == null) {
			throw 'Unable to add primary ownership for new incomeSource.';
		}

		const additionalOwners = owners.filter((owner: Dictionary<string | boolean>) => owner['ConsumerId'] !== primaryOwner['ConsumerId']);
		const additionalOwnerIds = additionalOwners.map((owner: Dictionary<string | boolean>) => owner['ConsumerId'] as string);

		const primaryConsumerId = primaryOwner['ConsumerId'] as string;
		const args = parameterFactory.createAddParameter(IncomeSourcesPageIncomeSourceDefinition, primaryConsumerId, additionalOwnerIds, insertIndex);

		let provider = new LqbDataServiceRequestFormatProvider();
		provider.addRequest(LqbDataServiceRequestType.Add, args);

		sendWebRequest(
			LqbDataServiceOperationType.Save,
			provider.toRequest(),
			(data: Dictionary<any>) => {
				const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
				if (handler.hasError()) {
					alert(handler.getErrorMessage());
					return;
				}

				$scope.$apply(function () {
					const response = handler.getResponseByKey(LqbDataServiceRequestType.Add, 'loan');

					var incomeSource = new Dictionary<any>();
					incomeSource['Id'] = response['id'];
					incomeSource['Owners'] = [primaryOwner, ...additionalOwners];

					if (insertIndex == null) {
						const incomeSourcesLength = $scope.vm.loan.IncomeSources.push(incomeSource);
						$scope.selectedRecordIndex = incomeSourcesLength - 1;
					}
					else {
						$scope.vm.loan.IncomeSources.splice(insertIndex, 0/*No entries before: only adding the incomeSource*/, incomeSource);
						$scope.selectedRecordIndex = insertIndex;
					}

					orderConsumerNames([incomeSource], $scope.getConsumerName);
					$scope.setSelectedIncomeSource(incomeSource);

					if (!hadActiveIncomeSource) {
						// No inputs are in the DOM until there is an active income source.
						// Once we've added an income source, initialize the new inputs.
						window.setTimeout(_initInput, 0);
					}
				});
			});
	}



	$scope.deleteRecord = function () {
		const recordId = $scope.selectedIncomeSource['Id'];

		const deleteArgs = parameterFactory.createRemoveParameter(IncomeSourcesPageIncomeSourceDefinition, recordId);
		const getIncomeSourceTotalArgs = $scope.getIncomeSourceTotalArgs();
		const getIncomeSourceSubtotalArgs = $scope.getIncomeSourceSubtotalArgs();

		let provider = new LqbDataServiceRequestFormatProvider();
		provider.addRequest(LqbDataServiceRequestType.Remove, deleteArgs);
		$scope.totalOpId = provider.addRequest(LqbDataServiceRequestType.GetIncomeSourceTotals, getIncomeSourceTotalArgs);
		$scope.subtotalOpId = provider.addRequest(LqbDataServiceRequestType.GetIncomeSourceTotals, getIncomeSourceSubtotalArgs);

		sendWebRequest(
			LqbDataServiceOperationType.Save,
			provider.toRequest(),
			(data: Dictionary<any>) => {
				const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
				if (handler.hasError()) {
					alert(handler.getErrorMessage());
					return;
				}

				$scope.$apply(function () {
					$scope.vm.incomeSourceTotals = handler.getResponseByKey(LqbDataServiceRequestType.GetIncomeSourceTotals, 'loan', $scope.totalOpId);
					$scope.vm.incomeSourceSubtotals = handler.getResponseByKey(LqbDataServiceRequestType.GetIncomeSourceTotals, 'loan', $scope.subtotalOpId);

					if ($scope.vm.loan.IncomeSources.length === 1) {
						$scope.vm.loan.IncomeSources = new Array<Dictionary<string>>();
						$scope.selectedRecordIndex = 0;
						$scope.selectedIncomeSource = null;
					}
					else {
						$scope.vm.loan.IncomeSources.splice($scope.selectedRecordIndex, 1);
						$scope.moveToPreviousRecord();
					}
				});
			});
	}

	$scope.moveRecordUp = function () {
		if ($scope.selectedRecordIndex === 0) {
			return;
		}

		$scope.checkForSave(() => {
			const incomeSources = $scope.vm.loan.IncomeSources;
			const temp = incomeSources[$scope.selectedRecordIndex];
			incomeSources[$scope.selectedRecordIndex] = incomeSources[$scope.selectedRecordIndex - 1];
			incomeSources[$scope.selectedRecordIndex - 1] = temp;

			$scope.setRecordOrder(incomeSources, () => { $scope.selectedRecordIndex-- });
		});
	}

	$scope.moveRecordDown = function () {
		if ($scope.selectedRecordIndex === $scope.vm.loan.IncomeSources.length - 1) {
			return;
		}

		$scope.checkForSave(() => {
			const incomeSources = $scope.vm.loan.IncomeSources;
			const temp = incomeSources[$scope.selectedRecordIndex];
			incomeSources[$scope.selectedRecordIndex] = incomeSources[$scope.selectedRecordIndex + 1];
			incomeSources[$scope.selectedRecordIndex + 1] = temp;

			$scope.setRecordOrder(incomeSources, () => { $scope.selectedRecordIndex++ });
		});
	}

	$scope.setRecordOrder = function (incomeSources: Array<Dictionary<string>>, callback?: () => void) {
		const incomeSourceIds = incomeSources.map((incomeSource: Dictionary<string>) => incomeSource['Id']);
		const args = parameterFactory.createSetOrderParameter(IncomeSourcesPageIncomeSourceDefinition, incomeSourceIds);

		let provider = new LqbDataServiceRequestFormatProvider();
		provider.addRequest(LqbDataServiceRequestType.SetOrder, args);

		sendWebRequest(
			LqbDataServiceOperationType.Save,
			provider.toRequest(),
			(data: Dictionary<any>) => {
				const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
				if (handler.hasError()) {
					alert(handler.getErrorMessage());
					return;
				}

				$scope.$apply(() => {
					$scope.vm.loan.IncomeSources = incomeSources;

					if (typeof callback === 'function') {
						callback();
					}
				});
			});
	}

	// popups
    $scope.editEmploymentRecords = function (incomeSourceId: string) {
        $scope.checkForSave(() => {
            const selectedEmploymentRecords = $scope.getEmploymentRecords(incomeSourceId);

            const helper = new EditAssociationPopupHelper();
            helper.storeExistingAssociationIdsToCache(selectedEmploymentRecords, (result: Dictionary<any>) => {
                if (result.error) {
                    alert(result.UserMessage);
                    return;
                }

                const cacheKey = result.value['CacheKey'];
                const url = `${VRoot}/newlos/Ulad/EditAssociations.aspx?loanid=${getLoanId()}&appid=${getAppId()}&id=${incomeSourceId}&t=IncomeSource&s=${cacheKey}`;
                const settings = {
                    onReturn: function (returnArgs: Dictionary<any>) { $scope.onEditEmploymentRecordsClose(incomeSourceId, selectedEmploymentRecords, returnArgs); }
                };
                LQBPopup.Show(url, settings);
            });
        });
    }

    $scope.onEditEmploymentRecordsClose = function (incomeSourceId: string, existingEmploymentRecords: Array<Dictionary<string>>, returnArgs: Dictionary<Array<Dictionary<string>>>) {
        const associationsByEmploymentRecordId: Dictionary<string> = $scope.vm.loan.IncomeSourceEmploymentRecords
            .filter((association: Dictionary<string>) => association['IncomeSourceId'] === incomeSourceId)
            .reduce((map: Dictionary<any>, association: Dictionary<string>) => {
                map[association['EmploymentRecordId']] = association['Id'];
                return map;
            }, {});

        const selectedEmploymentRecords = returnArgs['selectedAssociations'];

        const removedRecords = existingEmploymentRecords.filter(existingRecord => selectedEmploymentRecords.find(selectedRecord => selectedRecord['Id'] === existingRecord['Id']) == null);
        const addedRecords = selectedEmploymentRecords.filter(selectedRecord => existingEmploymentRecords.find(existingRecord => existingRecord['Id'] === selectedRecord['Id']) == null);

        const provider = new LqbDataServiceRequestFormatProvider();

        for (const removedRecord of removedRecords) {
            const associationId = associationsByEmploymentRecordId[removedRecord['Id']];
            const parameter = parameterFactory.createRemoveAssociationParameter(IncomeSourceEmploymentRecordsDefinition, associationId);
            provider.addRequest(LqbDataServiceRequestType.RemoveAssociation, parameter);
        }

        for (const addedRecord of addedRecords) {
            const parameter = parameterFactory.createAddAssociationParameter(IncomeSourceEmploymentRecordsDefinition, incomeSourceId, addedRecord['Id']);
            provider.addRequest(LqbDataServiceRequestType.AddAssociation, parameter);
        }

        sendWebRequest(
            LqbDataServiceOperationType.Save,
            provider.toRequest(),
            (data: Dictionary<any>) => {
                const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
                if (handler.hasError()) {
                    alert(handler.getErrorMessage());
                    return;
                }

                LQBPopup.Hide();

                const provider = $scope.createInitProvider();
                $scope.loadData(provider.toRequest())
            });
    }

	$scope.editIncomeSourceOwners = function (incomeSource: Dictionary<Array<Dictionary<string>>>) {
		$scope.checkForSave(() => {
			const selectedOwners = incomeSource['Owners'];

			var helper = new EditOwnerPopupHelper();
			helper.storeEntityOwnerIdsToCache(selectedOwners, (result: Dictionary<any>) => {
				if (result.error) {
					alert(result.UserMessage);
					return;
				}

				const cacheKey = result.value['CacheKey'];
				const url = `${VRoot}/newlos/Ulad/EditOwner.aspx?loanid=${getLoanId()}&appid=${getAppId()}&id=${incomeSource['Id']}&t=IncomeSource&s=${cacheKey}`;
				const settings = {
					onReturn: $scope.onOwnerPopupClose
				};
				LQBPopup.Show(url, settings);
			});
		});
	}

	$scope.onOwnerPopupClose = function (returnArgs: Dictionary<any>) {
		const primaryOwner: Dictionary<string> = returnArgs['PrimaryOwner'];
		const additionalOwners: Array<Dictionary<string>> = returnArgs['AdditionalOwners'];

		const getIncomeSourceTotalArgs = $scope.getIncomeSourceTotalArgs();
		const getIncomeSourceSubtotalArgs = $scope.getIncomeSourceSubtotalArgs();
		const setOwnerArgs = parameterFactory.createSetOwnerParameter(
			ConsumerIncomeSourceDefinition,
			$scope.selectedIncomeSource['Id'],
			primaryOwner['Id'],
			additionalOwners.map((owner: Dictionary<string>) => owner['Id']));

		let provider = new LqbDataServiceRequestFormatProvider();
		provider.addRequest(LqbDataServiceRequestType.SetOwner, setOwnerArgs);
		$scope.totalOpId = provider.addRequest(LqbDataServiceRequestType.GetIncomeSourceTotals, getIncomeSourceTotalArgs);
		$scope.subtotalOpId = provider.addRequest(LqbDataServiceRequestType.GetIncomeSourceTotals, getIncomeSourceSubtotalArgs);

		sendWebRequest(
			LqbDataServiceOperationType.Save,
			provider.toRequest(),
			(data: Dictionary<any>) => {
				const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
				if (handler.hasError()) {
					alert(handler.getErrorMessage());
					return;
				}

				LQBPopup.Hide();

				const provider = $scope.createInitProvider();
				$scope.loadData(provider.toRequest(), () => {
					$scope.vm.incomeSourceTotals = handler.getResponseByKey(LqbDataServiceRequestType.GetIncomeSourceTotals, 'loan', $scope.totalOpId);
					$scope.vm.incomeSourceSubtotals = handler.getResponseByKey(LqbDataServiceRequestType.GetIncomeSourceTotals, 'loan', $scope.subtotalOpId);

					// Ensure that the income source editor is hidden when the income source
					// no longer matches the application filter. For the "All" view, this logic is ignored.
					if ($scope.incomeSourceFilterModel === AllApplicationsFilter) {
						$scope.setSelectedIncomeSource($scope.vm.loan.IncomeSources[$scope.selectedRecordIndex]);
						return;
					}

					const selectedApplication: Dictionary<Array<Dictionary<string>>> = $scope.selectedApplications.find((app: Dictionary<string>) => {
						return app['key'] === $scope.selectedApplicationIdModel;
					});

					const updatedSelectedIncomeSource = $scope.vm.loan.IncomeSources.find((incomeSource: Dictionary<string>) => incomeSource['Id'] === $scope.selectedIncomeSource['Id']);
					const selectedIncomeSourceNewOwners: Array<Dictionary<string>> = updatedSelectedIncomeSource['Owners'];
					const selectedAppOwners: Array<Dictionary<string>> = selectedApplication['values'];

					const selectedIncomeSourceOwnerInCurrentApp = selectedIncomeSourceNewOwners.find((incomeSourceOwner: Dictionary<string>) => {
						return selectedAppOwners.find((appOwner: Dictionary<string>) => appOwner['ConsumerId'] === incomeSourceOwner['ConsumerId']) != null;
					});

					if (selectedIncomeSourceOwnerInCurrentApp != null) {
						$scope.setSelectedIncomeSource($scope.vm.loan.IncomeSources[$scope.selectedRecordIndex]);
					}
					else {
						$scope.selectedRecordIndex = -1;
						$scope.selectedIncomeSource = null;
						$scope.moveToNextRecord();
					}
				});
			});
	}

	$scope.init();
}]);