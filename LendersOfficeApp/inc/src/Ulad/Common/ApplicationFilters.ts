export const AllApplicationsFilter: string = '0';
export const LegacyApplicationsFilter: string = '1';
export const UladApplicationsFilter: string = '2';
export const BorrowerFilter: string = '3';

export const ApplicationLabelsByValue = {
	'0': '',
	'1': 'Legacy Application',
	'2': 'ULAD Application',
	'3': 'Borrower'
};