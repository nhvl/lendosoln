import EntityDefinition from '../../api/EntityDefinition';

export default class IncomeSourceDefinition extends EntityDefinition {
	classType: string = 'loan';
	collectionName: string = 'IncomeSources';
}