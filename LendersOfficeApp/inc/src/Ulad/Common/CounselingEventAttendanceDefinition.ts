import EntityDefinition from '../../api/EntityDefinition';

export default class CounselingEventAttendanceDefinition extends EntityDefinition {
	classType: string = 'loan';
    collectionName: string = 'CounselingEventAttendances';
	
    CounselingEventId: string = '';
	ConsumerId: string = '';
}