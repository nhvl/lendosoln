import Dictionary from '../../api/Dictionary';

declare var VRoot: string;
declare var gService: {
    utils: {
        callAsyncSimple: (methodName: string, args: Dictionary<string>, successCallback: (parameter: any) => void, errorCallback: () => void) => void,
        callAsyncBypassOverlay: (methodName: string, args: Dictionary<string>, successCallback: (parameter: any) => void, errorCallback: () => void) => void,
    }
};

export default class EditAssociationPopupHelper {
    public storeExistingAssociationIdsToCache(idList: Array<Dictionary<string>>, callback: (result: Dictionary<any>) => void) {
        const data = {
            IdList: JSON.stringify(idList)
        };

        this.callWebMethod('StoreIdListToCache', data, callback);
    }

    public retrieveExistingAssociationIdsFromCache(cacheKey: string, callback: (result: Dictionary<any>) => void) {
        const data = {
            CacheKey: cacheKey
        };

        this.callWebMethod('RetrieveIdListFromCache', data, callback);
    }

    private callWebMethod(method: string, data: Dictionary<string>, callback: (parameter: any) => void) {
        const errorCallback = () => { alert('System error. Please contact us if this happens again.'); };
        gService.utils.callAsyncBypassOverlay(method, data, callback, errorCallback);
    }
}