import EntityDefinition from '../../api/EntityDefinition';

export default class AssetDefinition extends EntityDefinition {
	classType: string = 'loan';
    collectionName: string = 'Assets';

    public isReadonly(field: string) {
        return field == 'IsCreditAtClosing' || field == 'IsCreditAtClosingUserDependent' || field == 'GiftSource' || field == 'RequiresVod';
    }
}