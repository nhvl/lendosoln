import EntityDefinition from '../../api/EntityDefinition';

export default class ConsumerAssetDefinition extends EntityDefinition {
	classType: string = 'loan';
	collectionName: string = 'ConsumerAssets';
	
	AssetId: string = '';
	ConsumerId: string = '';
	AppId: string = '';
	IsPrimary: string = '';
}