import EntityDefinition from '../../api/EntityDefinition';

export default class IncomeSourceEmploymentRecordsDefinition extends EntityDefinition {
    classType: string = 'loan';
    collectionName: string = 'IncomeSourceEmploymentRecords';

    IncomeSourceId: string = '';
    EmploymentRecordId: string = '';
}