enum TotalsType
{
	Loan = 'Loan',
	UladApplication = 'UladApplication',
	LegacyApplication = 'LegacyApplication',
	Consumer = 'Consumer'
}

export default TotalsType;
