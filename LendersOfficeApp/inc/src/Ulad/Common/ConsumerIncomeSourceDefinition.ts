import EntityDefinition from '../../api/EntityDefinition';

export default class ConsumerIncomeSourceDefinition extends EntityDefinition {
	classType: string = 'loan';
	collectionName: string = 'ConsumerIncomeSources';

	IncomeSourceId: string = '';
	ConsumerId: string = '';
	AppId: string = '';
	IsPrimary: string = '';
}