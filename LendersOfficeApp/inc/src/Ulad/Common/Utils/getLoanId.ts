declare function getQueryStringVariable(variable: string): string;

export default function getLoanId(): string {
    return getQueryStringVariable('loanid');
}