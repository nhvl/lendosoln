declare function getQueryStringVariable(variable: string): string;

export default function getAppId(): string {
    return getQueryStringVariable('appid');
}