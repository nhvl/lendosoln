import { unnamedBorrowerPlaceholder } from './getConsumerName';

export default function orderConsumerNames(entities: Array<Dictionary<any>>, getConsumerName: (id: string) => string) {
    for (const entity of entities) {
        entity.Owners.sort((owner1: Dictionary<string>, owner2: Dictionary<string>) => {
            if (owner1['IsPrimary']) {
                return -1;
            }

            if (owner2['IsPrimary']) {
                return 1;
            }

            const owner1Name: string = getConsumerName(owner1['ConsumerId']);
            if (owner1Name == unnamedBorrowerPlaceholder) {
                return 1;
            }

            const owner2Name: string = getConsumerName(owner2['ConsumerId']);
            if (owner2Name == unnamedBorrowerPlaceholder) {
                return -1;
            }

            return owner1Name.localeCompare(owner2Name);
        });
    }
}
