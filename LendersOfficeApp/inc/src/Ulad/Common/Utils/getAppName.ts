export default function getAppName(appIdConsumerIdPairs: Array<Dictionary<string>>, consumerNameGetter: (consumerId: string) => string) {
	if (appIdConsumerIdPairs.length > 2) {
		const firstConsumer = appIdConsumerIdPairs[0];
		const firstConsumerName = consumerNameGetter(firstConsumer['ConsumerId']);
		return `${firstConsumerName} & ${appIdConsumerIdPairs.length - 1} others`;
	}

	return appIdConsumerIdPairs
		.map((appIdConsumerIdPair : Dictionary<string>) => consumerNameGetter(appIdConsumerIdPair['ConsumerId']))
		.join(' & ');
}