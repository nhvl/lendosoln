import Dictionary from "../../../api/Dictionary";
import LqbDataServiceOperationType from "../../../api/LqbDataServiceOperationType";
import getLoanId from './getLoanId';

declare function callWebMethodAsync(params: Dictionary<any>): void;
declare var VRoot: string;

export default function sendWebRequest(operationType: LqbDataServiceOperationType, request: string, onSuccess: (data: Dictionary<any>) => void, onError?: () => void) {
    const loanId = getLoanId();

    let displayOverlay = true;
    if (operationType === LqbDataServiceOperationType.LoadNoOverlay) {
        operationType = LqbDataServiceOperationType.Load;
        displayOverlay = false;
    }

    const url = `${VRoot}/lqbdataservice.aspx?op=${operationType}&loanid=${loanId}`;
    sendWebRequestByUrl(url, request, displayOverlay, onSuccess, onError);
}

export function sendWebRequestByUrl(url: string, request: string, displayOverlay: boolean, onSuccess: (data: Dictionary<any>) => void, onError?: () => void) {
    if (onError == null) {
        onError = () => { alert('System error. Please contact us if this happens again.'); };
    }

    let params = new Dictionary<any>();
    params['type'] = 'POST';
    params['url'] = url;
    params['dataType'] = 'json';
    params['data'] = request;
    params['success'] = onSuccess;
    params['error'] = onError;
    params['async'] = true;
    params['displayOverlay'] = displayOverlay;

    callWebMethodAsync(params);
}
