// 1. In all assets view, treat the addition like we are viewing the primary ULAD Application (see #2)
// 2. In an application view, assume all borrowers on the app, with the primary borrower as the primary owner
// 3. In a borrower view, assume the selected borrower
// Returns an array of consumers with datapoints IsPrimary (boolean) and ConsumerId (string).
export default function getOwnersForNewEntity(
	selectedApplications: Array<Dictionary<string|Dictionary<any>>>, 
	selectedApplicationIdModel: string, 
	uladApplications: Array<Dictionary<boolean|string>>,
	uladApplicationConsumers: Array<Dictionary<string>>) : Array<Dictionary<string|boolean>> {

	if (selectedApplications == null || selectedApplications.length === 0) {
        const primaryUladApplication = uladApplications.find(app => app['IsPrimary'] as boolean);
        if (primaryUladApplication == null) {
            throw 'Unable to locate primary ULAD application.';
        }

        return uladApplicationConsumers.filter(association => association['UladApplicationId'] === primaryUladApplication['Id']);
	}
	else {
        const selectedApp = selectedApplications.find(app => app['key'] === selectedApplicationIdModel);
        if (selectedApp == null) {
            throw 'Unable to locate selected application with ID ' + selectedApplicationIdModel;
        }

        return selectedApp['values'] as Array<Dictionary<string|boolean>>;
	}
}