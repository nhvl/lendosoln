export const unnamedBorrowerPlaceholder = '[unnamed borrower]';

export default function getConsumerName(consumers: Array<Dictionary<any>>, consumerId: string) {
	if (consumers == null || consumers.length === 0) {
		return '';
	}
	
	const matchingConsumer = consumers.find(consumer => consumer['Id'] === consumerId);
	if (matchingConsumer == null) {
		throw `Unable to locate consumer with id ${consumerId}`;
	}
	
    const consumerName = `${matchingConsumer['FirstName']} ${matchingConsumer['LastName']}`;
    if (consumerName.trim().length === 0) {
        return unnamedBorrowerPlaceholder;
    }

    return consumerName;
}