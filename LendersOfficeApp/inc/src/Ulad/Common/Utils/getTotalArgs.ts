import LqbDataServiceRequestParameterFactory from '../../../api/LqbDataServiceRequestParameterFactory';

import { AllApplicationsFilter, LegacyApplicationsFilter, UladApplicationsFilter, BorrowerFilter } from '../ApplicationFilters';
import LoanDefinition from '../LoanDefinition';
import TotalsType from '../TotalsType';

export default function getTotalArgs(parameterFactory: LqbDataServiceRequestParameterFactory, filterModel: string, selectedApplicationIdModel: string): Dictionary<string> {
	switch (filterModel) {
		case AllApplicationsFilter:
			return parameterFactory.createGetTotalsParameter(LoanDefinition, TotalsType.Loan);

		case LegacyApplicationsFilter:
			return parameterFactory.createGetTotalsParameter(LoanDefinition, TotalsType.LegacyApplication, selectedApplicationIdModel);

		case UladApplicationsFilter:
			return parameterFactory.createGetTotalsParameter(LoanDefinition, TotalsType.UladApplication, selectedApplicationIdModel);

		case BorrowerFilter:
			return parameterFactory.createGetTotalsParameter(LoanDefinition, TotalsType.Consumer, selectedApplicationIdModel);

		default:
			throw `Unhandled filter value ${filterModel}.`;
	}
}