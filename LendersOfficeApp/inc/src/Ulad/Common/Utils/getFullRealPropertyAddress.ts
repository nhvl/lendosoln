import Dictionary from '../../../api/Dictionary';

export default function getFullRealPropertyAddress(realProperty: Dictionary<string>): string {
	return `${realProperty['StreetAddress'] || ''}, ${realProperty['City'] || ''}, ${realProperty['State'] || ''} ${realProperty['Zip'] || ''}`;
}
