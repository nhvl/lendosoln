import Dictionary from '../../../api/Dictionary';
import groupBy from '../../../utils/groupBy';

export default function normalizeSelectedApplications(originalApplications: Array<Dictionary<any>>, appIdKey: string, consumerIdKey: string, isPrimaryGetter: (app: Dictionary<any>) => boolean) : Array<Dictionary<any>> {
	const normalizedApplications = originalApplications.map(function (app: Dictionary<any>) {
			let appDict = new Dictionary<any>();
			appDict['AppId'] = app[appIdKey];
			appDict['ConsumerId'] = app[consumerIdKey];
			appDict['IsPrimary'] = isPrimaryGetter(app);
			return appDict;
		});

	return groupBy<Dictionary<any>>(normalizedApplications, (app: Dictionary<any>) => app['AppId'] as string);
}