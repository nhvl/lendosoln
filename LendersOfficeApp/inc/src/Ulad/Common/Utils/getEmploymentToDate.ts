const currentEmploymentStatusType = '0';

export default function getEmploymentToDate(endDate: string, isCurrent: boolean, employmentStatusType: string) {
    if (isCurrent || employmentStatusType == currentEmploymentStatusType) {
        return 'Present';
    }

    return endDate;
}