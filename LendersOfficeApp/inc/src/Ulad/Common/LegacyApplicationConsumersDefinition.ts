import EntityDefinition from '../../api/EntityDefinition';

export default class LegacyApplicationConsumersDefinition extends EntityDefinition {
	classType: string = 'loan';
	collectionName: string = 'LegacyApplicationConsumers';
	
	LegacyApplicationId: string = '';
	ConsumerId: string = '';
	IsPrimary: string = '';
}