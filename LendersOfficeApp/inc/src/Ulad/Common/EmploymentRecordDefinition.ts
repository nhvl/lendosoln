import EntityDefinition from '../../api/EntityDefinition';

export default class EmploymentRecordDefinition extends EntityDefinition {
	classType: string = 'loan';
	collectionName: string = 'EmploymentRecords';
}