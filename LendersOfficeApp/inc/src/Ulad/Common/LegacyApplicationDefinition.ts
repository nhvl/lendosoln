import EntityDefinition from '../../api/EntityDefinition';

export default class LegacyApplicationDefinition extends EntityDefinition {
	classType: string = 'loan';
    collectionName: string = 'LegacyApplications'

    IsPrimary: string = '';
}