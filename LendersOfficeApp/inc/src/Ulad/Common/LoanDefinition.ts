import EntityDefinition from '../../api/EntityDefinition';

export default class LoanDefinition extends EntityDefinition {
	classType: string = 'loan';
}