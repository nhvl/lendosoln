import EntityDefinition from '../../api/EntityDefinition';

export default class CounselingEventDefinition extends EntityDefinition {
    classType: string = 'loan';
    collectionName: string = 'CounselingEvents';
}