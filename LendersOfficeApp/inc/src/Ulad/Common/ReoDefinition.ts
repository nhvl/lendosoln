import EntityDefinition from '../../api/EntityDefinition';

export default class ReoDefinition extends EntityDefinition {
	classType: string = 'loan';
	collectionName: string = 'RealProperties';
}