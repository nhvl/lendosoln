import EntityDefinition from "../../api/EntityDefinition";

export default class LiabilityDefinition extends EntityDefinition {
    classType: string = 'loan';
    collectionName: string = 'Liabilities';

    public isReadonly(field: string) {
        return field == 'RequiresVom' || field == 'RequiresVol';
    }
}