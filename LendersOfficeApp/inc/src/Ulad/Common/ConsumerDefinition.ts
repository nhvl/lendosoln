import EntityDefinition from '../../api/EntityDefinition';

export default class ConsumerDefinition extends EntityDefinition {
	classType: string = 'loan';
	collectionName: string = 'Consumers';
}