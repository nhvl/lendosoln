import EntityDefinition from "../../api/EntityDefinition";

export default class RealPropertyLiabilitiesDefinition extends EntityDefinition {
    classType: string = 'loan';
    collectionName: string = 'RealPropertyLiabilities';

    RealPropertyId: string = '';
    LiabilityId: string = '';
}