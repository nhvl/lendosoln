enum ApplicationType {
    LegacyApplication = 'LegacyApplication',
    UladApplication = 'UladApplication'
}

export default ApplicationType;