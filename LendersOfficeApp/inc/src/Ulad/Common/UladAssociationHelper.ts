import IAssociationHelper from '../../api/IAssociationHelper';

export default class UladAssociationHelper implements IAssociationHelper {
	public hasAssociation(collectionName: string) : boolean {
		switch (collectionName) {
			case 'Assets':
                return true;
            case 'Liabilities':
                return true;
            case 'RealProperties':
				return true;
			case 'IncomeSources':
                return true;
            case 'CounselingEvents':
                return true;
			default:
				return false;
		}
    }

    public hasPrimary(collectionName: string): boolean {
        switch (collectionName) {
            case 'CounselingEvents':
                return false;
            default:
                return true;
        }
    }
	
	public getPrimaryIndicatorKey(collectionName: string) : string {
		switch (collectionName) {
			case 'Assets':
                return 'IsPrimary';
            case 'Liabilities':
                return 'IsPrimary';
            case 'RealProperties':
				return 'IsPrimary';
			case 'IncomeSources':
				return 'IsPrimary';
			default:
				throw `Unhandled collection name ${collectionName}`;
		}
	}
	
	public getAssociationEntityKey(associationName: string) : string {
		switch (associationName) {
			case 'ConsumerAssets':
                return 'AssetId';
            case 'ConsumerLiabilities':
                return 'LiabilityId';
            case 'ConsumerRealProperties':
				return 'RealPropertyId';
			case 'ConsumerIncomeSources':
                return 'IncomeSourceId';
            case 'CounselingEventAttendances':
                return 'CounselingEventId';
			default:
				throw `Unhandled collection name ${associationName}`;
		}
	}
	
	public getAssociationName(collectionName: string) : string {
		switch (collectionName) {
			case 'Assets':
                return 'ConsumerAssets';
            case 'Liabilities':
                return 'ConsumerLiabilities';
            case 'RealProperties':
				return 'ConsumerRealProperties';
			case 'IncomeSources':
                return 'ConsumerIncomeSources';
            case 'CounselingEvents':
                return 'CounselingEventAttendances';
			default:
				throw `Unhandled collection name ${collectionName}`;
		}
	}
}