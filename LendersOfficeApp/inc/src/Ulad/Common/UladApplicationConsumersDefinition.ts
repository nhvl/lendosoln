import EntityDefinition from '../../api/EntityDefinition';

export default class UladApplicationConsumersDefinition extends EntityDefinition {
	classType: string = 'loan';
	collectionName: string = 'UladApplicationConsumers';
	
	UladApplicationId: string = '';
	ConsumerId: string = '';
	IsPrimary: string = '';
}