import EntityDefinition from '../../api/EntityDefinition';

export default class ConsumerRealPropertiesDefinition extends EntityDefinition {
	classType: string = 'loan';
	collectionName: string = 'ConsumerRealProperties';
	
	ConsumerId: string = '';
	RealPropertyId: string = '';
	AppId: string = '';
	IsPrimary: string = '';
}