import EntityDefinition from '../../api/EntityDefinition';

export default class ConsumerLiabilityDefinition extends EntityDefinition {
    classType: string = 'loan';
    collectionName: string = 'ConsumerLiabilities';

    ConsumerId: string = '';
    LiabilityId: string = '';
    AppId: string = '';
    IsPrimary: string = '';
}