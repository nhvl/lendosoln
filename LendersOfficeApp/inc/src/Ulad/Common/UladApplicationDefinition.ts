import EntityDefinition from '../../api/EntityDefinition';

export default class UladApplicationDefinition extends EntityDefinition {
	classType: string = 'loan';
	collectionName: string = 'UladApplications';
	
	IsPrimary: string = '';
}