﻿import * as angular from 'angular';
import { ITimeoutService } from 'angular';

import { AllApplicationsFilter, LegacyApplicationsFilter, UladApplicationsFilter, BorrowerFilter, ApplicationLabelsByValue } from '../Common/ApplicationFilters';
import AssetDefinition from '../Common/AssetDefinition';
import ConsumerDefinition from '../Common/ConsumerDefinition';
import ConsumerAssetDefinition from '../Common/ConsumerAssetDefinition';
import EditOwnerPopupHelper from '../Common/EditOwnerPopupHelper';
import LegacyApplicationConsumersDefinition from '../Common/LegacyApplicationConsumersDefinition';
import LegacyApplicationDefinition from '../Common/LegacyApplicationDefinition';
import LoanDefinition from '../Common/LoanDefinition';
import TotalsType from '../Common/TotalsType';
import UladApplicationDefinition from '../Common/UladApplicationDefinition';
import UladApplicationConsumersDefinition from '../Common/UladApplicationConsumersDefinition';
import UladAssociationHelper from '../Common/UladAssociationHelper';

import BooleanLqbDataServiceResponseConverter from '../../api/BooleanLqbDataServiceResponseConverter';
import Dictionary from '../../api/Dictionary';
import LqbDataServiceRequestFormatProvider from '../../api/LqbDataServiceRequestFormatProvider';
import LqbDataServiceRequestParameterFactory from '../../api/LqbDataServiceRequestParameterFactory';
import LqbDataServiceResponseHandler from '../../api/LqbDataServiceResponseHandler';
import LqbDataServiceOperationType from '../../api/LqbDataServiceOperationType';
import LqbDataServiceRequestType from '../../api/LqbDataServiceRequestType';

import getAppId from '../Common/Utils/getAppId';
import getAppName from '../Common/Utils/getAppName';
import getConsumerName from '../Common/Utils/getConsumerName';
import getLoanId from '../Common/Utils/getLoanId';
import getOwnersForNewEntity from '../Common/Utils/getOwnersForNewEntity';
import getTotalArgs from '../Common/Utils/getTotalArgs';
import groupBy from '../../utils/groupBy';
import orderConsumerNames from '../Common/Utils/orderConsumerNames';
import normalizeSelectedApplications from '../Common/Utils/normalizeSelectedApplications';
import sendWebRequest, { sendWebRequestByUrl } from '../Common/Utils/sendWebRequest';

class AssetsPageLoanDefinition extends LoanDefinition {
    sTridTargetRegulationVersionT: string = '';
    sLienToIncludeCashDepositInTridDisclosures: string = '';
    sLienPosT: string = '';
    sIsOFinNew: string = '';
    sConcurSubFin: string = '';
}

class AssetsPageLegacyApplicationDefinition extends LegacyApplicationDefinition {
    AssetLiaCompletedNotJointly : string = '';
}

class AssetsPageAssetDefinition extends AssetDefinition {
	AssetType: string = '';
    Desc: string = '';
    OtherTypeDesc: string = '';
	Value: string = '';
	GiftSourceData: string = '';
	GiftSource: string = '';
	
	CompanyName: string = '';
	DepartmentName: string = '';
	AccountName: string = '';
	AccountNum: string = '';
	
	StreetAddress: string = ''
	City: string = ''
	State: string = ''
	Zip: string = ''
	Phone: string = '';
	
	VerifSentDate: string = '';
	VerifReorderedDate: string = '';
	VerifRecvDate: string = '';
    VerifExpiresDate: string = '';

    IsCreditAtClosing: string = '';
    IsCreditAtClosingData: string = '';
    IsCreditAtClosingUserDependent: string = '';

    RequiresVod: string = '';
}

class AssetsPageConsumerDefinition extends ConsumerDefinition {
    FirstName: string = '';
    LastName: string = '';
}

declare function parseMoneyFloat(str: string): number;
declare function callWebMethodAsync(params: Dictionary<any>) : void;
declare function getQueryStringVariable(variable: string) : string;
declare function clearDirty(): void;
declare function updateDirtyBit(): void;
declare function linkMe(href: string, extraArgs: string): void;
declare function isDirty(): boolean;
declare function _initInput(): void;
declare function PolyConfirmSave(confirmCallback: (shouldSave: boolean) => void, shouldShowCancel: boolean, cancelCallback?: () => void): void;
declare function AddToRolodex_postServiceCall(result: Dictionary<any>, args: Dictionary<any>): void;

declare var LQBPopup: any;
declare var VRoot: string;
declare var AssetDescriptionsByValue: Dictionary<string>;

const $ = jQuery;
const parameterFactory = new LqbDataServiceRequestParameterFactory();
const uladAssociationHelper = new UladAssociationHelper();
const booleanValueConverter = new BooleanLqbDataServiceResponseConverter();

angular
.module('Assets', [])
.controller('AssetsController', ['$scope', '$timeout', function AssetsController($scope, $timeout : ITimeoutService) {
    const DefaultGiftSource = '0'; // Blank

    $scope.applicationLabelsByValue = ApplicationLabelsByValue;

    $scope.sortKey = null;
    $scope.sortDesc = false;

    $scope.assetFilterModel = AllApplicationsFilter;
    $scope.selectedApplicationIdModel = null;

	$scope.selectedApplications = new Array<string>();
    $scope.selectedRecordIndex = 0;

    $scope.selectedAsset = null;
    $scope.originalSelectedAsset = null;

    $scope.AssetLiaCompletedNotJointly = null;

    $scope.isDirty = isDirty;
    $scope.updateDirtyBit = updateDirtyBit;

    $scope.parseMoneyFloat = parseMoneyFloat;

    $scope.getFriendlyAssetTypeName = function (assetTypeValue: string) : string {
        return AssetDescriptionsByValue[assetTypeValue] as string;
    }

    $scope.onFormatMoney = function (input: HTMLInputElement) {
        if (input.id === 'SelectedAssetValue') {
            // Ensure the asset's value is formatted as expected,
            // which happens after the initial model update.
            $scope.$apply(() => {
                $scope.selectedAsset['Value'] = input.value;
            });
        }
    }

    $scope.onSmartZipcode = function () {
        // The changes to the inputs happen asynchronously and
        // will not update the model's value during the normal
        // digest cycle.
        $scope.selectedAsset.City = (document.getElementById('City') as HTMLInputElement).value;
        $scope.selectedAsset.State = $('#State').val() as string;
    }

    $scope.onDateFormat = function (input: HTMLInputElement) {
        $scope.selectedAsset[input.id] = input.value;
    }

    $scope.pickFromContacts = function () {
        const agentTypeBank = 20;

        const url = `${VRoot}/los/RolodexList.aspx?type=${agentTypeBank}&loanid=${getLoanId()}&IsFeeContactPicker=false&isCurrentAgentOnly=null`;
        const popupSettings = {
            onReturn: $scope.onPickFromContacts,
        };

        LQBPopup.Show(url, popupSettings);
    }

    $scope.onPickFromContacts = function (returnArgs: Dictionary<any>) {
        const ok: boolean = returnArgs["OK"];
        if (!ok) {
            return;
        }

        $scope.$apply(() => {
            $scope.selectedAsset.CompanyName = returnArgs["AgentCompanyName"];
            $scope.selectedAsset.DepartmentName = returnArgs["AgentDepartmentName"];
            $scope.selectedAsset.StreetAddress = returnArgs["CompanyStreetAddr"];
            $scope.selectedAsset.City = returnArgs["CompanyCity"];
            $scope.selectedAsset.State = returnArgs["CompanyState"];
            $scope.selectedAsset.Zip = returnArgs["CompanyZip"];
            $scope.selectedAsset.Phone = returnArgs["PhoneOfCompany"];

            updateDirtyBit();
        });
    }

    $scope.addToContacts = function () {
        $scope.checkForSave(() => {
            const url = `${VRoot}/common/UtilitiesService.aspx?method=AddToRolodex`;
            const agentTypeBank = 20;

            const args = {
                AddCommand: 'AddIfUnique',
                AgentType: agentTypeBank,
                AgentCompanyName: $scope.selectedAsset.CompanyName,
                AgentDepartmentName: $scope.selectedAsset.DepartmentName,
                AgentStreetAddr: $scope.selectedAsset.StreetAddress,
                AgentCity: $scope.selectedAsset.City,
                AgentState: $scope.selectedAsset.State,
                AgentZip: $scope.selectedAsset.Zip,
                PhoneOfCompany: $scope.selectedAsset.Phone
            }

            sendWebRequestByUrl(
                url,
                JSON.stringify(args),
                true/*displayOverlay*/,
                (result: Dictionary<any>) => {
                    // JSON POSTs send the result in the 'd' variable,
                    // but the legacy rolodex expects a 'value' key.
                    result['value'] = result['d'];
                    AddToRolodex_postServiceCall(result, args);
                });
        });
    }

    $scope.refreshCalculation = function () {
        // Allow other scripts to finish running before calling
        // for a data refresh, e.g. money formatting.
        $timeout(() => {
            $scope.saveData(null/*callback*/, true/*refreshOnly*/);
        });
    }

    $scope.revertAssetChanges = function (backupAsset: Dictionary<any>) {
        if ($scope.vm.loan == null || $scope.vm.loan.Assets == null) {
            return;
        }

        const backupId = backupAsset['Id'];
        for (let i = 0; i < $scope.vm.loan.Assets.length; ++i) {
            const asset = $scope.vm.loan.Assets[i];

            if (asset['Id'] === backupId) {
                $scope.vm.loan.Assets[i] = backupAsset;
                return;
            }
        }
    }

    $scope.sortAssets = function (asset1: Dictionary<any>, asset2: Dictionary<any>): number {
        const direction = $scope.sortDesc ? -1 : 1;

        switch ($scope.sortKey) {
            case 'Owner':
                const firstAssetOwners: Array<Dictionary<any>> = asset1['Owners'];
                const secondAssetOwners: Array<Dictionary<any>> = asset2['Owners'];

                const firstAssetPrimaryOwner = firstAssetOwners[0];
                const secondAssetPrimaryOwner = secondAssetOwners[0];

                const firstAssetPrimaryOwnerName = $scope.getConsumerName(firstAssetPrimaryOwner['ConsumerId']);
                const secondAssetPrimaryOwnerName = $scope.getConsumerName(secondAssetPrimaryOwner['ConsumerId']);

                return direction * firstAssetPrimaryOwnerName.localeCompare(secondAssetPrimaryOwnerName);

            case 'AssetType':
                const firstAssetType = asset1['AssetType'];
                const secondAssetType = asset2['AssetType'];

                if (firstAssetType == null || firstAssetType.trim().length === 0) {
                    if (secondAssetType == null || secondAssetType.trim().length === 0) {
                        return 0;
                    }

                    return direction * -1;
                }

                if (secondAssetType == null || secondAssetType.trim().length === 0) {
                    if (firstAssetType == null || firstAssetType.trim().length === 0) {
                        return 0;
                    }

                    return direction * 1;
                }

                const firstAssetTypeDescription = AssetDescriptionsByValue[firstAssetType];
                const secondAssetTypeDescription = AssetDescriptionsByValue[secondAssetType];

                return direction * firstAssetTypeDescription.localeCompare(secondAssetTypeDescription);

            case 'Desc':
                const firstAssetDescription = asset1['Desc'];
                const secondAssetDescription = asset2['Desc'];

                if (firstAssetDescription == null || firstAssetDescription.trim().length === 0) {
                    if (secondAssetDescription == null || secondAssetDescription.trim().length === 0) {
                        return 0;
                    }

                    return direction * -1;
                }

                if (secondAssetDescription == null || secondAssetDescription.trim().length === 0) {
                    if (firstAssetDescription == null || firstAssetDescription.trim().length === 0) {
                        return 0;
                    }

                    return direction * 1;
                }

                return direction * firstAssetDescription.localeCompare(secondAssetDescription);

            case 'Value':
                let firstAssetValue = asset1['Value'];
                let secondAssetValue = asset2['Value'];

                if (firstAssetValue == null || firstAssetValue.trim().length === 0) {
                    firstAssetValue = Number.MIN_SAFE_INTEGER.toString();
                }

                if (secondAssetValue == null || secondAssetValue.trim().length === 0) {
                    secondAssetValue = Number.MIN_SAFE_INTEGER.toString();
                }

                const firstAssetValueParsed = parseMoneyFloat(firstAssetValue);
                const secondAssetValueParsed = parseMoneyFloat(secondAssetValue);

                if (firstAssetValueParsed === secondAssetValueParsed) {
                    return 0;
                }

                return direction * (firstAssetValueParsed > secondAssetValueParsed ? -1 : 1);

            default:
                throw 'Unhandled sort key ' + $scope.sortKey;
        }
    }

    $scope.orderBy = function (key: string) {
        $scope.checkForSave(() => {
            if (key === $scope.sortKey) {
                $scope.sortDesc = !$scope.sortDesc;
            }
            else {
                $scope.sortKey = key;
                $scope.sortDesc = false;
            }

            if ($scope.vm.loan == null ||
                $scope.vm.loan.Assets == null ||
                $scope.vm.loan.Assets.length === 0) {
                return;
            }

            $scope.vm.loan.Assets.sort($scope.sortAssets);
            $scope.setRecordOrder($scope.vm.loan.Assets);

            // Update the selected record index to account for the new
            // order based on the sorting result.
            const selectedAssetId = $scope.selectedAsset['Id'];
            $scope.selectedRecordIndex = $scope.vm.loan.Assets.findIndex((asset: Dictionary<any>) => {
                return asset['Id'] == selectedAssetId;
            });
        });
    }

    $scope.setSelectedAsset = function (asset: Dictionary<any>) {
        // Normalize the gift source to blank if it was previously undefined.
        if (asset['GiftSourceData'] == null || asset['GiftSourceData'].trim() === "") {
            asset['GiftSourceData'] = DefaultGiftSource;
        }

        $scope.selectedAsset = asset;

        const backupAsset: Dictionary<any> = Object.assign(new Dictionary<any>(), asset);
        $scope.originalSelectedAsset = backupAsset;
    }

    $scope.checkForSave = function (postCallback: () => void, cancelCallback?: () => void, updateTotalsOnSave: boolean = true) {
        if (isDirty()) {
            PolyConfirmSave(
                (shouldSave: boolean) => {
                    if (shouldSave) {
                        $scope.saveData(postCallback, false/*refreshOnly*/, updateTotalsOnSave);
                    }
                    else {
                        clearDirty();

                        $timeout(() => {
                            $scope.revertAssetChanges($scope.originalSelectedAsset);
                            $scope.setSelectedAsset($scope.originalSelectedAsset);
                            $scope.refreshCalculation();

                            postCallback();
                        });
                    }
                },
                true/*shouldShowCancel*/,
                () => {
                    if (cancelCallback != null) {
                        $timeout(cancelCallback);
                    }
                });
        }
        else {
            postCallback();
        }
    }
	
    $scope.moveToPreviousRecord = function () {
        if ($scope.vm.loan.Assets == null || $scope.vm.loan.Assets.length === 0) {
            return;
        }

        $scope.checkForSave(() => {
            const startIndex = Math.max($scope.selectedRecordIndex - 1, 0);
            let nextIndex = -1;

            for (let i = startIndex; i >= 0; --i) {
                const assetAtIndex = $scope.vm.loan.Assets[i];
                if ($scope.assetMatchesFilter(assetAtIndex)) {
                    nextIndex = i;
                    break;
                }
            }

            if (nextIndex >= 0) {
                $scope.selectedRecordIndex = nextIndex;
                $scope.setSelectedAsset($scope.vm.loan.Assets[$scope.selectedRecordIndex]);
            }
            else if ($scope.selectedRecordIndex === 0) {
                $scope.moveToNextRecord();
            }
        });
	}
	
    $scope.moveToNextRecord = function () {
        if ($scope.vm.loan.Assets == null || $scope.vm.loan.Assets.length === 0) {
            return;
        }

         $scope.checkForSave(() => {
            const startIndex = Math.min($scope.selectedRecordIndex + 1, $scope.vm.loan.Assets.length - 1);
            let nextIndex = -1;

            for (let i = startIndex; i < $scope.vm.loan.Assets.length; ++i) {
                const assetAtIndex = $scope.vm.loan.Assets[i];
                if ($scope.assetMatchesFilter(assetAtIndex)) {
                    nextIndex = i;
                    break;
                }
            }

            if (nextIndex >= 0) {
                $scope.selectedRecordIndex = nextIndex;
                $scope.setSelectedAsset($scope.vm.loan.Assets[$scope.selectedRecordIndex]);
            }
        });
	}
	
	$scope.selectAsset = function(asset: Dictionary<string>) {
         $scope.checkForSave(() => {
            const assetId = asset['Id'];

            for (let i = 0; i < $scope.vm.loan.Assets.length; ++i) {
                const assetAtIndex = $scope.vm.loan.Assets[i];

                if (assetId === assetAtIndex['Id']) {
                    $scope.selectedRecordIndex = i;
                    $scope.setSelectedAsset($scope.vm.loan.Assets[$scope.selectedRecordIndex]);
                    break;
                }
            }
        });
	}

	$scope.insertRecord = function() {
        $scope.checkForSave(() => {
            const insertIndex = Math.max(0, $scope.selectedRecordIndex);
            $scope.addAsset(insertIndex);
        });
	}
	
	$scope.addRecord = function() {
        $scope.checkForSave(() => {
            $scope.addAsset();
        });
	}
	
    $scope.addAsset = function (insertIndex?: number) {		
        const hadActiveAsset = $scope.vm.loan.Assets.filter($scope.assetMatchesFilter).length !== 0;

        const owners = getOwnersForNewEntity(
            $scope.selectedApplications,
            $scope.selectedApplicationIdModel,
            $scope.vm.loan.UladApplications,
            $scope.vm.loan.UladApplicationConsumers);

        const primaryOwner = owners.find((owner: Dictionary<string|boolean>) => owner['IsPrimary'] as boolean);
        if (primaryOwner == null) {
            throw 'Unable to add primary ownership for new asset.';
        }

        const additionalOwners = owners.filter((owner: Dictionary<string|boolean>) => owner['ConsumerId'] !== primaryOwner['ConsumerId']);
        const additionalOwnerIds = additionalOwners.map((owner: Dictionary<string|boolean>) => owner['ConsumerId'] as string);

        const primaryConsumerId = primaryOwner['ConsumerId'] as string;
        const args = parameterFactory.createAddParameter(AssetsPageAssetDefinition, primaryConsumerId, additionalOwnerIds, insertIndex);
		
		let provider = new LqbDataServiceRequestFormatProvider();
		provider.addRequest(LqbDataServiceRequestType.Add, args);
		
		sendWebRequest(
			LqbDataServiceOperationType.Save, 
			provider.toRequest(),
			(data: Dictionary<any>) => { 
				const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
				if (handler.hasError()) {
					alert(handler.getErrorMessage());
					return;
				}
				
				$scope.$apply(function() {
					const response = handler.getResponseByKey(LqbDataServiceRequestType.Add, 'loan');
			
					var asset = new Dictionary<any>();
					asset['Id'] = response['id'];
                    asset['Owners'] = [primaryOwner, ...additionalOwners];
					
					if (insertIndex == null) {
						const assetsLength = $scope.vm.loan.Assets.push(asset);
						$scope.selectedRecordIndex = assetsLength - 1;
					}
					else {
						$scope.vm.loan.Assets.splice(insertIndex, 0/*No entries before: only adding the asset*/, asset);
						$scope.selectedRecordIndex = insertIndex;
					}

                    orderConsumerNames([asset], $scope.getConsumerName);
                    $scope.setSelectedAsset(asset);

                    if (!hadActiveAsset) {
                        // No inputs are in the DOM until there is an active asset.
                        // Once we've added an asset, initialize the new inputs.
                        window.setTimeout(_initInput, 0);
                    }
				});
			});
	}
	
	$scope.deleteRecord = function() {
		const recordId = $scope.selectedAsset['Id'];
		
        const deleteArgs = parameterFactory.createRemoveParameter(AssetsPageAssetDefinition, recordId);
        const getAssetTotalArgs = $scope.getAssetTotalArgs();

		let provider = new LqbDataServiceRequestFormatProvider();
        provider.addRequest(LqbDataServiceRequestType.Remove, deleteArgs);
        provider.addRequest(LqbDataServiceRequestType.GetAssetTotals, getAssetTotalArgs);
		
		sendWebRequest(
            LqbDataServiceOperationType.Save, 
			provider.toRequest(),
			(data: Dictionary<any>) => { 
                const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
				if (handler.hasError()) {
					alert(handler.getErrorMessage());
					return;
				}
				
                $scope.$apply(function () {
                    $scope.vm.assetTotals = handler.getResponseByKey(LqbDataServiceRequestType.GetAssetTotals, 'loan');

                    if ($scope.vm.loan.Assets.length === 1) {
                        $scope.vm.loan.Assets = new Array<Dictionary<string>>();
                        $scope.selectedRecordIndex = 0;
                        $scope.selectedAsset = null;
                    }
                    else {
                        $scope.vm.loan.Assets.splice($scope.selectedRecordIndex, 1);
                        $scope.moveToPreviousRecord();
                    }
				});
			});
	}

    $scope.navigateToVod = function() {
        $scope.checkForSave(() => {
            linkMe(VRoot + '/newlos/Verifications/VODRecord.aspx', 'isUlad=1&recordid=' + $scope.selectedAsset['Id']);
        }, null, false);
    }
	
	$scope.moveRecordUp = function() {
		if ($scope.selectedRecordIndex === 0) {
			return;
		}
		
         $scope.checkForSave(() => {
            const assets = $scope.vm.loan.Assets;
            const temp = assets[$scope.selectedRecordIndex];
            assets[$scope.selectedRecordIndex] = assets[$scope.selectedRecordIndex - 1];
            assets[$scope.selectedRecordIndex - 1] = temp;

            $scope.setRecordOrder(assets, () => { $scope.selectedRecordIndex-- });
        });
	}
	
	$scope.moveRecordDown = function() {
		if ($scope.selectedRecordIndex === $scope.vm.loan.Assets.length - 1) {
			return;
        }

         $scope.checkForSave(() => {
            const assets = $scope.vm.loan.Assets;
            const temp = assets[$scope.selectedRecordIndex];
            assets[$scope.selectedRecordIndex] = assets[$scope.selectedRecordIndex + 1];
            assets[$scope.selectedRecordIndex + 1] = temp;

            $scope.setRecordOrder(assets, () => { $scope.selectedRecordIndex++ });
        });
	}
	
	$scope.setRecordOrder = function(assets: Array<Dictionary<string>>, callback?: () => void) {
		const assetIds = assets.map((asset: Dictionary<string>) => asset['Id']);
        const args = parameterFactory.createSetOrderParameter(AssetsPageAssetDefinition, assetIds);
		
		let provider = new LqbDataServiceRequestFormatProvider();
		provider.addRequest(LqbDataServiceRequestType.SetOrder, args);
		
		sendWebRequest(
            LqbDataServiceOperationType.Save, 
			provider.toRequest(),
			(data: Dictionary<any>) => { 
				const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
				if (handler.hasError()) {
					alert(handler.getErrorMessage());
					return;
				}
				
				$scope.$apply(() => {
					$scope.vm.loan.Assets = assets;

                    if (typeof callback === 'function') {
                        callback();
                    }
				});
			});
	}
	
    $scope.saveData = function (callback: () => void, refreshOnly: boolean = false, updateTotalsOnSave: boolean = true) {
        let provider = new LqbDataServiceRequestFormatProvider();
        let setters = new Array<Dictionary<any>>();
        let getters = new Array<Dictionary<any>>();

        setters.push(parameterFactory.createIndividualSetParameter(
            AssetsPageLoanDefinition,
            { sLienToIncludeCashDepositInTridDisclosures: $scope.vm.loan.sLienToIncludeCashDepositInTridDisclosures }));

        if ($scope.selectedAsset != null) {
            setters.push(parameterFactory.createIndividualSetParameter(
                AssetsPageAssetDefinition,
                $scope.selectedAsset,
                $scope.selectedAsset['Id']));
        }

        if ($scope.assetFilterModel == LegacyApplicationsFilter && $scope.vm.AssetLiaCompletedNotJointly != null) {
            setters.push(parameterFactory.createIndividualSetParameter(
                AssetsPageLegacyApplicationDefinition,
                { AssetLiaCompletedNotJointly: $scope.vm.AssetLiaCompletedNotJointly },
                $scope.selectedApplicationIdModel));

            getters.push(parameterFactory.createIndividualGetParameter([AssetsPageLegacyApplicationDefinition]));
        }

        let selectedAssetId: string;
        if ($scope.selectedAsset != null) {
            $scope.selectedAsset.IsCreditAtClosingData = $scope.selectedAsset.IsCreditAtClosing;
            selectedAssetId = $scope.selectedAsset['Id'];
            getters.push(parameterFactory.createIndividualGetParameter([AssetsPageAssetDefinition], selectedAssetId));
        }
        
        const setArgs = parameterFactory.createAggregateSetParameter(setters);
        provider.addRequest(LqbDataServiceRequestType.Set, setArgs);

        if (updateTotalsOnSave) {
            const getAssetTotalArgs = $scope.getAssetTotalArgs();
            provider.addRequest(LqbDataServiceRequestType.GetAssetTotals, getAssetTotalArgs);
        }

        if (getters.length !== 0) {
            const aggregateGetter = parameterFactory.createAggregateGetParameter(getters);
            provider.addRequest(LqbDataServiceRequestType.Get, aggregateGetter);
        }
		
		sendWebRequest(
			refreshOnly ? LqbDataServiceOperationType.Load : LqbDataServiceOperationType.Save, 
			provider.toRequest(),
			(data: Dictionary<any>) => { 
				const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
				if (handler.hasError()) {
					alert(handler.getErrorMessage());
					return;
                }

                $scope.$apply(() => {
                    if (updateTotalsOnSave) {
                        $scope.vm.assetTotals = handler.getResponseByKey(LqbDataServiceRequestType.GetAssetTotals, 'loan');
                    }

                    if ($scope.assetFilterModel == LegacyApplicationsFilter && $scope.vm.AssetLiaCompletedNotJointly != null) {
                        const getResponse = handler.getResponseByKey(LqbDataServiceRequestType.Get, 'loan');
                        $scope.vm.loan.LegacyApplications = getResponse['LegacyApplications'];
                    }

                    if (!refreshOnly) {
                        clearDirty();
                    }

                    if ($scope.selectedAsset != null) {
                        const getResponse = handler.getResponseByKey(LqbDataServiceRequestType.Get, 'loan');
                        for (const key of Object.keys(getResponse.Assets[0])) {
                            $scope.selectedAsset[key] = getResponse.Assets[0][key];
                        }
                    }

                    if (typeof callback === 'function') {
                        callback();
                    }
                });
			});
	}
	
	$scope.editAssetOwners = function(asset: Dictionary<Array<Dictionary<string>>>) {
        $scope.checkForSave(() => {
            const selectedOwners = asset['Owners'];

            var helper = new EditOwnerPopupHelper();
            helper.storeEntityOwnerIdsToCache(selectedOwners, (result: Dictionary<any>) => {
                if (result.error) {
                    alert(result.UserMessage);
                    return;
                }

                const cacheKey = result.value['CacheKey'];
                const url = `${VRoot}/newlos/Ulad/EditOwner.aspx?loanid=${getLoanId()}&appid=${getAppId()}&id=${asset['Id']}&t=Asset&s=${cacheKey}`;
                const settings = {
                    onReturn: $scope.onOwnerPopupClose
                };
                LQBPopup.Show(url, settings);
            });
        });
	}
	
	$scope.onOwnerPopupClose = function(returnArgs: Dictionary<any>) {
		const primaryOwner : Dictionary<string> = returnArgs['PrimaryOwner'];
		const additionalOwners : Array<Dictionary<string>> = returnArgs['AdditionalOwners'];

        const getAssetTotalArgs = $scope.getAssetTotalArgs();
        const setOwnerArgs = parameterFactory.createSetOwnerParameter(
            ConsumerAssetDefinition,
            $scope.selectedAsset['Id'],
            primaryOwner['Id'],
            additionalOwners.map((owner: Dictionary<string>) => owner['Id']));
		
		let provider = new LqbDataServiceRequestFormatProvider();
        provider.addRequest(LqbDataServiceRequestType.SetOwner, setOwnerArgs);
        provider.addRequest(LqbDataServiceRequestType.GetAssetTotals, getAssetTotalArgs);
		
		sendWebRequest(
			LqbDataServiceOperationType.Save, 
			provider.toRequest(),
			(data: Dictionary<any>) => { 
				const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
				if (handler.hasError()) {
					alert(handler.getErrorMessage());
					return;
				}
				
				LQBPopup.Hide();

				const provider = $scope.createInitProvider();
                $scope.loadData(provider.toRequest(), () => {
                    $scope.vm.assetTotals = handler.getResponseByKey(LqbDataServiceRequestType.GetAssetTotals, 'loan');

                    // Ensure that the asset editor is hidden when the asset
                    // no longer matches the application filter. For the "All"
                    // view, this logic is ignored.
                    if ($scope.assetFilterModel === AllApplicationsFilter) {
                        $scope.setSelectedAsset($scope.vm.loan.Assets[$scope.selectedRecordIndex]);
                        return;
                    }

                    const selectedApplication: Dictionary<Array<Dictionary<string>>> = $scope.selectedApplications.find((app: Dictionary<string>) => {
                        return app['key'] === $scope.selectedApplicationIdModel;
                    });

                    const updatedSelectedAsset = $scope.vm.loan.Assets.find((asset: Dictionary<string>) => asset['Id'] === $scope.selectedAsset['Id']);
                    const selectedAssetNewOwners: Array<Dictionary<string>> = updatedSelectedAsset['Owners'];
                    const selectedAppOwners: Array<Dictionary<string>> = selectedApplication['values'];

                    const selectedAssetOwnerInCurrentApp = selectedAssetNewOwners.find((assetOwner: Dictionary<string>) => {
                        return selectedAppOwners.find((appOwner: Dictionary<string>) => appOwner['ConsumerId'] === assetOwner['ConsumerId']) != null;
                    });

                    if (selectedAssetOwnerInCurrentApp != null) {
                        $scope.setSelectedAsset($scope.vm.loan.Assets[$scope.selectedRecordIndex]);
                    }
                    else {
                        $scope.selectedRecordIndex = -1;
                        $scope.selectedAsset = null;
                        $scope.moveToNextRecord();
                    }
				});
			});
	}
	
    $scope.getConsumerName = (consumerId: string) => getConsumerName($scope.vm.loan.Consumers, consumerId);
	
    $scope.getAppName = (appIdConsumerIdPairs: Array<Dictionary<string>>) => getAppName(appIdConsumerIdPairs, $scope.getConsumerName);
	
	$scope.applyResult = function(data: Dictionary<any>, callback?: any) {
		$scope.$apply(function() {
			const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
			if (handler.hasError()) {
				alert(handler.getErrorMessage());
				return;
			}
			
			const getResponse = handler.getResponseByKey(LqbDataServiceRequestType.Get, 'loan');
            $scope.vm.loan = Object.assign($scope.vm.loan, getResponse);
            orderConsumerNames($scope.vm.loan.Assets, $scope.getConsumerName);
			
			const assetTotalsResponse = handler.getResponseByKey(LqbDataServiceRequestType.GetAssetTotals, 'loan');
			$scope.vm.assetTotals = assetTotalsResponse;
			
			if (callback != null) {
				callback();
			}
		});
	}
	
	$scope.loadData = function(request: string, callback?: any) {
		sendWebRequest(
			LqbDataServiceOperationType.Load,
			request,
			(data: Dictionary<any>) => { $scope.applyResult(data, callback); });
	}
	
	$scope.assetMatchesFilter = function(asset: Dictionary<any>) {
		if ($scope.vm.loan.ConsumerAssets == null) {
			return true;
		}
		
        if ($scope.selectedApplications == null || $scope.assetFilterModel === AllApplicationsFilter) {
			return true;
		}
		
        const applicableApps = $scope.selectedApplications.filter((app: Dictionary<any>) => app['key'] === $scope.selectedApplicationIdModel)
			.map((app: Dictionary<any>) => app['values'])
			.reduce((a: Array<Dictionary<any>>, b: Array<Dictionary<any>>) => a.concat(b), new Array<Dictionary<any>>());
		
		if (applicableApps.length === 0) {
			return false;
		}
		
        const matchingOwner = asset['Owners'].find(function (assetOwner: Dictionary<string>) {
            // Ignore IsPrimary when searching at the borrower level.
            if ($scope.assetFilterModel !== BorrowerFilter && !assetOwner['IsPrimary']) {
				return false;
			}
			
			return applicableApps.find((app: Dictionary<string>) => app['ConsumerId'] === assetOwner['ConsumerId']) != null;
		});
		
		return matchingOwner != null;
	}
	
    $scope.getAssetTotalArgs = function () {
        return getTotalArgs(parameterFactory, $scope.assetFilterModel, $scope.selectedApplicationIdModel);
    }
	
	$scope.updateAssetTotalsForCurrentApplication = function() {
        const getAssetTotalArgs = $scope.getAssetTotalArgs();
        $scope.updateAssetTotals(getAssetTotalArgs);
	}
	
	$scope.updateAssetTotals = function(getAssetTotalArgs: Dictionary<string>) {
		let provider = new LqbDataServiceRequestFormatProvider();
		provider.addRequest(LqbDataServiceRequestType.GetAssetTotals, getAssetTotalArgs);
		
		sendWebRequest(
			LqbDataServiceOperationType.Load, 
			provider.toRequest(),
			(data: Dictionary<any>) => {
				const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
				if (handler.hasError()) {
					alert(handler.getErrorMessage());
					return;
				}
				
                $scope.$apply(() => {
                    $scope.vm.assetTotals = handler.getResponseByKey(LqbDataServiceRequestType.GetAssetTotals, 'loan');
                });
			});
    }

    $scope.updateAssetsLiabilitiesCompletedJointly = function () {
        if ($scope.assetFilterModel !== LegacyApplicationsFilter) {
            return;
        }

        var selectedLegacyApp = $scope.vm.loan.LegacyApplications.find((legacyApp: Dictionary<string>) => legacyApp['Id'] === $scope.selectedApplicationIdModel);
        $scope.vm.AssetLiaCompletedNotJointly = selectedLegacyApp['AssetLiaCompletedNotJointly'];
    }
	
	$scope.displayAllAssets = function() {
		$scope.init();
	}
	
	$scope.displayLegacyAppAssets = function() { 	
        $scope.selectedApplications = normalizeSelectedApplications(
            $scope.vm.loan.LegacyApplicationConsumers,
            'LegacyApplicationId',
            'ConsumerId',
            (app: Dictionary<boolean>) => app['IsPrimary']);

        $scope.selectedApplicationIdModel = $scope.selectedApplications[0]['key'];

        $scope.selectedRecordIndex = -1;
        $scope.moveToNextRecord();

        const getAssetTotalArgs = parameterFactory.createGetTotalsParameter(
            AssetsPageLoanDefinition,
            TotalsType.LegacyApplication,
            $scope.selectedApplicationIdModel);

        $scope.updateAssetTotals(getAssetTotalArgs);
        $scope.updateAssetsLiabilitiesCompletedJointly();
	}
	
	$scope.displayUladAppAssets = function() { 
		$scope.selectedApplications = normalizeSelectedApplications(
            $scope.vm.loan.UladApplicationConsumers,
            'UladApplicationId',
            'ConsumerId',
            (app: Dictionary<boolean>) => app['IsPrimary']);

        $scope.selectedApplicationIdModel = $scope.selectedApplications[0]['key'];

        $scope.selectedRecordIndex = -1;
        $scope.moveToNextRecord();

        const getAssetTotalArgs = parameterFactory.createGetTotalsParameter(
            AssetsPageLoanDefinition,
            TotalsType.UladApplication,
            $scope.selectedApplicationIdModel);

        $scope.updateAssetTotals(getAssetTotalArgs);
	}
	
    $scope.displayBorrowerAssets = function () { 
        $scope.selectedApplications = normalizeSelectedApplications(
            $scope.vm.loan.Consumers,
            'Id',
            'Id',
            (app: Dictionary<string>) => true);

        $scope.selectedApplicationIdModel = $scope.selectedApplications[0]['key'];

        $scope.selectedRecordIndex = -1;
        $scope.moveToNextRecord();

        const getAssetTotalArgs = parameterFactory.createGetTotalsParameter(
            AssetsPageLoanDefinition,
            TotalsType.Consumer,
            $scope.selectedApplicationIdModel);

        $scope.updateAssetTotals(getAssetTotalArgs);
	}
	
    $scope.onAssetFilterChange = function (oldAssetFilterModel: string) {
        $scope.checkForSave(
            () => {
                $scope.selectedApplicationIdModel = null;
                $scope.selectedApplications = null;
                $scope.selectedAsset = null;

                switch ($scope.assetFilterModel) {
                    case AllApplicationsFilter:
                        $scope.displayAllAssets();
                        break;
                    case LegacyApplicationsFilter:
                        $scope.displayLegacyAppAssets();
                        break;
                    case UladApplicationsFilter:
                        $scope.displayUladAppAssets();
                        break;
                    case BorrowerFilter:
                        $scope.displayBorrowerAssets();
                        break;
                    default:
                        throw `Unhandled asset filter value ${$scope.assetFilterModel}.`;
                }

                // Give the filtering time to complete before initializing inputs.
                $timeout(_initInput, 500);
            },
            () => {
                $scope.assetFilterModel = oldAssetFilterModel;
            },
            false/*updateTotalsOnSave*/);
	}
	
    $scope.onApplicationFilterChange = function (newSelectedApplicationIdModel: string) {
        const oldSelectedApplicationIdModel : string = $scope.selectedApplicationIdModel;

        $scope.checkForSave(
            () => {
                $scope.selectedApplicationIdModel = newSelectedApplicationIdModel;

                $scope.selectedRecordIndex = -1;
                $scope.selectedAsset = null;

                $scope.moveToNextRecord();
                $scope.updateAssetTotalsForCurrentApplication();
                $scope.updateAssetsLiabilitiesCompletedJointly();

                // Give the filtering time to complete before initializing inputs.
                $timeout(_initInput, 500);
            },
            () => {
                $('#ApplicationFilter').val(oldSelectedApplicationIdModel);
                $scope.selectedApplicationIdModel = oldSelectedApplicationIdModel;
            },
            false/*updateTotalsOnSave*/);
    }

    $scope.onGiftSourceChange = function() {
        $scope.selectedAsset['GiftSourceData'] = $scope.selectedAsset['GiftSource'];
    };

    $scope.isGiftSourceDisabled = function(){
       switch($scope.selectedAsset["AssetType"])  {
        case '0':
        case '9':
        case '13':
        case '21':
        case '26':
        case '28':
            return true;
        default: 
            return false;
      }
    };

    $scope.onAssetTypeChange = function () {
        const newAssetType: string = $scope.selectedAsset["AssetType"];

        // Clear values when selecting Auto or Other Illiquid Asset.
        if (newAssetType === "0" || newAssetType === "9") {
            $scope.selectedAsset.CompanyName = null;
            $scope.selectedAsset.DepartmentName = null;
            $scope.selectedAsset.StreetAddress = null;
            $scope.selectedAsset.City = null;
            $scope.selectedAsset.State = '';
            $scope.selectedAsset.Zip = null;
            $scope.selectedAsset.Phone = null;
            $scope.selectedAsset.AccountName = null;
            $scope.selectedAsset.AccountNum = null;
            $scope.selectedAsset.VerifSentDate = null;
            $scope.selectedAsset.VerifReorderedDate = null;
            $scope.selectedAsset.VerifRecvDate = null;
            $scope.selectedAsset.VerifExpiresDate = null;
        }

        // Switching from blank to a valid value may not
        // trigger the page's dirty bit update. Trigger this
        // behavior here.
        updateDirtyBit();
        $scope.saveData(null/*callback*/, true/*refreshOnly*/);
    }
	
	$scope.init = function() {
		$scope.vm = new Dictionary<Dictionary<any>>();
		$scope.vm.loan = new Dictionary<any>();
		
		const provider = $scope.createInitProvider();
        $scope.loadData(provider.toRequest(), function () {
            $scope.moveToPreviousRecord();
            window.setTimeout(_initInput, 0);
		});
	}
	
	$scope.createInitProvider = function() : LqbDataServiceRequestFormatProvider {
		let provider = new LqbDataServiceRequestFormatProvider();
		
        const getArgs = parameterFactory.createIndividualGetParameter([
            AssetsPageAssetDefinition,
            AssetsPageConsumerDefinition,
            AssetsPageLegacyApplicationDefinition,
            AssetsPageLoanDefinition,
            ConsumerAssetDefinition,
            LegacyApplicationConsumersDefinition,
            UladApplicationDefinition,
            UladApplicationConsumersDefinition]);
		
        const getAssetTotalArgs = parameterFactory.createGetTotalsParameter(AssetsPageLoanDefinition, TotalsType.Loan);
		
		provider.addRequest(LqbDataServiceRequestType.Get, getArgs);
		provider.addRequest(LqbDataServiceRequestType.GetAssetTotals, getAssetTotalArgs);
		return provider;
	}
	
	$scope.init();
}]);
