import * as angular from 'angular';
import { ITimeoutService } from 'angular';

import ApplicationType from '../Common/ApplicationType';
import ConsumerDefinition from '../Common/ConsumerDefinition';
import LegacyApplicationConsumersDefinition from '../Common/LegacyApplicationConsumersDefinition';
import LegacyApplicationDefinition from '../Common/LegacyApplicationDefinition';
import LoanDefinition from '../Common/LoanDefinition';
import UladApplicationDefinition from '../Common/UladApplicationDefinition';
import UladApplicationConsumersDefinition from '../Common/UladApplicationConsumersDefinition';
import UladAssociationHelper from '../Common/UladAssociationHelper';

import BooleanLqbDataServiceResponseConverter from '../../api/BooleanLqbDataServiceResponseConverter';
import Dictionary from '../../api/Dictionary';
import LqbDataServiceRequestFormatProvider from '../../api/LqbDataServiceRequestFormatProvider';
import LqbDataServiceRequestParameterFactory from '../../api/LqbDataServiceRequestParameterFactory';
import LqbDataServiceResponseHandler from '../../api/LqbDataServiceResponseHandler';
import LqbDataServiceOperationType from '../../api/LqbDataServiceOperationType';
import LqbDataServiceRequestType from '../../api/LqbDataServiceRequestType';

import getConsumerName from '../Common/Utils/getConsumerName';
import getLoanId from '../Common/Utils/getLoanId';
import sendWebRequest from '../Common/Utils/sendWebRequest';

class ApplicationManagementPageConsumerDefinition extends ConsumerDefinition {
    FirstName: string = '';
    LastName: string = '';
}

class ApplicationManagementPageLoanDefinition extends LoanDefinition {
    sCanSyncUladAndLegacyApps: string = '';
    sSyncUladAndLegacyApps: string = '';
    sGseTargetApplicationT: string = '';
    sGseTargetApplicationTLckd: string = '';
}

declare function callWebMethodAsync(params: Dictionary<any>): void;
declare function getQueryStringVariable(variable: string): string;
declare function forceResetLeftNav(): void;
declare function clearDirty(): void;
declare function isDirty(): boolean;
declare function PolyConfirmSave(confirmCallback: (shouldSave: boolean) => void, shouldShowCancel: boolean, cancelCallback?: () => void): void;

declare var VRoot: string;

declare global {
    interface Window {
        info: {
            f_swapPrimary: (legacyAppId: string) => void;
            f_removeBorrower: (legacyAppId: string) => void;
            f_updateApplicantDDL: (consumerName: string, legacyAppId: string) => void;
            addNewAppToDropdown: (legacyAppId: string) => void;
            f_refreshInfoFromDB?: () => void;
            f_refreshInfo?: () => void;
        }
    }
}

const $ = jQuery;

const parameterFactory = new LqbDataServiceRequestParameterFactory();
const uladAssociationHelper = new UladAssociationHelper();
const booleanValueConverter = new BooleanLqbDataServiceResponseConverter();

angular
.module('ApplicationManagement', [])
.controller('ApplicationManagementController', ['$scope', '$timeout', function ApplicationManagementController($scope, $timeout : ITimeoutService) {
    $scope.legacyMode = true;

    $scope.changeAppMode = function () {
        $scope.legacyMode = !$scope.legacyMode;

        $timeout(() => {
            $scope.setupGripHandles();
        });
    }

    $scope.getApplicationsForMode = function (): Array<Dictionary<string>> {
        return $scope.legacyMode
            ? $scope.vm.loan.LegacyApplications
            : $scope.vm.loan.UladApplications;
    }

    $scope.getAssociationsForMode = function (): Array<Dictionary<string>> {
        return $scope.legacyMode
            ? $scope.vm.loan.LegacyApplicationConsumers
            : $scope.vm.loan.UladApplicationConsumers;
    }

    $scope.getAssociationIdKeyForMode = function (): string {
        return $scope.legacyMode ? 'LegacyApplicationId' : 'UladApplicationId';
    }

    $scope.getAppName = function (applicationId: string): string {
        const consumers: Array<Dictionary<any>> = $scope.getConsumersInApplication(applicationId);
        const firstConsumerName = $scope.getConsumerName(consumers[0]);

        if (consumers.length === 1) {
            return firstConsumerName;
        }
        else if (consumers.length === 2) {
            return `${firstConsumerName} & ${$scope.getConsumerName(consumers[1])}`;
        }
        else {
            return `${firstConsumerName} & ${consumers.length - 1} others`;
        }
    }

    $scope.getConsumerName = function (consumer: Dictionary<any>): string {
        return getConsumerName($scope.vm.loan.Consumers, consumer['Id']);
    }

    $scope.getConsumersInApplication = function (applicationId: string): Array<Dictionary<any>> {
        const appIdKey = $scope.getAssociationIdKeyForMode();

        const matchingAssociations: Array<Dictionary<any>> = $scope.getAssociationsForMode()
            .filter((association: Dictionary<any>) => association[appIdKey] === applicationId);

        const consumerDictionary = $scope.vm.loan.Consumers.reduce((map: Dictionary<any>, consumer: Dictionary<string>, index: number) => {
            map[consumer['Id']] = { consumer, index };
            return map;
        }, {});

        if ($scope.legacyMode) {
            // For legacy apps, the primary consumer always displays first.
            matchingAssociations.sort((association1: Dictionary<boolean>, association2: Dictionary<boolean>) => {
                if (association1['IsPrimary']) {
                    return -1;
                }

                if (association2['IsPrimary']) {
                    return 1;
                }

                return 0;
            });
        }
        else {
            // For ULAD apps, use the order returned by the endpoint, which
            // will return the consumers for a ULAD application in that application's
            // order.
            matchingAssociations.sort((association1: Dictionary<string>, association2: Dictionary<string>) => {
                const association1ConsumerIndex = consumerDictionary[association1['ConsumerId']].index;
                const association2ConsumerIndex = consumerDictionary[association2['ConsumerId']].index;

                if (association1ConsumerIndex < association2ConsumerIndex) {
                    return -1;
                }

                if (association1ConsumerIndex > association2ConsumerIndex) {
                    return 1;
                }

                return 0;
            });
        }

        return matchingAssociations.map(association => consumerDictionary[association['ConsumerId']].consumer);
    }

    $scope.onTargetAppLockChanged = function () {
        // Update UI to display calculated value.
        if (!$scope.vm.loan.sGseTargetApplicationTLckd) {
            $scope.saveData(null/*callback*/, true/*refreshOnly*/);
        }
    }

    $scope.checkForSave = function (postCallback: () => void, cancelCallback?: () => void) {
        if (isDirty()) {
            PolyConfirmSave(
                (shouldSave: boolean) => {
                    if (shouldSave) {
                        $scope.saveData(postCallback);
                    }
                    else {
                        clearDirty();
                        $timeout(postCallback);
                    }
                },
                true/*shouldShowCancel*/,
                cancelCallback);
        }
        else {
            postCallback();
        }
    }

    $scope.saveData = function (callback?: () => void, refreshOnly: boolean = false) {
        let provider = new LqbDataServiceRequestFormatProvider();

        const updatedValues = {
            sSyncUladAndLegacyApps: $scope.vm.loan.sSyncUladAndLegacyApps,
            sGseTargetApplicationT: $scope.vm.loan.sGseTargetApplicationT,
            sGseTargetApplicationTLckd: $scope.vm.loan.sGseTargetApplicationTLckd
        };

        const setArg = parameterFactory.createIndividualSetParameter(ApplicationManagementPageLoanDefinition, updatedValues);
        const setters = [setArg];

        const setArgs = parameterFactory.createAggregateSetParameter(setters);
        const getArgs = parameterFactory.createIndividualGetParameter([ApplicationManagementPageLoanDefinition]);

        provider.addRequest(LqbDataServiceRequestType.Get, getArgs);
        provider.addRequest(LqbDataServiceRequestType.Set, setArgs);

        sendWebRequest(
            refreshOnly ? LqbDataServiceOperationType.Load : LqbDataServiceOperationType.Save,
            provider.toRequest(),
            (data: Dictionary<any>) => {
                const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
                if (handler.hasError()) {
                    alert(handler.getErrorMessage());
                    return;
                }

                $scope.applyResult(data);

                if (!refreshOnly) {
                    clearDirty();

                    if ($scope.originalGseTarget != $scope.vm.loan.sGseTargetApplicationT) {
                        forceResetLeftNav();
                        $scope.originalGseTarget = $scope.vm.loan.sGseTargetApplicationT;
                    }
                }
                
                if (typeof callback === 'function') {
                    callback();
                }
            });
    }

    $scope.setPrimaryApplication = function (applicationId: string, existingApplications?: Array<Dictionary<string>>) {
        $scope.checkForSave(
            () => {
                const appType = $scope.legacyMode ? ApplicationType.LegacyApplication : ApplicationType.UladApplication;
                const setPrimaryAppArgs = parameterFactory.createSetPrimaryApplicationArgs(ApplicationManagementPageLoanDefinition, appType, applicationId);

                let provider: LqbDataServiceRequestFormatProvider = $scope.createInitProvider();
                provider.addRequest(LqbDataServiceRequestType.SetPrimaryApplication, setPrimaryAppArgs);

                sendWebRequest(
                    LqbDataServiceOperationType.Save,
                    provider.toRequest(),
                    (data: Dictionary<any>) => {
                        if ($scope.legacyMode) {
                            parent.info.f_swapPrimary(applicationId);
                        }

                        $scope.applyResult(data);
                    });
            },
            () => {
                if (existingApplications == null) {
                    return;
                }

                $timeout(() => {
                    if ($scope.legacyMode) {
                        $scope.vm.loan.LegacyApplications = existingApplications;
                    } 
                    else {
                        $scope.vm.loan.UladApplications = existingApplications;
                    }
                });
            });
    }

    $scope.swapBorrowerAndCoborrower = function (applicationId: string) {
        $scope.checkForSave(() => {
            if (!$scope.legacyMode) {
                throw 'Swap borrower and coborrower is only available for legacy applications.';
            }

            const swapArgs = parameterFactory.createSwapBorrowerAndCoborrowerArgs(ApplicationManagementPageLoanDefinition, applicationId);

            let provider: LqbDataServiceRequestFormatProvider = $scope.createInitProvider();
            provider.addRequest(LqbDataServiceRequestType.SwapBorrowerAndCoborrower, swapArgs);

            sendWebRequest(
                LqbDataServiceOperationType.Save,
                provider.toRequest(),
                (data: Dictionary<any>) => {
                    if ($scope.legacyMode) {
                        var handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
                        if (handler.hasError()) {
                            alert(handler.getErrorMessage());
                            return;
                        }

                        const loanResult = handler.getResponseByKey(LqbDataServiceRequestType.Get, 'loan');

                        const legacyApplicationConsumers: Array<Dictionary<any>> = loanResult['LegacyApplicationConsumers'];
                        const primaryLegacyAppConsumerAssociation = legacyApplicationConsumers.find(appConsumer => {
                            return appConsumer['IsPrimary'] && appConsumer['LegacyApplicationId'] === applicationId;
                        });

                        const consumers: Array<Dictionary<string>> = loanResult['Consumers'];
                        const matchingConsumer = consumers.find(consumer => consumer['Id'] === primaryLegacyAppConsumerAssociation!['ConsumerId']);
                        parent.info.f_updateApplicantDDL(`${matchingConsumer!['LastName']}, ${matchingConsumer!['FirstName']}`, applicationId);
                    }

                    $scope.applyResult(data);
                });
        });
    }

    $scope.addNewConsumer = function (applicationId: string) {
        if ($scope.legacyMode) {
            throw 'Add new consumer is only available for ULAD applications.';
        }

        const originalLegacyAppIds = $scope.vm.loan.LegacyApplications.reduce((map: Dictionary<string>, legacyApp: Dictionary<string>) => {
            map[legacyApp['Id']] = legacyApp['Id'];
            return map;
        }, {});

        $scope.checkForSave(() => {
            const addConsumerArgs = parameterFactory.createAddNewConsumerArgs(ApplicationManagementPageLoanDefinition, ApplicationType.UladApplication, applicationId);

            let provider: LqbDataServiceRequestFormatProvider = $scope.createInitProvider();
            provider.addRequest(LqbDataServiceRequestType.AddConsumer, addConsumerArgs);

            sendWebRequest(
                LqbDataServiceOperationType.Save,
                provider.toRequest(),
                (data: Dictionary<any>) => {
                    $scope.applyResult(data);

                    // If adding a new consumer also added a new legacy app, update
                    // the app dropdown.
                    const newLegacyApps = $scope.vm.loan.LegacyApplications.filter((legacyApp: Dictionary<string>) => {
                        return typeof originalLegacyAppIds[legacyApp['Id']] === 'undefined';
                    });

                    for (let i = 0; i < newLegacyApps.length; ++i) {
                        const newLegacyApp = newLegacyApps[i];
                        const newLegacyAppId = newLegacyApp['Id'];
                        parent.info.addNewAppToDropdown(newLegacyAppId);
                    }
                });
        });
    }

    $scope.deleteConsumer = function (consumerId: string, consumerName: string) {
        $scope.checkForSave(() => {
            if (!window.confirm(`Delete consumer ${consumerName}?`)) {
                return;
            }

            const appType = $scope.legacyMode ? ApplicationType.LegacyApplication : ApplicationType.UladApplication;
            const deleteConsumerArgs = parameterFactory.createDeleteConsumerArgs(ApplicationManagementPageLoanDefinition, appType, consumerId);

            let provider: LqbDataServiceRequestFormatProvider = $scope.createInitProvider();
            provider.addRequest(LqbDataServiceRequestType.DeleteConsumer, deleteConsumerArgs);

            sendWebRequest(
                LqbDataServiceOperationType.Save,
                provider.toRequest(),
                (data: Dictionary<any>) => { $scope.applyResult(data); });
        });
    }

    $scope.addNewApplication = function () {
        $scope.checkForSave(() => {
            const appType = $scope.legacyMode ? ApplicationType.LegacyApplication : ApplicationType.UladApplication;
            const addApplicationArgs = parameterFactory.createAddApplicationArgs(ApplicationManagementPageLoanDefinition, appType);

            let provider: LqbDataServiceRequestFormatProvider = $scope.createInitProvider();
            provider.addRequest(LqbDataServiceRequestType.AddApplication, addApplicationArgs);

            sendWebRequest(
                LqbDataServiceOperationType.Save,
                provider.toRequest(),
                (data: Dictionary<any>) => {
                    if ($scope.legacyMode) {
                        const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper);
                        if (handler.hasError()) {
                            alert(handler.getErrorMessage());
                            return;
                        }

                        const newAppId: string = handler.getResponseByKey(LqbDataServiceRequestType.AddApplication, 'id');
                        parent.info.addNewAppToDropdown(newAppId);
                    }

                    $scope.applyResult(data);
                });
        });
    }

    $scope.deleteApplication = function (applicationId: string) {
        if ($scope.getApplicationsForMode().length < 2) {
            return;
        }

        $scope.checkForSave(() => {
            const appName = $scope.getAppName(applicationId);
            if (!window.confirm(`Delete application ${appName}?`)) {
                return;
            }

            const appType = $scope.legacyMode ? ApplicationType.LegacyApplication : ApplicationType.UladApplication;
            const deleteApplicationArgs = parameterFactory.createDeleteApplicationArgs(ApplicationManagementPageLoanDefinition, appType, applicationId);

            let provider: LqbDataServiceRequestFormatProvider = $scope.createInitProvider();
            provider.addRequest(LqbDataServiceRequestType.DeleteApplication, deleteApplicationArgs);

            sendWebRequest(
                LqbDataServiceOperationType.Save,
                provider.toRequest(),
                (data: Dictionary<any>) => {
                    if ($scope.legacyMode) {
                        parent.info.f_removeBorrower(applicationId);
                    }

                    $scope.applyResult(data);
                });
        });
    }

    $scope.updateConsumerOrder = function ($consumerRow: JQuery, startingIndex: number) {
        const $appRow = $consumerRow.parent();

        $scope.checkForSave(
            () => {
                const applicationId = $appRow.attr('data-app-id');

                if ($scope.legacyMode) {
                    $scope.swapBorrowerAndCoborrower(applicationId);
                }
                else {
                    const consumerIds = $appRow.find('.consumer-row').toArray().map((element: HTMLElement) => {
                        return $(element).attr('data-consumer-id')!;
                    });

                    $scope.setApplicationOrder(applicationId, consumerIds);
                }
            },
            () => {
                $consumerRow.detach();
                const lastIndex = $appRow.children().length;
                
                $appRow.append($consumerRow);
                if (startingIndex < lastIndex) {
                    $appRow.children().eq(startingIndex).before($appRow.children().last());
                }
            });
    }

    $scope.setApplicationOrder = function (uladApplicationId: string, consumerIds: Array<string>) {
        const primaryConsumerId = consumerIds[0];

        const setPrimaryBorrowerArgs = parameterFactory.createSetPrimaryBorrowerArgs(ApplicationManagementPageConsumerDefinition, primaryConsumerId);
        const setOrderArgs = parameterFactory.createSetOrderParameter(ApplicationManagementPageConsumerDefinition, consumerIds, uladApplicationId);

        let provider: LqbDataServiceRequestFormatProvider = $scope.createInitProvider();
        provider.addRequest(LqbDataServiceRequestType.SetPrimaryBorrower, setPrimaryBorrowerArgs);
        provider.addRequest(LqbDataServiceRequestType.SetOrder, setOrderArgs);

        sendWebRequest(
            LqbDataServiceOperationType.Save,
            provider.toRequest(),
            (data: Dictionary<any>) => { $scope.applyResult(data); });
    }

    $scope.setupGripHandles = function () {
        $('.application-row').each((index: number, element: HTMLElement) => {
            const $element = $(element) as any;
            $element.sortable({
                items: '> .consumer-row',
                axis: 'y',
                handle: '.grippy',
                placeholder: 'grippy-placeholder',
                cursor: 'move',
                tolerance: 'pointer',
                helper: function (e: Event, tr: any) {
                    var $originals = tr.children();
                    var $helper = tr.clone();

                    // Set helper cell sizes to match the original sizes.
                    // From https://stackoverflow.com/a/1372954
                    $helper.children().each(function (index: number, element: HTMLElement) {
                        $(element).width($originals.eq(index).width());
                    });

                    return $helper;
                },
                start: function (e: Event, ui: { item: JQuery }) {
                    ui.item.data('startingIndex', ui.item.index());
                },
                stop: function (e: Event, ui: { item: JQuery }) {
                    const startingIndex = Number(ui.item.data('startingIndex'));
                    const endingIndex = ui.item.index();

                    if (startingIndex !== endingIndex) {
                        $scope.updateConsumerOrder(ui.item, startingIndex);
                    }
                }
            });
        });
    }

    $scope.applyResult = function (data: Dictionary<any>) {
        $scope.$apply(function () {
            const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
            if (handler.hasError()) {
                alert(handler.getErrorMessage());
                return;
            }

            const getResponse = handler.getResponseByKey(LqbDataServiceRequestType.Get, 'loan');
            $scope.vm.loan = Object.assign($scope.vm.loan, getResponse);

            $timeout(() => {
                $scope.setupGripHandles();
            });
        });
    }

    $scope.loadData = function (request: string) {
        sendWebRequest(
            LqbDataServiceOperationType.Load,
            request,
            (data: Dictionary<any>) => {
                $scope.applyResult(data);
                $scope.originalGseTarget = $scope.vm.loan.sGseTargetApplicationT;
            });
    }

    $scope.createInitProvider = function (): LqbDataServiceRequestFormatProvider {
        const getArgs = parameterFactory.createIndividualGetParameter([
            ApplicationManagementPageConsumerDefinition,
            ApplicationManagementPageLoanDefinition,
            LegacyApplicationDefinition,
            LegacyApplicationConsumersDefinition,
            UladApplicationDefinition,
            UladApplicationConsumersDefinition]);

        const provider = new LqbDataServiceRequestFormatProvider();
        provider.addRequest(LqbDataServiceRequestType.Get, getArgs);

        return provider;
    }

    $scope.init = function () {
        $scope.vm = new Dictionary<Dictionary<any>>();
        $scope.vm.loan = new Dictionary<any>();

        const provider: LqbDataServiceRequestFormatProvider = $scope.createInitProvider();
        $scope.loadData(provider.toRequest());
    }

    $scope.init();
}]);
