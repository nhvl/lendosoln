import LiabilityDefinition from '../../Common/LiabilityDefinition';
import ConsumerDefinition from '../../Common/ConsumerDefinition';
import ConsumerAssetDefinition from '../../Common/ConsumerAssetDefinition';
import RealPropertyLiabilitiesDefinition from '../../Common/RealPropertyLiabilitiesDefinition';
import ReoDefinition from '../../Common/ReoDefinition';
import LoanDefinition from '../../Common/LoanDefinition';

import BooleanLqbDataServiceResponseConverter from '../../../api/BooleanLqbDataServiceResponseConverter';
import UladAssociationHelper from '../../Common/UladAssociationHelper'
import Dictionary from '../../../api/Dictionary';
import EntityDefinition from '../../../api/EntityDefinition';
import LqbDataServiceRequestFormatProvider from '../../../api/LqbDataServiceRequestFormatProvider';
import LqbDataServiceRequestParameterFactory from '../../../api/LqbDataServiceRequestParameterFactory';
import LqbDataServiceResponseHandler from '../../../api/LqbDataServiceResponseHandler';
import LqbDataServiceOperationType from '../../../api/LqbDataServiceOperationType';
import LqbDataServiceRequestType from '../../../api/LqbDataServiceRequestType';
import sendWebRequest from '../../Common/Utils/sendWebRequest';

declare const ML: any;
declare function retrieveSelectedReoId(): string;
const uladAssociationHelper = new UladAssociationHelper();
const booleanValueConverter = new BooleanLqbDataServiceResponseConverter();

export class LiabilityDepository extends LiabilityDefinition {
    AssetType: string = '';
    Value: string = '';

    AccountName: string = '';
    AccountNum: string = '';
}

export class LiabilityPageRealPropertyDefinition extends ReoDefinition {
    StreetAddress: string = '';
    City: string = '';
    State: string = '';
    Zip: string = '';
}

export class LiabilityRecordPageLiabilityDefinition extends LiabilityDefinition {
    Attention: string = '';
    DebtType: string = '';

    CompanyName: string = '';
    AccountName: string = '';
    AccountNum: string = '';

    CompanyAddress: string = '';
    CompanyCity: string = '';
    CompanyState: string = '';
    CompanyZip: string = '';

    RequiresVom: string = '';

    IsSeeAttachment: boolean = false;

    VerifSentDate: string = '';
    VerifReorderedDate: string = '';
    VerifRecvDate: string = '';
    VerifExpiresDate: string = '';

    VerifSigningEmployeeId: string = '';
    VerifHasSignature: string = '';
}

export class LiabilityRecordPageLiabilitySaveDefinition extends LiabilityDefinition {
    Attention: string = '';
    VerifSentDate: string = '';
    VerifReorderedDate: string = '';
    VerifRecvDate: string = '';
    VerifExpiresDate: string = '';
    IsSeeAttachment: boolean = false;
}

let realPropertyLiabilities: any;
let realProperties: any;

const parameterFactory = new LqbDataServiceRequestParameterFactory();
function updateData(callback: (entries: any, otherData: any) => void, isSaving: boolean = false, liability: LiabilityRecordPageLiabilityDefinition | null = null, recordId: string = ML.recordId) {
    let provider = new LqbDataServiceRequestFormatProvider();

    //Save
    if (isSaving && liability != null) {
        let setters = new Array<Dictionary<any>>();
        setters.push(parameterFactory.createIndividualSetParameter(
            LiabilityRecordPageLiabilityDefinition,
            liability,
            liability.Id));
        const setArgs = parameterFactory.createAggregateSetParameter(setters);
        provider.addRequest(LqbDataServiceRequestType.Set, setArgs);
        associateRealPropertyLiabilitySetter(setters, provider, liability);
    }

    const getArgs = parameterFactory.createIndividualGetParameter([LiabilityRecordPageLiabilityDefinition, LiabilityPageRealPropertyDefinition, RealPropertyLiabilitiesDefinition]);
    provider.addRequest(LqbDataServiceRequestType.Get, getArgs);

    sendWebRequest(
        isSaving ? LqbDataServiceOperationType.Save : LqbDataServiceOperationType.Load,
        provider.toRequest(),
        (data: Dictionary<any>) => {

            var responseHandler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
            if (responseHandler.hasError()) {
                alert(responseHandler.getErrorMessage());
                return;
            }

            var getResponse = responseHandler.getResponseByKey(LqbDataServiceRequestType.Get, 'loan');
            var liabilities = getResponse.Liabilities;
            realProperties = getResponse.RealProperties;
            realPropertyLiabilities = getResponse.RealPropertyLiabilities;

            if (typeof (callback) == 'function') {
                callback(liabilities, { RealProperties: realProperties, RealPropertyLiabilities: realPropertyLiabilities });
            }
        });
}


function getLinkedReoId (liabilityId: string, realPropertyLiabilities: any) {
    var matchingAssociation = realPropertyLiabilities.find((association: Dictionary<string>) => {
        return association['LiabilityId'] === liabilityId;
    });

    return matchingAssociation == null ? null : matchingAssociation['RealPropertyId'];
}

function associateRealPropertyLiabilitySetter(setters: Array<Dictionary<any>>, provider: LqbDataServiceRequestFormatProvider, liability: LiabilityRecordPageLiabilityDefinition) {
    let realPropertyLiabilityValues = Object.values(realPropertyLiabilities) as Dictionary<string>[];
    let oldReoId = getLinkedReoId(liability['Id'], realPropertyLiabilityValues);
    var linkedReoId = retrieveSelectedReoId();

    setters.push(parameterFactory.createIndividualSetParameter(
        LiabilityRecordPageLiabilityDefinition,
        liability,
        liability['Id']));

    if (oldReoId != linkedReoId) {
        const liabilityId = liability['Id'];

        if (oldReoId != null && oldReoId != '') {
            var keys = Object.keys(realPropertyLiabilities);
            var values = Object.values(realPropertyLiabilities);
            const matchingAssociation: Dictionary<string> | undefined = realPropertyLiabilityValues.find((association: Dictionary<string>) => {
                return association['LiabilityId'] === liabilityId && association['RealPropertyId'] === oldReoId;
            });

            var id = keys[values.findIndex(association => association == matchingAssociation)];

            if (matchingAssociation != null) {
                const removeAssociationArgs = parameterFactory.createRemoveAssociationParameter(
                    RealPropertyLiabilitiesDefinition,
                    id);

                provider.addRequest(LqbDataServiceRequestType.RemoveAssociation, removeAssociationArgs);
            }
        }

        if (linkedReoId != null && linkedReoId != '') {
            const addAssociationArgs = parameterFactory.createAddAssociationParameter(
                RealPropertyLiabilitiesDefinition,
                linkedReoId,
                liabilityId);

            provider.addRequest(LqbDataServiceRequestType.AddAssociation, addAssociationArgs);
        }
    }
}

Object.assign(window, { updateData, getLinkedReoId});