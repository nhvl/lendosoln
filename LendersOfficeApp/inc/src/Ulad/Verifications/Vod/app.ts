import AssetDefinition from '../../Common/AssetDefinition';
import ConsumerDefinition from '../../Common/ConsumerDefinition';
import ConsumerAssetDefinition from '../../Common/ConsumerAssetDefinition';
import LoanDefinition from '../../Common/LoanDefinition';

import BooleanLqbDataServiceResponseConverter from '../../../api/BooleanLqbDataServiceResponseConverter';
import Dictionary from '../../../api/Dictionary';
import EntityDefinition from '../../../api/EntityDefinition';
import LqbDataServiceRequestFormatProvider from '../../../api/LqbDataServiceRequestFormatProvider';
import LqbDataServiceRequestParameterFactory from '../../../api/LqbDataServiceRequestParameterFactory';
import LqbDataServiceResponseHandler from '../../../api/LqbDataServiceResponseHandler';
import LqbDataServiceOperationType from '../../../api/LqbDataServiceOperationType';
import LqbDataServiceRequestType from '../../../api/LqbDataServiceRequestType';
import UladAssociationHelper from '../../Common/UladAssociationHelper';
import sendWebRequest from '../../Common/Utils/sendWebRequest';

declare const ML: any;
declare var gatherAssetEntity: () => AssetDefinition;
const uladAssociationHelper = new UladAssociationHelper();
const booleanValueConverter = new BooleanLqbDataServiceResponseConverter();

export class AssetDepository extends AssetDefinition {
    AssetType: string = '';
    Value: string = '';

    AccountName: string = '';
    AccountNum: string = '';
}


export class AssetRecordPageAssetDefinition extends AssetDefinition {
    Attention: string = '';
    AssetType: string = '';
    AssetTypeDesc: string = '';
    Value: string = '';

    CompanyName: string = '';
    AccountName: string = '';
    AccountNum: string = '';

    StreetAddress: string = ''
    City: string = ''
    State: string = ''
    Zip: string = ''

    IsSeeAttachment: boolean = false;
    RequiresVod: boolean = false;

    VerifSentDate: string = '';
    VerifReorderedDate: string = '';
    VerifRecvDate: string = '';
    VerifExpiresDate: string = '';

    VerifSigningEmployeeId: string = '';
    VerifHasSignature: string = '';
}

const parameterFactory = new LqbDataServiceRequestParameterFactory();
function updateData(callback: (entries: any, otherData: any) => void, isSaving: boolean = false, asset: AssetRecordPageAssetDefinition | null = null, recordId: string = ML.recordId) {
    let provider = new LqbDataServiceRequestFormatProvider();

    // To get the depository values, Ask Michael how to retrieve the filtered lista

    //Save
    if (isSaving && asset != null) {
        let setters = new Array<Dictionary<any>>();
        setters.push(parameterFactory.createIndividualSetParameter(
            AssetRecordPageAssetDefinition,
            asset,
            asset.Id));
        const setArgs = parameterFactory.createAggregateSetParameter(setters);
        provider.addRequest(LqbDataServiceRequestType.Set, setArgs);
    }

    const getArgs = parameterFactory.createIndividualGetParameter([AssetRecordPageAssetDefinition]);
    provider.addRequest(LqbDataServiceRequestType.Get, getArgs);

    sendWebRequest(
        isSaving ? LqbDataServiceOperationType.Save : LqbDataServiceOperationType.Load,
        provider.toRequest(),
        (data: Dictionary<any>) => {
            var responseHandler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
            if (responseHandler.hasError()) {
                alert(responseHandler.getErrorMessage());
                return;
            }

            var assets = responseHandler.getResponseByKey(LqbDataServiceRequestType.Get, 'loan').Assets;

            if (typeof (callback) == 'function') {
                callback(assets, null);
            }
        });
}

Object.assign(window, { updateData});