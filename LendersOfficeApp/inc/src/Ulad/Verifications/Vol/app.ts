import LiabilityDefinition from '../../Common/LiabilityDefinition';
import ConsumerDefinition from '../../Common/ConsumerDefinition';
import ConsumerAssetDefinition from '../../Common/ConsumerAssetDefinition';
import LoanDefinition from '../../Common/LoanDefinition';

import BooleanLqbDataServiceResponseConverter from '../../../api/BooleanLqbDataServiceResponseConverter';
import UladAssociationHelper from '../../Common/UladAssociationHelper'
import Dictionary from '../../../api/Dictionary';
import EntityDefinition from '../../../api/EntityDefinition';
import LqbDataServiceRequestFormatProvider from '../../../api/LqbDataServiceRequestFormatProvider';
import LqbDataServiceRequestParameterFactory from '../../../api/LqbDataServiceRequestParameterFactory';
import LqbDataServiceResponseHandler from '../../../api/LqbDataServiceResponseHandler';
import LqbDataServiceOperationType from '../../../api/LqbDataServiceOperationType';
import LqbDataServiceRequestType from '../../../api/LqbDataServiceRequestType';
import sendWebRequest from '../../Common/Utils/sendWebRequest';

declare const ML: any;
declare var gatherLiabilityEntity: () => LiabilityDefinition;
const uladAssociationHelper = new UladAssociationHelper();
const booleanValueConverter = new BooleanLqbDataServiceResponseConverter();

export class LiabilityDepository extends LiabilityDefinition {
    AssetType: string = '';
    Value: string = '';

    AccountName: string = '';
    AccountNum: string = '';
}


export class LiabilityRecordPageLiabilityDefinition extends LiabilityDefinition {
    Attention: string = '';
    Bal: string = '';
    DebtType: string = '';

    CompanyName: string = '';
    AccountName: string = '';
    AccountNum: string = '';

    CompanyAddress: string = '';
    CompanyCity: string = '';
    CompanyState: string = '';
    CompanyZip: string = '';

    RequiresVol: string = '';

    IsSeeAttachment: boolean = false;

    VerifSentDate: string = '';
    VerifReorderedDate: string = '';
    VerifRecvDate: string = '';
    VerifExpiresDate: string = '';

    VerifSigningEmployeeId: string = '';
    VerifHasSignature: string = '';
    // OwnerType: string = '';
}

const parameterFactory = new LqbDataServiceRequestParameterFactory();
function updateData(callback: (entries: any, otherData: any) => void, isSaving: boolean = false, liability: LiabilityRecordPageLiabilityDefinition | null = null, recordId: string = ML.recordId) {
    let provider = new LqbDataServiceRequestFormatProvider();

    //Save
    if (isSaving && liability != null) {
        let setters = new Array<Dictionary<any>>();
        setters.push(parameterFactory.createIndividualSetParameter(
            LiabilityRecordPageLiabilityDefinition,
            liability,
            liability.Id));
        const setArgs = parameterFactory.createAggregateSetParameter(setters);
        provider.addRequest(LqbDataServiceRequestType.Set, setArgs);
    }

    const getArgs = parameterFactory.createIndividualGetParameter([LiabilityRecordPageLiabilityDefinition]);
    provider.addRequest(LqbDataServiceRequestType.Get, getArgs);

    sendWebRequest(
        isSaving ? LqbDataServiceOperationType.Save : LqbDataServiceOperationType.Load,
        provider.toRequest(),
        (data: Dictionary<any>) => {
            var responseHandler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
            if (responseHandler.hasError()) {
                alert(responseHandler.getErrorMessage());
                return;
            }

            var liabilities = responseHandler.getResponseByKey(LqbDataServiceRequestType.Get, 'loan').Liabilities;
            if (typeof (callback) == 'function') {
                callback(liabilities, null);
            }
        });
}

Object.assign(window, { updateData});