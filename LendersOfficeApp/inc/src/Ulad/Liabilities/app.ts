import * as angular from 'angular';
import { ITimeoutService } from 'angular';

import { AllApplicationsFilter, LegacyApplicationsFilter, UladApplicationsFilter, BorrowerFilter, ApplicationLabelsByValue } from '../Common/ApplicationFilters';
import ConsumerLiabilityDefinition from '../Common/ConsumerLiabilityDefinition';
import EditOwnerPopupHelper from '../Common/EditOwnerPopupHelper';
import LegacyApplicationConsumersDefinition from '../Common/LegacyApplicationConsumersDefinition';
import LoanDefinition from '../Common/LoanDefinition';
import RealPropertyLiabilitiesDefinition from '../Common/RealPropertyLiabilitiesDefinition';
import TotalsType from '../Common/TotalsType';
import UladApplicationDefinition from '../Common/UladApplicationDefinition';
import UladApplicationConsumersDefinition from '../Common/UladApplicationConsumersDefinition';
import UladAssociationHelper from '../Common/UladAssociationHelper';

import {
    LiabilityPageLoanDefinition,
    LiabilityPageLegacyApplicationDefinition,
    LiabilityPageRealPropertyDefinition,
    LiabilityPmlAuditTrailDefinition,
    LiabilityPageLiabilityDefinition,
    LiabilityPageConsumerDefinition
} from './LiabilityDefinitions';

import BooleanLqbDataServiceResponseConverter from '../../api/BooleanLqbDataServiceResponseConverter';
import Dictionary from '../../api/Dictionary';
import EntityDefinition from '../../api/EntityDefinition';
import LqbDataServiceRequestFormatProvider from '../../api/LqbDataServiceRequestFormatProvider';
import LqbDataServiceRequestParameterFactory from '../../api/LqbDataServiceRequestParameterFactory';
import LqbDataServiceResponseHandler from '../../api/LqbDataServiceResponseHandler';
import LqbDataServiceOperationType from '../../api/LqbDataServiceOperationType';
import LqbDataServiceRequestType from '../../api/LqbDataServiceRequestType';

import getAppName from '../Common/Utils/getAppName';
import getConsumerName from '../Common/Utils/getConsumerName';
import getFullRealPropertyAddress from '../Common/Utils/getFullRealPropertyAddress';
import getLoanId from '../Common/Utils/getLoanId';
import getOwnersForNewEntity from '../Common/Utils/getOwnersForNewEntity';
import getTotalArgs from '../Common/Utils/getTotalArgs';
import normalizeSelectedApplications from '../Common/Utils/normalizeSelectedApplications';
import orderConsumerNames from '../Common/Utils/orderConsumerNames';
import sendWebRequest, { sendWebRequestByUrl } from '../Common/Utils/sendWebRequest';

declare function getQueryStringVariable(variable: string, fallbackValue?: string): string;
declare function parseMoneyFloat(str: string): number;
declare function clearDirty(): void;
declare function updateDirtyBit(): void;
declare function linkMe(href: string, extraArgs: string): void;
declare function isDirty(): boolean;
declare function _initInput(): void;
declare function PolyConfirmSave(confirmCallback: (shouldSave: boolean) => void, shouldShowCancel: boolean, cancelCallback?: () => void): void;
declare function AddToRolodex_postServiceCall(result: Dictionary<any>, args: Dictionary<any>): void;

declare var ML: { IsPmlEnabled: boolean };
declare var LQBPopup: any;
declare var VRoot: string;
declare var DebtTypeDescriptionsByValue: Dictionary<string>;

declare global {
    interface Window {
        info: {
            f_swapPrimary: (legacyAppId: string) => void;
            f_removeBorrower: (legacyAppId: string) => void;
            f_updateApplicantDDL: (consumerName: string, legacyAppId: string) => void;
            addNewAppToDropdown: (legacyAppId: string) => void;
            f_refreshInfoFromDB?: () => void;
            f_refreshInfo?: () => void;
        }
    }
}

const parameterFactory = new LqbDataServiceRequestParameterFactory();
const uladAssociationHelper = new UladAssociationHelper();
const booleanValueConverter = new BooleanLqbDataServiceResponseConverter();

angular
.module('Liabilities', [])
.controller('LiabilitiesController', ['$scope', '$timeout', function OwnersController($scope, $timeout: ITimeoutService) {
    const trid2017TargetVersion = '2';
    const formattableFieldIds = [
        'sLAmtCalc',
        'sNoteIR',
        'sTransNetCash',
        'sProdCashoutAmt',
        'Bal',
        'Pmt',
        'Rate',
        'PayoffAmtData',
        'OriginalDebtAmt',
        'MaximumBalance'
    ];

    const revolvingDebtType = '5';
    const conventionalMortgageType = '0';
    const defaultPayoffTiming = '0';

    $scope.mortgageDebtTypeValue = '3';
    $scope.helocDebtTypeValue = '11';
    $scope.purchaseType = '0';
    $scope.refinCashoutType = '2';

    $scope.applicationLabelsByValue = ApplicationLabelsByValue;

    $scope.sortKey = null;
    $scope.sortDesc = false;

    $scope.liabilityFilterModel = AllApplicationsFilter;
    $scope.selectedApplicationIdModel = null;

    $scope.selectedApplications = new Array<string>();
    $scope.selectedRecordIndex = 0;

    $scope.selectedliability = null;
    $scope.originalSelectedLiability = null;

    $scope.originalDebtConsolidationData = null;

    $scope.lienToPayoffEnabled = false;
    $scope.displayDebtsToBePaidOffRow = true;

    $scope.viewLiabilities = true;
    $scope.useAutoCalc = true;

    $scope.AssetLiaCompletedNotJointly = null;

    $scope.getFullRealPropertyAddress = getFullRealPropertyAddress;
    $scope.isDirty = isDirty;
    $scope.updateDirtyBit = updateDirtyBit;

    $scope.getFriendlyAlteredInPmlStatus = function (pmlAuditTrail: Array<Dictionary<string>>): string {
        return pmlAuditTrail && pmlAuditTrail.length !== 0 ? 'Yes' : 'No';
    }

    $scope.getFriendlyDebtTypeName = function (debtTypeValue: string): string {
        return DebtTypeDescriptionsByValue[debtTypeValue];
    }

    $scope.getFriendlyUsedInRatioType = function (debtTypeValue: string, usedInRatio: boolean): string {
        if ($scope.isMortgageDebtType(debtTypeValue)) {
            return 'See REO';
        }

        return usedInRatio ? 'Yes' : 'No';
    }

    $scope.isMortgageDebtType = function(debtType: string) {
        return debtType === $scope.mortgageDebtTypeValue || debtType === $scope.helocDebtTypeValue;
    }

    $scope.onFormatField = function (input: HTMLInputElement) {
        if (formattableFieldIds.indexOf(input.id) !== -1) {
            // Ensure the asset's value is formatted as expected,
            // which happens after the initial model update.
            $scope.$apply(() => {
                if ($scope.viewLiabilities) {
                    $scope.selectedLiability[input.id] = input.value;
                }
                else {
                    $scope.vm.loan[input.id] = input.value;
                }
            });
        }
    }

    $scope.onSmartZipcode = function () {
        // The changes to the inputs happen asynchronously and
        // will not update the model's value during the normal
        // digest cycle.
        $scope.selectedLiability['CompanyCity'] = (document.getElementById('City') as HTMLInputElement).value;
        $scope.selectedLiability['CompanyState'] = $('#State').val() as string;
    }

    $scope.onDateFormat = function (input: HTMLInputElement) {
        $scope.selectedLiability[input.id] = input.value;
    }

    $scope.pickFromContacts = function () {
        const url = `${VRoot}/los/RolodexList.aspx?type=null&loanid=${getLoanId()}&IsFeeContactPicker=false&isCurrentAgentOnly=null`;
        const popupSettings = {
            onReturn: $scope.onPickFromContacts,
        };

        LQBPopup.Show(url, popupSettings);
    }

    $scope.onPickFromContacts = function (returnArgs: Dictionary<any>) {
        const ok: boolean = returnArgs['OK'];
        if (!ok) {
            return;
        }

        $scope.$apply(() => {
            $scope.selectedLiability.CompanyName = returnArgs['AgentCompanyName'];
            $scope.selectedLiability.CompanyAddress = returnArgs['CompanyStreetAddr'];
            $scope.selectedLiability.CompanyCity = returnArgs['CompanyCity'];
            $scope.selectedLiability.CompanyState = returnArgs['CompanyState'];
            $scope.selectedLiability.CompanyZip = returnArgs['CompanyZip'];
            $scope.selectedLiability.CompanyPhone = returnArgs['PhoneOfCompany'];
            $scope.selectedLiability.CompanyFax = returnArgs['FaxOfCompany'];

            updateDirtyBit();
        });
    }

    $scope.addToContacts = function () {
        $scope.checkForSave(() => {
            const url = `${VRoot}/common/UtilitiesService.aspx?method=AddToRolodex`;

            const args = {
                AddCommand: 'AddIfUnique',
                AgentCompanyName: $scope.selectedLiability.CompanyName,
                AgentStreetAddr: $scope.selectedLiability.CompanyAddress,
                AgentCity: $scope.selectedLiability.CompanyCity,
                AgentState: $scope.selectedLiability.CompanyState,
                AgentZip: $scope.selectedLiability.CompanyZip,
                PhoneOfCompany: $scope.selectedLiability.CompanyPhone,
                FaxOfCompany: $scope.selectedLiability.CompanyFax
            };

            sendWebRequestByUrl(
                url,
                JSON.stringify(args),
                true/*displayOverlay*/,
                (result: Dictionary<any>) => {
                    // JSON POSTs send the result in the 'd' variable,
                    // but the legacy rolodex expects a 'value' key.
                    result['value'] = result['d'];
                    AddToRolodex_postServiceCall(result, args);
                });
        });
    }

    $scope.orderCredit = function () {
        window.location.href = `${VRoot}/newlos/Services/OrderCredit.aspx?loanId=${getLoanId()}&islead=${$scope.vm.loan.sIsStatusLead}`;
    }

    $scope.viewCredit = function () {
        if ($scope.liabilityFilterModel == LegacyApplicationsFilter) {
            window.open(
                `${VRoot}/newlos/services/ViewCreditFrame.aspx?loanid=${getLoanId()}&applicationid=${$scope.selectedApplicationIdModel}&islead=${$scope.vm.loan.sIsStatusLead}`,
                'report',
                'toolbar=no,menubar=no,location=no,status=no,resizable=yes');

            return;
        }

        if ($scope.liabilityFilterModel == BorrowerFilter) {
            const consumerId: string = $scope.selectedApplicationIdModel;
            const legacyAppAssociation = $scope.vm.loan.LegacyApplicationConsumers.find((association: Dictionary<string>) => association['ConsumerId'] === consumerId);
            const legacyAppId = legacyAppAssociation['LegacyApplicationId'];

            window.open(
                `${VRoot}/newlos/services/ViewCreditFrame.aspx?loanid=${getLoanId()}&applicationid=${legacyAppId}&islead=${$scope.vm.loan.sIsStatusLead}`,
                'report',
                'toolbar=no,menubar=no,location=no,status=no,resizable=yes');

            return;
        }
    }

    $scope.refreshCalculation = function (forceReload: boolean = false) {
        if ($scope.viewLiabilities || $scope.useAutoCalc || forceReload) {
            // Allow other scripts to finish running before calling
            // for a data refresh, e.g. money formatting.
            $timeout(() => {
                $scope.saveData(null/*callback*/, true/*refreshOnly*/);
            });
        }
    }

    $scope.backupDebtConsolidationData = function () {
        $scope.originalDebtConsolidationData = Object.assign(new Dictionary(), $scope.vm.loan);
    }

    $scope.revertLiabilityChanges = function (backupLiability: Dictionary<any>) {
        if ($scope.vm.loan == null || $scope.vm.loan.Liabilities == null) {
            return;
        }

        const backupId = backupLiability['Id'];
        for (let i = 0; i < $scope.vm.loan.Liabilities.length; ++i) {
            const liability = $scope.vm.loan.Liabilities[i];

            if (liability['Id'] === backupId) {
                $scope.vm.loan.Liabilities[i] = backupLiability;
                return;
            }
        }
    }

    $scope.revertDebtConsolidationChanges = function() {
        $scope.vm.loan = Object.assign(new Dictionary(), $scope.originalDebtConsolidationData);
    }

    $scope.sortByOwner = function (liability1: Dictionary<any>, liability2: Dictionary<any>, direction: number): number {
        const firstAssetOwners: Array<Dictionary<any>> = liability1['Owners'];
        const secondAssetOwners: Array<Dictionary<any>> = liability2['Owners'];

        const firstAssetPrimaryOwner = firstAssetOwners[0];
        const secondAssetPrimaryOwner = secondAssetOwners[0];

        const firstAssetPrimaryOwnerName = $scope.getConsumerName(firstAssetPrimaryOwner['ConsumerId']);
        const secondAssetPrimaryOwnerName = $scope.getConsumerName(secondAssetPrimaryOwner['ConsumerId']);

        return direction * firstAssetPrimaryOwnerName.localeCompare(secondAssetPrimaryOwnerName);
    }

    $scope.sortByDebtType = function (liability1: Dictionary<any>, liability2: Dictionary<any>, direction: number) : number {
        const firstLiabilityDebtType = liability1['DebtType'];
        const secondLiabilityDebtType = liability2['DebtType'];

        if (firstLiabilityDebtType == null || firstLiabilityDebtType.trim().length === 0) {
            if (secondLiabilityDebtType == null || secondLiabilityDebtType.trim().length === 0) {
                return 0;
            }

            return direction * -1;
        }

        if (secondLiabilityDebtType == null || secondLiabilityDebtType.trim().length === 0) {
            if (firstLiabilityDebtType == null || firstLiabilityDebtType.trim().length === 0) {
                return 0;
            }

            return direction * 1;
        }

        const firstDebtTypeDescription = DebtTypeDescriptionsByValue[firstLiabilityDebtType];
        const secondDebtTypeDescription = DebtTypeDescriptionsByValue[secondLiabilityDebtType];

        return direction * firstDebtTypeDescription.localeCompare(secondDebtTypeDescription);
    }

    $scope.sortByCompanyName = function (liability1: Dictionary<any>, liability2: Dictionary<any>, direction: number): number {
        const firstLiabilityCompanyName = liability1['CompanyName'];
        const secondLiabilityCompanyName = liability2['CompanyName'];

        if (firstLiabilityCompanyName == null || firstLiabilityCompanyName.trim().length === 0) {
            if (secondLiabilityCompanyName == null || secondLiabilityCompanyName.trim().length === 0) {
                return 0;
            }

            return direction * -1;
        }

        if (secondLiabilityCompanyName == null || secondLiabilityCompanyName.trim().length === 0) {
            if (firstLiabilityCompanyName == null || firstLiabilityCompanyName.trim().length === 0) {
                return 0;
            }

            return direction * 1;
        }

        return direction * firstLiabilityCompanyName.localeCompare(secondLiabilityCompanyName);
    }

    $scope.sortByMoneyField = function (liability1: Dictionary<any>, liability2: Dictionary<any>, direction: number): number {
        let firstLiabilityValue = liability1[$scope.sortKey];
        let secondLiabilityValue = liability2[$scope.sortKey];

        if (firstLiabilityValue == null || firstLiabilityValue.trim().length === 0) {
            firstLiabilityValue = Number.MIN_SAFE_INTEGER.toString();
        }

        if (secondLiabilityValue == null || secondLiabilityValue.trim().length === 0) {
            secondLiabilityValue = Number.MIN_SAFE_INTEGER.toString();
        }

        const firstAssetValueParsed = parseMoneyFloat(firstLiabilityValue);
        const secondAssetValueParsed = parseMoneyFloat(secondLiabilityValue);

        if (firstAssetValueParsed === secondAssetValueParsed) {
            return 0;
        }

        return direction * (firstAssetValueParsed > secondAssetValueParsed ? -1 : 1);
    }

    $scope.sortByBoolField = function (liability1: Dictionary<any>, liability2: Dictionary<any>, direction: number): number {
        const firstLiabilityBit = liability1[$scope.sortKey];
        const secondLiabilityBit = liability2[$scope.sortKey];

        if (firstLiabilityBit == null || firstLiabilityBit == '') {
            if (secondLiabilityBit == null || secondLiabilityBit == '') {
                return 0;
            }

            return direction * -1;
        }

        if (secondLiabilityBit == null || secondLiabilityBit == '') {
            if (firstLiabilityBit == null || firstLiabilityBit == '') {
                return 0;
            }

            return direction * 1;
        }

        const firstLiabilityBitString = firstLiabilityBit ? 'Yes' : 'No';
        const secondLiabilityBitString = secondLiabilityBit ? 'Yes' : 'No';
        return direction * firstLiabilityBitString.localeCompare(secondLiabilityBitString);
    }

    $scope.sortLiabilities = function (liability1: Dictionary<any>, liability2: Dictionary<any>): number {
        const direction = $scope.sortDesc ? -1 : 1;

        switch ($scope.sortKey) {
            case 'Owner':
                return $scope.sortByOwner(liability1, liability2, direction);

            case 'PmlAuditTrail':
                const firstLiabilityFriendlyStatus = $scope.getFriendlyAlteredInPmlStatus(liability1['PmlAuditTrail']);
                const secondLiabilityFriendlyStatus = $scope.getFriendlyAlteredInPmlStatus(liability2['PmlAuditTrail']);
                return direction * firstLiabilityFriendlyStatus.localeCompare(secondLiabilityFriendlyStatus);

            case 'DebtType':
                return $scope.sortByDebtType(liability1, liability2, direction);

            case 'CompanyName':
                return $scope.sortByCompanyName(liability1, liability2, direction);

            case 'Bal':
            case 'Pmt':
                return $scope.sortByMoneyField(liability1, liability2, direction);

            case 'WillBePdOff':
            case 'UsedInRatioData':
                return $scope.sortByBoolField(liability1, liability2, direction);

            default:
                throw 'Unhandled sort key ' + $scope.sortKey;
        }
    }

    $scope.orderBy = function (key: string) {
        $scope.checkForSave(() => {
            if (key === $scope.sortKey) {
                $scope.sortDesc = !$scope.sortDesc;
            }
            else {
                $scope.sortKey = key;
                $scope.sortDesc = false;
            }

            if ($scope.vm.loan == null ||
                $scope.vm.loan.Liabilities == null ||
                $scope.vm.loan.Liabilities.length === 0) {
                return;
            }

            $scope.vm.loan.Liabilities.sort($scope.sortLiabilities);
            $scope.setRecordOrder($scope.vm.loan.Liabilities);

            // Update the selected record index to account for the new
            // order based on the sorting result.
            const selectedLiabilityId = $scope.selectedLiability['Id'];
            $scope.selectedRecordIndex = $scope.vm.loan.Liabilities.findIndex((liability: Dictionary<any>) => {
                return liability['Id'] == selectedLiabilityId;
            });
        });
    }

    $scope.getLinkedReoId = function (liabilityId: string) {
        var matchingAssociation = $scope.vm.loan.RealPropertyLiabilities.find((association: Dictionary<string>) => {
            return association['LiabilityId'] === liabilityId;
        });

        return matchingAssociation == null ? null : matchingAssociation['RealPropertyId'];
    }

    $scope.setSelectedLiability = function (liability: Dictionary<any>) {
        liability['LinkedReoId'] = $scope.getLinkedReoId(liability['Id']);

        if (liability['PayoffTimingData'] == null || liability['PayoffTimingData'] == '') {
            // Default to blank to match old liabilities editor
            liability['PayoffTimingData'] = defaultPayoffTiming;
        }

        if ($scope.isMortgageDebtType(liability['DebtType']) &&
            (liability['MortgageType'] == null || liability['MortgageType'] == '')) {
            // Default mortgage type to 'Conventional' to match the dropdown's default
            // in other UI locations like the loan info page.
            liability['MortgageType'] = conventionalMortgageType;
        }

        const booleanFields = [
            'WillBePdOff',
            'PayoffAmtLocked',
            'PayoffTimingLockedData',
            'UsedInRatioData',
            'IsPiggyBack',
            'ExcludeFromUw',
            'IncludeInReposession',
            'IncludeInBk',
            'IncludeInFc',
            'IsMtgFhaInsured',
            'IsForAuto'
        ];

        for (const boolField of booleanFields) {
            if (liability[boolField] == null || liability[boolField] == '') {
                // Default to false to match old liabilities editor
                liability[boolField] = false;
            }
        }

        $scope.selectedLiability = liability;

        $scope.onWillBePdOffChanged(false/*refreshCalculation*/);
        $scope.onExcludeFromUwChanged(false/*refreshCalculation*/);

        const backupLiability: Dictionary<any> = Object.assign(new Dictionary<any>(), liability);
        $scope.originalSelectedLiability = backupLiability;
    }

    $scope.checkForSave = function (postCallback: (wasSaved?: boolean) => void, cancelCallback?: () => void, updateTotalsOnSave: boolean = true) {
        if (isDirty()) {
            PolyConfirmSave(
                (shouldSave: boolean) => {
                    if (shouldSave) {
                        $scope.saveData(() => { postCallback(true); }, false/*refreshOnly*/, updateTotalsOnSave);
                    }
                    else {
                        clearDirty();

                        const revertDebtConsolidation = $scope.originalDebtConsolidationData != null;
                        $timeout(() => {
                            if (revertDebtConsolidation) {
                                $scope.revertDebtConsolidationChanges();
                                $scope.originalDebtConsolidationData = null;
                            }
                            else {
                                $scope.revertLiabilityChanges($scope.originalSelectedLiability);
                                $scope.setSelectedLiability($scope.originalSelectedLiability);
                            }
                            
                            $scope.refreshCalculation();
                            postCallback(false);
                        });
                    }
                },
                true/*shouldShowCancel*/,
                () => {
                    if (cancelCallback != null) {
                        $timeout(cancelCallback);
                    }
                });
        }
        else {
            postCallback(false);
        }
    }

    $scope.moveToPreviousRecord = function () {
        if ($scope.vm.loan.Liabilities == null || $scope.vm.loan.Liabilities.length === 0) {
            return;
        }

        $scope.checkForSave(() => {
            const startIndex = Math.max($scope.selectedRecordIndex - 1, 0);
            let nextIndex = -1;

            for (let i = startIndex; i >= 0; --i) {
                const liabilityAtIndex = $scope.vm.loan.Liabilities[i];
                if ($scope.liabilityMatchesFilter(liabilityAtIndex)) {
                    nextIndex = i;
                    break;
                }
            }

            if (nextIndex >= 0) {
                $scope.selectedRecordIndex = nextIndex;
                $scope.setSelectedLiability($scope.vm.loan.Liabilities[$scope.selectedRecordIndex]);
            }
            else if ($scope.selectedRecordIndex === 0) {
                $scope.moveToNextRecord();
            }
        });
    }

    $scope.moveToNextRecord = function () {
        if ($scope.vm.loan.Liabilities == null || $scope.vm.loan.Liabilities.length === 0) {
            return;
        }

        $scope.checkForSave(() => {
            const startIndex = Math.min($scope.selectedRecordIndex + 1, $scope.vm.loan.Liabilities.length - 1);
            let nextIndex = -1;

            for (let i = startIndex; i < $scope.vm.loan.Liabilities.length; ++i) {
                const liabilityAtIndex = $scope.vm.loan.Liabilities[i];
                if ($scope.liabilityMatchesFilter(liabilityAtIndex)) {
                    nextIndex = i;
                    break;
                }
            }

            if (nextIndex >= 0) {
                $scope.selectedRecordIndex = nextIndex;
                $scope.setSelectedLiability($scope.vm.loan.Liabilities[$scope.selectedRecordIndex]);
            }
        });
    }

    $scope.navigateToVom = function() {
        $scope.checkForSave(() => {
            if ($scope.selectedLiability['RequiresVom']) {
                linkMe(VRoot + '/newlos/Verifications/VOMRecord.aspx', 'isUlad=1&recordid=' + $scope.selectedLiability['Id']);
            }
            else {
                LQBPopup.ShowString('The liability does not require a verification of mortgage.', { height: 150, width: 300} );
            }
        }, null, false);
    }

    $scope.navigateToVol = function() {
        $scope.checkForSave(() => {
            if ($scope.selectedLiability['RequiresVol']) {
                linkMe(VRoot + '/newlos/Verifications/VOLRecord.aspx', 'isUlad=1&recordid=' + $scope.selectedLiability['Id']);
            }
            else {
                LQBPopup.ShowString('The liability does not require a verification of loan.', { height: 150, width: 300} );
            }
        }, null, false);
    }

    $scope.selectLiability = function (liability: Dictionary<string>) {
        $scope.checkForSave(() => {
            const liabilityId = liability['Id'];

            for (let i = 0; i < $scope.vm.loan.Liabilities.length; ++i) {
                const liabilityAtIndex = $scope.vm.loan.Liabilities[i];

                if (liabilityId === liabilityAtIndex['Id']) {
                    $scope.selectedRecordIndex = i;
                    $scope.setSelectedLiability($scope.vm.loan.Liabilities[$scope.selectedRecordIndex]);
                    break;
                }
            }
        });
    }

    $scope.addBalPmtToRealProperty = function () {
        const realPropertyId = $scope.selectedLiability['LinkedReoId'];
        if (realPropertyId == null || realPropertyId == '') {
            return;
        }

        $scope.checkForSave(() => {
            const addBalPmtToRealPropertyArgs = parameterFactory.createAddBalPmtToRealPropertyArgs(
                RealPropertyLiabilitiesDefinition,
                $scope.selectedLiability['Id'],
                realPropertyId);

            let provider = new LqbDataServiceRequestFormatProvider();
            provider.addRequest(LqbDataServiceRequestType.AddBalPmtToRealProperty, addBalPmtToRealPropertyArgs);

            sendWebRequest(
                LqbDataServiceOperationType.Save,
                provider.toRequest(),
                (data: Dictionary<any>) => {
                    var responseHandler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
                    if (responseHandler.hasError()) {
                        alert(responseHandler.getErrorMessage());
                    }
                    else {
                        alert('The balance and payment information has been added to the REO record.');
                    }
                });
        });
    }

    $scope.insertRecord = function () {
        $scope.checkForSave(() => {
            const insertIndex = Math.max(0, $scope.selectedRecordIndex);
            $scope.addLiability(insertIndex);
        });
    }

    $scope.addRecord = function () {
        $scope.checkForSave(() => {
            $scope.addLiability();
        });
    }

    $scope.addLiability = function (insertIndex?: number) {
        const hadActiveLiability = $scope.vm.loan.Liabilities.filter($scope.liabilityMatchesFilter).length !== 0;

        const owners = getOwnersForNewEntity(
            $scope.selectedApplications,
            $scope.selectedApplicationIdModel,
            $scope.vm.loan.UladApplications,
            $scope.vm.loan.UladApplicationConsumers);

        const primaryOwner = owners.find((owner: Dictionary<string | boolean>) => owner['IsPrimary'] as boolean);
        if (primaryOwner == null) {
            throw 'Unable to add primary ownership for new liability.';
        }

        const primaryConsumerId = primaryOwner['ConsumerId'] as string;

        const additionalOwners = owners.filter((owner: Dictionary<string | boolean>) => owner['ConsumerId'] !== primaryOwner['ConsumerId']);
        const additionalOwnerIds = additionalOwners.map((owner: Dictionary<string | boolean>) => owner['ConsumerId'] as string);

        const newOwners = [primaryOwner, ...additionalOwners];
        const accountName = newOwners.map(owner => getConsumerName($scope.vm.loan.Consumers, owner['ConsumerId'] as string)).join(' & ');
        const args = parameterFactory.createAddParameter(
            LiabilityPageLiabilityDefinition,
            primaryConsumerId,
            additionalOwnerIds,
            insertIndex,
            { AccountName: accountName, UsedInRatioData: 'True' });

        let provider = new LqbDataServiceRequestFormatProvider();
        provider.addRequest(LqbDataServiceRequestType.Add, args);

        sendWebRequest(
            LqbDataServiceOperationType.Save,
            provider.toRequest(),
            (data: Dictionary<any>) => {
                const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
                if (handler.hasError()) {
                    alert(handler.getErrorMessage());
                    return;
                }

                $scope.$apply(function () {
                    const response = handler.getResponseByKey(LqbDataServiceRequestType.Add, 'loan');

                    var liability = new Dictionary<any>();
                    liability['Id'] = response['id'];
                    liability['Owners'] = [primaryOwner, ...additionalOwners];
                    liability['AccountName'] = accountName;

                    // Default "Debt should be included in ratios" to checked
                    // for parity with the old UI when adding a new liability.
                    liability['UsedInRatioData'] = true;

                    if (insertIndex == null) {
                        const liabilitiesLength = $scope.vm.loan.Liabilities.push(liability);
                        $scope.selectedRecordIndex = liabilitiesLength - 1;
                    }
                    else {
                        $scope.vm.loan.Liabilities.splice(insertIndex, 0/*No entries before: only adding the liability*/, liability);
                        $scope.selectedRecordIndex = insertIndex;
                    }

                    orderConsumerNames([liability], $scope.getConsumerName);
                    $scope.setSelectedLiability(liability);

                    if (!hadActiveLiability) {
                        // No inputs are in the DOM until there is an active liability.
                        // Once we've added an liability, initialize the new inputs.
                        window.setTimeout(_initInput, 0);
                    }
                });
            });
    }

    $scope.deleteRecord = function () {
        const recordId = $scope.selectedLiability['Id'];

        const deleteArgs = parameterFactory.createRemoveParameter(LiabilityPageLiabilityDefinition, recordId);
        const getLiabilityTotalArgs = $scope.getLiabilityTotalArgs();

        let provider = new LqbDataServiceRequestFormatProvider();
        provider.addRequest(LqbDataServiceRequestType.Remove, deleteArgs);
        provider.addRequest(LqbDataServiceRequestType.GetLiabilityTotals, getLiabilityTotalArgs);

        sendWebRequest(
            LqbDataServiceOperationType.Save,
            provider.toRequest(),
            (data: Dictionary<any>) => {
                const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
                if (handler.hasError()) {
                    alert(handler.getErrorMessage());
                    return;
                }

                $scope.$apply(function () {
                    clearDirty();
                    $scope.vm.liabilityTotals = handler.getResponseByKey(LqbDataServiceRequestType.GetLiabilityTotals, 'loan');

                    if ($scope.vm.loan.Liabilities.length === 1) {
                        $scope.vm.loan.Liabilities = new Array<Dictionary<string>>();
                        $scope.selectedRecordIndex = 0;
                        $scope.selectedLiability = null;
                    }
                    else {
                        $scope.vm.loan.Liabilities.splice($scope.selectedRecordIndex, 1);
                        $scope.moveToPreviousRecord();
                    }
                });
            });
    }

    $scope.moveRecordUp = function () {
        if ($scope.selectedRecordIndex === 0) {
            return;
        }

        $scope.checkForSave(() => {
            const liabilities = $scope.vm.loan.Liabilities;
            const temp = liabilities[$scope.selectedRecordIndex];
            liabilities[$scope.selectedRecordIndex] = liabilities[$scope.selectedRecordIndex - 1];
            liabilities[$scope.selectedRecordIndex - 1] = temp;

            $scope.setRecordOrder(liabilities, () => { $scope.selectedRecordIndex-- });
        });
    }

    $scope.moveRecordDown = function () {
        if ($scope.selectedRecordIndex === $scope.vm.loan.Liabilities.length - 1) {
            return;
        }

        $scope.checkForSave(() => {
            const liabilities = $scope.vm.loan.Liabilities;
            const temp = liabilities[$scope.selectedRecordIndex];
            liabilities[$scope.selectedRecordIndex] = liabilities[$scope.selectedRecordIndex + 1];
            liabilities[$scope.selectedRecordIndex + 1] = temp;

            $scope.setRecordOrder(liabilities, () => { $scope.selectedRecordIndex++ });
        });
    }

    $scope.setRecordOrder = function (liabilities: Array<Dictionary<string>>, callback?: () => void) {
        const liabilityIds = liabilities.map((liability: Dictionary<string>) => liability['Id']);
        const args = parameterFactory.createSetOrderParameter(LiabilityPageLiabilityDefinition, liabilityIds);

        let provider = new LqbDataServiceRequestFormatProvider();
        provider.addRequest(LqbDataServiceRequestType.SetOrder, args);

        sendWebRequest(
            LqbDataServiceOperationType.Save,
            provider.toRequest(),
            (data: Dictionary<any>) => {
                const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
                if (handler.hasError()) {
                    alert(handler.getErrorMessage());
                    return;
                }

                $scope.$apply(() => {
                    $scope.vm.loan.Liabilities = liabilities;

                    if (typeof callback === 'function') {
                        callback();
                    }
                });
            });
    }

    $scope.saveData = function (callback: () => void, refreshOnly: boolean = false, updateTotalsOnSave: boolean = true) {
        let provider = new LqbDataServiceRequestFormatProvider();

        let getters = new Array<Dictionary<any>>();
        let setters = new Array<Dictionary<any>>();

        let shouldUpdateLiabilityAssociations = false;

        if ($scope.viewLiabilities) {
            if ($scope.selectedLiability != null) {
                setters.push(parameterFactory.createIndividualSetParameter(
                    LiabilityPageLiabilityDefinition,
                    $scope.selectedLiability,
                    $scope.selectedLiability['Id']));

                if (!refreshOnly && $scope.originalSelectedLiability['LinkedReoId'] != $scope.selectedLiability['LinkedReoId']) {
                    const liabilityId = $scope.selectedLiability['Id'];

                    if ($scope.originalSelectedLiability['LinkedReoId'] != null && $scope.originalSelectedLiability['LinkedReoId'] != '') {
                        const matchingAssociation: Dictionary<string> = $scope.vm.loan.RealPropertyLiabilities.find((association: Dictionary<string>) => {
                            return association['LiabilityId'] === liabilityId && association['RealPropertyId'] === $scope.originalSelectedLiability['LinkedReoId'];
                        });

                        const removeAssociationArgs = parameterFactory.createRemoveAssociationParameter(
                            RealPropertyLiabilitiesDefinition,
                            matchingAssociation['Id']);

                        provider.addRequest(LqbDataServiceRequestType.RemoveAssociation, removeAssociationArgs);
                        shouldUpdateLiabilityAssociations = true;
                    }

                    if ($scope.selectedLiability['LinkedReoId'] != null && $scope.selectedLiability['LinkedReoId'] != '') {
                        const addAssociationArgs = parameterFactory.createAddAssociationParameter(
                            RealPropertyLiabilitiesDefinition,
                            $scope.selectedLiability['LinkedReoId'],
                            liabilityId);

                        provider.addRequest(LqbDataServiceRequestType.AddAssociation, addAssociationArgs);
                        shouldUpdateLiabilityAssociations = true;
                    }
                }
            }
        }
        else {
            setters.push(parameterFactory.createIndividualSetParameter(
                LiabilityPageLoanDefinition,
                {
                    sLPurposeT: $scope.vm.loan.sLPurposeT,
                    sLAmtLckd: $scope.vm.loan.sLAmtLckd,
                    sLAmtCalc: $scope.vm.loan.sLAmtCalc,
                    sNoteIR: $scope.vm.loan.sNoteIR,
                    sTransNetCashLckd: $scope.vm.loan.sTransNetCashLckd,
                    sTransNetCash: $scope.vm.loan.sTransNetCash,
                    sTerm: $scope.vm.loan.sTerm,
                    sDue: $scope.vm.loan.sDue,
                    sIOnlyMon: $scope.vm.loan.sIOnlyMon,
                    sProdCashoutAmt: $scope.vm.loan.sProdCashoutAmt
                }));

            for (const liability of $scope.vm.loan.Liabilities) {
                let liabilityValues: Dictionary<any> = {
                    WillBePdOff: liability['WillBePdOff']
                };

                if (!$scope.isMortgageDebtType(liability.DebtType)) {
                    liabilityValues['UsedInRatioData'] = liability['UsedInRatioData'];
                }

                setters.push(parameterFactory.createIndividualSetParameter(LiabilityPageLiabilityDefinition, liabilityValues, liability['Id']));
            }
        }

        if ($scope.liabilityFilterModel == LegacyApplicationsFilter && $scope.vm.AssetLiaCompletedNotJointly != null) {
            setters.push(parameterFactory.createIndividualSetParameter(
                LiabilityPageLegacyApplicationDefinition,
                { AssetLiaCompletedNotJointly: $scope.vm.AssetLiaCompletedNotJointly },
                $scope.selectedApplicationIdModel));

            const getArgs = parameterFactory.createIndividualGetParameter([LiabilityPageLegacyApplicationDefinition]);
            getters.push(getArgs);
        }

        if ($scope.lienToPayoffEnabled) {
            setters.push(parameterFactory.createIndividualSetParameter(
                LiabilityPageLoanDefinition,
                { sLienToPayoffTotDebt: $scope.vm.loan.sLienToPayoffTotDebt }));
        }

        if (shouldUpdateLiabilityAssociations) {
            const getAssociationArgs = parameterFactory.createIndividualGetParameter([RealPropertyLiabilitiesDefinition]);
            getters.push(getAssociationArgs);
        }

        const setArgs = parameterFactory.createAggregateSetParameter(setters);
        provider.addRequest(LqbDataServiceRequestType.Set, setArgs);

        if (updateTotalsOnSave) {
            const getLiabilityTotalArgs = $scope.getLiabilityTotalArgs();
            provider.addRequest(LqbDataServiceRequestType.GetLiabilityTotals, getLiabilityTotalArgs);
        }

        if (refreshOnly) {
            const refreshDataArgs = $scope.viewLiabilities
                ? parameterFactory.createIndividualGetParameter([LiabilityPageLiabilityDefinition], $scope.selectedLiability['Id'])
                : parameterFactory.createIndividualGetParameter([LiabilityPageLoanDefinition]);

            getters.push(refreshDataArgs);
        }

        if (getters.length !== 0) {
            const aggregateGetter = parameterFactory.createAggregateGetParameter(getters);
            provider.addRequest(LqbDataServiceRequestType.Get, aggregateGetter);
        }

        sendWebRequest(
            refreshOnly ? LqbDataServiceOperationType.LoadNoOverlay : LqbDataServiceOperationType.Save,
            provider.toRequest(),
            (data: Dictionary<any>) => {
                const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
                if (handler.hasError()) {
                    alert(handler.getErrorMessage());
                    return;
                }

                $scope.$apply(() => {
                    if (updateTotalsOnSave) {
                        $scope.vm.liabilityTotals = handler.getResponseByKey(LqbDataServiceRequestType.GetLiabilityTotals, 'loan');
                    }

                    if ($scope.liabilityFilterModel == LegacyApplicationsFilter && $scope.vm.AssetLiaCompletedNotJointly != null) {
                        const getResponse = handler.getResponseByKey(LqbDataServiceRequestType.Get, 'loan');
                        $scope.vm.loan.LegacyApplications = getResponse['LegacyApplications'];
                    }

                    if (shouldUpdateLiabilityAssociations) {
                        const getAssociationResponse = handler.getResponseByKey(LqbDataServiceRequestType.Get, 'loan');
                        $scope.vm.loan.RealPropertyLiabilities = getAssociationResponse['RealPropertyLiabilities'];
                    }

                    if (refreshOnly) {
                        const getResponse = handler.getResponseByKey(LqbDataServiceRequestType.Get, 'loan');

                        if ($scope.viewLiabilities) {
                            const liabilityArray = getResponse['Liabilities'];
                            const updatedLiability = liabilityArray[0];

                            // Avoid using Object.assign since Angular adds tracking
                            // metadata for the selected liability as part of applying
                            // the model to the editor.
                            for (const key of Object.keys(updatedLiability)) {
                                $scope.selectedLiability[key] = updatedLiability[key];
                            }
                        }
                        else {
                            // Object.assign doesn't work here because the 
                            // keys are already defined in the model.
                            for (const key of Object.keys(getResponse)) {
                                $scope.vm.loan[key] = getResponse[key];
                            }
                        }
                    }
                    else {
                        clearDirty();
                        $scope.originalSelectedLiability = Object.assign(new Dictionary<any>(), $scope.selectedLiability);

                        if (!$scope.viewLiabilities) {
                            if (typeof parent.info.f_refreshInfoFromDB === 'function') {
                                parent.info.f_refreshInfoFromDB();
                            }
                            else if (typeof parent.info.f_refreshInfo === 'function') {
                                parent.info.f_refreshInfo();
                            }
                        }
                    }

                    if (typeof callback === 'function') {
                        callback();
                    }
                });
            });
    }

    $scope.editLiabilityOwners = function (liability: Dictionary<Array<Dictionary<string>>>) {
        $scope.checkForSave(() => {
            const selectedOwners = liability['Owners'];

            var helper = new EditOwnerPopupHelper();
            helper.storeEntityOwnerIdsToCache(selectedOwners, (result: Dictionary<any>) => {
                if (result.error) {
                    alert(result.UserMessage);
                    return;
                }

                const cacheKey = result.value['CacheKey'];
                const url = `${VRoot}/newlos/Ulad/EditOwner.aspx?loanid=${getLoanId()}&id=${liability['Id']}&t=Liability&s=${cacheKey}`;
                const settings = {
                    onReturn: $scope.onOwnerPopupClose
                };
                LQBPopup.Show(url, settings);
            });
        });
    }

    $scope.onOwnerPopupClose = function (returnArgs: Dictionary<any>) {
        const primaryOwner: Dictionary<string> = returnArgs['PrimaryOwner'];
        const additionalOwners: Array<Dictionary<string>> = returnArgs['AdditionalOwners'];

        const getLiabilityTotalArgs = $scope.getLiabilityTotalArgs();
        const setOwnerArgs = parameterFactory.createSetOwnerParameter(
            ConsumerLiabilityDefinition,
            $scope.selectedLiability['Id'],
            primaryOwner['Id'],
            additionalOwners.map((owner: Dictionary<string>) => owner['Id']));

        const newOwners = [primaryOwner, ...additionalOwners];
        const accountName = newOwners.map(owner => `${owner['FirstName']} ${owner['LastName']}`).join(' & ');

        let setters = new Array<Dictionary<any>>();

        setters.push(parameterFactory.createIndividualSetParameter(
            LiabilityPageLiabilityDefinition,
            { AccountName: accountName },
            $scope.selectedLiability['Id']));

        const setArgs = parameterFactory.createAggregateSetParameter(setters);

        let provider = new LqbDataServiceRequestFormatProvider();
        provider.addRequest(LqbDataServiceRequestType.Set, setArgs);
        provider.addRequest(LqbDataServiceRequestType.SetOwner, setOwnerArgs);
        provider.addRequest(LqbDataServiceRequestType.GetLiabilityTotals, getLiabilityTotalArgs);

        sendWebRequest(
            LqbDataServiceOperationType.Save,
            provider.toRequest(),
            (data: Dictionary<any>) => {
                const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
                if (handler.hasError()) {
                    alert(handler.getErrorMessage());
                    return;
                }

                LQBPopup.Hide();

                const provider = $scope.createInitProvider();
                $scope.loadData(provider.toRequest(), () => {
                    $scope.vm.liabilityTotals = handler.getResponseByKey(LqbDataServiceRequestType.GetLiabilityTotals, 'loan');

                    // Ensure that the liability editor is hidden when the liability
                    // no longer matches the application filter. For the 'All'
                    // view, this logic is ignored.
                    if ($scope.liabilityFilterModel === AllApplicationsFilter) {
                        $scope.setSelectedLiability($scope.vm.loan.Liabilities[$scope.selectedRecordIndex]);
                        return;
                    }

                    const selectedApplication: Dictionary<Array<Dictionary<string>>> = $scope.selectedApplications.find((app: Dictionary<string>) => {
                        return app['key'] === $scope.selectedApplicationIdModel;
                    });

                    const updatedSelectedLiability = $scope.vm.loan.Liabilities.find((liability: Dictionary<string>) => liability['Id'] === $scope.selectedLiability['Id']);
                    const selectedLiabilityNewOwners: Array<Dictionary<string>> = updatedSelectedLiability['Owners'];
                    const selectedAppOwners: Array<Dictionary<string>> = selectedApplication['values'];

                    const selectedLiabilityOwnerInCurrentApp = selectedLiabilityNewOwners.find((liabilityOwner: Dictionary<string>) => {
                        return selectedAppOwners.find((appOwner: Dictionary<string>) => appOwner['ConsumerId'] === liabilityOwner['ConsumerId']) != null;
                    });

                    if (selectedLiabilityOwnerInCurrentApp != null) {
                        $scope.setSelectedLiability($scope.vm.loan.Liabilities[$scope.selectedRecordIndex]);
                    }
                    else {
                        $scope.selectedRecordIndex = -1;
                        $scope.selectedLiability = null;
                        $scope.moveToNextRecord();
                    }
                });
            });
    }

    $scope.getConsumerName = (consumerId: string) => getConsumerName($scope.vm.loan.Consumers, consumerId);

    $scope.getAppName = (appIdConsumerIdPairs: Array<Dictionary<string>>) => getAppName(appIdConsumerIdPairs, $scope.getConsumerName);

    $scope.applyResult = function (data: Dictionary<any>, callback?: any) {
        $scope.$apply(function () {
            const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
            if (handler.hasError()) {
                alert(handler.getErrorMessage());
                return;
            }

            const getResponse = handler.getResponseByKey(LqbDataServiceRequestType.Get, 'loan');
            $scope.vm.loan = Object.assign($scope.vm.loan, getResponse);

            $scope.displayDebtsToBePaidOffRow = $scope.vm.loan.sTridTargetRegulationVersionT == trid2017TargetVersion;
            orderConsumerNames($scope.vm.loan.Liabilities, $scope.getConsumerName);

            const liabilityTotalsResponse = handler.getResponseByKey(LqbDataServiceRequestType.GetLiabilityTotals, 'loan');
            $scope.vm.liabilityTotals = liabilityTotalsResponse;

            if (callback != null) {
                callback();
            }
        });
    }

    $scope.loadData = function (request: string, callback?: any) {
        sendWebRequest(
            LqbDataServiceOperationType.Load,
            request,
            (data: Dictionary<any>) => { $scope.applyResult(data, callback); });
    }

    $scope.liabilityMatchesFilter = function (liability: Dictionary<any>): boolean {
        if ($scope.vm.loan.ConsumerLiabilities == null) {
            return true;
        }

        if ($scope.selectedApplications == null || $scope.liabilityFilterModel === AllApplicationsFilter) {
            return true;
        }

        const applicableApps = $scope.selectedApplications.filter((app: Dictionary<any>) => app['key'] === $scope.selectedApplicationIdModel)
            .map((app: Dictionary<any>) => app['values'])
            .reduce((a: Array<Dictionary<any>>, b: Array<Dictionary<any>>) => a.concat(b), new Array<Dictionary<any>>());

        if (applicableApps.length === 0) {
            return false;
        }

        const matchingOwner = liability['Owners'].find(function (liabilityOwner: Dictionary<string>) {
            // Ignore IsPrimary when searching at the borrower level.
            if ($scope.assetFilterModel !== BorrowerFilter && !liabilityOwner['IsPrimary']) {
                return false;
            }

            return applicableApps.find((app: Dictionary<string>) => app['ConsumerId'] === liabilityOwner['ConsumerId']) != null;
        });

        return matchingOwner != null;
    }

    $scope.getLiabilityTotalArgs = function () {
        return getTotalArgs(parameterFactory, $scope.liabilityFilterModel, $scope.selectedApplicationIdModel);
    }

    $scope.updateLiabilityTotalsForCurrentApplication = function () {
        const getLiabilityTotalArgs = $scope.getLiabilityTotalArgs();
        $scope.updateLiabilityTotals(getLiabilityTotalArgs);
    }

    $scope.updateLiabilityTotals = function (getLiabilityTotalArgs: Dictionary<string>) {
        let provider = new LqbDataServiceRequestFormatProvider();
        provider.addRequest(LqbDataServiceRequestType.GetLiabilityTotals, getLiabilityTotalArgs);

        sendWebRequest(
            LqbDataServiceOperationType.Load,
            provider.toRequest(),
            (data: Dictionary<any>) => {
                const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
                if (handler.hasError()) {
                    alert(handler.getErrorMessage());
                    return;
                }

                $scope.$apply(() => {
                    $scope.vm.liabilityTotals = handler.getResponseByKey(LqbDataServiceRequestType.GetLiabilityTotals, 'loan');
                });
            });
    }

    $scope.updateAssetsLiabilitiesCompletedJointly = function () {
        if ($scope.liabilityFilterModel !== LegacyApplicationsFilter) {
            return;
        }

        var selectedLegacyApp = $scope.vm.loan.LegacyApplications.find((legacyApp: Dictionary<string>) => legacyApp['Id'] === $scope.selectedApplicationIdModel);
        $scope.vm.AssetLiaCompletedNotJointly = selectedLegacyApp['AssetLiaCompletedNotJointly'];
    }

    $scope.displayAllLiabilities = function () {
        $scope.init();
    }

    $scope.displayLegacyAppLiabilities = function () {
        $scope.selectedApplications = normalizeSelectedApplications(
            $scope.vm.loan.LegacyApplicationConsumers,
            'LegacyApplicationId',
            'ConsumerId',
            (app: Dictionary<boolean>) => app['IsPrimary']);

        $scope.selectedApplicationIdModel = $scope.selectedApplications[0]['key'];

        $scope.selectedRecordIndex = -1;
        $scope.moveToNextRecord();

        const getLiabilityTotalArgs = parameterFactory.createGetTotalsParameter(
            LoanDefinition,
            TotalsType.LegacyApplication,
            $scope.selectedApplicationIdModel);

        $scope.updateLiabilityTotals(getLiabilityTotalArgs);
        $scope.updateAssetsLiabilitiesCompletedJointly();
    }

    $scope.displayUladAppLiabilities = function () {
        $scope.selectedApplications = normalizeSelectedApplications(
            $scope.vm.loan.UladApplicationConsumers,
            'UladApplicationId',
            'ConsumerId',
            (app: Dictionary<boolean>) => app['IsPrimary']);

        $scope.selectedApplicationIdModel = $scope.selectedApplications[0]['key'];

        $scope.selectedRecordIndex = -1;
        $scope.moveToNextRecord();

        const getLiabilityTotalArgs = parameterFactory.createGetTotalsParameter(
            LoanDefinition,
            TotalsType.UladApplication,
            $scope.selectedApplicationIdModel);

        $scope.updateLiabilityTotals(getLiabilityTotalArgs);
    }

    $scope.displayBorrowerLiabilities = function () {
        $scope.selectedApplications = normalizeSelectedApplications(
            $scope.vm.loan.Consumers,
            'Id',
            'Id',
            (app: Dictionary<string>) => true);

        $scope.selectedApplicationIdModel = $scope.selectedApplications[0]['key'];

        $scope.selectedRecordIndex = -1;
        $scope.moveToNextRecord();

        const getLiabilityTotalArgs = parameterFactory.createGetTotalsParameter(
            LoanDefinition,
            TotalsType.Consumer,
            $scope.selectedApplicationIdModel);

        $scope.updateLiabilityTotals(getLiabilityTotalArgs);
    }

    $scope.onLiabilityFilterChange = function (oldLiabilityFilterModel: string) {
        $scope.checkForSave(
            () => {
                $scope.selectedApplicationIdModel = null;
                $scope.selectedApplications = null;
                $scope.selectedLiability = null;

                switch ($scope.liabilityFilterModel) {
                    case AllApplicationsFilter:
                        $scope.displayAllLiabilities();
                        break;
                    case LegacyApplicationsFilter:
                        $scope.displayLegacyAppLiabilities();
                        break;
                    case UladApplicationsFilter:
                        $scope.displayUladAppLiabilities();
                        break;
                    case BorrowerFilter:
                        $scope.displayBorrowerLiabilities();
                        break;
                    default:
                        throw `Unhandled liability filter value ${$scope.liabilityFilterModel}.`;
                }

                // Give the filtering time to complete before initializing inputs.
                $timeout(_initInput, 500);
            },
            () => {
                $scope.liabilityFilterModel = oldLiabilityFilterModel;
            },
            false/*updateTotalsOnSave*/);
    }

    $scope.onApplicationFilterChange = function (newSelectedApplicationIdModel: string) {
        const oldSelectedApplicationIdModel: string = $scope.selectedApplicationIdModel;

        $scope.checkForSave(
            () => {
                $scope.selectedApplicationIdModel = newSelectedApplicationIdModel;

                $scope.selectedRecordIndex = -1;
                $scope.selectedLiability = null;

                $scope.moveToNextRecord();
                $scope.updateLiabilityTotalsForCurrentApplication();
                $scope.updateAssetsLiabilitiesCompletedJointly();

                // Give the filtering time to complete before initializing inputs.
                $timeout(_initInput, 500);
            },
            () => {
                $('#ApplicationFilter').val(oldSelectedApplicationIdModel);
                $scope.selectedApplicationIdModel = oldSelectedApplicationIdModel;
            },
            false/*updateTotalsOnSave*/);
    }

    $scope.onDebtTypeChange = function () {
        const newDebtType: string = $scope.selectedLiability['DebtType'];

        if ($scope.isMortgageDebtType(newDebtType)) {
            $scope.selectedLiability['UsedInRatioData'] = false;

            if ($scope.selectedLiability['MortgageType'] == null ||
                $scope.selectedLiability['MortgageType'] == '') {
                // Default mortgage type to 'Conventional' to match the dropdown's default
                // in other UI locations like the loan info page.
                $scope.selectedLiability['MortgageType'] = conventionalMortgageType;
            }
        }
        else {
            $scope.selectedLiability['LinkedReoId'] = null;
        }

        if (newDebtType === revolvingDebtType) {
            $scope.selectedLiability['RemainMon'] = '(R)';
        }

        // Switching from blank to a valid value may not
        // trigger the page's dirty bit update. Trigger this
        // behavior here.
        updateDirtyBit();
        $scope.refreshCalculation();
    }

    $scope.onExcludeFromUwChanged = function (refreshCalculation: boolean = true) {
        if ($scope.selectedLiability['ExcludeFromUw']) {
            $scope.selectedLiability['UsedInRatioData'] = false;
        }

        if (refreshCalculation) {
            $scope.refreshCalculation();
        }
    }

    $scope.onWillBePdOffChanged = function (refreshCalculation: boolean = true) {
        if ($scope.selectedLiability['WillBePdOff']) {
            if ($scope.selectedLiability['PayoffTimingData'] == defaultPayoffTiming) {
                $scope.selectedLiability['PayoffTimingData'] = '1';
            }
        }
        else {
            $scope.selectedLiability['PayoffTimingLockedData'] = false;
            $scope.selectedLiability['PayoffTimingData'] = defaultPayoffTiming;
        }

        if (refreshCalculation) {
            $scope.refreshCalculation();
        }
    }

    $scope.onPayoffAmtLockedClick = function () {
        // Sync calculated and user-input fields when locking field.
        if ($scope.selectedLiability['PayoffAmtLocked']) {
            $scope.selectedLiability['PayoffAmtData'] = $scope.selectedLiability['PayoffAmt'];
        }

        $scope.refreshCalculation();
    }

    $scope.onPayoffTimingLockedDataClicked = function () {
        // Sync calculated and user-input fields when locking field.
        if ($scope.selectedLiability['PayoffTimingLockedData']) {
            $scope.selectedLiability['PayoffTimingData'] = $scope.selectedLiability['PayoffTiming'];
        }

        $scope.refreshCalculation();
    }

    $scope.onLinkedReoIdChanged = function () {
        if ($('#LinkedReoId').val() != '') {
            $scope.selectedLiability['Desc'] = $('#LinkedReoId option:selected').text();
        }

        updateDirtyBit();
    }

    $scope.onChangeLiabilitiesView = function (originalViewLiabilities: boolean) {
        $scope.checkForSave(
            () => { 
                if ($scope.viewLiabilities) {
                    $scope.originalDebtConsolidationData = null;
                }
                else {
                    $scope.backupDebtConsolidationData();
                }
            },
            () => { $scope.viewLiabilities = originalViewLiabilities; });
    }

    $scope.onLoanPurposeChange = function () {
        if ($scope.vm.loan.sLPurposeT != $scope.purchaseType) {
            $scope.vm.loan.sLAmtLckd = true;
        }

        $scope.refreshCalculation();
    }

    $scope.onLienToPayoffTotDebtChanged = function (originalLienToPayoffTotDebt: string) {
        if (isDirty()) {
            $scope.checkForSave(
                (wasSaved: boolean) => {
                    if (!wasSaved) {
                        $scope.saveData();
                    }
                },
                () => { $scope.vm.loan.sLienToPayoffTotDebt = originalLienToPayoffTotDebt; });
        }
        else {
            $scope.saveData();
        }
    }

    $scope.postInit = function () {
        $scope.lienToPayoffEnabled = $scope.vm.loan.sLienPosT === '1' ||
            ($scope.vm.loan.sLienPosT === '0' &&
                $scope.vm.loan.sIsOFinNew &&
                parseMoneyFloat($scope.vm.loan.sConcurSubFin) > 0);

        const liabilityIdFromQueryString = getQueryStringVariable('liabilityId', '');
        if (liabilityIdFromQueryString == null || liabilityIdFromQueryString == '') {
            $scope.moveToPreviousRecord();
        }
        else {
            $scope.selectLiability({ Id: liabilityIdFromQueryString });
        }
        
        window.setTimeout(_initInput, 0);
    }

    $scope.init = function () {
        $scope.IsPmlEnabled = ML.IsPmlEnabled;
        $scope.vm = new Dictionary<Dictionary<any>>();
        $scope.vm.loan = new Dictionary<any>();

        const provider = $scope.createInitProvider();
        $scope.loadData(provider.toRequest(), $scope.postInit);
    }

    $scope.createInitProvider = function (): LqbDataServiceRequestFormatProvider {
        let provider = new LqbDataServiceRequestFormatProvider();

        const getArgs = parameterFactory.createIndividualGetParameter([
            ConsumerLiabilityDefinition,
            LiabilityPageLiabilityDefinition,
            LiabilityPageConsumerDefinition,
            LiabilityPageLegacyApplicationDefinition,
            LiabilityPageLoanDefinition,
            LiabilityPageRealPropertyDefinition,
            LegacyApplicationConsumersDefinition,
            RealPropertyLiabilitiesDefinition,
            UladApplicationDefinition,
            UladApplicationConsumersDefinition]);

        const getLiabilityTotalArgs = parameterFactory.createGetTotalsParameter(LiabilityPageLoanDefinition, TotalsType.Loan);

        provider.addRequest(LqbDataServiceRequestType.Get, getArgs);
        provider.addRequest(LqbDataServiceRequestType.GetLiabilityTotals, getLiabilityTotalArgs);
        return provider;
    }

    $scope.init();
}]);