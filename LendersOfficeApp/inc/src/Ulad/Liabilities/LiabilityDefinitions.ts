import ConsumerDefinition from '../Common/ConsumerDefinition';
import LegacyApplicationDefinition from '../Common/LegacyApplicationDefinition';
import LiabilityDefinition from '../Common/LiabilityDefinition';
import LoanDefinition from '../Common/LoanDefinition';
import ReoDefinition from '../Common/ReoDefinition';

export class LiabilityPageLoanDefinition extends LoanDefinition {
    // Liabilities mode
    sConcurSubFin: string = '';
    sIsOFinNew: string = '';
    sIsStatusLead: string = '';
    sLT: string = '';
    sLienPosT: string = '';
    sLienToPayoffTotDebt: string = '';
    sTridTargetRegulationVersionT: string = '';

    // Debt consolidation mode
    sCurrentTotMonPmt: string = '';
    sDue: string = '';
    sIOnlyMon: string = '';
    sIsRateLocked: string = '';
    sIsRenovationLoan: string = '';
    sLAmtCalc: string = '';
    sLAmtLckd: string = '';
    sLPurposeT: string = '';
    sMonSavingPmt: string = '';
    sMonthlyPmt: string = '';
    sNoteIR: string = '';
    sProdCashoutAmt: string = '';
    sQualBottomR: string = '';
    sIsStandAlone2ndLien: string = '';
    sTerm: string = '';
    sTransNetCash: string = '';
    sTransNetCashLckd: string = '';
    sTransmTotMonPmt: string = '';
}

export class LiabilityPageLegacyApplicationDefinition extends LegacyApplicationDefinition {
    AssetLiaCompletedNotJointly: string = '';
}

export class LiabilityPageRealPropertyDefinition extends ReoDefinition {
    StreetAddress: string = '';
    City: string = '';
    State: string = '';
    Zip: string = '';
}

export class LiabilityPmlAuditTrailDefinition extends LiabilityDefinition {
    EventDate: string = '';
    UserName: string = '';
    LoginId: string = '';
    Action: string = '';
    Field: string = '';
    Value: string = '';
}

export class LiabilityPageLiabilityDefinition extends LiabilityDefinition {
    CompanyName: string = '';
    CompanyAddress: string = '';
    CompanyCity: string = '';
    CompanyState: string = '';
    CompanyZip: string = '';
    CompanyPhone: string = '';
    CompanyFax: string = '';

    DebtType: string = '';
    Desc: string = '';
    AccountName: string = '';
    AccountNum: string = '';

    Bal: string = '';
    Pmt: string = '';
    RemainMon: string = '';
    Rate: string = '';
    OriginalTermAsString: string = '';
    DueInMonAsString: string = '';

    WillBePdOff: string = '';

    PayoffAmt: string = '';
    PayoffAmtData: string = '';
    PayoffAmtLocked: string = '';

    PayoffTiming: string = '';
    PayoffTimingData: string = '';
    PayoffTimingLockedData: string = '';

    Late30AsString: string = '';
    Late60AsString: string = '';
    Late90PlusAsString: string = '';

    MaximumBalance: string = '';
    MortgageType: string = '';

    UsedInRatioData: string = '';
    IsPiggyBack: string = '';
    ExcludeFromUw: string = '';

    IncludeInReposession: string = '';
    IncludeInBk: string = '';
    IncludeInFc: string = '';

    VerifSentDate: string = '';
    VerifReorderedDate: string = '';
    VerifRecvDate: string = '';
    VerifExpiresDate: string = '';

    OriginalDebtAmt: string = '';
    IsMtgFhaInsured: string = '';
    IsForAuto: string = '';
    AutoYearMakeAsString: string = '';

    RequiresVom: string = '';
    RequiresVol: string = '';

    PmlAuditTrail: LiabilityPmlAuditTrailDefinition = new LiabilityPmlAuditTrailDefinition();

    isReadonly(key: string) {
        switch (key) {
            case 'PmlAuditTrail':
            case 'PayoffAmt':
            case 'PayoffTiming':
                return true;
            default:
                return super.isReadonly(key);
        }
    }
}

export class LiabilityPageConsumerDefinition extends ConsumerDefinition {
    FirstName: string = '';
    LastName: string = '';
}