import * as angular from 'angular';
import { ITimeoutService } from 'angular';

import ConsumerDefinition from '../Common/ConsumerDefinition';
import UladAssociationHelper from '../Common/UladAssociationHelper';

import BooleanLqbDataServiceResponseConverter from '../../api/BooleanLqbDataServiceResponseConverter';
import Dictionary from '../../api/Dictionary';
import ILqbDataServiceResponseValueConverter from '../../api/ILqbDataServiceResponseConverter';
import LqbDataServiceRequestFormatProvider from '../../api/LqbDataServiceRequestFormatProvider';
import LqbDataServiceRequestParameterFactory from '../../api/LqbDataServiceRequestParameterFactory';
import LqbDataServiceResponseHandler from '../../api/LqbDataServiceResponseHandler';
import LqbDataServiceOperationType from '../../api/LqbDataServiceOperationType';
import LqbDataServiceRequestType from '../../api/LqbDataServiceRequestType';

import getConsumerName from '../Common/Utils/getConsumerName';
import sendWebRequest from '../Common/Utils/sendWebRequest';

declare function clearDirty(): void;
declare function isDirty(): boolean;
declare function PolyConfirmSave(confirmCallback: (shouldSave: boolean) => void, shouldShowCancel: boolean, cancelCallback?: () => void): void;
declare function retrieveEventTarget(event: any): HTMLInputElement;

class DeclarationsPageConsumerDefinition extends ConsumerDefinition {
    DecAmtBorrowedFromOthersForTransUlad: string = '';
    DecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad: string = '';
    DecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueExplanationUlad: string = '';
    DecHasBankruptcyChapter11Ulad: string = '';
    DecHasBankruptcyChapter12Ulad: string = '';
    DecHasBankruptcyChapter13Ulad: string = '';
    DecHasBankruptcyChapter7Ulad: string = '';
    DecHasBankruptcyLast7YearsUlad: string = '';
    DecHasBankruptcyLast7YearsExplanationUlad: string = '';
    DecHasConveyedTitleInLieuOfFcLast7YearsUlad: string = '';
    DecHasConveyedTitleInLieuOfFcLast7YearsExplanationUlad: string = '';
    DecHasForeclosedLast7YearsUlad: string = '';
    DecHasForeclosedLast7YearsExplanationUlad: string = '';
    DecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad: string = '';
    DecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscExplanationUlad: string = '';
    DecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad: string = '';
    DecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscExplanationUlad: string = '';
    DecHasOutstandingJudgmentsUlad: string = '';
    DecHasOutstandingJudgmentsExplanationUlad: string = '';
    DecHasOwnershipIntOtherPropLastThreeYearsUlad: string = '';
    DecHasOwnershipIntOtherPropLastThreeYearsExplanationUlad: string = '';
    DecIsBorrowingFromOthersForTransUlad: string = '';
    DecIsBorrowingFromOthersForTransExplanationUlad: string = '';
    DecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad: string = '';
    DecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedExplanationUlad: string = '';
    DecIsDelinquentOrDefaultFedDebtUlad: string = '';
    DecIsDelinquentOrDefaultFedDebtExplanationUlad: string = '';
    DecIsFamilyOrBusAffiliateOfSellerUlad: string = '';
    DecIsFamilyOrBusAffiliateOfSellerExplanationUlad: string = '';
    DecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad: string = '';
    DecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityExplanationUlad: string = '';
    DecIsPrimaryResidenceUlad: string = '';
    DecIsPrimaryResidenceExplanationUlad: string = '';
    DecOwnershipIntOtherPropLastThreeYearsPropTypeUlad: string = '';
    DecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad: string = '';
    DecPropSubjectToHigherPriorityLienThanFirstMtgUlad: string = '';
    DecPropSubjectToHigherPriorityLienThanFirstMtgExplanationUlad: string = '';
    FirstName: string = '';
    LastName: string = '';

    public isReadonly(field: string) {
        return field == 'FirstName' || field == 'LastName';
    }
}

class DeclarationsLqbDataServiceResponseConverter implements ILqbDataServiceResponseValueConverter {
    public canConvert(key: string, value: string): boolean {
        switch (key) {
            case 'DecHasBankruptcyChapter7Ulad':
            case 'DecHasBankruptcyChapter11Ulad':
            case 'DecHasBankruptcyChapter12Ulad':
            case 'DecHasBankruptcyChapter13Ulad':
                return true;
            default:
                return false;
        }
    }

    public convert(value: string): any {
        if (value == null) {
            throw `Unhandled conversion value ${value}`;
        }

        switch (value.trim().toLowerCase()) {
            case 'true':
            case 'yes':
                return true;
            case 'false':
            case 'no':
                return false;
            default:
                throw `Unhandled conversion value ${value}`;
        }
    }
}

const parameterFactory = new LqbDataServiceRequestParameterFactory();
const uladAssociationHelper = new UladAssociationHelper();
const valueConverter = new DeclarationsLqbDataServiceResponseConverter();

angular
.module('Declarations', [])
.controller('DeclarationsController', ['$scope', '$timeout', function ApplicationManagementController($scope, $timeout: ITimeoutService) {
    $scope.triStateTrueValue = 'True';

    $scope.selectedConsumer = new Dictionary<any>();
    $scope.backupSelectedConsumer = new Dictionary<any>();

    $scope.getConsumerName = function (consumer: Dictionary<string>): string {
        return getConsumerName([consumer], consumer['Id']);
    }

    $scope.setSelectedConsumer = function(consumerId: string) {
        const callback = () => {
            $scope.selectedConsumer = $scope.vm.loan.Consumers.find((consumer: Dictionary<string>) => consumer['Id'] === consumerId);
            $scope.backupSelectedConsumer = Object.assign(new Dictionary<any>(), $scope.selectedConsumer);
        };

        if (isDirty()) {
            PolyConfirmSave(
                (shouldSave: boolean) => {
                    if (shouldSave) {
                        $scope.saveData(callback);
                    }
                    else {
                        $timeout(() => {
                            $scope.revertConsumerChange();
                            clearDirty();
                            callback();
                        });
                    }
                },
                true/*shouldShowCancel*/);
        }
        else {
            callback();
        }
    }

    $scope.revertConsumerChange = function () {
        const backupId = $scope.backupSelectedConsumer['Id'];
        for (let i = 0; i < $scope.vm.loan.Consumers.length; ++i) {
            const consumer = $scope.vm.loan.Consumers[i];

            if (consumer['Id'] === backupId) {
                $scope.vm.loan.Consumers[i] = $scope.backupSelectedConsumer;
                return;
            }
        }
    }

    $scope.onFormatMoney = function (input: HTMLInputElement) {
        // Ensure the value is formatted as expected in the model,
        // which happens after the initial model update.
        $scope.$apply(() => {
            $scope.selectedConsumer[input.id] = input.value;
        });
    }

    $scope.saveData = function (callback?: () => void) {
        let provider = new LqbDataServiceRequestFormatProvider();

        let setters = new Array<Dictionary<any>>();
        setters.push(parameterFactory.createIndividualSetParameter(
            DeclarationsPageConsumerDefinition,
            $scope.selectedConsumer,
            $scope.selectedConsumer['Id']));

        const setArgs = parameterFactory.createAggregateSetParameter(setters);
        provider.addRequest(LqbDataServiceRequestType.Set, setArgs);

        sendWebRequest(
            LqbDataServiceOperationType.Save,
            provider.toRequest(),
            (data: Dictionary<any>) => {
                const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, valueConverter);
                if (handler.hasError()) {
                    alert(handler.getErrorMessage());
                    return;
                }

                $scope.$apply(() => {
                    clearDirty();
                    $scope.backupSelectedConsumer = Object.assign(new Dictionary<any>(), $scope.selectedConsumer);

                    if (callback != null) {
                        callback();
                    }
                });
            });
    }

    $scope.applyResult = function(data: Dictionary<any>, callback?: () => void) {
        $scope.$apply(function () {
            const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, valueConverter);
            if (handler.hasError()) {
                alert(handler.getErrorMessage());
                return;
            }

            const getResponse = handler.getResponseByKey(LqbDataServiceRequestType.Get, 'loan');
            $scope.vm.loan = Object.assign($scope.vm.loan, getResponse);

            if (callback != null) {
                callback();
            }
        });
    }

    $scope.loadData = function (request: string, callback?: () => void) {
        sendWebRequest(
            LqbDataServiceOperationType.Load,
            request,
            (data: Dictionary<any>) => { $scope.applyResult(data, callback); });
    }

    $scope.createInitProvider = function (): LqbDataServiceRequestFormatProvider {
        const getArgs = parameterFactory.createIndividualGetParameter([DeclarationsPageConsumerDefinition]);

        const provider = new LqbDataServiceRequestFormatProvider();
        provider.addRequest(LqbDataServiceRequestType.Get, getArgs);

        return provider;
    }

    $scope.init = function () {
        $scope.vm = new Dictionary<Dictionary<any>>();
        $scope.vm.loan = new Dictionary<any>();

        const provider: LqbDataServiceRequestFormatProvider = $scope.createInitProvider();
        $scope.loadData(provider.toRequest(), () => {
            const firstConsumer = $scope.vm.loan.Consumers[0];
            $scope.setSelectedConsumer(firstConsumer['Id']);
        });
    }

    $scope.init();
}])
.directive('triStateCheckboxList', function () {
    const falseValue = 'False';
    const trueValue = 'True';
    const nullValue = 'Indeterminant';

    const templateData = [
        '<span>',
        `<input type="checkbox" ng-checked="noCheckboxChecked" ng-click="updateTriState($event, '${falseValue}');" ng-value="'${falseValue}'" /> No `,
        `<input type="checkbox" ng-checked="yesCheckboxChecked" ng-click="updateTriState($event, '${trueValue}');" ng-value="'${trueValue}'" /> Yes `,
        '</span>'
    ];

    return {
        restrict: 'E',
        require: 'ngModel',
        scope: {
            ngModel: '='
        },
        template: templateData.join(""),
        link: function(scope: ITriStateCheckboxListScope) {
            scope.updateTriState = function($event: any, newValue: string) {
                const checked = $event == null ? true : retrieveEventTarget($event).checked;
                if (checked) {
                    scope.ngModel = newValue;
                    scope.noCheckboxChecked = newValue == falseValue;
                    scope.yesCheckboxChecked = newValue == trueValue;
                }
                else {
                    scope.ngModel = nullValue;
                    scope.noCheckboxChecked = false;
                    scope.yesCheckboxChecked = false;
                }
            };

            scope.$watch('ngModel', function (newValue: string) {
                scope.updateTriState(null, newValue);
            });
        }
    }
});

interface ITriStateCheckboxListScope extends angular.IScope {
    updateTriState: ($event: any, newValue: string) => void;
    ngModel: string;
    noCheckboxChecked: boolean;
    yesCheckboxChecked: boolean;
}