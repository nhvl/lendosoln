﻿import * as angular from 'angular';
import { ITimeoutService } from 'angular';

import CounselingEventDefinition from '../Common/CounselingEventDefinition';
import ConsumerDefinition from '../Common/ConsumerDefinition';
import CounselingEventAttendanceDefinition from '../Common/CounselingEventAttendanceDefinition';
import EditOwnerPopupHelper from '../Common/EditOwnerPopupHelper';
import UladAssociationHelper from '../Common/UladAssociationHelper';

import BooleanLqbDataServiceResponseConverter from '../../api/BooleanLqbDataServiceResponseConverter';
import Dictionary from '../../api/Dictionary';
import LqbDataServiceRequestFormatProvider from '../../api/LqbDataServiceRequestFormatProvider';
import LqbDataServiceRequestParameterFactory from '../../api/LqbDataServiceRequestParameterFactory';
import LqbDataServiceResponseHandler from '../../api/LqbDataServiceResponseHandler';
import LqbDataServiceOperationType from '../../api/LqbDataServiceOperationType';
import LqbDataServiceRequestType from '../../api/LqbDataServiceRequestType';

import getAppId from '../Common/Utils/getAppId';
import getConsumerName from '../Common/Utils/getConsumerName';
import getLoanId from '../Common/Utils/getLoanId';
import orderConsumerNames from '../Common/Utils/orderConsumerNames';
import sendWebRequest, { sendWebRequestByUrl } from '../Common/Utils/sendWebRequest';

class CounselingPageCounselingEventDefinition extends CounselingEventDefinition {
    CompletedDate: string = '';
    CounselingFormat: string = '';
    CounselingType: string = '';
    HousingCounselingAgency: string = '';
    HousingCounselingHudAgencyID: string = '';
}

class CounselingPageCounselingEventAttendeeDefinition extends ConsumerDefinition {
    FirstName: string = '';
    LastName: string = '';
}

declare function callWebMethodAsync(params: Dictionary<any>) : void;
declare function getQueryStringVariable(variable: string) : string;
declare function clearDirty(): void;
declare function updateDirtyBit(): void;
declare function isDirty(): boolean;
declare function _initInput(): void;
declare function PolyConfirmSave(confirmCallback: (shouldSave: boolean) => void, shouldShowCancel: boolean, cancelCallback?: () => void): void;

declare var LQBPopup: any;
declare var VRoot: string;
declare var CounselingTypeByValue: Dictionary<string>;
declare var CounselingFormatTypeByValue: Dictionary<string>;

const $ = jQuery;
const parameterFactory = new LqbDataServiceRequestParameterFactory();
const booleanValueConverter = new BooleanLqbDataServiceResponseConverter();
const uladAssociationHelper = new UladAssociationHelper();


angular
.module('HomeownerCounseling', [])
.controller('HomeownerCounselingController', ['$scope', '$timeout', function HomeownerCounselingController($scope, $timeout : ITimeoutService) {
    $scope.sortKey = null;
    $scope.sortDesc = false;

    $scope.selectedRecordIndex = 0;

    $scope.selectedEvent = null;
    $scope.originalSelectedEvent = null;

    $scope.isDirty = isDirty;
    $scope.updateDirtyBit = updateDirtyBit;

    $scope.getFriendlyEventCounselingTypeName = function (counselingTypeValue: string) : string {
        return CounselingTypeByValue[counselingTypeValue] as string;
    }

    $scope.getFriendlyEventCounselingFormatTypeName = function (counselingFormatTypeValue: string): string {
        return CounselingFormatTypeByValue[counselingFormatTypeValue] as string;
    }

    $scope.onDateFormat = function (input: HTMLInputElement) {
        $scope.selectedEvent[input.id] = input.value;
    }

    $scope.refreshCalculation = function () {
        // Allow other scripts to finish running before calling
        // for a data refresh, e.g. money formatting.
        $timeout(() => {
            $scope.saveData(null/*callback*/, true/*refreshOnly*/);
        });
    }

    $scope.revertEventChanges = function (backupEvent: Dictionary<any>) {
        if ($scope.vm.loan == null || $scope.vm.loan.CounselingEvents == null) {
            return;
        }

        const backupId = backupEvent['Id'];
        for (let i = 0; i < $scope.vm.loan.CounselingEvents.length; ++i) {
            const event = $scope.vm.loan.CounselingEvents[i];

            if (event['Id'] === backupId) {
                $scope.vm.loan.CounselingEvents[i] = backupEvent;
                return;
            }
        }
    }

    $scope.sortEvents = function (event1: Dictionary<any>, event2: Dictionary<any>): number {
        const direction = $scope.sortDesc ? -1 : 1;

        switch ($scope.sortKey) {
            case 'Attendees':
                const firstEventAttendees: Array<Dictionary<any>> = event1['Owners'];
                const secondEventAttendees: Array<Dictionary<any>> = event2['Owners'];

                const firstEventPrimaryAttendee = firstEventAttendees[0];
                const secondEventPrimaryAttendee = secondEventAttendees[0];

                const firstEventPrimaryAttendeeName = $scope.getConsumerName(firstEventPrimaryAttendee['ConsumerId']);
                const secondEventPrimaryAttendeeName = $scope.getConsumerName(secondEventPrimaryAttendee['ConsumerId']);

                return direction * firstEventPrimaryAttendeeName.localeCompare(secondEventPrimaryAttendeeName);

            case 'CompletedDate':
                const firstEventCompletedDate = event1['CompletedDate'];
                const secondEventCompletedDate = event2['CompletedDate'];

                if (firstEventCompletedDate == null || firstEventCompletedDate.trim().length === 0) {
                    if (secondEventCompletedDate == null || secondEventCompletedDate.trim().length === 0) {
                        return 0;
                    }

                    return direction * -1;
                }

                if (secondEventCompletedDate == null || secondEventCompletedDate.trim().length === 0) {
                    if (firstEventCompletedDate == null || firstEventCompletedDate.trim().length === 0) {
                        return 0;
                    }

                    return direction * 1;
                }

                const firstEventCompletedDateParsed = Date.parse(firstEventCompletedDate);
                const secondEventCompletedDateParsed = Date.parse(secondEventCompletedDate);

                return direction * (firstEventCompletedDateParsed.valueOf() - secondEventCompletedDateParsed.valueOf()); // Note: 0 if a == b, positive if a > b, negative if a < b.

            case 'CounselingType':
                const firstCounselingType = event1['CounselingType'];
                const secondCounselingType = event2['CounselingType'];

                if (firstCounselingType == null || firstCounselingType.trim().length === 0) {
                    if (secondCounselingType == null || secondCounselingType.trim().length === 0) {
                        return 0;
                    }

                    return direction * -1;
                }

                if (secondCounselingType == null || secondCounselingType.trim().length === 0) {
                    if (firstCounselingType == null || firstCounselingType.trim().length === 0) {
                        return 0;
                    }

                    return direction * 1;
                }

                const firstCounselingTypeDescription = CounselingTypeByValue[firstCounselingType];
                const secondCounselingTypeDescription = CounselingTypeByValue[secondCounselingType];

                return direction * firstCounselingTypeDescription.localeCompare(secondCounselingTypeDescription);

            case 'CounselingFormat':
                const firstCounselingFormatType = event1['CounselingFormat'];
                const secondCounselingFormatType = event2['CounselingFormat'];

                if (firstCounselingFormatType == null || firstCounselingFormatType.trim().length === 0) {
                    if (secondCounselingFormatType == null || secondCounselingFormatType.trim().length === 0) {
                        return 0;
                    }

                    return direction * -1;
                }

                if (secondCounselingFormatType == null || secondCounselingFormatType.trim().length === 0) {
                    if (firstCounselingFormatType == null || firstCounselingFormatType.trim().length === 0) {
                        return 0;
                    }

                    return direction * 1;
                }

                const firstCounselingFormatTypeDescription = CounselingFormatTypeByValue[firstCounselingFormatType];
                const secondCounselingFormatTypeDescription = CounselingFormatTypeByValue[secondCounselingFormatType];

                return direction * firstCounselingFormatTypeDescription.localeCompare(secondCounselingFormatTypeDescription);

            case 'HousingCounselingAgency':
                const firstHousingCounselingAgency = event1['HousingCounselingAgency'];
                const secondHousingCounselingAgency = event2['HousingCounselingAgency'];

                if (firstHousingCounselingAgency == null || firstHousingCounselingAgency.trim().length === 0) {
                    if (secondHousingCounselingAgency == null || secondHousingCounselingAgency.trim().length === 0) {
                        return 0;
                    }

                    return direction * -1;
                }

                if (secondHousingCounselingAgency == null || secondHousingCounselingAgency.trim().length === 0) {
                    if (firstHousingCounselingAgency == null || firstHousingCounselingAgency.trim().length === 0) {
                        return 0;
                    }

                    return direction * 1;
                }

                return direction * firstHousingCounselingAgency.localeCompare(secondHousingCounselingAgency);
            default:
                throw 'Unhandled sort key ' + $scope.sortKey;
        }
    }

    $scope.orderBy = function (key: string) {
        $scope.checkForSave(() => {
            if (key === $scope.sortKey) {
                $scope.sortDesc = !$scope.sortDesc;
            }
            else {
                $scope.sortKey = key;
                $scope.sortDesc = false;
            }

            if ($scope.vm.loan == null ||
                $scope.vm.loan.CounselingEvents == null ||
                $scope.vm.loan.CounselingEvents.length === 0) {
                return;
            }

            $scope.vm.loan.CounselingEvents.sort($scope.sortEvents);
            $scope.setRecordOrder($scope.vm.loan.CounselingEvents);

            // Update the selected record index to account for the new
            // order based on the sorting result.
            const selectedEventId = $scope.selectedEvent['Id'];
            $scope.selectedRecordIndex = $scope.vm.loan.CounselingEvents.findIndex((event: Dictionary<any>) => {
                return event['Id'] == selectedEventId;
            });
        });
    }

    $scope.setSelectedEvent = function (event: Dictionary<any>) {
        $scope.selectedEvent = event;

        const backupEvent: Dictionary<any> = Object.assign(new Dictionary<any>(), event);
        $scope.originalSelectedEvent = backupEvent;
    }

    $scope.checkForSave = function (postCallback: () => void, cancelCallback?: () => void) {
        if (isDirty()) {
            PolyConfirmSave(
                (shouldSave: boolean) => {
                    if (shouldSave) {
                        $scope.saveData(postCallback, false/*refreshOnly*/);
                    }
                    else {
                        clearDirty();

                        $timeout(() => {
                            $scope.revertEventChanges($scope.originalSelectedEvent);
                            $scope.setSelectedEvent($scope.originalSelectedEvent);
                            $scope.refreshCalculation();

                            postCallback();
                        });
                    }
                },
                true/*shouldShowCancel*/,
                () => {
                    if (cancelCallback != null) {
                        $timeout(cancelCallback);
                    }
                });
        }
        else {
            postCallback();
        }
    }
	
    $scope.moveToPreviousRecord = function () {
        if ($scope.vm.loan.CounselingEvents == null || $scope.vm.loan.CounselingEvents.length === 0) {
            return;
        }

        $scope.checkForSave(() => {
            const nextIndex = Math.max($scope.selectedRecordIndex - 1, 0);
            $scope.selectedRecordIndex = nextIndex;
            $scope.setSelectedEvent($scope.vm.loan.CounselingEvents[$scope.selectedRecordIndex]);
        });
	}
	
    $scope.moveToNextRecord = function () {
        if ($scope.vm.loan.CounselingEvents == null || $scope.vm.loan.CounselingEvents.length === 0) {
            return;
        }

         $scope.checkForSave(() => {
            const nextIndex = Math.min($scope.selectedRecordIndex + 1, $scope.vm.loan.CounselingEvents.length - 1);

            $scope.selectedRecordIndex = nextIndex;
            $scope.setSelectedEvent($scope.vm.loan.CounselingEvents[$scope.selectedRecordIndex]);
        });
	}
	
	$scope.selectEvent = function(event: Dictionary<string>) {
         $scope.checkForSave(() => {
             const eventId = event['Id'];

             for (let i = 0; i < $scope.vm.loan.CounselingEvents.length; ++i) {
                 const eventAtIndex = $scope.vm.loan.CounselingEvents[i];

                 if (eventId === eventAtIndex['Id']) {
                    $scope.selectedRecordIndex = i;
                    $scope.setSelectedEvent($scope.vm.loan.CounselingEvents[$scope.selectedRecordIndex]);
                    break;
                 }
             }
        });
	}

	$scope.insertRecord = function() {
        $scope.checkForSave(() => {
            const insertIndex = Math.max(0, $scope.selectedRecordIndex);
            $scope.addEvent(insertIndex);
        });
	}
	
	$scope.addRecord = function() {
        $scope.checkForSave(() => {
            $scope.addEvent();
        });
    }

    $scope.addEvent = function (insertIndex?: number) {		
        const hadActiveEvent = $scope.vm.loan.CounselingEvents.length !== 0;

        const attendeeIds = $scope.vm.loan.Consumers.map((attendee: Dictionary<string>) => attendee['Id']);
        const args = parameterFactory.createAddParameterForCounselingEvent(CounselingPageCounselingEventDefinition, attendeeIds, insertIndex);

		let provider = new LqbDataServiceRequestFormatProvider();
		provider.addRequest(LqbDataServiceRequestType.Add, args);
		
		sendWebRequest(
			LqbDataServiceOperationType.Save, 
			provider.toRequest(),
			(data: Dictionary<any>) => { 
				const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
				if (handler.hasError()) {
					alert(handler.getErrorMessage());
					return;
				}
				
				$scope.$apply(function() {
					const response = handler.getResponseByKey(LqbDataServiceRequestType.Add, 'loan');
			
					var event = new Dictionary<any>();
                    event['Id'] = response['id'];
                    event['Owners'] = $scope.vm.loan.Consumers.map((attendee: Dictionary<string>) => { attendee['ConsumerId'] = attendee['Id']; return attendee; });
					
					if (insertIndex == null) {
                        const eventsLength = $scope.vm.loan.CounselingEvents.push(event);
                        $scope.selectedRecordIndex = eventsLength - 1;
					}
					else {
                        $scope.vm.loan.CounselingEvents.splice(insertIndex, 0/*No entries before: only adding the event*/, event);
						$scope.selectedRecordIndex = insertIndex;
					}

                    orderConsumerNames([event], $scope.getConsumerName);
                    $scope.setSelectedEvent(event);

                    if (!hadActiveEvent) {
                        // No inputs are in the DOM until there is an active event.
                        // Once we've added an event, initialize the new inputs.
                        window.setTimeout(_initInput, 0);
                    }
				});
			});
    }

	$scope.deleteRecord = function() {
		const recordId = $scope.selectedEvent['Id'];
		
        const deleteArgs = parameterFactory.createRemoveParameter(CounselingPageCounselingEventDefinition, recordId);

		let provider = new LqbDataServiceRequestFormatProvider();
        provider.addRequest(LqbDataServiceRequestType.Remove, deleteArgs);
		
		sendWebRequest(
            LqbDataServiceOperationType.Save, 
			provider.toRequest(),
			(data: Dictionary<any>) => { 
                const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
				if (handler.hasError()) {
					alert(handler.getErrorMessage());
					return;
				}
				
                $scope.$apply(function () {
                    if ($scope.vm.loan.CounselingEvents.length === 1) {
                        $scope.vm.loan.CounselingEvents = new Array<Dictionary<string>>();
                        $scope.selectedRecordIndex = 0;
                        $scope.selectedEvent = null;
                    }
                    else {
                        $scope.vm.loan.CounselingEvents.splice($scope.selectedRecordIndex, 1);
                        $scope.moveToPreviousRecord();
                    }
				});
			});
	}
	
	$scope.moveRecordUp = function() {
		if ($scope.selectedRecordIndex === 0) {
			return;
		}
		
         $scope.checkForSave(() => {
             const events = $scope.vm.loan.CounselingEvents;
             const temp = events[$scope.selectedRecordIndex];
             events[$scope.selectedRecordIndex] = events[$scope.selectedRecordIndex - 1];
             events[$scope.selectedRecordIndex - 1] = temp;

             $scope.setRecordOrder(events, () => { $scope.selectedRecordIndex-- });
        });
	}
	
	$scope.moveRecordDown = function() {
        if ($scope.selectedRecordIndex === $scope.vm.loan.CounselingEvents.length - 1) {
			return;
        }

         $scope.checkForSave(() => {
             const events = $scope.vm.loan.CounselingEvents;
             const temp = events[$scope.selectedRecordIndex];
             events[$scope.selectedRecordIndex] = events[$scope.selectedRecordIndex + 1];
             events[$scope.selectedRecordIndex + 1] = temp;

             $scope.setRecordOrder(events, () => { $scope.selectedRecordIndex++ });
        });
	}
	
    $scope.setRecordOrder = function (events: Array<Dictionary<string>>, callback?: () => void) {
        const eventIds = events.map((event: Dictionary<string>) => event['Id']);
        const args = parameterFactory.createSetOrderParameter(CounselingPageCounselingEventDefinition, eventIds);
		
		let provider = new LqbDataServiceRequestFormatProvider();
		provider.addRequest(LqbDataServiceRequestType.SetOrder, args);
		
		sendWebRequest(
            LqbDataServiceOperationType.Save, 
			provider.toRequest(),
			(data: Dictionary<any>) => { 
				const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
				if (handler.hasError()) {
					alert(handler.getErrorMessage());
					return;
				}
				
				$scope.$apply(() => {
                    $scope.vm.loan.CounselingEvents = events;

                    if (typeof callback === 'function') {
                        callback();
                    }
				});
			});
	}
	
    $scope.saveData = function (callback: () => void, refreshOnly: boolean = false) {
        if ($scope.selectedEvent == null) {
            throw "No event selected.";
        }

        let provider = new LqbDataServiceRequestFormatProvider();
        let setters = new Array<Dictionary<any>>();

        setters.push(parameterFactory.createIndividualSetParameter(
            CounselingPageCounselingEventDefinition,
            $scope.selectedEvent,
            $scope.selectedEvent['Id']));
        
        const setArgs = parameterFactory.createAggregateSetParameter(setters);
        provider.addRequest(LqbDataServiceRequestType.Set, setArgs);
		
		sendWebRequest(
			refreshOnly ? LqbDataServiceOperationType.Load : LqbDataServiceOperationType.Save, 
			provider.toRequest(),
			(data: Dictionary<any>) => { 
				const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
				if (handler.hasError()) {
					alert(handler.getErrorMessage());
					return;
                }

                $scope.$apply(() => {
                    if (!refreshOnly) {
                        clearDirty();
                        $scope.originalSelectedEvent = $scope.selectedEvent;
                    }

                    if (typeof callback === 'function') {
                        callback();
                    }
                });
			});
	}
	
	$scope.editEventAttendees = function(event: Dictionary<Array<Dictionary<string>>>) {
        $scope.checkForSave(() => {
            const selectedOwners = event['Owners'];

            var helper = new EditOwnerPopupHelper();
            helper.storeEntityOwnerIdsToCache(selectedOwners, (result: Dictionary<any>) => {
                if (result.error) {
                    alert(result.UserMessage);
                    return;
                }

                const cacheKey = result.value['CacheKey'];
                const url = `${VRoot}/newlos/Ulad/EditOwner.aspx?loanid=${getLoanId()}&appid=${getAppId()}&id=${event['Id']}&t=CounselingEvent&s=${cacheKey}`;
                const settings = {
                    onReturn: $scope.onOwnerPopupClose
                };
                LQBPopup.Show(url, settings);
            });
        });
	}

	$scope.onOwnerPopupClose = function(returnArgs: Dictionary<any>) {
        const attendees: Array<Dictionary<string>> = returnArgs['Attendees'];
        const setAttendanceArgs = parameterFactory.createSetOwnerParameterForCounselingEvent(
            CounselingEventAttendanceDefinition,
            $scope.selectedEvent['Id'],
            attendees.map((attendee: Dictionary<string>) => attendee['Id']));
		
		let provider = new LqbDataServiceRequestFormatProvider();
        provider.addRequest(LqbDataServiceRequestType.SetAttendance, setAttendanceArgs);
		
		sendWebRequest(
			LqbDataServiceOperationType.Save, 
			provider.toRequest(),
			(data: Dictionary<any>) => { 
				const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
				if (handler.hasError()) {
					alert(handler.getErrorMessage());
					return;
				}
				
				LQBPopup.Hide();

				const provider = $scope.createInitProvider();
                $scope.loadData(provider.toRequest(), () => {
                    // Refresh the attendees for the selected event.
                    $scope.selectEvent($scope.selectedEvent);
                });
			});
	}
	
    $scope.getConsumerName = (consumerId: string) => getConsumerName($scope.vm.loan.Consumers, consumerId);
		
	$scope.applyResult = function(data: Dictionary<any>, callback?: any) {
		$scope.$apply(function() {
			const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
			if (handler.hasError()) {
				alert(handler.getErrorMessage());
				return;
			}
			
			const getResponse = handler.getResponseByKey(LqbDataServiceRequestType.Get, 'loan');
            $scope.vm.loan = Object.assign($scope.vm.loan, getResponse);
            orderConsumerNames($scope.vm.loan.CounselingEvents, $scope.getConsumerName);
			
			if (callback != null) {
				callback();
			}
		});
	}
	
	$scope.loadData = function(request: string, callback?: any) {
		sendWebRequest(
			LqbDataServiceOperationType.Load,
			request,
			(data: Dictionary<any>) => { $scope.applyResult(data, callback); });
	}
		
	$scope.init = function() {
		$scope.vm = new Dictionary<Dictionary<any>>();
		$scope.vm.loan = new Dictionary<any>();
		
		const provider = $scope.createInitProvider();
        $scope.loadData(provider.toRequest(), function () {
            $scope.moveToPreviousRecord();
            window.setTimeout(_initInput, 0);
		});
	}
	
	$scope.createInitProvider = function() : LqbDataServiceRequestFormatProvider {
		let provider = new LqbDataServiceRequestFormatProvider();
		
        const getArgs = parameterFactory.createIndividualGetParameter([
            CounselingPageCounselingEventDefinition,
            CounselingPageCounselingEventAttendeeDefinition,
            CounselingEventAttendanceDefinition]);
				
		provider.addRequest(LqbDataServiceRequestType.Get, getArgs);
		return provider;
	}
	
	$scope.init();
}]);
