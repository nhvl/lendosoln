import * as angular from 'angular';
import { ITimeoutService } from 'angular';

import Dictionary from '../../api/Dictionary';
import LqbDataServiceRequestFormatProvider from '../../api/LqbDataServiceRequestFormatProvider';
import LqbDataServiceRequestParameterFactory from '../../api/LqbDataServiceRequestParameterFactory';
import LqbDataServiceResponseHandler from '../../api/LqbDataServiceResponseHandler';
import LqbDataServiceOperationType from '../../api/LqbDataServiceOperationType';
import LqbDataServiceRequestType from '../../api/LqbDataServiceRequestType';

import EditAssociationPopupHelper from '../Common/EditAssociationPopupHelper';
import EmploymentRecordDefinition from '../Common/EmploymentRecordDefinition';
import BooleanLqbDataServiceResponseConverter from '../../api/BooleanLqbDataServiceResponseConverter';
import UladAssociationHelper from '../Common/UladAssociationHelper';

import getEmploymentToDate from '../Common/Utils/getEmploymentToDate';
import sendWebRequest from '../Common/Utils/sendWebRequest';

class EditAssociationsPageEmploymentRecordDefinition extends EmploymentRecordDefinition {
    EmployerName: string = '';
    MonthlyIncome: string = '';
    EmploymentStartDate: string = '';
    EmploymentEndDate: string = '';
    IsCurrent: string = '';
    EmploymentStatusType: string = '';
}

declare global {
    interface Window { LQBPopup: any; }
}

declare function getQueryStringVariable(variable: string): string;
declare var VRoot: string;
declare var LQBPopup: any;

const $ = jQuery;
const uladAssociationHelper = new UladAssociationHelper();
const parameterFactory = new LqbDataServiceRequestParameterFactory()
const booleanValueConverter = new BooleanLqbDataServiceResponseConverter();

angular
.module('EditAssociations', [])
.controller('EditAssociationsController', ['$scope', '$timeout', function OwnersController($scope, $timeout: ITimeoutService) {
    $scope.getEmploymentToDate = function (employmentRecord: Dictionary<string | boolean>): string {
        return getEmploymentToDate(
            employmentRecord['EmploymentEndDate'] as string,
            employmentRecord['IsCurrent'] as boolean,
            employmentRecord['EmploymentStatusType'] as string)
    }

    $scope.processResult = function (response: Dictionary<any>) {
        for (const employmentRecord of response['EmploymentRecords']) {
            var matchingOwner = $scope.selectedAssociations.find((record: Dictionary<string>) => record['Id'] === employmentRecord['Id']);
            employmentRecord.IsSelected = matchingOwner != null;
        }
    }

    $scope.applyResult = function (data: Dictionary<any>) {
        $scope.$apply(function () {
            const handler = new LqbDataServiceResponseHandler(data, uladAssociationHelper, booleanValueConverter);
            const response = handler.getResponseByKey(LqbDataServiceRequestType.Get, 'loan');

            $scope.processResult(response);
            $scope.vm.loan = Object.assign($scope.vm.loan, response);

            $timeout(() => {
                // Resize to fit content once the table has been bound
                // with the consumer data.
                parent.LQBPopup.Resize(NaN/*width*/, NaN/*height*/, {
                    isUsingDefaultWidth: true,
                    isUsingDefaultHeight: true
                });
            });
        });
    }

    $scope.loadData = function () {
        const args = parameterFactory.createIndividualGetParameter([EditAssociationsPageEmploymentRecordDefinition]);

        let provider = new LqbDataServiceRequestFormatProvider();
        provider.addRequest(LqbDataServiceRequestType.Get, args);

        sendWebRequest(
            LqbDataServiceOperationType.Load,
            provider.toRequest(),
            (data: Dictionary<any>) => { $scope.applyResult(data); });
    }

    $scope.onEditAssociations = function () {
        const selectedAssociations = $scope.vm.loan.EmploymentRecords.filter((record: Dictionary<boolean>) => record['IsSelected']);
        const returnArgs = { selectedAssociations };
        parent.LQBPopup.Return(returnArgs);
    }

    $scope.init = function () {
        $scope.vm = new Dictionary<Dictionary<any>>();
        $scope.vm.loan = new Dictionary<any>();

        const existingAssociationsCacheKey = getQueryStringVariable('s');

        var helper = new EditAssociationPopupHelper();
        helper.retrieveExistingAssociationIdsFromCache(existingAssociationsCacheKey, (result: Dictionary<any>) => {
            if (result.error) {
                alert(result.UserMessage);
                return;
            }

            const ownersJson = result.value['IdList'];
            $scope.selectedAssociations = JSON.parse(ownersJson);
            $scope.loadData();
        });
    }

    // Allow init scripts like service registration to complete
    // before running the controller's initialization.
    $timeout(() => {
        $scope.init();
    });
}]);