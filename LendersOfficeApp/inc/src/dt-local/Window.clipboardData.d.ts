﻿
interface DataTransfer {
    clearData(format?: string): boolean;
    setData(format: string, data: string): boolean;
    getData(format: string): string;
}

interface MSWindowExtensions {
    clipboardData: DataTransfer;
}

interface Window extends MSWindowExtensions {
    clipboardData: DataTransfer;
}

// https://github.com/Microsoft/TypeScript/blob/v1.5.0-alpha/bin/lib.d.ts