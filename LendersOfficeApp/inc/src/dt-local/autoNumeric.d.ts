interface AutoNumericOptions {
    aSep        ?: string;
    dGroup      ?: number;
    aDec        ?: string;
    altDec      ?: string;
    aSign       ?: string;
    pSign       ?: string;
    vMin        ?: number;
    vMax        ?: number;
    mDec        ?: number;
    mRound      ?: string;
    aPad        ?: boolean;
    nBracket    ?: string;
    wEmpty      ?: string;
    lZero       ?: string;
    aForm       ?: boolean;
    anDefault   ?: string;
}

interface JQuery {
    

    /**
     * Init
     *
     * @param command 'init'
     */
    autoNumeric(command: "init"): JQuery;

    /**
     * Init
     *
     * @param command 'init'
     * @param options AutoNumericOptions
     */
    autoNumeric(command: "init", options: AutoNumericOptions): JQuery;

    /**
     * Get
     *
     * @param command 'get'
     */
    autoNumeric(command: "get"): string;

    /**
     * Update
     *
     * @param command 'update'
     * @param options AutoNumericOptions
     */
    autoNumeric(command: "update", options: AutoNumericOptions): JQuery;

    /**
     * Set
     *
     * @param command 'set'
     * @param value Value
     */
    autoNumeric(command: "set", value: number|string): JQuery; 

    autoNumeric(command: string): string;
    autoNumeric(command: string): JQuery;
    autoNumeric(command: string, options: AutoNumericOptions): JQuery;
    autoNumeric(command: string, value: number | string): JQuery;
}