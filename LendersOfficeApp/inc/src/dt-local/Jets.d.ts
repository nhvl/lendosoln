// Type definitions for Jets.js v0.5.0
// Project: https://nexts.github.io/Jets.js/
// Definitions by: Rex Pan <https://github.com/rexpan>
// Definitions: https://github.com/borisyankov/DefinitelyTyped

declare var Jets: Jets.IJetsConstructor;
declare module "jets" {
    export = Jets;
}

declare module Jets {
    interface IJets {
        update(force: boolean):void;
        destroy():void;
    }

    interface IJetsOptions {
        searchTag                : string;
        contentTag               : string;
        columns                 ?: number[];
        addImportant            ?: boolean;
        searchSelector          ?: string;
        manualContentHandling   ?: (tag: Node) => string;
        didSearch               ?: (searchPhrase: string) => void;
        diacriticsMap           ?: { [letter: string]: string };
    }

    interface IJetsConstructor {
        new (options: IJetsOptions): IJets;
        prototype: IJets;
    }
}
