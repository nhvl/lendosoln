// https://igorescobar.github.io/jQuery-Mask-Plugin/
interface JQuery {
    mask(mask:(string|((val:string) => string)), options ?: {
        reverse        ?: boolean,
        clearIfNotMatch?: boolean,
        selectOnFocus  ?: boolean,
        placeholder    ?: string,
        translation    ?: {[key:string] : {patter:RegExp, optional?:boolean, recursive?:boolean}},

        // onComplete() : void,
        // onKeyPress(cep:string, event:JQueryEventObject, currentField:any, options:any) : void,
        // onChange():void,
        // onInvalid():void,

        byPassKeys    ?: number[], // keyCode http://www.cambiaresearch.com/articles/15/javascript-key-codes
    }):JQuery;
    unmask():JQuery;
}
interface JQueryStatic {
    jMaskGlobals: {
        maskElements  ?: string,
        dataMaskAttr  ?: string,
        dataMask      ?: boolean,
        watchInterval ?: number,
        watchInputs   ?: boolean,
        watchDataMask ?: boolean,
        byPassKeys    ?: number[],
        translation   ?: {[key:string]: {patter:RegExp, optional?:boolean, recursive?:boolean}},
    }
}