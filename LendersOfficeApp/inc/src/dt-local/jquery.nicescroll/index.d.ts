// Project: https://github.com/inuyaksa/jquery.nicescroll
// Definitions by: Rex Pan

/// <reference types="jquery" />

declare namespace nicescroll {
    interface INiceScrollOptions {
        cursorcolor                 ?: string, // "#424242", // change cursor color in hex
        cursoropacitymin            ?: number, // 0, // change opacity when cursor is inactive (scrollabar "hidden" state), range from 1 to 0
        cursoropacitymax            ?: number, // 1, // change opacity when cursor is active (scrollabar "visible" state), range from 1 to 0
        cursorwidth                 ?: string, // "5px", // cursor width in pixel (you can also write "5px")
        cursorborder                ?: string, // "1px solid #fff", // css definition for cursor border
        cursorborderradius          ?: string, // "5px", // border radius in pixel for cursor
        zindex                      ?: string | number, // "auto" | change z-index for scrollbar div
        scrollspeed                 ?: number, // 60, // scrolling speed
        mousescrollstep             ?: number, // 40, // scrolling speed with mouse wheel (pixel)
        touchbehavior               ?: boolean, // false, // enable cursor-drag scrolling like touch devices in desktop computer
        hwacceleration              ?: boolean, // true, // use hardware accelerated scroll when supported
        boxzoom                     ?: boolean, // false, // enable zoom for box content
        dblclickzoom                ?: boolean, // true, // (only when boxzoom=true) zoom activated when double click on box
        gesturezoom                 ?: boolean, // true, // (only when boxzoom=true and with touch devices) zoom activated when pinch out/in on box
        grabcursorenabled           ?: boolean, // true // (only when touchbehavior=true) display "grab" icon
        autohidemode                ?: (  // true, // how hide the scrollbar works, possible values:
                                            boolean    // if true then hide when no scrolling
                                            | "cursor" // only cursor hidden
                                            | "leave"  // hide only if pointer leaves content
                                            | "hidden" // hide always
                                            | "scroll" // show only on scroll
                                            ),
        background                  ?: string, // "", // change css for rail background
        iframeautoresize            ?: boolean, // true, // autoresize iframe on load event
        cursorminheight             ?: number, // 32, // set the minimum cursor height (pixel)
        preservenativescrolling     ?: boolean, // true, // you can scroll native scrollable areas with mouse, bubbling mouse wheel event
        railoffset                  ?: boolean, // false, // you can add offset top/left for rail position
        bouncescroll                ?: boolean, // false, // (only hw accell) enable scroll bouncing at the end of content as mobile-like
        spacebarenabled             ?: boolean, // true, // enable page down scrolling when space bar has pressed
        railpadding                 ?: { top: number, right: number, left: number, bottom: number }, // set padding for rail bar
        disableoutline              ?: boolean, // true, // for chrome browser, disable outline (orange highlight) when selecting a div with nicescroll
        horizrailenabled            ?: boolean, // true, // nicescroll can manage horizontal scroll
        railalign                   ?: string, // right, // alignment of vertical rail
        railvalign                  ?: string, // bottom, // alignment of horizontal rail
        enabletranslate3d           ?: boolean, // true, // nicescroll can use css translate to scroll content
        enablemousewheel            ?: boolean, // true, // nicescroll can manage mouse wheel events
        enablekeyboard              ?: boolean, // true, // nicescroll can manage keyboard events
        smoothscroll                ?: boolean, // true, // scroll with ease movement
        sensitiverail               ?: boolean, // true, // click on rail make a scroll
        enablemouselockapi          ?: boolean, // true, // can use mouse caption lock API (same issue on object dragging)
        cursorfixedheight           ?: boolean, // false, // set fixed height for cursor in pixel
        hidecursordelay             ?: number, // 400, // set the delay in microseconds to fading out scrollbars
        directionlockdeadzone       ?: number, // 6, // dead zone in pixels for direction lock activation
        nativeparentscrolling       ?: boolean, // true, // detect bottom of content and let parent to scroll, as native scroll does
        enablescrollonselection     ?: boolean, // true, // enable auto-scrolling of content when selection text
        cursordragspeed             ?: number, // 0.3, // speed of selection when dragged with cursor
        rtlmode                     ?: string, // "auto", // horizontal div scrolling starts at left side
        cursordragontouch           ?: boolean, // false, // drag cursor in touch / touchbehavior mode also
        oneaxismousemode            ?: string, //  "auto", // it permits horizontal scrolling with mousewheel on horizontal only content, if false (vertical-only) mousewheel don't scroll horizontally, if value is auto detects two-axis mouse
        scriptpath                  ?: string, // "" // define custom path for boxmode icons ("" => same script path)
        preventmultitouchscrolling  ?: boolean, // true // prevent scrolling on multitouch events
    }

    interface INiceScrollObject {
        hide(): void;
        show(): void;
        resize(): void;

        doScrollLeft(x: number, duration: number): void;
        doScrollTop (y: number, duration: number): void;
    }
}

export as namespace nicescroll;
export = nicescroll;

// declare module global {
//     interface JQuery {
//         niceScroll(options: nicescroll.INiceScrollOptions): JQuery;
//         getNiceScroll(): nicescroll.INiceScrollObject;
//     }
// }
