// https://github.com/Microsoft/TypeScript/blob/v1.8.10/lib/lib.dom.d.ts
interface TextRange {
    boundingHeight: number;
    boundingLeft: number;
    boundingTop: number;
    boundingWidth: number;
    htmlText: string;
    offsetLeft: number;
    offsetTop: number;
    text: string;
    collapse(start?: boolean): void;
    compareEndPoints(how: string, sourceRange: TextRange): number;
    duplicate(): TextRange;
    execCommand(cmdID: string, showUI?: boolean, value?: any): boolean;
    execCommandShowHelp(cmdID: string): boolean;
    expand(Unit: string): boolean;
    findText(string: string, count?: number, flags?: number): boolean;
    getBookmark(): string;
    getBoundingClientRect(): ClientRect;
    getClientRects(): ClientRectList;
    inRange(range: TextRange): boolean;
    isEqual(range: TextRange): boolean;
    move(unit: string, count?: number): number;
    moveEnd(unit: string, count?: number): number;
    moveStart(unit: string, count?: number): number;
    moveToBookmark(bookmark: string): boolean;
    moveToElementText(element: Element): void;
    moveToPoint(x: number, y: number): void;
    parentElement(): Element;
    pasteHTML(html: string): void;
    queryCommandEnabled(cmdID: string): boolean;
    queryCommandIndeterm(cmdID: string): boolean;
    queryCommandState(cmdID: string): boolean;
    queryCommandSupported(cmdID: string): boolean;
    queryCommandText(cmdID: string): string;
    queryCommandValue(cmdID: string): any;
    scrollIntoView(fStart?: boolean): void;
    select(): void;
    setEndPoint(how: string, SourceRange: TextRange): void;
}

interface HTMLInputElement {
    createTextRange(): TextRange;
}
