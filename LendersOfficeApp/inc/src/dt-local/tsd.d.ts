/// <reference path="./promise.d.ts" />
/// <reference path="./angularjs/index.d.ts" />
/// <reference path="./console.d.ts" />
/// <reference path="./HTMLInputElement.d.ts" />

declare module "*.html" {
    const content: string;
    export = content;
}

declare module "*.json" {
    const data: any;
    export = data;
}

declare const __DEV__: boolean;
