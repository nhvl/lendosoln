
declare module "react-addons-update" {
    function update<S>(state:S, x: any): void;
    export = update;
}
declare module "react-selection" {
    import {Component} from "react";
    interface ISelectionProps {
        target        : string;
        selectedClass?: string;
        isLimit      ?: boolean;
        afterSelect  ?: (selectedTargets: any[]) => void;
    }
    class Selection extends Component<ISelectionProps, {}> {

    }
    export default Selection;
    export {ISelectionProps};
}
declare module "react-selectable" {
    export const SelectableGroup: any;
    export const createSelectable: any;
}
declare module "react-highlight-words" {
    import {Component} from "react";
    interface IHighlighterProps {
        highlightClassName ?: string;
        highlightStyle     ?: {};
        sanitize           ?: (text: string) => string;
        searchWords         : string[];
        textToHighlight     : string;
        alterBoxCoordCalc  ?: boolean;
    }
    class Highlighter extends Component<IHighlighterProps, {}> {}
    export default Highlighter;
}

declare module "react-resizable" {
    import {Component, MouseEvent} from "react";
    interface IResizableProps {
        // Functions
        onResizeStop  ?: () => void;
        onResizeStart ?: () => void;
        onResize      ?: <T>(event: MouseEvent<T>, p: {element:HTMLElement, size:{width:number; height:number}}) => void;

        width          : number;
        height         : number;

        handleSize    ?: any[];

        draggableOpts ?: {};
    }
    class Resizable extends Component<IResizableProps, {}> {}

    interface IResizableBoxProps {
        lockAspectRatio?: boolean; // Preserves aspect

        minConstraints ?: [number, number]; // Constaints coords, pass [x,y]
        maxConstraints ?: [number, number];

        width          ?: number;
        height         ?: number;
    }
    class ResizableBox extends Component<IResizableBoxProps, {}> {}

    export {Resizable, ResizableBox};
}

declare module "react-waypoint" {
    // https://github.com/brigade/react-waypoint
    import {Component} from "react";
    // namespace "react-waypoint" {}

    interface IWayPointEventObject {
        currentPosition : string;
        previousPosition: string;
        event           : Event;
        waypointTop     : number; // waypoint's distance to the top of the viewport
        viewportTop     : number; // distance from the scrollable ancestor to the viewport top
        viewportBottom  : number; // distance from the bottom of the scrollable ancestor to the viewport top
    }
    interface IWaypointProps {
        // Functions
        onEnter             ?: (props: IWayPointEventObject) => void;
        onLeave             ?: (props: IWayPointEventObject) => void;
        onPositionChange    ?: (props: IWayPointEventObject) => void;
        topOffset           ?: number | string;
        bottomOffset        ?: number | string;
        scrollableAncestor  ?: any;
        fireOnRapidScroll   ?: boolean;
        debug               ?: boolean;
        throttleHandler     ?: Function;
    }
    class Waypoint extends Component<IWaypointProps, {}> {
        static below     : string;
        static above     : string;
        static inside    : string;
        static invisible : string;
    }

    export = Waypoint;
}

declare module "react-visibility-sensor" {
    // https://github.com/joshwnj/react-visibility-sensor
    import {Component} from "react";

    interface IVisibilitySensorProps {
        // Functions
        onChange            ?: (isVisible: boolean) => void;
        active              ?: boolean;
        partialVisibility   ?: boolean;
        minTopValue         ?: boolean;
        delay               ?: number;
        containment         ?: number;
        delayedCall         ?: boolean;
    }
    class VisibilitySensor extends Component<IVisibilitySensorProps, {}> {

    }

    export = VisibilitySensor;
}
