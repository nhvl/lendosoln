import {EditorController              } from "../ng-common/EditorController";
import {createBasicLoanEditorDirective} from "../ng-common/createBasicLoanEditorDirective";
import {ILqbDataModel                 } from "../ng-common/ILqbDataModel";
import {$filter                       } from "../ng-common/ngServices";

import * as appHtml from "./index.html";

class LendingStaffNotesController extends EditorController {
    PrependTimestamp(textField: ILqbDataModel){
        var timeString = $filter('date')(new Date(), 'MM/dd/yyyy hh:mm a');
        var userName = this.loan.BrokerUser.firstName + ' ' + this.loan.BrokerUser.lastName;

        var timestampString = timeString + '[' + userName + ']-' + '\n\n';

        textField.v = timestampString + textField.v;
    }
}

export const lendingStaffNotesDirective = createBasicLoanEditorDirective("lendingStaffNotes", appHtml, LendingStaffNotesController);
