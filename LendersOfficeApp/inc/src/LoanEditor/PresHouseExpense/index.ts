import {createBasicLoanEditorDirective} from "../ng-common/createBasicLoanEditorDirective";

import * as appHtml from "./index.html";

export const presHouseExpenseDirective = createBasicLoanEditorDirective("presHouseExpense", appHtml);
