export enum ModalStatus {
    Save = 2, //close and save modal
    Discard = 1, //close and not save modal
    Open = 0, //not close (leave modal open) modal
}
