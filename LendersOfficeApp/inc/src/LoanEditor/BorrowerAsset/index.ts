import {EditorController              } from "../ng-common/EditorController";
import {createBasicLoanEditorDirective} from "../ng-common/createBasicLoanEditorDirective";
import {ILqbDataModel                 } from "../ng-common/ILqbDataModel";
import {E_AssetT                      } from "../../DataLayer/Enums/E_AssetT";
import {LqModalController             } from "../components/lqModal";
import {ModalStatus                   } from "./ModalStatus";
import isEqual = require("lodash/isEqual");

import * as appHtml from "./index.html";

class BorrowerAssetController extends EditorController {
    selectedIndex: number;
    sortKey: string;
    isAsc: boolean;
    selectedRegular:{ [key: string]: ILqbDataModel };
    selectedRegularTemp: { [key: string]: ILqbDataModel };
    isAddNew: boolean;
    isSet:boolean;//Used in modal to check ok button is pressed or not
    isConfirmed:boolean;// confirm editor page is closed
    modalStatus:ModalStatus;
    modal: LqModalController;
    init() {
        this.selectedIndex = -1;
        this.sortKey = "";
        this.isSet=false;
        this.isConfirmed=false;
    }
    setModal(calculationModal: LqModalController):void {
        this.modal = calculationModal;
        // this.modal.setOption({ backdrop:'static',
        //     keyboard: false });
    }
    onModalHide(e:JQueryEventObject):void {
        if(this.modalStatus == ModalStatus.Open){
            //check if current regular is changed or not
            if(isEqual(angular.merge({},this.selectedRegular),this.selectedRegularTemp)) return;
            const m = `Do you wish to save this asset?`;
            this.dialog.ync(m, "Save?").then((o) => {
                    switch (o) {
                        case 1:
                            this.done();
                            return;
                        case 0:
                            this.cancel();
                            return;
                        case -1:
                            this.modalStatus = ModalStatus.Open;
                            return;
                        default:
                            console.error(o);
                            return;
                    }
                });
            e.preventDefault();
        }
    }
    done(): void {
        this.modalStatus = ModalStatus.Save;
        this.isAddNew = false;
        this.modal.close();
        this.selectedIndex = -1;
        this.loan.calculate();
    }
    cancel(): void {
        this.modalStatus = ModalStatus.Discard;
        if (this.isAddNew) {
            this.executeDelete(this.loan.app.aAssetCollection.regulars.length - 1);
            this.isAddNew = false;
        }
        else {
            this.loan.app.aAssetCollection.regulars[this.selectedIndex] = angular.merge({}, this.selectedRegularTemp);
        }
        this.modal.close();
        this.selectedIndex = -1;
        this.loan.calculate();
    }

    isWideScreen():boolean{
        return $( window ).width()>1280;
    }
    getWide():number{
        return $( window ).width();
    }
    setVODStatus(regular:{ [key: string]: ILqbDataModel }):boolean{
        if (regular == null || regular["AssetT"] == null) return false;
        switch (regular["AssetT"].v) {
             case E_AssetT.BridgeLoanNotDeposited:
             case E_AssetT.CertificateOfDeposit:
             case E_AssetT.Checking:
             case E_AssetT.GiftEquity:
             case E_AssetT.MoneyMarketFund:
             case E_AssetT.MutualFunds:
             case E_AssetT.Savings:
             case E_AssetT.SecuredBorrowedFundsNotDeposit:
             case E_AssetT.TrustFunds:
             case E_AssetT.PendingNetSaleProceedsFromRealEstateAssets:
             case E_AssetT.OtherLiquidAsset:
             return true;
            default:
                return false;
        }
    }
    getOwnerValue(regular:{ [key: string]: ILqbDataModel }){
        const x = regular["OwnerT"];
        const o = x.options.find(item => item.v === x.v);
        if (o == null) return null;
        return o.l;
    }
    getAssetValue(regular:{ [key: string]: ILqbDataModel }){
        if (regular == null || regular["AssetT"] == null) return null;
        const x = regular["AssetT"];
        const o = x.options.find(item => item.v === x.v);
        if (o != null && regular["OtherTypeDesc"].v != null && regular["OtherTypeDesc"].v!="") {
            if (o.v == E_AssetT.OtherIlliquidAsset || o.v == E_AssetT.OtherLiquidAsset)
                return regular["OtherTypeDesc"].v;
        }
        return (o == null) ? null : o.l;
    }
    isEmpty() {
        return (this.loan == null) ||
            (this.loan.app.aAssetCollection.regulars == null) ||
            (this.loan.app.aAssetCollection.regulars.length == 0);
    }

    clickItem(index: number) {
        this.selectedIndex = index;
        this.updateItem();
    }
    delete(index:number) {
        const m = `Do you want to delete this asset?`;
        this.dialog.confirm(m, "Confirm?")
            .then((isConfirmed) => {
                if (!isConfirmed) return false;
                this.executeDelete(index);
                return true;
            });
    }
    executeDelete(index:number): void {
        if (!this.isEmpty()) {
            if (this.loan.app.aAssetCollection.regulars.length == 1) {
                this.loan.app.aAssetCollection.regulars = [];
                this.loan.calculate();
                return;
            }
            this.loan.app.aAssetCollection.regulars.splice(index, 1);
            this.loan.calculate();
        }
    }
    sortRegulars(by: string) {
        if (this.loan.isCalculating) return;
        if (by == this.sortKey) this.isAsc = !this.isAsc;
        else {
            this.sortKey = by;
            this.isAsc = true;
        }
        var property = this.sortKey;
        var sortOrder = -1;
        if (this.isAsc) sortOrder = 1;
        this.loan.app.aAssetCollection.regulars = this.loan.app.aAssetCollection.regulars.sort(function (a, b) {
            var result = 0;
            if((typeof a[property].v) == (typeof b[property].v))
                    result = (a[property].v < b[property].v) ? -1 : (a[property].v > b[property].v) ? 1 : 0;
                else
                    result = (String(a[property].v) < String(b[property].v)) ? -1 : (String(a[property].v) > String(b[property].v)) ? 1 : 0;
            return result * sortOrder;
        });

        for (var i = 0; i < this.loan.app.aAssetCollection.regulars.length; i++) {
            if (this.selectedRegular["id"].v == this.loan.app.aAssetCollection.regulars[i]["id"].v) {
                this.selectedIndex = i;
                break;
            }
        }
        this.loan.calculate();
    }
    add() {
        //Object.assign({}, source)
        this.isAddNew = true;
        this.loan.app.aAssetCollection.regulars.push(angular.merge({}, this.loan.app.aAssetCollection.regularTemplate));
        this.selectedIndex = this.loan.app.aAssetCollection.regulars.length - 1;
        this.updateItem();
        this.loan.calculate();
    }
    updateItem(): void {
        if (this.isEmpty())
            this.selectedRegular = this.selectedRegularTemp = null;
        else {
            this.selectedRegular = this.loan.app.aAssetCollection.regulars[this.selectedIndex];
            this.selectedRegularTemp = angular.merge({}, this.loan.app.aAssetCollection.regulars[this.selectedIndex]);
            this.modalStatus=ModalStatus.Open;
            this.modal.show();
        }
    }

}
export const borrowerAssetDirective = createBasicLoanEditorDirective("borrowerAsset", appHtml, BorrowerAssetController);
