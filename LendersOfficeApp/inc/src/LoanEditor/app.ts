import "./polyfill";

import {ngServiceRunner} from "./ng-common/ngServices";

import {ServiceFactory                              } from "./ng-common/service";
import {DataModels                                  } from "./ng-common/DataModels";
import {lqbInput                                    } from "./ng-common/lqbInput";
import {EditorController                            } from "./ng-common/EditorController";
import {lqModal                                     } from "./components/lqModal";
import {mortgageLoanOriginationAgreementDirective   } from "./Forms/MortgageLoanOriginationAgreement";
import {mortgageBrokerageBusinessContractDirective  } from "./Forms/MortgageBrokerageBusinessContract";
import {loan1003page1Directive                      } from "./Forms/Loan1003page1";
import {loan1003page2Directive                      } from "./Forms/Loan1003page2";
import {loan1003page3Directive                      } from "./Forms/Loan1003page3";
import {loan1003page4Directive                      } from "./Forms/Loan1003page4";
import {licenseList                                 } from "./components/LicenseList";
import {amortizationTableModal                      } from "./components/AmortizationTableModal";
import {loanTemplatePopulator                       } from "./components/LoanTemplatePopulator";
import {rateFloorCalculator                         } from "./components/RateFloorCalculator";
import {agentRecord                                 } from "./components/AgentRecord";

import {alternateLenderModal                } from "./AlternateLenders/index";
import {borrowerListDirective               } from "./BorrowerList/index";
import {loanInfoDirective                   } from "./LoanInfo/index";
import {constructionLoanInfoDirective       } from "./ConstructionLoanInfo/index";
import {propertyInfoDirective               } from "./PropertyInfo/index";
import {monthlyIncomeDirective              } from "./MonthlyIncome/index";
import {brokerRatelockDirective             } from "./LockDesk/BrokerRateLock/index";
import {titleAndEstateDirective             } from "./TitleAndEstate/index";
import {declarationsDirective               } from "./Declarations/index";
import {loanOfficerLicensingInfoDirective   } from "./LoanOfficerLicensingInfo/index";
import {upfrontMipffDirective               } from "./UpfrontMipff/index";
import {borrowerClosingCostsDirective       } from "./Disclosure/BorrowerClosingCosts/index";
import {disclosureCenterDirective           } from "./Disclosure/DisclosureCenter/index";
import {borrowerInfoDirective               } from "./BorrowerInfo/index";
import {statusProcessingDirective           } from "./Status/Processing/index";
import {statusGeneralDirective              } from "./Status/General";
import {viewBrokerRateLockDirective         } from "./Status/viewBrokerRateLock/index";
import {statusCommissionDirective           } from "./Status/Commission/index";
import {hcoListDirective                    } from "./Status/HomeownerCounselingOrganizationList/index";

import {continuationDataDirective           } from "./ContinuationData/index";
import {detailsOfTransactionDirective       } from "./DetailsOfTransaction/index";
import {governmentMonitoringDataDirective   } from "./GovernmentMonitoringData/index";
import {presHouseExpenseDirective           } from "./PresHouseExpense/index";
import {propertyRentalIncomeDirective       } from "./PropertyRentalIncome/index";
import {refiConstructionLoanDirective       } from "./RefiConstructionLoan/index";
import {otherFinancingDirective             } from "./OtherFinancing/index";
import {creditScoresDirective               } from "./CreditScores/index";
import {customFieldsDirective               } from "./CustomFields/index";
import {customFields2Directive              } from "./CustomFields2/index";
import {customFields3Directive              } from "./CustomFields3/index";
import {propertyDetailDirective             } from "./PropertyDetail/index";
import {transmittalDirective                } from "./Forms/Transmittal/index";
import {transmittalCombinedDirective        } from "./Forms/TransmittalCombined/index";
import {floodHazardNoticeDirective          } from "./Forms/FloodHazardNotice/index";
import {goodbyeLetterDirective              } from "./Forms/GoodbyeLetter/index";
import {taxReturnDirective                  } from "./Forms/TaxReturn/index";
import {creditDenialDirective               } from "./Forms/CreditDenial/index";
import {lenderCreditsDirective              } from "./Forms/LenderCredits/index";
import {taxFormDirective                    } from "./Forms/TaxForm/index";
import {borrowerAssetDirective              } from "./BorrowerAsset/index";
import {borrowerLiabilityDirective          } from "./BorrowerLiability/index";
import {nmlsCallReportDirective             } from "./NMLSCallReport/index";
import {additionalHelocDirective            } from "./AdditionalHELOC/index";
import {borrowerAliasesModal                } from "./BorrowerAliasesPOA/index";
import {lendingStaffNotesDirective          } from "./LendingStaffNotes/index";
import {reoDirective                        } from "./Reo/index";
import {statusTrustAccountsDirective        } from "./Status/TrustAccount/index";
import {statusHmdaDirective                 } from "./Status/HMDA/index";
import {statusAgentsDirective               } from "./Status/Agents/index";
import {statusAgentEditorDirective          } from "./Status/AgentEditor/index";
import {ccTemplatePicker                    } from "./Template/ClosingCost/ClosingCostTemplatePicker";

import {aggregateEscrowDisclosureDirective  } from "./Forms/aggregateEscrowDisclosure";
import {loanCommitmentDirective             } from "./Forms/LoanCommitment";
import {escrowImpoundsSetupDirective        } from "./Forms/EscrowImpoundsSetup";
import {loanSubmissionDirective             } from "./Forms/LoanSubmission";
import {changeOfCircumstancesNewDirective   } from "./Forms/ChangeOfCircumstancesNew";
import {truthInLendingDirective             } from "./Forms/TruthInLending";
import {truthInLending32Directive           } from "./Forms/TruthInLending32";
import {txMortgageBrokerDisclosureDirective } from "./Forms/TxMortgageBrokerDisclosure";
import {usdaLoanConditionalCommitmentDirective} from "./Forms/UsdaLoanConditionalCommitment";
import {usdaLoanGuaranteeDirective          } from "./Forms/UsdaLoanGuarantee";

import {requestForAppraisalDirective        } from "./RequestForms/requestForAppraisal";
import {requestForInsuranceDirective        } from "./RequestForms/RequestForInsurance";
import {requestForTitleDirective            } from "./RequestForms/requestForTitle";
import {surveyRequestDirective              } from "./RequestForms/SurveyRequest";

import {initialEscrowAccSetupModal } from "./components/InitialEscrowAccSetup";
import {loanTermsDirective                  } from "./Forms/LoanTerms";

import {loanStatusListModal                 } from "./View/LoanStatusList";

import {leftNavComponent} from "./components/leftNav/leftNav";
import {leAppDirective                      } from "./leApp";

import {StateProvider, UrlRouter} from "angular-ui-router";
stateConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
function stateConfig($stateProvider: StateProvider, $urlRouterProvider: UrlRouter): void {
    $urlRouterProvider.otherwise("/loanInfo");
    $stateProvider
        .state("borrowerList"                     , { url: "/borrowerList"                     , template: getTemplate(borrowerListDirective                     ) })
        .state("constructionLoanInfo"             , { url: "/constructionLoanInfo"             , template: getTemplate(constructionLoanInfoDirective             ) })
        .state("mortgageLoanOriginationAgreement" , { url: "/mortgageLoanOriginationAgreement" , template: getTemplate(mortgageLoanOriginationAgreementDirective ) })
        .state("mortgageBrokerageBusinessContract", { url: "/mortgageBrokerageBusinessContract", template: getTemplate(mortgageBrokerageBusinessContractDirective) })
        .state("loan1003page1"                   , { url: "/loan1003page1"                   , template: getTemplate(loan1003page1Directive                   ) })
        .state("loan1003page2"                   , { url: "/loan1003page2"                   , template: getTemplate(loan1003page2Directive                   ) })
        .state("loan1003page3"                   , { url: "/loan1003page3"                   , template: getTemplate(loan1003page3Directive                   ) })
        .state("loan1003page4"                   , { url: "/loan1003page4"                   , template: getTemplate(loan1003page4Directive                   ) })

        .state("loanInfo"                , { url: "/loanInfo"                , template: getTemplate(loanInfoDirective                ) })
        .state("propertyInfo"            , { url: "/propertyInfo"            , template: getTemplate(propertyInfoDirective            ) })
        .state("monthlyIncome"           , { url: "/monthlyIncome"           , template: getTemplate(monthlyIncomeDirective           ) })
        .state("brokerRatelock"          , { url: "/brokerRatelock"          , template: getTemplate(brokerRatelockDirective          ) })
        .state("titleAndEstate"          , { url: "/titleAndEstate"          , template: getTemplate(titleAndEstateDirective          ) })
        .state("declarations"            , { url: "/declarations"            , template: getTemplate(declarationsDirective            ) })
        .state("loanOfficerLicensingInfo", { url: "/loanOfficerLicensingInfo", template: getTemplate(loanOfficerLicensingInfoDirective) })
        .state("upfrontMipff"            , { url: "/upfrontMipff"            , template: getTemplate(upfrontMipffDirective            ) })
        .state("borrowerClosingCosts"    , { url: "/borrowerClosingCosts"    , template: getTemplate(borrowerClosingCostsDirective    ) })
        .state("disclosureCenter"        , { url: "/disclosureCenter"        , template: getTemplate(disclosureCenterDirective        ) })
        .state("borrowerInfo"            , { url: "/borrowerInfo"            , template: getTemplate(borrowerInfoDirective            ) })
        .state("statusProcessing"        , { url: "/statusProcessing"        , template: getTemplate(statusProcessingDirective        ) })
        .state("statusGeneral"           , { url: "/statusGeneral"           , template: getTemplate(statusGeneralDirective           ) })
        .state("viewBrokerRateLock"      , { url: "/viewBrokerRateLock"      , template: getTemplate(viewBrokerRateLockDirective      ) })
        .state("statusCommission"        , { url: "/statusCommission"        , template: getTemplate(statusCommissionDirective        ) })
        .state("hcoList"                 , { url: "/hcoList"                 , template: getTemplate(hcoListDirective                 ) })

        .state("continuationData"        , { url: "/continuationData"        , template: getTemplate(continuationDataDirective        ) })
        .state("detailsOfTransaction"    , { url: "/detailsOfTransaction"    , template: getTemplate(detailsOfTransactionDirective    ) })
        .state("governmentMonitoringData", { url: "/governmentMonitoringData", template: getTemplate(governmentMonitoringDataDirective) })
        .state("presHouseExpense"        , { url: "/presHouseExpense"        , template: getTemplate(presHouseExpenseDirective        ) })
        .state("propertyRentalIncome"    , { url: "/propertyRentalIncome"    , template: getTemplate(propertyRentalIncomeDirective    ) })

        .state("refiConstructionLoan"    , { url: "/refiConstructionLoan"    , template: getTemplate(refiConstructionLoanDirective    ) })
        .state("otherFinancing"          , { url: "/otherFinancing"          , template: getTemplate(otherFinancingDirective          ) })
        .state("creditScores"            , { url: "/creditScores"            , template: getTemplate(creditScoresDirective            ) })
        .state("customFields"            , { url: "/customFields"            , template: getTemplate(customFieldsDirective            ) })
        .state("customFields2"           , { url: "/customFields2"           , template: getTemplate(customFields2Directive           ) })
        .state("customFields3"           , { url: "/customFields3"           , template: getTemplate(customFields3Directive           ) })
        .state("propertyDetail"          , { url: "/propertyDetail"          , template: getTemplate(propertyDetailDirective          ) })
        .state("transmittal"             , { url: "/transmittal"             , template: getTemplate(transmittalDirective             ) })
        .state("transmittalCombined"     , { url: "/transmittalCombined"     , template: getTemplate(transmittalCombinedDirective     ) })
        .state("floodHazardNotice"       , { url: "/floodHazardNotice"       , template: getTemplate(floodHazardNoticeDirective       ) })
        .state("goodbyeLetter"           , { url: "/goodbyeLetter"           , template: getTemplate(goodbyeLetterDirective           ) })
        .state("taxReturn"               , { url: "/taxReturn"               , template: getTemplate(taxReturnDirective          ) })
        .state("creditDenial"            , { url: "/creditDenial"            , template: getTemplate(creditDenialDirective            ) })
        .state("lenderCredits"           , { url: "/lenderCredits"           , template: getTemplate(lenderCreditsDirective           ) })
        .state("taxForm/params"          , { url: "/taxForm/:version"        , template: getTemplate(taxFormDirective                 ) })
        .state("taxForm"                 , { url: "/taxForm"                 , template: getTemplate(taxFormDirective                 ) })
        .state("borrowerAsset"           , { url: "/borrowerAsset"           , template: getTemplate(borrowerAssetDirective           ) })
        .state("borrowerLiability"       , { url: "/borrowerLiability"       , template: getTemplate(borrowerLiabilityDirective       ) })
        .state("borrowerLiability/params", { url: "/borrowerLiability/:LiaId", template: getTemplate(borrowerLiabilityDirective) })
        .state("nmlsCallReport"          , { url: "/nmlsCallReport"          , template: getTemplate(nmlsCallReportDirective          ) })
        .state("additionalHeloc"         , { url: "/additionalHeloc"         , template: getTemplate(additionalHelocDirective         ) })
        .state("lendingStaffNotes"       , { url: "/lendingStaffNotes"       , template: getTemplate(lendingStaffNotesDirective       ) })
        .state("reo"                     , { url: "/reo"                     , template: getTemplate(reoDirective                     ) })
        .state("statusTrustAccount"      , { url: "/statusTrustAccount"      , template: getTemplate(statusTrustAccountsDirective     ) })
        .state("statusHmda"              , { url: "/statusHmda"              , template: getTemplate(statusHmdaDirective              ) })
        .state("statusAgents"            , { url: "/statusAgents"            , template: getTemplate(statusAgentsDirective            ) })
        .state("statusAgentEditor"       , { url: "/statusAgentEditor"       , template: getTemplate(statusAgentEditorDirective       ) })
        .state("statusAgentEditor/params", { url: "/statusAgentEditor/:RecId/:orderby/:source/:tab", template: getTemplate(statusAgentEditorDirective       ) })

        .state("aggregateEscrowDisclosure"    , { url: "/aggregateEscrowDisclosure"    , template: getTemplate(aggregateEscrowDisclosureDirective    ) })
        .state("loanCommitment"               , { url: "/loanCommitment"               , template: getTemplate(loanCommitmentDirective              ) })
        .state("escrowImpoundsSetup"       , { url: "/escrowImpoundsSetup"       , template: getTemplate(escrowImpoundsSetupDirective       ) })
        .state("loanSubmission"               , { url: "/loanSubmission"               , template: getTemplate(loanSubmissionDirective               ) })
        .state("changeOfCircumstancesNew"     , { url: "/changeOfCircumstancesNew"     , template: getTemplate(changeOfCircumstancesNewDirective     ) })
        .state("truthInLending"            , { url: "/truthInLending"            , template: getTemplate(truthInLendingDirective            ) })
        .state("truthInLending32"             , { url: "/truthInLending32"             , template: getTemplate(truthInLending32Directive             ) })
        .state("txMortgageBrokerDisclosure"   , { url: "/txMortgageBrokerDisclosure"   , template: getTemplate(txMortgageBrokerDisclosureDirective   ) })
        .state("loanTerms", { url: "/loanTerms", template: getTemplate(loanTermsDirective) })

        .state("usdaLoanConditionalCommitment", { url: "/usdaLoanConditionalCommitment", template: getTemplate(usdaLoanConditionalCommitmentDirective) })
        .state("usdaLoanGuarantee"            , { url: "/usdaLoanGuarantee"            , template: getTemplate(usdaLoanGuaranteeDirective            ) })

        .state("requestForAppraisal"          , { url: "/requestForAppraisal"          , template: getTemplate(requestForAppraisalDirective          ) })
        .state("requestForInsurance"          , { url: "/requestForInsurance"          , template: getTemplate(requestForInsuranceDirective          ) })
        .state("requestForTitle"              , { url: "/requestForTitle"              , template: getTemplate(requestForTitleDirective              ) })
        .state("surveyRequest"                , { url: "/surveyRequest"                , template: getTemplate(surveyRequestDirective                ) })

        .state("TODO"                    , { url: "/TODO"                    , template: '<p>Invalid page or not yet implemented</p>'   })
    ;
}

import {module} from "angular";
const ngSanitizeNgName:string = require("angular-sanitize");
import uiRouterNgName from "angular-ui-router";
const uiSelectNgName:string = require("ui-select"); import "ui-select/dist/select.min.css";
const dragularNgName:string = require("dragular");

export const app = module("app", [ngSanitizeNgName, uiRouterNgName, uiSelectNgName, dragularNgName])
    .run(ngServiceRunner)
    .service  (ServiceFactory      .ngName, ServiceFactory      )
    .factory  (DataModels          .ngName, DataModels.ngFactory)
    .directive(lqbInput            .ngName, lqbInput            )
    .config(["$rootScopeProvider", ($rootScopeProvider: any) => { $rootScopeProvider.digestTtl(30); }])
    .config(stateConfig)
    ;


import {LoadingIndicator} from "./ng-common/loadingIndicator";
import {Dialog} from "../ng-common/dialog";
([
    Dialog,
    LoadingIndicator,
]).forEach(d => { app.service(d.ngName, d) });

import {ngNicescroll} from "../ng-directives/angular-nicescroll";
import {reRefDirective} from "../ng-directives/reRef";
import {bsCollapseDirective} from "../ng-directives/bsCollapse";
([
    ngNicescroll,
    reRefDirective,
    bsCollapseDirective,
]).forEach(d => { app.directive(d.ngName, d) });

import {userSettingsModal} from "./UserSettings"
([
    lqModal,
    alternateLenderModal,
    amortizationTableModal,
    loanTemplatePopulator,
    rateFloorCalculator,
    ccTemplatePicker,
    initialEscrowAccSetupModal,
    loanStatusListModal,
    borrowerAliasesModal,
    licenseList,
    agentRecord,

    leftNavComponent,
    userSettingsModal,
].forEach((c: ng.IComponentOptions) => {
    app.component(c.ngName, c);
}));

[
    constructionLoanInfoDirective,
    loan1003page1Directive,
    loan1003page2Directive,
    loan1003page3Directive,
    loan1003page4Directive,
    mortgageLoanOriginationAgreementDirective,
    mortgageBrokerageBusinessContractDirective,

    propertyInfoDirective,
    loanInfoDirective,
    monthlyIncomeDirective,
    brokerRatelockDirective,
    titleAndEstateDirective,
    declarationsDirective,
    loanOfficerLicensingInfoDirective,
    upfrontMipffDirective,
    borrowerClosingCostsDirective,
    disclosureCenterDirective,
    borrowerInfoDirective,
    statusProcessingDirective,
    statusGeneralDirective,
    viewBrokerRateLockDirective,
    statusCommissionDirective,
    hcoListDirective,

    continuationDataDirective,
    detailsOfTransactionDirective,
    governmentMonitoringDataDirective,
    presHouseExpenseDirective,
    propertyRentalIncomeDirective,
    refiConstructionLoanDirective,
    otherFinancingDirective,
    creditScoresDirective,
    customFieldsDirective,
    customFields2Directive,
    customFields3Directive,
    propertyDetailDirective,
    transmittalDirective,
    transmittalCombinedDirective,
    floodHazardNoticeDirective,
    goodbyeLetterDirective,
    taxReturnDirective,
    creditDenialDirective,
    lenderCreditsDirective,
    taxFormDirective,
    borrowerAssetDirective,
    borrowerListDirective,
    borrowerLiabilityDirective,
    nmlsCallReportDirective,
    additionalHelocDirective,
    lendingStaffNotesDirective,
    reoDirective,
    statusTrustAccountsDirective,
    statusHmdaDirective,
    statusAgentsDirective,
    statusAgentEditorDirective,

    aggregateEscrowDisclosureDirective,
    loanCommitmentDirective,
    truthInLendingDirective,
    escrowImpoundsSetupDirective,
    loanSubmissionDirective,
    changeOfCircumstancesNewDirective,
    loanTermsDirective,
    truthInLending32Directive,
    txMortgageBrokerDisclosureDirective,
    usdaLoanConditionalCommitmentDirective,
    usdaLoanGuaranteeDirective,

    requestForAppraisalDirective,
    requestForInsuranceDirective,
    requestForTitleDirective,
    surveyRequestDirective,

    leAppDirective,
].forEach(d => { app.directive(d.ngName, d) });

// loanInfoDirective => <loan-info ng2-var="vm.loanEditor">
function getTemplate(directive: (_1 ?:any) => ng.IDirective) {
    const directiveTag = directive.ngName.replace(/([a-z])([A-Z])/g, "$1-$2").toLowerCase();
    return `<${directiveTag} ng2-var="vm.loanEditor"
        on-summary-changed="vm.onSummaryChanged($summary, $applicants)"
        on-state-changed="vm.navToPage($pageStateId, $pageStateParams)"
        >`;
}

import { productionConfig } from "../ng-common/productionConfig"
if (!__DEV__) app.config(productionConfig);
