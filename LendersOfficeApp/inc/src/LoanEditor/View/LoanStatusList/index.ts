/*

```html
<loan-status-list-modal
    ref="vm.lslModal = $ref"
    open="vm.isShowLslModal"
    channel="vm.branchChannel"
    process="vm.correspondentProcess"
    on-complete="vm.setStatus($status)"
    on-cancel="vm.cancelSetStatus()"
    ></loan-status-list-modal>
```

```typescript
class SomeController {
    isShowLslModal: boolean;

    openLslModal() {
        this.isShowLslModal = true;
    }

    setStatus(status: E_sStatusT) {
        this.isShowLslModal = false;
        this.loan.fields.sStatusT.v = status;
    }

    cancelSetStatus() {
        this.isShowLslModal = false;
    }
}
```

Register

```js
import {loanStatusListModal} from ".../LoanStatusList";
angular("app").component(loanStatusListModal.ngName, loanStatusListModal);
```

*/

import {ILqbDataModel,updateFromRaw   } from "../../ng-common/ILqbDataModel";
import {IApplicant                    } from "../../ng-common/IApplicant";
import {LqModalController             } from "../../components/lqModal";

import {E_sStatusT                    } from "../../../DataLayer/Enums/E_sStatusT";
import {E_BranchChannelT              } from "../../../DataLayer/Enums/E_BranchChannelT";
import {E_sCorrespondentProcessT      } from "../../../DataLayer/Enums/E_sCorrespondentProcessT";

import {ServiceFactory                } from "./../../ng-common/service/ServiceFactory";


import * as appHtml from "./index.html";

export class LoanStatusListController {
    private loading      : boolean;
    private loanStatuses : {label:string, value:E_sStatusT}[];
    private leadStatuses : {label:string, value:E_sStatusT}[];

    private leadOnly: boolean;
    private channel: E_BranchChannelT;
    private process: E_sCorrespondentProcessT;

    private modal: LqModalController;

    private ref       : (_: {$ref  : LoanStatusListController}) => void;
    private onComplete: (_: {$status: E_sStatusT}) => void;

    static $inject = [ServiceFactory.ngName]
    constructor(private service: ServiceFactory) {
        // code...
    }

    private $onInit(){
        if (!!this.ref) this.ref({ $ref: this }); else debugger;
    }

    private $onChanges({open}: {
        open: ng.IChangesObject<boolean>;
    }) {
        if (open != null && open.currentValue != open.previousValue) {
            if (open.currentValue) {
                this.modal.show();

                this.loading = true;
                this.service.loanStatus.getLoanStatus({ channel: this.channel, process: this.process })
                .then(({loan, lead}) => {
                    this.loading = false;
                    this.loanStatuses = loan;
                    this.leadStatuses = lead;
                });
            } else {
                if (this.modal) {
                    this.modal.close();
                }
            }
        }
    }

    private selectStatus($status: E_sStatusT) {
        this.onComplete({$status});
    }

    private setModal(modal: LqModalController){
        this.modal = modal
    }
}

export const loanStatusListModal: ng.IComponentOptions = {
    ngName         : "loanStatusListModal",
    bindings       : {
        open       : "<",
        leadOnly   : "<",
        channel    : "<",
        process    : "<",

        ref        : "&",
        onComplete : "&",
        onCancel   : "&",
    },
    controllerAs   : "vm",
    template       : appHtml,
    controller     : LoanStatusListController as any,
};
