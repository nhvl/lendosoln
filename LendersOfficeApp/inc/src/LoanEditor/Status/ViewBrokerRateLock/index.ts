import {E_sOriginatorCompensationLenderFeeOptionT} from "../../../DataLayer/Enums/E_sOriginatorCompensationLenderFeeOptionT";
import {E_sOriginatorCompensationPaymentSourceT  } from "../../../DataLayer/Enums/E_sOriginatorCompensationPaymentSourceT";
import {E_sOriginatorCompensationPlanT           } from "../../../DataLayer/Enums/E_sOriginatorCompensationPlanT";
import {E_RateLockAuditActionT,
        eventType2Action                         } from "../../../DataLayer/Enums/E_RateLockAuditActionT";

import {EditorController              } from "../../ng-common/EditorController";
import {createBasicLoanEditorDirective} from "../../ng-common/createBasicLoanEditorDirective";
import {ILqbDataModel                 } from "../../ng-common/ILqbDataModel";
import {DataModels                    } from "../../ng-common/DataModels";

import * as appHtml from "./index.html";

const ocpsLabels = new Map<E_sOriginatorCompensationPaymentSourceT, string>([
    [ E_sOriginatorCompensationPaymentSourceT.SourceNotSpecified, "Payment source not specified" ],
    [ E_sOriginatorCompensationPaymentSourceT.BorrowerPaid      , "Borrower paid"                ],
    [ E_sOriginatorCompensationPaymentSourceT.LenderPaid        , "Lender paid"                  ],
] as any);


class ViewBrokerRateLockController extends EditorController {
    private E_sOriginatorCompensationLenderFeeOptionT = E_sOriginatorCompensationLenderFeeOptionT;
    private E_sOriginatorCompensationPaymentSourceT = E_sOriginatorCompensationPaymentSourceT;

    private displayOriginatorCompLines: boolean;
    protected onLoaded(loan: DataModels){
        super.onLoaded(loan);
        const {sOriginatorCompensationLenderFeeOptionT} = loan.fields;
        this.displayOriginatorCompLines = sOriginatorCompensationLenderFeeOptionT.v == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;

        try {
            const rateLockHistory = this.loan.dataContents.sRateLockHistoryXmlContent.RateLockHistory;
            rateLockHistory.Event = rateLockHistory.Event.map(e => Object.assign({}, e, eventType2Action(e.EvenType)));
        } catch (e) { console.error(e); }

        return true;
    }
    private ocpsLabel(v: E_sOriginatorCompensationPaymentSourceT) : string {
        return ocpsLabels.has(v) ? ocpsLabels.get(v) : E_sOriginatorCompensationPaymentSourceT[v] || "";
    }
    private canShowDetail(){
        if (!this.loan) return false;
        const {sOriginatorCompensationPaymentSourceT, sHasOriginatorCompensationPlan} = this.loan.fields;
        return (sOriginatorCompensationPaymentSourceT.v === E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
            sHasOriginatorCompensationPlan.v);
    }
    private originatorPlanDetails: string;
    private showDetail(){
        const {
            sOriginatorCompensationPlanT,
            sOriginatorCompensationAppliedBy,
            sOriginatorCompensationPlanSourceEmployeeName,
        } = this.loan.fields;
        const {$element} = this;
        this.originatorPlanDetails = getOrigCompPlanDesc(
            sOriginatorCompensationPlanT                 .v,
            sOriginatorCompensationAppliedBy             .v,
            sOriginatorCompensationPlanSourceEmployeeName.v);
        $element.find(".modal").modal("show");
    }
}

export const viewBrokerRateLockDirective = createBasicLoanEditorDirective("viewBrokerRateLock", appHtml, ViewBrokerRateLockController);

function getOrigCompPlanDesc(plan: E_sOriginatorCompensationPlanT, appliedBy: string, sourceEmployeeName: string) {
    const {Manual, FromEmployee} = E_sOriginatorCompensationPlanT;
    return (
        (plan === Manual      ) ? `Plan set manually by ${appliedBy}` :
        (plan === FromEmployee) ? `Plan set from the employee record${(!sourceEmployeeName) ? `, ` : ` of ${sourceEmployeeName}`}` :
                                  `UnhandledEnumException: E_sOriginatorCompensationPlanT = ${plan}`
    );
}
/*

sRLckdDays.maxLength = 4

*/
