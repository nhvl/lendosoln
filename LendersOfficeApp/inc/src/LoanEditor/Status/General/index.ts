/// <reference types="jquery"/>

import * as moment from "moment";
import {$timeout} from "../../ng-common/ngServices";

import {StrGuid, guidEmpty} from "../../../utils/guid";

import {StatusProcessingController    } from "../Processing/index";
import {createBasicLoanEditorDirective} from "../../ng-common/createBasicLoanEditorDirective";
import {DataModels                    } from "../../ng-common/DataModels";
import {AlternateLenderController     } from "../../AlternateLenders";

import {E_BranchChannelT              } from "../../../DataLayer/Enums/E_BranchChannelT";
import {E_sCorrespondentProcessT      } from "../../../DataLayer/Enums/E_sCorrespondentProcessT";

import {E_TriState                    } from "../../../DataLayer/Enums/E_TriState";
import {E_sStatusT,
        E_sStatusT_map_Date           } from "../../../DataLayer/Enums/E_sStatusT";

import * as appHtml from "./index.html";

const defaultHideRow: { [key:string] : boolean } = {
    sClosedD                    : true,
    sSubmittedForPurchaseReviewD: true,
    sInPurchaseReviewD          : true,
    sPrePurchaseConditionsD     : true,
    sSubmittedForFinalPurchaseD : true,
    sInFinalPurchaseReviewD     : true,
    sClearToPurchaseD           : true,
    sPurchasedD                 : true,
    sReadyForSaleD              : true,
    sShippedToInvestorD         : true,
};

export class StatusGeneralController extends StatusProcessingController {
    private guidEmpty = guidEmpty;

    private E_BranchChannelT = E_BranchChannelT;
    private E_TriState = E_TriState;

    private hideRow: { [key:string] : boolean };

    private daySinceOpened: number;
    private isDocMagicEnabled: boolean;
    onLoaded(loan:DataModels) {
        super.onLoaded(loan);

        const {sClosedD, sOpenedD} = loan.fields;
        if (sClosedD != null && sOpenedD != null) {
            const now : Date = new Date();
            const c = !sClosedD.v ? moment() : moment(sClosedD.v as any, "MM/DD/YYYY");
            const o = !sOpenedD.v ? moment() : moment(sOpenedD.v as any, "MM/DD/YYYY");
            this.daySinceOpened = c.diff(o, "days");
        }

        this.isDocMagicEnabled = loan.BrokerUser.documentVendorBrokerSettings.BrokerId !== guidEmpty;

        this.on_sBranchChannelT_changed();

        // Make panel collapsible
        this.$element.find(".panel .panel-heading").on("click", function(event){
            $(this).parent(".panel").find(".panel-body").toggle();
        });

        return true;
    }

    private on_sBranchChannelT_changed() {
        const {EnabledStatusesByChannel} = this.loan.BrokerDB;
        const {sBranchChannelT} = this.loan.fields;
        const {sCorrespondentProcessT} = this.loan.fields;

        if (sBranchChannelT.v == null) return;

        const xs: E_sStatusT[] = ((sBranchChannelT.v === E_BranchChannelT.Correspondent)
            ? EnabledStatusesByChannel.Correspondent[E_sCorrespondentProcessT[sCorrespondentProcessT.v]]
            : (EnabledStatusesByChannel[E_BranchChannelT[sBranchChannelT.v]] as E_sStatusT[]));


        if (xs == null || !Array.isArray(xs)) { debugger; }
        else {
            const hideRow = this.hideRow = Object.assign({}, defaultHideRow);
            E_sStatusT_map_Date.forEach(([statusT, statusId, isSkip]: [E_sStatusT, string, boolean]) => {
                if (isSkip) return;
                hideRow[statusId] = !xs.includes(statusT) && ((this.loan.fields[statusId] == null) || (this.loan.fields[statusId].v == null));
            });
        }

        {
            const hideRow = this.hideRow;
            if (sBranchChannelT.v !== E_BranchChannelT.Broker && sBranchChannelT.v !== E_BranchChannelT.Correspondent) {
                hideRow["sSuspendedByInvestorD"] =
                hideRow["sCondSentToInvestorD" ] = false;
            }
            hideRow["sAdditionalCondSentD"] = hideRow["sCondSentToInvestorD"];
        }
    }

    private showLeadNew() {
        const {sBranchChannelT, sLeadD} = this.loan.fields;
        return !(
            [E_BranchChannelT.Wholesale, E_BranchChannelT.Correspondent].includes(sBranchChannelT.v) && sLeadD.v == ""
        );
    }
    private showCorrespondentProcess(){
        const {sBranchChannelT} = this.loan.fields;
        return sBranchChannelT.v === E_BranchChannelT.Correspondent;
    }

    private isShowLslModal: boolean;
    openLslModal() {
        this.isShowLslModal = true;

        if (__DEV__) {
            // For Typescript compiler to verify since the binding code is in HTML
            "../los/View/LoanStatusList.aspx";
            const {sBranchChannelT, sCorrespondentProcessT} = this.loan.fields;
            const args = {
                channel: sBranchChannelT.v,
                process: (sBranchChannelT.v === E_BranchChannelT.Correspondent ? sCorrespondentProcessT.v : undefined),
            };
        }
    }
    setStatus(status: E_sStatusT) {
        this.isShowLslModal = false;

        const {sStatusT, sStatusD} = this.loan.fields;
        sStatusT.v = status;

        {
            const [, f] = E_sStatusT_map_Date.find(([statusT]) => statusT === sStatusT.v);
            const field = this.loan.fields[f];
            if (field.v == null) field.v = new Date();
            sStatusD.v = field.v;
        }

        this.loan.calculate(sStatusT);
    }
    cancelSetStatus() {
        this.isShowLslModal = false;
    }

    private alternateLenderModal: AlternateLenderController;
    private onEditAddLender() {
        this.alternateLenderModal.show(this.loan.sLId, this.loan.appId);
        // const {sDocMagicAlternateLenderId} = this.loan.fields;
        // const url = '/newlos/Status/AlternateLender.aspx';

        // var method = "GetAlternateLenderList";
        // var args = { id: sDocMagicAlternateLenderId.v };
    }
    private onAlternateLenderChanged() {
        this.loan.calculate();
    }

    private isShowTtpModal: boolean;
    private openTtpModal() {
        if (!this.isDocMagicEnabled) return;
        this.isShowTtpModal = true;

        if (__DEV__) {
            // For Typescript compiler to verify since the binding code is in HTML
            '../newlos/Services/TransferToPicker.aspx';
            const {sDocMagicPlanCodeNm} = this.loan.fields;
            const args = {
                code    : sDocMagicPlanCodeNm.v,
                vendorId: this.loan.BrokerUser.documentVendorBrokerSettings.VendorId,
            };
        }
    }
    private setTransferTo({Description, Code}: {Description:string, Code:string}) {
        this.isShowTtpModal = false;
        const {sDocMagicTransferToInvestorNm, sDocMagicTransferToInvestorCode} = this.loan.fields;
        sDocMagicTransferToInvestorNm  .v = Description;
        sDocMagicTransferToInvestorCode.v = Code       ;
        this.loan.calculate(sDocMagicTransferToInvestorCode);
    }
    private cancelSetTransferTo() {
        this.isShowTtpModal = false;
    }

    private isFocus_sClosedD = false;
    private set_sClosedD_wrapper(wrap: HTMLElement) {
        $(wrap)
            .on("focus", "input", () => { $timeout(() => { this.isFocus_sClosedD = true  }) })
            .on("blur", "input", () =>  { $timeout(() => { this.isFocus_sClosedD = false }) })
            ;
    }
}
export const statusGeneralDirective = createBasicLoanEditorDirective("statusGeneral", appHtml, StatusGeneralController);

function naiveClone(x: any) { return JSON.parse(JSON.stringify(x)) }
