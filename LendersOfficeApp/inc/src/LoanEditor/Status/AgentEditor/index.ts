import {EditorController              	} from "../../ng-common/EditorController";
import {createBasicLoanEditorDirective	} from "../../ng-common/createBasicLoanEditorDirective";
import {DataModels,IDataModelsRaw    	} from "../../ng-common/DataModels";
import {E_RoleT,GetRole                 } from "../../../DataLayer/Enums/E_RoleT";
import {EmployeeRole                    } from "../../EmployeeRole";
import {TeamPicker                    	} from "../../TeamPicker";
import {StrGuid,guidEmpty               } from "../../../utils/guid";
import {$stateParams, $timeout          } from "../../ng-common/ngServices";
import {LicenseListController           } from "../../components/LicenseList/LicenseList";
import {AgentRecordController           } from "../../components/AgentRecord/AgentRecord";
import {updateFromRaw                   } from "../../ng-common/ILqbDataModel";
import {SortAgentList                   } from "../Agents";

import * as appHtml from "./index.html";

/*
LN
Original Page: LendersOfficeApp\newlos\Status\AgentRecord.aspx
*/
class StatusAgentEditorController extends EditorController {
    private currentTab:number;
    private backText:string;
    private recordId:StrGuid;
    private source:string;
    private sortKey: string;
    private isAsc: boolean;
    private sAgentList:any[];

    private loanOfficerLicenses: LicenseListController;
    private companyLicenses: LicenseListController;

    private agentRecord: AgentRecordController;
    private isEnableSave:boolean;
    private index:number;
    init(){
        this.isEnableSave = false;
        if($stateParams != null){
            if ($stateParams["RecId"] != null && $stateParams["RecId"] != ""){
                this.recordId = $stateParams["RecId"];
            }
            else{
                this.recordId = guidEmpty;
            }
            if ($stateParams["orderby"] != null && $stateParams["orderby"] != ""){
                let term = String($stateParams["orderby"]).split("-");
                if(term != null && term.length == 2){
                    this.sortKey=term[0];
                    this.isAsc = (term[1].trim()=="ASC");
                }
            }
            if ($stateParams["source"] != null){
                this.source = $stateParams["source"];
            }
            else{
                 this.source = "";
            }
            if ($stateParams["tab"] != null){
                this.currentTab = $stateParams["tab"];
            }
            else{
                this.currentTab = 0;
            }

        }
        switch (this.source) {
            case "commissions":
                this.backText = "Back to Comissions screen";
                break;
            case "leadAssignment":
                this.backText = "Back to Lead Assignment";
                break;
            default:
                this.backText = "Back to agents list";
                break;
        }
    }
    onLoaded(loan: DataModels): boolean {
        super.onLoaded(loan);
        if(this.loan != null && this.loan.sAgentList != null){
            this.sAgentList = this.loan.sAgentList.data.slice();
            this.sAgentList = SortAgentList(this.sAgentList,this.sortKey,this.isAsc);
            //Calculate Indexes
            this.index = 0;
            if(this.recordId != null && this.recordId != "" && this.recordId != guidEmpty && this.sAgentList != null && this.sAgentList.length != 0){
                for(var i = 0; i < this.sAgentList.length; i++){
                    if(this.recordId == String(this.sAgentList[i]["RecordId"])){
                        this.index = i + 1;
                        break;
                    }
                }
            }
            this.loan.once("postCalc", ()=>{
                this.isEnableSave = true;
            });
        }
        const data = {
            loanId: String(this.loan.sLId),
            recordId: String(this.recordId)
        }
        this.service.agents.getAgentRecord(data).then((dm:any)=>{
            if(this.loan != null && this.loan.AgentRecord != null && dm != null){
                this.loan.updateAgentRecordData(dm);

                this.agentRecord.load(this.loan);
                this.loanOfficerLicenses.licenses = this.loan.AgentRecord.LicenseInfoList["List"];
                this.loanOfficerLicenses.DisplayLosIdentifier = false;
                this.loanOfficerLicenses.IsCompanyLicense = false;

                this.companyLicenses.licenses = this.loan.AgentRecord.CompanyLicenseInfoList["List"];
                this.companyLicenses.DisplayLosIdentifier = false;
                this.companyLicenses.IsCompanyLicense = true;
            }
        });
        this.service.agents.getLicenseTemplate().then((template:any)=>{
            if(this.loan != null && this.loan.AgentRecord != null && template != null){
                this.loan.AgentRecord.LicenseTemplate =[];
                updateFromRaw(template , this.loan.AgentRecord.LicenseTemplate);
                this.loanOfficerLicenses.licenseTemplate = angular.merge({}, this.loan.AgentRecord.LicenseTemplate);
                this.companyLicenses.licenseTemplate = angular.merge({}, this.loan.AgentRecord.LicenseTemplate);
            }
        });
        return true;
    }
    GoBack(){
        let url = "";
        switch (this.source) {
            case "commissions":
                url="../newlos/Status/Commission.aspx?loanid="+this.loan.sLId+"&appid="+this.loan.app.fields["aAppId"].v;
                break;
            case "leadAssignment":
                url="../los/lead/LeadAssignment.aspx?loanid="+this.loan.sLId+"&appid="+this.loan.app.fields["aAppId"].v;
                break;
            default:
                this.goTo("statusAgents");
                return;
        }
        if(url != ""){
            window.open(url, '_blank');
        }
    }
    header():string{
        let s = "Contact Information";
        if(this.index == 0 || this.sAgentList == null){
            return s;
        }
        return s + " (" + this.index + " of " + this.sAgentList.length + ")";
    }
    isEnabledNext():boolean{
        return (this.sAgentList!=null) && (this.index != this.sAgentList.length) && (this.index != 0);
    }
    isEnabledPrevious():boolean{
        return (this.index != 1);
    }
    next(){
        if(!this.isEnabledNext()) return;
        let recId:StrGuid = null;
        if(this.index != 0)
        {
            recId = String(this.sAgentList[this.index]["RecordId"]);
        }
        this.displayPeopleRecord(recId);
    }
    previous(){
        if(!this.isEnabledPrevious()) return;
        let recId:StrGuid = null;
        if(this.index == 0)
        {
            recId = String(this.sAgentList[this.sAgentList.length-1]["RecordId"]);
        }
        else{
            recId = String(this.sAgentList[this.index-2]["RecordId"]);
        }
        this.displayPeopleRecord(recId);
    }
    displayPeopleRecord(recordId:StrGuid){
        if(recordId == null){
            this.goTo("statusAgentEditor");
        }
        else{
            let orderby="";
            if(this.sortKey!=null && this.sortKey!="")
            {
                orderby = this.sortKey+"-"+ ((this.isAsc)? "ASC":"DESC");
            }
            this.goTo("statusAgentEditor/params",{
                RecId:recordId,
                orderby:orderby,
                source: this.source,
                tab:this.currentTab
            });
        }
    }
    saveClick(){
        this.save().then(()=>{
            this.displayPeopleRecord(String(this.loan.AgentRecord.RecordId.v));
        });
    }
    saveAddNew(){
        this.save().then(()=>{
            this.displayPeopleRecord(null);
        });
    }
}
export const statusAgentEditorDirective = createBasicLoanEditorDirective("statusAgentEditor", appHtml, StatusAgentEditorController);
