import {EditorController              	} from "../../ng-common/EditorController";
import {createBasicLoanEditorDirective	} from "../../ng-common/createBasicLoanEditorDirective";
import {DataModels      				} from "../../ng-common/DataModels";
import {E_RoleT,GetRole                 } from "../../../DataLayer/Enums/E_RoleT";
import {EmployeeRole                    } from "../../EmployeeRole";
import {TeamPicker                    	} from "../../TeamPicker";
import {StrGuid,guidEmpty               } from "../../../utils/guid";

import * as appHtml from "./index.html";

/*
Author: Long Nguyen
Email: longn@dory.vn
*/
class StatusAgentsController extends EditorController {
    sortKey: string;
    isAsc: boolean;

    currentTab: number;

    teamName:{ [key: string]: string };

    loIndex: number;
    bpIndex: number;
    externalSecondaryIndex: number;
    externalPostCloserIndex: number;

    formattedRoles: string;
    showmOCError: boolean;

    isPageReadOnly: boolean;

    selectedRecordIds: StrGuid[];

    employeeRole: EmployeeRole;
    teamPicker: TeamPicker;
    sAgentList:any[];
    init() {
        this.sortKey = "";
        this.employeeRole = new EmployeeRole(this.dialog,this.service,this._loadingIndicator,this);
        this.teamPicker = new TeamPicker(this.dialog,this.service,this._loadingIndicator,this);
        this.currentTab = 0;
        this.loIndex = -1;
        this.bpIndex = -1;
        this.externalSecondaryIndex = -1;
        this.externalPostCloserIndex = -1;
        this.isPageReadOnly = false;

        this.service.agents.isPageReadOnly().then((m:any)=>{
            this.isPageReadOnly = m;
        });
    }
    onLoaded(loan: DataModels): boolean {
        super.onLoaded(loan);
        this.selectedRecordIds = [];
        this.LoadData();
        this.sAgentList = this.loan.sAgentList.data.slice();
        if(this.loan.BrokerDB.EnableTeamsUI){
            this.service.agents.getTeamNames({
                loanId:this.loan.sLId
            }).then((m : { [key: string]: string }) => {
                if(m == null) return;
                this.teamName = m;
                for(var e in E_RoleT){
                    var enumVal=parseInt(e, 10);
                       if (enumVal >= 0) {
                            this.displayTeamName(enumVal, m[E_RoleT[e]]);
                       }
                }
            });
        }
        return true;
    }
    LoadData(): void {
        const data = this.loan.EmployeeLoanAssignment.data;
        for (var i = 0; i < data.length; i++) {
            var role = data[i]["Role"]["RoleT"].v;
            if(data[i]["Role"]["RoleT"].v == E_RoleT.LoanOfficer && data[i]["IsPmlUser"].v){
                // The LoanAssignmentContactTable will return E_RoleT.LoanOfficer
                // for a PML Loan Officer instead of E_RoleT.Pml_LoanOfficer.
                role = E_RoleT.Pml_LoanOfficer;
            }
            this.displayRoleName(role,data[i]["EmployeeName"].v,data[i]["Email"].v);
            switch (role) {
                case E_RoleT.LoanOfficer:
                case E_RoleT.Pml_LoanOfficer:
                    this.loIndex = i;
                    break;
                case E_RoleT.Pml_BrokerProcessor:
                    this.bpIndex = i;
                    break;
                case E_RoleT.Pml_Secondary:
                    this.externalSecondaryIndex = i;
                    break;
                case E_RoleT.Pml_PostCloser:
                    this.externalPostCloserIndex = i;
                    break;
            }
        }
        this.getIncorrectRoles();
    }
    displayRoleName(role:E_RoleT, employeeName: string, email:string){
        var selector = $(`a[name='` + role + `'`).first();
            selector.text(employeeName);
            selector.attr("href", "mailto:" + email + "?subject=" + encodeURI(this.loan.sLId as string));
    }
    displayRoleNameCallBack(role:E_RoleT, sLId:StrGuid, copyToOfficialContact:boolean = false, value?:{[key: string]: any}){
        location.reload();
        return;
        /*
        // Reload page with conditions. Error. Not fix yet.
        //When edit, should reload page too.
        if(copyToOfficialContact || role >= 50 || role == E_RoleT.LoanOfficer){
            location.reload();
            return;
        }
        var selector = $(`a[name='` + role + `'`).first();
        if(value == null){
            selector.text("");
            selector.attr("href","");
        }
        else{
            selector.text(String(value["EmployeeName"]));
            selector.attr("href", "mailto:" + value["Email"] + "?subject=" + encodeURI(sLId as string));
        }*/
    }
    displayTeamName(role:E_RoleT,value:string){
        var selector = $(`a[ng-click='vm.displayTeamsInRole(` + role + `)'`).first();
        if(selector == null) return;
        if(value == null) value = "(assign)";
        selector.text(value);
    }
    clickItem(index: number) {
        if(this.isPageReadOnly) return;
        var RecordId = this.sAgentList[index]["RecordId"];
        var i = this.selectedRecordIds.indexOf(RecordId);
        if(i > -1){
            this.selectedRecordIds.splice(i, 1);
        }
        else{
            this.selectedRecordIds.push(RecordId);
        }
    }
    hasSelected(index:number):boolean{
        if(this.selectedRecordIds == null) return false;
        var RecordId = this.sAgentList[index]["RecordId"];
        return this.selectedRecordIds.indexOf(RecordId) > -1;
    }
    hasAnySelected():boolean{
        if(this.selectedRecordIds == null) return false;
        return this.selectedRecordIds.length != 0;
    }
    disableBrokerProcessorLink(): boolean {
        return this.loIndex != -1 && !this.loan.EmployeeLoanAssignment.data[this.loIndex]["IsPmlUser"].v;
    }
    disableExternalSecondaryLink(): boolean {
        return this.disableBrokerProcessorLink() && this.externalSecondaryIndex == -1;
    }
    disableLOLink(): boolean {
        return this.loIndex == -1 && this.externalSecondaryIndex != -1;
    }
    disablePostCloserLink(): boolean {
        return this.externalSecondaryIndex == -1 && this.externalPostCloserIndex == -1;
    }
    displayYesNo(v: string): string {
        return (v == "True") ? "Yes" : "No";
    }
    retrieveRole(role: E_RoleT): [string, string, string, string] {
        var roledesc: string;
        var show: string;
        var elementName: string;
        var roleName: string = GetRole(role);
        switch (role) {
            case E_RoleT.LoanOfficer:
            case E_RoleT.Pml_LoanOfficer:
                // <%=AspxTools.JsString(ConstApp.ROLE_LOAN_OFFICER) %>
                roledesc = 'Loan Officer';
                show = 'full';
                elementName = "m_agentTeam";
                break;

            case E_RoleT.Pml_BrokerProcessor:
                // <%=AspxTools.JsString(ConstApp.ROLE_BROKERPROCESSOR) %>
                roledesc = 'Processor (External)';
                show = 'full';
                elementName = "brokerprocessorTeam";
                break;

            case E_RoleT.Processor:
                // <%=AspxTools.JsString(ConstApp.ROLE_PROCESSOR) %>
                roledesc = 'Processor';
                show = 'simple';
                elementName = "m_processorTeam";
                break;

            case E_RoleT.Manager:
                // <%=AspxTools.JsString(ConstApp.ROLE_MANAGER) %>
                roledesc = 'Manager';
                show = 'simple';
                elementName = "m_managerTeam";
                break;
            case E_RoleT.RealEstateAgent:
                // <%=AspxTools.JsString(ConstApp.ROLE_REAL_ESTATE_AGENT) %>
                roledesc = 'Real Estate Agent';
                show = 'simple';
                elementName = "m_realEstateTeam";
                break;

            case E_RoleT.CallCenterAgent:
                // <%=AspxTools.JsString(ConstApp.ROLE_CALL_CENTER_AGENT) %>
                roledesc = 'Call Center Agent';
                show = 'simple';
                elementName = "m_telemarketerTeam";
                break;


            case E_RoleT.LenderAccountExecutive:
                // <%=AspxTools.JsString(ConstApp.ROLE_LENDER_ACCOUNT_EXEC) %> :
                roledesc = 'Lender Account Executive';
                show = 'simple';
                elementName = "m_lenderAcctExecTeam";
                break;

            case E_RoleT.LockDesk:
                // <%=AspxTools.JsString(ConstApp.ROLE_LOCK_DESK) %> :
                roledesc = 'Lock Desk Agent';
                show = 'simple';
                elementName = "m_lockDeskTeam";
                break;

            case E_RoleT.Underwriter:
                // <%=AspxTools.JsString(ConstApp.ROLE_UNDERWRITER) %>
                roledesc = 'Underwriter';
                show = 'simple';
                elementName = "m_underwriterTeam";
                break;

            case E_RoleT.LoanOpener:
                // <%=AspxTools.JsString(ConstApp.ROLE_LOAN_OPENER) %>
                roledesc = 'Loan Opener';
                show = 'simple';
                elementName = "m_loanOpenerTeam";
                break;

            case E_RoleT.Closer:
                // <%=AspxTools.JsString(ConstApp.ROLE_CLOSER) %>
                roledesc = 'Closer';
                show = 'simple';
                elementName = "m_closerTeam";
                break;

            case E_RoleT.Shipper:
                // <%=AspxTools.JsString(ConstApp.ROLE_SHIPPER) %>
                roledesc = 'Shipper';
                show = 'simple';
                elementName = "m_shipperTeam";
                break;

            case E_RoleT.Funder:
                // <%=AspxTools.JsString(ConstApp.ROLE_FUNDER) %>
                roledesc = 'Funder';
                show = 'simple';
                elementName = "m_funderTeam";
                break;

            case E_RoleT.PostCloser:
                // <%=AspxTools.JsString(ConstApp.ROLE_POSTCLOSER) %>
                roledesc = 'Post Closer';
                show = 'simple';
                elementName = "m_postCloserTeam";
                break;

            case E_RoleT.Insuring:
                // <%=AspxTools.JsString(ConstApp.ROLE_INSURING) %>
                roledesc = 'Insuring';
                show = 'simple';
                elementName = "m_insuringTeam";
                break;

            case E_RoleT.CollateralAgent:
                // <%=AspxTools.JsString(ConstApp.ROLE_COLLATERALAGENT) %>
                roledesc = 'Collateral Agent';
                show = 'simple';
                elementName = "m_collateralAgentTeam";
                break;

            case E_RoleT.DocDrawer:
                // <%=AspxTools.JsString(ConstApp.ROLE_DOCDRAWER) %>
                roledesc = 'Doc Drawer';
                show = 'simple';
                elementName = "m_docDrawerTeam";
                break;

            case E_RoleT.CreditAuditor:
                // <%=AspxTools.JsString(ConstApp.ROLE_CREDITAUDITOR) %>
                roledesc = 'Credit Auditor';
                show = 'simple';
                elementName = "m_creditAuditorTeam";
                break;

            case E_RoleT.DisclosureDesk:
                // <%=AspxTools.JsString(ConstApp.ROLE_DISCLOSUREDESK) %>
                roledesc = 'Disclosure Desk';
                show = 'simple';
                elementName = "m_disclosureDeskTeam";
                break;

            case E_RoleT.JuniorProcessor:
                // <%=AspxTools.JsString(ConstApp.ROLE_JUNIORPROCESSOR) %>
                roledesc = 'Junior Processor';
                show = 'simple';
                elementName = "m_juniorProcessorTeam";
                break;

            case E_RoleT.JuniorUnderwriter:
                // <%=AspxTools.JsString(ConstApp.ROLE_JUNIORUNDERWRITER) %>
                roledesc = 'Junior Underwriter';
                show = 'simple';
                elementName = "m_juniorUnderwriterTeam";
                break;

            case E_RoleT.LegalAuditor:
                // <%=AspxTools.JsString(ConstApp.ROLE_LEGALAUDITOR) %>
                roledesc = 'Legal Auditor';
                show = 'simple';
                elementName = "m_legalAuditorTeam";
                break;

            case E_RoleT.LoanOfficerAssistant:
                // <%=AspxTools.JsString(ConstApp.ROLE_LOANOFFICERASSISTANT) %>
                roledesc = 'Loan Officer Assistant';
                show = 'simple';
                elementName = "m_loanOfficerAssistantTeam";
                break;

            case E_RoleT.Purchaser:
                // <%=AspxTools.JsString(ConstApp.ROLE_PURCHASER) %>
                roledesc = 'Purchaser';
                show = 'simple';
                elementName = "m_purchaserTeam";
                break;

            case E_RoleT.QCCompliance:
                // <%=AspxTools.JsString(ConstApp.ROLE_QCCOMPLIANCE) %>
                roledesc = 'QC Compliance';
                show = 'simple';
                elementName = "m_qcComplianceTeam";
                break;

            case E_RoleT.Secondary:
                // <%=AspxTools.JsString(ConstApp.ROLE_SECONDARY) %>
                roledesc = 'Secondary';
                show = 'simple';
                elementName = "m_secondaryTeam";
                break;

            case E_RoleT.Servicing:
                // <%=AspxTools.JsString(ConstApp.ROLE_SERVICING) %>
                roledesc = 'Servicing';
                show = 'simple';
                elementName="";
                break;

            case E_RoleT.Pml_Secondary:
                // <%=AspxTools.JsString(ConstApp.ROLE_EXTERNAL_SECONDARY) %>
                roledesc = 'Secondary (External)';
                show = 'full';
                elementName = "m_servicingTeam";
                break;

            case E_RoleT.Pml_PostCloser:
                // <%=AspxTools.JsString(ConstApp.ROLE_EXTERNAL_POST_CLOSER) %>
                roledesc = 'Post-Closer (External)';
                show = 'full';
                elementName="";
                break;

            default:
                return;
        }

        return [roleName, roledesc, show, elementName];
    }
    displayPeopleInRole(role: E_RoleT, isOnlyDisplayLqbUsers: boolean, isOnlyDisplayPmlUsers: boolean): void {
        if(this.isPageReadOnly){
            this.dialog.alert("Cannot perform assignment because this loan file is read-only.");
            return;
        }
        var roleVal = this.retrieveRole(role);

        var url = '../los/EmployeeRole.aspx?role=' + roleVal[0] + '&roledesc=' + roleVal[1] + '&loanid=' + this.loan.sLId + '&show=' + roleVal[2];
        if (isOnlyDisplayLqbUsers) {
            url += '&isLqbOnly=t';
        }
        if (isOnlyDisplayPmlUsers) {
            url += '&isPmlOnly=t';
        }
        this.employeeRole.assign(role, roleVal[1], roleVal[2], isOnlyDisplayLqbUsers, isOnlyDisplayPmlUsers, this.displayRoleNameCallBack);
    }
    displayTeamsInRole(role: E_RoleT): void {
        if (this.isPageReadOnly) {
            this.dialog.alert("Cannot perform assignment because this loan file is read-only.");
            return;
        }
        var roleVal = this.retrieveRole(role);
        var url = '../los/TeamPicker.aspx?role=' + roleVal[0] + '&roledesc=' + roleVal[1] + '&loanid=' + this.loan.sLId + '&assignType=loan&tab=1';
        this.teamPicker.assign(role, roleVal[1],this.displayTeamName);
    }
    displayPeopleRecord(recordId:StrGuid){
        if(recordId == null){
            this.goTo("statusAgentEditor");
        }
        else{
            let orderby="";
            if(this.sortKey!=null && this.sortKey!="")
            {
                orderby = this.sortKey+"-"+ ((this.isAsc)? "ASC":"DESC");
            }
            this.goTo("statusAgentEditor/params",{
                RecId:recordId,
                orderby:orderby,
                source: "",
                tab:0
            });
        }
    }
    hasPmlBroker(): boolean {
        return (this.loan != null) && (this.loan.PmlBroker != null);
    }
    getsPmlBrokerStatusTValue(): string {
        if (this.hasPmlBroker()) {
            const op = this.loan.fields["sPmlBrokerStatusT"];
            const o = op.options.find(o => o.v === Number(op.v));
            if (o != null) {
                console.log(o);
                return o.l;
            }
            return "";
        }
    }
    getIncorrectRoles() {
        if (this.hasPmlBroker()) {
            var incorrectRoles: string[] = [];
            const data = this.loan.EmployeeLoanAssignment.data;
            const pmlBroker = this.loan.PmlBroker;
            //incorrectRoles.push(data[this.loIndex]["RoleModifiableDesc"].v);
            if (this.loIndex != -1 && data[this.loIndex]["PmlBrokerId"].v != pmlBroker.PmlBrokerId) {
                incorrectRoles.push(data[this.loIndex]["RoleModifiableDesc"].v);
            }
            if (this.bpIndex != -1 && data[this.bpIndex]["PmlBrokerId"].v != pmlBroker.PmlBrokerId) {
                incorrectRoles.push(data[this.bpIndex]["RoleModifiableDesc"].v);
            }
            if (this.externalSecondaryIndex != -1 && data[this.externalSecondaryIndex]["PmlBrokerId"].v != pmlBroker.PmlBrokerId) {
                incorrectRoles.push(data[this.externalSecondaryIndex]["RoleModifiableDesc"].v);
            }
            if (this.externalPostCloserIndex != -1 && data[this.externalPostCloserIndex]["PmlBrokerId"].v != pmlBroker.PmlBrokerId) {
                incorrectRoles.push(data[this.externalPostCloserIndex]["RoleModifiableDesc"].v);
            }
            this.formattedRoles = "";
            var length = incorrectRoles.length;
            for (var i = 0; i < length; i++) {
                this.formattedRoles += incorrectRoles[i];
                if (i < length - 2) {
                    this.formattedRoles += ", ";
                }
                if (i == length - 2) {
                    this.formattedRoles += " and ";
                }
            }
            this.showmOCError = (length != 0);
        }
    }
    showTip():void{
        this.dialog.alert("The Originating Company is applied when the Loan Officer, Processor (External), Secondary (External), or Post- Closer (External) are assigned. Note that the Loan Officer, Processor (External), Secondary (External), and Post- Closer (External) must belong to the same Originating Company.", "Originating Company");
    }
    delete():void{
        const m = `Do you want to delete these agents?`;
        this.dialog.confirm(m, "Confirm?")
            .then((isConfirmed) => {
                if (!isConfirmed) return false;
                this.executeDelete();
                return true;
            });
    }
    executeDelete(){
        this._loadingIndicator.show("Deleting...");
        const data = {
            loanId: String(this.loan.sLId),
            fileVersion: Number(this.loan.fields["sFileVersion"].v),
            idList: this.selectedRecordIds
        }
        this.service.agents.deleteOfficialContact(data).then(() => {
            this._loadingIndicator.hide();
            location.reload();
        }).catch((e: any) => {
            this._loadingIndicator.hide();
            this.dialog.alert("Unable to delete agent. Please try again","Delete Agent");
            console.log(e);
        })
    }
    sortRegulars(by: string) {
        if (this.loan.isCalculating) return;
        if (by == this.sortKey) this.isAsc = !this.isAsc;
        else {
            this.sortKey = by;
            this.isAsc = true;
        }
        this.sAgentList = SortAgentList(this.sAgentList,this.sortKey,this.isAsc);
    }
}
export function SortAgentList(sAgentList: any[], sortKey:string, isAsc:boolean):any[]{
    if(sortKey == null || sortKey.trim() == "") return sAgentList;
    var property = sortKey;
    var sortOrder = -1;
    if (isAsc) sortOrder = 1;
    sAgentList = sAgentList.sort(function (a, b) {
        var result = 0;
        if((typeof a[property]) == (typeof b[property])
                && (typeof a[property]) != "string")
                    result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            else
                result = (String(a[property]).toLowerCase() < String(b[property]).toLowerCase())
                        ? -1
                        : (String(a[property]).toLowerCase() > String(b[property]).toLowerCase())
                        ? 1 : 0;
        return result * sortOrder;
    });
    return sAgentList;
}
export const statusAgentsDirective = createBasicLoanEditorDirective("statusAgents", appHtml, StatusAgentsController);
