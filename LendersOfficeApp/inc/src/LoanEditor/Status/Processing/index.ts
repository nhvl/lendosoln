import {EditorController              } from "../../ng-common/EditorController";
import {createBasicLoanEditorDirective} from "../../ng-common/createBasicLoanEditorDirective";
import {ILqbDataModel                 } from "../../ng-common/ILqbDataModel";
import {DataModels                    } from "../../ng-common/DataModels";

import * as moment from "moment";
function getTimeStamp(user: string) {
    return `${moment().format("MM/DD/YYYY hh:mm A")} [${user}] - \r\n\r\n`;
}

import * as appHtml from "./index.html";

export class StatusProcessingController extends EditorController {
    private onDateStampClick() {
        const {sTrNotes} = this.loan.fields;

        const {firstName, lastName} = this.loan.BrokerUser;

        const timeStamp = getTimeStamp(`${firstName} ${lastName}`);
        sTrNotes.v = timeStamp + sTrNotes.v;
    }
}
export const statusProcessingDirective = createBasicLoanEditorDirective("statusProcessing", appHtml, StatusProcessingController);


/*

sEstCloseN           MaxLength="21"
sU1LStatDesc         MaxLength="21"
sU1LStatN            MaxLength="21"
sU2LStatDesc         MaxLength="21"
sU2LStatN            MaxLength="21"
sU3LStatDesc         MaxLength="21"
sU3LStatN            MaxLength="21"
sU4LStatDesc         MaxLength="21"
sU4LStatN            MaxLength="21"
sPrelimRprtN         MaxLength="21"
sClosingServN        MaxLength="21"
sApprRprtN           MaxLength="21"
sTilGfeN             MaxLength="21"
sFloodCertN          MaxLength="21"
sUSPSCheckN          MaxLength="21"
sPayDemStmntN        MaxLength="21"
sCAIVRSN             MaxLength="21"
sFraudServN          MaxLength="21"
sAVMN                MaxLength="21"
sHOACertN            MaxLength="21"
sEstHUDN             MaxLength="21"
sCPLAndICLN          MaxLength="21"
sWireInstructN       MaxLength="21"
sInsurancesN         MaxLength="21"
sU1DocStatDesc       MaxLength="21"
sU1DocStatN          MaxLength="21"
sU2DocStatDesc       MaxLength="21"
sU2DocStatN          MaxLength="21"
sU3DocStatDesc       MaxLength="21"
sU3DocStatN          MaxLength="21"
sU4DocStatDesc       MaxLength="21"
sU4DocStatN          MaxLength="21"
sU5DocStatDesc       MaxLength="21"
sU5DocStatN          MaxLength="21"
aU1DocStatDesc       MaxLength="21"
aU2DocStatDesc       MaxLength="21"

*/

/*

sAppSubmittedD                    lock by     sAppSubmittedDLckd
sAppReceivedByLenderD             lock by     sAppReceivedByLenderDLckd
sDocMagicApplicationD             lock by     sDocMagicApplicationDLckd
sDocMagicGFED                     lock by     sDocMagicGFEDLckd
sDocMagicEstAvailableThroughD     lock by     sDocMagicEstAvailableThroughDLckd
sDocMagicDocumentD                lock by     sDocMagicDocumentDLckd
sDocMagicClosingD                 lock by     sDocMagicClosingDLckd
sDocMagicSigningD                 lock by     sDocMagicSigningDLckd
sDocMagicCancelD                  lock by     sDocMagicCancelDLckd
sDocMagicDisbursementD            lock by     sDocMagicDisbursementDLckd
sDocMagicRateLockD                lock by     sDocMagicRateLockDLckd
sDocMagicPreZSentD                lock by     sDocMagicPreZSentDLckd
sDocMagicReDiscSendD              lock by     sDocMagicReDiscSendDLckd
sDocMagicReDiscReceivedD          lock by     sDocMagicReDiscReceivedDLckd
sEstCloseD                        lock by     sEstCloseDLckd
sDocMagicRedisclosureMethodT      lock by     sDocMagicRedisclosureMethodTLckd
*/
