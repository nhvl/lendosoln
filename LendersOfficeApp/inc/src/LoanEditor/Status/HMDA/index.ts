import {EditorController              	} from "../../ng-common/EditorController";
import {createBasicLoanEditorDirective	} from "../../ng-common/createBasicLoanEditorDirective";
import {DataModels      				} from "../../ng-common/DataModels";
import {E_sGseSpT      					} from "../../../DataLayer/Enums/E_sGseSpT";
import {toastrOptions                   } from "../../ng-common/toastrOptions";
import {$timeout, $http                 } from "../../ng-common/ngServices";

import * as appHtml from "./index.html";

/*
Author: Long Nguyen
Email: longn@dory.vn
*/
class StatusHmdaController extends EditorController {
    isShowDeniedWarning: boolean;
    onLoaded(loan: DataModels): boolean {
        super.onLoaded(loan);

        $timeout(() => {
            $(`lqb-input[lqb-model="vm.loan.fields.sRejectD"] input`).on("focus", () => {
                $timeout(() => { this.isShowDeniedWarning = true; });
            });
        });
        $timeout(() => {
            $(`lqb-input[lqb-model="vm.loan.fields.sRejectD"] input`).on("blur", () => {
                $timeout(() => { this.isShowDeniedWarning = false; });
            });
        });
        $timeout(() => {
            $(`lqb-input[lqb-model="vm.loan.fields.sHmdaDenialReason1"] div input`).on("change", () => {
                $timeout(() => {
                        this.SetDeniedBy();
                 },500);
            });
        });
        this.loan.calculate();
        return true;
    }
    issHmdaPropTWarning(): boolean {
        if (this.loan == null) return false;
        return this.loan.fields["sGseSpT"].v == E_sGseSpT.Modular;
    }
    isNCCallReportPanel(): boolean {
        if (this.loan == null) return false;
        return !((this.loan.fields["sSpState"].v != "NC") && !this.loan.fields["IsTemplate"].v);
    }
    ImportGeoCodes(e: JQueryEventObject): void {
        toastr.info(`importing GeoCode`, null, toastrOptions);
        const data = {
            addr: String(this.loan.fields["sSpAddr"].v),
            city: String(this.loan.fields["sSpCity"].v),
            county: String(this.loan.fields["sSpCounty"].v),
            state: String(this.loan.fields["sSpState"].v),
            zip: String(this.loan.fields["sSpZip"].v),
            hmdaActionD: String(this.loan.fields["sHmdaActionD"].v)
        };
        this.loan.isCalculating = true;
        this.service.hmda.importGeoCodes(data).then((m: { Key: string, Value: string }[]) => {
            this.loan.isCalculating = false;
            if (m != null && m.length > 0 && m[0].Key == "Error") {
                this.dialog.alert(m[0].Value, "Geocode").then(
                    (isConfirmed) => {
                        if (isConfirmed) {
                            var url = 'http://www.ffiec.gov/Geocode/default.aspx';
                            window.open(url, 'Geocode', 'width=750,height=490,menu=no,status=yes,location=no,scrollbars=yes,resizable=yes');
                        }
                    });
                e.stopPropagation();
                return;
            }
            m.map((a: { Key: string, Value: string }) => this.loan.fields[a.Key].v = a.Value);
            this.loan.calculate();
        }).catch((err: any) => {
            console.log(err);
            this.loan.isCalculating = false;
            //alert(e);
        });
    }
    SetDeniedBy(): void {
        toastr.info(`setting Denied Values`, null, toastrOptions);
        this.loan.isCalculating = true;
        this.service.hmda.setDeniedBy().then((m: any) => {
            m.map((a: any) => {
                if (a.Key == "sHmdaLoanDenied") {
                    this.loan.fields[a.Key].v = Boolean(a.Value);
                }
                else {
                    this.loan.fields[a.Key].v = a.Value
                }
            });
            this.loan.isCalculating = false;
            this.loan.calculate();
        }).catch((e: any) => {
            console.log(e);
            this.loan.isCalculating = false;
        });
    }
    GetUserMessageError(error:any):string{
        var message:string = JSON.parse(error.responseText).Message;
        var indexUM = message.indexOf("UserMessage:");
        var indexDM = message.indexOf("DeveloperMessage:");
        if (indexDM > -1) {
            if (indexUM > -1) {
                message = message.slice(indexUM + 13, indexDM - 4);
            }
            else {
                message = message.slice(indexUM + 13);
            }
        }
        return message;
    }
}
export const statusHmdaDirective = createBasicLoanEditorDirective("statusHmda", appHtml, StatusHmdaController);
