import {createBasicLoanEditorDirective	} from "../../ng-common/createBasicLoanEditorDirective";

import * as appHtml from "./index.html";

/*
Author: Long Nguyen
Email: longn@dory.vn
*/
export const statusTrustAccountsDirective = createBasicLoanEditorDirective("statusTrustAccounts", appHtml);
