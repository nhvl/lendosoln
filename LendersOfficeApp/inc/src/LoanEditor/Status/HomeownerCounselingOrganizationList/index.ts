// import {E_sOriginatorCompensationLenderFeeOptionT} from "../../../DataLayer/Enums/E_sOriginatorCompensationLenderFeeOptionT";

import {EditorController                } from "../../ng-common/EditorController";
import {IHomeownerCounselingOrganization} from "../../ng-common/DataModels";
import {createBasicLoanEditorDirective  } from "../../ng-common/createBasicLoanEditorDirective";
// import {ILqbDataModel                 } from "../../ng-common/ILqbDataModel";
// import {DataModels                    } from "../../ng-common/DataModels";

import {$http, $timeout} from "../../ng-common/ngServices";

import * as appHtml from "./index.html";

class HcoListController extends EditorController {
    // protected onLoaded(loan: DataModels){
    //     super.onLoaded(loan);
    //     return true;
    // }
    private addNew(){
        const {sHomeownerCounselingOrganizationCollection} = this.loan;
        console.assert(sHomeownerCounselingOrganizationCollection != null);
        const {data, template} = sHomeownerCounselingOrganizationCollection;
        sHomeownerCounselingOrganizationCollection.data = data.concat([angular.copy(template)]);
    }
    private genCco() {
        const {aBZip} = this.loan.app.fields; // TODO: ML.aBZip
        if (!aBZip  || !/^\d{5}$/.test(aBZip.v)) {
            this.dialog.alert(`Borrower's present address zip code is not valid.`, "Invalid");
            return;
        }

        // https://github.com/cfpb/django-hud
        $.getJSON(`http://www.consumerfinance.gov/hud-api-replace/${aBZip.v}?callback=?`).done((data: any) => {
            $timeout(() => {
                if (!data || !data.counseling_agencies || data.counseling_agencies.length < 1) {
                    this.dialog.alert(`CFPB was unable to find housing counselor data for zip code ${aBZip.v} of the borrower's present address.`, "Unable to load data from CFPB");
                    return;
                }
                const {counseling_agencies} = data;

                if (counseling_agencies.length < 10) this.dialog.alert("We were unable to load all 10 Counseling Organizations from CFPB");

                const {sHomeownerCounselingOrganizationCollection} = this.loan;
                sHomeownerCounselingOrganizationCollection.data = counseling_agencies.map((organization: any) => {
                    const y: IHomeownerCounselingOrganization = angular.copy(sHomeownerCounselingOrganizationCollection.template);
                    y.Name                  .v = organization.nme;
                    y.Address.StreetAddress .v = (organization.adr1 || organization.mailingadr1);
                    y.Address.StreetAddress2.v = (organization.adr2 || organization.mailingadr2);
                    y.Address.City          .v = (organization.city || organization.mailingcity);
                    y.Address.State         .v = (organization.statecd || organization.mailingstatecd);
                    y.Address.Zip           .v = (prettifyZip(organization.zipcd) || prettifyZip(organization.mailingzipcd));
                    y.Phone                 .v = prettifyPhone(organization.phone1 || organization.phone2 || organization.fax);
                    y.Email                 .v = (organization.email);
                    y.Website               .v = (organization.weburl);
                    y.Services              .v = (organization.services);
                    y.Languages             .v = (organization.languages);
                    y.Distance              .v = (organization.distance);
                    return y;
                });
            });
        });
    }
    private removeItem(i: number) {
        const {sHomeownerCounselingOrganizationCollection} = this.loan;
        sHomeownerCounselingOrganizationCollection.data = immutableSplice(sHomeownerCounselingOrganizationCollection.data, i, 1);
    }
}

export const hcoListDirective = createBasicLoanEditorDirective("hcoList", appHtml, HcoListController);

function prettifyZip(zip:string): string {
    return zip.split('-')[0];  // XXXXX-XXXX => XXXXX
}

function prettifyPhone(phone: string): string {
    // XXX-XXX-XXXX, we want (XXX) XXX-XXXX
    const firstDash = phone.indexOf('-');
    return ((firstDash > 0)
        ? `(${phone.substring(0, firstDash)}) ${phone.substring(firstDash + 1)}`
        : phone
        );
}

function immutableSplice<T>(xs: T[], start: number, deleteCount: number) {
    const ys = [].concat(xs);
    ys.splice(start, deleteCount);
    return ys;
}

/*

http://localhost/LendersOfficeApp/newlos/Status/HomeownerCounselingOrganizationList.aspx?loanid=b6a0498a-0376-45a3-ab8b-a4dd01406df0&printid=&appid=a88eae7c-b081-4e8e-bbb3-a4dd01406ef5

update trigger updateLastModifiedDate


*/
