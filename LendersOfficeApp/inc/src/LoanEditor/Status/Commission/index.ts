// import {E_sOriginatorCompensationLenderFeeOptionT} from "../../../DataLayer/Enums/E_sOriginatorCompensationLenderFeeOptionT";

import {EditorController              } from "../../ng-common/EditorController";
import {createBasicLoanEditorDirective} from "../../ng-common/createBasicLoanEditorDirective";
// import {ILqbDataModel                 } from "../../ng-common/ILqbDataModel";
// import {DataModels                    } from "../../ng-common/DataModels";

import * as appHtml from "./index.html";

class StatusCommissionController extends EditorController {
    // protected onLoaded(loan: DataModels){
    //     super.onLoaded(loan);
    //     return true;
    // }
    private displayPeopleRecord(recordId: string) {
        alert("TODO: " +
            `AgentRecord.aspx?recordid=${recordId}&appid=${this.loan.appId}&source=commissions"`);
    }
}

export const statusCommissionDirective = createBasicLoanEditorDirective("statusCommission", appHtml, StatusCommissionController);
/*

sProfitAncillaryFeeLckd       sProfitAncillaryFee
http://localhost/LendersOfficeApp/newlos/Status/Commission.aspx?loanid=b6a0498a-0376-45a3-ab8b-a4dd01406df0&printid=CRateLockConfirmationPDF&appid=a88eae7c-b081-4e8e-bbb3-a4dd01406ef5


*/
