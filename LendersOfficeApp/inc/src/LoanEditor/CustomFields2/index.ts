import {createBasicLoanEditorDirective} from "../ng-common/createBasicLoanEditorDirective";

import * as appHtml from "./index.html";

export const customFields2Directive = createBasicLoanEditorDirective("customFields2", appHtml);
