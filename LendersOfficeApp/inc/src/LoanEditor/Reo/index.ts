import {EditorController              	} from "../ng-common/EditorController";
import {createBasicLoanEditorDirective	} from "../ng-common/createBasicLoanEditorDirective";
import {ILqbDataModel                 	} from "../ng-common/ILqbDataModel";
import {DataModels      				} from "../ng-common/DataModels";
import {E_ReoStatusT                    } from "../../DataLayer/Enums/E_ReoStatusT";

import * as appHtml from "./index.html";

//Author: Long Nguyen
//Email: longn@dory.vn
class ReoController extends EditorController {

    selectedIndex: number;
    sortKey: string;
    isAsc: boolean;
    selectedRegular: { [key: string]: ILqbDataModel };
    init() {
        this.selectedIndex = -1;
        this.sortKey = "";
        this.updateItem();
    }
    onLoaded(loan: DataModels): boolean {
        super.onLoaded(loan);

        if ((this.loan.app != null) &&
            (this.loan.app.aReCollection.Regulars != null) &&
            (this.loan.app.aReCollection.Regulars.length != 0) &&
            this.selectedIndex == -1) {
            this.selectedIndex = 0;
            this.sortKey = "";
            this.updateItem();
            this.preCalculate();
        }
        return true;
    }
    preCalculate() {
        this.loan.on("preCalc", (dm: ILqbDataModel) => {
            if ((dm != null) && Array.isArray(dm.options) && dm.options.length == 4) {
                const o = dm.options.find(o => o.v === Number(dm.v));
                if (o.v !== E_ReoStatusT.PendingSale && o.v !== E_ReoStatusT.Residence) {
                    this.selectedRegular["IsForceCalcNetRentalI"].v = false;
                }
            }
        });
    }
    copyFrom(isBorr: boolean) {
        const f = () => {
            if (isBorr) {
                this.selectedRegular["Addr"].v = this.loan.app.fields["aBAddr"].v;
                this.selectedRegular["City"].v = this.loan.app.fields["aBCity"].v;
                this.selectedRegular["State"].v = this.loan.app.fields["aBState"].v;
                this.selectedRegular["Zip"].v = this.loan.app.fields["aBZip"].v;
                this.selectedRegular["IsPrimaryResidence"].v = true;
            }
            else {
                this.selectedRegular["Addr"].v = this.loan.fields["sSpAddr"].v;
                this.selectedRegular["City"].v = this.loan.fields["sSpCity"].v;
                this.selectedRegular["State"].v = this.loan.fields["sSpState"].v;
                this.selectedRegular["Zip"].v = this.loan.fields["sSpZip"].v;
                this.selectedRegular["IsSubjectProp"].v = true;
            }
            this.loan.calculate();
        };
        this.executeClick(f);
    }
    displayIsSubjectProp(v: boolean): string {
        return v ? "Yes" : "No";
    }
    displayAddress(Addr: string, City: string, State: string, Zip: string): string {
        return Addr + ", " + City + ", " + State + " " + Zip;
    }
    displayStatus(v: number): string {
        switch (v) {
            case E_ReoStatusT.Residence: return "";
            case E_ReoStatusT.Sale: return "S";
            case E_ReoStatusT.PendingSale: return "PS";
            case E_ReoStatusT.Rental: return "R";
        }
        return "";
    }
    isFirstOrEmpty() {
        return this.selectedIndex <= 0;
    }
    isEmpty() {
        return (this.loan == null) ||
            (this.loan.app.aReCollection.Regulars == null) ||
            (this.loan.app.aReCollection.Regulars.length == 0) ||
            (this.selectedIndex == -1);
    }
    isLastOrEmpty() {
        if (!this.isEmpty()) {
            return (this.loan.app.aReCollection.Regulars.length - 1) == this.selectedIndex;
        }
        return true;
    }

    clickItem(index: number) {
        const f = () => {
            this.selectedIndex = index;
            this.updateItem();
        };
        this.executeClick(f);
    }
    clickPrev() {
        const f = () => {
            if (!this.isFirstOrEmpty()) {
                this.selectedIndex--;
                this.updateItem();
            }
        };
        this.executeClick(f);
    }
    clickNext() {
        const f = () => {
            if (!this.isLastOrEmpty()) {
                this.selectedIndex++;
                this.updateItem();
            }
        };
        this.executeClick(f);
    }
    clickMoveUp() {
        const f = () => {
            if (!this.isFirstOrEmpty()) {
                var x = this.loan.app.aReCollection.Regulars[this.selectedIndex - 1];
                this.loan.app.aReCollection.Regulars[this.selectedIndex - 1] = this.loan.app.aReCollection.Regulars[this.selectedIndex];
                this.loan.app.aReCollection.Regulars[this.selectedIndex] = x;
                this.selectedIndex--;
                this.updateItem();
            }
        };
        this.executeClick(f);
    }
    clickMoveDown() {
        const f = () => {
            if (!this.isLastOrEmpty()) {
                var x = this.loan.app.aReCollection.Regulars[this.selectedIndex + 1];
                this.loan.app.aReCollection.Regulars[this.selectedIndex + 1] = this.loan.app.aReCollection.Regulars[this.selectedIndex];
                this.loan.app.aReCollection.Regulars[this.selectedIndex] = x;
                this.selectedIndex++;
                this.updateItem();
            }
        };
        this.executeClick(f);
    }
    delete() {
        const f = () => {
            const m = `Do you want to delete this record?`;
            this.dialog.confirm(m, "Confirm?")
                .then((isConfirmed) => {
                    if (!isConfirmed) return false;

                    if (!this.isEmpty()) {
                        if (this.loan.app.aReCollection.Regulars.length == 1) {
                            this.loan.app.aReCollection.Regulars = [];
                            this.updateItem();
                            this.loan.calculate();
                            return true;
                        }
                        this.loan.app.aReCollection.Regulars.splice(this.selectedIndex, 1);
                        if (this.selectedIndex != 0) {
                            this.selectedIndex--;
                        }
                        this.updateItem();
                        this.loan.calculate();
                    }
                    return true;
                });
        };
        this.executeClick(f);
    }
    sortRegulars(by: string) {
        const f = () => {
            if (by == this.sortKey) this.isAsc = !this.isAsc;
            else {
                this.sortKey = by;
                this.isAsc = true;
            }
            var property = this.sortKey;
            var sortOrder = -1;
            if (this.isAsc) sortOrder = 1;
            var result = 0;
            this.loan.app.aReCollection.Regulars = this.loan.app.aReCollection.Regulars.sort(function(a, b) {
                if((typeof a[property].v) == (typeof b[property].v))
                    result = (a[property].v < b[property].v) ? -1 : (a[property].v > b[property].v) ? 1 : 0;
                else
                    result = (String(a[property].v) < String(b[property].v)) ? -1 : (String(a[property].v) > String(b[property].v)) ? 1 : 0;
                return result * sortOrder;
            });
            /*for (var i = 0; i < this.loan.app.aReCollection.Regulars.length; i++) {
                if (this.selectedRegular["RecordId"].v == this.loan.app.aReCollection.Regulars[i]["RecordId"].v) {
                    this.selectedIndex = i;
                    break;
                }
            }*/
            this.updateItem();
        };
        this.executeClick(f);
    }
    insert() {
        const f = () => {
            if (!this.isEmpty()) {
                this.loan.app.aReCollection.Regulars.splice(this.selectedIndex, 0, angular.merge({}, this.loan.app.aReCollection.RegularTemplate));
                this.updateItem();
                this.loan.calculate();
            }
            else {
                this.add();
            }
        };
        this.executeClick(f);
    }
    add() {
        const f = () => {
            this.loan.app.aReCollection.Regulars.push(angular.merge({}, this.loan.app.aReCollection.RegularTemplate));
            this.selectedIndex = this.loan.app.aReCollection.Regulars.length - 1;
            this.updateItem();
            this.loan.calculate();
        };
        this.executeClick(f);
    }
    executeClick(f: Function): void {
        if (this.loan.isCalculating) this.loan.once("postCalc", f);
        else f();
    }
    updateItem() {
        if (this.isEmpty())
            this.selectedRegular = null;
        else
            this.selectedRegular = this.loan.app.aReCollection.Regulars[this.selectedIndex];
    }
}
export const reoDirective = createBasicLoanEditorDirective("reo", appHtml, ReoController);
