import {EditorController              } from "../ng-common/EditorController";
import {createBasicLoanEditorDirective} from "../ng-common/createBasicLoanEditorDirective";
import {ILqbDataModel                 } from "../ng-common/ILqbDataModel";
import {DataModels                    } from "../ng-common/DataModels";

import {E_sLPurposeT                  } from "../../DataLayer/Enums/E_sLPurposeT";
import {E_sLienPosT                   } from "../../DataLayer/Enums/E_sLienPosT";
import {E_sLT                         } from "../../DataLayer/Enums/E_sLT";

import {ngFilter} from "../ng-common/ngServices";
import {StrGuid} from "../../utils/guid";

import * as appHtml from "./index.html";

const canModifyLoanName = true; // TODO

const refinance_sLPurposeT = [E_sLPurposeT.FhaStreamlinedRefinance, E_sLPurposeT.VaIrrrl, E_sLPurposeT.Refin, E_sLPurposeT.RefinCashout];
export class LoanInfoController extends EditorController {
    private E_sLT = E_sLT;
    init() { }

    // TODO
    private canModifyLoanName = true;
    private isPmlEnabled = true;
    private showCRMID = true;

    onLoaded(loan:DataModels) {
        if (super.onLoaded(loan)){
            this.showCRMID = loan.BrokerDB.ShowCRMID && !loan.fields.IsTemplate.v;
            return true;
        }
        return false;
    }

    //private refinance_sLPurposeT = refinance_sLPurposeT;
    private isRefinance(): boolean {
        if (this.loan == null) return false;

        const {sLPurposeT} = this.loan.fields;
        return refinance_sLPurposeT.includes(sLPurposeT.v);
    }

    private isCcTemplatePickerOpen = false;
    private findCCTemplate(){
        this.goTo("loanInfo").then(() => {
            this.isCcTemplatePickerOpen = true;
        });

        // Before choosing a closing cost template, you always need to save the loan.
        // LoanInfo.ascx ClosingCostPageUrl
        // if applied, refresh
    }
    private onCcTemplateSelected(template: {Id: StrGuid, Name: string}){
        location.reload();
    }
    private onCcTemplatePickerRequestClose(){
        this.isCcTemplatePickerOpen = false;
    }

    private generateNewLoanName(){
        if (! this.canModifyLoanName) {
            this.dialog.alert("You do not have permission to modify the loan number.");
            return;
        }

        this.service.loanInfo.generateNewLoanNumber({loanId: this.loan.sLId})
        .then(
            (sLNm) => { this.loan.fields.sLNm.v = sLNm; },
            () => { this.dialog.alert("Please contact your system administrator if the problem persists.", "Failed to update loan number."); }
        );
    }

    private runLPE(){
        window.open(`../newlos/Template/RunEmbeddedPML.aspx?loanid=${this.loan.sLId}&islpe=t`, "_blank");
    }

    private runNonLPE() {
        window.open(`../newlos/Template/QualifiedLoanProgramsearchResultNew.aspx?loanid=${this.loan.sLId}&islpe=t`, "_blank");
    }

    private otherFinancingDescription(): string {
        if (this.loan == null) return "";
        const {currency, number} = ngFilter;

        const {sLienPosT, sConcurSubFin, sSubFinIR, sSubFinPmtLckd} = this.loan.fields;
        return (sLienPosT.v === E_sLienPosT.First && sConcurSubFin.v > 0 && sSubFinIR.v > 0 && !sSubFinPmtLckd.v) ?
            `(${currency(sConcurSubFin.v)} @ ${number(sSubFinIR.v,3)}%)` : "";
    }
}
export const loanInfoDirective = createBasicLoanEditorDirective("loanInfo", appHtml, LoanInfoController);
