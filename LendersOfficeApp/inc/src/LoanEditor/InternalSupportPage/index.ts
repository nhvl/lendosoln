import {createBasicLoanEditorDirective} from "../ng-common/createBasicLoanEditorDirective";
import {EditorController} from "../ng-common/EditorController";

import * as appHtml from "./index.html";

class InternalSupportPageController extends EditorController {
    private migrate(i: number) {
        // TODO: migrate(${i})
    }
    private createClosingCostDisclosure() {
        // TODO: createClosingCostDisclosure()
    }
    private FixLEAndCDDates() {
        // TODO: FixLEAndCDDates()
    }
    private PerformRollback() {
        // TODO: PerformRollback()
    }
}

export const internalSupportPageDirective = createBasicLoanEditorDirective("internalSupportPage", appHtml, InternalSupportPageController);
