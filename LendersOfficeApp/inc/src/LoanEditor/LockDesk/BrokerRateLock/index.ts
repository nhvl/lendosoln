type Guid = string;

import {LoadingIndicator              } from "../../ng-common/loadingIndicator";
import {EditorController              } from "../../ng-common/EditorController";
import {E_sBrokerLockPriceRowLockedT  } from "../../../DataLayer/Enums/E_sBrokerLockPriceRowLockedT";
import {DataModels                    } from "../../ng-common/DataModels";
import {createBasicLoanEditorDirective} from "../../ng-common/createBasicLoanEditorDirective";

import * as appHtml from "./index.html";

class BrokerRateLockController extends EditorController {
    private E_sBrokerLockPriceRowLockedT: typeof E_sBrokerLockPriceRowLockedT;
    private sRateLockHistory: {}[];
    private sLId: Guid;

    protected init() {
        this.E_sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT;
    }

    protected onLoaded(loan: DataModels):boolean {
        const success = super.onLoaded(loan); if (!success) return false;

        if (typeof this.loan.fields.sBrokerLockPriceRowLockedT.v !== "undefined") {
            const nv = Number(this.loan.fields.sBrokerLockPriceRowLockedT.v);
            if (!Number.isNaN(nv)) this.loan.fields.sBrokerLockPriceRowLockedT.v = nv;
        }

        if (this.loan.fields.sRateLockHistoryXmlContent != null) {
            try {
                const d = JSON.parse(<string>this.loan.fields.sRateLockHistoryXmlContent.v,
                    (k, v) => {
                        if (v == null || typeof v !== "object") return v;
                        Object.keys(v).forEach((k) => {
                            if (k[0] !== "@") return;

                            const x = v[k];
                            delete v[k];
                            v[k.slice(1)] = x;
                        });
                        return v;
                    });
                if (typeof d.RateLockHistory.Event !== "undefined") {
                    this.sRateLockHistory = Array.isArray(d.RateLockHistory.Event) ?
                        d.RateLockHistory.Event :
                        [d.RateLockHistory.Event];
                }
            } catch (e) { console.error("parse sRateLockHistoryXmlContent FAIL: ", e); }
        }

        return true;
    }
}

export const brokerRatelockDirective = createBasicLoanEditorDirective("brokerRatelock", appHtml, BrokerRateLockController);

$(document).ready(function () {

    $(".lock-checkbox").click(function () {
        styleCheckbox($(this));
    });

    $(".lock-checkbox").each(function () { styleCheckbox($(this)); });

});

function styleCheckbox(container: any) {

    var chkBox = container.find("input");

    if (chkBox.is(":checked")) {
        container.addClass("checked");
    }
    else {
        container.removeClass("checked");
    }
}
