import {createBasicLoanEditorDirective} from "../ng-common/createBasicLoanEditorDirective";

import * as appHtml from "./index.html";

export const loanOfficerLicensingInfoDirective = createBasicLoanEditorDirective("loanOfficerLicensingInfo", appHtml);
// ~/newlos/LoanOfficerLicensingInfo.aspx?loanid=e28f7e55-7cb3-443c-85ad-a545018774b7&printid=&appid=b4c88b01-621c-4f41-a0e8-a54501877512&printid=
