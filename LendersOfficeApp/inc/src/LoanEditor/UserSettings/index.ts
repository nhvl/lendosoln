import {LqModalController} from "../components/lqModal";
import {ModalStatus      } from "../BorrowerAsset/ModalStatus";
import {ServiceFactory   } from "../ng-common/service";
import {UserSettingsService} from "../ng-common/service/UserSettingsService";
import {Dialog           } from "../../ng-common/dialog";
import {E_UserTheme      } from "../../DataLayer/Enums/E_UserTheme";

import {ILqbDataModelG, LqbInputType, E_CalculationType} from "../ng-common/ILqbDataModel";
import {IComponentOptions, IChangesObject, IOnChangesObject} from "angular";


export class UserSettings {
    UserTheme   : E_UserTheme;
    // [key:string]: any;

    save() {
        return (
            UserSettings.service.Update({userTheme : this.UserTheme})
        );
    }

    static get() {
        return (
            UserSettings.service.Get().then((data: { Key: string, Value: any }[]) => {
                const s = new UserSettings();
                data.forEach(({Key, Value}) => {
                    (s as any)[Key] = Value;
                });
                return s;
            })
        );
    }
    static service:UserSettingsService;
}

class UserSettingsController { ngName = "userSettingsModal";
    // angular binding
    private isShow             : boolean;
    private onSaved            : () => void;

    private E_UserTheme        = E_UserTheme;
    private userSettings       : UserSettings;
    private fields             : {
            UserTheme          : ILqbDataModelG<E_UserTheme>,
    };
    private settingsModal      : LqModalController;
    private settingsModalStatus: ModalStatus;

    static $inject = [Dialog.ngName, ServiceFactory.ngName];
    constructor(private dialog:Dialog , service: ServiceFactory) {
        UserSettings.service = service.userSettings;
        this.fields = {
            UserTheme: {
                t:LqbInputType.Radio,
                v: E_UserTheme.Light,
                calcAction:E_CalculationType.NoCalculate,
                r: false, h:false, required:false,
            },
        }
        UserSettings.get().then(s => {
            this.setUserSettings(s);
            this.setTheme();
        });
    }

    private $onChanges(changeObject:IOnChangesObject) {
        const {isShow} = changeObject;
        if (isShow != null) {
            if (isShow.currentValue) {
                this.setUserSettings(this.userSettings);
                this.show();
            } else {
                if (this.settingsModal != null) {
                    this.settingsModalStatus = ModalStatus.Discard;
                    this.settingsModal.close();
                }
            }
        }
    }

    private setSettingsModal(settingsModal: LqModalController):void {
        this.settingsModal = settingsModal;
        this.settingsModal.setOption({
            backdrop: 'static',
            keyboard: false,
        });
    }
    private setUserSettings(s: UserSettings) {
        this.userSettings = s;
        this.fields.UserTheme.v = s.UserTheme;
    }

    private show():void {
        this.settingsModalStatus = ModalStatus.Open;
        this.settingsModal.show();
    }
    private onSettingsModalHide(e:JQueryEventObject):void {

        if (this.settingsModalStatus != ModalStatus.Open) return;

        // If content is unchanged, escapse
        if(this.fields.UserTheme.v == this.userSettings.UserTheme) return;

        this.dialog.ync(`Do you wish to save this user settings?`, "Save?")
        .then((o)=>{
            switch (o) {
                case  1: this.saveSettings(); return;
                case  0: this.onSaved(); return;
                case -1: this.settingsModalStatus = ModalStatus.Open; return;
                default: console.error(o); return;
            }
        });
        e.preventDefault();
    }
    private saveSettings() : void {
        this.settingsModalStatus = ModalStatus.Save;

        this.userSettings.UserTheme = this.fields.UserTheme.v;
        this.userSettings.save().then(()=>{
            this.setTheme();
            this.onSaved();
        });
    }
    private requestClose() {
        return this.onSaved()
    }
    private setTheme():void {
        const themeCSS = $("#Theme");
        themeCSS.attr("href",
            this.userSettings.UserTheme == E_UserTheme.Light
            ? "../css/Prototype/SASS/LoanEditorTheme.css"
            : "../css/Prototype/SASS/DarkTheme.css");
    }
}

export const userSettingsModal: IComponentOptions = {
    bindings: {
        isShow : "<",
        onSaved: "&",
    },
    controller: UserSettingsController as any,
    controllerAs:"vm",
    template: require("./index.html"),
    ngName:"userSettingsModal",
}
