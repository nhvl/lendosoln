// import {StrGuid, guidEmpty} from "../../../utils/guid";

import {createBasicLoanEditorDirective} from "../../ng-common/createBasicLoanEditorDirective";
import {EditorController                } from "../../ng-common/EditorController";

import {DataModels                    } from "../../ng-common/DataModels";

import {E_GseRequestAppraisal} from "../../../DataLayer/Enums/E_GseRequestAppraisal";

import * as appHtml from "./index.html";

export class RequestForAppraisalController extends EditorController {
    // private guidEmpty = guidEmpty;

    private E_GseRequestAppraisal = E_GseRequestAppraisal;

    // onLoaded(loan:DataModels) {
    //     super.onLoaded(loan);
    //     return true;
    // }
}
export const requestForAppraisalDirective = createBasicLoanEditorDirective("requestForAppraisal", appHtml, RequestForAppraisalController);
