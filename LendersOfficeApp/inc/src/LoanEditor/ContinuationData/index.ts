import {createBasicLoanEditorDirective} from "../ng-common/createBasicLoanEditorDirective";

import * as appHtml from "./index.html";

export const continuationDataDirective = createBasicLoanEditorDirective("continuationData", appHtml);
