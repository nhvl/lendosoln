import {EditorController              } from "../ng-common/EditorController";
import {createBasicLoanEditorDirective} from "../ng-common/createBasicLoanEditorDirective";
import {IApplicant                    } from "../ng-common/IApplicant";
import {setQueryStringValue           } from "../../utils/setQueryStringValue";

import * as appHtml from "./index.html";

class BorrowerListController extends EditorController {
    swapBorrower(index: number){ // need more field id
        const m = `Do you want to swap borrower & spouse ?`;
        this.dialog.confirm(m, "Confirm?")
            .then((isConfirmed) => {
                if (!isConfirmed) { return; }
                let applicant:IApplicant = this.loan.applicants[index];

                const data = {
                    loanId: this.loan.sLId,
                    appId: applicant.aAppId
                };

                this._loadingIndicator.show("Updating ...");
                this.service.app.SwapBorrowerCoborrower(data)
                .then(() => { this.refresh(); })
                .catch((e: any) => {
                    this._loadingIndicator.hide();
                    alert(e);
                });
            }
        )
    }

    deleteSpouse(index: number){
        const m = `Do you want to delete all data of spouse ?`;
        this.dialog.confirm(m, "Confirm?")
            .then((isConfirmed) => {
                if (!isConfirmed) { return; }
                let applicant:IApplicant = this.loan.applicants[index];

                const data = {
                    loanId: this.loan.sLId,
                    appId: applicant.aAppId
                };

                this._loadingIndicator.show("Updating ...");
                this.service.app.DeleteCoborrower(data).then(() => {
                    this.refresh();
                }).catch((e: any) => {
                    this._loadingIndicator.hide();
                    alert(e);
                });
            }
        )
    }

    deleteApp(index: number){
        const m = `Please note: for loans with an IRS 4506-T order, removing a borrower will also remove that IRS order. Do you want to delete all data of borrower and spouse ?`;
        this.dialog.confirm(m, "Confirm?")
            .then((isConfirmed) => {
                if (!isConfirmed) { return; }
                let applicant:IApplicant = this.loan.applicants[index];

                const data = {
                    loanId: this.loan.sLId,
                    appId: applicant.aAppId
                };

                this._loadingIndicator.show("Updating ...");
                this.service.app.DeleteApplicant(data).then(() => {
                    this.refresh();
                }).catch((e: any) => {
                    this._loadingIndicator.hide();
                    alert(e);
                });
            }
        )
    }

    createApplicant(){
        const data = {
            loanId: this.loan.sLId,
        };

        this._loadingIndicator.show("Updating ...");
        this.service.app.AddApplication(data).then((newAppId) => {
            //this.onStateChanged({ $pageStateId: "borrowerInfo" });
            let newUrl = setQueryStringValue(window.location.href, "appId",encodeURIComponent(newAppId)).replace("borrowerList","borrowerInfo");

            window.location.href = newUrl;
        }).catch((e: any) => {
            this._loadingIndicator.hide();
            alert(e);
        });
    }

    getAppName(index: number): string{
        let applicant:IApplicant = this.loan.applicants[index];
        if (applicant){
            let aBNm = applicant.aBNm ? applicant.aBNm : "";
            let aCNm = applicant.aCNm ? applicant.aCNm : "";
            if (aBNm.length > 0 && aCNm.length > 0) {
                return aBNm + " + " + aCNm;
            } else {
                return aBNm + aCNm;
            }
        }
        return "";
    }

    radioChanged(index: number){ // need better behavour
        const m = `Do you want to set ` + this.getAppName(index) + ` as primary borrower?`;
        this.loan.applicants[index].aIsPrimary = !this.loan.applicants[index].aIsPrimary;

        this.dialog.confirm(m, "Confirm?")
            .then((isConfirmed) => {
                if (!isConfirmed) {
                     return;
                }
                let applicant:IApplicant = this.loan.applicants[index];

                const data = {
                    loanId: this.loan.sLId,
                    appId: applicant.aAppId
                };

                this._loadingIndicator.show("Updating ...");
                this.service.app.UpdatePrimaryBorrower(data).then(() => {
                    this.refresh();
                }).catch((e: any) => {
                    this._loadingIndicator.hide();
                    alert(e);
                });
            }
        )
    }

    refresh(){
        this.loan.save().finally(() => {  this._loadingIndicator.hide(); });;
    }
}

export const borrowerListDirective = createBasicLoanEditorDirective("borrowerList", appHtml, BorrowerListController);
