import {EditorController              } from "../ng-common/EditorController";
import {createBasicLoanEditorDirective} from "../ng-common/createBasicLoanEditorDirective";
import {IApplicant                    } from "../ng-common/IApplicant";
import {DataModels                    } from "../ng-common/DataModels";

import * as appHtml from "./index.html";


class ConstructionLoanInfoController extends EditorController {
    selectAll : any;
    private isAllSelected: boolean = false;

    createCheckbox(): any{
        return {
                    calcAction: 1,
                    r: false,
                    t: 8,
                    v: false,
                    h: false,
                };
    }

    protected onLoaded(loan:DataModels) : boolean {
        let returnValue = super.onLoaded(loan);

        loan.sDrawSchedules.data.forEach(x => x.isSelected = this.createCheckbox() );
        this.selectAll = this.createCheckbox();
        $('lqb-input[lqb-model="vm.selectAll"]').click(() => this.selectAllChecboxChanged());
        return returnValue;
    }

    addDraw()
    {
        const newItem = angular.merge({},this.loan.sDrawSchedules.template);
        newItem.isSelected = this.createCheckbox();
        newItem.isSelected.v = this.isAllSelected;
        this.loan.sDrawSchedules.data.push(newItem);
    };

    selectAllChecboxChanged()
    {
        const _isAllSelected = this.isAllSelected = !this.isAllSelected;
        this.selectAll.v = _isAllSelected;
        this.loan.sDrawSchedules.data.forEach( function(entry: any) { entry.isSelected.v = _isAllSelected; });
    };

    removeSelectedDraws()
    {
        this.loan.sDrawSchedules.data = this.loan.sDrawSchedules.data.filter( x => x.isSelected.v != true);
    };

    isDeletable(){
        return  this.loan != null && this.loan.sDrawSchedules.data.filter( x => x.isSelected.v == true ).length > 0;
    }
}

export const constructionLoanInfoDirective = createBasicLoanEditorDirective("constructionLoanInfo", appHtml, ConstructionLoanInfoController);
