import {StrGuid, guidEmpty} from "../../../utils/guid";

import {createBasicLoanEditorDirective} from "../../ng-common/createBasicLoanEditorDirective";
import {EditorController} from "../../ng-common/EditorController";
import {DataModels} from "../../ng-common/DataModels";

import * as appHtml from "./index.html";

export class DisclosureCenterController extends EditorController {
    private guidEmpty = guidEmpty;

    private daySinceOpened: number;
    onLoaded(loan:DataModels) {
        super.onLoaded(loan);

        return true;
    }

    private isShowInvalidLoanEstimates: boolean;
    private toggleIsShowInvalidLoanEstimates() {
        this.isShowInvalidLoanEstimates = !this.isShowInvalidLoanEstimates;
    }
    private addLoanEstimates(){
        const {sLoanEstimateDatesInfo} = this.loan;
        sLoanEstimateDatesInfo.data = sLoanEstimateDatesInfo.data.concat(
            [naiveClone(sLoanEstimateDatesInfo.template)]);
        this.loan.calculate();
    }
    private removeLoanEstimates(uniqueId: StrGuid){
        const {sLoanEstimateDatesInfo} = this.loan;
        sLoanEstimateDatesInfo.data = sLoanEstimateDatesInfo.data.filter(d => d.UniqueId.v != uniqueId);
        this.loan.calculate();
    }

    private isShowInvalidClosingDisclosures: boolean;
    private toggleIsShowInvalidClosingDisclosures() {
        this.isShowInvalidClosingDisclosures = !this.isShowInvalidClosingDisclosures;
    }
    private addClosingDisclosure(){
        const {sClosingDisclosureDatesInfo} = this.loan;
        sClosingDisclosureDatesInfo.data = sClosingDisclosureDatesInfo.data.concat(
            [naiveClone(sClosingDisclosureDatesInfo.template)]);
        this.loan.calculate();
    }
    private removeClosingDisclosure(uniqueId: StrGuid){
        const {sClosingDisclosureDatesInfo} = this.loan;
        sClosingDisclosureDatesInfo.data = sClosingDisclosureDatesInfo.data.filter(d => d.UniqueId.v != uniqueId);
        this.loan.calculate();
    }
}
export const disclosureCenterDirective = createBasicLoanEditorDirective("disclosureCenter", appHtml, DisclosureCenterController);

function naiveClone(x: any) { return JSON.parse(JSON.stringify(x)) }
