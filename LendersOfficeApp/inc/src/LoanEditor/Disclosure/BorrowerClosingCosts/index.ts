import {StrGuid, guidEmpty} from "../../../utils/guid";

import {E_sClosingCostAutomationUpdateT        } from "../../../DataLayer/Enums/E_sClosingCostAutomationUpdateT";
import {E_sOriginatorCompensationPaymentSourceT} from "../../../DataLayer/Enums/E_sOriginatorCompensationPaymentSourceT";
import {E_sDisclosureRegulationT               } from "../../../DataLayer/Enums/E_sDisclosureRegulationT";
import {E_IntegratedDisclosureSectionT         } from "../../../DataLayer/Enums/E_IntegratedDisclosureSectionT";
import {E_ClosingCostFeeFormulaT               } from "../../../DataLayer/Enums/E_ClosingCostFeeFormulaT";
import {E_sLenderCreditCalculationMethodT      } from "../../../DataLayer/Enums/E_sLenderCreditCalculationMethodT";
import {Hud800OriginatorCompensationFeeTypeId  } from "../../../DataLayer/DefaultSystemClosingCostFee";

import {recursiveUpdate} from "../../../utils/recursiveUpdate";

import {EditorController} from "../../ng-common/EditorController";
import {ngFilter} from "../../ng-common/ngServices";
import {DataModels} from "../../ng-common/DataModels";
import { IBorrowerClosingCostSection,
         IBorrowerClosingCostSectionFee,
         IBorrowerClosingCostSectionFeeF,
         IBorrowerClosingCostSectionFeePmt} from "../../ng-common/IBorrowerClosingCosts";
import {createBasicLoanEditorDirective } from "../../ng-common/createBasicLoanEditorDirective";
import {isLqbDataModel, E_CalculationType} from "../../ng-common/ILqbDataModel";
import {LqModalController} from "../../components/lqModal";

import * as appHtml from "./index.html";

class BorrowerClosingCostsController extends EditorController {
    private E_sClosingCostAutomationUpdateT         = E_sClosingCostAutomationUpdateT;
    private E_IntegratedDisclosureSectionT          = E_IntegratedDisclosureSectionT ;
    private E_ClosingCostFeeFormulaT                = E_ClosingCostFeeFormulaT       ;
    private E_sOriginatorCompensationPaymentSourceT = E_sOriginatorCompensationPaymentSourceT;
    private E_sLenderCreditCalculationMethodT       = E_sLenderCreditCalculationMethodT;

    private Guid_Empty: StrGuid;
    private Hud800OriginatorCompensationFeeTypeId   = Hud800OriginatorCompensationFeeTypeId;
    private currentTab = 3;
    private calculationModal: LqModalController;

    protected init() {
        this.Guid_Empty = guidEmpty;
    }
    protected onLoaded(loan: DataModels): boolean {
        super.onLoaded(loan);
        return true;
    }

    private isHud800OriginatorCompensationFeeTypeId(typeId: StrGuid) {
        return (typeId.toLowerCase() == Hud800OriginatorCompensationFeeTypeId);
    }

    private showCcAutoOptions(){
        if (this.loan == null) return false;

        const {sIsLoanProgramRegistered} = this.loan.fields;
        const {CalculateclosingCostInPML} = this.loan.BrokerDB;
        const {feeServiceHistory} = this.loan.sClosingCosts;
        console.log("sIsLoanProgramRegistered", sIsLoanProgramRegistered);
        return CalculateclosingCostInPML && sIsLoanProgramRegistered.v && !feeServiceHistory.IsBlank;
    }
    showLenderPaidOriginatorCompensation(){
        try {
            return (
                this.loan.fields.sOriginatorCompensationPaymentSourceT.v == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                this.loan.fields.sDisclosureRegulationT.v == E_sDisclosureRegulationT.TRID2015 &&
                this.loan.fields.sGfeIsTPOTransaction.v
            );
        } catch(e) { return false; }
    }
    addFee(section:  IBorrowerClosingCostSection, fee:  IBorrowerClosingCostSectionFee) {
        console.assert(section.NewFeeTemplates.includes(fee));
        section.ClosingCostFeeList = section.ClosingCostFeeList.concat(fee);
        section.NewFeeTemplates = section.NewFeeTemplates.filter(f => f !== fee);
        this.loan.calculate();
    }
    paymentDelete(section:  IBorrowerClosingCostSection, fee:  IBorrowerClosingCostSectionFee) {
        const amt = (fee.f.t.v === E_ClosingCostFeeFormulaT.MinBaseOnly) ? fee.f.base.v : fee.total.v;
        this.dialog.confirm(`${fee.desc.v || fee.org_desc.v} : ${ngFilter.currency(amt as number)}`, "Are you sure you would like to remove the following fee?")
            .then((confirm) => {
                if (!confirm) return;
                section.ClosingCostFeeList = section.ClosingCostFeeList.filter(f => f !== fee);
                this.loan.calculate();
            });
    }
    removePayment(fee:  IBorrowerClosingCostSectionFee, payment: IBorrowerClosingCostSectionFeePmt) {
        const amt = payment.amt.v;
        const paid_by = payment.paid_by.v;
        const desc = fee.desc.v || fee.org_desc.v;
        var paidByDesc = GetPaidByDesc(paid_by);
        this.dialog.confirm(`Are you sure you would like to remove the ${ngFilter.currency(amt as number)} ${paidByDesc} split payment for the ${desc}?`, "Confirm")
            .then((confirm) => {
                if (!confirm) return;
                fee.payments = fee.payments.filter(p => p !== payment);
                this.loan.calculate();
            });
    }

    pickAgent(){
        // showAgentPicker(section i, fee j)
        debugger;

        // var rolodex = new cRolodex();
        // var args = rolodex.chooseFromRolodex(fee.bene, sLId, true);

        // function ClearAgent(){
        //     // web method ClearAgent(recordId = fee.bene_id)
        // }
        // if (args.RecordId != "" && args.RecordId != guidEmpty) {
        //     fee.bene_id = args.RecordId;
        //     fee.bene = args.AgentType;
        //     fee.disable_bene_auto = false;
        //     // calc
        // }
        // else {
        //     var id = "";
        //     var populateFromRolodex = false;

        //     if (args.BrokerLevelAgentID != guidEmpty) {
        //         //If the selcted contact has no record id, then add it to the agents list.
        //         //If there is a BrokerLevelAgentID, populate using the rolodex and that id

        //         id = args.BrokerLevelAgentID;
        //         populateFromRolodex = true;
        //     } else {
        //         //If the selected contact has no record id, then add it to the agents list.
        //         //If there isn't a BrokerLevelAgentID, populate using the employee info

        //         id = args.EmployeeId;
        //         populateFromRolodex = false;
        //     }

        //     // ConfirmAgent => OK
        //     // web method CreateAgent
        //     const wmData = { loanId: sLId, id: id, populateFromRolodex, agentType: args.AgentType };
        //     const errorMessage = "Error:  Could not create a new agent.";
        //     fee.bene_id = result.RecordId;
        //     fee.bene = args.AgentType;
        //     fee.disable_bene_auto = false;
        // }
    }
    updatePaidBy(fee: IBorrowerClosingCostSectionFee){
        // updatePaidBy(i, j, f.total)
        var value = fee.payments[0].paid_by.v;
        if (value == -1) {
            fee.payments[0].paid_by.v = 1;

            // Add new system payment
            var newPayment = {
                amt      : "",
                ent      : 0,
                is_fin   : false,
                is_system: true,
                made     : false,
                paid_by  : 1,
                pmt_at   : 1,
                pmt_dt   : "",
            };
            // fee.pmts.push(newPayment);
        }
        else if (value == 3) {
            fee.payments[0].paid_by.v = value;
            fee.payments[0].pmt_at.v = 1; // Set to 'at closing'
        }
        else {
            fee.payments[0].paid_by.v = value;
        }

        debugger;
    }
    updatePaidBySplit(fee:  IBorrowerClosingCostSectionFee, payment:  IBorrowerClosingCostSectionFeePmt) {
        // updatePaidBy(i, j, f.total)
        var value = payment.paid_by.v;

        if (value == 3) {
            payment.paid_by.v = value;
            payment.pmt_at.v = 1; // Set to 'at closing'
        }
        else {
            payment.paid_by.v = value;
        }

        debugger;
    }
    AddSplitPayment(fee:  IBorrowerClosingCostSectionFee) {
        const p :  IBorrowerClosingCostSectionFeePmt = angular.merge({}, fee.payments[0]);
        p.amt.v = null;
        p.ent.v = null;
        p.is_fin = {t:0, v:false, r:false, h:false, calcAction:0,};
        p.is_system.v = false;
        p.made.v = false;
        p.paid_by.v = 1;
        p.pmt_at.v = 1;
        p.pmt_dt.v = null;
        fee.payments.push(p);
        this.loan.calculate();
    }
    updateLenderOrigComp(){

    }

    private selectedFee:  IBorrowerClosingCostSectionFee;
    private selectedFeeF:  IBorrowerClosingCostSectionFeeF;
    private fee_t_options = [
        { v: 3, l: 'Flat Amount' },
        { v: 1, l: 'Full' },
        { v: 2, l: 'Percent Only' },
    ];
    private fee_pt_options = [
        { v: 0, l: 'Loan Amount' },
        { v: 1, l: 'Purchase Price' },
        { v: 2, l: 'Appraisal Value' },
        { v: 4, l: 'Total Loan Amount' },
    ];

    private setCalculationModal(calculationModal: LqModalController) {
        this.calculationModal = calculationModal;
        this.calculationModal.setOption({ backdrop:"static", keyboard:false, });
    }
    updateCalc(fee:  IBorrowerClosingCostSectionFee) {
        this.selectedFee = fee;
        this.selectedFeeF = recursiveUpdate(fee.f, (x:any) => {
            if (x == null) return [true, x];
            if (isLqbDataModel(x)) return [true, Object.assign({}, x, { calcAction: E_CalculationType.NoCalculate })];

            return [false, x];
        });

        const {
            sIPiaDyLckd ,
            sIPiaDy     ,
            sIPerDayLckd,
            sIPerDay    ,
        } = this.loan.fields;
        sIPiaDyLckd .__old_v = sIPiaDyLckd .v;
        sIPiaDy     .__old_v = sIPiaDy     .v;
        sIPerDayLckd.__old_v = sIPerDayLckd.v;
        sIPerDay    .__old_v = sIPerDay    .v;

        this.calculationModal.show();
        // updateCalc(i,j)
    }
    onCalculationOk(){
        angular.merge(this.selectedFee.f, recursiveUpdate(this.selectedFeeF, (x: any) => {
            if (x == null) return [true, x];
            if (isLqbDataModel(x)) return [true, {v: x.v}];
            return [false, x];
        }));
        this.selectedFee = this.selectedFeeF = null;
        this.loan.calculate();
        this.calculationModal.close();
    }
    onCalculationCancel(){
        const {
            sIPiaDyLckd ,
            sIPiaDy     ,
            sIPerDayLckd,
            sIPerDay    ,
        } = this.loan.fields;
        sIPiaDyLckd .v = sIPiaDyLckd .__old_v; delete sIPiaDyLckd .__old_v;
        sIPiaDy     .v = sIPiaDy     .__old_v; delete sIPiaDy     .__old_v;
        sIPerDayLckd.v = sIPerDayLckd.__old_v; delete sIPerDayLckd.__old_v;
        sIPerDay    .v = sIPerDay    .__old_v; delete sIPerDay    .__old_v;
        this.loan.calculate();
        this.calculationModal.close();
    }
}

export const borrowerClosingCostsDirective = createBasicLoanEditorDirective("borrowerClosingCosts", appHtml, BorrowerClosingCostsController);

const paidByDescDict = new Map<number, string>([
    [ 1, "borr pd" ],
    [ 2, "seller"  ],
    [ 3, "borr fin"],
    [ 4, "lender"  ],
    [ 5, "broker"  ],
    [ 6, "other"   ],
] as any);
function GetPaidByDesc(paidBy:number) {
    if (paidByDescDict.has(paidBy)) return paidByDescDict.get(paidBy);
    return "UNKNOWN PAID BY";
}

/*

links
    http://dory.vn/ext/newlos/Disclosure/BorrowerClosingCosts.aspx?loanid=0b87c954-e4f3-4bc5-af50-a52c00a5c8ba&appid=0bcac8fd-81f7-4c45-8119-a53a0001939f
    http://localhost/LendersOfficeApp/LoanEditor/LoanEditor.aspx?loanid=0b87c954-e4f3-4bc5-af50-a52c00a5c8ba#/borrowerClosingCosts
    http://dory.vn/ext/los/RolodexList.aspx?type=21&loanid=0b87c954-e4f3-4bc5-af50-a52c00a5c8ba&IsFeeContactPicker=true

TODO
    hide [-] button pee
*/
