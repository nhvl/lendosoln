import {LqModalController      } from "../components/lqModal";
import {ModalStatus            } from "../BorrowerAsset/ModalStatus";
import {ServiceFactory         } from "../ng-common/service";
import {IDialog                } from "../../ng-common/dialog";
import {StrGuid,guidEmpty      } from "../../utils/guid";
import {E_EmployeeStatusFilterT} from "../../DataLayer/Enums/E_EmployeeStatusFilterT";
import {E_RoleT,GetRole        } from "../../DataLayer/Enums/E_RoleT";
import {EditorController       } from "../ng-common/EditorController";
import {LoadingIndicator       } from "../ng-common/loadingIndicator";

export interface IEmployeeAssignment{
    Addr: string;
    BranchId:StrGuid;
    BranchNm:string;
    CellPhone:string;
    City:string;
    CompanyLicenseXml:string;
    CompanyName:string;
    Email:string;
    EmployeeId:StrGuid;
    EmployeeLicenseXml:string;
    EmployeeName:string;
    Fax:string;
    FirstName:string;
    FullName:string;
    FullNameWithIsActiveStatus:string;
    IsActive:boolean;
    LastName:string;
    Licenses:any;
    LoginName:string;
    MiddleName:string;
    Pager:string;
    Phone:string;
    State:string;
    Suffix:string;
    Zip:string;
    [key: string]: any;
}
export class EmployeeRole{
    sortTable:string;
    sortKey: string;
    isAsc: boolean;

    modalInstance: LqModalController;
    modalStatus:number;

    RoleDesc:string;
    Role:E_RoleT;

    PmlBrokerIdFilter:StrGuid;

    ShowMode:string;

    IsOnlyDisplayLqbUsers:boolean;
    IsOnlyDisplayPmlUsers:boolean;

    status:E_EmployeeStatusFilterT;

    warn_assign_lo_message = "This will assign the loan to a different branch. Are you sure you want to continue?";

    employeeAssignment:IEmployeeAssignment[];
    supervisorGrid:any[];
    companyGrid:any[];

    CopyToContactsEnabled:boolean;
    copyToOfficialContactEnabledCheckbox:boolean;
    copyToOfficialContactState:boolean;

    OrigRoleDesc:string;
    OriginationCompanyLimitingRoleDesc:string;

    SearchFilter:string;

    HasManyUser:boolean;
    IsEmpty:boolean;

    CallBack:any;
    currentSelect:number;
    constructor(private dialog: IDialog, private service: ServiceFactory, private _loadingIndicator: LoadingIndicator, private controller: EditorController) {
    }
    assign(role: E_RoleT, roledesc:string, show:string, isLqbOnly:boolean, isPmlOnly:boolean,callback:any){
        this.modalStatus = ModalStatus.Open;
        this.currentSelect = -1;
        this.CallBack = callback;
        this.SearchFilter = "";
        this.CopyToContactsEnabled = true;
        this.copyToOfficialContactEnabledCheckbox = true;
        this.copyToOfficialContactState = false;
        this.status = E_EmployeeStatusFilterT.ActiveOnly;
        this.Role = role;
        this.RoleDesc = roledesc;

        this.PmlBrokerIdFilter = guidEmpty;
        this.IsOnlyDisplayLqbUsers = (isLqbOnly == null) ? false : isLqbOnly;
        this.IsOnlyDisplayPmlUsers = (isPmlOnly == null) ? false : isPmlOnly;
        this.ShowMode = show;

        this.search();

        this.modalStatus = ModalStatus.Open;
        this.modalInstance.show();
    }
    private search(){
        const data = {
            loanId: this.controller.loan.sLId,
            searchFilter:this.SearchFilter,
            status: this.status,
            role: GetRole(this.Role),
            showMode: this.ShowMode,
            isOnlyDisplayLqbUsers: this.IsOnlyDisplayLqbUsers,
            isOnlyDisplayPmlUsers: this.IsOnlyDisplayPmlUsers
        }
        this.service.employeeRole.search(data).then((response: { [key: string]: any }) => {
            this.employeeAssignment = response["SearchResult"];
            this.OriginationCompanyLimitingRoleDesc = response["OriginationCompanyLimitingRoleDesc"];
            this.OrigRoleDesc = response["OrigRoleDesc"];
            this.PmlBrokerIdFilter = response["PmlBrokerIdFilter"];
            if(response["CopyToOfficialContactEnabledCheckbox"] != null){
                this.copyToOfficialContactEnabledCheckbox = Boolean(response["CopyToOfficialContactEnabledCheckbox"]);
            }
            if(response["CopyToOfficialContactChecked"] != null){
                this.copyToOfficialContactState = Boolean(response["CopyToOfficialContactChecked"]);   
            }
            this.setCopyToOfficialContactValue();
            this.HasManyUser = this.CheckManyUser();
            this.IsEmpty = this.CheckEmpty();
            $('input[ng-model="vm.employeeRole.SearchFilter"]').focus();
        });
    }
    private IsDisplayCopyToOfficialContact():boolean{
        return this.CopyToContactsEnabled && (
            (GetRole(this.Role) == "Agent")
            || this.Role == E_RoleT.Processor
            || this.Role == E_RoleT.Manager
            || this.Role == E_RoleT.RealEstateAgent
            || this.Role == E_RoleT.CallCenterAgent
            || this.Role == E_RoleT.LenderAccountExecutive
            || this.Role == E_RoleT.Underwriter
            || this.Role == E_RoleT.LoanOpener
            || this.Role == E_RoleT.Pml_BrokerProcessor
            || this.Role == E_RoleT.Shipper
            || this.Role == E_RoleT.Funder
            || this.Role == E_RoleT.PostCloser
            || this.Role == E_RoleT.Insuring
            || this.Role == E_RoleT.CollateralAgent
            || this.Role == E_RoleT.DocDrawer
            || this.Role == E_RoleT.CreditAuditor
            || this.Role == E_RoleT.DisclosureDesk
            || this.Role == E_RoleT.JuniorProcessor
            || this.Role == E_RoleT.JuniorUnderwriter
            || this.Role == E_RoleT.LegalAuditor
            || this.Role == E_RoleT.LoanOfficerAssistant
            || this.Role == E_RoleT.Purchaser
            || this.Role == E_RoleT.QCCompliance
            || this.Role == E_RoleT.Secondary
            || this.Role == E_RoleT.Servicing
            || this.Role == E_RoleT.Pml_Secondary
            || this.Role == E_RoleT.Pml_PostCloser);
    }
    private GetPmlLabel(index:number):string{
        if(this.employeeAssignment == null || this.employeeAssignment[index] == null) return "";
        //if the actual broker name is not length 0 then the user is a PML user 
        return this.employeeAssignment[index].CompanyName.length != 0 ? "PML" : "";
    }
    private GetBranchNm(index:number):string
    {
        if(this.employeeAssignment == null || this.employeeAssignment[index] == null) return "";
        return this.employeeAssignment[index].CompanyName.length != 0 ? "" : this.employeeAssignment[index].BranchNm;
    }
    private isShowPmlColumn():boolean{
                // 11/1/2004 kb - We only show the external broker name
                // when the broker is a lender user with price my loan
                // enabled.  We expect (especially with loan officers)
                // long lists that underwriters and lender account execs
                // must sift to find the desired outside user.
                //
                // 8/2/2005 kb - We now only show the external broker
                // column with agents in a non-simple mode.
        if(this.employeeAssignment == null) return false;
        return !((!this.controller.loan.BrokerUser.HasPmlEnabled) 
                || (GetRole(this.Role) != "Agent" && this.Role != E_RoleT.Pml_BrokerProcessor &&
                    this.Role != E_RoleT.Pml_Secondary && this.Role != E_RoleT.Pml_PostCloser)
                || this.ShowMode == "simple"
                || this.IsOnlyDisplayLqbUsers);
    }
    private pmlHeader():string {
        // 7/17/2007 av If the user is looking for a loan officer and he/she is from 
        // a pml enabled broker then the pml column is renamed. OPM 10443
        return (this.isShowUserType() ? "Originating Company (PML only)" : "External Broker");
    }
    private isShowUserType():boolean{
        //hides the column if the broker is not  pml enabled or the user is 
        //filling is not looking for a loan officer.
        return (((GetRole(this.Role) == "Agent" && !this.IsOnlyDisplayLqbUsers)
                    || this.Role == E_RoleT.Pml_BrokerProcessor
                    || this.Role == E_RoleT.Pml_Secondary
                    || this.Role == E_RoleT.Pml_PostCloser)
                    && this.controller.loan.BrokerUser.HasPmlEnabled);
    }
    private firstHeader():string {
        if(this.employeeAssignment == null) return "Employee";
        return (((!this.controller.loan.BrokerUser.HasPmlEnabled) 
                || (GetRole(this.Role) != "Agent" && this.Role != E_RoleT.Pml_BrokerProcessor &&
                    this.Role != E_RoleT.Pml_Secondary && this.Role != E_RoleT.Pml_PostCloser)
                || this.ShowMode == "simple"
                || this.IsOnlyDisplayLqbUsers) ? "Employee" : "User");
    }
    
    private isShowBranch():boolean{
        if(this.employeeAssignment == null) return false;
        return this.controller.loan.BrokerUser.AllowLoanAssignmentsToAnyBranch 
            && !((this.IsOnlyDisplayPmlUsers ||
                this.Role == E_RoleT.Pml_Secondary ||
                this.Role == E_RoleT.Pml_PostCloser ||
                this.Role == E_RoleT.Pml_BrokerProcessor));
    }
    private setCopyToOfficialContactValue(){
        if(GetRole(this.Role) != "Agent"){
            if((this.SearchFilter == "") && this.controller.loan.BrokerDB.HasManyUsers)
                this.copyToOfficialContactEnabledCheckbox = false;
        }
        this.copyToOfficialContactEnabledCheckbox = !(this.employeeAssignment==null||this.employeeAssignment.length == 0);

        // OPM 51224 - When changing the assignment of a loan officer, check the "Copy user info to Official Contact List and appropriate forms" by default.
        if (GetRole(this.Role) == "Agent" && this.copyToOfficialContactEnabledCheckbox)
        {
                this.copyToOfficialContactState = true;
        }
        // OPM 150546 - If the lender so desires, set this by default for everyone
        if (this.controller.loan.BrokerDB.CopyInternalUsersToOfficalContactsByDefault)
        {
            this.copyToOfficialContactState = true;
        }
    }
    private copyToOfficialContactClick(){
        if(this.copyToOfficialContactEnabledCheckbox){
            this.copyToOfficialContactState =! this.copyToOfficialContactState;
        }
    }
    private setERModal(modalInstance: LqModalController):void {
        this.modalInstance = modalInstance;
        this.modalInstance.setOption({ backdrop: 'static',
            keyboard: false });
    }
    private onERModalHide(e:JQueryEventObject):void {
        if((this.modalStatus == ModalStatus.Save) && (this.CallBack != null))
            this.CallBack(this.Role, this.controller.loan.sLId, this.copyToOfficialContactState, (this.currentSelect != -1) ? this.employeeAssignment[this.currentSelect]:null);
    }
    private DialogTitle(){
        switch (this.RoleDesc) {
            case "Loan Officer":
                    return "Users to select for role loan officer : ";
            case "Supervisor":
                    return "Supervisors : ";
            case "Originating Company":
                    return "Originating Companies : ";
            case "Processor (External)":
                    return "PML Users to select for role processor (external): ";
            case "Secondary (External)":
                    return "PML Users to select for role secondary (external):";
            case "Post-Closer (External)":
                    return "PML Users to select for role post-closer (external):";
            default:
                    return "Employees to select for role " + ((this.RoleDesc == null) ? "" : this.RoleDesc.toLowerCase()) + " : ";
        }
    }
    private HelpText(){
        return (this.RoleDesc != "Originating Company")
        ? "(\"s\" for John Smith or Sam Cash, \"b s\" for Bob Smith)"
        :"";
    }
    private EmployeeStatusLabel(){
        switch (this.RoleDesc) {
            case "Loan Officer":
                return "User status :";
            case "Processor (External)":
            case "Secondary (External)":
            case "Post-Closer (External)":
                return "PML User status :";
            default: 
                return "Employee status :";
        }
    }
    
    private CheckManyUser(){
        if((GetRole(this.Role) != "Agent") ||(this.controller.loan == null)) 
            return false;
        return (this.SearchFilter == "") && this.controller.loan.BrokerDB.HasManyUsers;
    }
    private ManyUserMessage(){
        let s = "Too many ";
        switch (this.RoleDesc) {
            case "Loan Officer":
                s += "users ";
                break;
            case "Originating Company":
                s += "companies found. ";
                break;
            default: 
                s += "employees ";
        }
        if(this.RoleDesc != "Originating Company"){
            s += "with this role. ";
        }
        s += "Please refine your search using the above filters. <br /><br />";
        if(this.RoleDesc != "Originating Company"){
            s = s + "We match against "
            + ((this.RoleDesc=="Loan Officer") ? "user" : "employee")
            +"'s full name, e.g. \"Jo\", will find all the "
            + ((this.RoleDesc=="Loan Officer") ? "users" : "employees")
            +" with a first or last name that begins with \"Jo\". To search for "
            + ((this.RoleDesc=="Loan Officer") ? "users" : "employees")
            +" by first *and* last name, use two patterns separated by a space. For example: use \"bo sm\" to return \"Bob Smith\", \"Boaz Smalls\", etc.";
        }
        return s;
    }
    private CheckEmpty(){
        if(this.CheckManyUser()) return false;
        return ((this.employeeAssignment == null || this.employeeAssignment.length == 0))
            && ((this.supervisorGrid == null || this.supervisorGrid.length == 0))
            && ((this.companyGrid == null || this.companyGrid.length == 0));
    }
    private EmptyMessage(){
        let s = "No ";
         switch (this.RoleDesc) {
            case "Loan Officer":
                s += "users";
                break;
            case "Processor (External)":
            case "Secondary (External)":
            case "Post-Closer (External)":
                s += "PML users";
                if(this.PmlBrokerIdFilter != guidEmpty)
                    s+=" who belong to the same originating company and match the specified criteria were found.";
                break;
            case "Originating Company":
                s += "companies";
                break;
            default: 
                s += "employees";
        }
        if ((this.RoleDesc != "Processor (External)" 
            && this.RoleDesc != "Secondary (External)" 
            && this.RoleDesc != "Post-Closer (External)") 
            || this.PmlBrokerIdFilter == guidEmpty) 
        {
            s += " matching the specified criteria.  Nothing to show.";
        }
        return s;
    }
    private assignEmployee(index:number){
        if(this.employeeAssignment == null || this.employeeAssignment[index] == null) return;
        const data = {
            employeeID: this.employeeAssignment[index].EmployeeId,
            email: this.employeeAssignment[index].Email,
            employeeName: this.employeeAssignment[index].EmployeeName,
            pmlUser: (this.GetPmlLabel(index)=="PML"),
            loanId: this.controller.loan.sLId,
            copyToOfficialAgent:(this.CopyToContactsEnabled && this.copyToOfficialContactState),
            role:GetRole(this.Role)
        }
        this._loadingIndicator.show();
        this.service.employeeRole.assignEmployee(data).then((response: string[])=>{
            this._loadingIndicator.hide();
            this.currentSelect = index;
            if(response == null || response.length == 0) {
                this.modalStatus = ModalStatus.Save;
                this.modalInstance.close();
            }
            if(response[0] == "WARNING"){
                this.dialog.confirm("This will assign the loan to a different branch. Are you sure you want to continue?","Warning")
                .then((isConfirmed:boolean) => {
                    if(isConfirmed){
                        const data = {
                            loanId: this.controller.loan.sLId,
                            loanofficerId: response[1],
                            branchId: response[2],
                            copyToOfficialAgent:Boolean(response[3]),
                            loanOfficerName: response[4],
                            loanOfficerEmail: response[5],
                            roleTStr:response[6]
                        }
                        this._loadingIndicator.show();
                        this.service.employeeRole.assignLOAndBranch(data).then((response: string) => {
                            this._loadingIndicator.hide();
                            if(response.startsWith("ERROR: ")) {
                                this.dialog.alert(response.replace("ERROR: ",""),"Error");
                            }
                            else{
                                this.modalStatus = ModalStatus.Save;
                                this.modalInstance.close();
                            }
                        });
                    }
                })
            }
            else{
                var error = response.join("\n").trim();
                if(error != "")
                    this.dialog.alert(error,"Error");
                else
                {
                    this.modalStatus = ModalStatus.Save;
                    this.modalInstance.close();
                }
            }
        }).catch((e:any) => {
            this._loadingIndicator.hide();
            this.dialog.alert("An error occurs","Error");
            console.log(e);
        });
    }
    private clear(){
        this._loadingIndicator.show();
        const data = {
            loanId: String(this.controller.loan.sLId),
            copyToOfficialAgent:(this.CopyToContactsEnabled && this.copyToOfficialContactState),
            role:GetRole(this.Role)
        }
        this.service.employeeRole.clearAssignment(data).then((response: string[])=>{
            this._loadingIndicator.hide();
            this.currentSelect = -1;
            if(response == null || response.length == 0){
                this.modalStatus = ModalStatus.Save;
                this.modalInstance.close();
            }
            else{
                var error = response.join("\n").trim();
                if(error!="")
                    this.dialog.alert(error,"Error");
                else
                {
                    this.modalStatus = ModalStatus.Save;
                    this.modalInstance.close();
                }
            }
        }).catch((e:any) => {
            this._loadingIndicator.hide();
            this.dialog.alert("An error occurs");
            console.log(e);
        });
    }
    private cancel(){
        this.modalStatus = ModalStatus.Discard;
        this.modalInstance.close();
    }
    private sortRegulars(table:string, by: string) {
        if (this.controller.loan.isCalculating) return;
        if (by == this.sortKey && table == this.sortTable) this.isAsc = !this.isAsc;
        else {
            this.sortKey = by;
            this.sortTable = table;
            this.isAsc = true;
        }
        var property = this.sortKey;
        var sortOrder = -1;
        if (this.isAsc) sortOrder = 1;
        if(this.sortTable == "EmployeeAssignment"){
            this.employeeAssignment = this.employeeAssignment.sort(function (a, b) {
                var result = 0;
                if((typeof a[property]) == (typeof b[property])
                    && (typeof a[property]) != "string")
                        result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                    else
                        result = (String(a[property]).toLowerCase() < String(b[property]).toLowerCase()) 
                                ? -1 
                                : (String(a[property]).toLowerCase() > String(b[property]).toLowerCase()) 
                                ? 1 : 0;
                return result * sortOrder;
            });
        }
    }
}
