/*
The `lqModal` component for showing a modal.

API:

    - Event:
        - `ref`: call when `lqModal` initialized. It past `$ref` as it self.
            Component should use this event to get the reference of the modal.
        - `onShow`
        - `onShown`
        - `onHide`
        - `onHidden`

    - Methods: public methods in `LqModalController` class

Example

```html
<cc-template-picker
    ref="vm.aModal = $ref"
    open="vm.isOpen"
    loan-id="vm.loan.sLId"
    gfe-version="null"
    on-cancel="vm.onCancelPick()"
    on-selected="vm.onSelected($template)"
></cc-template-picker>
```
*/

import {StrGuid} from "../../../../utils/guid";
import {ServiceFactory} from "../../../ng-common/service";

export class CcTemplatePickerController {
    private $modal        : JQuery;
    private ccTemplates   : any[];

    private open          : boolean;
    private loanId        : StrGuid;
    private gfeVersion    : number;
    private onSelected    : (_: {$template : any}) => void;
    private onRequestClose: () => void;
    private ref           : (_: {$ref  : CcTemplatePickerController}) => void;

    static $inject = ["$element", ServiceFactory.ngName];
    constructor($element: ng.IAugmentedJQuery, private service: ServiceFactory) {
        this.open         = false;

        const $modal = this.$modal = $element.find("div.modal");

        $modal.modal({ show: false, backdrop:"static", });
        // $modal.on("show.bs.modal"  , (e: JQueryEventObject) => { if (e.namespace === 'bs.modal') this.onShow  ({$event: e}); });
        // $modal.on("shown.bs.modal" , (e: JQueryEventObject) => { if (e.namespace === 'bs.modal') this.onShown ({$event: e}); });
        // $modal.on("hide.bs.modal"  , (e: JQueryEventObject) => { if (e.namespace === 'bs.modal') this.onHide  ({$event: e}); });
        // $modal.on("hidden.bs.modal", (e: JQueryEventObject) => { if (e.namespace === 'bs.modal') this.onHidden({$event: e}); });
    }

    private $onInit() {
        if (!!this.ref) this.ref({ $ref: this }); else debugger;
    }

    private $onChanges({open}:{
        open: ng.IChangesObject<boolean>;
    }) {
        if (!!open && open.previousValue != open.currentValue) {
            if (!!open.currentValue) this.show();
            else this.close();
        }
    }

    private show() {
        this.service.closingCostTemplate.getClosingCost({loanId: this.loanId, gfeVersion: this.gfeVersion})
        .then((templates: any[]) => {
            this.ccTemplates = templates;
            this.$modal.modal("show");
            return templates;
        });
    }
    private close() {
        this.$modal.modal("hide");
    }
    private requestClose() {
        this.onRequestClose();
    }
    private apply($template: any) {
        this.service.closingCostTemplate.applyClosingCostTemplate({loanId: this.loanId, templateId: $template.Id})
        .then(() => {
            this.onSelected({$template});
        });
    }
}

export const ccTemplatePicker: ng.IComponentOptions = {
    ngName            : "ccTemplatePicker",
    bindings          : {
        ref           : "&",
        open          : "<",
        loanId        : "<",
        gfeVersion    : "<",
        onRequestClose: "&",
        onSelected    : "&",
    },
    controllerAs  : "vm",
    template      : require("./index.html"),
    controller    : CcTemplatePickerController as any,
};


