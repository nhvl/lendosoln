type StrGuid = string;

export interface IApplicant {
    aAppId : StrGuid;
    aBNm : string;
    aCNm : string;
    aIsPrimary: boolean;
}
