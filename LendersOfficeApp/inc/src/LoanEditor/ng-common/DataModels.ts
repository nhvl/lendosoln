import {EventEmitter} from "events";
import isEqual = require("lodash/isEqual");
import {getCacheValue} from "./getCacheValue";

import {StrGuid} from "../../utils/guid";
import {recursiveUpdate, recursiveTraversal} from "../../utils/recursiveUpdate";

import {ILqbDataModel, ILqbDataModelG,
    LqbInputType, ILqbDataModelRaw,
    updateFromRaw, isLqbDataModel, isValidLqbInputModel} from "./ILqbDataModel";
import {ServiceFactory} from "./service";
import {ILoanSummary} from "./ILoanSummary";
import {IApplicant  } from "./IApplicant";
import {toastrOptions} from "./toastrOptions";

import {E_sLPurposeT                           } from "../../DataLayer/Enums/E_sLPurposeT";
import {E_sBrokerLockPriceRowLockedT           } from "../../DataLayer/Enums/E_sBrokerLockPriceRowLockedT";
import {E_sOriginatorCompensationPaymentSourceT} from "../../DataLayer/Enums/E_sOriginatorCompensationPaymentSourceT";
import {E_sDisclosureRegulationT               } from "../../DataLayer/Enums/E_sDisclosureRegulationT";
import {E_BranchChannelT                       } from "../../DataLayer/Enums/E_BranchChannelT";
import {E_sCorrespondentProcessT               } from "../../DataLayer/Enums/E_sCorrespondentProcessT";
import {E_sOriginatorCompensationPlanT         } from "../../DataLayer/Enums/E_sOriginatorCompensationPlanT";
import {E_AgentRoleT                           } from "../../DataLayer/Enums/E_AgentRoleT";
import {E_PercentBaseT                         } from "../../DataLayer/Enums/E_PercentBaseT";
import {E_sStatusT                             } from "../../DataLayer/Enums/E_sStatusT";

import {IAssignedContactsFromFee} from "./IAssignedContactsFromFee";

import {IBorrowerClosingCosts} from "./IBorrowerClosingCosts";
import {IAgent, IAgentComission} from "./IAgent";
import {ISettlementServiceProvider, IAvailableSettlementServiceProviderGroup } from "./ISettlementServiceProvider";

import {IEDocument} from "./IEdocument";
import {IBrokerUser} from "./IBrokerUser";
import {IBrokerDb} from "./IBrokerDb";

import {ILoanEstimateDate} from "./ILoanEstimateDate";
import {IClosingDisclosureDate} from "./IClosingDisclosureDate";

export const WarnSaveMessage = "Your change is not save. Do you really want to exit?";

export interface IDataModelsRaw {
    app: ILoanApp;
        aBAliases?: ILqbDataModel[],
        aCAliases?: ILqbDataModel[],
    fields: { [key: string]: ILqbDataModelRaw },
    summary: ILoanSummary;

    applicants: {[key: string]: string }[];

    sDrawSchedules: {
        template: any;
        data: any[];
    },
}

let service: ServiceFactory;
let $q: ng.IQService;

import {IDataContents} from "./IDataContents";
type DataTemplateList<T> = { data: T[], template: T };

export class DataModels { static ngName = "DataModels";
    app               : ILoanApp;

    fields            : ILoanFields;
    preparerFields    : {[key: string]: IPreparerFields};
    sAgentOfRole      : {
        Seller       ?: IAgent[];
    };

    alternateLenders: {
        template: any;
        data: { [key: string]: any }[];
    };
    sLinkedLoanInfo   : any;
    sClosingCosts     : IBorrowerClosingCosts;
    EmployeeLoanAssignment: {
        data: { [key: string]: any }[];
    };
    PmlBroker: {
        NmlsName: string;
        CompanyId: string;
        PmlBrokerId: StrGuid;
    };
    sAgentList:{
        data: { [key: string]: any }[];
    }
    AgentRecord: IAgentRecord;
    sDocCollection    : {
        documents     : IEDocument[];
    };
    sTitleBorrowers   : DataTemplateList<{ [key: string]: ILqbDataModel }>;
    sSellerCollection : DataTemplateList<ISeller>;
    AgentCommission   : DataTemplateList<IAgentComission>;
    sHomeownerCounselingOrganizationCollection: DataTemplateList<IHomeownerCounselingOrganization>;
    sDrawSchedules    : DataTemplateList<any>;

    sLoanEstimateDatesInfo : DataTemplateList<ILoanEstimateDate>;
    sClosingDisclosureDatesInfo : DataTemplateList<IClosingDisclosureDate>;

    BrokerUser        : IBrokerUser;
    BrokerDB          : IBrokerDb;

    SettlementServiceProviders: {
        sMiscSettlementServiceProviders           : DataTemplateList<ISettlementServiceProvider>;
        sTitleSettlementServiceProviders          : DataTemplateList<ISettlementServiceProvider>;
        sEscrowSettlementServiceProviders         : DataTemplateList<ISettlementServiceProvider>;
        sSurveySettlementServiceProviders         : DataTemplateList<ISettlementServiceProvider>;
        sPestInspectionSettlementServiceProviders : DataTemplateList<ISettlementServiceProvider>;
        sAllSettlementServiceProviders            : DataTemplateList<ISettlementServiceProvider>;
        // sAvailableSettlementServiceProviders      : DataTemplateList<ISettlementServiceProvider>;
    };
    AssignedContactsFromFees: IAssignedContactsFromFee[];
    AvailableSettlementServiceProviderGroups: IAvailableSettlementServiceProviderGroup[];

    dataContents: IDataContents;

    summary:ILoanSummary;
    applicants: IApplicant[];

    sLId               : StrGuid;
    appId              : StrGuid;
    //----------------------------------------------
    skipDirtyCheck1Time: boolean;
    private eventEmitter: EventEmitter;

    constructor(dm?: IDataModelsRaw, sLId?: string, appId?: string) {
        this.app = {
            fields : {},
        };
        this.fields = {};

        this.preparerFields = {};
        this.sAgentOfRole = {};

        this.sLId = sLId;
        this.appId = appId;

        this.skipDirtyCheck1Time = false;

        if (dm != null) this.update(dm);

        this.isCalculating = false;
        this.deferChangedFieldsForCalculate = [];

        setTimeout(()=> {
            this._cacheValue = this.getCacheValue();
        }, 100);

        this.eventEmitter = new EventEmitter();

        window.onbeforeunload = () => {
            if (this.skipDirtyCheck1Time) {
                this.skipDirtyCheck1Time = false;
                return;
            }

            $(document.activeElement).trigger("change");
            if (this.isDirty()) return WarnSaveMessage;
        }
    }
    protected update(dm: IDataModelsRaw) {
        if (dm == null) { console.error("dm is null"); return; }

        updateFromRaw(dm, this);

        this.summary = dm.summary;

        this.applicants = dm.applicants.map(a => ({ aAppId: a["appId"], aBNm:a["borrName"], aCNm:a["coBorrName"], aIsPrimary: (a["isPrimary"] == "True") }));

        {
            const {aOtherIncomeList} = this.app;
            if (aOtherIncomeList != null) {
                const {aBNm, aBFirstNm, aBLastNm,
                       aCNm, aCFirstNm, aCLastNm,} = this.app.fields;
                let B = "B";
                let C = "C";
                try  { B = (aBNm != null) ? aBNm.v : `${aBFirstNm.v} ${aBLastNm.v}`; } catch(e){ debugger; }
                try  { C = (aCNm != null) ? aCNm.v : `${aCFirstNm.v} ${aCLastNm.v}`; } catch(e){ debugger; }

                aOtherIncomeList.data.concat([aOtherIncomeList.template])
                .forEach((otherIncome: IOtherIncomeItem) => {
                    otherIncome.isCo.options = otherIncome.isCo.options.map(o =>
                        o.l === "B" ? ({l: B, v: o.v}) :
                        o.l === "C" ? ({l: C, v: o.v}) : o);
                });
            }
        }
    }

    public isCalculating: boolean;
    private deferChangedFieldsForCalculate: ILqbDataModel[];
    private calcPromise: ng.IPromise<DataModels>;

    public on(event: "preCalc" | "postCalc", f:Function) {
        this.eventEmitter.on(event, f);
    }
    public once(event: "preCalc" | "postCalc", f: Function) {
        this.eventEmitter.once(event, f);
    }

    populate(templateId: string){
        const p: ng.IPromise<DataModels> = this.calcPromise =
            service.loan.populate(this.sLId, this.appId, this, templateId).then((dm) => {
                this.update(dm);
                return this;
            });
        p.catch(() => {
            toastr.error(`Populating Failed`, null, toastrOptions);
        });
        return p;
    }

    calculate(dm?: ILqbDataModel): ng.IPromise<DataModels> {
        if (this.isCalculating) { this.deferChangedFieldsForCalculate.push(dm); return; }

        // fire event onPreCalcEvents
        this.eventEmitter.emit("preCalc", dm);
        this.deferChangedFieldsForCalculate = [];
        this.isCalculating = true;

        toastr.info(`onCalculate: ${(!dm) ? "all" : JSON.stringify(dm.v)}`, null, toastrOptions);
        const p: ng.IPromise<DataModels> = this.calcPromise =
            service.loan.calculate(this.sLId, this.appId, this).then((dm) => {
                this.update(dm);
                this.isCalculating = false;
                if (this.deferChangedFieldsForCalculate.length > 0) {
                    return this.calculate(this.deferChangedFieldsForCalculate[0]);
                }
                this.calcPromise = null;
                this.eventEmitter.emit("postCalc");
                return this;
            });
        p.catch(() => {
            toastr.error(`Calculate Failed`, null, toastrOptions);
            this.isCalculating = false });
        return p;
    }

    calculateZip(dm?: ILqbDataModel): ng.IPromise<DataModels> {
        if (this.isCalculating) { this.deferChangedFieldsForCalculate.push(dm); return; }

        this.deferChangedFieldsForCalculate = [];
        this.isCalculating = true;

        toastr.info(`onCalculate: ${(!dm) ? "all" : JSON.stringify(dm.v)}`, null, toastrOptions);
        const p: ng.IPromise<DataModels> = service.loan.calculateZipcode(this.sLId, this.appId, this)
            .then((dm) => {
                this.update(dm);
                if (this.deferChangedFieldsForCalculate.length > 0) {
                    return this.calculate(this.deferChangedFieldsForCalculate[0]);
                }
                this.isCalculating = false;
                return this;
            });

        p.catch(() => {
            toastr.error(`Calculate Failed`, null, toastrOptions);
            this.isCalculating = false
        });

        return p;
    }

    save() {
        const calcPromise = (this.isCalculating && this.calcPromise) ? this.calcPromise : $q.when(null);
        return calcPromise.then(() => {
            this.deferChangedFieldsForCalculate = [];

            const p: ng.IPromise<DataModels> = service.loan.save(this.sLId, this.appId, this)
                .then((dm) => {
                    this.update(dm);
                    this._cacheValue = this.getCacheValue();
                    return this;
                })
            return p;
        });
    }

    validateToSave(): string {
        let message: string = null;
        recursiveTraversal(this, (x: any, paths: (string|number)[]) => {
            if (!isLqbDataModel(x)) return { handled: false, terminate: false };

            const path = paths.join(".");
            let terminate = !isValidLqbInputModel(x, path);
            if (terminate) {
                console.log(path + " required but empty");
                message = "Please fill in all required fields before saving data.";
            }
            return {handled: true, terminate};
        });
        return message;
    }

    private _cacheValue: any;
    private getCacheValue(){
        return getCacheValue(this);
    }

    isDirty(){
        const cache = this.getCacheValue();
        return !isEqual(this._cacheValue, cache);
    }

    //LN: update agent record data and reset cache.
    updateAgentRecordData(dm:any) {
        updateFromRaw(dm , this.AgentRecord);
        setTimeout(()=> {
            this._cacheValue = this.getCacheValue();
        }, 100);
    }

    static get(sLId: string, appId: string, fieldList: string[]):ng.IPromise<DataModels> {
        return service.loan.load(sLId, appId, fieldList)
            .then((dm: IDataModelsRaw) => new DataModels(dm, sLId, appId));
    }

    static ngFactory(_service: ServiceFactory, _$q:ng.IQService) {
        service = _service;
        $q = _$q;

        return DataModels;
    }
}
DataModels.ngFactory.$inject = [ServiceFactory.ngName, "$q"];


export interface ILoanFields {
    sLNm                                          ?: ILqbDataModelG<string>;

    sLPurposeT                                    ?: ILqbDataModelG<E_sLPurposeT>;
    sBrokerLockPriceRowLockedT                    ?: ILqbDataModelG<E_sBrokerLockPriceRowLockedT>;
    sRateLockHistoryXmlContent                    ?: ILqbDataModelG<string>;
    sOriginatorCompensationPaymentSourceT         ?: ILqbDataModelG<E_sOriginatorCompensationPaymentSourceT>;
    sDisclosureRegulationT                        ?: ILqbDataModelG<E_sDisclosureRegulationT>;
    sGfeIsTPOTransaction                          ?: ILqbDataModelG<boolean>;
    sConcurSubFin                                 ?: ILqbDataModelG<number>;
    sSubFinIR                                     ?: ILqbDataModelG<number>;

    sClosedD                                      ?: ILqbDataModelG<Date>;
    sOpenedD                                      ?: ILqbDataModelG<Date>;

    sIPiaDyLckd                                   ?: ILqbDataModelG<boolean>;
    sIPiaDy                                       ?: ILqbDataModelG<number>;
    sIPerDayLckd                                  ?: ILqbDataModelG<boolean>;
    sIPerDay                                      ?: ILqbDataModelG<number>;

    sBranchChannelT                               ?: ILqbDataModelG<E_BranchChannelT>;
    sCorrespondentProcessT                        ?: ILqbDataModelG<E_sCorrespondentProcessT>;

    sOriginatorCompensationPlanT                  ?: ILqbDataModelG<E_sOriginatorCompensationPlanT>;
    sOriginatorCompensationAppliedBy              ?: ILqbDataModelG<string>;
    sOriginatorCompensationPlanSourceEmployeeName ?: ILqbDataModelG<string>;

    HasPmlEnabled                                 ?: ILqbDataModelG<boolean>;

    sSpZip                                        ?: ILqbDataModelG<string>;

    sProMInsT                                     ?: ILqbDataModelG<E_PercentBaseT>;

    sStatusT                                      ?: ILqbDataModelG<E_sStatusT>;

    sIsLegacyClosingCostVersion                   ?: ILqbDataModelG<boolean>;
    sIsInTRID2015Mode                             ?: ILqbDataModelG<boolean>;

    IsTemplate                                    ?: ILqbDataModelG<boolean>;

    [key: string]: ILqbDataModel;
}

export interface ILoanApp {
    fields              : ILoanAppFields;
    aOtherIncomeList   ?: DataTemplateList<IOtherIncomeItem>;
    aAssetCollection   ?: {
        cashDeposit1    : { [key: string]: ILqbDataModel };
        cashDeposit2    : { [key: string]: ILqbDataModel };
        businessWorth   : { [key: string]: ILqbDataModel };
        lifeInsurance   : { [key: string]: ILqbDataModel };
        retirement      : { [key: string]: ILqbDataModel };
        regularTemplate : { [key: string]: ILqbDataModel };
        regulars       ?: { [key: string]: ILqbDataModel }[];
    };
    aLiaCollection     ?: {
        regularTemplate : { [key: string]: ILqbDataModel };
        regulars       ?: { [key: string]: ILqbDataModel }[];
    };
    aReCollection      ?: {
        RegularTemplate : { [key: string]: ILqbDataModel };
        Regulars       ?: { [key: string]: ILqbDataModel }[];
    };

    aCAliases          ?: ILqbDataModel[];
    aBAliases          ?: ILqbDataModel[];
}

export interface ILoanAppFields {
    aBFirstNm     ?: ILqbDataModelG<string>;    aCFirstNm     ?: ILqbDataModelG<string>;
    aBLastNm      ?: ILqbDataModelG<string>;    aCLastNm      ?: ILqbDataModelG<string>;
    aBNm          ?: ILqbDataModelG<string>;    aCNm          ?: ILqbDataModelG<string>;

    aBZip         ?: ILqbDataModelG<string>;
    [key: string]  : ILqbDataModel;
}

export interface IOtherIncomeItem {
    isCo ?: ILqbDataModelG<boolean>;
    desc ?: ILqbDataModelG<string>;
    val  ?: ILqbDataModelG<number>;
}


export interface ISeller {
    Name              : ILqbDataModelG<string>;
    AgentId           : ILqbDataModelG<StrGuid>;
    Address           : {
        StreetAddress : ILqbDataModelG<string>;
        City          : ILqbDataModelG<string>;
        State         : ILqbDataModelG<string>;
        Zip           : ILqbDataModelG<string>;
    };
    Type              : ILqbDataModelG<string>;
    EntityType        : ILqbDataModelG<string>;
}

export interface IHomeownerCounselingOrganization {
    Name               : ILqbDataModelG<string>;
    Address            : {
        StreetAddress  : ILqbDataModelG<string>;
        StreetAddress2 : ILqbDataModelG<string>;
        City           : ILqbDataModelG<string>;
        State          : ILqbDataModelG<string>;
        Zip            : ILqbDataModelG<string>;
    };
    Phone              : ILqbDataModelG<string>;
    Email              : ILqbDataModelG<string>;
    Website            : ILqbDataModelG<string>;
    Services           : ILqbDataModelG<string>;
    Languages          : ILqbDataModelG<string>;
    Distance           : ILqbDataModelG<string>;
}

export interface IPreparerFields {
    IsLocked                        ?: ILqbDataModelG<boolean>,
    HadInitialCheck                 ?: ILqbDataModelG<boolean>,
    NameLocked                      ?: ILqbDataModelG<boolean>,
    LicenseNumOfAgentLocked         ?: ILqbDataModelG<boolean>,
    AgentRoleT                      ?: ILqbDataModelG<number>,
    PhoneOfCompany                  ?: ILqbDataModelG<string>,
    FaxOfCompany                    ?: ILqbDataModelG<string>,
    PreparerFormT                   ?: ILqbDataModelG<number>,
    PreparerName                    ?: ILqbDataModelG<string>,
    TaxId                           ?: ILqbDataModelG<string>,
    ChumsId                         ?: ILqbDataModelG<string>,
    Title                           ?: ILqbDataModelG<string>,
    LicenseNumOfAgent               ?: ILqbDataModelG<string>,
    LicenseNumOfAgentIsExpired      ?: ILqbDataModelG<boolean>,
    LicenseNumOfCompany             ?: ILqbDataModel,
    LicenseNumOfCompanyIsExpired    ?: ILqbDataModelG<boolean>,
    DepartmentName                  ?: ILqbDataModelG<string>,
    CaseNum                         ?: ILqbDataModel,
    CompanyName                     ?: ILqbDataModelG<string>,
    StreetAddr                      ?: ILqbDataModelG<string>,
    StreetAddrMultiLines            ?: ILqbDataModelG<string>,
    StreetAddrSingleLine            ?: ILqbDataModelG<string>,
    City                            ?: ILqbDataModelG<string>,
    State                           ?: ILqbDataModelG<string>,
    Zip                             ?: ILqbDataModelG<string>,
    Phone                           ?: ILqbDataModelG<string>,
    CellPhone                       ?: ILqbDataModelG<string>,
    PagerNum                        ?: ILqbDataModel,
    FaxNum                          ?: ILqbDataModel,
    EmailAddr                       ?: ILqbDataModelG<string>,
    CityStateZip                    ?: ILqbDataModelG<string>,
    LoanOriginatorIdentifier        ?: ILqbDataModel,
    CompanyLoanOriginatorIdentifier ?: ILqbDataModel,
    AgentId                         ?: ILqbDataModelG<StrGuid>,
    IsEmpty                         ?: ILqbDataModelG<boolean>,
    PrepareDate                     ?: ILqbDataModelG<Date>,
    IsNewRecord                     ?: ILqbDataModelG<boolean>,
    IsValid                         ?: ILqbDataModelG<boolean>,
    RecordId                        ?: ILqbDataModelG<StrGuid>,
}

export interface IAgentRecord {
    RecordId                          : ILqbDataModel;
    EmailAddr                         : ILqbDataModel;
    OverrideLicenses                  : ILqbDataModel;
    LicenseNumOfCompany               : ILqbDataModel;
    LicenseNumOfAgent                 : ILqbDataModel;
    IsListedInGFEProviderForm         : ILqbDataModel;
    ProviderItemNumber                : ILqbDataModel;
    IsLenderAffiliate                 : ILqbDataModel;
    IsLenderRelative                  : ILqbDataModel;
    HasLenderRelationship             : ILqbDataModel;
    HasLenderAccountLast12Months      : ILqbDataModel;
    IsUsedRepeatlyByLenderLast12Months: ILqbDataModel;
    IsLenderAssociation               : ILqbDataModel;
    LicenseInfoList                   : any;
    CompanyLicenseInfoList            : any;
    LicenseTemplate                   : ILoanFields[];
    [key: string]                     : any;
}
