import {HtmlInputInput} from "./HtmlInputInput";

const maskDefaultOptions = { dataMask:false, watchInputs:false };

export class ZipcodeInput extends HtmlInputInput<string> {
    protected init() {
        this.$element.html('<span><input type="text" maxlength="5"></span>');
        this.$input = this.$element.find("input");
        this.$input.mask('00000', maskDefaultOptions);
    }
    dispose() {
        this.$input.unmask();
        super.dispose();
    }
}
