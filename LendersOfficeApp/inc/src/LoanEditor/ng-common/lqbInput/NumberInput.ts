import {HtmlInputInput} from "./HtmlInputInput";

const zeroValue = 0;

export class NumberInput extends HtmlInputInput<number> {
    private hideZero: boolean;
    
    protected init() {
        this.$element.html('<span><input type="text"></span>');
        this.$input = this.$element.find("input");
        if (typeof this.attrs.hideZero == "string") {
            this.hideZero = this.attrs.hideZero != "false";
        } else {
            this.hideZero = false;
        }
    }
    // LN - 247662
    /*protected getValueFromDom(){
        const v = Number.parseFloat(this.$input.val());
        return (Number.isNaN(v) ? null : v);
    }*/

    protected getValueFromDom(){ 
        var val = super.getValueFromDom();;

        if (this.hideZero && val == ''){
            return zeroValue;
        }

        return val;
    }
    public setValueToDom(v: number) {
        if (this.hideZero && v == zeroValue){
            this.$input.val('');
        } else {
            super.setValueToDom(v);
        }
    }
}
