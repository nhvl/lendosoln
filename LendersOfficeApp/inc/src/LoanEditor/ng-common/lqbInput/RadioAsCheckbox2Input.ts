import {Radio2Input} from "./Radio2Input";

const template = (
    `<label class="checkbox">
        <span class="material-icons">${require("../../../../../images/Prototype/checkbox.svg")}</span>
        <span></span>
    </label>`);
export class RadioAsCheckbox2Input extends Radio2Input {
    private ngFalseValue : any;

	protected init() {
        this.ngValue = this.$eval("ngValue");
        this.ngFalseValue = this.$eval("ngFalseValue");
        this.$element.html(template);

        this.$element.on("click", "label:not(.disabled)", () => {
            const {dm} = this;
            console.assert(!dm.r);

            if (this.isSynced()) {
                const value = this.ngFalseValue;
                this.onDomChangeUpdateModel(value);
            } else {
                const value = this.getValueFromDom();
                //console.debug("from dom to model: set value [%s] = %s", attrs.lqbModel, JSON.stringify(value));
                this.onDomChangeUpdateModel(value);
            }


        });

        const $span = this.$element.find("span:not(.material-icons)");
        if (!this.attrs.label) $span.remove()
        else $span.text(this.attrs.label);
    }
}
