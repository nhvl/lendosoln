import {LqbInputType} from "../ILqbDataModel";
import {HtmlInputInput} from "./HtmlInputInput";
// TODO: this is not HtmlInput

const template = (
`<div class="input-group dropdown">
    <span><div class="dropdown-input"></div></span>
    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role=button aria-expanded="false"><span class="caret"></span></button>
    <ul class="dropdown-menu dropdown-menu-right gu-show" role="menu"></ul>
</div>`);

export class Dropdown2Input extends HtmlInputInput<string> {
    private selectedValue: any;
    private $dropdownTrigger: JQuery;

    protected init() {
        const {dm, $element} = this;
        console.assert(dm.t === LqbInputType.Dropdown);

        $element.html(template);

        $element.attr("typeof", "dropdown");

        const $input = this.$input = $element.find("div.dropdown-input");
        const $inputGroup = $element.find("div.input-group");
         this.$dropdownTrigger = $element.find("button.dropdown-toggle");

        $input.click(() => {
            setTimeout(() => {
                this.$dropdownTrigger.click();
            });
        });

        if (Array.isArray(dm.options)) this.setOptions(dm.options);

        // Handle option selection
        {
            const $ul = this.$element.find("ul.dropdown-menu");
            $ul.on("click", "li", (e) => {
                const $li = $(e.currentTarget);
                if (!$li.hasClass("active")) {
                    $ul.find("li").removeClass("active");
                    $li.addClass("active");

                    this.selectedValue = $li.data("lqValue");
                    $input.text($li.text());
                }
                this.onDomChangeUpdateModel(this.selectedValue);
            });

            // LN - 239428
            this.$dropdownTrigger.on("click", () => {
                setTimeout(() => {
                    $ul.find("li.active a").focus();
                });
            });
            $ul.focusin((e: JQueryEventObject) => {
                const $li = $(e.target.parentNode);
                if (!$li.hasClass("active")) {
                    $ul.find("li").removeClass("active");
                    $li.addClass("active");

                    this.selectedValue = $li.data("lqValue");
                    $input.text($li.text());
                }
                //_this.onDomChangeUpdateModel(_this.selectedValue);
            });
            $ul.keydown((e: JQueryKeyEventObject)=>{
                if (e.which == 13) {
                    setTimeout(() => {
                            $ul.find("li.active").click();
                        });
                }
            });
        }
    }

    protected getValueFromDom() { return this.selectedValue; }
    public setValueToDom(value: any) {
        this.selectedValue = value;
        this.updateInputText();
    }
    public setReadonly(state: boolean) {
        this.$input.toggleClass("disabled", state);
        this.$dropdownTrigger.toggleClass("disabled", state);
        (this.$element.find("button")[0] as HTMLButtonElement).disabled = state;
    }
    public setOptions(options: { l:string, v:any }[]) {
        const os = options.map(o => {
            const op = $(`<li><a class='cursor' tabindex='0'>${o.l}</a></li>`);
            op.data("lqValue", o.v);
            return op;
        });
        this.$element.find("ul.dropdown-menu").empty().append(os);
        this.updateInputText();
    }
    private updateInputText() {
        const {selectedValue, $element, $input} = this;
        // const o = dm.options.find(o => o.v === value);
        // const text = o == null ? "" : o.l;

        const $ul = $element.find("ul.dropdown-menu");
        $ul.find("li").removeClass("active");

        const $li = $ul.find("li").filter((i: number, li: HTMLLIElement) => $(li).data("lqValue") === selectedValue).first();
        $li.addClass("active");

        $input.text($li.text());
    }

    protected postInit(){
        this.setupCssClass();
    }
    protected isSynced():boolean {
        return this.dm.v === this.getValueFromDom();
    }
}
