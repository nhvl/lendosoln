/// <reference path="../../../dt-local/autonumeric.d.ts" />
/// <reference path="../../../dt-local/jquery-mask-plugin/index.d.ts" />

import {InputSupportType, LqbInputType, ILqbDataModel, E_CalculationType} from "../ILqbDataModel";
import {DataModels} from "../DataModels";
import {toastrOptions} from "../toastrOptions";
import {EventEmitter} from "events";

import {ILiAttributes} from "./ILiAttributes";
import {inputFactory} from "./inputFactory";

import {copyToClipboard, ICopyFunc} from "../../../utils/copyToClipboard";
let copy: ICopyFunc;

let $timeout : ng.ITimeoutService;

$.jMaskGlobals.watchInputs = false;

export function lqbInputGen(getModelFromScope:(scope: ng.IScope) => any) {
    return function lqbInput(_$timeout:ng.ITimeoutService): ng.IDirective {
        $timeout = _$timeout;
        return {
            restrict: "E",
            link(scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: ILiAttributes): void {
                if (!attrs.lqbModel) {
                    const m = "directive <lqbInput> has not initialize with `lqbModel` attribute";
                    console.error(m, element[0], attrs); debugger;
                    return;
                }

                const inputName = attrs.lqbModel.split(".").slice(-1)[0];
                const dmUnWatch = scope.$watch(attrs.lqbModel, (dm: ILqbDataModel) => {
                    if (dm == null) return;
                    dmUnWatch();
                    setup(dm);
                });

                // const eventEmitter = new EventEmitter();
                // scope.$watch(`${attrs.lqbModel}.v`,  (newValue: InputSupportType, oldValue: InputSupportType) => {
                //     eventEmitter.emit("change.lqbModel.v", newValue, oldValue);
                // });
                // if (typeof attrs.readonly == "string") { }
                // else scope.$watch(`${attrs.lqbModel}.r`, (value: boolean) => { eventEmitter.emit("change.lqbModel.r", value); });

                // scope.$watch(`${attrs.lqbModel}.h`, (value: boolean) => {
                //     element.hidden = value;
                //     eventEmitter.emit("change.lqbModel.h", value);
                // });
                // scope.$watch(`${attrs.lqbModel}.options`, (options: { l:string, v:any }[]) => {
                //     eventEmitter.emit("change.lqbModel.h", options);
                // });


                on_Ctrl_Alt_Q(element, (event: JQueryEventObject) => {
                    if (copy == null) copy = copyToClipboard();

                    copy(inputName, event.originalEvent, null);
                    toastr.info(`Field Id: ${inputName} is copied to your clipboard.`, null, toastrOptions);
                });

                function setup(dm: ILqbDataModel) {
                    let loan = getModelFromScope(scope);

                    const input = inputFactory(scope, element, attrs, dm);
                    input.onChange((value: any) => {
                        $timeout(() => {
                            const loan = getModelFromScope(scope);
                            dm.v = value as any;

                            if (!!attrs.lqbChange) try {
                                scope.$eval(attrs.lqbChange);
                            } catch(e) { debugger; }

                            if (loan == null) return;
                            if (dm.calcAction === E_CalculationType.NoCalculate) return;
                            if (dm.t === LqbInputType.Zipcode || dm.calcAction === E_CalculationType.CalculateZip)
                                loan.calculateZip(dm);
                            else loan.calculate(dm);
                        });
                    });

                    scope.$watch(`${attrs.lqbModel}.v`, (value: InputSupportType) => {
                        element.addClass("has-success");
                        setTimeout(() => { element.removeClass("has-success"); }, 1000);

                        if (input != null) input.setValueToDom(value);
                        input.updateRequireField();
                    });

                    if (typeof attrs.readonly == "string") {
                        if (input != null)
                            input.setReadonly(attrs.readonly != "false");
                    }
                    else scope.$watch(`${attrs.lqbModel}.r`, (value: boolean) => {
                        if (input != null) input.setReadonly(value);
                    });

                    scope.$watch(`${attrs.lqbModel}.required`, (value: boolean) => {
                        (element[0] as HTMLInputElement).required = value;
                        if (input != null) input.setRequired(value);
                    });

                    scope.$watch(`${attrs.lqbModel}.h`, (value: boolean) => {
                        element[0].hidden = value;
                        if (input != null) input.setHidden(value);
                    });
                    scope.$watch(`${attrs.lqbModel}.options`, (options: { l:string, v:any }[]) => {
                        if (input != null) input.setOptions(options);
                     });
                }

                scope.$on("$destroy", () => {
                    element.off();
                    element.find("*").off();
                });
            },
        };
    }
}

///////////////////////////////////////////////////////////////////
const keys_Q = 81;
function on_Ctrl_Alt_Q(element: JQuery, func: (event: JQueryKeyEventObject) => void) {

    element.on("keydown.shortkey", (event: JQueryKeyEventObject) => {
        const is_Ctrl_Alt_Q = (event.ctrlKey && event.altKey && event.keyCode === keys_Q);
        if (!is_Ctrl_Alt_Q) return;

        func(event);
        return false;
    });
}
