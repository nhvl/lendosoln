import {lqbInputGen       } from "./lqbInputGen";
import {DataModels        } from "../DataModels";

export const lqbInput = lqbInputGen(getLoanFromScope);
lqbInput.ngName = "lqbInput";
lqbInput.$inject= ["$timeout"];

function getLoanFromScope(scope: ng.IScope): DataModels {
    let loan: DataModels;
    let leScope = scope;
    while (leScope != null && (loan = leScope.$eval("vm.loan")) == null)
        leScope = leScope.$parent;
    if (loan == null) console.warn("vm.loan not found", scope);
    return loan;
}


