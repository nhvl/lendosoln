import {LqbInputType} from "../ILqbDataModel";
import {HtmlInputInput} from "./HtmlInputInput";

export class TextInput extends HtmlInputInput<string> {
    protected init() {
        const {dm, $element} = this;
        $element.html(!Array.isArray(dm.options) ?
                `<span>
                    <input type="text" class="form-control">
                </span>` :
                `<div class="input-group combobox">

                    <span><input type="text"></span>
                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></button>
                    <ul class="dropdown-menu dropdown-menu-right gu-show" role="menu">
                        ${dm.options.map(o => `<li><a class='cursor' tabindex='0'>${o.v}</a></li>`).join("\n")}
                    </ul>
                </div>`);
        $element.attr("typeof", Array.isArray(dm.options) ? "combobox" : "textbox");

        const $input = this.$input = this.$element.find("input");
        $input.attr("placeholder", $element.attr("placeholder"));

        if (Array.isArray(dm.options)) {
            // LN - 246954
            const _this = this;
            const $ul = this.$element.find("ul.dropdown-menu");
            $ul.on("click", "li", (e: JQueryEventObject) => {
                const $li = $(e.currentTarget);
                if (!$li.hasClass("active")) {
                    $ul.find("li").removeClass("active");
                    $li.addClass("active");

                    $input.val($li.text());
                }
                _this.onDomChangeUpdateModel($li.text());
            });
            this.$element.find("button").click(() => {
                setTimeout(() => {
                    $ul.find("li.active a").focus();
                });
            });
            $ul.focusin((e:JQueryEventObject) => {
                const $li = $(e.target.parentNode);
                if (!$li.hasClass("active")) {
                    $ul.find("li").removeClass("active");
                    $li.addClass("active");

                    $input.val($li.text());
                }
                //_this.onDomChangeUpdateModel(_this.selectedValue);
            });
            $ul.keydown((e: JQueryKeyEventObject) => {
                if (e.which == 13) {
                    setTimeout(() => {
                        $ul.find("li.active").click();
                    });
                }
            });
        }
    }
    public setReadonly(state: boolean) {
        super.setReadonly(state);
        this.$element.find("button").toggleClass("disabled", state);
    }
}
