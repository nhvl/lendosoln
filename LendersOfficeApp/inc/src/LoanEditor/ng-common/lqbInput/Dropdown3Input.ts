import {LqbInputType} from "../ILqbDataModel";
import {BaseInput} from "./BaseInput";

export class Dropdown3Input extends BaseInput<any> {
    private selectedValue: any;

    protected init() {
        const {dm, $element} = this;
        console.assert(dm.t === LqbInputType.Dropdown);

        $element.html(
            `<div class="dropdown">
                <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="dropdown3input-label"></span>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right"></ul>
            </div>`);

        if (Array.isArray(dm.options)) {
            const os = dm.options.map(o => {
                const op = $(`<li><a>${o.l}</a></li>`);
                op.data("lqValue", o.v);
                return op;
            });
            const $ul = $element.find("ul.dropdown-menu").append(os);

            const _this = this;
            this.$element.find("ul.dropdown-menu").on("click", "li", function() {
                const $li = $(this);
                $ul.find("li").removeClass("active");
                $li.addClass("active");

                _this.selectedValue = $li.data("lqValue");
                $element.find(".dropdown3input-label").html($li.text());
                _this.onDomChangeUpdateModel(_this.selectedValue);
            });
        }
    }

    protected getValueFromDom() { return this.selectedValue; }
    public setValueToDom(value: any) {
        this.selectedValue = value;
        const {dm, $element} = this;
        // const o = dm.options.find(o => o.v === value);
        // const text = o == null ? "" : o.l;

        const $ul = $element.find("ul.dropdown-menu");
        $ul.find("li").removeClass("active");
        $element.find(".dropdown3input-label").html(
            $ul.find("li").filter((i: number, li: HTMLLIElement) => $(li).data("lqValue") === value).first().addClass("active").text()
        );
    }
    public setReadonly(state: boolean) {
        const {$element} = this;
        $element.find("a[data-toggle]").attr("data-toggle", state ? "dropdown-disabled" : "dropdown");
    }

    protected postInit() {
        //this.setupCssClass();
    }
    protected isSynced(): boolean {
        return this.dm.v === this.getValueFromDom();
    }
}
