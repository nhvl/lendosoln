import {HtmlInputInput} from "./HtmlInputInput";

export class RadioInput extends HtmlInputInput<any> {
    protected init() {
        this.$element.html('<input type="radio">');
        this.$input = this.$element.find("input");

        this.$input.data("lqValue", this.$eval("ngValue"));
    }
    protected postInit(){
        this.setupCssClass();

        this.$input.on("change", (event: JQueryEventObject) => {
            const radio = this.$input[0] as HTMLInputElement;
            if (!radio.checked || this.isSynced()) return;

            const value = this.getValueFromDom();
            //console.debug("from dom to model: set value [%s] = %s", attrs.lqbModel, JSON.stringify(value));

            this.onDomChangeUpdateModel(value);
        });
    }
    protected getValueFromDom():any {
        return this.$input.data("lqValue");
    }
    public setValueToDom(value:any) {
        //console.log(this.$input[0], this.$input.data("lqValue"), input.checked ? "✓" : "✗", value);
        (this.$input[0] as HTMLInputElement).checked = (this.$input.data("lqValue") === value);
    }
    public setReadonly(state:boolean){
        (this.$input[0] as HTMLInputElement).disabled = state;
    }
}
