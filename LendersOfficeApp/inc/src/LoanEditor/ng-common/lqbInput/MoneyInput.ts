import {HtmlInputInput} from "./HtmlInputInput";

export class MoneyInput extends HtmlInputInput<number> {
    protected init() {
        this.$element.html('<span><input type="text"></span>');
        this.$input = this.$element.find("input");
        this.$input.autoNumeric("init", {
            aSign: "$ ", nBracket: "(,)",
            mDec: Number(this.attrs.mDec) || undefined
        });
    }
    protected getValueFromDom(): number {
        const v = Number.parseFloat(this.$input.autoNumeric("get"));
        return (Number.isNaN(v) ? null : v);
    }
    public setValueToDom(value:number) {
        this.$input.autoNumeric("set", ((value == null) ? "" : value));
    }
}
