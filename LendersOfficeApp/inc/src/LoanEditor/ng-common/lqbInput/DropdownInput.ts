import {HtmlInputInput} from "./HtmlInputInput";

export class DropdownInput extends HtmlInputInput<string> {
    protected init() {
        const {dm, $element} = this;

        $element.html('<select></select>');
        this.$input = $element.find("select");

        if (Array.isArray(this.dm.options) && this.dm.options.length > 0) {
            const os = this.dm.options.map(o => {
                const op = document.createElement("option");
                op.value = o.v; // i.toString();
                op.text = o.l;
                return op;
            });
            this.$input.append(os);
        }
    }
    public setValueToDom(value:string) {
        const input = this.$input[0] as HTMLSelectElement;
        (value != null && value !== "") ? input.value = value : input.selectedIndex = -1;
    }
    public setReadonly(state:boolean){
        (this.$input[0] as HTMLInputElement).disabled = state;
    }
}
