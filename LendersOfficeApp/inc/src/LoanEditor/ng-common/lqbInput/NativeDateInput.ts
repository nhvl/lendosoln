import {ILqbDataModel} from "../ILqbDataModel";
import {HtmlInputInput} from "./HtmlInputInput";

export class NativeDateInput extends HtmlInputInput<Date> {
    private input: HTMLInputElement;
    protected init() {
        this.$element.html('<span><input type="date"></span>');
        this.input = this.$element.find("input")[0] as HTMLInputElement;
    }
    protected valueStandardlizer(dm: ILqbDataModel) {
        if ((dm.v == null) || (dm.v == "")) { dm.v = null; }
        else {
            const v = new Date(<any>dm.v);
            dm.v = (Number.isNaN(v.getTime())) ? null : v;
        }
    }
    protected getValueFromDom(): Date {
        const timestamp = Date.parse(this.$input.val())
        return (Number.isNaN(timestamp) ? null : new Date(timestamp));
    }
    public setValueToDom(value:Date) {
        const input = this.$input[0] as HTMLSelectElement;
        input.value   = toDateString(value as Date);
    }
    protected isSynced():boolean {
        let x = this.dm.v;
        let y = this.getValueFromDom();

        if (x === y || (isEmpty(x) && isEmpty(y))) return true;
        if (typeof x === "string") try { x = new Date(x as any) } catch (e) { }
        if (typeof y === "string") try { y = new Date(y as any) } catch (e) { }
        return x instanceof Date && y instanceof Date && x.getTime() === y.getTime();

        function isEmpty(x: any) { return x == null || x == "" }
    }

    public setReadonly(state:boolean){
        super.setReadonly(state);
    }
}

function toDateString(d: Date):string {
    return (d === null) ? "" : d.toISOString().slice(0, "YYYY-MM-DD".length);
}
