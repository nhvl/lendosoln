// import {BaseInput} from "./BaseInput";
// TODO: inherit BaseInput instead

import {HtmlInputInput} from "./HtmlInputInput";

export class LockInput extends HtmlInputInput<boolean> {
    protected init() {
        this.$element.html(`<label class="lock-checkbox"><span class="material-icons"></span><input type="checkbox"></label>`);
        this.$input = this.$element.find("input");
    }
    protected getValueFromDom(){ return (this.$input[0] as HTMLInputElement).checked; }
    public setValueToDom(value:boolean) {
        const input = this.$input[0] as HTMLInputElement;
        input.checked = value;
        this.$element.find("label.lock-checkbox").toggleClass("checked", value);
    }
    public setReadonly(state:boolean){
        (this.$element.find("label")).toggleClass("disabled",state);
        (this.$input[0] as HTMLInputElement).disabled = state;
    }
}
