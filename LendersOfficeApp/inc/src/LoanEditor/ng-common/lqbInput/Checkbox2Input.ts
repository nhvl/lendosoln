import {BaseInput} from "./BaseInput";

const template= `
    <label class="checkbox">
        <span class="material-icons">${require("../../../../../images/Prototype/checkbox.svg")}</span>
        <span></span>
    </label>
`;

export class Checkbox2Input extends BaseInput<boolean> {
    protected init() {
        this.$element.html(template);

        this.$element.on("click", "label:not(.disabled)", () => {
            const {dm} = this;
            if (!!dm.r) { debugger; return; }
            this.onDomChangeUpdateModel(!this.$element.find("label").hasClass("checked"));
        });

        const $span = this.$element.find("span:not(.material-icons)");
        if (!this.attrs.label) $span.remove()
        else $span.text(this.attrs.label);
    }

    protected onDomChangeUpdateModel(value: boolean) {
        this.eventEmitter.emit("change", this.getValue_boolean2Model(value));
    }
    protected getValueFromDom() {
        const isChecked = this.$element.find("label").hasClass("checked");
        return this.getValue_boolean2Model(isChecked);
    }
    public setValueToDom(value: boolean) {
        const v = this.getValue_model2Boolean(value);
        if (v == null) debugger;
        else this.$element.find("label").toggleClass("checked", v);
    }
    public setReadonly(state: boolean){
        this.$element.find("label").toggleClass("disabled", state);
    }
    protected isSynced(): boolean {
        return this.dm.v === this.getValueFromDom();
    }

    private trueValue: any;
    private falseValue: any;
    private getTrueValue () { return this.trueValue  === undefined ? (this.trueValue  = !this.attrs.ngTrueValue  ? true  : this.$eval("ngTrueValue" )) : this.trueValue ; }
    private getFalseValue() { return this.falseValue === undefined ? (this.falseValue = !this.attrs.ngFalseValue ? false : this.$eval("ngFalseValue")) : this.falseValue; }
    private getValue_boolean2Model(v: boolean): any {
        return v ? this.getTrueValue () : this.getFalseValue();
    }
    private getValue_model2Boolean(v: any): boolean {
        return (
            this.getTrueValue () === v ? true  :
            this.getFalseValue() === v ? false :
            null
        );
    }
}
