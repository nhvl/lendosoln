import {RadioInput} from "./RadioInput";

export class RadioAsCheckboxInput extends RadioInput {
    protected init() {
        this.$element.html('<input type="checkbox">');
        this.$input = this.$element.find("input");

        this.$input.data("lqValue", this.$eval("ngValue"));
    }
}
