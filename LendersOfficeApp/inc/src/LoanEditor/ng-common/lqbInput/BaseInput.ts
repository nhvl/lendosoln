import {copyToClipboard, ICopyFunc} from "../../../utils/copyToClipboard";
let copy: ICopyFunc;

import {InputSupportType, LqbInputType, ILqbDataModel, E_CalculationType} from "../ILqbDataModel";
import {DataModels} from "../DataModels";
import {toastrOptions} from "../toastrOptions";
import {EventEmitter} from "events";

import {ILiAttributes} from "./ILiAttributes";

export class BaseInput<T> {
    protected inputName: string;
    protected inputPath: string;
    protected eventEmitter: EventEmitter;

    constructor(
        private   scope   : ng.IScope,
        protected $element: ng.IAugmentedJQuery,
        protected attrs   : ILiAttributes,
        protected dm      : ILqbDataModel
    ) {
        this.eventEmitter = new EventEmitter;

        this.inputPath == attrs.lqbModel;
        const inputName = attrs.lqbModel.split(".").slice(-1)[0];

        this.valueStandardlizer(dm);
        this.init();
        this.postInit();
    }
    protected init(): void { throw "not implemented" };
    protected postInit() {}

    protected dispose(){
        this.$element.off();
        this.$element.find("*").off();
    }
    protected valueStandardlizer(dm: ILqbDataModel): void {}

    protected getValueFromDom()   : T       { throw "not implemented"; };
    public    setValueToDom  (v:T): void    { throw "not implemented"; };
    protected isSynced       ()   : boolean { throw "not implemented"; };
    protected onDomChangeUpdateModel(value:T) {
        this.eventEmitter.emit("change", value);
    }
    onChange(h: (value: T) => void) { this.eventEmitter.on("change", h) }

    public setReadonly(state:boolean): void { throw "not implemented"; };
    public setRequired(state:boolean): void { throw "not implemented"; };
    public setHidden  (state: boolean) { this.$element[0].hidden = state; }
    public setOptions (options: { l:string, v:any }[]) { }

    public updateRequireField(): void { throw "not implemented"; };

    protected $eval(a:string) {
        return this.scope.$eval(this.attrs[a]);
    }
}
