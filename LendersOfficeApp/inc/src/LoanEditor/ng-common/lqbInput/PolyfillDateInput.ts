import {BaseInput} from "./BaseInput";
import {NativeDateInput} from "./NativeDateInput";

// http://eternicode.github.io/bootstrap-datepicker/
const datePickerOptions: DatepickerOptions = {
    format: "mm/dd/yyyy",
    autoclose: true,
    enableOnReadonly: false,
    showOnFocus: false,
};

export class PolyfillDateInput extends NativeDateInput {
    protected $input: JQuery;
    protected init() {
        this.$element.html(`<div class="input-group date">
            <span><input type="text"></span>
            <span class="input-group-addon">
                <span class="material-icons"><img src="../images/Prototype/calendaricon.png" /></span>
            </span>
        </div>`);
        this.$input = this.$element.find("input")
        this.$input.datepicker(datePickerOptions).addClass("polyfill");
        this.$element.find("span.input-group-addon").on("click", () => { this.$input.datepicker("show") });
    }
    protected postInit(){
        const {dm} = this;

        this.setupCssClass();

        // TODO: refactory this: maybe use angular event???
        this.$input.on("changeDate clearDate", (event: DatepickerEventObject) => {
            const value : Date = event.date;
            if (!this.isSynced()) {
                if (isEmpty(value) && isEmpty(dm.v as Date)) return;
                //console.debug("from dom to model: set value [%s] = %s", <string> attrs.lqbModel, <any> value);
                this.onDomChangeUpdateModel(value);
            }
        });

        function isEmpty(x:Date|string){ return x == null || x === "" }
    }
    protected getValueFromDom() : Date {
        return this.$input.datepicker("getDate") as Date;
    }
    setValueToDom(d:Date) {
        this.$input.datepicker("setDate", d);
        this.updateRequireField();
    }

    public setReadonly(state:boolean){
        super.setReadonly(state);
        const iconSpanJquery = this.$element.find("span.input-group-addon");
        if (state){
            iconSpanJquery.addClass('disabled');
        } else {
            iconSpanJquery.removeClass('disabled');
        }
    }
}
