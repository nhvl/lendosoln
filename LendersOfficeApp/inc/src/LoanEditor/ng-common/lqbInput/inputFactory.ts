import {InputSupportType, LqbInputType, ILqbDataModel, E_CalculationType} from "../ILqbDataModel";
import {DataModels} from "../DataModels";
import {toastrOptions} from "../toastrOptions";
import {EventEmitter} from "events";

import {ILiAttributes} from "./ILiAttributes";

import {BaseInput           } from "./BaseInput";
import {TextInput           } from "./TextInput";
import {TextareaInput       } from "./TextareaInput";
import {MoneyInput          } from "./MoneyInput";
import {PercentInput        } from "./PercentInput";
import {SsnInput            } from "./SsnInput";
import {PhoneInput          } from "./PhoneInput";
import {NumberInput         } from "./NumberInput";
import {Checkbox2Input      } from "./Checkbox2Input";
import {PolyfillDateInput   } from "./PolyfillDateInput";
import {Dropdown2Input      } from "./Dropdown2Input";
import {LockInput           } from "./LockInput";
import {ZipcodeInput        } from "./ZipcodeInput";
import {Radio2Input         } from "./Radio2Input";
import {RadioAsCheckbox2Input} from "./RadioAsCheckbox2Input";

export function inputFactory(
    scope   : ng.IScope,
    $element: ng.IAugmentedJQuery,
    attrs   : ILiAttributes,
    dm      : ILqbDataModel
) : BaseInput<InputSupportType> {
    const CInput = factory(dm, attrs);
    return CInput ? new CInput(scope, $element, attrs, dm) : null;
}

function factory(dm: ILqbDataModel, attrs: ILiAttributes): any {
    switch (attrs.liType) {
        case "text"    : return TextInput;
        case "radio"   : return Radio2Input;
        case "radio2"  : return RadioAsCheckbox2Input;
        case "checkbox": return Checkbox2Input;
    }

    switch (dm.t) {
        case LqbInputType.Text              : return TextInput;
        case LqbInputType.TextArea          : return TextareaInput;
        case LqbInputType.Money             : return MoneyInput;
        case LqbInputType.Percent           : return PercentInput;
        case LqbInputType.Ssn               : return SsnInput;
        case LqbInputType.Phone             : return PhoneInput;
        case LqbInputType.Number            : return NumberInput;
        case LqbInputType.Checkbox          : return Checkbox2Input; // Checkbox2Input
        case LqbInputType.Date              : return PolyfillDateInput; //(!Modernizr.inputtypes.date) ? PolyfillDateInput : NativeDateInput;
        case LqbInputType.Dropdown          : return Dropdown2Input; // Dropdown3Input; // Dropdown2Input;
        case LqbInputType.Lock              : return LockInput;
        case LqbInputType.Zipcode           : return ZipcodeInput;
        case LqbInputType.Radio             : return Radio2Input; // RadioInput;
        case LqbInputType.StringWithDropDown: return TextInput; // Combox box
        default: return null;
    }
}
