export interface ILiAttributes extends ng.IAttributes {
    lqbModel: string;
    ngValue : string; // for Radio
    liType  : "text" | "radio" | "radio2" | "checkbox";
    readonly: string;

    mDec    : string;

    hideZero : string;

    ngTrueValue: string;
    ngFalseValue: string;

    label: string;
}
