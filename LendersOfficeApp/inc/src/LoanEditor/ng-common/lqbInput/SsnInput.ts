import {HtmlInputInput} from "./HtmlInputInput";

const maskDefaultOptions = { dataMask:false, watchInputs:false };

export class SsnInput extends HtmlInputInput<string> {
    protected init() {
        this.$element.html('<span><input type="text" maxlength="11"></span>');
        this.$input = this.$element.find("input");
        this.$input.mask('000-00-0000', maskDefaultOptions);
    }
    dispose() {
        this.$input.unmask();
        super.dispose();
    }
}
