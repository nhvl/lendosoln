import {HtmlInputInput} from "./HtmlInputInput";

export class CheckboxInput extends HtmlInputInput<boolean> {
    protected init() {
        this.$element.html('<input type="checkbox">');
        this.$input = this.$element.find("input");
    }
    protected getValueFromDom(){ return (this.$input[0] as HTMLInputElement).checked; }
    public setValueToDom(value:boolean) {
        const input = this.$input[0] as HTMLInputElement;
        input.checked = value;
    }
    public setReadonly(state:boolean){
        (this.$input[0] as HTMLInputElement).disabled = state;
    }
}
