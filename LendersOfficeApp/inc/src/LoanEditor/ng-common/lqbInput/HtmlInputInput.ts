import {LqbInputType        } from "../ILqbDataModel";
import {BaseInput           } from "./BaseInput";
import {isValidLqbInputModel} from "./../ILqbDataModel";
export class HtmlInputInput<T> extends BaseInput<T> {
    protected $input: JQuery; //HtmlInputInput;

    protected postInit(){
        this.setupCssClass();

        this.$input.on("change", (event: JQueryEventObject) => {
            if (this.isSynced()) return;

            const value = this.getValueFromDom();

            this.onDomChangeUpdateModel(value);
        });
    }
    protected setupCssClass() {
        const {dm, $input, $element} = this;

        {
            const maxLength = (dm.maxLength != null) ? dm.maxLength :
                !!$element.attr("maxlength") ? Number($element.attr("maxlength")) : null;
            if (!!maxLength) ($input[0] as HTMLInputElement).maxLength = maxLength;
        }

        if (dm.s != null) dm.s.forEach(s => { $input.addClass(s); });

        { const c = $element.attr("class");
            if (!!c) {
                // Copy from element to input
                $input.addClass(c);
                $element.removeClass();
            } else {
                const input = $input[0] as HTMLInputElement;
                if (!"radio checkbox".includes(input.type)) $input.addClass("form-control");

                switch (dm.t) {
                    case LqbInputType.Money   : $input.addClass("form-control-money"  ); break;
                    case LqbInputType.Percent : $input.addClass("form-control-percent"); break;
                    case LqbInputType.Zipcode : $input.addClass("form-control-zipcode"); break;
                    case LqbInputType.Ssn     : $input.addClass("form-control-ssn"    ); break;
                    case LqbInputType.Phone   : $input.addClass("form-control-phone"  ); break;
                    case LqbInputType.Date    : $input.addClass("form-control-date"   ); break;
                    case LqbInputType.Number  : $input.addClass("form-control-number" ); break;
                }
            }
        }

        ["tabIndex", "list", "placeholder"].forEach((a) => {
            const v = $element.attr(a);
            if (!!v) {
                $input.attr(a, v);
                $element.attr(a, null);
            }
        });
    }
    protected isSynced():boolean {
        return this.dm.v === this.getValueFromDom();
    }

    protected getValueFromDom(){ return this.$input.val(); }
    public setValueToDom(v:T) {
        this.$input.val(v as any);
    }

    public setReadonly(state:boolean){
        (this.$input[0] as HTMLInputElement).readOnly = state;
    }

    public setRequired(state: boolean) {
        if (state){
            this.$element.find("span:not([class])").addClass("has-required-field").append('<span class="form-control-required-field material-icons"></span>');
            this.updateRequireField();
        }
    }

    public updateRequireField(){
        const requireInputDiv = this.$element.find("span.has-required-field")
        requireInputDiv.find("input").attr( 'id', this.dm.v ? 'inputSuccess2' : 'inputError2');
        let fieldPath = requireInputDiv.parent().attr('lqb-model');

        let validated = isValidLqbInputModel(this.dm, fieldPath);

        if (validated){
            requireInputDiv.removeClass('has-required-field-error');
            requireInputDiv.addClass('has-required-field-success');
        } else {
            requireInputDiv.addClass('has-required-field-error');
            requireInputDiv.removeClass('has-required-field-success');
        }
    }
}
