import {HtmlInputInput} from "./HtmlInputInput";

const maskDefaultOptions = { dataMask:false, watchInputs:false };

export class PhoneInput extends HtmlInputInput<string> {
    protected init() {
        this.$element.html('<span><input type="tel"></span>');
        this.$input = this.$element.find("input");
        this.$input.mask('(000) 000-0000', maskDefaultOptions);
    }
    dispose() {
        this.$input.unmask();
        super.dispose();
    }
}
