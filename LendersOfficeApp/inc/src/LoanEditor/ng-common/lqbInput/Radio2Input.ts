import {BaseInput} from "./BaseInput";

const template = (
`<label class="radio-btn">
    <span class="material-icons">${require("../../../../../images/Prototype/radio-button.svg")}</span>
    <span></span>
</label>`);

export class Radio2Input extends BaseInput<any> {
    protected ngValue: any;
    protected init() {
        this.ngValue = this.$eval("ngValue");
        this.$element.html(template);

        this.$element.on("click", "label:not(.disabled)", () => {
            const {dm} = this;
            console.assert(!dm.r);

            if (this.isSynced()) return;

            const value = this.getValueFromDom();
            //console.debug("from dom to model: set value [%s] = %s", attrs.lqbModel, JSON.stringify(value));

            this.onDomChangeUpdateModel(value);
        });
        
        const $span = this.$element.find("span:not(.material-icons)");
        if (!this.attrs.label) $span.remove()
        else $span.text(this.attrs.label);
    }
    protected getValueFromDom() { return this.ngValue; }
    public setValueToDom(value: boolean) {
        this.$element.find("label").toggleClass("checked", value === this.ngValue);
    }
    public setReadonly(state: boolean) {
        this.$element.find("label").toggleClass("disabled", state);
    }
    protected isSynced(): boolean {
        return this.dm.v === this.getValueFromDom();
    }
}
