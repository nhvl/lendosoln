import {HtmlInputInput} from "./HtmlInputInput";

export class TextareaInput extends HtmlInputInput<string> {
    protected init() {
        const {dm, $element} = this;

        $element.html('<textarea>');
        $element.attr("typeof", "textarea")
        this.$input = this.$element.find("textarea");

        this.$input.attr("cols", $element.attr("cols")); $element.attr("cols", null);
        this.$input.attr("rows", $element.attr("rows")); $element.attr("rows", null);

        this.$input.on("focus", () => { this.$input.select(); });

        this.$input.each(function () {
            this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
        }).on('input', () => { this.resizeonChange(); });
    }

    public setValueToDom(v:string) {
        super.setValueToDom(v);
        this.resizeonChange();
    }

    private resizeonChange(){
        const input = this.$input[0] as HTMLTextAreaElement;
        input.style.height = 'auto';
        input.style.height = (input.scrollHeight) + 'px';
    }
}
