import {HtmlInputInput} from "./HtmlInputInput";

export class PercentInput extends HtmlInputInput<number> {
    protected init() {
        this.$element.html('<span><input type="text"></span>');
        this.$input = this.$element.find("input");
        this.$input.autoNumeric("init", { mDec: Number(this.attrs.mDec) || 3, aSign: " %", pSign: "s", });
    }
    protected getValueFromDom(): number {
        const v = Number.parseFloat(this.$input.autoNumeric("get"));
        return (Number.isNaN(v) ? null : v);
    }
    public setValueToDom(value:number) {
        this.$input.autoNumeric("set", ((value == null) ? "" : value));
    }
}
