import {StrGuid} from "../../utils/guid";
import { E_EDocStatus } from "../../DataLayer/Enums/EDocs/E_EDocStatus";

export interface IEDocument {
    PdfHasUnsupportedFeatures       : boolean;
    CreationSource                  : number;
    LastDeletedBy                   : string;
    LastDeletedOn                   : Date;
    // DocType                         : {
    //     DocTypeId                   : number;
    //     FolderAndDocTypeName        : string;
    // };
    Classification                  : number;
    FirstCloseDocTypeId             : number;
    FirstCloseSentD                 : Date;
    EDocOrigin                      : number;
    IsNew                           : boolean;
    RequiresDocTypeChange           : boolean;
    AppName                         : string;
    AuditEvents                     : IEdocAuditEvent[];
    PdfFileDBKeyCurrentRevision     : StrGuid;
    IsValid                         : boolean;
    IsAccepted                      : boolean;
    DocTypeName                     : string;
    FolderAndDocTypeName            : string;
    FolderId                        : number;
    Folder                          : IEDocFolder;
    DocumentTypeId                  : number;
    BrokerId                        : StrGuid;
    DocumentId                      : StrGuid;
    LoanId                          : StrGuid;
    AppId                           : StrGuid;
    FaxNumber                       : string;
    InternalDescription             : string;
    PublicDescription               : string;
    Comments                        : string;
    Version                         : number;
    LastModifiedDate                : Date;
    IsUploadedByPmlUser             : boolean;
    HideFromPMLUsers                : boolean;
    DocStatusInitial                : number;
    DocStatus                       : E_EDocStatus;
    StatusDescription               : string;
    CreatedDate                     : Date;
    PageCount                       : number;
    InternalAnnotations             : {
        NotesHaveChanged            : boolean;
        Version                     : number;
        AnnotationList              : IEdocAnnotation[];
        AnnotationCount             : number;
    },
    HasInternalAnnotations          : boolean;
    ImageStatus                     : number;
    TimeToRequeueForImageGeneration : any;
    TimeImageCreationEnqueued       : Date;
    CopiedFromFileName              : any;
    FileDbKey_CurrentRevision       : StrGuid;
    CanEdit                         : boolean;
    CanDelete                       : boolean;

    pages                           : IEdocPage[];
    annotations                     : IEdocAnnotation[];
    nonPdfData                      : {
        conditionAssociations       : IEdocConditionAssociation[];
    };
}

export interface IFolderRole {
    RoleDesc           : string;
    RoleDisplayDesc    : string;
    RoleId             : StrGuid;
    FolderId           : number;
    IsIncludedInFolder : boolean;
    IsAdmin            : boolean;
    IsPmlUser          : boolean;
}

export interface IEDocFolder {
    IsSystemFolder         : boolean;
    RoleListDescString     : string;
    IsUnclassifiedFolder   : boolean;
    FolderRolesList        : IFolderRole[];
    AssociatedRoleIds      : {
        RoleId             : StrGuid;
        IsPmlUser          : boolean;
    }[];
    FolderId               : number;
    FolderNm               : string;
}

export interface IEdocPage {
    Page             : number;
    Rotation         : number;
    Scale            : number;
    PageWidth        : number;
    PageHeight       : number;
    PngKey           : string;

    pngUrl           : string;
    thumbUrl         : string;
    isSelected       : boolean;
    annotations      : IEdocAnnotation[];
}

export interface IEdocAnnotation {
    Id             : StrGuid;
    ApplyToPDF     : boolean;
    pg             : number;
    cssClass       : "pdf-field-highlight" | "pdf-field-note";
    LastModifiedOn : string; // "7/10/2016"
    width          : number;
    height         : number;
    Text           : string;
    NoteStateType  : "minimized" | "maximized";
    lx             : number;
    ly             : number;
    ux             : number;
    uy             : number;
}

export interface IEdocAuditEvent {
    UserId        : StrGuid;
    ModifiedDate  : Date;
    Version       : number;
    Description   : string;
    Name          : string;
    PreviousValue : any;
    NewValue      : any;
    HasDetails    : boolean;
}
export interface IEdocConditionAssociation {
    IsAssociated  : boolean;
    TaskId        : string;
    RowId         : number;
    Order         : number;
    IsProtected   : boolean;
    ConditionText : string;
    Category      : string;
}

