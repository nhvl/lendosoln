import {E_AgentRoleT} from "../../DataLayer/Enums/E_AgentRoleT";
import {StrGuid} from "../../utils/guid";
import {ILqbDataModelG} from "./ILqbDataModel";
type DataTemplateList<T> = { data: T[], template: T };

export interface ISettlementServiceProvider {
    ServiceT                     : ILqbDataModelG<E_AgentRoleT>;
    // ServiceTDesc                 : ILqbDataModelG<string>;
    CompanyName                  : ILqbDataModelG<string>;
    StreetAddress                : ILqbDataModelG<string>;
    City                         : ILqbDataModelG<string>;
    State                        : ILqbDataModelG<string>;
    Zip                          : ILqbDataModelG<string>;
    ContactName                  : ILqbDataModelG<string>;
    Phone                        : ILqbDataModelG<string>;
    AgentId                      : ILqbDataModelG<StrGuid>;
    EstimatedCostAmountPopulated : ILqbDataModelG<boolean>;
    EstimatedCostAmount          : ILqbDataModelG<number>;
    Email                        : ILqbDataModelG<string>;
    Fax                          : ILqbDataModelG<string>;
}

export interface IAvailableSettlementServiceProviderGroup {
    FeeTotalAmount            : number;
    FeeTypeId                 : StrGuid;
    FeeDescription            : string;
    IsTitleFee                : boolean;
    SettlementServiceProviders: DataTemplateList<ISettlementServiceProvider>;
}[]
