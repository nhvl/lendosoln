type StrGuid = string;
import {E_RateLockAuditActionT} from "../../DataLayer/Enums/E_RateLockAuditActionT";

export interface IDataContents {
    sRateLockHistoryXmlContent: {
        RateLockHistory: {
            Event: {
                EvenType                                  : string; // E_RateLockAuditActionT
                EventId                                  ?: StrGuid;
                EventDate                                 : string; // Date    5/20/2016 2:15:30 AM PDT
                DoneBy                                    : string; //         LOTEST 001
                sNoteIR                                  ?: string; // Percent 12.000%
                sBrokComp1Pc                             ?: string; // Percent -95.000%
                sRAdjMarginR                             ?: string; // Percent 0.000%
                sTerm                                    ?: string; // number  240
                sDue                                     ?: string; // number  360
                sRLckdExpiredD                           ?: string; // Date    5/25/2016
                sPpmtPenaltyMon                          ?: string; // number  0
                sRLckBreakReasonDesc                      : string;
                sRLckdD                                  ?: string; // Date    5/20/2016
                sOptionArmTeaserR                        ?: string; // Percent 0.000%
                sIsOptionArm                             ?: string; //         No
                sLpTemplateNm                            ?: string; // number  234
                sRLckdDays                               ?: string; // number  5
                sAmortMethodT                            ?: string; //         Fixed
                sBrokerLockBaseNoteIR                    ?: string; // Percent 11.000%
                sBrokerLockBaseBrokComp1PcPrice          ?: string; // Percent 100.000%
                sBrokerLockBaseBrokComp1PcFee            ?: string; // Percent 0.000%
                sBrokerLockBaseRAdjMarginR               ?: string; // Percent 0.000%
                sBrokerLockBaseOptionArmTeaserR          ?: string; // Percent 0.000%
                sBrokerLockBrokerBaseNoteIR              ?: string; // Percent 11.000%
                sBrokerLockBrokerBaseBrokComp1PcPrice    ?: string; // Percent 100.000%
                sBrokerLockBrokerBaseBrokComp1PcFee      ?: string; // Percent 0.000%
                sBrokerLockBrokerBaseRAdjMarginR         ?: string; // Percent 0.000%
                sBrokerLockBrokerBaseOptionArmTeaserR    ?: string; // Percent 0.000%
                sBrokerLockTotHiddenAdjNoteIR            ?: string; // Percent 0.000%
                sBrokerLockTotHiddenAdjBrokComp1PcPrice  ?: string; // Percent 0.000%
                sBrokerLockTotHiddenAdjBrokComp1PcFee    ?: string; // Percent 0.000%
                sBrokerLockTotHiddenAdjRAdjMarginR       ?: string; // Percent 0.000%
                sBrokerLockTotHiddenAdjOptionArmTeaserR  ?: string; // Percent 0.000%
                sBrokerLockTotVisibleAdjNoteIR           ?: string; // Percent 1.000%
                sBrokerLockTotVisibleAdjBrokComp1PcPrice ?: string; // Percent 95.000%
                sBrokerLockTotVisibleAdjBrokComp1PcFee   ?: string; // Percent -95.000%
                sBrokerLockTotVisibleAdjRAdjMarginR      ?: string; // Percent 0.000%
                sBrokerLockTotVisibleAdjOptionArmTeaserR ?: string; // Percent 0.000%
                sBrokerLockFinalBrokComp1PcPrice         ?: string; // Percent 195.000%
                sBrokerLockRateSheetEffectiveD           ?: string; // ??
                Adjustments                              ?: {
                    Adjustment: {
                        Description                       : string; //         aaaaa
                        Rate                              : string; // Percent 1.000%
                        Price                             : string; // Percent -95.000%
                        Fee                               : string; // Percent -95.000%
                        Margin                            : string; // Percent 0.000%
                        TRate                             : string; // Percent 0.000%
                        IsHidden                          : string; // boolean No
                    }
                }
            }[];
        };
    };
}
