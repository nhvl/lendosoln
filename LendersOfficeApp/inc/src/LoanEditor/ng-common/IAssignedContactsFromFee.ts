import {ILqbDataModelG} from "./ILqbDataModel";
import {StrGuid} from "../../utils/guid";

import {E_PercentBaseT                    } from "../../DataLayer/Enums/E_PercentBaseT";
import {E_GfeProviderChoiceT              } from "../../DataLayer/Enums/E_GfeProviderChoiceT";
import {E_LegacyGfeFieldT                 } from "../../DataLayer/Enums/E_LegacyGfeFieldT";
import {E_sClosingCostSetType             } from "../../DataLayer/Enums/E_sClosingCostSetType";
import {E_AgentSourceT                    } from "../../DataLayer/Enums/E_AgentSourceT";
import {E_GfeResponsiblePartyT            } from "../../DataLayer/Enums/E_GfeResponsiblePartyT";
import {E_ClosingCostFeePaymentPaidByT    } from "../../DataLayer/Enums/E_ClosingCostFeePaymentPaidByT";
import {E_GfeClosingCostFeePaymentTimingT } from "../../DataLayer/Enums/E_GfeClosingCostFeePaymentTimingT";
import {E_ClosingCostFeeMismoFeeT         } from "../../DataLayer/Enums/E_ClosingCostFeeMismoFeeT";
import {E_IntegratedDisclosureSectionT    } from "../../DataLayer/Enums/E_IntegratedDisclosureSectionT";
import {E_GfeSectionTGroups               } from "../../DataLayer/Enums/E_GfeSectionTGroups";
import {E_GfeSectionT                     } from "../../DataLayer/Enums/E_GfeSectionT";
import {E_ClosingCostFeeFormulaT          } from "../../DataLayer/Enums/E_ClosingCostFeeFormulaT";
import {E_AgentRoleT                      } from "../../DataLayer/Enums/E_AgentRoleT";

export interface ICAgentFields {
    IsApplyToFormsUponSave                 : ILqbDataModelG<boolean>;
    AgentSourceT                           : ILqbDataModelG<E_AgentSourceT>;
    BrokerLevelAgentID                     : ILqbDataModelG<StrGuid>;
    AgentRoleT                             : ILqbDataModelG<E_AgentRoleT>;
    AgentRoleTDesc                         : ILqbDataModelG<string>;
    AgentRoleDescription                   : ILqbDataModelG<string>;
    KeyType                                : {},
    AgentName                              : ILqbDataModelG<string>;
    TaxId                                  : ILqbDataModelG<string>;
    ChumsId                                : ILqbDataModelG<string>;
    EmployeeId                             : ILqbDataModelG<StrGuid>;
    LicenseNumOfAgent                      : ILqbDataModelG<string>;
    LicenseNumOfAgentIsExpired             : ILqbDataModelG<boolean>;
    LicenseNumOfCompany                    : ILqbDataModelG<string>;
    LicenseNumOfCompanyIsExpired           : ILqbDataModelG<boolean>;
    DepartmentName                         : ILqbDataModelG<string>;
    CaseNum                                : ILqbDataModelG<string>;
    CompanyName                            : ILqbDataModelG<string>;
    StreetAddr                             : ILqbDataModelG<string>;
    City                                   : ILqbDataModelG<string>;
    State                                  : ILqbDataModelG<string>;
    Zip                                    : ILqbDataModelG<string>;
    StreetAddr_SingleLine                  : ILqbDataModelG<string>;
    StreetAddr_MultiLine                   : ILqbDataModelG<string>;
    OtherAgentRoleTDesc                    : ILqbDataModelG<string>;
    Phone                                  : ILqbDataModelG<string>;
    CellPhone                              : ILqbDataModelG<string>;
    PagerNum                               : ILqbDataModelG<string>;
    FaxNum                                 : ILqbDataModelG<string>;
    EmployeeIDInCompany                    : ILqbDataModelG<string>;
    EmailAddr                              : ILqbDataModelG<string>;
    Notes                                  : ILqbDataModelG<string>;
    CityStateZip                           : ILqbDataModelG<string>;
    InvestorSoldDate                       : ILqbDataModelG<Date>;
    InvestorSoldDate_rep                   : ILqbDataModelG<string>;
    InvestorBasisPoints                    : ILqbDataModelG<number>;
    InvestorBasisPoints_rep                : ILqbDataModelG<string>;
    IsListedInGFEProviderForm              : ILqbDataModelG<boolean>;
    IsLenderAssociation                    : ILqbDataModelG<boolean>;
    IsLenderAffiliate                      : ILqbDataModelG<boolean>;
    IsLenderRelative                       : ILqbDataModelG<boolean>;
    HasLenderRelationship                  : ILqbDataModelG<boolean>;
    HasLenderAccountLast12Months           : ILqbDataModelG<boolean>;
    IsUsedRepeatlyByLenderLast12Months     : ILqbDataModelG<boolean>;
    ProviderItemNumber                     : ILqbDataModelG<string>;
    PhoneOfCompany                         : ILqbDataModelG<string>;
    FaxOfCompany                           : ILqbDataModelG<string>;
    IsNotifyWhenLoanStatusChange           : ILqbDataModelG<boolean>;
    LoanOriginatorIdentifier               : ILqbDataModelG<string>;
    CompanyLoanOriginatorIdentifier        : ILqbDataModelG<string>;
    BranchName                             : ILqbDataModelG<string>;
    PayToBankName                          : ILqbDataModelG<string>;
    PayToBankCityState                     : ILqbDataModelG<string>;
    PayToABANumber                         : ILqbDataModelG<string>;
    PayToAccountNumber                     : ILqbDataModelG<string>;
    PayToAccountName                       : ILqbDataModelG<string>;
    FurtherCreditToAccountNumber           : ILqbDataModelG<string>;
    FurtherCreditToAccountName             : ILqbDataModelG<string>;
    IsLender                               : ILqbDataModelG<boolean>;
    IsOriginator                           : ILqbDataModelG<boolean>;
    IsOriginatorAffiliate                  : ILqbDataModelG<boolean>;
    OverrideLicenses                       : ILqbDataModelG<boolean>;
    LicenseInfoList                        : {
        List                               : any[];
        Count                              : ILqbDataModelG<number>;
        MaxLicenses                        : ILqbDataModelG<number>;
        IsFull                             : ILqbDataModelG<boolean>;
    };
    CompanyLicenseInfoList                 : {
        List                               : {
            Street                         : ILqbDataModelG<string>;
            Zip                            : ILqbDataModelG<string>;
            City                           : ILqbDataModelG<string>;
            AddrState                      : ILqbDataModelG<string>;
            Phone                          : ILqbDataModelG<string>;
            Fax                            : ILqbDataModelG<string>;
            DisplayName                    : ILqbDataModelG<string>;
            State                          : ILqbDataModelG<string>;
            ExpD                           : ILqbDataModelG<Date>;
            License                        : ILqbDataModelG<string>;
            Editable                       : ILqbDataModelG<boolean>;
            ExpirationDate                 : ILqbDataModelG<Date>;
        }[];
        Count                              : ILqbDataModelG<number>;
        MaxLicenses                        : ILqbDataModelG<number>;
        IsFull                             : ILqbDataModelG<boolean>;
    },
    IsNewRecord                            : ILqbDataModelG<boolean>;
    IsValid                                : ILqbDataModelG<boolean>;
    RecordId                               : ILqbDataModelG<StrGuid>;
}

export interface IBorrowerClosingCostFee {
    OrigCompSkipTRIDCheck                  : ILqbDataModelG<boolean>;
    Beneficiary                            : ILqbDataModelG<E_AgentRoleT>;
    IsTitleFee                             : ILqbDataModelG<boolean>;
    IsThirdParty                           : ILqbDataModelG<boolean>;
    CanShop                                : ILqbDataModelG<boolean>;
    HudLine                                : ILqbDataModelG<number>;
    IsShowQmWarning                        : ILqbDataModelG<boolean>;
    GfeProviderChoiceT                     : ILqbDataModelG<E_GfeProviderChoiceT>;
    ProviderChosenByLender                 : ILqbDataModelG<E_AgentRoleT>;
    IsConstantBaseAmountFromFieldId        : ILqbDataModelG<number>;
    IsIncludedInQm                         : ILqbDataModelG<number>;
    QmAmount                               : ILqbDataModelG<number>;
    QmAmount_rep                           : ILqbDataModelG<string>;
    FinancedQmAmount                       : ILqbDataModelG<number>;
    LegacyGfeFieldT                        : ILqbDataModelG<E_LegacyGfeFieldT>;
    FinancedQmAmount_rep                   : ILqbDataModelG<string>;
    ParentClosingCostSet                   : {
        SetType                            : ILqbDataModelG<E_sClosingCostSetType>;
        IsMigration                        : ILqbDataModelG<boolean>;
        LosConvert                         : {
            FormatTargetCurrent            : ILqbDataModelG<number>; // enum FormatTarget
            InvalidRateString              : ILqbDataModelG<string>;
            InvalidMoneyString             : ILqbDataModelG<string>;
            InvalidDecimalString           : ILqbDataModelG<string>;
            InvalidCountString             : ILqbDataModelG<string>;
            InvalidDateString              : ILqbDataModelG<string>;
        },
        HasDataLoanAssociate               : ILqbDataModelG<boolean>;
        HasClosingCostArchiveAssociate     : ILqbDataModelG<boolean>;
    };
    IsAffiliate                            : ILqbDataModelG<boolean>;
    BeneficiaryType                        : ILqbDataModelG<string>;
    BeneficiaryAgent                       : ICAgentFields;
    IsPrepaidFromExpensesFee               : ILqbDataModelG<boolean>;
    OriginalDescription                    : ILqbDataModelG<string>;
    DidShop                                : ILqbDataModelG<boolean>;
    DisableBeneficiaryAutomation           : ILqbDataModelG<boolean>;
    BeneficiaryDescription                 : ILqbDataModelG<string>;
    BeneficiaryAgentId                     : ILqbDataModelG<StrGuid>;
    GfeResponsiblePartyT                   : ILqbDataModelG<E_GfeResponsiblePartyT>;
    IsBonaFide                             : ILqbDataModelG<boolean>;
    TotalAmount                            : ILqbDataModelG<number>;
    BorrowerAmount                         : ILqbDataModelG<number>;
    TotalAmount_rep                        : ILqbDataModelG<string>;
    Percent                                : ILqbDataModelG<number>;
    Percent_rep                            : ILqbDataModelG<string>;
    PercentBaseT                           : ILqbDataModelG<E_PercentBaseT>;
    BaseAmount                             : ILqbDataModelG<number>;
    BaseAmount_rep                         : ILqbDataModelG<string>;
    NumberOfPeriods                        : ILqbDataModelG<number>;
    NumberOfPeriods_rep                    : ILqbDataModelG<string>;
    PercentTotalAmount                     : ILqbDataModelG<number>;
    Payments                               : {
        IsMIPFinancedPayment               : ILqbDataModelG<boolean>;
        IsCashPdPayment                    : ILqbDataModelG<boolean>;
        PaidByT                            : ILqbDataModelG<E_ClosingCostFeePaymentPaidByT>;
        ResponsiblePartyT                  : ILqbDataModelG<E_GfeResponsiblePartyT>;
        IsSystemGenerated                  : ILqbDataModelG<boolean>;
        Entity                             : ILqbDataModelG<E_AgentRoleT>;
        IsFinanced                         : ILqbDataModelG<boolean>;
        IsMade                             : ILqbDataModelG<boolean>;
        Id                                 : ILqbDataModelG<StrGuid>;
        Amount                             : ILqbDataModelG<number>;
        Amount_rep                         : ILqbDataModelG<string>;
        IsIncludedInApr                    : ILqbDataModelG<boolean>;
        IsAPRRaw                           : ILqbDataModelG<boolean>;
        GfeClosingCostFeePaymentTimingT    : ILqbDataModelG<E_GfeClosingCostFeePaymentTimingT>;
        PaymentDate                        : ILqbDataModelG<Date>;
        PaymentDate_rep                    : ILqbDataModelG<string>;
    }[];
    IsFeeEmpty                             : ILqbDataModelG<boolean>;
    MismoFeeT                              : ILqbDataModelG<E_ClosingCostFeeMismoFeeT>;
    HudLine_rep                            : ILqbDataModelG<string>;
    IsOptional                             : ILqbDataModelG<boolean>;
    IntegratedDisclosureSectionT           : ILqbDataModelG<E_IntegratedDisclosureSectionT>;
    UniqueId                               : ILqbDataModelG<StrGuid>;
    ClosingCostFeeTypeId                   : ILqbDataModelG<StrGuid>;
    Description                            : ILqbDataModelG<string>;
    IsApr                                  : ILqbDataModelG<boolean>;
    IsFhaAllowable                         : ILqbDataModelG<boolean>;
    IsVaAllowable                          : ILqbDataModelG<boolean>;
    GfeSectionGroup                        : ILqbDataModelG<E_GfeSectionTGroups>;
    GfeSectionT                            : ILqbDataModelG<E_GfeSectionT>;
    Dflp                                   : ILqbDataModelG<boolean>;
    IsSystemLegacyFee                      : ILqbDataModelG<boolean>;
    CanChangeSection                       : ILqbDataModelG<boolean>;
    IsEditableSystemFee                    : ILqbDataModelG<boolean>;
    CanManuallyAddToEditor                 : ILqbDataModelG<boolean>;
    CanModifySystemFeeMismoType            : ILqbDataModelG<boolean>;
    FormulaT                               : ILqbDataModelG<E_ClosingCostFeeFormulaT>;
}

export interface IAssignedContactsFromFee {
    Fees: IBorrowerClosingCostFee[];
    Contact: ICAgentFields;
}
