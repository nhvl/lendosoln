export type InputSupportType = (string|number|boolean|Date);

export enum LqbInputType { // LendersOfficeLib/ObjLib/UI/InputFieldType.cs
    Unknown            =  0,
    Text               =  1,
    TextArea           =  2,
    Money              =  3,
    Percent            =  4,
    Ssn                =  5,
    Phone              =  6,
    Number             =  7,
    Checkbox           =  8,
    Date               =  9,
    Dropdown           = 10,
    Lock               = 11,
    Zipcode            = 12,
    Radio              = 13,
    StringWithDropDown = 14,
}

export enum E_CalculationType {
    // LendersOfficeLib/ObjLib/UI/InputFieldModel.cs
    CalculateNormal = 0,
    NoCalculate     = 1,
    CalculateZip    = 2,
}

export interface ILqbDataModelG<T> {
    t         : LqbInputType;
    v         : T;
    __old_v  ?: T;

    required ?: boolean;
    r         : boolean;
    h         : boolean;

    maxLength?: number;

    options  ?: { l:string, v:any }[], // for DropDown only, `l` is label, `v` is value
    s        ?: string[]; // CSS classes

    calcAction: E_CalculationType;
}
export interface ILqbDataModel extends ILqbDataModelG<InputSupportType> {}

export interface ILqbDataModelRaw {
    t: LqbInputType;
    v: InputSupportType;

    required?: boolean;
    r: boolean;
    h: boolean;

    o?: { Key: string, Value: any }[], // for DropDown only, `l` is label, `v` is value
    s?: string[],
}

export function isLqbDataModel(o: any): o is ILqbDataModel {
    return (o != null && typeof o.t === "number");
}

export function updateFromRaw(newX:any, oldX:any, fieldName ?:string):any {
    if (newX == null) { debugger; return; }

    if (isLqbDataModel(newX)) { updateFieldFromRaw(newX, oldX, fieldName); return; }
    if (Array.isArray(newX)) { updateArrayFromRaw(newX, oldX); return; }
    if (angular.isObject(newX))  { updateDictFromRaw(newX, oldX); return; }
}

function updateArrayFromRaw(xs:any[], o_xs:any[]) {
    xs.forEach((x, i) => {
        if (o_xs[i] == null) o_xs[i] = anyFromRaw(x, undefined);
        else updateFromRaw(x, o_xs[i]);
    });
    o_xs.splice(xs.length);
}

function updateDictFromRaw(dict:{[key:string]:any}, oDict:{[key:string]:any}={}) {
    Object.keys(dict).forEach((key) => {
        if (oDict[key] == null) oDict[key] = anyFromRaw(dict[key], {}, key);
        else updateFromRaw(dict[key], oDict[key], key);
    });
}

function updateFieldFromRaw(field: ILqbDataModelRaw, oField ?:ILqbDataModel, fieldName ?:string): ILqbDataModel {
    const tf = Object.assign(oField, field);

    tf.v = field.v;
    switch (field.t) {
        case LqbInputType.Checkbox:
        case LqbInputType.Lock: {
            if      (["yes", "true" ].includes(String(field.v).toLowerCase())) { tf.v =  true; }
            else if (["no" , "false"].includes(String(field.v).toLowerCase())) { tf.v = false; }
            else { debugger; }
        }
    }

    console.assert(
        (field.t !== LqbInputType.Dropdown) ||
        (!!tf && Array.isArray(tf.options)) ||
        Array.isArray(field.o), `Field [${fieldName}] has no options.`);
    if (Array.isArray(field.o)) {
        tf.options = optionsFromRaw(field.o);
    }

    tf.r = !!field.r;
    tf.h = !!field.h;

    return tf;
}

// ------------------------------------------
function anyFromRaw(newX:any, oldX:any, fieldName ?:string):any {
    if (newX == null) return oldX;

    if (isLqbDataModel(newX)) return fieldFromRaw(newX, oldX, fieldName);
    if (Array.isArray(newX)) return arrayFromRaw(newX, oldX);
    if (angular.isObject(newX)) return dictFromRaw(newX, oldX);
    return newX;
}

function arrayFromRaw(xs:any[], o_xs:any[]=[]): any[] {
    return xs.map((x, i) => anyFromRaw(x, o_xs[i]));
}

function dictFromRaw(dict:{[key:string]:any}, oDict:{[key:string]:any}={}): {[key:string]:any} {
    return Object.keys(dict).reduce((d, key) => {
        d[key] = anyFromRaw(dict[key], oDict[key], key);
        return d;
    }, {} as {[key:string]:any});
}

function fieldFromRaw(field: ILqbDataModelRaw, oField ?:ILqbDataModel, fieldName ?:string): ILqbDataModel {
    const tf = Object.assign({}, oField, field);

    tf.v = field.v;
    switch (field.t) {
        case LqbInputType.Checkbox:
        case LqbInputType.Lock: {
            if      (["yes", "true" ].includes((field.v as string).toLowerCase())) { tf.v =  true; }
            else if (["no" , "false"].includes((field.v as string).toLowerCase())) { tf.v = false; }
            else { debugger; }
        }
    }

    console.assert(
        (field.t !== LqbInputType.Dropdown) ||
        (!!tf && Array.isArray(tf.options)) ||
        Array.isArray(field.o), `Field [${fieldName}] has no options.`);
    if (Array.isArray(field.o)) {
        tf.options = optionsFromRaw(field.o);
    }

    tf.r = !!field.r;
    tf.h = !!field.h;

    return tf;
}

function optionsFromRaw(xs: { Key: string, Value: any }[]): { l:string, v:any }[] {
    return xs.map(x => {
        const nv = Number(x.Key);
        const v = Number.isNaN(nv) ? x.Key : nv;
        const l = x.Value == null ? x.Key : x.Value;

        return { l, v };
    })
}


export function isValidLqbInputModel(model: ILqbDataModel, path: string): boolean{
    const {v, required, r, h} = model;

    if (!!path){
        if (path.indexOf('CompanyLicenseTemplate') >= 0) return true;

        if ( path.indexOf('broker.FhaLenderId') >= 0)
        {
            return  v == null || ((/^\d{10}$/.test(v as string) || (v as string).length == 0));
        }
        if(path.indexOf('AgentRecord.EmailAddr') >= 0){
            return v == null ||  ((/^[A-Za-z0-9&._%+-]+@[A-Za-z0-9&.-]+\.[A-Za-z]{2,}$/.test(v as string) || (v as string).length == 0));
        }
    }

    return r || h || !required
                ||  !(  v == null || (typeof v === "string" && v.length < 1));
}
