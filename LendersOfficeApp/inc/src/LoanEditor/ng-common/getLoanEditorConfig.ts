
import {getQueryStringValue} from "../../utils/getQueryStringValue";

const $ = jQuery;

const qsk_sLId = "loanid";
const qsk_aAppId = "appId";

export function getLoanId(){ return getQueryStringValue(qsk_sLId); }
export function getApplicantId(){ return getQueryStringValue(qsk_aAppId) || ""; }

export function getLoanEditorConfig(loanEditorHtml: string) {

    const sLId = getLoanId();
    if (!sLId) {
        console.error(`query string '${qsk_sLId}' is not found. Example: `,
            `${window.location.pathname}?${qsk_sLId}=6903f95c-9dff-48e5-ae25-a3b70184e0be`,
            `${window.location.pathname}?${qsk_sLId}=38814b00-2cd2-46ce-9d6d-1532b3fce53f`
            );
    }

    const appId = getApplicantId();

    const $loanEditorTemplate = $(`<div>${loanEditorHtml}</div>`);
    const fieldList:string[] = Array.from(new Set<string>([].concat(
        $loanEditorTemplate.find("lqb-input[lqb-model]").map((i, m) => lqbModelBindExpression_2_fieldId(m)).toArray(),
        $loanEditorTemplate.find("lqb-lockable-input[lqb-model]").map((i, m) => lqbModelBindExpression_2_fieldId(m)).toArray(),
        $loanEditorTemplate.find("lqb-lockable-input[lockable-model]").map((i, m) => lqbModelBindExpression_2_fieldId(m, "lockable-model")).toArray(),
        $loanEditorTemplate.find("[ng-repeat]").map((i, m) => ngRepeateExpression_2_fieldId(m)).toArray(),
        ...$loanEditorTemplate.find("div[lqb-load-fields]").toArray().map((m) => $(m).attr("lqb-load-fields").split(" ").map((s:string) => s.trim()))
        ).filter(f => !!f)
    ));

    // console.debug("parsed fieldList: ");
    // console.table(fieldList.map(x => [x]));

    return {
        sLId,
        appId,
        fieldList,
    };
}

function lqbModelBindExpression_2_fieldId(m: Element, attrName: string = "lqb-model") {
    const expr = $(m).attr(attrName).trim();

    if (expr.match(/vm\.loan\.app\.aAssetCollection\.\w+\.\w+/)) return "aAssetCollection"; // vm.loan.app.aAssetCollection.*

    if (expr.match(/vm\.loan\.app\.aLiaCollection\.\w+\.\w+/)) return "aLiaCollection"; // vm.loan.app.aLiaCollection.*

    if (expr.match(/vm\.loan\.app\.aReCollection\.\w+\.\w+/)) return "aReCollection"; // vm.loan.app.aReCollection.*

    let match = expr.match(/vm\.loan\.(sAgentOfRole\.\w+)/); // vm.loan.sAgentOfRole.Appraiser
    if (match != null) return match[1];

    match = expr.match(/vm\.loan\.(preparerFields\.\w+)/);
    if (match != null) return match[1];

    match = expr.match(/vm\.loan\.AggregateEscrowAccount\b/);
    if (match != null) return "AggregateEscrowAccount";

    match = expr.match(/vm\.loan\.(AgentRecord\.\w+)/); // vm.loan.AgentRecord.*
    if (match != null) return match[1];

    match =  expr.match(/vm\.loan\.fields\.(\w+)/)  // vm.loan.field.*
          || expr.match(/vm\.loan\.app\.fields\.(\w+)/)  // vm.loan.app.fields.*
        ;
    return match == null ? null : match[1];
}

function ngRepeateExpression_2_fieldId(m: Element) {
    const expr = $(m).attr("ng-repeat").trim();

    let match = expr.match(/vm\.loan\.app\.aOtherIncomeList/); // vm.loan.app.aOtherIncomeList
    if (match != null) return "aOtherIncomeList";

    match = expr.match(/vm\.loan\.sDrawSchedules/); // vm.loan.app.sDrawSchedules
    if (match != null) return "sDrawSchedules";
    
    match = expr.match(/vm\.loan\.app\.aBAliases/); // vm.loan.sSellerCollection
    if (match != null) return "aBAliases";
    
    match = expr.match(/vm\.loan\.(\w+)/); // vm.loan.sSellerCollection
    if (match != null && ["sSellerCollection", "sTitleBorrowers", "sHomeownerCounselingOrganizationCollection", "FeeChanges"].includes(match[1])) return match[1];
    

    return null;
}
