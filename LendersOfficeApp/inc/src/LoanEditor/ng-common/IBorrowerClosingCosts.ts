import {StrGuid} from "../../utils/guid";

import {E_ClosingCostFeeFormulaT} from "../../DataLayer/Enums/E_ClosingCostFeeFormulaT";

import {ILqbDataModel, ILqbDataModelG} from "./ILqbDataModel";

export interface IBorrowerClosingCosts {
    sections                    : IBorrowerClosingCostSection[];
    StandaloneLenderOrigCompFee :  IBorrowerClosingCostSectionFee;
    // LastSavedBorrowerOrigCompFee: any;
    DefaulFees: {
        DefaultSectionGFeeTypeId                         : StrGuid;
        Hud800LoanOriginationFeeTypeId                   : StrGuid;
        Hud800OriginatorCompensationFeeTypeId            : StrGuid;
        Hud800DiscountPointsFeeTypeId                    : StrGuid;
        Hud800CreditOrChargeFeeTypeId                    : StrGuid;
        Hud800AppraisalFeeFeeTypeId                      : StrGuid;
        Hud800CreditReportFeeTypeId                      : StrGuid;
        Hud800TaxServiceFeeTypeId                        : StrGuid;
        Hud800FloodCertificationFeeTypeId                : StrGuid;
        Hud800MortgageBrokerFeeTypeId                    : StrGuid;
        Hud800LenderInspectionFeeTypeId                  : StrGuid;
        Hud800ProcessingFeeTypeId                        : StrGuid;
        Hud800UnderwritingFeeTypeId                      : StrGuid;
        Hud800WireTransferFeeTypeId                      : StrGuid;
        Hud800Custom1FeeTypeId                           : StrGuid;
        Hud800Custom2FeeTypeId                           : StrGuid;
        Hud800Custom3FeeTypeId                           : StrGuid;
        Hud800Custom4FeeTypeId                           : StrGuid;
        Hud800Custom5FeeTypeId                           : StrGuid;
        Hud900DailyInterestFeeTypeId                     : StrGuid;
        Hud900MortgageInsurancePremiumRecurringFeeTypeId : StrGuid;
        Hud900MortgageInsurancePremiumUpfrontFeeTypeId   : StrGuid;
        Hud900HazardInsuranceFeeTypeId                   : StrGuid;
        Hud900FloodInsuranceFeeTypeId                    : StrGuid;
        Hud900WindstormInsuranceFeeTypeId                : StrGuid;
        Hud900CondoInsuranceFeeTypeId                    : StrGuid;
        Hud900PropertyTaxFeeTypeId                       : StrGuid;
        Hud900SchoolTaxFeeTypeId                         : StrGuid;
        Hud900OtherTax1FeeTypeId                         : StrGuid;
        Hud900OtherTax2FeeTypeId                         : StrGuid;
        Hud900OtherTax3FeeTypeId                         : StrGuid;
        Hud900OtherTax4FeeTypeId                         : StrGuid;
        Hud900HOADuesFeeTypeId                           : StrGuid;
        Hud900GroundRentFeeTypeId                        : StrGuid;
        Hud900OtherExp1FeeTypeId                         : StrGuid;
        Hud900OtherExp2FeeTypeId                         : StrGuid;
        Hud900OtherExp3FeeTypeId                         : StrGuid;
        Hud900OtherExp4FeeTypeId                         : StrGuid;
        Hud900Custom1FeeTypeId                           : StrGuid;
        Hud900VAFundingFeeTypeId                         : StrGuid;
        Hud900Custom2FeeTypeId                           : StrGuid;
        Hud1000HazardInsuranceReserveFeeTypeId           : StrGuid;
        Hud1000MortgageInsuranceReserveFeeTypeId         : StrGuid;
        Hud1000TaxReserveFeeTypeId                       : StrGuid;
        Hud1000SchoolTaxFeeTypeId                        : StrGuid;
        Hud1000FloodInsuranceReserveFeeTypeId            : StrGuid;
        Hud1000AggregateAdjustmentFeeTypeId              : StrGuid;
        Hud1000Custom1FeeTypeId                          : StrGuid;
        Hud1000Custom2FeeTypeId                          : StrGuid;
        Hud1000Custom3FeeTypeId                          : StrGuid;
        Hud1000Custom4FeeTypeId                          : StrGuid;
        Hud1000WindStormInsuranceFeeTypeId               : StrGuid;
        Hud1000CondoHo6InsuranceFeeTypeId                : StrGuid;
        Hud1000HomeOwnerAssociationDuesFeeTypeId         : StrGuid;
        Hud1000GroundRentFeeTypeId                       : StrGuid;
        Hud1000OtherTax1FeeTypeId                        : StrGuid;
        Hud1000OtherTax2FeeTypeId                        : StrGuid;
        Hud1000OtherTax3FeeTypeId                        : StrGuid;
        Hud1000OtherTax4FeeTypeId                        : StrGuid;
        Hud1100ClosingEscrowFeeTypeId                    : StrGuid;
        Hud1100OwnerTitleInsuranceFeeTypeId              : StrGuid;
        Hud1100LenderTitleInsuranceFeeTypeId             : StrGuid;
        Hud1100DocPreparationFeeTypeId                   : StrGuid;
        Hud1100NotaryFeeTypeId                           : StrGuid;
        Hud1100AttorneyFeeTypeId                         : StrGuid;
        Hud1100Custom1FeeTypeId                          : StrGuid;
        Hud1100Custom2FeeTypeId                          : StrGuid;
        Hud1100Custom3FeeTypeId                          : StrGuid;
        Hud1100Custom4FeeTypeId                          : StrGuid;
        Hud1200RecordingFeeTypeId                        : StrGuid;
        Hud1200DeedFeeTypeId                             : StrGuid;
        Hud1200MortgageFeeTypeId                         : StrGuid;
        Hud1200ReleaseFeeTypeId                          : StrGuid;
        Hud1200CountyTaxStampsFeeTypeId                  : StrGuid;
        Hud1200StateTaxStampsFeeTypeId                   : StrGuid;
        Hud1200Custom1FeeTypeId                          : StrGuid;
        Hud1200Custom2FeeTypeId                          : StrGuid;
        Hud1200Custom3FeeTypeId                          : StrGuid;
        Hud1300PestInspectionFeeTypeId                   : StrGuid;
        Hud1300Custom1FeeTypeId                          : StrGuid;
        Hud1300Custom2FeeTypeId                          : StrGuid;
        Hud1300Custom3FeeTypeId                          : StrGuid;
        Hud1300Custom4FeeTypeId                          : StrGuid;
        Hud1300Custom5FeeTypeId                          : StrGuid;
    };
    DisableTPAffIfHasContact: boolean;
    CanReadDFLP: boolean;
    CanSetDFLP: boolean;
    feeServiceHistory: { IsBlank: boolean; FeeSetHistory: any[] }; // sFeeServiceApplicationHistoryXmlContent
}

export interface  IBorrowerClosingCostSection {
    HudLineEnd         : ILqbDataModelG<number>;
    HudLineStart       : ILqbDataModelG<number>;
    SectionName        : ILqbDataModelG<string>;
    SectionType        : ILqbDataModelG<number>;
    TotalAmount        : ILqbDataModelG<number>;
    TotalAmountBorrPaid: ILqbDataModelG<number>;
    ClosingCostFeeList :  IBorrowerClosingCostSectionFee[];
    NewFeeTemplates    :  IBorrowerClosingCostSectionFee[];
}
export interface  IBorrowerClosingCostSectionFee {
    aff               : ILqbDataModelG<boolean>;
    apr               : ILqbDataModelG<boolean>;
    bene              : ILqbDataModelG<number>;
    can_shop          : ILqbDataModelG<boolean>;
    changeSect        : ILqbDataModelG<boolean>;
    desc              : ILqbDataModelG<string>;
    dflp              : ILqbDataModelG<boolean>;
    disc_sect         : ILqbDataModelG<number>;
    editableSystem    : ILqbDataModelG<boolean>;
    fha               : ILqbDataModelG<boolean>;
    gfeGrps           : ILqbDataModelG<number>;
    hudline           : ILqbDataModelG<string>;
    id                : ILqbDataModelG<string>;
    is_optional       : ILqbDataModelG<boolean>;
    is_system         : ILqbDataModelG<boolean>;
    is_title          : ILqbDataModelG<boolean>;
    legacy            : ILqbDataModelG<number>;
    mismo             : ILqbDataModelG<number>;
    section           : ILqbDataModelG<number>;
    tp                : ILqbDataModelG<boolean>;
    typeid            : ILqbDataModelG<StrGuid>;
    va                : ILqbDataModelG<boolean>;
    bene_desc         : ILqbDataModelG<string>;
    bene_id           : ILqbDataModelG<string>;
    did_shop          : ILqbDataModelG<boolean>;
    disable_bene_auto : ILqbDataModelG<boolean>;
    is_bf             : ILqbDataModelG<boolean>;
    org_desc          : ILqbDataModelG<string>;
    responsible       : ILqbDataModelG<number>;
    total             : ILqbDataModelG<number>;
    is_qm             : ILqbDataModelG<boolean>;
    prov              : ILqbDataModelG<number>;
    prov_choice       : ILqbDataModelG<number>;
    qm_amount         : ILqbDataModelG<number>;
    qm_warning        : ILqbDataModelG<boolean>;
    f                 :  IBorrowerClosingCostSectionFeeF;
    payments          :  IBorrowerClosingCostSectionFeePmt[];
}

export interface  IBorrowerClosingCostSectionFeeF {
    t          : ILqbDataModelG<E_ClosingCostFeeFormulaT>;
    base       : ILqbDataModelG<string>;
    p          : ILqbDataModel;
    pb         : ILqbDataModelG<string>;
    period     : ILqbDataModelG<string>;
    pt         : ILqbDataModelG<number>;
    base_fid   : ILqbDataModelG<string>;
    period_fid : ILqbDataModelG<string>;
}
export interface  IBorrowerClosingCostSectionFeePmt {
    amt            : ILqbDataModelG<number>;
    ent            : ILqbDataModelG<number>;
    id             : ILqbDataModelG<StrGuid>;
    is_fin         : ILqbDataModelG<boolean>;
    is_system      : ILqbDataModelG<boolean>;
    made           : ILqbDataModelG<boolean>;
    paid_by        : ILqbDataModelG<number>;
    pmt_at         : ILqbDataModelG<number>;
    pmt_dt         : ILqbDataModelG<Date>;
    responsible    : ILqbDataModelG<number>;
    cashPdPmt      : ILqbDataModelG<number>;
    mipFinancedPmt : ILqbDataModelG<number>;
}
