import {StrGuid} from "../../utils/guid";
import {ILqbDataModel, ILqbDataModelG, } from "./ILqbDataModel";
import { E_DeliveryMethodT } from "../../DataLayer/Enums/E_DeliveryMethodT";

export interface ILoanEstimateDate {
      UniqueId                               : ILqbDataModelG<StrGuid>;
      CreatedDate                            : ILqbDataModelG<Date>;
      DeliveryMethod                         : ILqbDataModelG<E_DeliveryMethodT>;
      ReceivedDate                           : ILqbDataModelG<Date>;
      IsInitial                              : ILqbDataModelG<boolean>;
      LastDisclosedTRIDLoanProductDescription: ILqbDataModelG<string>;
      IsManual                               : ILqbDataModelG<boolean>;
      ArchiveDate                            : ILqbDataModelG<Date>;
      ArchiveId                              : ILqbDataModelG<StrGuid>;
      TransactionId                          : ILqbDataModelG<string>;
      DisableManualArchiveAssociation        : ILqbDataModelG<boolean>;
      invalidArchive                         : ILqbDataModelG<boolean>;
}
