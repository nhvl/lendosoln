export interface ILoanSummary {
    sLNm                : string; // Loan Num
    sStatusT            : string; // Status
    sEmployeeLoanRepName: string; // Loan Officer
    sQualTopR           : string; // Top
    sQualBottomR        : string; // Bottom
    sCltvR              : string; // CLTV
    sLtvR               : string; // LTV
    sHcltvR             : string; // HCLTV
    sRateLockStatusT    : string; // Rate Lock Status
    sFinalLAmt          : string; // Total Loan Amt
    sLT                 : string; // Loan Type
    sNoteIR             : string; // Rate
}
