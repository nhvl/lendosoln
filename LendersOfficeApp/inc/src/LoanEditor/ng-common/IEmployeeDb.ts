import {StrGuid} from "../../utils/guid";

export interface IEmployeeDb {
    HasUploadedSignature: boolean;
}
