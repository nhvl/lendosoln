import {EditorController    } from "../ng-common/EditorController";
import {getLoanEditorConfig } from "../ng-common/getLoanEditorConfig";
import {INg2VarAttrs         } from "../../ng-directives/INg2VarAttrs";

export function createBasicLoanEditorDirective(name:string, appHtml: string, Controller = EditorController) {
    const config = getLoanEditorConfig(appHtml);

    const loanEditorDirective = ($parse: ng.IParseService): ng.IDirective => {
        return {
            restrict        : "E",
            scope           : { onSummaryChanged: "&", onStateChanged: "&" },
            controller      : Controller,
            controllerAs    : "vm",
            bindToController: true,
            template        : appHtml,
            link(scope          : ng.IScope,
                $ele            : ng.IAugmentedJQuery,
                attrs           : INg2VarAttrs,
                editorController: EditorController
            ) {
                const ng2Var = $parse(attrs.ng2Var);
                if (!!ng2Var && !!ng2Var.assign) ng2Var.assign(scope.$parent, editorController);
                editorController.load(config.sLId, config.appId, config.fieldList);
            }
        };
    };
    loanEditorDirective.$inject = ["$parse"];
    loanEditorDirective.ngName = name;
    return loanEditorDirective;
}
