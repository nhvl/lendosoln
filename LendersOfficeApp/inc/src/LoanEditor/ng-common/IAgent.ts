import {StrGuid} from "../../utils/guid";

import { E_AgentRoleT   } from "../../DataLayer/Enums/E_AgentRoleT";
import { E_AgentSourceT } from "../../DataLayer/Enums/E_AgentSourceT";

import {ILqbDataModel, ILqbDataModelG} from "./ILqbDataModel";

export interface IAgent {
    AgentName                          : ILqbDataModelG<string>;
    AgentRoleDescription               : ILqbDataModelG<string>;
    AgentRoleT                         : ILqbDataModelG<E_AgentRoleT>;
    AgentRoleTDesc                     : ILqbDataModelG<string>;
    AgentSourceT                       : ILqbDataModelG<E_AgentSourceT>;
    BranchName                         : ILqbDataModelG<string>;
    BrokerLevelAgentID                 : ILqbDataModelG<StrGuid>;
    CaseNum                            : ILqbDataModelG<string>;
    CellPhone                          : ILqbDataModelG<string>;
    ChumsId                            : ILqbDataModelG<string>;
    City                               : ILqbDataModelG<string>;
    CityStateZip                       : ILqbDataModelG<string>;
    // CompanyLicenseInfoList             : { };
    CompanyLoanOriginatorIdentifier    : ILqbDataModelG<string>;
    CompanyName                        : ILqbDataModelG<string>;
    DepartmentName                     : ILqbDataModelG<string>;
    EmailAddr                          : ILqbDataModelG<string>;
    EmployeeIDInCompany                : ILqbDataModelG<string>;
    EmployeeId                         : ILqbDataModelG<StrGuid>;
    FaxNum                             : ILqbDataModelG<string>;
    FaxOfCompany                       : ILqbDataModelG<string>;
    FurtherCreditToAccountName         : ILqbDataModelG<string>;
    FurtherCreditToAccountNumber       : ILqbDataModelG<string>;
    HasLenderAccountLast12Months       : ILqbDataModelG<boolean>;
    HasLenderRelationship              : ILqbDataModelG<boolean>;
    InvestorBasisPoints                : ILqbDataModelG<number>;
    // InvestorBasisPoints_rep            : ILqbDataModel;
    InvestorSoldDate                   : ILqbDataModelG<Date>;
    // InvestorSoldDate_rep               : ILqbDataModel;
    IsApplyToFormsUponSave             : ILqbDataModelG<boolean>;
    IsLender                           : ILqbDataModelG<boolean>;
    IsLenderAffiliate                  : ILqbDataModelG<boolean>;
    IsLenderAssociation                : ILqbDataModelG<boolean>;
    IsLenderRelative                   : ILqbDataModelG<boolean>;
    IsListedInGFEProviderForm          : ILqbDataModelG<boolean>;
    IsNewRecord                        : ILqbDataModelG<boolean>;
    IsNotifyWhenLoanStatusChange       : ILqbDataModelG<boolean>;
    IsOriginator                       : ILqbDataModelG<boolean>;
    IsOriginatorAffiliate              : ILqbDataModelG<boolean>;
    IsUsedRepeatlyByLenderLast12Months : ILqbDataModelG<boolean>;
    IsValid                            : ILqbDataModelG<boolean>;
    // KeyType                            : { };
    // LicenseInfoList                    : { };
    LicenseNumOfAgent                  : ILqbDataModelG<string>;
    LicenseNumOfAgentIsExpired         : ILqbDataModelG<boolean>;
    LicenseNumOfCompany                : ILqbDataModelG<string>;
    LicenseNumOfCompanyIsExpired       : ILqbDataModelG<boolean>;
    LoanOriginatorIdentifier           : ILqbDataModelG<string>;
    Notes                              : ILqbDataModelG<string>;
    OtherAgentRoleTDesc                : ILqbDataModelG<string>;
    OverrideLicenses                   : ILqbDataModelG<boolean>;
    PagerNum                           : ILqbDataModelG<string>;
    PayToABANumber                     : ILqbDataModelG<string>;
    PayToAccountName                   : ILqbDataModelG<string>;
    PayToAccountNumber                 : ILqbDataModelG<string>;
    PayToBankCityState                 : ILqbDataModelG<string>;
    PayToBankName                      : ILqbDataModelG<string>;
    Phone                              : ILqbDataModelG<string>;
    PhoneOfCompany                     : ILqbDataModelG<string>;
    ProviderItemNumber                 : ILqbDataModelG<string>;
    RecordId                           : ILqbDataModelG<StrGuid>;
    State                              : ILqbDataModelG<string>;
    StreetAddr                         : ILqbDataModelG<string>;
    StreetAddr_MultiLine               : ILqbDataModelG<string>;
    StreetAddr_SingleLine              : ILqbDataModelG<string>;
    TaxId                              : ILqbDataModelG<string>;
    Zip                                : ILqbDataModelG<string>;
    [key: string]                      : ILqbDataModel;
}

export interface IAgentComission {
    AgentType       : ILqbDataModelG<string>;
    AgentName       : ILqbDataModelG<string>;
    CompanyName     : ILqbDataModelG<string>;
    CommissionAmount: ILqbDataModelG<string>;
    IsPaid          : ILqbDataModelG<boolean>;
    Notes           : ILqbDataModelG<string>;
    RecordId        : ILqbDataModelG<StrGuid>;
}
