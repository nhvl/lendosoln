const $ = jQuery;

import html = require("./loadingIndicator.html");

export class LoadingIndicator { static ngName = "loadingIndicator";

    private message: string;
    private $ele: JQuery;

    constructor() {
        this.$ele = $(html);
        this.hide();
        this.$ele.appendTo("body");
    }

    show(message: string = "Loading...") {
        this.$ele.find("span").text(message);
        this.$ele.attr("hidden", null);
        //this.$ele[0].hidden = false;
    }

    hide() {
        this.$ele.attr("hidden", "true");
        //this.$ele[0].hidden = true;
    }
}
