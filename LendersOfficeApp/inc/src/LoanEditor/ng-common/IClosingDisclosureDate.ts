import {StrGuid} from "../../utils/guid";
import {ILqbDataModel, ILqbDataModelG, } from "./ILqbDataModel";
import { E_DeliveryMethodT } from "../../DataLayer/Enums/E_DeliveryMethodT";

// LendersOfficeLib\DataAccess\CFPBRequiredDates\ClosingDisclosureDates.cs

export interface IClosingDisclosureDate {
      UniqueId                                                         : ILqbDataModelG<StrGuid>;
      LoanClosingDate                                                  : ILqbDataModelG<Date>;
      DeliveryMethod                                                   : ILqbDataModelG<E_DeliveryMethodT>;
      ReceivedDate                                                     : ILqbDataModelG<Date>;
      IsInitial                                                        : ILqbDataModelG<boolean>;
      IsPreview                                                        : ILqbDataModelG<boolean>;
      IsFinal                                                          : ILqbDataModelG<boolean>;
      LastDisclosedTRIDLoanProductDescription                          : ILqbDataModelG<string>;
      IsManual                                                         : ILqbDataModelG<boolean>;
      ArchiveDate                                                      : ILqbDataModelG<Date>;
      ArchiveId                                                        : ILqbDataModelG<StrGuid>;
      TransactionId                                                    : ILqbDataModelG<string>;
      DisableManualArchiveAssociation                                  : ILqbDataModelG<boolean>;
      IsPostClosing                                                    : ILqbDataModelG<boolean>;
      IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower: ILqbDataModelG<boolean>;
      IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller  : ILqbDataModelG<boolean>;
      IsDisclosurePostClosingDueToNonNumericalClericalError            : ILqbDataModelG<boolean>;
      IsDisclosurePostClosingDueToCureForToleranceViolation            : ILqbDataModelG<boolean>;
      PostConsummationRedisclosureReasonDate                           : ILqbDataModelG<Date>;
      PostConsummationKnowledgeOfEventDate                             : ILqbDataModelG<Date>;
}
