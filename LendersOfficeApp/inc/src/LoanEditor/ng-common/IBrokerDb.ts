import {E_sStatusT} from "../../DataLayer/Enums/E_sStatusT";

export interface IBrokerDb {
    IsUseLayeredFinancing            : boolean;
    IsUseNewNonPurchaseSpouseFeature : boolean;
    CalculateclosingCostInPML        : boolean;
    IsDisplayChoiceUfmipInLtvCalc    : boolean;
    HasLenderDefaultFeatures         : boolean;
    EnabledStatusesByChannel         : {
        Blank                        : E_sStatusT[];
        Broker                       : E_sStatusT[];
        Wholesale                    : E_sStatusT[];
        Retail                       : E_sStatusT[];
        Correspondent                : {
            [key:string]             : E_sStatusT[];
        };
        [key:string]                 : E_sStatusT[] | {[key:string]: E_sStatusT[];};
    },
    ShowCRMID                        : boolean;
    EnableTeamsUI                    : boolean;
    HasManyUsers                     : boolean;
    CopyInternalUsersToOfficalContactsByDefault:boolean;
    EnableLenderCreditsWhenInLegacyClosingCostMode:boolean;
    IsEnableGfe2015                  : boolean;
}
