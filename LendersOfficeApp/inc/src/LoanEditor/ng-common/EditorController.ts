import {StrGuid} from "../../utils/guid";

import {IApplicant      } from "./IApplicant";
import {DataModels      } from "./DataModels";
import {LoadingIndicator} from "./loadingIndicator";
import {ILoanSummary    } from "./ILoanSummary";
import {IDialog         } from "../../ng-common/dialog";
import {ServiceFactory  } from "./service";

let $timeout : ng.ITimeoutService;
let loadingIndicator: LoadingIndicator;
let $q: ng.IQService;

export class EditorController { static ngName = "EditorController";

    loan        : DataModels;
    disabledSave: boolean;
    protected appId: StrGuid;
    private fieldList: string[];

    static $inject = ["$element", "$q", "$timeout", "dialog",ServiceFactory.ngName, LoadingIndicator.ngName, DataModels.ngName];
    constructor(protected $element: ng.IAugmentedJQuery,
                _$q: ng.IQService,
                _$timeout: ng.ITimeoutService,
                protected dialog: IDialog,
                protected service: ServiceFactory,
                protected _loadingIndicator: LoadingIndicator) {
        $q = _$q;
        $timeout = _$timeout;
        loadingIndicator = _loadingIndicator;

        this.init();
    }

    protected init(){}

    load(sLId: string, appId: string, fieldList:string[]) {
        const {dialog} = this;
        loadingIndicator.show();
        DataModels.get(sLId, (this.appId = appId), (this.fieldList = fieldList))
            .then((loan) => { this.onLoaded(loan) },
                  () => {
                      dialog.alert("Error occurred while loading data.", "Error");
                      return false;
                    })
            .finally(() => { loadingIndicator.hide(); });

        this.disabledSave = true;
        this.$element.one("change", ":input", () => { this.disabledSave = false; });
    }

    updateAppId(appId: StrGuid) {
        if (!appId && this.appId == appId) return;

        this.appId = appId;
        this.load(this.loan.sLId, this.appId, this.fieldList);
    }

    protected onSummaryChanged: (x:{ $summary: ILoanSummary, $applicants: IApplicant[] }) => void;
    protected onStateChanged: (x: { $pageStateId: string, $pageStateParams?: {} }) => ng.IPromise<void>;
    protected onLoaded(loan:DataModels) : boolean {
        this.loan = loan;

        if (typeof this.onSummaryChanged === "function") {
            this.onSummaryChanged({ $summary: loan.summary, $applicants: loan.applicants });
        }

        return true;
    }

    save(): ng.IPromise<void> {
        const {dialog} = this;

        const message = this.loan.validateToSave();
        if (message != null) return dialog.alert(message, "Invalid").then(() => $q.reject(message));

        loadingIndicator.show("Saving...");

        return $q<void>((resolve, reject) => {
            if (document.activeElement) {
                $(document.activeElement).trigger("change");
            }

            $timeout(() => {
                this.loan.save().then(
                    () => {
                        loadingIndicator.hide();
                        resolve();
                    },
                    (error) => {
                        loadingIndicator.hide();
                        dialog.alert("Error occurred while saving data.", "Error");
                        reject(error);
                    });
            });
        });
    }

    protected goTo(pageStateId:string,pageStateParams?:{}): ng.IPromise<void> {
        return this.onStateChanged({$pageStateId:pageStateId, $pageStateParams:pageStateParams});
    }
}
