import {ServiceFactory} from "./service";

export let $filter     : ng.IFilterService;
export let $q          : ng.IQService;
export let $timeout    : ng.ITimeoutService;
export let $http       : ng.IHttpService;
export let $rootScope  : ng.IRootScopeService;
export let $stateParams: ng.ui.IStateParamsService;
export let service     : ServiceFactory;

export let ngFilter: {
    currency ?: ng.IFilterCurrency,
    number   ?: ng.IFilterNumber,
    [key:string]: any
} = {};

ngServiceRunner.$inject = [
    "$filter"     ,
    "$q"          ,
    "$timeout"    ,
    "$http"       ,
    "$rootScope"  ,
    "$stateParams",
    ServiceFactory.ngName,
];
export function ngServiceRunner(
    _$filter     : ng.IFilterService        ,
    _$q          : ng.IQService             ,
    _$timeout    : ng.ITimeoutService       ,
    _$http       : ng.IHttpService          ,
    _$rootScope  : ng.IRootScopeService     ,
    _$stateParams: ng.ui.IStateParamsService,
    _service     : ServiceFactory
) {
    $filter      = _$filter     ;
    $q           = _$q          ;
    $timeout     = _$timeout    ;
    $http        = _$http       ;
    $rootScope   = _$rootScope  ;
    $stateParams = _$stateParams;
    service      = _service     ;

    "currency number".split(" ").forEach(k => { ngFilter[k] = $filter(k) });
}
