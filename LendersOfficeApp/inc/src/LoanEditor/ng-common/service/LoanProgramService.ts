import {BaseService} from "./BaseService";

import {StrGuid} from "../../../utils/guid";

type KeyValuePair = {Key: string, Value: string};

export class LoanProgramService extends BaseService {
    constructor() {
        super("LoanProgram");
    }

    GetLoanProgram() {
        return this.http<KeyValuePair[]>("GetLoanProgram", {});
    }
}
