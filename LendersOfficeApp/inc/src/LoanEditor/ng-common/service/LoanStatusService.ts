import {E_BranchChannelT        } from "../../../DataLayer/Enums/E_BranchChannelT";
import {E_sCorrespondentProcessT} from "../../../DataLayer/Enums/E_sCorrespondentProcessT";
import {E_sStatusT              } from "../../../DataLayer/Enums/E_sStatusT";

import {BaseService} from "./BaseService";

export class LoanStatusService extends BaseService {
    constructor() {
        super("LoanStatus");
    }

    getLoanStatus(params: {channel: E_BranchChannelT, process: E_sCorrespondentProcessT}) {
        return this.http<{
            loan: {label:string, value:E_sStatusT}[],
            lead: {label:string, value:E_sStatusT}[],
        }>("GetLoanStatus", params);
    }
}
