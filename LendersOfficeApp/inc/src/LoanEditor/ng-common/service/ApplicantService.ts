import {BaseService} from "./BaseService";

export class ApplicantService extends BaseService {
    constructor() {
        super("Applicant");
    }

    UpdatePrimaryBorrower(data: { loanId: string, appId: string }) {
        return this.http<void>("UpdatePrimaryBorrower", data);
    }

    AddApplication(data: { loanId: string }) {
        return this.http<string>("AddApplication", data);
    }

    DeleteApplicant(data: { loanId: string, appId: string }) {
        return this.http<void>("DeleteApplicant", data);
    }

    SwapBorrowerCoborrower(data: { loanId: string, appId: string }) {
        return this.http<void>("SwapBorrowerCoborrower", data);
    }

    DeleteCoborrower(data: { loanId: string, appId: string }) {
        return this.http<void>("DeleteCoborrower", data);
    }
}
