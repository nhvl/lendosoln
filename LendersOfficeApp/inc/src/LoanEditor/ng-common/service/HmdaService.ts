import {BaseService} from "./BaseService";
type KeyValuePair = {Key: string, Value: string};

export class HmdaService extends BaseService {
    constructor() {
        super("HMDA");
    }

    setDeniedBy() {
        return this.http<KeyValuePair[]>("SetDeniedBy", {});
    }
    importGeoCodes(data: {
        addr: string;
        city: string;
        county: string;
        state: string;
        zip: string;
        hmdaActionD: string;
    }) {
        return this.http<KeyValuePair[]>("ImportGeoCodes", data);
    }
}
