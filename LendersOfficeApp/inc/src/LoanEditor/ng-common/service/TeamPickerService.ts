import {BaseService          } from "./BaseService";
import {StrGuid,guidEmpty    } from "../../../utils/guid";

export class TeamPickerService extends BaseService {
    constructor() {
        super("TeamPicker");
    }
    search(data:{
        isAssignedTeam:boolean, 
        roleString:string
    }){
        return this.http<any[]>("Search", data);
    }
}

