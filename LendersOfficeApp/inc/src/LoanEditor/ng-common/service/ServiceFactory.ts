import {BaseService                } from "./BaseService";

import {AgentsService              } from "./AgentsService";
import {AlternateLender            } from "./AlternateLender";
import {ApplicantService           } from "./ApplicantService";
import {BrokerInfoService          } from "./BrokerInfoService";
import {ClosingCostTemplateService } from "./ClosingCostTemplateService";
import {DocumentVendorService      } from "./DocumentVendorService";
import {EdocService                } from "./EdocService";
import {HmdaService                } from "./HmdaService";
import {LiabilityRecordService     } from "./LiabilityRecordService";
import {LoanInfoService            } from "./LoanInfoService";
import {LoanService                } from "./LoanService";
import {LoanStatusService          } from "./LoanStatusService";
import {NavigationService          } from "./NavigationService";
import {OtherFinancingService      } from "./OtherFinancingService";
import {PipelineService            } from "./PipelineService";
import {EditTeamsService           } from "./EditTeamsService";
import {TeamPickerService          } from "./TeamPickerService";
import {EmployeeRoleService        } from "./EmployeeRoleService";
import {TransmittalService         } from "./TransmittalService";
import {GoodbyeLetterService       } from "./GoodbyeLetterService";
import {UserSettingsService        } from "./UserSettingsService";
import {LoanProgramService         } from "./LoanProgramService";

export class ServiceFactory { static ngName = "service";

    agents             : AgentsService             ;
    altLender          : AlternateLender           ;
    app                : ApplicantService          ;
    brokerInfo         : BrokerInfoService         ;
    closingCostTemplate: ClosingCostTemplateService;
    documentVendor     : DocumentVendorService     ;
    editTeams          : EditTeamsService          ;
    edoc               : EdocService               ;
    employeeRole       : EmployeeRoleService       ;
    goodbyeLetter      : GoodbyeLetterService      ;
    hmda               : HmdaService               ;
    liabilityRecord    : LiabilityRecordService    ;
    loan               : LoanService               ;
    loanInfo           : LoanInfoService           ;
    loanProgram        : LoanProgramService        ;
    loanStatus         : LoanStatusService         ;
    navigation         : NavigationService         ;
    otherFinancing     : OtherFinancingService     ;
    pipeline           : PipelineService           ;
    teamPicker         : TeamPickerService         ;
    transmittal        : TransmittalService        ;
    userSettings       : UserSettingsService       ;

    static $inject = ["$http", "$q"];
    constructor($http: ng.IHttpService, $q: ng.IQService) {
        BaseService.$http = $http;
        BaseService.$q    = $q;

        this.agents              = new AgentsService             ();
        this.altLender           = new AlternateLender           ();
        this.app                 = new ApplicantService          ();
        this.brokerInfo          = new BrokerInfoService         ();
        this.closingCostTemplate = new ClosingCostTemplateService();
        this.documentVendor      = new DocumentVendorService     ();
        this.edoc                = new EdocService               ();
        this.hmda                = new HmdaService               ();
        this.liabilityRecord     = new LiabilityRecordService    ();
        this.loan                = new LoanService               ();
        this.loanInfo            = new LoanInfoService           ();
        this.loanStatus          = new LoanStatusService         ();
        this.navigation          = new NavigationService         ();
        this.otherFinancing      = new OtherFinancingService     ();
        this.pipeline            = new PipelineService           ();
        this.editTeams           = new EditTeamsService          ();
        this.teamPicker          = new TeamPickerService         ();
        this.employeeRole        = new EmployeeRoleService       ();
        this.transmittal         = new TransmittalService        ();
        this.goodbyeLetter       = new GoodbyeLetterService      ();
        this.userSettings        = new UserSettingsService       ();
        this.loanProgram         = new LoanProgramService        ();
    }
}
