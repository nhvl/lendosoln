import {IDataModelsRaw} from "../DataModels";
import {BaseService} from "./BaseService";

export class LoanService extends BaseService {
    constructor() {
        super("loan");
    }
    
    protected getUrl(op: string): string {
        return `lqbapi/loan.ashx?op=${op}`; 
    }

    load(loanId: string, appId: string, data: string[]) {
        return this.http<IDataModelsRaw>("Load", data, {params: {loanid:loanId, appId:appId }});
    }

    calculate(loanId: string, appId: string, data: any) {
        return this.http<IDataModelsRaw>("Calculate", data, {params: {loanid:loanId, appId:appId }});
    }

    calculateZipcode(loanId: string, appId: string, data: any) {
        return this.http<IDataModelsRaw>("CalculateZipcode", data, {params: {loanid:loanId, appId:appId }});
    }

    save(loanId: string, appId: string, data: any) {
        return this.http<IDataModelsRaw>("Save", data, {params: {loanid:loanId, appId:appId }});
    }

    populate(loanId: string, appId: string, data: any, templateId: string) {
        return this.http<IDataModelsRaw>("Populate", {item1: data, item2: templateId}, {params: {loanid:loanId, appId:appId }});
    }
}
