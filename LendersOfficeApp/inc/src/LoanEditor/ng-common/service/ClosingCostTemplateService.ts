import {BaseService} from "./BaseService";
import {E_GfeVersion} from "../../../DataLayer/Enums/E_GfeVersion";

import {StrGuid} from "../../../utils/guid";

export interface IClosingCostTemplate {
    Id     : StrGuid;
    Name   : string;
    Version: E_GfeVersion;
}

export class ClosingCostTemplateService extends BaseService {
    constructor() {
        super("ClosingCostTemplate");
    }

    getClosingCost(params: {loanId: StrGuid, gfeVersion?: number}): ng.IPromise<IClosingCostTemplate[]> {
        if (params.gfeVersion == null) params.gfeVersion = -1;
        return this.http<IClosingCostTemplate[]>("GetClosingCost", params);
    }

    applyClosingCostTemplate(params: {loanId: StrGuid, templateId: StrGuid}) {
        return this.http<void>("ApplyClosingCostTemplate", params);
    }
}
