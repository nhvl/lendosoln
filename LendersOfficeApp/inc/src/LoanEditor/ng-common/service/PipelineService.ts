import {IDataModelsRaw} from "../DataModels";
import {BaseService} from "./BaseService";

export class PipelineService extends BaseService {
    constructor() {
        super("pipeline");
    }
    
    protected getUrl(op: string): string {
        return `lqbapi/pipeline.ashx?op=${op}`; 
    }

    load(data: any) {
        return this.http<IDataModelsRaw>("Load", data);
    }

    calculate(data: any) {
        return this.http<IDataModelsRaw>("Calculate", data);
    }

    calculateZipcode(data: any) {
        return this.http<IDataModelsRaw>("CalculateZipcode", data);
    }

    save(data: any) {
        return this.http<IDataModelsRaw>("Save", data);
    }
}
