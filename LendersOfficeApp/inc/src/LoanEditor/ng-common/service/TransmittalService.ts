import {BaseService          } from "./BaseService";
import {StrGuid,guidEmpty    } from "../../../utils/guid";

export class TransmittalService extends BaseService {
    constructor() {
        super("Transmittal");
    }
    calculateAssetTotal(data:{
        loanId:StrGuid
    }){
        return this.http<number>("CalculateAssetTotal", data);
    }
}

