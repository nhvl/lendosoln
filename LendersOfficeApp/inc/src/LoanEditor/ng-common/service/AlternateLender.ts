import {BaseService} from "./BaseService";

export class AlternateLender extends BaseService{
    constructor() {
        super("AlternateLender");
    }

    DeleteLender(data: { lenderId: string }){
        return this.http<void>("DeleteLender", data);
    }
}
