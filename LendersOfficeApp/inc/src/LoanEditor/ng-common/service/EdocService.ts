import {StrGuid, guidEmpty} from "../../../utils/guid";
import {BaseService} from "./BaseService";

export interface IConditionAssociationDto {
    IsAssociated  : boolean;
    TaskId        : string;
    RowId         : number;
    Order         : number;
    IsProtected   : boolean;
    ConditionText : string;
    Category      : string;

    isSelected    : boolean;
}

export interface IFolder {
    folderId  : number;
    folderName: string;
}
export interface IDocType extends IFolder {
    docId  : number;
    docName: string;
}


export class EdocService extends BaseService {
    constructor() {
        super("Edoc");
    }

    GetAllFiles(data: {showSystemFolders: boolean; brokerId?:StrGuid}) {
        if (!data.brokerId) data.brokerId = guidEmpty;
        return this.http<IDocType[]>("GetAllFiles", data);
    }

    GetConditionAssociation(data: {docId: StrGuid}) {
        return this.http<IConditionAssociationDto[]>("GetConditionAssociation", data);
    }
}
