import {StrGuid              } from "../../../utils/guid";

import {BaseService} from "./BaseService";

export class DocumentVendorService extends BaseService {
    constructor() {
        super("DocumentVendor");
    }

    getTransferToVendor(params: {vendorId: StrGuid, code: string}) {
        return this.http<{
            ErrorMessage: string,
            Result      : {Description:string, Code:string}[],
        }>("GetTransferToVendor", params);
    }
}
