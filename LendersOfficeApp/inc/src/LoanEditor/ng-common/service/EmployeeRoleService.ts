import {BaseService          } from "./BaseService";
import {StrGuid,guidEmpty    } from "../../../utils/guid";

export class EmployeeRoleService extends BaseService {
    constructor() {
        super("EmployeeRole");
    }
    clearAssignment(data:{
        loanId: StrGuid;
        copyToOfficialAgent:boolean;
        role:string;
    }){
       return this.http<string[]>("ClearAssignment", data);
    }
    assignEmployee(data:{
        employeeID:StrGuid;
        email:string;
        employeeName:string;
        pmlUser:boolean;
        loanId: StrGuid;
        copyToOfficialAgent:boolean;
        role:string;
    }){
       return this.http<string[]>("AssignEmployee", data);
    }
    assignLOAndBranch(data:{
        loanId: StrGuid;
        loanofficerId: StrGuid;
        branchId: StrGuid;
        copyToOfficialAgent:boolean;
        loanOfficerName: string;
        loanOfficerEmail: string;
        roleTStr:string;
    }){
       return this.http<string>("AssignLOAndBranch", data);
    }
    search(data:{
        loanId:StrGuid,
        searchFilter?:string,
        status:number,
        role:string,
        showMode:string,
        isOnlyDisplayLqbUsers:boolean,
        isOnlyDisplayPmlUsers:boolean,
        pmlBrokerId?:StrGuid,
        origid?:StrGuid,
    }){
        if(data.searchFilter == null){
            data.searchFilter = "";
        }
        if(data.pmlBrokerId == null){
            data.pmlBrokerId = guidEmpty;
        }
        if(data.origid == null){
            data.origid = guidEmpty;
        }
        return this.http<{ [key: string]: any }>("Search", data);
    }
}

