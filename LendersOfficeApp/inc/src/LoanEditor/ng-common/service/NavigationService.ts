
import {BaseService} from "./BaseService";

export interface IPage {
    Id         : string;
    Title      : string;
    Name       : string;
    Route     ?: string;
    Children  ?: IPage[];

    IsVisible ?: boolean;
    // ItemType  ?: "root" | "page" | "folder";

    IsExpand  ?: boolean;
    FullPath  ?: string;

    isFavorite?: boolean;
    parents   ?: IPage[]; // if Root / Cat / page => [Cat, Root]
}



export class NavigationService extends BaseService {
    constructor() {
        super("Navigation");
    }

    refreshNavigation(loanId: string) {
        return this.http<IPage>("RefreshNavigationFromLoanId", {loanId: loanId});
    }

    addFavorite(source: string, pageId: string){
        return this.http<void>("UpdateFavorites", {
            source: source,
            pageId: pageId,
            action: "add",
            previousPageId: ""
        });
    }

    removeFavorite(source: string, pageId: string){
        return this.http<void>("UpdateFavorites", {
            source: source,
            pageId: pageId,
            action: "remove",
            previousPageId: ""
        });
    }

    moveFavorite(source: string, pageId: string, previousPageId: string){
        return this.http<void>("UpdateFavorites", {
            source: source,
            pageId: pageId,
            action: "move",
            previousPageId: previousPageId
        });
    }

    saveMenuLayout(navigation: any){
        return this.http<void>("SaveMenuLayout", {
            newMenuSettingsXml: `<root>${this.retrieveFoldersXml(navigation)}</root>`
        });
    }

    retrieveFoldersXml(navigation: IPage[]): string
    {
        return navigation.map(item => item.Children == null ? null :
            [
                `<folder path=\"${item.FullPath}\" collapse=\"${!item.IsExpand}\"/>`,
                this.retrieveFoldersXml(item.Children)
            ].join("\r\n"))
            .filter(x => x != null)
            .join("\r\n");
    }
}



export class NavTree {
    private navigationService: NavigationService;
    items         : IPage[];
    descPages     : {desc: string, page: IPage}[];
    route2Page    : Map<string, IPage>;
    favoriteFolder: IPage;
    selectedPage  : IPage;
    select1       : IPage;
    select2       : IPage;
    select3       : IPage;

    constructor() {
        return this;
    }

    static init(navigationService: NavigationService, sLId: string) {
        return (
            navigationService.refreshNavigation(sLId)
            .then((rootNav: IPage) => {
                const navTree = new NavTree();
                navTree.navigationService = navigationService;
                navTree.init(rootNav.Children);
                return navTree;
            })
        );
    }

    init(items: IPage[]) {
        const favoriteFolder = items.find(p => p.Id == "Folder_Favorites");
        const favoritesId = favoriteFolder == null ? [] : favoriteFolder.Children.map(p => p.Id);

        this.items = items.filter(p => p.Id != "Folder_Favorites");
        this.route2Page       = registerRoute2Map(new Map<string, IPage>(), this.items, []);
        this.descPages        = initDescPages(this.items);

        const favoritePages = this.descPages.map(({page}) => page).filter(page => favoritesId.includes(page.Id));
        favoritePages.forEach(page => page.isFavorite = true);
        if (favoriteFolder != null) favoriteFolder.Children = favoritePages;
        this.favoriteFolder = favoriteFolder;

        return this;
    }

    setCurrentSelectedPageByRoute(route:string) {
        if (!this.route2Page.has(route)) { debugger; return; }
        const page = this.route2Page.get(route);
        this.selectedPage = page;
        [this.select1, this.select2, this.select3] = page.parents.concat([page]);
        if (this.select1 != null && this.select1.Route == null) this.select1.IsExpand = true;
        if (this.select2 != null && this.select2.Route == null) this.select2.IsExpand = true;
    }
    setSelectedPage(page:IPage) {
        if (!page.Route) return;
        this.setCurrentSelectedPageByRoute(page.Route);
        console.assert(this.selectedPage == page);
        this.selectedPage = page;
    }

    toggleFavorite(page ?: IPage) {
        page = page || this.selectedPage;

        if (page.isFavorite) {
            this.favoriteFolder.Children = this.favoriteFolder.Children.filter(p => p.Id != page.Id);
            this.navigationService.removeFavorite("loan", page.Id);

        } else {
            this.favoriteFolder.Children = this.favoriteFolder.Children.concat(page);
            this.navigationService.addFavorite("loan", page.Id);
        }
        page.isFavorite = !page.isFavorite;
    }

    favoriteMove(pageId:string, nextPageId:string){
        this.navigationService.moveFavorite("loan", pageId, nextPageId);
    }

    saveMenuLayout() {
        this.navigationService.saveMenuLayout([this.favoriteFolder].concat(this.items));
    }
}

function registerRoute2Map(route2Page: Map<string, IPage>, ps: IPage[], path: IPage[]) {
    ps.forEach(p => {
        const newPath = path.concat(p);
        if (p.Children != null) {
            registerRoute2Map(route2Page, p.Children, newPath);
        } else {
            p.parents = path;
            route2Page.set(p.Route, p);
        }
    });
    return route2Page;
}

function initDescPages(ps: IPage[], prefix: string = ""): {desc:string, page:IPage}[] {
    return [].concat(...ps.map(page =>
        page.Children == null
            ? [{ desc: !prefix ? page.Name : `${prefix} / ${page.Name}`, page }]
            : initDescPages(page.Children, `${prefix} / ${page.Name}`))) ;
}
