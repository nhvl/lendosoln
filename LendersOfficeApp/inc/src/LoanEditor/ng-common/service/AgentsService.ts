import {IDataModelsRaw,ILoanFields} from "../DataModels";
import {BaseService               } from "./BaseService";
import {StrGuid                   } from "../../../utils/guid";
import {Permission                } from "../../../DataLayer/Enums/Permission";

export class AgentsService extends BaseService {
    constructor() {
        super("Agents");
    }
    isPageReadOnly(){
        return this.http<boolean>("IsPageReadOnly", {});
    }
    getTeamNames(data:{
        loanId: StrGuid
    }){
        return this.http<{ [key: string]: string }>("GetTeamNames", data);
    }
    deleteOfficialContact(data:{
        loanId: StrGuid;
        fileVersion: number;
        idList: StrGuid[]
    }){
        return this.http<void>("DeleteOfficialContact", {
            loanId: data.loanId,
            fileVersion: data.fileVersion,
            idItems: data.idList.join(";")
        });
    }
    getAgentRecord(data:{
        loanId: StrGuid;
        recordId: StrGuid
    }){
        return this.http<IDataModelsRaw>("GetAgentRecord", data);
    }
    hasPermission(data:{
        permission:Permission
    }){
        return this.http<boolean>("HasPermission",data);
    }
    getLicenseTemplate(){
        return this.http<ILoanFields>("GetLicenseTemplate",null);
    }
}

