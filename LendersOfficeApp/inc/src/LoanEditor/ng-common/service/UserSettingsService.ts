import {BaseService} from "./BaseService";
import {E_UserTheme      } from "../../../DataLayer/Enums/E_UserTheme";
type KeyValuePair = {Key: string, Value: any};

export class UserSettingsService extends BaseService {
    constructor() {
        super("UserSettings");
    }

    Get() {
        return this.http<KeyValuePair[]>("Get", {});
    }
    Update(data: {
        userTheme : E_UserTheme;
    }) {
        return this.http<void>("Update", data);
    }
}
