import {IDataModelsRaw} from "../DataModels";
import {BaseService} from "./BaseService";

type KeyValuePair = {Key: string, Value: string};

export class BrokerInfoService extends BaseService {
    GetBranchList() {
        return this.http<KeyValuePair[]>("GetBranchList", {});
    }
    
    ApplyPasswordOptions(data: {
        branch: string;
        option: string;
        expirationD: string;
        cycleExpire: boolean;
        expirePeriod: string;
    }) {
        return this.http<string>("ApplyPasswordOptions", data);
    }
    
    constructor() {
        super("BrokerInfo");
    }
        
    GetCRAOptions() {
        return this.http<any[]>("GetCRAOptions", {});
    }
}
