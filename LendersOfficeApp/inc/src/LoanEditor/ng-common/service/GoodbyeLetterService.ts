import {BaseService          } from "./BaseService";
import {StrGuid,guidEmpty    } from "../../../utils/guid";

export class GoodbyeLetterService extends BaseService {
    constructor() {
        super("GoodbyeLetter");
    }
    getSchedDateOfFirstPmt(data:{
        loanId: StrGuid,
        acceptingD: String
    }){
        return this.http<String>("GetSchedDateOfFirstPmt", data);
    }
    getInvestorInfo(data:{
        id: number
    }){
        return this.http<{ [key: string]: any }>("GetInvestorInfo", data);
    }
}

