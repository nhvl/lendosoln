import {StrGuid, guidEmpty} from "../../../utils/guid";
import {IException} from "../../../ng-common/IException";
import {
    IPromise,
    IHttpPromiseCallbackArg,
    IQService,
    IHttpService,
} from "angular";

const defaultConfig = {
    //transformRequest
    transformResponse,
};

export class BaseService {
    private endPoint: string;
    constructor(endPoint: string) {
        this.endPoint = endPoint;
    }

    protected getUrl(op: string): string {
        const {endPoint} = this;
        return `lqbapi/service.ashx?op=${endPoint}/${op}`;
    }
    protected http<T>(op: string, data: {}, config ?: {}): IPromise<T> {
        const {$q, $http} = BaseService;
        return new $q<T>((resolve, reject) => {
            $http.post<T>(this.getUrl(op), data, angular.extend({}, defaultConfig, config))
                .then((response: IHttpPromiseCallbackArg<T>) => {
                        const contentType = response.headers("Content-Type");
                        if (contentType != null && contentType.includes("text/html")) {
                            reject("System Error.");
                        } else {
                            resolve(response.data);
                        }
                    },
                    (response: IHttpPromiseCallbackArg<IException>) => {
                        if (response.status === 403) {
                            const url = response.headers("Location");
                            if (!!url) window.location.href = url;
                        }

                        const e: IException = response.data;
                        if (e != null && e.Message != null) reject(e.Message);
                        else                                reject("Service Error");
                    }
                );
        });
    }

    static $q: IQService;
    static $http: IHttpService;
}

function transformResponse(text:string):any {
    try {
        return JSON.parse(text,
            (key: string, value: any) => {
                if (typeof value === "string") {

                    const date = parseDate(value); if (date != null) return date;

                    if (key === "v") {
                        value = value.trim();
                        if (value.length < 1) return value;

                            //Some numbers begin with 0. Keep them as string.
                            if (value.length > 1
                                && value.startsWith("0")
                                && value.slice(-1) !== "%")
                                return value;

                        {
                            const nv = Number(value);
                            if (!Number.isNaN(nv)) return nv;
                        }

                        if (/^\(\$[0-9\.,]+\)$/.test(value)) {
                            try {
                                return - Number(value.replace(/[^0-9\.]+/g, ""));
                            } catch (e) { }
                        }
                        if (value[0] === "$") {
                            try {
                                return Number(value.replace(/[^0-9\.]+/g, ""));
                            } catch (e) { }
                        } else if (value.slice(-1) === "%") {
                            try {
                                return Number.parseFloat(value.slice(0, -1));
                            } catch (e) { }
                        }
                    }

                    if (key === "Key") {
                        if (value == "") return value;

                        const nv = Number(value);
                        if (!Number.isNaN(nv)) return nv;
                    }
                }
                return value;
            });
    }
    catch (e) { return text; }
}

function parseDate(value: string): Date {
    // parse ASP.NET DateTime type
    const result = /\/Date\((-?[0-9]+)(?:[a-zA-Z]|(?:\+|-)[0-9]{4})?\)\//g.exec(value);
    if (!!result) return new Date(parseFloat(result[1]));

    //Make sure that numbers cannot be parsed to Date
    if ( !Number.isNaN(Number(value))){
        return null;
    }

    if (/^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/g.
        test(value)) return new Date(value);
}
