import {BaseService          } from "./BaseService";
import {StrGuid,guidEmpty    } from "../../../utils/guid";

export class EditTeamsService extends BaseService {
    constructor() {
        super("EditTeams");
    }
    clearLoanAssignment(data:{
        loanId: StrGuid;
        roleString:string
    }){
       return this.http<void>("ClearLoanAssignment", data);
    }
    assignLoan(data:{
        loanId: StrGuid;
        teamId: StrGuid;
    }){
       return this.http<void>("AssignLoan", data);
    }
}

