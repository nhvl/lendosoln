import {BaseService          } from "./BaseService";

export class LiabilityRecordService extends BaseService {
    constructor() {
        super("LiabilityRecord");
    }
    addToReo(data: {
        reoId       :  string;
        balValue    : string;
        paymentValue: string;
        loanId      : string;
        appId       : string;
        fileVersion : number;
    }) {
        return this.http<void>("AddToReo", data);
    }
}

