import {BaseService} from "./BaseService";

import {StrGuid} from "../../../utils/guid";

export class LoanInfoService extends BaseService {
    constructor() {
        super("LoanInfo");
    }

    generateNewLoanNumber(params: {loanId: StrGuid}) {
        return this.http<string>("GenerateNewLoanNumber", params);
    }
}
