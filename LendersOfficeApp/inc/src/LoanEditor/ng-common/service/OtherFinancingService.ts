import {BaseService} from "./BaseService";

export class OtherFinancingService extends BaseService {
    constructor() {
        super("OtherFinancing");
    }
    unlinkLoan(data: {
        loanId: string;
    }) {
        return this.http<void>("UnlinkLoan", data);
    }
    updateLinkLoan(data: {
        loanId: string;
    }) {
        return this.http<string>("UpdateLinkLoan", data);
    }
}
