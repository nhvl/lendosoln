import omit = require("lodash/omit");
import {recursiveUpdate, recursiveTraversal} from "../../utils/recursiveUpdate";
import {ILqbDataModel} from "./ILqbDataModel";

export function getCacheValue(state:any){
    return recursiveUpdate(
        omit(state, ["isCalculating", "deferChangedFieldsForCalculate", "_cacheValue", "eventEmitter"]),
        updateFunc);

    function updateFunc(thing: any): [boolean, any] {
        if (isLqbDateModel(thing)) return [true, thing.v];

        if (thing != null && typeof thing === "object") {
            const angularTempKeys = Object.keys(thing).filter(key => key.startsWith("$$"));
            if (angularTempKeys.length > 0) {
                return [true, recursiveUpdate(omit(thing, angularTempKeys), updateFunc)];
            }
        }
        return [false, undefined];
    }

    function isLqbDateModel(x: any): x is ILqbDataModel {
        return (x != null && (typeof x === "object") && x.hasOwnProperty("v"));
    }
}
