import {StrGuid} from "../../utils/guid";

export interface IBrokerUser {
    firstName                        : string;
    lastName                         : string;
    HasPmlEnabled                    : boolean;
    documentVendorBrokerSettings     : {
        BrokerId                     : StrGuid;
        VendorId                     : StrGuid;
        Login                        : string;
        AccountId                    : string;
        ServicesLinkNm               : string;
        DisclosureInTest             : boolean;
        DisclosureBilling            : boolean;
        DisclosureOnlineInterface    : boolean;
        IsEnableForProductionLoans   : boolean;
        IsEnableForTestLoans         : boolean;
        BlockManualFulfillment       : boolean;
        HasCustomPackageData         : boolean;
        CustomPackageDataJSON        : string;
        EPassword                    : string;
        Password                     : string;
    };
    EmployeeId                       : StrGuid;
    AllowLoanAssignmentsToAnyBranch  :boolean;
}
