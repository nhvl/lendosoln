import {EditorController                } from "../ng-common/EditorController";
import {createBasicLoanEditorDirective  } from "../ng-common/createBasicLoanEditorDirective";
import {E_sDisclosureRegulationT        } from "../../DataLayer/Enums/E_sDisclosureRegulationT";
import {DataModels                      } from "../ng-common/DataModels";

import * as appHtml from "./index.html";

class DetailsOfTransactionController extends EditorController {
    onLoaded(loan: DataModels): boolean {
        super.onLoaded(loan);
        //https://lqbopm/default.asp?231830#BugEvent.1981600
        //sLAmtLckd is always disabled in this page
        this.loan.fields["sLAmtLckd"].r = true;
        return true;
    }
    isTRID2015(v:number) {
        return v == E_sDisclosureRegulationT.TRID2015;
    }
}
export const detailsOfTransactionDirective = createBasicLoanEditorDirective("detailsOfTransaction", appHtml, DetailsOfTransactionController);
