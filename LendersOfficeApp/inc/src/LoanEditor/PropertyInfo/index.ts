import {createBasicLoanEditorDirective} from "../ng-common/createBasicLoanEditorDirective";

import * as appHtml from "./index.html";

export const propertyInfoDirective = createBasicLoanEditorDirective("propertyInfo", appHtml);
