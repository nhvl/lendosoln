import {EditorController              } from "../ng-common/EditorController";
import {createBasicLoanEditorDirective} from "../ng-common/createBasicLoanEditorDirective";
import {ILqbDataModel                 } from "../ng-common/ILqbDataModel";

import * as appHtml from "./index.html";


class NmlsCallReportController extends EditorController {
}
export const nmlsCallReportDirective = createBasicLoanEditorDirective("nmlsCallReport", appHtml, NmlsCallReportController);
