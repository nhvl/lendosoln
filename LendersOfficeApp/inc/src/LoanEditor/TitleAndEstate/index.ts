import {createBasicLoanEditorDirective} from "../ng-common/createBasicLoanEditorDirective";
import {EditorController} from "../ng-common/EditorController";

import * as appHtml from "./index.html";

class TitleAndEstateController extends EditorController {
    addTob() {
        const {sTitleBorrowers} = this.loan;
        const {template, data} = sTitleBorrowers;
        data.push(naiveClone(template));

        this.loan.calculate();
    }
    removeTitleBorrower(b:any) {
        const {sTitleBorrowers} = this.loan;
        sTitleBorrowers.data = sTitleBorrowers.data.filter(x => x !== b);
        this.loan.calculate();
    }

    addSeller(){
        const {sSellerCollection} = this.loan;
        const {template, data} = sSellerCollection;
        data.push(naiveClone(template));

        this.loan.calculate();
    }
    removeSeller(b:any){
        const {sSellerCollection} = this.loan;
        sSellerCollection.data = sSellerCollection.data.filter(x => x !== b);
        this.loan.calculate();
    }
}

export const titleAndEstateDirective = createBasicLoanEditorDirective("titleAndEstate", appHtml, TitleAndEstateController);

function naiveClone(x: any) { return JSON.parse(JSON.stringify(x)) }
