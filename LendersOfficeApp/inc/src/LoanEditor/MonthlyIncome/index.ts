import {ILqbDataModelG, ILqbDataModel   } from "../ng-common/ILqbDataModel";
import {EditorController                } from "../ng-common/EditorController";
import {createBasicLoanEditorDirective  } from "../ng-common/createBasicLoanEditorDirective";

import * as appHtml from "./index.html";

class MonthlyIncomeController extends EditorController {
    addOtherIncome() {
        const {data, template} = this.loan.app.aOtherIncomeList;
        this.loan.app.aOtherIncomeList.data = data.concat([angular.merge({}, template)]);
        this.loan.calculate();
    }
    deleteOtherIncome(i: number) {
        if (i < 3) return; // https://lqbopm.meridianlink.com/default.asp?234594#BugEvent.2326885

        const {data} = this.loan.app.aOtherIncomeList;
        const item = data[i];

        this.dialog.confirm(`Delete the record (${item.desc.v || ("#"+(i+1))}, $ ${item.val.v})?`, "Delete?", "Delete", "Cancel")
        .then(isConfirm => {
            if (isConfirm) {
                data.splice(i, 1);1
                this.loan.calculate();
            }
        });
    }
}

export const monthlyIncomeDirective = createBasicLoanEditorDirective("monthlyIncome", appHtml, MonthlyIncomeController);

