import {EditorController              } from "../ng-common/EditorController";
import {createBasicLoanEditorDirective} from "../ng-common/createBasicLoanEditorDirective";
import {ILqbDataModel                 } from "../ng-common/ILqbDataModel";
import {DataModels                    } from "../ng-common/DataModels";

import {E_MipFrequency} from "../../DataLayer/Enums/E_MipFrequency";
import {E_TriState} from "../../DataLayer/Enums/E_TriState";
import {E_sClosingCostFeeVersionT} from "../../DataLayer/Enums/E_sClosingCostFeeVersionT";

import * as appHtml from "./index.html";

class UpfrontMipffController extends EditorController {
    private E_MipFrequency = E_MipFrequency;
    private E_TriState = E_TriState;
    private E_sClosingCostFeeVersionT = E_sClosingCostFeeVersionT;
    init() { }
    // onLoaded(loan:DataModels) {
    //     super.onLoaded(loan);
    //     return true;
    // }
}
export const upfrontMipffDirective = createBasicLoanEditorDirective("upfrontMipff", appHtml, UpfrontMipffController);

// ~/newlos/LoanInfo.aspx?pg=1&loanid=e28f7e55-7cb3-443c-85ad-a545018774b7&printid=&appid=b4c88b01-621c-4f41-a0e8-a54501877512&printid=
// ~/newlos/UpfrontMIP.ascx
