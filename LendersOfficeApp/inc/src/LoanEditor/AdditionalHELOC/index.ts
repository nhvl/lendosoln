import {EditorController              } from "../ng-common/EditorController";
import {createBasicLoanEditorDirective} from "../ng-common/createBasicLoanEditorDirective";

import * as appHtml from "./app.html";

class AdditionalHelocController extends EditorController {
}
export const additionalHelocDirective = createBasicLoanEditorDirective("additionalHeloc", appHtml, AdditionalHelocController);
