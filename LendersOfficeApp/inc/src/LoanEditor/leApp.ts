import {on_Ctrl_S} from "../utils/keyboardEvents";

import {IAngularEvent, IRootScopeService, IQService, IScope} from "angular";
import {State, StateService, Transition} from "angular-ui-router";
import {INiceScrollOptions, INiceScrollObject} from "../dt-local/jquery.nicescroll/index.d";

import {EditorController        } from "./ng-common/EditorController";
import {IApplicant              } from "./ng-common/IApplicant";
import {ILoanSummary            } from "./ng-common/ILoanSummary";
import {getApplicantId, getLoanId} from "./ng-common/getLoanEditorConfig";
import {Dialog, IDialog         } from "../ng-common/dialog";
import {sublMatch               } from "../utils/sublMatch";
import {ServiceFactory          } from "./ng-common/service";
import {$timeout                } from "./ng-common/ngServices";

import appHtml = require("./leApp.html");

import {NavTree, IPage} from "./ng-common/service/NavigationService";

let dialog: IDialog;



class LeAppController {
    private loanSummary: ILoanSummary;

    private applicants: IApplicant[];
    private selectedApplicant: IApplicant;

    private loanEditor: EditorController;

    private isShowUserSettingsModal: boolean;

    private isShowApplicantDropdown: boolean = false;
    private isShowLoanSummary: boolean = false;

    navTree: NavTree;

    private eleContent: HTMLDivElement;

    static $inject = ["$transitions", "$element", "$state", "$q", "dialog",ServiceFactory.ngName, "$scope"];
    constructor(
                $transitions : Transition,
        private $element   : JQuery,
        private $state     : StateService,
        private $q         : IQService,
                _dialog    : IDialog,
        private service    : ServiceFactory,
                $scope     : IScope
    ){
        dialog = _dialog;

        this.isShowUserSettingsModal = false;

        this.loanSummary = null;

        this.applicants = null;
        this.selectedApplicant = null;

        $("body").css("overflow", "hidden");

        window.addEventListener("blur", () => {
            $timeout(() => { this.isShowApplicantDropdown = false; });
        });

        setTimeout(() => {
            //
            const applicantsDdlBtn = $element.find(".applicants-ddl-btn");
            const itemWidth = $element.find(".applicants-ddl-items").outerWidth();
            if (applicantsDdlBtn.outerWidth() < itemWidth) {
                applicantsDdlBtn.width(itemWidth);
            }
        });

        on_Ctrl_S(() => { this.save(); return false; });

        const navTreePromise = NavTree.init(this.service.navigation, getLoanId()).then((navTree) => this.navTree = navTree);

        $transitions.onEnter({}, (transition:Transition, state: State) => {
            navTreePromise.then(() => {
                this.onStateChange(state.name)
            })
        });
    }

    private onLoanSummaryClick() {
        this.isShowLoanSummary = !this.isShowLoanSummary;
    }

    private onStateChange(route: string) {
        if (this.navTree == null) { debugger; return; }

        this.navTree.setCurrentSelectedPageByRoute(route);

        if (this.eleContent != null) {
            this.eleContent.scrollTop = 0;
        }
    }
    private navToPage(pageStateId: string, params?: {}) {
        this.askSave().then(() => { this.$state.go(pageStateId, params); });
    }
    private navBack   () { this._historyGo(-1); }
    private navForward() { this._historyGo( 1); }
    private _historyGo(i: number){
        this.askSave().then(() => { window.history.go(i); });
    }

    private save() {
        this.loanEditor.save();
    }
    private close(){
        this.askSave().then(() => {
            window.close();
        });
    }
    private askSave() {
        const {$q} = this;

        return (this.loanEditor.loan.isDirty() ?
            dialog.ync("Do you want to save the changes?", "Save?").then(o => {
                switch (o) {
                    case  1: return this.loanEditor.save();
                    case  0: this.loanEditor.loan.skipDirtyCheck1Time = true; return;
                    case -1: return $q.reject();
                    default: console.error(o); return $q.reject();
                }
            }) :
            $q.when()
        );
    }
    private print() { window.print(); }

    private navigatePipeline() { window.location.href = "Pipeline.aspx"; }







    private onSummaryChanged(summary: ILoanSummary, applicants: IApplicant[], navJson: string) {
        this.loanSummary = summary;
        this.applicants = applicants;
        const applicantId = getApplicantId();

        if (this.selectedApplicant == null) {
            this.selectedApplicant = !applicantId
                ? this.applicants[0]
                : this.applicants.find(x => x.aAppId == applicantId)
            ;
        }
    }






    private selectApplicant(applicant: IApplicant){
        this.selectedApplicant = applicant;
        this.loanEditor.updateAppId(applicant.aAppId);
    }

















    private leftNavShowSm: boolean = false;
    private leftNavShowMd: boolean = true;
    private toggleSidebarNavM() {
        this.leftNavShowSm = !this.leftNavShowSm;
    }
    private toggleSidebarNav() {
        // reset
        this.leftNavShowSm = false;

        this.leftNavShowMd = !this.leftNavShowMd;
    }
}

leAppDirective.ngName = "leApp";
export function leAppDirective(): ng.IDirective {
    return {
        restrict        : "E",
        scope           : {},
        controller      : LeAppController,
        controllerAs    : "vm",
        template        : appHtml,
    };
}
