import {service                        } from "../../ng-common/ngServices";
import {LqModalController, ModalStatus } from "../lqModal";
import {PipelineEditorController       } from "./../../../Pipeline/ng-common/PipelineEditorController";

export class BranchPrefixModalController extends PipelineEditorController {
    public vendors: any[] = [];
    public didChoose: {(name: string): void}
    private stateOption: number = 0;

    private filteredEmployee: any[] = [];
    private modal: LqModalController;

    protected fieldList(): string[] { return ["BranchPrefixes"] };

    protected $postLink() { }

    show() {
        super.$postLink();
        this.modal.setOption({ backdrop:"static", keyboard:false, });
        this.modal.showWithOKCancelApply(
            ((promiseValue) =>{
                switch(promiseValue){
                    case ModalStatus.Save:
                        this.done();
                        break;
                    case ModalStatus.Discard:
                        this.cancel();
                        break;
                    case ModalStatus.Apply:
                        this.apply();
                        break;
                }
            }), (() => {
                return this.pipeline.isDirty();
            })
        );
    }

    done(){
        this.apply();
        this.modal.close();
    }

    cancel(){
        this.modal.close();
    }

    apply(){
        this.pipeline.save();
        this.pipeline.skipDirtyCheck1Time = true;
    }
}

import * as branchPrefixModalHtml from "./BranchPrefixModal.html";


export const branchPrefixModal: ng.IComponentOptions = {
    ngName        : "branchPrefixModal",
    bindings      : {
        ref       : "&",
    },
    controllerAs  : "vm",
    template      : branchPrefixModalHtml,
    controller    : BranchPrefixModalController as any,
};
