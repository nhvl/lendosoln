import {service                        } from "../../ng-common/ngServices";
import {LqModalController, ModalStatus } from "../lqModal";
import {EditorController               } from "./../../ng-common/EditorController"
import {ILoanFields, DataModels        } from "./../../ng-common/DataModels"
import {updateFromRaw                  } from "./../../ng-common/ILqbDataModel";


export class RateFloorCalculatorController extends EditorController {

    private modal: LqModalController;
    private ref  : (_: {$ref  : RateFloorCalculatorController}) => void;
    private onUpdate: (_: {$loanField: ILoanFields}) => void;

    private fieldPopulator: any;
    private $onInit() {
        if (!!this.ref) this.ref({ $ref: this }); else debugger;
    }

    show(loan: DataModels){
        this.fieldPopulator = loan.fields;
        this.load(loan.sLId, loan.appId, ["sRAdjFloorCalcT", "sRAdjFloorBaseR", "sRAdjFloorAddR", "sNoteIR", "sRAdjLifeCapR", "sRAdjFloorTotalR", "sRAdjFloorLifeCapR", "sRAdjFloorR"]);
        
        this.modal.setOption({ backdrop:"static", keyboard:false, });
        this.modal.showWithOKCancel(
            ((promiseValue) =>{
                switch(promiseValue){
                    case ModalStatus.Save:
                        this.saveData();
                        break;
                    case ModalStatus.Discard:
                        this.close();
                        break;
                }
            }), (() => { return this.loan.isDirty() })
        );
    }

    protected onLoaded(loan:DataModels) : boolean {
        const returnVal = super.onLoaded(loan);
        
        this.loan.fields["sRAdjFloorCalcT"].v = this.fieldPopulator.sRAdjFloorCalcT.v;
        this.loan.fields["sRAdjFloorAddR"].v = this.fieldPopulator.sRAdjFloorAddR.v;
        this.loan.fields["sRAdjFloorR"].v = this.fieldPopulator.sRAdjFloorR.v;

        this.loan.calculate();
        return returnVal;
    }

    close(){
        this.modal.close();
    }

    saveData(){
        if (this.onUpdate != null){
            let $loanField = this.loan.fields;
            this.onUpdate({$loanField });
        }

        this.close();
    }
}

export const rateFloorCalculator: ng.IComponentOptions = {
    ngName        : "rateFloorCalculator",
    bindings      : {
        ref       : "&",
        onUpdate  : "&",
    },
    controllerAs  : "vm",
    template      : require("./RateFloorCalculator.html"),
    controller    : RateFloorCalculatorController as any,
};
