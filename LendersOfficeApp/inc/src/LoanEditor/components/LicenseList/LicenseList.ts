import {service       } from "../../ng-common/ngServices";
import {ILqbDataModel } from "./../../ng-common/ILqbDataModel";
import {ILoanFields   } from "./../../ng-common/DataModels";

export class LicenseListController {
    licenses: ILoanFields[];
    licenseTemplate: ILoanFields;

    nmLsIdentifier: ILqbDataModel;
    DisplayLosIdentifier:boolean;
    IsBranchLicense:boolean;
    IsCompanyLicense:boolean;

    private ref       : (_: {$ref  : LicenseListController}) => void;

    removeLicense(index : number){
        this.licenses.splice(index, 1)
    }

    addLicense(){
        this.licenses.push(angular.merge({}, this.licenseTemplate))
    }
    isExpiredLicense(index: number): boolean{
        const ExpD = this.licenses[index]['ExpD'].v;
        if(ExpD == null || ExpD == "")
            return false;
        const dateValue = new Date(ExpD);
        return !(dateValue == null || dateValue > new Date());
    }

    private $onInit() {
        this.DisplayLosIdentifier = true;
        this.IsBranchLicense = false;
        this.IsCompanyLicense = false;
        if (!!this.ref) this.ref({ $ref: this }); else debugger;
    }
    private GetLicenseAddress(index:number){
        if(this.IsCompanyLicense && this.licenses[index]["Street"].v != ""){
            return this.licenses[index]["Street"].v + ", " + this.licenses[index]["City"].v + ", " + this.licenses[index]["AddrState"].v + ", " + this.licenses[index]["Zip"].v;
        }
        return "";
    }
}

import * as licenseListHtml from "./LicenseList.html";


export const licenseList: ng.IComponentOptions = {
    ngName        : "licenseList",
    bindings      : {
        ref       : "&",
    },
    controllerAs  : "vm",
    template      : licenseListHtml,
    controller    : LicenseListController as any,
};
