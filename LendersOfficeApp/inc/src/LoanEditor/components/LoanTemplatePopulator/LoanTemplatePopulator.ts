import {service                        } from "../../ng-common/ngServices";
import {LqModalController, ModalStatus } from "../lqModal";
import {EditorController               } from "./../../ng-common/EditorController"

export class LoanTemplatePopulatorController extends EditorController {

    private modal: LqModalController;
    private ref  : (_: {$ref  : LoanTemplatePopulatorController}) => void;
    private templateSelected: (_: {$templateId: string}) => void;
 
    private programDict: any;
    private $onInit() {
        if (!!this.ref) this.ref({ $ref: this }); else debugger;
    }

    show(loanId: string, appId: string, isBorrower: boolean){
        this.modal.setOption({ backdrop:"static", keyboard:false, });
        this.service.loanProgram.GetLoanProgram().then((x) => this.programDict = x);
        this.modal.show();
    }
    
    selectTemplate(id:string){
        if (this.templateSelected != null){
            this.templateSelected({$templateId: id});
        }
        this.close();
    }

    close(){
        this.modal.close();
    }
}

export const loanTemplatePopulator: ng.IComponentOptions = {
    ngName        : "loanTemplatePopulator",
    bindings      : {
        ref              : "&",
        templateSelected : "&",
    },
    controllerAs  : "vm",
    template      : require("./LoanTemplatePopulator.html"),
    controller    : LoanTemplatePopulatorController as any,
};
