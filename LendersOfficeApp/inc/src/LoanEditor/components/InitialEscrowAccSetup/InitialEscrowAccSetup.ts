import {EditorController              } from "../../ng-common/EditorController";
import {LqModalController             } from "../../components/lqModal";
import {ServiceFactory                 } from  "./../../ng-common/service";
import {IDataModelsRaw, PipelineModels } from  "./../../../Pipeline/ng-common/PipelineModels";

export class InitialEscrowAccSetupController extends EditorController {
    private modal: LqModalController;
    private pipeline: PipelineModels;
    private ref       : (_: {$ref  : InitialEscrowAccSetupController}) => void;

    private rowIndexes:[number] = Array.apply(null, {length: 13}).map(Number.call, Number);
    private defaltColumnIndexes:[number] = Array.apply(null, {length: 7}).map(Number.call, Number);
    private customColumnIndexes:[number] = [7, 8];
    private rowTitles:[string] = ["Cushion", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    private $onInit() {
        if (!!this.ref) this.ref({ $ref: this }); else debugger;
    }

    private close(){
        this.modal.close();
    }

    private saveModal(){
        this.loan.save();
        this.modal.close();
    }

    private show(loanId: string, appId: string){
        this.load(loanId, appId, ["InitialEscrowAcc", "sRealETxRsrvMonRecommended", "sHazInsRsrvMonRecommended", "sMInsRsrvMonRecommended", "sFloodInsRsrvMonRecommended", "sSchoolTxRsrvMonRecommended", "s1006RsrvMonRecommended", "s1007RsrvMonRecommended", "sU3RsrvMonRecommended", "sU4RsrvMonRecommended", "sSchedDueD1Lckd", "sSchedDueD1"]);
        this.service.pipeline.load(["broker.EnableAdditionalSection1000CustomFees"])
            .then((dm: IDataModelsRaw) => this.pipeline = new PipelineModels(dm));

        this.modal.setOption({ backdrop:"static", keyboard:false, });
        this.modal.show();
    }
}

import * as initialEscrowAccSetupModalHtml from "./InitialEscrowAccSetup.html";

export const initialEscrowAccSetupModal: ng.IComponentOptions = {
    ngName         : "initialEscrowAccSetupModal",
    bindings       : {
        ref        : "&"
    },
    controllerAs   : "vm",
    template       : initialEscrowAccSetupModalHtml,
    controller     : InitialEscrowAccSetupController as any,
};
