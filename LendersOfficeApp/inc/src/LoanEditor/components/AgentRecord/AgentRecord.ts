import {service                        } from "../../ng-common/ngServices";
import {IDialog                        } from "../../../ng-common/dialog";
import {LqbInputType, E_CalculationType, isValidLqbInputModel} from "./../../ng-common/ILqbDataModel";
import {IAgentRecord, DataModels       } from "./../../ng-common/DataModels";
import {Permission                     } from "../../../DataLayer/Enums/Permission";
import {E_sClosingCostFeeVersionT      } from "../../../DataLayer/Enums/E_sClosingCostFeeVersionT";

export class AgentRecordController {
    loan:DataModels;
    private ref       : (_: {$ref  : AgentRecordController}) => void;

    CommisionInfoVisible:boolean;
    IsShowAffilates:boolean;
    IsAllowToggleOverride:boolean;
    static $inject = ["dialog"]
    constructor(private dialog: IDialog) {
        let data = {
            permission: Permission.AllowAccountantRead
        }
        service.agents.hasPermission(data).then((response:boolean)=>{
            this.CommisionInfoVisible = response;
        });
        data = {
            permission: Permission.AllowOverridingContactLicenses
        }
        service.agents.hasPermission(data).then((response:boolean)=>{
            this.IsAllowToggleOverride = response;
        });
    }
    private $onInit() {
        if (!!this.ref) this.ref({ $ref: this }); else debugger;
    }
    public load(loan:DataModels){
        this.loan = loan;

        this.IsShowAffilates = this.loan.BrokerDB.IsEnableGfe2015 && (this.loan.fields["sClosingCostFeeVersionT"].v != E_sClosingCostFeeVersionT.Legacy);

        this.refreshUI();
        loan.on("postCalc",() => {
            this.refreshUI();
        });
    }
    private f_sendEmail(){
        var email = String(this.loan.AgentRecord.EmailAddr.v);
        email = email.replace(/&/i, "%26");
        window.open('mailto:' + email);
        return false;
    }
    private isInvalidEmail(){
        if(this.loan == null) return false;
        return !isValidLqbInputModel(this.loan.AgentRecord.EmailAddr,"vm.loan.AgentRecord.EmailAddr");
    }
    private refreshUI(){
        this.loan.fields["sSpState"].calcAction = E_CalculationType.NoCalculate;
        this.loan.fields["sSpState"].r = true;
        this.loan.fields["sSpState"].t = LqbInputType.Text;
        this.loan.fields["sSpState"].options = null;
        this.ToggleEnableChildInputCtrls();
        this.setLicenseUI();
    }

    private ToggleEnableChildInputCtrls(){
        let disable = !Boolean(this.loan.AgentRecord.IsListedInGFEProviderForm.v);
        this.loan.AgentRecord.ProviderItemNumber.r = disable;
        this.loan.AgentRecord.IsLenderAssociation.r = disable;

        this.loan.AgentRecord.IsLenderRelative.r = disable;
        this.loan.AgentRecord.HasLenderRelationship.r = disable;
        this.loan.AgentRecord.HasLenderAccountLast12Months.r = disable;
        this.loan.AgentRecord.IsUsedRepeatlyByLenderLast12Months.r = disable;
        if(!this.IsShowAffilates){
            this.loan.AgentRecord.IsLenderAffiliate.r = disable;
        }

        if(disable){
            this.loan.AgentRecord.IsLenderAssociation.v =
                this.loan.AgentRecord.IsLenderRelative.v =
                this.loan.AgentRecord.HasLenderRelationship.v =
                this.loan.AgentRecord.HasLenderAccountLast12Months.v =
                this.loan.AgentRecord.IsUsedRepeatlyByLenderLast12Months.v = !disable;
            if(!this.IsShowAffilates){
                this.loan.AgentRecord.IsLenderAffiliate.v = !disable;
            }
        }
    }

    private setLicenseUI(){
        let bToggle = Boolean(this.loan.AgentRecord.OverrideLicenses.v);
        this.loan.AgentRecord.LicenseNumOfAgent.r = !bToggle;
        this.loan.AgentRecord.LicenseNumOfCompany.r = !bToggle;
    }
    private save(){
        this.loan.save().then(()=>{
            this.dialog.alert("Done","Agent Record");
        })
    }
}

import * as agentRecordHtml from "./AgentRecord.html";


export const agentRecord: ng.IComponentOptions = {
    ngName        : "agentRecord",
    bindings      : {
        ref       : "&",
    },
    controllerAs  : "vm",
    template      : agentRecordHtml,
    controller    : AgentRecordController as any,
};
