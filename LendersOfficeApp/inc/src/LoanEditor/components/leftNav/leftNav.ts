import {IAngularEvent, IComponentOptions, IOnChangesObject, IRootScopeService} from "angular";
import {State, StateService} from "angular-ui-router";
import {INiceScrollOptions, INiceScrollObject} from "../../../dt-local/jquery.nicescroll/index.d";

import {sublMatch               } from "../../../utils/sublMatch";
import {IPage, NavTree} from "../../ng-common/service/NavigationService";

import * as appHtml from "./index.html";


export class LeftNavController {
    // These props is binding by angular
    navTree           : NavTree;
    onGoBack          : (                      ) => void;
    onGoForward       : (                      ) => void;
    onGoToPage        : (params:{ $route: string}) => void;
    isShowMd          : boolean;
    isShowSm          : boolean;
    hideSaveMenuLayout: boolean;

    private nicescrollOption: INiceScrollOptions = {
        cursorcolor : "rgba(255, 255, 255, .13)",
        cursorborder: "rgba(255, 255, 255, .13)",
        autohidemode: "leave",
        cursorwidth : ".5rem",
    };

    private eleContent: HTMLDivElement;

    static $inject = ["$rootScope", "$element", "$state", "$scope"];
    constructor($rootScope:IRootScopeService,
                private $element: JQuery,
                private $state:StateService,
                $scope: any
    ){
        this.searchQuery = "";

        $scope.$on("dragulardrop", (event: JQueryEventObject, el: HTMLLIElement) => {
            const $el = $(el);
            const $next = $el.next();
            const nextId = $next.length > 0 ? $next.data("page-id") : "";

            this.navTree.favoriteMove($el.data("page-id"), nextId);
        });

        setTimeout(() => { this.setContainerDimensions() }, 0);
        $(window).resize(() => { this.setContainerDimensions() });

        return this;
    }

    // private $onInit(){}
    private $onChanges(changesObj: IOnChangesObject){
        const {isShowMd, isShowSm} = changesObj;
        console.log("leftNav.$onChanges", changesObj);
        if (isShowMd != null) {
            this.toggleSidebarNav(isShowMd.currentValue);
        }
        if (isShowSm != null) {
            this.toggleSidebarNavM(isShowSm.currentValue);
        }
    }
    // private $doCheck(){}
    // private $onDestroy(){}
    // private $postLink(){}


    private searchQuery: string;
    private isSearchQueryOpened = false;
    private onSearchQueryOpenClose(isOpen: boolean) {
        this.isSearchQueryOpened = isOpen;
    }
    private searchFilterComparator(x:string, y:string){
        return (typeof x === "string" && typeof y === "string" && sublMatch(y, x));
    }


    private navClick(page: IPage) {
        if (page.Children != null) {
            page.IsExpand = !page.IsExpand;
        } else {
            this.goToPage(page);
        }
    }
    private goToPage(page: IPage){
        this.onGoToPage({$route: page.Route});
    }


    private sidebarNavigation: JQuery;
    private set_sidebarNavigation(sidebarNavigation: HTMLDivElement) {
        this.sidebarNavigation = $(sidebarNavigation);
    }
    private sidebarNavNicescroll: INiceScrollObject;
    private setContainerDimensions() {
        const sideBar = this.sidebarNavigation;
        if (sideBar == null) return;

        if ((sideBar.is(":visible") && sideBar.css("visibility") != "collapse")
            || sideBar.hasClass("override"))
        {
            this.sidebarNavNicescroll.show();
            setTimeout(() => {
                this.sidebarNavNicescroll.resize();
            }, 300);
        } else this.sidebarNavNicescroll.hide();
    }
    private toggleSidebarNavM(isShow:boolean) {
        const nav = this.sidebarNavigation; if (nav == null) return;
        // console.assert(isShow == !nav.hasClass("override"));
        nav.toggleClass("override in height", isShow);

        setTimeout(() => {
            this.setContainerDimensions();

            // hide the navigation when clicked outside
            if (isShow) {
                $(document).on('click.sidebarNav', (event: JQueryEventObject) => {
                    if ($(event.target).closest(nav).length > 0)  return; // will not hide when we click inside the sidebarNav
                    nav.removeClass('override');
                    $(document).off('click.sidebarNav');
                });
            }
        }, 10);
    }
    private toggleSidebarNav(isShow:boolean) {
        const nav = this.sidebarNavigation; if (nav == null) return;
        nav.removeClass("override"); // remove `override` was set by `toggleSidebarNavM`

        nav.collapse(isShow ? "show" : "hide");

        setTimeout(() => {
            this.setContainerDimensions()
        }, 10);
    }
}

export const leftNavComponent: IComponentOptions = {
    ngName                : "leftNav",
    bindings              : {
        navTree           : "<",
        onGoBack          : "&",
        onGoForward       : "&",
        onGoToPage        : "&",
        isShowMd          : "<",
        isShowSm          : "<",
        hideSaveMenuLayout: "<",
        // @ string
    },
    controller            : LeftNavController as any,
    controllerAs          : "vm",
    template              : appHtml,
};
