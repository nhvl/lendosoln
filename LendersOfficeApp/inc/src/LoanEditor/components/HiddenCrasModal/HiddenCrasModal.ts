
import {service                             } from "../../ng-common/ngServices";
import {LqModalController,ModalStatus       } from "../lqModal";
import {PipelineEditorController            } from "./../../../Pipeline/ng-common/PipelineEditorController";

export class HiddenCrasModalController extends PipelineEditorController{
    public vendors: any[] = [];
    public didChoose: {(name: string): void}
    private stateOption: number = 0;

    private filteredEmployee: any[] = [];
    private modal: LqModalController;

    protected fieldList(): string[] { return ["HiddenCras"] };

    protected $postLink() { }

    show() {
        super.$postLink();
        this.modal.setOption({ backdrop:"static", keyboard:false, });
        this.modal.showWithOKCancel(
            ((promiseValue) =>{
                switch(promiseValue){
                    case ModalStatus.Save:
                        this.done();
                        break;
                    case ModalStatus.Discard:
                        this.cancel();
                        break;
                }
            }), (() => {
                return this.pipeline.isDirty();
            })
        );
    }

    addToHiddenCras(){
        const craList = this.pipeline.CraList;
        const selectedOption = craList.options.find(item => item.v === craList.v);
        this.pipeline.HiddenCras.push({Key: selectedOption.v, Value:selectedOption.l});

        this.pipeline.calculate();
    }

    removeFromHiddenCras(index: number){
        this.pipeline.HiddenCras.splice(index, 1);
        this.pipeline.calculate();
    }

    done(){
        this.pipeline.save();
        this.modal.close();
    }

    cancel(){
        this.modal.close();
    }
}

export const hiddenCrasModal: ng.IComponentOptions = {
    ngName        : "hiddenCrasModal",
    bindings      : {
        ref       : "&",
    },
    controllerAs  : "vm",
    template      : require("./HiddenCrasModal.html"),
    controller    : HiddenCrasModalController as any,
};
