/*
The `lqModal` component for showing a modal.

API:

    - Event:
        - `ref`: call when `lqModal` initialized. It past `$ref` as it self.
            Component should use this event to get the reference of the modal.
        - `onShow`
        - `onShown`
        - `onHide`
        - `onHidden`

    - Methods: public methods in `LqModalController` class

Example

```html
<lq-modal
    ref="vm.aModal = $ref"
    hide-header="false"
    hide-footer="false"
    on-show="vm.onModalShow($event)",
    on-shown="vm.onModalShown($event)",
    on-hide="vm.onModalHide($event)"
    on-hidden="vm.onModalHidden($event)",
>
    <slot-heading>
        Modal title
    </slot-heading>
    <slot-body>
        Modal body
    </slot-body>
    <slot-footer>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" ng-click="vm.aModal.close()">Close</button>
    </slot-footer>
</lq-modal>
```

*/
import {IDialog         } from "./../../../ng-common/dialog";

export class LqModalController {
    private $modal: JQuery;
    private hideHeader: boolean;
    private hideFooter: boolean;
    private ref       : (_: {$ref  : LqModalController}) => void;
    private onShow    : (_: {$event: JQueryEventObject}) => void;
    private onShown   : (_: {$event: JQueryEventObject}) => void;
    private onHide    : (_: {$event: JQueryEventObject}) => void;
    private onHidden  : (_: {$event: JQueryEventObject}) => void;
    private isDirty   : () => boolean;
    private applyState: (status: ModalStatus) => void;
    private $footer   : JQuery;
    private dialog    : IDialog;

    static $inject = ["$element", "dialog"];
    constructor($element: ng.IAugmentedJQuery, _dialog: IDialog) {
        this.hideHeader = false;
        this.hideFooter = false;
        this.dialog = _dialog;
        const $modal = this.$modal = $element.find("div.modal");
        this.$footer = $element.find("div.modal-footer");

        $modal.modal({ show: false, });
        $modal.on("show.bs.modal"  , (e: JQueryEventObject) => { if (e.namespace === 'bs.modal') this.onShow  ({$event: e}); });
        $modal.on("shown.bs.modal" , (e: JQueryEventObject) => { if (e.namespace === 'bs.modal') this.onShown ({$event: e}); });
        $modal.on("hide.bs.modal"  , (e: JQueryEventObject) => { if (e.namespace === 'bs.modal') this.onHide  ({$event: e}); });
        $modal.on("hidden.bs.modal", (e: JQueryEventObject) => { if (e.namespace === 'bs.modal') this.onHidden({$event: e}); });

        if (!this.isDirty) {
            this.isDirty = () => {return false;};
        }
    }

    private $onInit() {
        if (!!this.ref) this.ref({ $ref: this }); else debugger;
    }

    // private $onChanges() {}
    // private $onDestroy() {}

    show(){
        this.$modal.modal("show");
    }

    showWithOKCancel(completion: ((status: ModalStatus) => void), isDirtyCheck:(() => boolean)) {
        this.$footer.html(
            '<p class="text-center">' +
            '<span class="clipper"><button type="button" class="btn btn-default" data-index="2">OK</button></span>' +
            '<span class="clipper"><button type="button" class="btn btn-default" data-index="1">Cancel</button></span>' +
            '</p>'
        );

        this.isDirty = isDirtyCheck;
        this.applyState = completion;
        this.$footer.on("click", "button.btn", (e: JQueryEventObject) => {
            const v: ModalStatus = Number((e.target as HTMLElement).dataset["index"]);
            if (this.applyState != null){
                this.applyState(v);
            }
        });

        return this.show();
    }

    showWithOKCancelApply(completion: ((status: ModalStatus) => void), isDirtyCheck:(() => boolean)) {
        this.$footer.html(
            '<p class="text-center">' +
            '<span class="clipper"><button type="button" class="btn btn-default" data-index="2">OK</button></span>' +
            '<span class="clipper"><button type="button" class="btn btn-default" data-index="1">Cancel</button></span>' +
            '<span class="clipper"><button type="button" class="btn btn-default" data-index="3">Apply</button></span>' +
            '</p>'
        );

        this.isDirty = isDirtyCheck;
        this.applyState = completion;
        this.$footer.on("click", "button.btn", (e: JQueryEventObject) => {
            const v: ModalStatus = Number((e.target as HTMLElement).dataset["index"]);
            this.applyState(v);
        });

        return this.show();
    }
    
    close(){
        this.$modal.modal("hide");
        this.applyState = null;
        this.isDirty = null;
    }

    askClose() {
        if (this.isDirty && this.isDirty() && this.applyState ){
            const m = `Do you wish to save your changes before closing this popup?`;
            this.dialog.ync(m, "Save?").then((o) => {
                    switch (o) {
                        case 1:
                            this.applyState(ModalStatus.Save);
                            return;
                        case 0:
                            this.applyState(ModalStatus.Discard);
                            return;
                        case -1: 
                            return;
                        default:
                            console.error(o);
                            return;
                    }
                });
        } else {
            this.close();
        }
        
    }

    setOption(options: ModalOptions) {
        this.$modal.data('bs.modal').options = options;
    }
}

import template = require("./lqModal.html");

export const lqModal: ng.IComponentOptions = {
    ngName        : "lqModal",
    bindings      : {
        hideHeader: "<",
        hideFooter: "<",
        ref       : "&",
        onShow    : "&",
        onShown   : "&",
        onHide    : "&",
        onHidden  : "&",
        isDirty   : "&",
    },
    controllerAs  : "vm",
    transclude    : {
        heading   : "slotHeading",
        body      : "slotBody",
        footer    : "slotFooter",
    },
    template,
    controller    : LqModalController as any,
};

export enum ModalStatus {
    Save = 2, //close and save modal
    Discard = 1, //close and not save modal
    Open = 0, //not close (leave modal open) modal
    Apply = 3
}
