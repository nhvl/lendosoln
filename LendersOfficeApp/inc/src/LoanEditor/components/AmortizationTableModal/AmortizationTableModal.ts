import {service                        } from "../../ng-common/ngServices";
import {LqModalController, ModalStatus } from "../lqModal";
import {EditorController               } from "./../../ng-common/EditorController"

export class AmortizationTableModalController extends EditorController {

    private modal: LqModalController;
    private ref  : (_: {$ref  : AmortizationTableModalController}) => void;
    private currentTab: 'Standard' | 'BestCase' | 'WorstCase';

    private $onInit() {
        if (!!this.ref) this.ref({ $ref: this }); else debugger;
    }

    show(loanId: string, appId: string, isBorrower: boolean){
        this.load(loanId, appId, ["Amortization"]);

        this.modal.setOption({ backdrop:"static", keyboard:false, });
        this.modal.show();
    }

    print(){
        let getPdfFileName: (tab: 'Standard' | 'BestCase' | 'WorstCase') => string =  (x) => {
            switch(x){
                case 'BestCase':
                    return  "BestCaseAmortization";
                case 'WorstCase':
                    return "WorstCaseAmortization"
                default:
                    return "Amortization";
            }
        }

        const urlLink = '../newlos/Forms/pdf/' + getPdfFileName(this.currentTab) + ".aspx?loanid=" + this.loan.sLId + "&cmd=download";
        window.open(urlLink);
    }
}

import * as amortizationTableModalHtml from "./AmortizationTableModal.html";

export const amortizationTableModal: ng.IComponentOptions = {
    ngName        : "amortizationTableModal",
    bindings       : {
        ref        : "&",
    },
    controllerAs  : "vm",
    template      : amortizationTableModalHtml,
    controller    : AmortizationTableModalController as any,
};

