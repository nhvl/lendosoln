import {createBasicLoanEditorDirective} from "../ng-common/createBasicLoanEditorDirective";

import * as appHtml from "./index.html";

export const customFields3Directive = createBasicLoanEditorDirective("customFields3", appHtml);
