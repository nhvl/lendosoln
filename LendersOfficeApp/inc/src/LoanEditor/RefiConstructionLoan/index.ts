import {EditorController              } from "../ng-common/EditorController";
import {createBasicLoanEditorDirective} from "../ng-common/createBasicLoanEditorDirective";
import {E_sLPurposeT} from "../../DataLayer/Enums/E_sLPurposeT";

import * as appHtml from "./index.html";

class RefiConstructionLoanController extends EditorController {
    private E_sLPurposeT = E_sLPurposeT;
    // protected init() { }
}
export const refiConstructionLoanDirective = createBasicLoanEditorDirective("refiConstructionLoan", appHtml, RefiConstructionLoanController);
