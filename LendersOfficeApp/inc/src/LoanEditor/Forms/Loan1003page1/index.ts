import {createBasicLoanEditorDirective  } from "../../ng-common/createBasicLoanEditorDirective";
import {EditorController                } from "../../ng-common/EditorController";

import * as appHtml from "./index.html";

export class Loan1003page1Controller extends EditorController {
}

export const loan1003page1Directive = createBasicLoanEditorDirective("loan1003page1", appHtml, Loan1003page1Controller);