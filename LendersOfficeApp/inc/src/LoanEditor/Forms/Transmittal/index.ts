import {EditorController              } from "../../ng-common/EditorController";
import {createBasicLoanEditorDirective} from "../../ng-common/createBasicLoanEditorDirective";
import {DataModels                    } from "../../ng-common/DataModels";
import {$timeout                      } from "../../ng-common/ngServices";
import {E_sLienPosT                   } from "../../../DataLayer/Enums/E_sLienPosT";

import * as appHtml from "./index.html";

class TransmittalController extends EditorController {
    onLoaded(loan: DataModels): boolean {
        super.onLoaded(loan);
        $timeout(() => {
            this.refreshUI();
            this.loan.on("postCalc",() => {
                this.refreshUI();
            });
        });

        return true;
    }
    refreshUI(){
        if(this.loan == null) return;
        this.on_sFinMethodPrintAsOtherClick();
        this.loan.fields["sFinMethT"].r = Boolean(this.loan.fields["sIsRateLocked"].v);
        this.loan.fields["sNoteIR"].r = Boolean(this.loan.fields["sIsRateLocked"].v);
        this.loan.fields["sTerm"].r = Boolean(this.loan.fields["sIsRateLocked"].v);
        this.loan.fields["sTransmBuydwnTermDesc"].r = (String(this.loan.fields["sBuydown"].v) == "No");

        var isFirstLien = this.loan.fields["sLienPosT"].v== E_sLienPosT.First;

        //this.loan.fields["sSubFin"].r=!isFirstLien;
        this.loan.fields["s1stMOwnerT"].r = isFirstLien;
        this.loan.fields["s1stMtgOrigLAmt"].r= isFirstLien;
    }
    on_sFinMethodPrintAsOtherClick(){
        if(this.loan.fields["sFinMethodPrintAsOther"].r) return;
        if(Boolean(this.loan.fields["sFinMethodPrintAsOther"].v)){
            this.loan.fields["sFinMethPrintAsOtherDesc"].r = false;
        }
        else{
            this.loan.fields["sFinMethPrintAsOtherDesc"].r = true;
            this.loan.fields["sFinMethPrintAsOtherDesc"].v = "";
        }
    }
    fromAsset(){
        const data= {
            loanId: String(this.loan.sLId)
        }
        this.service.transmittal.calculateAssetTotal(data).then((response:number) => {
            this.loan.fields["sVerifAssetAmt"].v = response;
        }).catch((e: any) => {
            console.log(e);
            this.dialog.alert("An error occured");
        })
    }
}
export const transmittalDirective = createBasicLoanEditorDirective("transmittal", appHtml,TransmittalController);
