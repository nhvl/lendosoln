import {createBasicLoanEditorDirective} from "../../ng-common/createBasicLoanEditorDirective";
import {EditorController              } from "../../ng-common/EditorController";
import {DataModels                    } from "../../ng-common/DataModels";
import {$timeout                      } from "../../ng-common/ngServices";

import * as appHtml from "./index.html";

class GoodbyeLetterController extends EditorController {
    isFirstLoad:boolean;
    currentsGLNewServNm:any;
    init(){
        this.isFirstLoad = true;
    }
    onLoaded(loan: DataModels): boolean {
        super.onLoaded(loan);
        $timeout(()=>{
            if(this.isFirstLoad){
                if(this.loan["preparerFields"]["GoodbyeLetter"]["PreparerName"].v == ""){
                    this.loan["preparerFields"]["GoodbyeLetter"]["PreparerName"].v = this.loan.BrokerUser.firstName + " "+this.loan.BrokerUser.lastName;
                }
                this.isFirstLoad = false;
            }
            $(`lqb-input[lqb-model="vm.loan.fields.sGLNewServAccPmtD"] input`).on("change", () => {
                $timeout(() => {
                    const data = {
                        loanId: this.loan.sLId,
                        acceptingD: String(this.loan.fields["sGLNewServAccPmtD"].v)
                    };
                        this.service.goodbyeLetter.getSchedDateOfFirstPmt(data).then((response:any)=>{
                            if(response!=null && response!=""){
                                this.loan.fields["sGLNewServFirstPmtSchedD"].v = response;
                            }
                        })
                 });
            });
            $(`lqb-input[lqb-model="vm.loan.fields.sGLServTransEffD"] input`).on("change", () => {
                this.loan.fields["sGLNewServAccPmtD"].v = this.loan.fields["sGLServTransEffD"].v;
            });

            this.loan.on("postCalc", () => {
                this.postCalcEvent();
            });
            this.postCalcEvent();

            $(`lqb-input[lqb-model="vm.loan.fields.sGLNewServNm"] input`).on("change", () => {
                this.f_populateInvestorInfo();
            });
        });
        return true;
    }
    postCalcEvent(){
        if(this.currentsGLNewServNm != null && this.loan.fields["sGLNewServNm"].v != this.currentsGLNewServNm){
            this.f_populateInvestorInfo();
        }
        this.currentsGLNewServNm = this.loan.fields["sGLNewServNm"].v;
    }
    f_populateInvestorInfo(){
        const o = this.loan.fields["sGLNewServNm"].options.find(item => item.v === this.loan.fields["sGLNewServNm"].v);
        if(o == null) return;
        const data = {
            id: Number(o.l)
        };
        this.service.goodbyeLetter.getInvestorInfo(data).then((response:any)=>{
            this.loan.fields["sGLNewServAddr"].v=response["MainAddress"];
            this.loan.fields["sGLNewServCity"].v=response["MainCity"];
            this.loan.fields["sGLNewServState"].v=response["MainState"];
            this.loan.fields["sGLNewServZip"].v=response["MainZip"];
            this.loan.fields["sGLNewServTFPhone"].v=response["MainPhone"];
            this.loan.fields["sGLNewServContactNm"].v=response["MainContactName"];
            this.loan.fields["sGLNewServContactNum"].v=response["MainPhone"];
            this.loan.fields["sGLNewServPmtAddr"].v=response["PaymentToAddress"];
            this.loan.fields["sGLNewServPmtCity"].v=response["PaymentToCity"];
            this.loan.fields["sGLNewServPmtState"].v=response["PaymentToState"];
            this.loan.fields["sGLNewServPmtZip"].v=response["PaymentToZip"];
        });
    }
}
export const goodbyeLetterDirective = createBasicLoanEditorDirective("goodbyeLetter", appHtml, GoodbyeLetterController);
