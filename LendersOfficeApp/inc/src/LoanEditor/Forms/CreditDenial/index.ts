import {createBasicLoanEditorDirective} from "../../ng-common/createBasicLoanEditorDirective";
import {EditorController              } from "../../ng-common/EditorController";
import {DataModels,IPreparerFields    } from "../../ng-common/DataModels";
import {$timeout                      } from "../../ng-common/ngServices";
import {toastrOptions                 } from "../../ng-common/toastrOptions";
import {E_aDecisionCreditSourceT      } from "../../../DataLayer/Enums/E_aDecisionCreditSourceT";

import * as appHtml from "./index.html";
class CreditDenialController extends EditorController {
    IsBorr:boolean;
    isShowDeniedWarning:boolean;
    init(){
        this.IsBorr = true;
        this.isShowDeniedWarning = true;
    }
    onLoaded(loan: DataModels): boolean {
        super.onLoaded(loan);
        $timeout(() => {
            // $(`lqb-input[lqb-model="vm.loan.fields.sRejectD"] input`).on("focus", () => {
            //     $timeout(() => { this.showDeniedWarning(); });
            // });
            $timeout(() => {
            $(`lqb-input[lqb-model="vm.loan.fields.sRejectD"] input`).on("focus", () => {
                    $timeout(() => { this.isShowDeniedWarning = true; });
                });
            });
            $timeout(() => {
                $(`lqb-input[lqb-model="vm.loan.fields.sRejectD"] input`).on("blur", () => {
                    $timeout(() => { this.isShowDeniedWarning = false; });
                });
            });
            this.refreshUI();
            this.loan.on("postCalc",() => {
                this.refreshUI();
            });
            //this.showDeniedWarning();
        });
        return true;
    }
    tabClick(isBorr:boolean){
        this.IsBorr = isBorr;
        this.refreshUI();
    }
    refreshUI(){
        if(this.loan == null) return;
        let isManually:boolean;
        if(this.IsBorr){
            isManually = Boolean(this.loan.app.fields["aDenialCreditScoreLckd"].v);
            if(this.loan.app.fields["aDenialCreditScoreD"] != null) {
                $(`lqb-input[lqb-model^="vm.loan.app.fields.aDenialCreditScore"] input`).prop("readonly",!isManually);
                $(`lqb-input[lqb-model^="vm.loan.app.fields.aDenialCreditScoreD"] span.input-group-addon`).toggleClass("disabled",!isManually);
            }
        }
        else{
            isManually = Boolean(this.loan.app.fields["aCDenialCreditScoreLckd"].v);
            if(this.loan.app.fields["aCDenialCreditScoreD"] != null) {
                $(`lqb-input[lqb-model^="vm.loan.app.fields.aCDenialCreditScore"] input`).prop("readonly",!isManually);
                $(`lqb-input[lqb-model^="vm.loan.app.fields.aCDenialCreditScoreD"] span.input-group-addon`).toggleClass("disabled",!isManually);
            }
        }
        $(`lqb-input[lqb-model^="vm.loan.preparerFields.CreditDenialScoreContact"] input`).prop("readonly",!isManually);
        $(`lqb-input[lqb-model="vm.loan.preparerFields.CreditDenialScoreContact.State"] div.dropdown-input`).toggleClass("disabled",!isManually);
        $(`lqb-input[lqb-model="vm.loan.preparerFields.CreditDenialScoreContact.State"] button`).prop("disabled",!isManually);

        if(!isManually){
            switch (this.loan.app.fields["aBDecisionCreditSourceT"].v) {
                case E_aDecisionCreditSourceT.Equifax:
                    this.setCreditDenialScoreContact(this.loan.preparerFields["CreditEquifax"]);
                    break;
                case E_aDecisionCreditSourceT.Experian:
                    this.setCreditDenialScoreContact(this.loan.preparerFields["CreditExperian"]);
                    break;
                case E_aDecisionCreditSourceT.TransUnion:
                    this.setCreditDenialScoreContact(this.loan.preparerFields["CreditTransUnion"]);
                    break;
                default:
                    // do nothing
                    break;
            }
        }
    }
    setCreditDenialScoreContact(preparer: IPreparerFields){
        this.loan.preparerFields["CreditDenialScoreContact"].CompanyName.v = preparer.CompanyName.v;
        this.loan.preparerFields["CreditDenialScoreContact"].StreetAddr.v = preparer.StreetAddr.v;
        this.loan.preparerFields["CreditDenialScoreContact"].City.v = preparer.City.v;
        this.loan.preparerFields["CreditDenialScoreContact"].State.v = preparer.State.v;
        this.loan.preparerFields["CreditDenialScoreContact"].Zip.v = preparer.Zip.v;
        this.loan.preparerFields["CreditDenialScoreContact"].PhoneOfCompany.v = preparer.PhoneOfCompany.v;
    }
    showDeniedWarning(){
        toastr.warning("Modifying the denied date could remove your permission to edit this loan. Please make sure you complete the HMDA form before you enter a denied date here.","",toastrOptions);
    }
    copyFromBorrower(){
        if(this.loan==null) return;
            this.loan.app.fields["aCDenialNoCreditFile"].v=this.loan.app.fields["aDenialNoCreditFile"].v;
            this.loan.app.fields["aCDenialInsufficientCreditRef"].v=this.loan.app.fields["aDenialInsufficientCreditRef"].v;
            this.loan.app.fields["aCDenialInsufficientCreditFile"].v=this.loan.app.fields["aDenialInsufficientCreditFile"].v;
            this.loan.app.fields["aCDenialUnableVerifyCreditRef"].v=this.loan.app.fields["aDenialUnableVerifyCreditRef"].v;
            this.loan.app.fields["aCDenialGarnishment"].v=this.loan.app.fields["aDenialGarnishment"].v;
            this.loan.app.fields["aCDenialExcessiveObligations"].v=this.loan.app.fields["aDenialExcessiveObligations"].v;
            this.loan.app.fields["aCDenialInsufficientIncome"].v=this.loan.app.fields["aDenialInsufficientIncome"].v;
            this.loan.app.fields["aCDenialUnacceptablePmtRecord"].v=this.loan.app.fields["aDenialUnacceptablePmtRecord"].v;
            this.loan.app.fields["aCDenialLackOfCashReserves"].v=this.loan.app.fields["aDenialLackOfCashReserves"].v;
            this.loan.app.fields["aCDenialDeliquentCreditObligations"].v=this.loan.app.fields["aDenialDeliquentCreditObligations"].v;
            this.loan.app.fields["aCDenialBankruptcy"].v=this.loan.app.fields["aDenialBankruptcy"].v;
            this.loan.app.fields["aCDenialInfoFromConsumerReportAgency"].v=this.loan.app.fields["aDenialInfoFromConsumerReportAgency"].v;
            this.loan.app.fields["aCDenialUnableVerifyEmployment"].v=this.loan.app.fields["aDenialUnableVerifyEmployment"].v;
            this.loan.app.fields["aCDenialLenOfEmployment"].v=this.loan.app.fields["aDenialLenOfEmployment"].v;
            this.loan.app.fields["aCDenialTemporaryEmployment"].v=this.loan.app.fields["aDenialTemporaryEmployment"].v;
            this.loan.app.fields["aCDenialInsufficientIncomeForMortgagePmt"].v=this.loan.app.fields["aDenialInsufficientIncomeForMortgagePmt"].v;
            this.loan.app.fields["aCDenialUnableVerifyIncome"].v=this.loan.app.fields["aDenialUnableVerifyIncome"].v;
            this.loan.app.fields["aCDenialTempResidence"].v=this.loan.app.fields["aDenialTempResidence"].v;
            this.loan.app.fields["aCDenialShortResidencePeriod"].v=this.loan.app.fields["aDenialShortResidencePeriod"].v;
            this.loan.app.fields["aCDenialUnableVerifyResidence"].v=this.loan.app.fields["aDenialUnableVerifyResidence"].v;
            this.loan.app.fields["aCDenialByHUD"].v=this.loan.app.fields["aDenialByHUD"].v;
            this.loan.app.fields["aCDenialByVA"].v=this.loan.app.fields["aDenialByVA"].v;
            this.loan.app.fields["aCDenialByFedNationalMortAssoc"].v=this.loan.app.fields["aDenialByFedNationalMortAssoc"].v;
            this.loan.app.fields["aCDenialByFedHomeLoanMortCorp"].v=this.loan.app.fields["aDenialByFedHomeLoanMortCorp"].v;
            this.loan.app.fields["aCDenialByOther"].v=this.loan.app.fields["aDenialByOther"].v;
            this.loan.app.fields["aCDenialByOtherDesc"].v=this.loan.app.fields["aDenialByOtherDesc"].v;
            this.loan.app.fields["aCDenialInsufficientFundsToClose"].v=this.loan.app.fields["aDenialInsufficientFundsToClose"].v;
            this.loan.app.fields["aCDenialCreditAppIncomplete"].v=this.loan.app.fields["aDenialCreditAppIncomplete"].v;
            this.loan.app.fields["aCDenialInadequateCollateral"].v=this.loan.app.fields["aDenialInadequateCollateral"].v;
            this.loan.app.fields["aCDenialUnacceptableProp"].v=this.loan.app.fields["aDenialUnacceptableProp"].v;
            this.loan.app.fields["aCDenialInsufficientPropData"].v=this.loan.app.fields["aDenialInsufficientPropData"].v;
            this.loan.app.fields["aCDenialUnacceptableAppraisal"].v=this.loan.app.fields["aDenialUnacceptableAppraisal"].v;
            this.loan.app.fields["aCDenialUnacceptableLeasehold"].v=this.loan.app.fields["aDenialUnacceptableLeasehold"].v;
            this.loan.app.fields["aCDenialNotGrantCreditToAnyAppOnSuchTermsAndConds"].v=this.loan.app.fields["aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds"].v;
            this.loan.app.fields["aCDenialWithdrawnByApp"].v=this.loan.app.fields["aDenialWithdrawnByApp"].v;
            this.loan.app.fields["aCDenialOtherReason1"].v=this.loan.app.fields["aDenialOtherReason1"].v;
            this.loan.app.fields["aCDenialOtherReason1Desc"].v=this.loan.app.fields["aDenialOtherReason1Desc"].v;
            this.loan.app.fields["aCDenialOtherReason2"].v=this.loan.app.fields["aDenialOtherReason2"].v;
            this.loan.app.fields["aCDenialOtherReason2Desc"].v=this.loan.app.fields["aDenialOtherReason2Desc"].v;
    }
}
export const creditDenialDirective = createBasicLoanEditorDirective("creditDenial", appHtml, CreditDenialController);
