import {createBasicLoanEditorDirective  } from "../../ng-common/createBasicLoanEditorDirective";
import {EditorController                } from "../../ng-common/EditorController";

import * as appHtml from "./index.html";

export class ChangeOfCircumstancesNewController extends EditorController {
    pageState: cocPageState = cocPageState.New;

    apply(){
        this.dialog.confirm("No fees were selected for this Change of Circumstance. New disclosures will reflect the fees from the previous disclosure.")
                    .then((isConfirm:boolean) => {
                        if (!isConfirm) return;
                        this.loan.calculate();
                    });
    }

    changeState(newPageState: cocPageState){
        if (newPageState == cocPageState.Past && this.loan.fields["cocArchivesSelect"].options.length == 0){
            return;
        }

        this.pageState = newPageState;
    }

    print(){
        alert("Update soon");
    }
}

enum cocPageState {
    New = 0,
    Past = 1,
}

export const changeOfCircumstancesNewDirective = createBasicLoanEditorDirective("changeOfCircumstancesNewDirective", appHtml, ChangeOfCircumstancesNewController);