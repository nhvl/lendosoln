import {createBasicLoanEditorDirective  } from "../../ng-common/createBasicLoanEditorDirective";
import {EditorController                } from "../../ng-common/EditorController";

import * as appHtml from "./index.html";

export class LoanCommitmentController extends EditorController {
}

export const loanCommitmentDirective = createBasicLoanEditorDirective("loanCommitmentDirective", appHtml, LoanCommitmentController);