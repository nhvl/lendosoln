import {createBasicLoanEditorDirective  } from "../../ng-common/createBasicLoanEditorDirective";
import {EditorController                } from "../../ng-common/EditorController";

import * as appHtml from "./index.html";

export class LoanTermsController extends EditorController {
}

export const loanTermsDirective = createBasicLoanEditorDirective("loanTermsDirective", appHtml, LoanTermsController);