import {createBasicLoanEditorDirective  } from "../../ng-common/createBasicLoanEditorDirective";
import {EditorController                } from "../../ng-common/EditorController";
import {InitialEscrowAccSetupController } from "./../../components/InitialEscrowAccSetup/InitialEscrowAccSetup"
import {ServiceFactory                  } from  "./../../ng-common/service";
import {IDataModelsRaw, PipelineModels  } from  "./../../../Pipeline/ng-common/PipelineModels";

import * as appHtml from "./index.html";

let service = ServiceFactory;
export class EscrowImpoundsSetupController extends EditorController {
    private feePicker: InitialEscrowAccSetupController;
    private pipeline: PipelineModels;

    load(sLId: string, appId: string, fieldList:string[]) {
        super.load(sLId, appId, fieldList);

        this.service.pipeline.load(["broker.EnableAdditionalSection1000CustomFees"])
            .then((dm: IDataModelsRaw) => this.pipeline = new PipelineModels(dm));
    }
}

export const escrowImpoundsSetupDirective = createBasicLoanEditorDirective("escrowImpoundsSetupDirective", appHtml, EscrowImpoundsSetupController);