import {createBasicLoanEditorDirective  } from "../../ng-common/createBasicLoanEditorDirective";
import {EditorController                } from "../../ng-common/EditorController";
import {AmortizationTableModalController} from "./../../components/AmortizationTableModal/AmortizationTableModal";
import {LoanTemplatePopulatorController } from "./../../components/LoanTemplatePopulator";
import {RateFloorCalculatorController   } from "./../../components/RateFloorCalculator";
import {ILoanFields                     } from "./../../ng-common/DataModels";
import {updateFromRaw                   } from "./../../ng-common/ILqbDataModel";
import {IDataModelsRaw, PipelineModels  } from  "./../../../Pipeline/ng-common/PipelineModels";
import {DataModels                      } from "./../../ng-common/DataModels"

import * as appHtml from "./index.html";

export class TruthInLendingController extends EditorController {
    amortizationModal: AmortizationTableModalController;
    loanTemplatePopulator: LoanTemplatePopulatorController;
    rateFloorCalculator : RateFloorCalculatorController;
    private pipeline: PipelineModels;

    populateFromTemplate(templateId: string){
        this.loan.populate(templateId);
    }

    showRateFloorCalculator(){
        this.rateFloorCalculator.show(this.loan);
    }

    updateRateFloor(loanFields: ILoanFields){
        updateFromRaw(loanFields, this.loan.fields, null);
        this.loan.calculate();
    }

    protected onLoaded(loan:DataModels) : boolean {
        this.service.pipeline.load(["broker.IsRemovePreparedDates"]);
        return super.onLoaded(loan);
    }
}


export const truthInLendingDirective = createBasicLoanEditorDirective("truthInLendingDirective", appHtml, TruthInLendingController);