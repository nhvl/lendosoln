import {EditorController              } from "../../ng-common/EditorController";
import {createBasicLoanEditorDirective} from "../../ng-common/createBasicLoanEditorDirective";
import {DataModels                    } from "../../ng-common/DataModels";
import {$stateParams,$timeout         } from "../../ng-common/ngServices";

import * as appHtml from "./index.html";

class TaxFormController extends EditorController {
    version:number;
    init(){
        if ($stateParams == null || $stateParams["version"] == null || $stateParams["version"] == "")
            this.version=(new Date()).getFullYear();
        else
            this.version = $stateParams["version"];
    }
    onLoaded(loan: DataModels): boolean {
    super.onLoaded(loan);
    $timeout(() => {
            this.refreshUI();
            this.loan.on("postCalc",() => {
                this.refreshUI();
            });
        });
        return true;
    }
    refreshUI(){
        let isLocked = Boolean(this.loan.preparerFields["Tax1098"].IsLocked.v);
        $(`lqb-input[lqb-model^="vm.loan.preparerFields.Tax1098"] input`).prop("readonly", !isLocked);

        $(`lqb-input[lqb-model="vm.loan.preparerFields.Tax1098.State"] div.dropdown-input`).toggleClass("disabled",!isLocked);
        $(`lqb-input[lqb-model="vm.loan.preparerFields.Tax1098.State"] button`).prop("disabled",!isLocked);

        $(`lqb-input[lqb-model="vm.loan.preparerFields.Tax1098.AgentRoleT"] div.dropdown-input`).toggleClass("disabled",isLocked);
        $(`lqb-input[lqb-model="vm.loan.preparerFields.Tax1098.AgentRoleT"] button`).prop("disabled",isLocked);
    }
    MipLabel(){
        if(this.version!=2016 && this.version!=2017){
            return "MIP";
        }
        return "MI Premiums";
    }
    OutstandingPrincipalLabel(){
        if(this.version!=2016 && this.version!=2017){
            return "Outstanding principal as of";
        }
        return "Outstanding principal as of 1/1/"+this.version;
    }
}
export const taxFormDirective = createBasicLoanEditorDirective("taxForm", appHtml, TaxFormController);
