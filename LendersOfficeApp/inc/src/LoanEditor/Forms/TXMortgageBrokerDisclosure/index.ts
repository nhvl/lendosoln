import {createBasicLoanEditorDirective  } from "../../ng-common/createBasicLoanEditorDirective";
import {EditorController                } from "../../ng-common/EditorController";

import * as appHtml from "./index.html";

export class TxMortgageBrokerDisclosureController extends EditorController {
}

export const txMortgageBrokerDisclosureDirective = createBasicLoanEditorDirective("txMortgageBrokerDisclosureDirective", appHtml, TxMortgageBrokerDisclosureController);