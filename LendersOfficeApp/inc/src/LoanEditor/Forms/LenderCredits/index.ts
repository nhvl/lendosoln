import {EditorController              } from "../../ng-common/EditorController";
import {createBasicLoanEditorDirective} from "../../ng-common/createBasicLoanEditorDirective";
import {E_sDisclosureRegulationT      } from "../../../DataLayer/Enums/E_sDisclosureRegulationT";
import {E_sClosingCostFeeVersionT     } from "../../../DataLayer/Enums/E_sClosingCostFeeVersionT";
import {E_CreditLenderPaidItemT       } from "../../../DataLayer/Enums/E_CreditLenderPaidItemT";
import {$timeout                      } from "../../ng-common/ngServices";
import {DataModels                    } from "../../ng-common/DataModels";
import {E_CalculationType             } from "../../ng-common/ILqbDataModel";

import * as appHtml from "./index.html";

class LenderCreditsController extends EditorController {
    onLoaded(loan: DataModels): boolean {
        super.onLoaded(loan);
        $timeout(() => {
            this.refreshUI();
            this.loan.on("preCalc",() => {
                if(!this.loan.fields["sToleranceZeroPercentCure"].r)
                    this.loan.fields["sToleranceZeroPercentCure_Neg"].v = Number(this.loan.fields["sToleranceZeroPercentCure"].v)*(-1);
                if(!this.loan.fields["sToleranceTenPercentCure"].r)
                    this.loan.fields["sToleranceTenPercentCure_Neg"].v = Number(this.loan.fields["sToleranceTenPercentCure"].v)*(-1);
            });
            this.loan.on("postCalc",() => {
                this.refreshUI();
            });
        });

        return true;
    }
    refreshUI(){
        if(this.loan == null) return;
        if(this.isTRID2015()){
            let index = this.loan.fields["sGfeCreditLenderPaidItemT"].options.map(function (item) { return item.v; }).indexOf(E_CreditLenderPaidItemT.OriginatorCompensationOnly);
            if(index > -1){
                this.loan.fields["sGfeCreditLenderPaidItemT"].options.splice(index,1);
                let li = $(`lqb-input[lqb-model="vm.loan.fields.sGfeCreditLenderPaidItemT"] ul.dropdown-menu li:eq(`+index+`)`);
                if( li!=null && !li.hasClass("hidden")){
                    li.addClass("hidden");
                }
            }
        }
        if((this.loan.fields["sClosingCostFeeVersionT"].v == E_sClosingCostFeeVersionT.Legacy)
            && this.loan.BrokerDB.EnableLenderCreditsWhenInLegacyClosingCostMode){
            this.loan.fields["sLenderCreditCalculationMethodT"].r = true;
        }
        this.loan.fields["sLenderCreditMaxAmt_Neg"].r = (Number(this.loan.fields["sLenderCreditMaxT"].v) != 4);
        if(Number(this.loan.fields["sLenderCreditMaxT"].v) == 3){
            this.loan.fields["sLenderCreditMaxAmt_Neg"].v = null;
        }
    }
    seeTolerance(){
        //https://ext.dory.vn/newlos/ToleranceCure.aspx?loanid=2e350b69-ff1b-425e-85e6-a635015f63c0&appid=bb60dd0d-d693-404a-b1a7-a635015f668f
        let url = '../newlos/ToleranceCure.aspx?loanid=' + this.loan.sLId + '&appid='+ this.loan.app.fields["aappId"].v;
        window.open(url,'_blank');
    }
    hasOriginatorComp(){
        if(this.loan == null) return true;
        return this.loan.fields["sLenderPaidBrokerCompF"].v != 0;
    }
    isTRID2015(){
        if(this.loan == null) return false;
        return (this.loan.fields["sDisclosureRegulationT"].v == E_sDisclosureRegulationT.TRID2015);
    }
    isFrontEnd(){
        if(this.loan == null) return false;
        return this.loan.fields["sLenderCreditCalculationMethodT"].v;
    }
    isManualCalculate(){
        if(this.loan == null) return true;
        return !this.loan.fields["sLenderCreditCalculationMethodT"].v;
    }
    bDisplayManualToleranceCure(){
        if(this.loan == null) return true;
        return !this.loan.fields["sLenderCreditCalculationMethodT"].v
            && this.loan.fields["sToleranceCureCalculationT"].v;
    }
    isManualTolerance(){
        if(this.loan == null) return true;
        return this.bDisplayManualToleranceCure();
    }
    isManualSummary(){
        if(this.loan == null) return true;
        return !this.loan.fields["sLenderCreditCalculationMethodT"].v;
    }
    isShowTolerance(){
        if(this.loan == null) return false;
        return this.loan.fields["sIsAllowExcludeToleranceCure"].v;
    }
}
export const lenderCreditsDirective = createBasicLoanEditorDirective("lenderCredits", appHtml, LenderCreditsController);
