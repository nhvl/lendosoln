import {createBasicLoanEditorDirective  } from "../../ng-common/createBasicLoanEditorDirective";
import {EditorController                } from "../../ng-common/EditorController";

import * as appHtml from "./index.html";

export class Loan1003page4Controller extends EditorController {
}

export const loan1003page4Directive = createBasicLoanEditorDirective("loan1003page4", appHtml, Loan1003page4Controller);