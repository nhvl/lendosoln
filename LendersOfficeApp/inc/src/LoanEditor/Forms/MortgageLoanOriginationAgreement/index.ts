import {createBasicLoanEditorDirective  } from "../../ng-common/createBasicLoanEditorDirective";
import {EditorController                } from "../../ng-common/EditorController";

import * as appHtml from "./index.html";

export class MortgageLoanOriginationAgreementController extends EditorController {
}

export const mortgageLoanOriginationAgreementDirective = createBasicLoanEditorDirective("mortgageLoanOriginationAgreementDirective", appHtml, MortgageLoanOriginationAgreementController);