import {createBasicLoanEditorDirective  } from "../../ng-common/createBasicLoanEditorDirective";
import {EditorController                } from "../../ng-common/EditorController";

import * as appHtml from "./index.html";

export class Loan1003page2Controller extends EditorController {
    addOtherIncome() {
        const {data, template} = this.loan.app.aOtherIncomeList;
        this.loan.app.aOtherIncomeList.data = data.concat([angular.merge({}, template)]);
        this.loan.calculate();
    }
    deleteOtherIncome(i: number) {
        // 8/20/2013 EM - If there are only 3 rows, and a row has all subsequent rows as blanks, disable the delete
        const {data} = this.loan.app.aOtherIncomeList;
        if (data.length < 4) return;
        const item = data[i];
        if (!!item.desc.v || (item.val.v > 0)) return;

        data.splice(i, 1);

        this.loan.calculate();
    }
}

export const loan1003page2Directive = createBasicLoanEditorDirective("loan1003page2", appHtml, Loan1003page2Controller);