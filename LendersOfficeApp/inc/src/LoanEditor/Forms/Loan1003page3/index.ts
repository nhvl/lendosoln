import {createBasicLoanEditorDirective  } from "../../ng-common/createBasicLoanEditorDirective";
import {EditorController                } from "../../ng-common/EditorController";

import * as appHtml from "./index.html";

export class Loan1003page3Controller extends EditorController {
}

export const loan1003page3Directive = createBasicLoanEditorDirective("loan1003page3", appHtml, Loan1003page3Controller);