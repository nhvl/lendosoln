import {EditorController              } from "../../ng-common/EditorController";
import {createBasicLoanEditorDirective} from "../../ng-common/createBasicLoanEditorDirective";
import {DataModels                    } from "../../ng-common/DataModels";
import {$timeout                      } from "../../ng-common/ngServices";

import * as appHtml from "./index.html";

class TaxReturnController extends EditorController {
    private IsBorr:boolean;
    init(){
        this.IsBorr = true;
    }
    onLoaded(loan: DataModels): boolean {
        super.onLoaded(loan);
        $timeout(() => {
        this.refreshUI();
        this.loan.on("postCalc",() => {
            this.refreshUI();
            });
        });
        return true;
    }
    refreshUI(){
        if(!Boolean(this.loan.app.fields["aIs4506TFiledTaxesSeparately"].v)){
            this.IsBorr = true;
        }
        this.loan.app.fields["aB4506TNm"].r = !Boolean(this.loan.app.fields["aB4506TNmLckd"].v);
        this.loan.app.fields["aC4506TNm"].r = !Boolean(this.loan.app.fields["aC4506TNmLckd"].v);
    }
}
export const taxReturnDirective = createBasicLoanEditorDirective("taxReturn", appHtml, TaxReturnController);
