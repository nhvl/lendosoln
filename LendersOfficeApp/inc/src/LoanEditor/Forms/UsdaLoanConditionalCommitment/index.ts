// import {StrGuid, guidEmpty} from "../../../utils/guid";

import {createBasicLoanEditorDirective} from "../../ng-common/createBasicLoanEditorDirective";
import {EditorController                } from "../../ng-common/EditorController";

import {DataModels                    } from "../../ng-common/DataModels";

// import {E_BranchChannelT              } from "../../../DataLayer/Enums/E_BranchChannelT";

import * as appHtml from "./index.html";

export class UsdaLoanConditionalCommitmentController extends EditorController {
    // private guidEmpty = guidEmpty;

    // private E_BranchChannelT = E_BranchChannelT;

    // onLoaded(loan:DataModels) {
    //     super.onLoaded(loan);
    //     return true;
    // }
}
export const usdaLoanConditionalCommitmentDirective = createBasicLoanEditorDirective("usdaLoanConditionalCommitment", appHtml, UsdaLoanConditionalCommitmentController);
