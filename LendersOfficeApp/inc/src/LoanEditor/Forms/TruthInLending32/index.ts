import {createBasicLoanEditorDirective  } from "../../ng-common/createBasicLoanEditorDirective";
import {EditorController                } from "../../ng-common/EditorController";

import * as appHtml from "./index.html";

export class TruthInLending32Controller extends EditorController {
}

export const truthInLending32Directive = createBasicLoanEditorDirective("truthInLending32", appHtml, TruthInLending32Controller);