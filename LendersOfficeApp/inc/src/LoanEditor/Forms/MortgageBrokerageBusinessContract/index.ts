import {createBasicLoanEditorDirective  } from "../../ng-common/createBasicLoanEditorDirective";
import {EditorController                } from "../../ng-common/EditorController";

import * as appHtml from "./index.html";

export class MortgageBrokerageBusinessContractController extends EditorController {
}

export const mortgageBrokerageBusinessContractDirective = createBasicLoanEditorDirective("mortgageBrokerageBusinessContractDirective", appHtml, MortgageBrokerageBusinessContractController);