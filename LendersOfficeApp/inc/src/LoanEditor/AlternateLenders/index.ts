import {EditorController              } from "../ng-common/EditorController";
import {createBasicLoanEditorDirective} from "../ng-common/createBasicLoanEditorDirective";
import {ILqbDataModel,updateFromRaw   } from "../ng-common/ILqbDataModel";
import {IApplicant                    } from "../ng-common/IApplicant";
import {LqModalController             } from "../components/lqModal";

import * as appHtml from "./app.html";

export class AlternateLenderController extends EditorController {
    private selectedLender:{ [key: string]: ILqbDataModel };
    private template: ILqbDataModel;
    private modal: LqModalController;
    private editModal: LqModalController;

    private ref       : (_: {$ref  : AlternateLenderController}) => void;
    private onChanged : () => void;

    private shouldAcceptModalChange: boolean = false;
    private editLenderBackup: any = null;

    private revertCallBack = () => {
        var index = this.loan.alternateLenders.data.indexOf(this.selectedLender, 0);
        if (index > -1) {
            if (this.editLenderBackup != null){
                updateFromRaw(this.editLenderBackup, this.loan.alternateLenders.data[index]);
            } else {
                this.loan.alternateLenders.data.splice(index, 1);
            }
        }
    };

    private $onInit() {
        if (!!this.ref) this.ref({ $ref: this }); else debugger;
    }

    private deleteLender(lenderIndex: string, recordIndex: number){
        const m = `Do you want to delete this record?`;
        this.dialog.confirm(m, "Confirm?")
            .then((isConfirmed) => {
                if (isConfirmed){
                    this.service.altLender.DeleteLender({lenderId: lenderIndex}).then(() => {
                                this.loan.alternateLenders.data.splice(recordIndex, 1);
                            });
                }
                return isConfirmed;
            });

    }

    private editLender(lender: any){
        this.selectedLender = lender;
        this.editLenderBackup = angular.merge({}, lender);

        this.editModal.setOption({ backdrop:"static", keyboard:false, });
        this.shouldAcceptModalChange = false;

        this.editModal.show();
    }

    private createLender(){
        this.selectedLender = angular.merge({},this.loan.alternateLenders.template);
        this.editLenderBackup = null;
        this.loan.alternateLenders.data.push(this.selectedLender);
        this.shouldAcceptModalChange = false;
        this.editModal.show();
    }

    private close(){
        this.editModal.close();
        this.modal.close();
    }

    private modalOk(){
        this.shouldAcceptModalChange = true;
    }

    private modalCancel(){

        if (this.loan.isCalculating) this.loan.once("postCalc", this.revertCallBack);
        else this.revertCallBack();
    }

    onAltLenderHide(e:JQueryEventObject):void {
        if (!!this.loan){
            this.loan.save().then(() => { this.onChanged() });
        }
    }

    onModalHide(e:JQueryEventObject):void {
        if (this.shouldAcceptModalChange){
            return;
        }

        if (this.loan.isCalculating) this.loan.once("postCalc", this.revertCallBack);
        else this.revertCallBack();

        this.loan.calculate();
    }

    shouldEnableSaveButton(): boolean{
        var emptyFields = angular.element("#editModal input").filter(function() {
            return this.value === "";
        });

        return emptyFields.length == 0;
    }

    show(loanId: string, appId: string){
        this.load(loanId, appId, ["alternateLenders"]);
        this.shouldAcceptModalChange = false;
        this.modal.setOption({ backdrop:"static", keyboard:false, });
        this.modal.show();
    }
}

export const alternateLenderModal: ng.IComponentOptions = {
    ngName         : "alternateLenderModal",
    bindings       : {
        ref        : "&",
        onChanged  : "&",
    },
    controllerAs   : "vm",
    template       : appHtml,
    controller     : AlternateLenderController as any,
};
