import {EditorController                } from "../ng-common/EditorController";
import {createBasicLoanEditorDirective  } from "../ng-common/createBasicLoanEditorDirective";
import {E_sLienPosT                     } from "../../DataLayer/Enums/E_sLienPosT";
import {E_sLPurposeT                    } from "../../DataLayer/Enums/E_sLPurposeT";
import {toastrOptions                   } from "../ng-common/toastrOptions";
import {$timeout,                       } from "../ng-common/ngServices";
import {DataModels                      } from "../ng-common/DataModels";

import * as appHtml from "./index.html";

class OtherFinancingController extends EditorController {
    onLoaded(loan: DataModels): boolean {
        super.onLoaded(loan);
        $timeout(() => {
            $(`lqb-input[lqb-model="vm.loan.fields.sIsOFinNew"] label`).on("click", (e) => {
                if (this.isFirstLien()) {
                    this.f_confirmsIsOFinNew(e);
                }
            });
        });
        return true;
    }
    isFirstLien() {
        if (this.loan != null)
            return this.loan.fields["sLienPosT"].v == E_sLienPosT.First;
    }
    isShowLayerFinancing() {
        if (this.loan != null)
            return this.loan.BrokerDB.IsUseLayeredFinancing && this.isFirstLien();
    }
    showCreditLineLabel(sIsOFinCreditLineInDrawPeriodCb: boolean) {
        if (sIsOFinCreditLineInDrawPeriodCb)
            $("#sSubFinLabel").text("Credit Line");
        else
            $("#sSubFinLabel").text("Original Balance");
    }
    hasLinkedLoan(): boolean {
        if (this.loan == null) return false;
        return this.loan.fields["sLinkedLoanName"].v != "";
    }
    showLinkedButtons(): boolean {
        if (this.loan == null || this.loan.sLinkedLoanInfo == null) return false;
        return this.loan.sLinkedLoanInfo["IsShowLinkedLoanButtons"];
    }
    f_goToLinkLoan(): void {
        window.name = this.loan.sLId;
        if (window.opener && !window.opener.closed && window.opener.name == this.loan.sLinkedLoanInfo["LinkedLId"]) {
            var location = window.opener.location.href;
            if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) window.opener.close();
            window.open(location, this.loan.sLinkedLoanInfo["LinkedLId"]);
            return;
        }

        var win = window.open("LoanEditor.aspx?loanid=" + this.loan.sLinkedLoanInfo["LinkedLId"] + "#/otherFinancing", this.loan.sLinkedLoanInfo["LinkedLId"]);
        if (win) {
            if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
                win.close();
                window.open("LoanEditor.aspx?loanid=" + this.loan.sLinkedLoanInfo["LinkedLId"] + "#/otherFinancing", this.loan.sLinkedLoanInfo["LinkedLId"]);
                return;
            }
            /*var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
            {
                window.open("LoanEditor.aspx?loanid=" + this.loan.sLinkedLoanInfo["LinkedLId"]+"#/otherFinancing", this.loan.sLinkedLoanInfo["LinkedLId"]);
                return;
            }*/
            win.focus();
        }
    }
    f_updateLinkLoan(): void {
        toastr.info(`updating Linked Loan`, null, toastrOptions);
        const data = {
            loanId: String(this.loan.fields["sLId"].v)
        };
        this.service.otherFinancing.updateLinkLoan(data).then((m: any) => {
            //console.log(m);
            alert(m);
        }).catch((e: any) => {
            //console.log(e);
            alert(e);
        });
    }
    f_confirmsIsOFinNew(e: JQueryEventObject): void {
        if ((this.loan != null)
            && (this.loan.fields["sLPurposeT"].v != E_sLPurposeT.Purchase)
            && this.loan.fields["sLinkedLoanName"].v != "") {
            console.log(this.loan.fields["sIsOFinNew"].v);
            if (this.loan.fields["sIsOFinNew"].v) {
                const m = "Warning: Marking the other financing as existing will permanently unlink the loan file currently linked to this loan file. Would you like to proceed?";
                this.dialog.confirm(m, "Confirm").then((isConfirmed) => {
                    if (isConfirmed) {
                        console.assert(this.loan.fields["sIsOFinNew"].v == true);
                        this.loan.fields["sIsOFinNew"].v = false;
                        this.f_unlinkLoan();
                    }
                });
                e.stopPropagation();
            }
        }
    }
    f_unlinkLoan(): void {
        const m = "Warning: This will permanently unlink the loan file currently linked to this loan file. Would you like to proceed?";
        this.dialog.confirm(m, "Confirm").then((isConfirmed) => {
            if (isConfirmed) {
                toastr.info(`unlink Loan`, null, toastrOptions);
                const data = {
                    loanId: String(this.loan.fields["sLId"].v)
                };
                this.service.otherFinancing.unlinkLoan(data).then(() => {
                    //alert(m);
                    $("button").prop("disabled", true);
                    this.loan.calculate();
                }).catch((e: any) => {
                    alert(e);
                });
            }
        });
    }
}
export const otherFinancingDirective = createBasicLoanEditorDirective("otherFinancing", appHtml, OtherFinancingController);
