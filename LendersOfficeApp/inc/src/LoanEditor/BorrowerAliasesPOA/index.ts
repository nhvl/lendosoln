import {EditorController              } from "../ng-common/EditorController";
import {createBasicLoanEditorDirective} from "../ng-common/createBasicLoanEditorDirective";
import {getQueryStringValue           } from "../../utils/getQueryStringValue";
import {LqModalController             } from "../components/lqModal";

export class BorrowerAliasesController extends EditorController {
    newAlias: string;
    isBorrower: boolean;

    private modal: LqModalController;

    private ref       : (_: {$ref  : BorrowerAliasesController}) => void;
    private onChanged : () => void;

    private firstLoad: boolean = true;
    private oldLoan: any;

    private $onInit() {
        if (!!this.ref) this.ref({ $ref: this }); else debugger;
    }

    show(loanId: string, appId: string, isBorrower: boolean){
        this.isBorrower = isBorrower;
        this.load(loanId, appId, ["aBAliases", "aCAliases", "aBPowerOfAttorneyNm", "aCPowerOfAttorneyNm"]);
        this.newAlias = "";
        this.modal.show();
    }

    getAliases(){
        if (this.loan == null)
        {
            return null;
        }

        if (this.isBorrower){
            if (this.loan.app.aBAliases == null)
            {
                this.loan.app.aBAliases = [];
            }
            return this.loan.app.aBAliases;
        }
        else{
            if (this.loan.app.aCAliases == null)
            {
                this.loan.app.aCAliases = [];
            }
            return this.loan.app.aCAliases;
        }
    }

    addAlias(){
        const addFunction = () => {
            var addingAlias = angular.merge({},this.loan.app.fields["aAliasTemplate"]);
            addingAlias.v = this.newAlias;
            this.newAlias = '';
            this.getAliases().push(addingAlias);
        };

        if (this.loan.isCalculating) this.loan.once("postCalc", addFunction) ; else addFunction();
    }

    deleteAlias(idx: number){
        const delFunction = () => {
            this.getAliases().splice(idx,1 );
            this.loan.calculate();
        };

        if (this.loan.isCalculating) this.loan.once("postCalc", delFunction); else delFunction();
    }

    done(){
        this.modal.close();
        this.loan.save();
    }

    cancel(){
        this.modal.close();
    }
}

import * as template from "./index.html";

export const borrowerAliasesModal: ng.IComponentOptions = {
    ngName         : "borrowerAliasesModal",
    bindings       : {
        ref        : "&",
        onChanged  : "&",
    },
    controllerAs   : "vm",
    template       : template,
    controller     : BorrowerAliasesController as any,
};
