import {EditorController              } from "../ng-common/EditorController";
import {createBasicLoanEditorDirective} from "../ng-common/createBasicLoanEditorDirective";
import {DataModels                    } from "../ng-common/DataModels";

import {E_aAddrMailSourceT  } from "../../DataLayer/Enums/E_aAddrMailSourceT";
import {E_aAddrPostSourceT  } from "../../DataLayer/Enums/E_aAddrPostSourceT";
import {E_sLPurposeT        } from "../../DataLayer/Enums/E_sLPurposeT";
import {E_aBAddrT           } from "../../DataLayer/Enums/E_aBAddrT";
import {BorrowerAliasesController} from "./../BorrowerAliasesPOA"

import * as appHtml from "./index.html";

const _ReadOnly = false;

class BorrowerInfoController extends EditorController {
    private E_sLPurposeT = E_sLPurposeT;
    private aliasesModal: BorrowerAliasesController;
    init() { }
    onLoaded(loan:DataModels) {
        super.onLoaded(loan);

        this.updateMailingAddressFields();
        this.enableVorBtn();
        return true;
    }
    private editAliasesPOA(isBorrower: boolean) {
        this.aliasesModal.show(this.loan.sLId, this.loan.appId, isBorrower);
    }
    private onCopyBorrowerClick(){
        const {
            aBLastNm             , aCLastNm             ,

            aCHPhone             , aBHPhone             ,
            aCMaritalStatT       , aBMaritalStatT       ,
            aCAddr               , aBAddr               ,
            aCCity               , aBCity               ,
            aCState              , aBState              ,
            aCZip                , aBZip                ,
            aCAddrT              , aBAddrT              ,
            aCAddrYrs            , aBAddrYrs            ,
            aCAddrMailSourceT    , aBAddrMailSourceT    ,
            aCAddrMail           , aBAddrMail           ,
            aCCityMail           , aBCityMail           ,
            aCStateMail          , aBStateMail          ,
            aCZipMail            , aBZipMail            ,

            aCPrev1Addr          , aBPrev1Addr          ,
            aCPrev1City          , aBPrev1City          ,
            aCPrev1State         , aBPrev1State         ,
            aCPrev1Zip           , aBPrev1Zip           ,
            aCPrev1AddrT         , aBPrev1AddrT         ,
            aCPrev1AddrYrs       , aBPrev1AddrYrs       ,
            aCPrev2Addr          , aBPrev2Addr          ,
            aCPrev2City          , aBPrev2City          ,
            aCPrev2State         , aBPrev2State         ,
            aCPrev2Zip           , aBPrev2Zip           ,
            aCPrev2AddrT         , aBPrev2AddrT         ,
            aCPrev2AddrYrs       , aBPrev2AddrYrs       ,

            aCAddrPostSourceTLckd, aBAddrPostSourceTLckd,
            aCAddrPostSourceT    , aBAddrPostSourceT    ,
            aCAddrPost           , aBAddrPost           ,
            aCCityPost           , aBCityPost           ,
            aCStatePost          , aBStatePost          ,
            aCZipPost            , aBZipPost            ,
        } = this.loan.app.fields;

        if (aCLastNm.v === "")
            aCLastNm.v = aBLastNm.v;

        aCHPhone             .v = aBHPhone.v;
        aCMaritalStatT       .v = aBMaritalStatT.v;
        aCAddr               .v = aBAddr.v;
        aCCity               .v = aBCity.v;
        aCState              .v = aBState.v;
        aCZip                .v = aBZip.v;
        aCAddrT              .v = aBAddrT.v;
        aCAddrYrs            .v = aBAddrYrs.v;
        aCAddrMailSourceT    .v = aBAddrMailSourceT.v;
        aCAddrMail           .v = aBAddrMail.v;
        aCCityMail           .v = aBCityMail.v;
        aCStateMail          .v = aBStateMail.v;
        aCZipMail            .v = aBZipMail.v;

        aCPrev1Addr          .v = aBPrev1Addr.v;
        aCPrev1City          .v = aBPrev1City.v;
        aCPrev1State         .v = aBPrev1State.v;
        aCPrev1Zip           .v = aBPrev1Zip.v;
        aCPrev1AddrT         .v = aBPrev1AddrT.v;
        aCPrev1AddrYrs       .v = aBPrev1AddrYrs.v;
        aCPrev2Addr          .v = aBPrev2Addr.v;
        aCPrev2City          .v = aBPrev2City.v;
        aCPrev2State         .v = aBPrev2State.v;
        aCPrev2Zip           .v = aBPrev2Zip.v;
        aCPrev2AddrT         .v = aBPrev2AddrT.v;
        aCPrev2AddrYrs       .v = aBPrev2AddrYrs.v;

        aCAddrPostSourceTLckd.v = aBAddrPostSourceTLckd.v;
        aCAddrPostSourceT    .v = aBAddrPostSourceT.v;
        aCAddrPost           .v = aBAddrPost.v;
        aCCityPost           .v = aBCityPost.v;
        aCStatePost          .v = aBStatePost.v;
        aCZipPost            .v = aBZipPost.v;

        this.updateMailingAddressFields();
    }

    private fillPresentAddress(){
        const {
            aBAddr ,
            aBCity ,
            aBZip  ,
            aBState,
        } = this.loan.app.fields;
        const {
            sSpAddr ,
            sSpCity ,
            sSpZip  ,
            sSpState,
        } = this.loan.fields;

        aBAddr .v = sSpAddr .v;
        aBCity .v = sSpCity .v;
        aBZip  .v = sSpZip  .v;
        aBState.v = sSpState.v;
    }

    private syncMailingAddress(){
        const {
            aBAddrMailSourceT    , aCAddrMailSourceT    ,
            aBAddrMail           , aCAddrMail           ,
            aBCityMail           , aCCityMail           ,
            aBStateMail          , aCStateMail          ,
            aBZipMail            , aCZipMail            ,
            aBAddr               , aCAddr               ,
            aBCity               , aCCity               ,
            aBState              , aCState              ,
            aBZip                , aCZip                ,
            aBAddrPostSourceT    , aCAddrPostSourceT    ,
            aBAddrPostSourceTLckd, aCAddrPostSourceTLckd,
            aBAddrPost           , aCAddrPost           ,
            aBCityPost           , aCCityPost           ,
            aBStatePost          , aCStatePost          ,
            aBZipPost            , aCZipPost            ,
            aBDecOcc             , aCDecOcc             ,
        } = this.loan.app.fields;

        const {
            sSpAddr,
            sSpCity,
            sSpState,
            sSpZip,
            sLPurposeT,
        } = this.loan.fields;

        switch (aBAddrMailSourceT.v) {
            case E_aAddrMailSourceT.PresentAddress:
                aBAddrMail .v = aBAddr .v;
                aBCityMail .v = aBCity .v;
                aBStateMail.v = aBState.v;
                aBZipMail  .v = aBZip  .v;
                break;
            case E_aAddrMailSourceT.SubjectPropertyAddress:
                aBAddrMail .v = sSpAddr .v;
                aBCityMail .v = sSpCity .v;
                aBStateMail.v = sSpState.v;
                aBZipMail  .v = sSpZip  .v;
                break;
        }

        switch (aCAddrMailSourceT.v) {
            case E_aAddrMailSourceT.PresentAddress:
                aCAddrMail .v = aCAddr .v;
                aCCityMail .v = aCCity .v;
                aCStateMail.v = aCState.v;
                aCZipMail  .v = aCZip  .v;
                break;
            case E_aAddrMailSourceT.SubjectPropertyAddress:
                aCAddrMail .v = sSpAddr .v;
                aCCityMail .v = sSpCity .v;
                aCStateMail.v = sSpState.v;
                aCZipMail  .v = sSpZip  .v;
                break;
        }

        // OPM 59554 - Post-Closing Address Fields
        if (!aBAddrPostSourceTLckd.v) {
            // If here aBAddrPostSourceTLckd is unlocked
            aBAddrPostSourceT.v = (sLPurposeT.v === E_sLPurposeT.Purchase && aBDecOcc.v) ? E_aAddrPostSourceT.SubjectPropertyAddress : E_aAddrPostSourceT.MailingAddress;
        }

        switch (aBAddrPostSourceT.v) {
            case E_aAddrPostSourceT.PresentAddress:
                aBAddrPost .v = aBAddr .v;
                aBCityPost .v = aBCity .v;
                aBStatePost.v = aBState.v;
                aBZipPost  .v = aBZip  .v;
                break;
            case E_aAddrPostSourceT.MailingAddress:
                aBAddrPost .v = aBAddrMail .v;
                aBCityPost .v = aBCityMail .v;
                aBStatePost.v = aBStateMail.v;
                aBZipPost  .v = aBZipMail  .v;
                break;
            case E_aAddrPostSourceT.SubjectPropertyAddress:
                aBAddrPost .v = sSpAddr .v;
                aBCityPost .v = sSpCity .v;
                aBStatePost.v = sSpState.v;
                aBZipPost  .v = sSpZip  .v;
            break;
        }

        // OPM 59554 - Post-Closing Address Fields
        if (!aCAddrPostSourceTLckd.v) {
            aCAddrPostSourceT.v = (sLPurposeT.v === E_sLPurposeT.Purchase && aCDecOcc.v) ? E_aAddrPostSourceT.SubjectPropertyAddress : E_aAddrPostSourceT.MailingAddress;
        }

        switch (aCAddrPostSourceT.v) {
            case E_aAddrPostSourceT.PresentAddress:
                aCAddrPost .v = aCAddr .v;
                aCCityPost .v = aCCity .v;
                aCStatePost.v = aCState.v;
                aCZipPost  .v = aCZip  .v;
                break;
            case E_aAddrPostSourceT.MailingAddress:
                aCAddrPost .v = aCAddrMail .v;
                aCCityPost .v = aCCityMail .v;
                aCStatePost.v = aCStateMail.v;
                aCZipPost  .v = aCZipMail  .v;
                break;
            case E_aAddrPostSourceT.SubjectPropertyAddress:
                aCAddrPost .v = sSpAddr .v;
                aCCityPost .v = sSpCity .v;
                aCStatePost.v = sSpState.v;
                aCZipPost  .v = sSpZip  .v;
                break;
        }

        this.updateMailingAddressFields();
    }
    private updateMailingAddressFields() {
        const {
            aBAddrMailSourceT    , aCAddrMailSourceT    ,
            aBAddrMail           , aCAddrMail           ,
            aBCityMail           , aCCityMail           ,
            aBStateMail          , aCStateMail          ,
            aBZipMail            , aCZipMail            ,
            aBAddrPostSourceT    , aCAddrPostSourceT    ,
            aBAddrPost           , aCAddrPost           ,
            aBCityPost           , aCCityPost           ,
            aBStatePost          , aCStatePost          ,
            aBZipPost            , aCZipPost            ,
            aBAddrPostSourceTLckd, aCAddrPostSourceTLckd,
        } = this.loan.app.fields;

        aBAddrMail .r =
        aBCityMail .r =
        aBStateMail.r =
        aBZipMail  .r = (aBAddrMailSourceT.v !== E_aAddrMailSourceT.Other);

        aCAddrMail .r =
        aCCityMail .r =
        aCStateMail.r =
        aCZipMail  .r = (aCAddrMailSourceT.v !== E_aAddrMailSourceT.Other);

        aBAddrPost .r =
        aBCityPost .r =
        aBStatePost.r =
        aBZipPost  .r = (aBAddrPostSourceT.v !== E_aAddrPostSourceT.Other);

        aBAddrPostSourceT.r = (!aBAddrPostSourceTLckd.v);

        aCAddrPost .r =
        aCCityPost .r =
        aCStatePost.r =
        aCZipPost  .r = (aCAddrPostSourceT.v !== E_aAddrPostSourceT.Other);

        aCAddrPostSourceT.r = (!aCAddrPostSourceTLckd.v);
    }

    private enableEditVorB = false;
    private enableEditVorC = false;
    private enableVorBtn() {
        if (_ReadOnly) return;
        const {
            aBAddrT,    aCAddrT,
            aBAddr ,    aCAddr ,
            aBCity ,    aCCity ,
            aBState,    aCState,
            aBZip  ,    aCZip  ,
        } = this.loan.app.fields;

        this.enableEditVorB = (aBAddrT.v === E_aBAddrT.Rent) && [aBAddr, aBCity, aBState, aBZip].some(f => (f.v as string).trim().length > 0);
        this.enableEditVorC = (aCAddrT.v === E_aBAddrT.Rent) && [aCAddr, aCCity, aCState, aCZip].some(f => (f.v as string).trim().length > 0);
    }
    EditVOR(isborrower: boolean) {
        if (_ReadOnly) return;
        if ((isborrower && !this.enableEditVorB) || (!isborrower && !this.enableEditVorC)) return;
        debugger;

        // if (isDirty()) {
        //     var ret = confirm('Save is required before proceding');
        //     if (ret) saveMe();
        //     else { return; }
        // }

        // var recordid = isborrower ? '11111111-1111-1111-1111-111111111111' : '44444444-4444-4444-4444-444444444444';

        // linkMe('Verifications/VORRecord.aspx', 'recordid=' + recordid);
    }
}
export const borrowerInfoDirective = createBasicLoanEditorDirective("borrowerInfo", appHtml, BorrowerInfoController);
