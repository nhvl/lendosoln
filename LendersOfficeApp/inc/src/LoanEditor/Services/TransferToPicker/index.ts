/*

```html
<transfer-to-picker-modal
    ref="vm.ttpModal = $ref"
    open="vm.isShowTtpModal"
    vendor-id="vm.loan.BrokerUser.documentVendorBrokerSettings.VendorId"
    code="vm.loan.fields.sDocMagicPlanCodeNm.v"
    on-complete="vm.setTransferTo($vendor)"
    on-cancel="vm.cancelSetTransferTo()"
    ></transfer-to-picker-modal>
```

```typescript
class SomeController {
    isShowTtpModal: boolean;

    openTtpModal() {
        this.isShowTtpModal = true;
    }

    setTransferTo({Description, Code}: {Description:string, Code:string}) {
        this.isShowTtpModal = false;
        this.loan.fields.sDocMagicTransferToInvestorNm  .v = Description;
        this.loan.fields.sDocMagicTransferToInvestorCode.v = Code       ;
    }

    cancelSetTransferTo() {
        this.isShowTtpModal = false;
    }
}
```

Register

```js
import {transferToPickerModal} from ".../transferToPicker";
angular("app").component(transferToPickerModal.ngName, transferToPickerModal);
```

*/
import {StrGuid} from "../../../utils/guid";
import {LqModalController             } from "../../components/lqModal";

import {ServiceFactory                } from "./../../ng-common/service/ServiceFactory";


import * as appHtml from "./index.html";

export class TransferToPickerController {
    private code    : string;
    private vendorId: StrGuid;

    private loading      : boolean;
    private error        : string;
    private investors    : {Description:string, Code:string}[];

    private modal: LqModalController;

    private ref       : (_: {$ref  : TransferToPickerController}) => void;

    private onComplete: (_: {$vendor: {Description:string, Code:string}}) => void;

    static $inject = [ServiceFactory.ngName]
    constructor(private service: ServiceFactory) {

    }

    private $onInit() {
        if (!!this.ref) this.ref({ $ref: this }); else debugger;
    }

    private $onChanges({open}: {
        open: ng.IChangesObject<boolean>;
    }) {
        if (open != null && open.currentValue != open.previousValue) {
            if (open.currentValue) {
                this.modal.show();

                this.loading   = true;
                this.error     = null;
                this.investors = null;

                this.service.documentVendor.getTransferToVendor({ vendorId: this.vendorId, code: this.code })
                .then(({ErrorMessage, Result}) => {
                    this.loading   = false;
                    this.error     = ErrorMessage;
                    this.investors = Result;
                });
            } else {
                if (this.modal) {
                    this.modal.close();
                }
            }
        }
    }

    private select($vendor: {Description:string, Code:string}) {
        this.onComplete({$vendor});
    }

    private setModal(modal: LqModalController){
        this.modal = modal
    }
}

export const transferToPickerModal: ng.IComponentOptions = {
    ngName         : "transferToPickerModal",
    bindings       : {
        open       : "<",
        vendorId   : "<",
        code       : "<",

        ref        : "&",
        onComplete : "&",
        onCancel   : "&",
    },
    controllerAs   : "vm",
    template       : appHtml,
    controller     : TransferToPickerController as any,
};
