import {LqModalController      } from "../components/lqModal";
import {ModalStatus            } from "../BorrowerAsset/ModalStatus";
import {ServiceFactory         } from "../ng-common/service";
import {IDialog                } from "../../ng-common/dialog";
import {StrGuid,guidEmpty      } from "../../utils/guid";
import {E_RoleT,GetRole        } from "../../DataLayer/Enums/E_RoleT";
import {EditorController       } from "../ng-common/EditorController";
import {LoadingIndicator       } from "../ng-common/loadingIndicator";


export class TeamPicker {
    Roledesc:string;
    Role:E_RoleT;
    
    IsAssignedTeam:boolean;
    teamGrid:any[];

    modalInstance: LqModalController;
    modalStatus:number;

    currentSelect = -1;
    CallBack:any;

    constructor(private dialog: IDialog, private service: ServiceFactory, private _loadingIndicator: LoadingIndicator, private controller: EditorController) {
    }
    assign(role: E_RoleT, roledesc:string,callback:any){
        this.modalStatus = ModalStatus.Open;
        this.Roledesc = roledesc;
        this.Role = role;
        this.IsAssignedTeam = false;
        this.CallBack = callback;
        this.currentSelect = -1;
        this.search();
        this.modalStatus = ModalStatus.Open;
        this.modalInstance.show();
    }
    private search(){
        const data = {
            isAssignedTeam:this.IsAssignedTeam, 
            roleString:GetRole(this.Role)
        }
        this.service.teamPicker.search(data).then((m:any[])=>{
            this.teamGrid = m;
            console.log(m);
        });
    }
    private setTPModal(modalInstance: LqModalController):void {
        this.modalInstance = modalInstance;
        this.modalInstance.setOption({ backdrop: 'static',
            keyboard: false });
    }
    private onTPModalHide(e:JQueryEventObject):void {
        if((this.modalStatus == ModalStatus.Save) && (this.CallBack != null))
            this.CallBack(this.Role,(this.currentSelect==-1 ? null : this.teamGrid[this.currentSelect]["Name"]));
    }
    private assignTeam(index:number){
        this._loadingIndicator.show();
        const data = {
            loanId: this.controller.loan.sLId,
            teamId: this.teamGrid[index]["Id"]
        }
        this.service.editTeams.assignLoan(data).then(()=>{
            this._loadingIndicator.hide();
            this.currentSelect = index;
            this.modalStatus = ModalStatus.Save;
            this.modalInstance.close();
        });
    }
    private clear(){
        this._loadingIndicator.show();
        const data = {
            loanId: this.controller.loan.sLId,
            roleString:GetRole(this.Role)
        }
        this.service.editTeams.clearLoanAssignment(data).then(()=>{
            this._loadingIndicator.hide();
            this.currentSelect = -1;
            this.modalStatus = ModalStatus.Save;
            this.modalInstance.close();
        });
    }
    private cancel(){
        this.modalStatus = ModalStatus.Discard;
        this.modalInstance.close();
    }
}
