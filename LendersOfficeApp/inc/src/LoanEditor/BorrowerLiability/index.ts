import {EditorController                                } from "../ng-common/EditorController";
import {createBasicLoanEditorDirective                  } from "../ng-common/createBasicLoanEditorDirective";
import {ILqbDataModel, LqbInputType,E_CalculationType   } from "../ng-common/ILqbDataModel";
import {E_sLT                                           } from "../../DataLayer/Enums/E_sLT";
import {E_DebtT                                         } from "../../DataLayer/Enums/E_DebtT";
import {E_LiaOwnerT                                     } from "../../DataLayer/Enums/E_LiaOwnerT";
import {E_sLPurposeT                                    } from "../../DataLayer/Enums/E_sLPurposeT";
import {DataModels                                      } from "../ng-common/DataModels";
import {$stateParams, $timeout                          } from "../ng-common/ngServices";
import {toastrOptions                                   } from "../ng-common/toastrOptions";
import {guidEmpty, isGuid                               } from "../../utils/guid";
import {LqModalController                               } from "../components/lqModal";
import {ModalStatus                                     } from "../BorrowerAsset/ModalStatus";
import isEqual = require("lodash/isEqual");

import * as appHtml from "./index.html";

class BorrowerLiabilityController extends EditorController {
    //Is Liability or Debt Consolidation
    isLia: boolean;
    selectedIndex: number;
    sortKey: string;
    //Use for sorting
    isAsc: boolean;
    //Use in Debt Consolidation
    isCalcAuto:boolean;
    selectedRegular: { [key: string]: ILqbDataModel };
    //is adding new regular or not
    isAddNew: boolean;
    //Store temp regular and restore it when cancel
    selectedRegularTemp: { [key: string]: ILqbDataModel };
    modalStatus:ModalStatus;
    modal: LqModalController;
    init() {
        this.isLia = true;
        this.isCalcAuto = true;
        this.selectedIndex = -1;
        this.sortKey = "";
        this.updateItem();
    }
    onLoaded(loan: DataModels): boolean {
        super.onLoaded(loan);

        if (!this.isEmpty()) {
            this.selectedIndex = this.getSelectedIndex();
            this.sortKey = "";
            this.updateItem();
        }
        this.calcEvents();
        // Events run when calculate manually
        $timeout(()=>{
            this.f_refreshUI();
            $(`div[name='DebtSum'] input`).change(()=>{
                $timeout(()=>{
                    if(!this.isCalcAuto){
                        this.f_refreshUI();
                    }
                });
            });
            $(`div[name='DebtSum'] lqb-input[lqb-model="vm.loan.fields.sLPurposeT"] *`).blur(()=>{
                $timeout(()=>{
                if(!this.isCalcAuto){
                    this.f_refreshUI();
                }});
            });
        });
        return true;
    }
    setModal(calculationModal: LqModalController):void {
        this.modal = calculationModal;
        // this.modal.setOption({ backdrop:'static',
        //     keyboard: false });
    }
    onModalHide(e:JQueryEventObject):void {
        if(this.modalStatus == ModalStatus.Open){
            //check if current regular is changed or not
            if(isEqual(angular.merge({},this.selectedRegular),this.selectedRegularTemp)) return;
            const m = `Do you wish to save this liability?`;
            this.dialog.ync(m,"Save?").then((o)=>{
                    switch (o) {
                        case 1:
                            this.done();
                            return;
                        case 0:
                            this.cancel();
                            return;
                        case -1:
                            this.modalStatus = ModalStatus.Open;
                            return;
                        default:
                            console.error(o);
                            return;
                    }
                });
            e.preventDefault();
        }
    }
    done(): void {
        this.modalStatus = ModalStatus.Save;
        this.isAddNew = false;
        this.modal.close();
        this.selectedIndex = -1;
        this.loan.calculate();
    }
    cancel(): void {
        this.modalStatus=ModalStatus.Discard;
        if (this.isAddNew) {
            this.executeDelete(this.loan.app.aLiaCollection.regulars.length - 1);
            this.isAddNew = false;
        }
        else {
            this.loan.app.aLiaCollection.regulars[this.selectedIndex] = angular.merge({}, this.selectedRegularTemp);
        }
        this.modal.close();
        this.selectedIndex = -1;
        this.loan.calculate();
    }
    registerPdOffClick(){
        $timeout(() => {
            $(`lqb-input[lqb-model$="WillBePdOff"] label`).off('click',($event) => {
                this.willBePayOffClick($event);
            }).on('click',($event) => {
                this.willBePayOffClick($event);
            });
        });
    }
    willBePayOffClick($event: JQueryEventObject){
        var item = $event.target.parentElement.parentElement;
        console.log(item.tagName);
        if(item.tagName=="DIV"){
            if(!this.selectedRegular["WillBePdOff"].v){
                this.selectedRegular["UsedInRatio"].v = false;
            }
        }
        else{
            for (var i = 0; i < this.loan.app.aLiaCollection.regulars.length; i++) {
                if ((this.loan.app.aLiaCollection.regulars[i]["id"].v === item.getAttribute("name")) &&
                    !this.loan.app.aLiaCollection.regulars[i]["WillBePdOff"].v &&
                    !this.isMortgage(this.loan.app.aLiaCollection.regulars[i])){
                        this.loan.app.aLiaCollection.regulars[i]["UsedInRatio"].v = false;
                }
            }
        }
    }

    setVOLStatus(regular:{ [key: string]: ILqbDataModel }):boolean{
        return this.updateVOStatus(regular,true);
    }
    setVOMStatus(regular:{ [key: string]: ILqbDataModel }):boolean{
        return this.updateVOStatus(regular,false);
    }
    updateVOStatus(regular:{ [key: string]: ILqbDataModel },isVOL:boolean):boolean {
        if (regular == null || regular["DebtT"] == null) {
            return false;
        }
        const debtType = regular["DebtT"].v;
        if (debtType === E_DebtT.Mortgage) {
            return isVOL ? false : true;
        }
        else {
            if (debtType == E_DebtT.Installment || debtType == E_DebtT.Revolving) {
                return isVOL ? true : false;
            }
            else {
                return false;
            }
        }
    }
    displayCreditReport(): void {
        var url = "../newlos/services/ViewCreditFrame.aspx?loanid=" + this.loan.sLId + "&applicationid=" + this.loan.app.fields["aAppId"].v;//+ "&islead=<%=RequestHelper.GetSafeQueryString("islead")%>";
        window.open(url, "report", "width=750,height=490,toolbar=no,menubar=no,location=no,status=no,resizable=yes");
    }
    f_addToReo(): void {
        if(this.loan.isCalculating) return;
        toastr.info(`adding to Reo`, null, toastrOptions);
        this.loan.isCalculating=true;
        const data= {
            reoId: String(this.selectedRegular["MatchedReRecordId"].v),
            balValue: String(this.selectedRegular["Bal"].v),
            paymentValue: String(this.selectedRegular["Pmt"].v),
            loanId: String(this.loan.sLId),
            appId: String(this.loan.app.fields["aAppId"].v),
            fileVersion: Number(this.loan.fields["sFileVersion"].v)
        }
        this.service.liabilityRecord.addToReo(data).then(() => {
            this.loan.isCalculating=false;
            this.dialog.alert("The balance and payment information has been added to the REO record.","Add To Reo");
        }).catch((e: any) => {
            this.loan.isCalculating=false;
            console.log(e);
        })
    }
    isDisableAddToReo():boolean{
        return this.selectedRegular == null || this.selectedRegular["MatchedReRecordId"].r || this.selectedRegular["MatchedReRecordId"].v == guidEmpty;
    }
    changeLia(v:boolean):void{
        this.isLia = v;
    }
    changeCalculation(v:boolean):void{
        this.isCalcAuto = v;
        this.updateCalculationState(this.isCalcAuto);
        if(v){
            this.loan.calculate();
        }
    }
    updateCalculationState(isCalc:boolean):void{
        var calcAction = isCalc ? E_CalculationType.CalculateNormal : E_CalculationType.NoCalculate;
        this.loan.fields["sLAmtCalc"].calcAction = calcAction;
        this.loan.fields["sLAmtLckd"].calcAction = calcAction;
        this.loan.fields["sLPurposeT"].calcAction = calcAction;
        this.loan.fields["sNoteIR"].calcAction = calcAction;
        this.loan.fields["sTransNetCash"].calcAction = calcAction;
        this.loan.fields["sTransNetCashLckd"].calcAction = calcAction;
        this.loan.fields["sTerm"].calcAction = calcAction;
        this.loan.fields["sDue"].calcAction = calcAction;
        this.loan.fields["sIOnlyMon"].calcAction = calcAction;
    }
    reCalculate():void{
        this.updateCalculationState(true);
        this.loan.calculate().then(() => {
            this.updateCalculationState(this.isCalcAuto);
        });
    }
    getSelectedIndex(): number {
        if ($stateParams == null || $stateParams["LiaId"] == null || $stateParams["LiaId"] == "") return -1;
        for (var i = 0; i < this.loan.app.aLiaCollection.regulars.length; i++) {
            if (this.loan.app.aLiaCollection.regulars[i]["id"].v === $stateParams["LiaId"]) return i;
        }
        return -1;
    }
    //Set up events for pre and post calculation
    calcEvents() {
        this.loan.on("preCalc", (dm: ILqbDataModel) => {
            if ((dm != null) && Array.isArray(dm.options) && dm.options.length > 0) {
                // DebtT
                var o = dm.options.find(o => o.v === Number(dm.v));
                // 5
                if ((o != null)&&(o.v === E_DebtT.Revolving)) {
                    this.selectedRegular["RemainMons_rep"].v = "(R)";
                }
                // OwnerT
                // 0,1,2
                if (o != null) {
                    switch (o.v) {
                        case E_LiaOwnerT.Borrower:
                            if(o.l=="Borrower"){
                                this.selectedRegular["AccNm"].v=this.loan.app.fields["aBNm"].v;
                            }
                            break;
                        case E_LiaOwnerT.CoBorrower:
                            if(o.l=="CoBorrower"){
                                    this.selectedRegular["AccNm"].v=this.loan.app.fields["aCNm"].v;
                                }
                            break;
                        case E_LiaOwnerT.Joint:
                            if(o.l=="Joint"){
                                    this.selectedRegular["AccNm"].v=this.loan.app.fields["aBNm"].v+" & "+this.loan.app.fields["aCNm"].v;
                                }
                            break;
                    }
                }

                // MatchedReRecordId
                o = dm.options.find(o => o.v === dm.v);
                if(o!=null && o.v !== guidEmpty && isGuid(o.v)){
                    this.selectedRegular["Desc"].v=o.l;
                }

            }
        });
        this.loan.on("postCalc", () => {
            this.f_refreshUI();
        });
    }
    //Some special behaviors in liability page only
    f_refreshUI(){
        const isItemReadOnly = <boolean>this.loan.fields["sIsRateLocked"].v;
        this.loan.fields["sNoteIR"].r = this.loan.fields["sNoteIR"].r || isItemReadOnly;
        this.loan.fields["sTerm"].r = this.loan.fields["sTerm"].r || isItemReadOnly;
        this.loan.fields["sDue"].r = this.loan.fields["sDue"].r || isItemReadOnly;

        if(this.loan.fields["sLPurposeT"].v != E_sLPurposeT.Purchase){
                this.loan.fields["sLAmtLckd"].r = true;
                this.loan.fields["sLAmtLckd"].v = true;
        }
        else{
            this.loan.fields["sLAmtLckd"].r = false;
        }

        if(!this.isCalcAuto){
            if(this.loan.fields["sTransNetCashLckd"].v!=null){
                this.loan.fields["sTransNetCash"].r=!<boolean>this.loan.fields["sTransNetCashLckd"].v;
            }

            if(this.loan.fields["sLAmtLckd"].v!=null){
                this.loan.fields["sLAmtCalc"].r=!<boolean>this.loan.fields["sLAmtLckd"].v;
            }

            if(this.loan.BrokerUser.HasPmlEnabled){
                this.loan.fields["sProdCashoutAmt"].r=(this.loan.fields["sLPurposeT"].v != E_sLPurposeT.RefinCashout);
            }
        }
    }
    isFHA(v: number) {
        return v == E_sLT.FHA;
    }
    displayPdOff(v: boolean) {
        return v ? "Yes" : "No";
    }
    displayUsedInRatio(regular: { [key: string]: ILqbDataModel }) {
        if (this.isMortgage(regular)) {
            return "See REO";
        }
        return <boolean>regular["UsedInRatio"].v ? "Yes" : "No";
    }
    isMortgage(regular: { [key: string]: ILqbDataModel }) {
        if (regular == null) return false;
        return regular["DebtT"].v === E_DebtT.Mortgage;
    }
    displayIsPmlAltered(v: string) {
        if (v == null || v == "") {
            return "No";
        }
        return "Yes";
    }
    getOwnerValue(regular: { [key: string]: ILqbDataModel }) {
        const x = regular["OwnerT"];
        const o = x.options.find(item => item.v === x.v);
        return (o == null) ? null : o.l;
    }
    getLiabilityValue(regular: { [key: string]: ILqbDataModel }) {
        const x = regular["DebtT"];
        const o = x.options.find(item => item.v === x.v);
        return (o == null) ? null : o.l;
    }
    isEmpty() {
        return (this.loan == null) ||
            (this.loan.app.aLiaCollection.regulars == null) ||
            (this.loan.app.aLiaCollection.regulars.length == 0);
    }

    clickItem(index: number) {
        if (!this.isLia) return;
        this.selectedIndex = index;
        this.updateItem();
    }
    delete(index:number) {
        const m = `Do you want to delete this liability?`;
        this.dialog.confirm(m, "Confirm?")
            .then((isConfirmed) => {
                if (!isConfirmed) return false;

                this.executeDelete(index);
                return true;
            });

    }
    executeDelete(index:number): void {
        if (!this.isEmpty()) {
            if (this.loan.app.aLiaCollection.regulars.length == 1) {
                this.loan.app.aLiaCollection.regulars = [];
                this.loan.calculate();
                return;
            }
            this.loan.app.aLiaCollection.regulars.splice(index, 1);
            this.loan.calculate();
        }
    }
    sortRegulars(by: string) {
        if (by == this.sortKey) this.isAsc = !this.isAsc;
        else {
            this.sortKey = by;
            this.isAsc = true;
        }
        var property = this.sortKey;
        var sortOrder = -1;
        if (this.isAsc) sortOrder = 1;
        const _this = this;
        this.loan.app.aLiaCollection.regulars = this.loan.app.aLiaCollection.regulars.sort(function(a, b) {
            var result = 0;
            if(property == "UsedInRatio"){
                const aVal = _this.displayUsedInRatio(a);
                const bVal = _this.displayUsedInRatio(b);
                result = (aVal < bVal) ? -1 : (aVal > bVal) ? 1 : 0;
            }
            else{
                if((typeof a[property].v) == (typeof b[property].v))
                    result = (a[property].v < b[property].v) ? -1 : (a[property].v > b[property].v) ? 1 : 0;
                else
                    result = (String(a[property].v) < String(b[property].v)) ? -1 : (String(a[property].v) > String(b[property].v)) ? 1 : 0;
            }
            return result * sortOrder;
        });
        this.updateItem();
    }
    add() {
        this.isAddNew = true;
        this.loan.app.aLiaCollection.regulars.push(angular.merge({}, this.loan.app.aLiaCollection.regularTemplate));
        this.selectedIndex = this.loan.app.aLiaCollection.regulars.length - 1;
        this.updateItem();
        this.loan.calculate();
    }
    updateItem(): void {
        if (this.isEmpty()||this.selectedIndex == -1)
            this.selectedRegular = this.selectedRegularTemp = null;
        else {
            this.selectedRegular = this.loan.app.aLiaCollection.regulars[this.selectedIndex];
            this.selectedRegularTemp = angular.merge({}, this.loan.app.aLiaCollection.regulars[this.selectedIndex]);
            this.renderPmlAuditTrail();
            this.registerPdOffClick();
            this.modalStatus = ModalStatus.Open;
            this.modal.show();
        }
    }
    isDebtUsedInRatios() {
        return (this.selectedRegular != null && this.selectedRegular["UsedInRatio"].t != LqbInputType.Checkbox);
    }
    pmlAuditTrail: any[];
    renderPmlAuditTrail() {
        if (!this.isEmpty() && this.loan.BrokerUser != null && this.loan.BrokerUser.HasPmlEnabled) {
            if (this.selectedRegular != null && this.selectedRegular["PmlAuditTrail"] != null && this.selectedRegular["PmlAuditTrail"].v != "") {
                this.pmlAuditTrail = JSON.parse(this.selectedRegular["PmlAuditTrail"].v as string) || [];
            }
            else {
                this.pmlAuditTrail = [];
            }
        }
    }
}
export const borrowerLiabilityDirective = createBasicLoanEditorDirective("borrowerLiability", appHtml, BorrowerLiabilityController);
