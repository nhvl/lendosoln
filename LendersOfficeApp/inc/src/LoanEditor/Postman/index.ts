import * as url from "url";
import {download} from "../../utils/download";

const defaultConfig = require("./postman.default.json");
const maxItem = 25;
const postmanLsKey = "postmanList";

interface IPostmanItem {
    method: string;
    url: string;
    data: {};
}

export class PostmanController { static ngName = "ApplicationDetailController";
    private method: string;
    private methodOptions: string[];
    private url: string;
    private data: string;

    private requestDuration: any;
    private response: any;

    private xs: IPostmanItem[];

    static $inject = ["$element", "$http"];
    constructor($element: ng.IAugmentedJQuery, private $http:ng.IHttpService) {
        this.use(defaultConfig);

        const str = localStorage.getItem(postmanLsKey);
        this.xs = JSON.parse(str) || [];
        if (this.xs.length > 0) this.use(this.xs[0]);

        {
            const x = url.parse(location.href, true).query;
            const y = url.parse(this.url, true);
            y.search = null;
            ["loanid", "appid"].forEach(k => {
                if (x[k]) y.query[k] = x[k];
            });
            this.url = url.format(y);
        }

        this.urlChange();
    }

    private use(x: IPostmanItem) {
        Object.assign(this, x);
        this.data = JSON.stringify(this.data);
    }

    private submit() {
        const $http = this.$http;

        let data: {};
        try {
            data = JSON.parse(this.data);
        } catch (e) {
            alert(`Data is not valid json: ${e}`);
            return;
        }

        const x :IPostmanItem = {
            method: this.method,
            url: this.url,
            data,
        };

        const t0 = performance.now();
        $http(x).then((response) => {
            this.requestDuration = performance.now() - t0;

            this.response = response.data;

            this.xs.unshift(x);
            localStorage.setItem(postmanLsKey, JSON.stringify(this.xs.slice(0, maxItem)));
        }, (response) => {
            this.response = response;
        });

        this.response = null;
    }

    private copyResultToData(){
        if (this.response == null) return;
        this.data = JSON.stringify(this.response, null, 2);
    }
    private downloadResultToData(){
        if (this.response == null) return;
        download(`${Date.now()}.json`, JSON.stringify(this.response, null, 2));
    }

    private currentOp: string;
    private opOptions: string[];
    private urlChange(){
        if (!this.url) return;

        const x = url.parse(this.url, true);
        if (!(/lqbapi\/loan\.ashx$/i).test(x.pathname)) {
            this.currentOp = this.opOptions = null;
            return;
        }

        const qs = x.query;
        this.currentOp = qs["op"];
        this.opOptions = ("Load Save Calculate CalculateZipcode").split(" ")
            .filter(op => op !== this.currentOp);
    }
    private changeOpTo(op:string) {
        const x = url.parse(this.url, true);
        x.search = null;
        if (x.query["op"]) x.query["op"] = op;
        this.url = url.format(x);

        this.urlChange();
    }
}

import * as appHtml from "./index.html";
export const postmanComponent: angular.IComponentOptions = {
    ngName      : "postman",

    controller  : PostmanController,
    controllerAs: "vm",
    template    : appHtml,
    isolate     : true,
};
