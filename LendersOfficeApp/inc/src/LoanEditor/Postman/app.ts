/// <reference path="../../dt-local/tsd.d.ts" />
/// <reference path="../../dt-local/angular-ui-layout/index.d.ts" />

import * as uiLayout_ngName from "angular-ui-layout";
import "angular-ui-layout/src/ui-layout.css";

import {postmanComponent} from "./index";
import {jsonViewer} from "../../ng-directives/jsonViewer";

export const app = angular.module("app", [uiLayout_ngName])
    .directive(jsonViewer.ngName, jsonViewer)
    .component(postmanComponent.ngName, postmanComponent)
;
