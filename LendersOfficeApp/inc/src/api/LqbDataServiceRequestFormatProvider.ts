import Dictionary from './Dictionary';
import EntityDefinition from './EntityDefinition';
import LqbDataServiceRequestType from './LqbDataServiceRequestType';

export default class LqbDataServiceRequestFormatProvider {
	private version : number;
	private requests : Array<Dictionary<any>> = new Array<Dictionary<any>>();
	
	public setVersion(newVersion: number) {
		this.version = newVersion;
	} 
	
	public addRequest(requestType: LqbDataServiceRequestType, requestArgs: Dictionary<any>) : string { 
		switch (requestType) {
			case LqbDataServiceRequestType.Get:
				return this.handleGet(requestArgs);
			case LqbDataServiceRequestType.Set:
				return this.handleSet(requestArgs);
			case LqbDataServiceRequestType.Add:
				return this.handleAdd(requestArgs);
            case LqbDataServiceRequestType.AddAssociation:
                return this.handleAddAssociation(requestArgs);
			case LqbDataServiceRequestType.Remove:
                return this.handleRemove(requestArgs);
            case LqbDataServiceRequestType.RemoveAssociation:
                return this.handleRemoveAssociation(requestArgs);
			case LqbDataServiceRequestType.SetOrder:
				return this.handleSetOrder(requestArgs);
			case LqbDataServiceRequestType.SetOwner:
				return this.handleSetOwner(requestArgs);
            case LqbDataServiceRequestType.GetAssetTotals:
			case LqbDataServiceRequestType.GetRealPropertyTotals:
			case LqbDataServiceRequestType.GetIncomeSourceTotals:
            case LqbDataServiceRequestType.GetLiabilityTotals:
                return this.handleGetTotals(requestArgs, requestType);
            case LqbDataServiceRequestType.SwapBorrowerAndCoborrower:
                return this.handleSwapBorrowerAndCoborrower(requestArgs);
            case LqbDataServiceRequestType.DeleteApplication:
                return this.handleDeleteApplication(requestArgs);
            case LqbDataServiceRequestType.AddConsumer:
                return this.handleAddConsumer(requestArgs);
            case LqbDataServiceRequestType.AddApplication:
                return this.handleAddApplication(requestArgs);
            case LqbDataServiceRequestType.DeleteConsumer:
                return this.handleDeleteConsumer(requestArgs);
            case LqbDataServiceRequestType.SetPrimaryApplication:
                return this.handleSetPrimaryApplication(requestArgs);
            case LqbDataServiceRequestType.SetPrimaryBorrower:
                return this.handleSetPrimaryBorrower(requestArgs);
            case LqbDataServiceRequestType.AddBalPmtToRealProperty:
                return this.handleAddBalPmtToRealProperty(requestArgs);
            case LqbDataServiceRequestType.SetAttendance:
                return this.handleSetAttendance(requestArgs);
			default:
				throw 'Unhandled request type ' + requestType;
		}
	}
	
	public toRequest() : string {
		return JSON.stringify(
		{
			data: {
				version: this.version,
				requests: this.requests
			}
		});
	}
	
    private handleGet(requestArgs: Dictionary<any>): string { 
        let getters: Array<Dictionary<any>> = requestArgs['getters'];
        if (getters == null) {
            getters = [requestArgs];
        }

        let getRequest = new Dictionary<any>();

        for (const getter of getters) {
            const entityDefinitions: Array<typeof EntityDefinition> = getter['definitions'];
            const recordId = getter['recordId'] || 'any';

            for (const entity of entityDefinitions) {
                const requestObj = new entity();
                const classKeys = this.getKeys(requestObj);

                let classWrapper = getRequest[requestObj.classType] || new Dictionary<any>();

                if (requestObj.collectionName != null) {
                    let collectionWrapper = new Dictionary<any>();
                    collectionWrapper[recordId] = Object.assign(new Dictionary<any>(), classKeys);
                    classWrapper[requestObj.collectionName] = collectionWrapper;
                }
                else {
                    classWrapper = Object.assign(classWrapper, classKeys);
                }

                getRequest[requestObj.classType] = classWrapper;
            }
        }
	
		const operationId = this.requests.length.toString();
		const request = {
			operationId,
			name: LqbDataServiceRequestType.Get,
			args: getRequest
		};
		
		this.requests.push(request);
		return operationId;
	}
	
	private handleSet(requestArgs: Dictionary<any>) : string { 
		const setters : Array<Dictionary<any>> = requestArgs['setters'];
		
		let setRequest = new Dictionary<any>();
		
		for (const setter of setters) {
			const entity : typeof EntityDefinition = setter['definition'];
			const requestObj = new entity();
			const classKeys = this.getKeys(requestObj);
			
			const values : Dictionary<any> = setter['values'];
			
			// Remove any decorator properties from the object.
			let newValues = new Dictionary<any>();
            for (const key of Object.keys(classKeys).filter(key => !requestObj.isReadonly(key))) {
				newValues[key] = values[key];
			}
			
			let classWrapper = setRequest[requestObj.classType] || new Dictionary<any>();
			
			if (requestObj.collectionName != null) {
				const recordId = setter['recordId'];
				if (recordId == null) {
					throw `Missing record ID for setting of ${entity}.`;
				}
				
                let collectionWrapper = classWrapper[requestObj.collectionName] || new Dictionary<any>();
				collectionWrapper[recordId] = Object.assign(new Dictionary<any>(), newValues);
				classWrapper[requestObj.collectionName] = collectionWrapper;
			}
			else {
				classWrapper = Object.assign(classWrapper, newValues);
			}
			
			setRequest[requestObj.classType] = classWrapper;
		}
		
		const operationId = this.requests.length.toString();
		const request = {
			operationId,
			name: LqbDataServiceRequestType.Set,
			args: setRequest
		};
		
		this.requests.push(request);
		return operationId;
	}
	
	private handleAdd(requestArgs: Dictionary<any>) : string { 
        const primaryOwnerId = requestArgs['primaryOwnerId'];
        const attendeeIds: Array<string> = requestArgs['attendeeIds'];

        if (primaryOwnerId == null && (attendeeIds == null || attendeeIds.length == 0)) {
			throw 'Must specify owner ID for addition.';
		}
		
		const operationId = this.requests.length.toString();
		const entity : typeof EntityDefinition = requestArgs['definition'];
		const requestObj = new entity();
		
		let args: Dictionary<any> = {
			path: requestObj.classType,
			collectionName: requestObj.collectionName,
			id: null // API will echo this back with the actual value of the new record.
        };

        if (primaryOwnerId != null) {
            args['ownerId'] = primaryOwnerId;
        }

        if (attendeeIds != null) {
            args['attendeeIds'] = attendeeIds;
        }

        const index: number|null = requestArgs['index'];
        if (index != null) {
            args['index'] = index;
        }

        const additionalOwnerIds : Array<string> = requestArgs['additionalOwnerIds'];
        if (additionalOwnerIds != null) {
            args['additionalOwnerIds'] = additionalOwnerIds;
        }

        const fields: Dictionary<any> = requestArgs['fields'];
        if (fields != null) {
            args['fields'] = fields;
        }
		
		const request = {
			operationId,
			name: LqbDataServiceRequestType.Add,
			args
		};
	
		this.requests.push(request);
		return operationId;
    }

    private handleAddAssociation(requestArgs: Dictionary<any>): string {
        const operationId = this.requests.length.toString();

        const firstId = requestArgs['firstId'];
        const secondId = requestArgs['secondId'];
        if (firstId == null || secondId == null) {
            throw 'Must specify IDs for new association.';
        }

        const entity: typeof EntityDefinition = requestArgs['definition'];
        const requestObj = new entity();

        const args: Dictionary<any> = {
            path: requestObj.classType,
            associationSet: requestObj.collectionName,
            firstId,
            secondId
        };

        const request = {
            operationId,
            name: LqbDataServiceRequestType.AddAssociation,
            args
        };

        this.requests.push(request);
        return operationId;
    }
	
	private handleRemove(requestArgs: Dictionary<any>) : string { 
		const recordId = requestArgs['recordId'];
		if (recordId == null) {
			throw 'Must specify record ID for deletion.';
		}
		
		const operationId = this.requests.length.toString();
		const entity : typeof EntityDefinition = requestArgs['definition'];
		const requestObj = new entity();
		
		const args = {
			path: requestObj.classType,
			collectionName: requestObj.collectionName,
			id: recordId
		};
		
		const request = {
			operationId,
			name: LqbDataServiceRequestType.Remove,
			args
		};
		
		this.requests.push(request);
		return operationId;
    }

    private handleRemoveAssociation(requestArgs: Dictionary<any>): string {
        const operationId = this.requests.length.toString();

        const id = requestArgs['id'];
        if (id == null) {
            throw 'ID must be specified for association to remove.';
        }

        const entity: typeof EntityDefinition = requestArgs['definition'];
        const requestObj = new entity();

        const args: Dictionary<any> = {
            path: requestObj.classType,
            associationSet: requestObj.collectionName,
            id
        };

        const request = {
            operationId,
            name: LqbDataServiceRequestType.RemoveAssociation,
            args
        };

        this.requests.push(request);
        return operationId;
    }
	
	private handleSetOrder(requestArgs: Dictionary<any>) : string { 
		const newOrderIds: Array<string> = requestArgs['newOrderIds'];
		if (newOrderIds == null) {
			throw 'Must specify new order of IDs.';
		}
		
		const operationId = this.requests.length.toString();
		const entity : typeof EntityDefinition = requestArgs['definition'];
		const requestObj = new entity();
		
		let args: Dictionary<any> = {
			path: requestObj.classType,
			collectionName: requestObj.collectionName,
			order: newOrderIds
        };

        const uladApplicationId: string = requestArgs['uladApplicationId'];
        if (uladApplicationId != null) {
            args['uladApplicationId'] = uladApplicationId;
        }
		
		const request = {
			operationId,
			name: LqbDataServiceRequestType.SetOrder,
			args
		};
		
		this.requests.push(request);
		return operationId;
	}
	
	private handleSetOwner(requestArgs: Dictionary<any>) : string { 
		const recordId = requestArgs['recordId'];
		if (recordId == null) {
			throw 'Must specify record ID to set owner.';
		}
		
        const primaryOwnerId = requestArgs['primaryOwnerId'];
		if (primaryOwnerId == null) {
			throw 'Must specify primary owner ID to set owner.';
		}
		
		const operationId = this.requests.length.toString();
		const entity : typeof EntityDefinition = requestArgs['definition'];
		const requestObj = new entity();
		
		let args = new Dictionary<any>();
		args['path'] = requestObj.classType;
		args['associationName'] = requestObj.collectionName;
        args['id'] = recordId;
		args['primaryOwnerId'] = primaryOwnerId;

		const additionalOwnerIds : Array<string> = requestArgs['additionalOwnerIds'];
		if (additionalOwnerIds != null) {
			args['additionalOwnerIds'] = additionalOwnerIds;
		}
		
		const request = {
			operationId,
			name: LqbDataServiceRequestType.SetOwner,
			args
		};
		
		this.requests.push(request);
		return operationId;
    }

    private handleSetAttendance(requestArgs: Dictionary<any>): string {
        const recordId = requestArgs['recordId'];
        if (recordId == null) {
            throw 'Must specify record ID to set attendance.';
        }

        const attendeeIds: Array<string> = requestArgs['attendeeIds'];
        if (attendeeIds == null || attendeeIds.length == 0) {
            throw 'Must specify at least one attendee to set attendance.';
        }

        const operationId = this.requests.length.toString();
        const entity: typeof EntityDefinition = requestArgs['definition'];
        const requestObj = new entity();

        let args = new Dictionary<any>();
        args['path'] = requestObj.classType;
        args['associationName'] = requestObj.collectionName;
        args['id'] = recordId;
        args['attendeeIds'] = attendeeIds;

        const request = {
            operationId,
            name: LqbDataServiceRequestType.SetAttendance,
            args
        };

        this.requests.push(request);
        return operationId;
    }
	
    private handleGetTotals(requestArgs: Dictionary<any>, getTotalsType: LqbDataServiceRequestType) : string {
        const operationId = this.requests.length.toString();

        const id = requestArgs['id'];
        const type = requestArgs['type'];

		const entity : typeof EntityDefinition = requestArgs['definition'];
		const requestObj = new entity();
		
        const args = {
            path: requestObj.classType,
            id,
            type
        }
		
		const request = {
			operationId,
            name: getTotalsType,
			args
		};
		
		this.requests.push(request);
		return operationId;
    }

    private handleSwapBorrowerAndCoborrower(requestArgs: Dictionary<any>): string {
        const operationId = this.requests.length.toString();
        const applicationId = requestArgs['applicationId'];
        const applicationType = requestArgs['applicationType'];

        const entity: typeof EntityDefinition = requestArgs['definition'];
        const requestObj = new entity();

        let args: Dictionary<any> = {
            path: requestObj.classType,
            applicationId,
            applicationType
        };

        const request = {
            operationId,
            name: LqbDataServiceRequestType.SwapBorrowerAndCoborrower,
            args
        };

        this.requests.push(request);
        return operationId;
    }

    private handleDeleteApplication(requestArgs: Dictionary<any>): string {
        const operationId = this.requests.length.toString();
        const applicationType: string = requestArgs['applicationType'];
        const applicationId: string = requestArgs['applicationId'];

        const entity: typeof EntityDefinition = requestArgs['definition'];
        const requestObj = new entity();

        let args: Dictionary<any> = {
            path: requestObj.classType,
            applicationType,
            applicationId
        };

        const request = {
            operationId,
            name: LqbDataServiceRequestType.DeleteApplication,
            args
        };

        this.requests.push(request);
        return operationId;
    }

    private handleAddConsumer(requestArgs: Dictionary<any>): string {
        const operationId = this.requests.length.toString();
        const applicationType: string = requestArgs['applicationType'];
        const applicationId: string = requestArgs['applicationId'];

        const entity: typeof EntityDefinition = requestArgs['definition'];
        const requestObj = new entity();

        let args: Dictionary<any> = {
            path: requestObj.classType,
            applicationType,
            applicationId
        };

        const request = {
            operationId,
            name: LqbDataServiceRequestType.AddConsumer,
            args
        };

        this.requests.push(request);
        return operationId;
    }

    private handleAddApplication(requestArgs: Dictionary<any>): string {
        const operationId = this.requests.length.toString();
        const applicationType: string = requestArgs['applicationType'];

        const entity: typeof EntityDefinition = requestArgs['definition'];
        const requestObj = new entity();

        let args: Dictionary<any> = {
            path: requestObj.classType,
            applicationType
        }

        const request = {
            operationId,
            name: LqbDataServiceRequestType.AddApplication,
            args
        };

        this.requests.push(request);
        return operationId;
    }

    private handleDeleteConsumer(requestArgs: Dictionary<any>): string {
        const operationId = this.requests.length.toString();
        const applicationType: string = requestArgs['applicationType'];
        const consumerId: string = requestArgs['consumerId'];

        const entity: typeof EntityDefinition = requestArgs['definition'];
        const requestObj = new entity();

        let args: Dictionary<any> = {
            path: requestObj.classType,
            applicationType,
            consumerId
        }

        const request = {
            operationId,
            name: LqbDataServiceRequestType.DeleteConsumer,
            args
        };

        this.requests.push(request);
        return operationId;
    }

    private handleSetPrimaryApplication(requestArgs: Dictionary<any>): string {
        const operationId = this.requests.length.toString();
        const applicationType: string = requestArgs['applicationType'];
        const applicationId: string = requestArgs['applicationId'];

        const entity: typeof EntityDefinition = requestArgs['definition'];
        const requestObj = new entity();

        let args: Dictionary<any> = {
            path: requestObj.classType,
            applicationType,
            applicationId
        };

        const request = {
            operationId,
            name: LqbDataServiceRequestType.SetPrimaryApplication,
            args
        };

        this.requests.push(request);
        return operationId;
    }

    private handleSetPrimaryBorrower(requestArgs: Dictionary<any>): string {
        const operationId = this.requests.length.toString();
        const consumerId: string = requestArgs['consumerId'];

        const entity: typeof EntityDefinition = requestArgs['definition'];
        const requestObj = new entity();

        let args: Dictionary<any> = {
            path: requestObj.classType,
            consumerId
        };

        const request = {
            operationId,
            name: LqbDataServiceRequestType.SetPrimaryBorrower,
            args
        };

        this.requests.push(request);
        return operationId;
    }

    private handleAddBalPmtToRealProperty(requestArgs: Dictionary<any>): string {
        const operationId = this.requests.length.toString();

        const liabilityId = requestArgs['liabilityId'];
        const realPropertyId = requestArgs['realPropertyId'];
        if (liabilityId == null || realPropertyId == null) {
            throw 'Must specify liability and real property ID to add balance and payment information.';
        }

        const entity: typeof EntityDefinition = requestArgs['definition'];
        const requestObj = new entity();

        let args: Dictionary<any> = {
            path: requestObj.classType,
            liabilityId,
            realPropertyId
        };

        const request = {
            operationId,
            name: LqbDataServiceRequestType.AddBalPmtToRealProperty,
            args
        };

        this.requests.push(request);
        return operationId;
    }
	
	private getKeys(obj: any) : Dictionary<any> {
        let keys = new Dictionary<any>();
		
        for (const key of Object.keys(obj)) {
            // Class Type and Collection Name are internal only.  Id is always provided by the endpoint.
			if (key === 'classType' || key === 'collectionName' || key === 'Id') {
				continue;
			}
			
            if (typeof obj[key] === 'object') {
                // For now, support selecting all sub-collection records.
                keys[key] = {
                    'any': this.getKeys(obj[key])
                };
            }
            else {
                // Data service expects keys with no values.
                keys[key] = null;
            }
		}
		
		return keys;
	}
}