export default class Dictionary<TValue> {
	[key: string] : TValue;
}
