import Dictionary from './Dictionary';
import IAssociationHelper from './IAssociationHelper';
import ILqbDataServiceResponseValueConverter from './ILqbDataServiceResponseConverter';
import LqbDataServiceRequestType from './LqbDataServiceRequestType';

export default class LqbDataServiceResponseHandler {
	private responses : Dictionary<any>;
	private error: string;
	
	constructor(response: string|Dictionary<any>, private associationHelper: IAssociationHelper, private responseValueConverter?: ILqbDataServiceResponseValueConverter) {
		const responseObj = typeof response === 'string' ? JSON.parse(response) as Dictionary<any> : response;
		const dataWrapper = responseObj['data'];
		
		if (typeof dataWrapper['error'] === 'undefined') {
			this.responses = dataWrapper['responses'] as Dictionary<any>;
		}
		else {
			this.error = dataWrapper['error'] as string;
		}
	}
	
	public hasError() : boolean {
		return typeof this.error !== 'undefined';
	}
	
	public getErrorMessage() : string {
		return this.error;
	}
	
	public getResponseByKey(requestType: LqbDataServiceRequestType, key: string, operationId?: string) : any {
		if (this.hasError()) {
			throw 'Cannot retrieve response due to error.';
		}
	
		const foundResponse = this.responses.find((response: Dictionary<any>) => {
			if (response['name'] !== requestType) {
				return false;
			}
			
			if (operationId == null) {
				return true;
			}
			
			const responseOperationId = response['operationId'];
			return responseOperationId === operationId;
		});
		
		if (typeof foundResponse === 'undefined') {
			throw `No response found with requestType ${requestType}.`;
		}
		
		const foundResponseReturnValue = foundResponse['returns'];
        const responseValue = (foundResponseReturnValue['path'] === key ? foundResponseReturnValue : foundResponseReturnValue[key]);

        if (typeof responseValue !== 'object') {
            return responseValue;
        }

        const flattenedResponse = this.flattenResponse(responseValue as Dictionary<any>);
        this.convertResponse(flattenedResponse);
		
		this.mergeAssociations(flattenedResponse);
		return flattenedResponse;
	}
	
	private flattenResponse(responseValue: Dictionary<any>) : Dictionary<any> {
		let flattenedResponse = new Dictionary<any>();
		
		for (const key of Object.keys(responseValue)) {
			const value = responseValue[key];
			if (typeof value !== 'object') {
				flattenedResponse[key] = value;
			}
			else {
				flattenedResponse[key] = this.collectionObjectToList(value as Dictionary<any>);
			}
		}
		
		return flattenedResponse;
    }

    private convertResponse(response: Dictionary<any>) {
        if (this.responseValueConverter == null) {
            return;
        }

        for (const key of Object.keys(response)) {
            const value = response[key];
            if (typeof value === 'string') {
                response[key] = this.convertValue(key, value);
            }
            else {
                const collectionObjectList: Array<Dictionary<any>> = value;
                for(const collectionResponse of collectionObjectList) {
                    this.convertResponse(collectionResponse);
                }
            }
        }
    }

    private convertValue(key: string, value: any): any {
        if (this.responseValueConverter!.canConvert(key, value)) {
            return this.responseValueConverter!.convert(value);
        }

        return value;
    }
	
	private collectionObjectToList(collectionObj: Dictionary<any>) : Array<Dictionary<any>> {
		let list = new Array<Dictionary<any>>();
		
		for (const collectionObjKey of Object.keys(collectionObj)) {
            const collectionItem = collectionObj[collectionObjKey];

            let keyDict = new Dictionary<any>();
            keyDict['Id'] = collectionObjKey;

            for (const collectionItemKey of Object.keys(collectionItem)) {
                const value = collectionItem[collectionItemKey];
                if (typeof value === 'object') {
                    keyDict[collectionItemKey] = this.collectionObjectToList(value);
                }
                else {
                    keyDict[collectionItemKey] = value;
                }
            }
			
			list.push(keyDict);
		}
		
		return list;
	}
	
	private mergeAssociations(response: Dictionary<any>) {
		const propertiesWithAssociations = Object.keys(response).filter((key: string) => this.associationHelper.hasAssociation(key));
		for (const property of propertiesWithAssociations) {
			const associationKey = this.associationHelper.getAssociationName(property);
			const associations = response[associationKey];
			
			if (typeof associations === 'undefined') {
				continue;
			}
			
			const associationEntityKey = this.associationHelper.getAssociationEntityKey(associationKey);
            const primaryIndicatorKey = this.associationHelper.hasPrimary(property) ? this.associationHelper.getPrimaryIndicatorKey(property) : null;
			
			const values = response[property];
			
			this.mergeAssociation(values, associations, associationEntityKey);
			this.orderAssociations(values, primaryIndicatorKey);
		}
	}
	
	private mergeAssociation(values: Array<Dictionary<any>>, associations: Array<Dictionary<any>>, associationEntityKey: string) {
		for (const association of associations) {
			const associationEntityId = association[associationEntityKey];
			const matchingEntity = values.find((value: Dictionary<any>) => value['Id'] === associationEntityId);
			
			if (matchingEntity == null) {
				throw `Could not locate value with ID ${associationEntityId}.`;
			}
			
			let entityOwners = matchingEntity.Owners || new Array<Dictionary<any>>();
			entityOwners.push(association);
			matchingEntity.Owners = entityOwners;
		}
	}
	
	private orderAssociations(values: Array<Dictionary<any>>, primaryIndicatorKey: string | null) {
		for (const value of values) {
			if (typeof value.Owners === 'undefined') {
				value.Owners = new Array<Dictionary<any>>();
            }
            else if (primaryIndicatorKey == null) {
                // DO NOTHING. No primary, so no need to sort.
            }
			else {
				value.Owners.sort((owner: Dictionary<boolean>) => owner[primaryIndicatorKey] ? -1 : 1);
			}
		}
	}
}