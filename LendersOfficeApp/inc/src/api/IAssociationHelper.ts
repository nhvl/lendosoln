export default interface IAssociationHelper {
    hasAssociation(property: string): boolean;
    hasPrimary(property: string): boolean;
	getPrimaryIndicatorKey(collectionName: string) : string;
	getAssociationEntityKey(associationName: string) : string;
	getAssociationName(collectionName: string) : string;
}