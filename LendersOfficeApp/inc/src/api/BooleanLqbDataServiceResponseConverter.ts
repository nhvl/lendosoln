import ILqbDataServiceResponseConverter from './ILqbDataServiceResponseConverter';

export default class BooleanLqbDataServiceResponseConverter implements ILqbDataServiceResponseConverter {
    public canConvert(key: string, value: string): boolean {
        if (value == null) {
            return false;
        }

        switch (value.trim().toLowerCase()) {
            case 'true':
            case 'yes':
            case 'false':
            case 'no':
                return true;
            default:
                return false;
        }
    }

    public convert(value: string): any {
        if (value == null) {
            throw `Unhandled conversion value ${value}`;
        }

        switch (value.trim().toLowerCase()) {
            case 'true':
            case 'yes':
                return true;
            case 'false':
            case 'no':
                return false;
            default:
                throw `Unhandled conversion value ${value}`;
        }
    }
}