export default interface ILqbDataServiceResponseValueConverter {
    canConvert(key: string, value: string): boolean;
    convert(value: string): any;
}
