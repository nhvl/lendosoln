export default class EntityDefinition {
	classType : string;
    collectionName?: string;
	Id: string;

    public isReadonly(field: string) {
        return false;
    }
}
