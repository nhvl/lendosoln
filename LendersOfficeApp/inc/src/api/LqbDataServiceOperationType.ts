enum LqbDataServiceOperationType {
    Load = 'load',
    LoadNoOverlay = 'loadNoOverlay',
    Save = 'save'
}

export default LqbDataServiceOperationType;