enum LqbDataServiceRequestType {
	Get = 'get',
	Set = 'set',
    Add = 'addEntity',
    AddAssociation = 'addAssociation',
    Remove = 'removeEntity',
    RemoveAssociation = 'removeAssociation',
	SetOrder = 'setOrder',
	SetOwner = 'setOwnership',
    GetAssetTotals = 'getAssetTotals',
	GetRealPropertyTotals = 'getRealPropertyTotals',
	GetIncomeSourceTotals = 'getIncomeSourceTotals',
    GetLiabilityTotals = 'getLiabilityTotals',
    SwapBorrowerAndCoborrower = 'swapBorrowers',
    AddConsumer = 'addBorrowerToApplication',
    DeleteConsumer = 'deleteBorrower',
    AddApplication = 'addApplication',
    DeleteApplication = 'deleteApplication',
    SetPrimaryApplication = 'setPrimaryApplication',
    SetPrimaryBorrower = 'setPrimaryBorrower',
    AddBalPmtToRealProperty = 'addLiabilityBalanceAndPaymentToRealProperty',
    SetAttendance = 'setAttendance'
}

// Workaround for exporting default enums per 
// https://github.com/Microsoft/TypeScript/issues/3320#issuecomment-107241679
export default LqbDataServiceRequestType;