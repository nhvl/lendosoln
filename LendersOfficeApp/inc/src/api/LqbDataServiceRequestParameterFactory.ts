import Dictionary from './Dictionary';
import EntityDefinition from './EntityDefinition';
import ApplicationType from '../Ulad/Common/ApplicationType';
import TotalsType from '../Ulad/Common/TotalsType';

export default class LqbDataServiceRequestParameterFactory {
    public createAddAssociationParameter(definition: typeof EntityDefinition, firstId: string, secondId: string) : Dictionary<any> {
        return {
            definition,
            firstId,
            secondId
        };
    }

    public createRemoveAssociationParameter(definition: typeof EntityDefinition, id: string): Dictionary<any> {
        return {
            definition,
            id
        };
    }

    public createAddParameter(definition: typeof EntityDefinition, primaryOwnerId: string, additionalOwnerIds?: Array<string>, index?: number, fields?: Dictionary<any>): Dictionary<any> {
        return {
            definition,
            primaryOwnerId,
            additionalOwnerIds,
            index,
            fields
        };
    }

    public createAddParameterForCounselingEvent(definition: typeof EntityDefinition, attendeeIds: Array<string>, index?: number, fields?: Dictionary<any>): Dictionary<any> {
        return {
            definition,
            attendeeIds,
            index,
            fields
        };
    }

    public createRemoveParameter(definition: typeof EntityDefinition, recordId: string): Dictionary<typeof EntityDefinition|string> {
        return {
            definition,
            recordId
        };
    }

    public createIndividualGetParameter(definitions: Array<typeof EntityDefinition>, recordId?: string): Dictionary<any> {
        return {
            definitions,
            recordId
        };
    }

    public createAggregateGetParameter(getters: Array<Dictionary<any>>): Dictionary<any> {
        return {
            getters
        };
    }

    public createGetTotalsParameter(definition: typeof EntityDefinition, type: TotalsType, id?: string): Dictionary<any> {
        return {
            definition,
            type,
            id
        };
    }

    public createSetOrderParameter(definition: typeof EntityDefinition, newOrderIds: Array<string>, uladApplicationId?: string): Dictionary<any> {
        return {
            definition,
            newOrderIds,
            uladApplicationId
        };
    }

    public createIndividualSetParameter(definition: typeof EntityDefinition, values: Dictionary<any>, recordId?: string) : Dictionary<any> {
        return {
            definition,
            values,
            recordId
        };
    }

    public createAggregateSetParameter(setters: Array<Dictionary<any>>): Dictionary<any> {
        return {
            setters
        };
    }

    public createSetOwnerParameter(definition: typeof EntityDefinition, recordId: string, primaryOwnerId: string, additionalOwnerIds?: Array<string>) {
        return {
            definition,
            recordId,
            primaryOwnerId,
            additionalOwnerIds
        };
    }

    public createSetOwnerParameterForCounselingEvent(definition: typeof EntityDefinition, recordId: string, attendeeIds: Array<string>) {
        return {
            definition,
            recordId,
            attendeeIds
        };
    }

    public createSwapBorrowerAndCoborrowerArgs(definition: typeof EntityDefinition, applicationId: string): Dictionary<typeof EntityDefinition|string> {
        return {
            definition,
            applicationId,
            applicationType: ApplicationType.LegacyApplication
        };
    }

    public createDeleteApplicationArgs(definition: typeof EntityDefinition, applicationType: ApplicationType, applicationId: string): Dictionary<typeof EntityDefinition | string> {
        return {
            definition,
            applicationType,
            applicationId
        };
    }

    public createAddNewConsumerArgs(definition: typeof EntityDefinition, applicationType: ApplicationType, applicationId: string): Dictionary<typeof EntityDefinition | string> {
        return {
            definition,
            applicationType,
            applicationId
        };
    }

    public createAddApplicationArgs(definition: typeof EntityDefinition, applicationType: ApplicationType): Dictionary<typeof EntityDefinition | string> {
        return {
            definition,
            applicationType
        };
    }

    public createDeleteConsumerArgs(definition: typeof EntityDefinition, applicationType: ApplicationType, consumerId: string): Dictionary<typeof EntityDefinition | string> {
        return {
            definition,
            applicationType,
            consumerId
        };
    }

    public createSetPrimaryApplicationArgs(definition: typeof EntityDefinition, applicationType: ApplicationType, applicationId: string): Dictionary<typeof EntityDefinition | string> {
        return {
            definition,
            applicationType,
            applicationId
        };
    }

    public createSetPrimaryBorrowerArgs(definition: typeof EntityDefinition, consumerId: string): Dictionary<typeof EntityDefinition | string> {
        return {
            definition,
            consumerId
        };
    }

    public createAddBalPmtToRealPropertyArgs(definition: typeof EntityDefinition, liabilityId: string, realPropertyId: string): Dictionary<typeof EntityDefinition | string> {
        return {
            definition,
            liabilityId,
            realPropertyId
        };
    }
}
