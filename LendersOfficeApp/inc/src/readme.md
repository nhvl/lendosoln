# Requirement

## Per machine

Install the tools:

1. *Node.js* v6.9.0 (with *npm*)

## Per repository

1. Run `npm install` to load dependency packages listed in `package.json`.

    ```
    > npm install
    ```

## Build using Browserify

The script to build and bundle is defined at `LendersOfficeApp\inc\src\build.js`.
By run `npm run tsc` in the command, will build and bundle the TypeScript source code.

---

The Typescript source code for a page,
for example `LendersOfficeApp\LOAdmin\Test\TestCenter.aspx`
is located at `LendersOfficeApp\inc\src\LOAdmin\Test\TestCenter` folder,
called page folder.

The main entry source file is the `app.ts` file in this page folder.

The `pages` variable is contained a list of page folder the need to build.

```js
pages = [
    "LOAdmin/Broker/BrokerEdit"             ,
    "LOAdmin/Broker/BrokerList"             ,
    // ...
]
```

This line in the source code will build and bundle the code for each page.

```js
for (var page of pages) build(page);
```

For development, comment out and left the page you are developing on.
Then run `npm run tsc:w` to build and
automatically rebuild when the related source code files is changed.

## Build using Webpack

The script to build and bundle is defined at
`LendOSoln/LendersOfficeApp/inc/src/webpack.config.js`.
By run `npm run webpack` in the command, will build and bundle the TypeScript source code.

---

The Typescript source code for a page,
for example `LendersOfficeApp\LOAdmin\Test\TestCenter.aspx`
is located at `LendersOfficeApp\inc\src\LOAdmin\Test\TestCenter` folder,
called page folder.

The main entry source file is the `app.ts` file in this page folder.

The `entry` variable is point to a page folder that need to build.
Change the `entry` variable to any page folder that you are working on.
For example, the below entry is pointing to `LendersOfficeApp\inc\src\LoanEditor`
and will generate `LendersOfficeApp\inc\LoanEditor.js`.

```js
const entry =
  // "LOAdmin/SAE/DownloadList"
  "LoanEditor"
  // "LoanEditor/Postman"
;
```

For development, run `npm run webpack:w` to build and
automatically rebuild when the related source code files is changed.

# LoanEditor project

## Convention

For each Loan Editor page, there should be `index.ts` and `index.html` for that
specific page.
