/// <reference path="../dt-local/ractive/index.d.ts" />

import Ractive from "ractive";

console.log("Ractive.DEBUG", Ractive.DEBUG = __DEV__);
((window: any) => {
    if (window.Promise == null) {
        window.Promise = Ractive.Promise;
    }
})(window);

export interface ContextEvent extends RactiveConext {
    event   : Event,
    name    : string,
    node    : HTMLElement,
    original: Event,
}

export interface RactiveConext extends Ractive.Ractive {
    element : any,
    fragment: any,
    model   : any,
    node    : HTMLElement,
    ractive : Ractive.Ractive,
    root    : ContextEvent,
}
