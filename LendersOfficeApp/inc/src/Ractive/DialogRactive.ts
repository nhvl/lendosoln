import Ractive from "ractive";

export interface IDialog {
    alert(message: string, title?: string, okBtn?: string): Promise<boolean>;
    confirm(message: string, title?: string, okBtn?: string, cancelBtn?: string): Promise<boolean>;
}

interface IDialogRactive extends Ractive.Ractive, IDialog {
    promiseResolve: ((value?: boolean) => void) | null;
    $modal: JQuery | null;
}

export const DialogRactive = Ractive.extend({
    data() {
        return {
            title     : "",
            message   : "",
            okBtn     : "OK",
            cancelBtn : "Cancel",
            showCancel: false,
        };
    },
    onrender() {
        this.on({
            ok(event: Ractive.Event) {
                this.$modal.one("hidden.bs.modal", () => {
                    console.debug("dialog.ok");

                    if (this.promiseResolve != null) this.promiseResolve(true);
                });
                this.$modal.modal("hide");
            },
            cancel(event: Ractive.Event) {
                this.$modal.one("hidden.bs.modal", () => {
                    console.debug("dialog.cancel");

                    if (this.promiseResolve != null) this.promiseResolve(false);
                });
                this.$modal.modal("hide");
            }
        });
    },
    oncomplete() {
        this.$modal = $("#dialogModal").modal({
            // backdrop: "static",
            show: false,
        });
    },
    $modal: null as (JQuery|null),
    promiseResolve: null as (((value?: boolean) => void)|null),
    alert(this: IDialogRactive, message: string, title: string = "", okBtn:string = "OK"): Promise<boolean> {
        if (this.promiseResolve != null) this.promiseResolve(false);

        this.set({ title, message, okBtn, showCancel:false, });

        if (this.$modal == null) return Promise.reject<boolean>("$modal have not init");
        this.$modal.modal("show");

        return new Promise <boolean>((resolve) => { this.promiseResolve = resolve; });
    },
    confirm(this: IDialogRactive, message: string, title: string = "", okBtn: string = "OK", cancelBtn: string = "Cancel"): Promise<boolean>{
        if (this.promiseResolve != null) this.promiseResolve(false);

        this.set({ title, message, okBtn, cancelBtn, showCancel: true, });

        if (this.$modal == null) return Promise.reject<boolean>("$modal have not init");
        this.$modal.modal("show");

        return new Promise<boolean>((resolve) => { this.promiseResolve = resolve; });
    },
    template:`
        <div id="dialogModal" class="modal v-middle fade"><div class="modal-dialog"><div class="modal-content">
            <div class="modal-header {{#if (!title)}}hidden{{/if}}">
                <h4 class="modal-title">{{title}}</h4>
            </div>
            <div class="modal-body"><p class="text-center">{{message}}</p></div>
            <div class="modal-footer">
                <span class="clipper {{#if !showCancel}}hidden{{/if}}">
                    <button type="button" class="btn btn-default" on-click="cancel">{{cancelBtn}}</button></span>
                <span class="clipper">
                    <button type="button" class="btn btn-primary" on-click="ok">{{okBtn}}</button></span>
            </div>
        </div></div></div>
    `,
} as any);

export function getDialog():IDialog {
    const $dialogContainer = $("<div>").appendTo("body");
    return (new DialogRactive({
        el: $dialogContainer,
    }) as any);
}
