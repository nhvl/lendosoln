﻿export function selectWhenClickedDecorator(node: HTMLInputElement) {
    $(node).on("click.selectWhenClicked", function () {
        if ((<any> document).selection) {
            const range = (<any> document.body).createTextRange();
            range.moveToElementText(this);
            range.select();
        } else if (window.getSelection) {
            const range = document.createRange();
            range.selectNode(this);
            const selection = window.getSelection();
            if (selection.toString()) return; // Skip event fired when selection
            selection.addRange(range);
        }
    });

    return {
        teardown() {
            $(node).off("click.selectWhenClicked");
        }
    };
}
