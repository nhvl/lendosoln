﻿
export const autofocusDecorator: Ractive.DecoratorPlugin = (Modernizr.input.autofocus) ?
    (() => {
        return { teardown() {} };
    }) :
    ((node) => {
        console.debug("polyfill: autofocus", node);
        (node as HTMLInputElement).focus();
        return { teardown() {} };
    });
