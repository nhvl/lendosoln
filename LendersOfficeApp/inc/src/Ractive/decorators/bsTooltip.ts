﻿export function bsTooltipDecorator(node: HTMLInputElement) {
    $(node).tooltip();

    return {
        teardown() {
            $(node).tooltip("destroy");
        }
    };
}
