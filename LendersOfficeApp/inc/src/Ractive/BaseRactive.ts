export const BaseRactive = Ractive.extend(<Ractive.ExtendOptions> {
    extend(keypath: string, mapValues: { [key: string]: any }) {
        const keys = Object.keys(mapValues);
        var v: { [key: string]: any } = {};
        keys.forEach((k) => { v[`${keypath}.${k}`] = mapValues[k]; });
        return this.set(v);
    }
});
