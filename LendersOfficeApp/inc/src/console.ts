// Avoid `console` errors in browsers that lack a console.
export const console : any = ((window as any).console = window.console || {});

const noop = () => {};
const methods = (
    ("assert clear count debug dir dirxml error exception group groupCollapsed groupEnd info log markTimeline profile profileEnd table time timeEnd timeline timelineEnd timeStamp trace warn")
        .split(" "));
methods.forEach(method => {
    if (!console[method])
        console[method] = noop;
});
