/// <reference path="../dt-local/Window.clipboardData.d.ts" />

export var usePrompt = false;

export interface ICopyFunc {
    (data: string, event: Event, successMessage: string) : void;
}

export const copyToClipboard = ((): ICopyFunc => {
    const document  = window.document;

    if (!!window.clipboardData) return IECopy;

    if (!!document.execCommand && !!document.queryCommandSupported) {
        let support = document.queryCommandSupported("copy");

        if (support) {
            if ($("#copyCanvas").length < 1) {
                $(() => { $("body").append('<textarea id="copyCanvas" class="hidden"></textarea>'); });
            }

            const copyCanvas = $("#copyCanvas");
            copyCanvas.removeClass("hidden").val("test").select();
            try { support = document.queryCommandEnabled("copy"); } catch(e) { support = false; }
            copyCanvas.addClass("hidden");
        }

        if (support) { return clipboardApiCopy; }
        else { $("#copyCanvas").remove(); }
    }

    usePrompt = true;
    return promptCopy;
});

function IECopy(data: string, event: Event, successMessage ?: string) {
    window.clipboardData.setData("text", data );
    if (!!successMessage) window.alert(successMessage);
    return Promise.resolve(true);
}

function clipboardApiCopy(data: string, event:Event, successMessage ?: string) {
    // https://developers.google.com/web/updates/2015/04/cut-and-copy-commands

    const copyCanvas = $("#copyCanvas");
    copyCanvas.removeClass("hidden").val(data).select();
    try {
        const successful = document.execCommand("copy");
    } catch(err) {
        console.error("Oops, unable to cut");
    }
    copyCanvas.addClass("hidden");

    if (!!successMessage) window.alert(successMessage);
}

function ideClipbroadApiCopy(data: string, event: Event, successMessage: string) {
    const clip = new ClipboardEvent("copy");
    clip.clipboardData.setData( "text/plain", data );
    clip.preventDefault();
    event.target.dispatchEvent(clip);

    window.alert(successMessage);
}

function promptCopy(data: string, event: Event, successMessage: string) {
    window.prompt("Copy to clipboard: Ctrl+C, Enter", data);
}

function copyToClipboard2(o: HTMLInputElement) {
    o.focus();
    o.select();
    o.createTextRange().execCommand("copy");
}
