const $ = jQuery;

function transformResponse(text:string) :any {
    try {
        return JSON.parse(text,
            (key:string, value:any) => {
                if (typeof value === "string") {
                    // parse ASP.NET DateTime type
                    {
                        const result = /\/Date\((-?[0-9]+)(?:[a-zA-Z]|(?:\+|-)[0-9]{4})?\)\//g.exec(value);
                        if (!!result) { return new Date(parseFloat(result[1])); }
                    }
                }
                return value;
            }
        );
    } catch (e) { return text; }
}

export function serviceGen(serviceUrl?:string) {
    if (!serviceUrl) serviceUrl = window.location.pathname;

    return function service<T>(method: string, data?:{}):Promise<T> {
        return new Promise<T>((fulfil, reject) => {
            $.ajax({
                url: `${serviceUrl}/${method}`,
                method: "POST",
                data: ((data == null) ? "" : JSON.stringify(data)),
                contentType: "application/json; charset:utf-8",
                dataType: "json",
                converters: { "text json": transformResponse }
            })
            .done((data: { d: T }) => { fulfil(data.d); })
            .fail(function (jqXHR: JQueryXHR, textStatus: string, errorThrown: string) {
                if (jqXHR.status == 403) {
                    const url = jqXHR.getResponseHeader("Location");
                    if (!!url) window.location.href = url;
                }
                if (jqXHR.responseJSON != null && jqXHR.responseJSON.Message != null) {
                    console.error(jqXHR.responseJSON);
                    reject(jqXHR.responseJSON.Message);
                }

                reject(errorThrown);
            });
        });
    };
}
