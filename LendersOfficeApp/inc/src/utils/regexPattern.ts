export const regexPattern = {
    // https://github.com/angular/angular.js/blob/master/src/ng/directive/input.js
    ISO_DATE_REGEXP      : /^\d{4,}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+(?:[+-][0-2]\d:[0-5]\d|Z)$/,
    URL_REGEXP           : /^[a-z][a-z\d.+-]*:\/*(?:[^:@]+(?::[^@]+)?@)?(?:[^\s:/?#]+|\[[a-f\d:]+\])(?::\d+)?(?:\/[^?#]*)?(?:\?[^#]*)?(?:#.*)?$/i,
    EMAIL_REGEXP         : /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i,
    NUMBER_REGEXP        : /^\s*(\-|\+)?(\d+|(\d*(\.\d*)))([eE][+-]?\d+)?\s*$/,
    DATE_REGEXP          : /^(\d{4,})-(\d{2})-(\d{2})$/,
    DATETIMELOCAL_REGEXP : /^(\d{4,})-(\d\d)-(\d\d)T(\d\d):(\d\d)(?::(\d\d)(\.\d{1,3})?)?$/,
    WEEK_REGEXP          : /^(\d{4,})-W(\d\d)$/,
    MONTH_REGEXP         : /^(\d{4,})-(\d\d)$/,
    TIME_REGEXP          : /^(\d\d):(\d\d)(?::(\d\d)(\.\d{1,3})?)?$/,
};
