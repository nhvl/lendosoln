export class GroupByResult<TElement> {
    key: string;
    values: Array<TElement>;
}

// Adapted from tomitrescak comment at https://stackoverflow.com/a/34890276
export default function groupBy<TElement extends { [indexer: string]: any }>(elements: Array<TElement>, key: string | Function): Array<GroupByResult<TElement>> {
    return elements.reduce(function (rv: Array<GroupByResult<TElement>>, x: TElement) {
		let v = key instanceof Function ? key(x) : x[key];
        let el: GroupByResult<TElement> = rv.find((r: any) => r && r.key === v) as GroupByResult<TElement>;

		if (el) {
			el.values.push(x);
        }
        else {
			rv.push({
				key : v,
				values : [x]
			});
        }

		return rv;
	}, []);
}