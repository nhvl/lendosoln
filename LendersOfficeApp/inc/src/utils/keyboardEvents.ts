export function on_Ctrl_S(func: () => boolean) {
    const keys_S = 83;

    $(document).on("keydown.shortkey", (event: JQueryKeyEventObject) => {
        const is_Ctrl_S = (event.ctrlKey && !event.altKey && event.keyCode === keys_S);
        if (!is_Ctrl_S) return true;

        return func();
    });
}
