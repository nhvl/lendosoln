﻿export const documentReadyPromise = new Promise((resolve) => {
    if (document.readyState === "complete") {
        resolve();
    } else {
        document.addEventListener("DOMContentLoaded", onReady, true);
        window.addEventListener("load", onReady, true);
    }

    function onReady() {
        resolve();
        document.removeEventListener("DOMContentLoaded", onReady, true);
        window.removeEventListener("load", onReady, true);
    }
});
