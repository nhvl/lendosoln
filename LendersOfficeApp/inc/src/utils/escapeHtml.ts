const entityMap: { [key: string]: string } = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;'
};

export function escapeHtml(str: string): string {
    // https://github.com/janl/mustache.js/blob/master/mustache.js

    return String(str).replace(/[&<>"'\/]/g, (s: string) => entityMap[s]);
}