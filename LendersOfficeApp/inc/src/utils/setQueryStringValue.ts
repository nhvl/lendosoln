export function setQueryStringValue(url: string, name: string, value:string) :string {
    var regex = new RegExp("([?&])" + name + "(=([^&#]*)|&|#|$)");

    if (url.match(regex)) {
        return url.replace(regex, '$1' + name + "=" + value + '$2');
    }
    else {
        let separatorIndex = url.indexOf('?') ;
        if (separatorIndex !== -1) {
            let index = separatorIndex + 1;
            return url.slice(0, index) + name + "=" + value +"&" + url.slice(index);
        } else {
            return url + '?' + name + "=" + value;
        }
    }
}
