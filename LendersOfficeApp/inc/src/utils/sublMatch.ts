// https://stackoverflow.com/a/16908326
export function sublMatch(search: string, text: string) {
    search = search.toUpperCase();
    text = text.toUpperCase();

    let j = -1; // remembers position of last found character

    // consider each search character one at a time
    for (let i = 0; i < search.length; i++) {
        let l = search[i];
        if (l == ' ') continue;     // ignore spaces

        j = text.indexOf(l, j + 1);     // search for character & update position
        if (j == -1) return false;  // if it's not found, exclude this item
    }
    return true;
}
