import {parse, format} from "url";

export function getUrlWithQueryString(strUrl:string, qs:object) {
    const u = parse(strUrl, true);
    u.query = Object.assign({}, u.query, qs);
    u.search = undefined;
    return format(u);
}

export function getQueryString(strUrl:string = location.href): {[key:string]: string} {
    const u = parse(strUrl, true);
    return u.query;
}