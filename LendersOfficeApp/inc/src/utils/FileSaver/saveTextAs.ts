import {saveAs} from "file-saver";

function endsWithAny(s: string, ...sf: string[]) {
    var $this = s.toLowerCase().toString();
    for (var i = 0; i < sf.length; i++) {
        if ($this.indexOf(sf[i], $this.length - sf[i].length) !== -1) return true;
    }
    return false;
};

export function saveTextAs(textContent: string, fileName: string = "download.txt", charset: string = "utf-8") {
    textContent = (textContent || "").replace(/\r?\n/g, "\r\n");
    if (saveAs && Blob) {
        const blob = new Blob([textContent], { type: `text/plain;charset=${charset}` });
        saveAs(blob, fileName);
        return true;
    } else {//IE9-
        let saveTxtWindow:Window|null = (window.frames as any).saveTxtWindow;
        if (!saveTxtWindow) {
            const tSaveTxtWindow = document.createElement("iframe");
            tSaveTxtWindow.id = "saveTxtWindow";
            tSaveTxtWindow.style.display = "none";
            document.body.insertBefore(tSaveTxtWindow, null);
            saveTxtWindow = (window.frames as any).saveTxtWindow;
            if (!saveTxtWindow) {
                saveTxtWindow = window.open("", "_temp", "width=100,height=100");
                if (!saveTxtWindow) {
                    window.alert("Sorry, download file could not be created.");
                    return false;
                }
            }
        }

        const doc = saveTxtWindow.document;
        doc.open("text/html", "replace");
        doc.charset = charset;
        if (endsWithAny(fileName, ".htm", ".html")) {
            doc.close();
            doc.body.innerHTML = "\r\n" + textContent + "\r\n";
        } else {
            if (! endsWithAny(fileName, ".txt")) fileName += ".txt";
            doc.write(textContent);
            doc.close();
        }
        const retValue = doc.execCommand("SaveAs", undefined, fileName);
        saveTxtWindow.close();
        return retValue;
    }
}
