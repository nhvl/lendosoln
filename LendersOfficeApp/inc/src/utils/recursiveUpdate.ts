export function recursiveUpdate(x: any, p: (x: any) => [boolean, any]): any {
    const [i, y] = p(x);
    if (i) return y;

    if (x == null) return null;

    if (Array.isArray(x)) return recursiveUpdateArray(x, p);
    if (typeof x === "object") return recursiveUpdateObject(x, p);

    return x;
}

function recursiveUpdateArray(xs: any[], p: (x: any) => [boolean, any]) {
    return xs.map(x => recursiveUpdate(x, p));
}

function recursiveUpdateObject(xs: { [key: string]: any }, p: (x: any) => [boolean, any]): { [key: string]: any } {
    const ys: { [key: string]: any } = {};
    Object.keys(xs).forEach((k: string) => {
        ys[k] = recursiveUpdate(xs[k], p);
    });
    return ys;
}

type IRecursiveTraversalP = (x: any, path: (string|number)[]) => {handled:boolean, terminate:boolean};

export function recursiveTraversal(x: any, p:IRecursiveTraversalP, path: (string|number)[] = []): boolean {
    const {handled, terminate} = p(x, path);
    if (handled) return terminate;

    if (x == null) return false;

    if (Array.isArray(x)) return recursiveTraversalArray(x, p, path);
    if (typeof x === "object") return recursiveTraversalObject(x, p, path);

    return false;
}

function recursiveTraversalArray(xs: any[], p: IRecursiveTraversalP, path: (string|number)[]): boolean {
    return xs.some((x, i) => recursiveTraversal(x, p, path.concat([i])));
}

function recursiveTraversalObject(xs: { [key: string]: any }, p: IRecursiveTraversalP, path: (string|number)[]): boolean {
    return Object.keys(xs).some((k: string) => recursiveTraversal(xs[k], p, path.concat([k])));
}
