﻿export {arrayToDictionary};

declare global {
    interface Array<T> {
        toDictionary<TValue>(
            keyFunc: (x: T) => string,
            valueFunc ?: (x: T) => TValue) : { [key:string]: TValue };
    }
}

function arrayToDictionary<TSource, TValue>(
    xs: TSource[],
    keyFunc: (x: TSource) => string,
    valueFunc ?: (x: TSource) => TValue) : { [key:string]: TValue } {
    return xs.reduce((d: { [key: string]: TValue }, e:TSource) => {
        const key = keyFunc(e);
        const value = valueFunc == null ? e as any : valueFunc(e);
        console.assert(d[key] === undefined, "", xs);
        d[key] = value;
        return d;
    }, <{ [key: string]: TValue }>{});
}

Array.prototype.toDictionary = function <TSource, TValue>(
    keyFunc: (x: TSource) => string,
    valueFunc ?: (x: TSource) => TValue) : { [key:string]: TValue } {
    return arrayToDictionary(this, keyFunc, valueFunc);
}
