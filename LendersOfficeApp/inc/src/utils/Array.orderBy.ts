﻿export {orderBy};

declare global {
    interface Array<T> {
        orderBy(p: (string|((x: T) => any)), desc ?: boolean):T[];
    }
}

Array.prototype.orderBy = function<T> (p: (string|((x: T) => any)), desc: boolean = false) {
    return orderBy(this, p, desc);
}

function orderBy<T>(xs:T[], p: (string|((x: T) => any)), desc: boolean = false):T[] {
    const comp = ((typeof p === "string") ?
        compareFuncGen(p, desc) :
        compareFuncGen2(p, desc));
    return (xs.sort(comp));
}

function compareFuncGen<T>(p: string, desc: boolean = false) {
    return function (x: T, y: T) {
        const u = (<{[p:string]: (string|number) }> <any> x)[p];
        const v = (<{ [p: string]: (string|number) }> <any> y)[p];
        const d = ((typeof u === "string") ?
            u.localeCompare(<string> v) :
            ((u < v) ? -1 :
            ((u > v) ? 1 : 0)));
        return (desc ? -d : d);
    };
}

function compareFuncGen2<T>(f: (x: T) => any, desc: boolean = false) {
    return function (x: T, y: T) {
        const u = f(x);
        const v = f(y);
        const d = ((typeof u === "string") ?
            u.localeCompare(v) :
            ((u < v) ? -1 :
            ((u > v) ? 1 : 0)));
        return (desc ? -d : d);
    };
}

