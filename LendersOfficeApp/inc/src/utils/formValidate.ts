﻿/* Usage:

HTML: <input> <select> and <textarea>
```html
< input required data-required="true" />
```

javascript
```js
$("form").on("submit", function(){
    var form = this;
    if (!(formValidate(form)) {

        // Form NOT VALID

    }

    return false;
});
```
*/

// return is form valid or not
export function formValidate(form: HTMLFormElement): boolean {
    const xs = $(form).find("input,textarea,select")
        .filter(":visible")
        .filter(function() {
            const $input = $(this);
            if (!!($input.attr("data-required"))) console.log(this, $input.attr("data-required"));
            return (
                ($input.prop("required") === true) ||
                (!!($input.attr("data-required")))
            );
        });
    console.debug("formValidate: xs =", xs.toArray());

    xs.tooltip("destroy");

    const ys = xs.filter((i: number, x: HTMLElement) => !((x as HTMLInputElement).value.trim()));
    console.debug("formValidate: ys =", ys.toArray());

    if (ys.length > 0) {
        const y = ys.first();
        y.tooltip({ title: "Required." });
        y.focus();
        y.one("change blur input", function (this: HTMLInputElement) { $(this).tooltip("destroy"); });
        return false;
    }

    return true;
}
