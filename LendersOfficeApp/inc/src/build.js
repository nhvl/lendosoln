/*
In command line, run

node build

node build watch

*/
"use strict";

const pages = [
    // "LOAdmin/Broker/BrokerEdit"                  ,
    // "LOAdmin/Broker/BrokerList"                  ,
    // "LOAdmin/Broker/DeletedLoans"                ,
    // "LOAdmin/Broker/FindUser"                    ,
	"LOAdmin/Broker/WorkflowOperations",
    // "LOAdmin/BrowserUsageStatistics"             ,
    // "LOAdmin/CacheTableRequestErrors"            ,
    // "LOAdmin/ConsumerPortal/FindConsumers"       ,
    // "LOAdmin/ConsumerPortal/ViewConsumer"        ,
    // "LOAdmin/Internal/AdminList"                 ,
    // "LOAdmin/Internal/ChangePassword"            ,
    // "LOAdmin/Internal/PriceServerList"           ,
	// "LOAdmin/Internal/QueueBacklogAutoEscalation",
    // "LOAdmin/Internal/StageConfigAdmin"          ,
    // "LOAdmin/MasterCRAList"                      ,
    // "LOAdmin/Manage/GenericFrameworkVendors"     ,
    // "LOAdmin/Manage/ManageDocumentVendors"       ,
    // "LOAdmin/Manage/SecurityQuestions"           ,
    // "LOAdmin/PricingPerformancePage"             ,
    // "LOAdmin/SAE/DownloadList"                   ,
    // "LOAdmin/SAE/InvestorXlsFileList"            ,
    // "LOAdmin/SAE/OutputFiles"                    ,
    // "LOAdmin/SAE/PlanCodeEditor"                 ,
    // "LOAdmin/SAE/RateSheetVersion"               ,
    // "LOAdmin/Test/TestCenter"                    ,
    // "LOAdmin/Test/TestCreditPage"                ,

    // "LoanEditor"                                 ,
    // "LoanEditor/Pipeline"                        ,
    // "LoanEditor/LoanEditorBe"                    ,
    ].map(s => s.split("/"));

const browserify = require("browserify");
const tsify = require("tsify");
const config = require("./tsconfig.json").compilerOptions;
    delete config.sourceMap; delete config.inlineSourceMap;
const fs = require('fs');
const path = require("path");
const watchify = require("watchify");

const browserifyOpts = { debug:true };

if (process.argv.slice(2).indexOf("watch") < 0){
    for (var page of pages) build(page);
} else {
    watch(pages[0]);
}

function build(page) {
    const src = path.join(...page, "app.ts");
    console.log("src = %s", src);

    const outFile = page.join(".")+".js";
    const mapFile = outFile+".map";

    const dest = path.join("..", outFile);

    const wstream = fs.createWriteStream(dest);

    browserify(src, browserifyOpts)
        .plugin('tsify', config)
        .transform('brfs')
        //.transform('stripify')
        .plugin('minifyify', {
            map   : mapFile,
            output: path.join("..", mapFile),
        })
        .bundle()
        .on('error', function (error) { console.error(error.toString()); })
        .pipe(wstream);
}

function watch(page) {
    const src = path.join(...page, "app.ts");
    console.log("watching [%s]", src);

    const outFile = page.join(".")+".js";
    const mapFile = outFile+".map";

    const dest = path.join("..", outFile);
    console.log("will create [%s]", dest);

    const w = watchify(
        browserify(src, Object.assign({}, browserifyOpts, watchify.args))
        .plugin('tsify', config)
        .transform('brfs')
    );

    var bytes, time;
    w.on('bytes', function (b) { bytes = b });
    w.on('time', function (t) { time = t });

    w.on('update', bundle);
    bundle([src]);

    function bundle(ids) {
        console.log("on Update: %s", ids.join(" "));

        var didError = false;
        var outStream = fs.createWriteStream(dest);

        var wb = w.bundle();
        wb.on('error', function (err) {
            console.error(String(err));
            didError = true;
            outStream.end('console.error('+JSON.stringify(String(err))+');');
        });
        wb.pipe(outStream);

        outStream.on('error', function (err) {
            console.error(err);
        });
        outStream.on('close', function () {
            if (!didError) {
                console.error(`${bytes} bytes written to ${dest} ( ${(time / 1000).toFixed(2)} seconds)`);
            }
        });
    }
}

