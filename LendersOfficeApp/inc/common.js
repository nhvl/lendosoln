var gIsDebug = false;
var gHighlightRowColor = "Bisque"; // Color of highlighted row.
var gReadonlyBackgroundColor = 'lightgrey';
var gHighlightColor = 'yellow';
var oldTabIndex = 0;
var gVirtualRoot = document.getElementById("VirtualRoot") == null ? "" : document.getElementById("VirtualRoot").value;
var gIsInputRangeValid = true;
var gInputInitializedAttr = 'CommonInputInitialized';
var UnloadMessage = "Leave site?  Changes you made may not be saved.";

//---- Put a transparent layer on top of the current screen.
function f_disabledInputForBlockMask() {
  var coll = document.getElementsByTagName('select');
  for (var i = 0; i < coll.length; i++) {
    var o = coll[i];    
    o._blockTabIndex = o.tabIndex;
    o.tabIndex = -1;
  }
  var oTaggableTags = new Array("INPUT", "A", "BUTTON", "TEXTAREA", "IFRAME");
  for (var i = 0; i < oTaggableTags.length; i++) {
    var oList = document.getElementsByTagName(oTaggableTags[i]);
    for (var j = 0; j < oList.length; j++) {
      var o = oList[j];
      o._blockTabIndex = o.tabIndex;
      o.tabIndex = -1;
    }  
  }
}
function f_enabledInputForBlockMask() {
  var coll = document.getElementsByTagName('select');
  for (var i = 0; i < coll.length; i++) {
    var o = coll[i];
    o.tabIndex = o._blockTabIndex;
  }
  var oTaggableTags = new Array("INPUT", "A", "BUTTON", "TEXTAREA", "IFRAME");
  for (var i = 0; i < oTaggableTags.length; i++) {
    var oList = document.getElementsByTagName(oTaggableTags[i]);
    for (var j = 0; j < oList.length; j++) {
      var o = oList[j];
      o.tabIndex = o._blockTabIndex;
    }  
  }
}
function f_getViewportHeight() {
    if (window.innerHeight != window.undefined) return window.innerHeight;
    
    var retHeight = -1;
    if (document.documentElement && document.documentElement.clientHeight)
        retHeight = document.documentElement.clientHeight;
    if (document.body && document.body.clientHeight)
        retHeight = Math.max(retHeight, document.body.clientHeight);

    if (retHeight == -1)
        return window.undefined;
    else
        return retHeight;
}
function f_getViewportWidth() {
	if (window.innerWidth!=window.undefined) return window.innerWidth;

	var retWidth = -1;
	if (document.documentElement && document.documentElement.clientWidth)
	    retWidth = document.documentElement.clientWidth;
	if (document.body && document.body.clientWidth)
	    retWidth = Math.max(retWidth, document.body.clientWidth);

	if (retWidth == -1)
	    return window.undefined;
	else
	    return retWidth;
}
function f_getScrollLeft() {
    var doc = document.documentElement;
    var body = document.body;
    var left = (doc && doc.scrollLeft || body && body.scrollLeft || 0);
    return left;
}
function f_getScrollTop() {
    var doc = document.documentElement;
    var body = document.body;
    var top = (doc && doc.scrollTop || body && body.scrollTop || 0);
    return top;
}
function f_displayBlockMask(bDisplay) {
  var sUniqueId = '__BlockMask210587__'
  var oBlockMaskContent = document.getElementById(sUniqueId);
  if (null == oBlockMaskContent && bDisplay) {
    
    var oBody = document.getElementsByTagName("BODY")[0];
    oBlockMaskContent = document.createElement("div");
    oBlockMaskContent.id = sUniqueId;
    oBlockMaskContent.className = 'BlockMask';
    oBody.appendChild(oBlockMaskContent);
    
  } else  if (null != oBlockMaskContent && !bDisplay) {
    oBlockMaskContent.style.display = 'none';
  }
  
  if (bDisplay) {
    f_disabledInputForBlockMask();
    f_resizeBlockMask();
    addEventHandler(window, "scroll", f_resizeBlockMask);
    addEventHandler(window, "resize", f_resizeBlockMask);

  } else {
  f_enabledInputForBlockMask();
    removeEventHandler(window, "scroll", f_resizeBlockMask);
    removeEventHandler(window, "resize", f_resizeBlockMask);
  }
  }
  
function f_resizeBlockMask() {
  var sUniqueId = '__BlockMask210587__'
  var oBlockMaskContent = document.getElementById(sUniqueId);

  oBlockMaskContent.style.display = '';
  oBlockMaskContent.style.width = f_getViewportWidth() + 'px';
  oBlockMaskContent.style.height = f_getViewportHeight() + 'px';
  oBlockMaskContent.style.left = f_getScrollLeft() + 'px';
  oBlockMaskContent.style.top = f_getScrollTop() + 'px';

}

//---- END Put a transparent layer on top of the current screen.


// return true if browser is IE 7.0+ on Windows.
function checkWinIE() {
  var ret = checkWinIE_NoAlert();
  if (!ret) {
      alert("LendingQB requires Microsoft Internet Explorer 7.0 or later.");
    // 8/6/2004 dd - To prevent non-IE user from generate error we force them to logout if they don't have IE.
    parent.location = "/logout.aspx"; 
  }
  return ret;
}

function checkWinIE_Version7() {
	var version = getBrowserVersion();
	if (version < 7)
		return false;
	return true;
}

function checkWinIE_NoAlert() {

    var missingAll = !document.all;

    if (missingAll)
    {
        try {
            missingAll = !document.all.length;
        }
        catch (e)
        { }
    }

    if ((typeof (window.showModalDialog) != 'object' && typeof (window.showModalDialog) != 'function') || missingAll)
	  return false; // Return false. THis browser is not IE.
	  
	var version = getBrowserVersion();

	if (version<7.0)
		return false;
		
	return true;
}

function checkWinIE_NoAlert_Old() {
  var userAgent = navigator.userAgent.toLowerCase();
  var isIE = document.all && userAgent.indexOf("msie") != -1 && userAgent.indexOf("opera") == -1;
  if (!isIE || navigator.platform.toLowerCase() != "win32") {
 
    return false;
  }
  var version=0;
	if (navigator.appVersion.indexOf("MSIE")!=-1)
	{
		var temp=navigator.appVersion.split("MSIE");
		version=parseFloat(temp[1]);
	}
	if (version<7.0)
		return false;
   
  return true;
}

function isInternetExplorer() {
    if (isIEButEdge()) return true;
    
    var ua = window.navigator.userAgent;

    var edge = ua.indexOf('Edge/');
    if (edge >= 0) {
        // Edge (IE 12+)
        return true;
    }

    return false;
}

function isIEButEdge(){
    // Modified from https://codepen.io/gapcode/pen/vEJNZN
    if (window.navigator.appName === 'Microsoft Internet Explorer') {
        return true;
    }
   
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE');
    if (msie >= 0) {
        // IE 10 or older
        return true;
    }

    var trident = ua.indexOf('Trident/');
    if (trident >= 0) {
        // IE 11
        return true;
    }
    
    return false;
}

//3/8/2007 db - trim whitespace from front and end of string
function trim(stringVal) {
	if(stringVal)
		return stringVal.replace(/^\s*|\s*$/g,"");
	else
		return stringVal;
}

// ------------------- Zero - One CheckBoxList

function _registerCBL(name, c) {
  for (var i = 0; i < c; i++) {
    var o = document.getElementById(name + "_" + i);
    if (null == o) continue;
    addEventHandler(o, "click", function(event){
        onZeroOneCheckboxClick(name, c, event);
    });
  }
}
// The hidden variables use to store the value of checkbox list will have prefix of 'cbl_'.
// If there is no prefix then error will occur when normal postback occur on CheckBoxList. dd 12/8/03
function onZeroOneCheckboxClick(name, count, event) {
  event = event || window.event;
  var cb = retrieveEventTarget(event);
  var b = cb.checked;
  for (var i = 0; i < count; i++) {
    document.getElementById(name + "_" + i).checked = false;
  }
  cb.checked = b;    
  var index = cb.name.substr(name.length + 1, cb.name.length - name.length);
  if (document.forms[0]["cbl_" + name] != null)
    document.forms[0]["cbl_" + name].value = b ? eval(name +"_items[" + index + "]") : "";
}
// ---- End Zero - One CheckBoxList
function TextAreaMaxLength(fieldObj,maxChars)
{
  var diff = maxChars - fieldObj.value.length;
  if (diff < 0) fieldObj.value = fieldObj.value.substring(0,maxChars);
}
function parseMoneyFloat(str) 
{
  var a = str.indexOf("(") >= 0 ? -1 : 1;
  str = str.replace(/[,$%()]/g, "");
  if (str == "") str = "0";
  return parseFloat(str) * a;
}

function f_getCharToAttachQueryStringToURL(url)
{
    if (url.indexOf('?', 0) > -1) {
        return '&';
    }
    else{
        return '?';
    }
}

function displayHelp()
{
    var helpUrl = 'https://support.lendingqb.com/imgstor/imagestore/KBSCPDF_Accessing the LendingQB Support Center PDF.pdf';
    var handle = window.open(helpUrl, 'help', 'resizable=yes,scrollbars=yes,menubar=no,toolbar=yes,height=550pt,width=785pt') ;
    if (handle != null) handle.focus() ;
}

function populateForm(obj, clientID) {
  var start = (new Date()).getTime();
  var count = 0;
  
  var prefix = (clientID == null || clientID == "") ? "" : clientID + "$";
  if (obj["sFileVersion"] != null)
  {
      var o = document.getElementById("sFileVersion");
      if (null != o) o.value = obj["sFileVersion"];
  }

  var coll = document.forms[0];
  for (property in obj) {
    var field = coll[prefix + property];

    if (field != null) {
      if (field.tagName == null) { // A radio button list.
        for (index = 0; index < field.length; index++)
          field[index].checked = field[index].value == obj[property];
      } else if (field.type == 'checkbox') {
        field.checked = obj[property] == 'True';
      } else {
        field.value = obj[property];
      }
    }
    count++;
    
  }
  var stop = (new Date()).getTime();
  if (gIsDebug)
    alert("Time executed in populateForm is " + (stop - start) + " ms. Count = " + count + ", Fields count=" + coll.length);
}

var g_skipFormNames = /VirtualRoot|IsUpdate|__VIEWSTATE|__EVENTTARGET|__EVENTARGUMENT|__EVENTVALIDATION/;
// Usage:
// getAllFormValues() will collect all input on current page and pack into an object.
// This could lead to performance problem if there are many input on the page. To skip
// unnessary input then add the attribute SkipMe to input tag (ie. <input type=text SkipMe>) and invoke
// getAllFormValues('', true); dd 7/25/2003
function getAllFormValues(clientID, skipIgnore)
{
  var start = (new Date()).getTime();
  var obj = new Object();
  var coll = document.forms[0];
  var collLength = coll.length;
  var count = 0;
  var clientIdPrefix = clientID + "_";

  var staticVariables = ['loanid', 'applicationid', 'sFileVersion'];
  var staticVariablesLength = staticVariables.length;
  for (var idx = 0; idx < staticVariablesLength; idx++)
  {
      var o = staticVariables[idx];
      if (document.getElementById(o) != null)
      {
          obj[o] = document.getElementById(o).value;
      }
  }
  for (var i = 0; i < collLength; i++) {
    var f = coll[i];

    if (null == f.name) continue;

    var name = f.name.replace(/:/g, "_").replace(/\$/g, "_"); // : will create a invalid xml string.
    if (name == "" || g_skipFormNames.test(name))
    {
        continue;
    }
    
    if (name == "_ClientID") {
      obj[name] = f.value;
      continue;
    }
    if (f.type == 'submit' || f.type == 'button')
      continue;
    if (skipIgnore && (f.SkipMe != null || f.getAttribute('SkipMe') != null)) {
      continue;
    }


    if (clientID != null && clientID != "" && !name.match(clientIdPrefix))
      continue; // Skip name if it does not belong to this control's clientID
    if (clientID != null && clientID != "")
        name = name.replace(clientIdPrefix, ""); // Remove clientID_ in front of input name.
      
    if (f.tagName == null) { // A radio button list.

      for (index = 0; index < f.length; index++)
        if (f[index].checked) {
          obj[name] = f[index].value;
        }
    } else if (f.type == 'checkbox') {
      //obj[f.name] = f.checked;
      obj[name] = f.checked ? 'True' : 'False';
      //alert("Check box of " + f.name + " = " + obj[f.name] + ", " + f.checked);
    } else if (f.type == 'radio') {
      if (f.checked)
        obj[name] = f.value; // Only set value for radio box when checked.
    } else {
      obj[name] = f.value;
    }      
    count++;

  }
  var stop = (new Date()).getTime();
  if (gIsDebug)
    alert("Time executed in getAllFormValues is " + (stop - start) + " ms. Count = " + count);  
  return obj;
}


var _postGetAllFormValuesCallbacks = [];
var _postPopulateFormCallbacks = [];

function registerPostGetAllFormValuesCallback(funcToRegister) {
    for (var i = 0; i < _postGetAllFormValuesCallbacks.length; i++) {
        if (_postGetAllFormValuesCallbacks[i] == funcToRegister) return false;
    }
    _postGetAllFormValuesCallbacks.push(funcToRegister);
    return true;
}

function registerPostPopulateFormCallback(funcToRegister) {
    for (var i = 0; i < _postPopulateFormCallbacks.length; i++) {
        if (_postPopulateFormCallbacks[i] == funcToRegister) return false;
    }
    _postPopulateFormCallbacks.push(funcToRegister);
    return true;
}


function executePostGetAllFormValuesCallback(args) {
    for (var i = 0; i < _postGetAllFormValuesCallbacks.length; i++) {
        if (_postGetAllFormValuesCallbacks[i]) {
            _postGetAllFormValuesCallbacks[i](args);
        }
    }
}

function executePostPopulateFormCallback(args) {
    for (var i = 0; i < _postPopulateFormCallbacks.length; i++) {
        if (_postPopulateFormCallbacks[i]) {
            _postPopulateFormCallbacks[i](args);
        }
    }
}

function getAllFormValuesWithPostCallbacks(clientId, skipIgnore) {
    var args = getAllFormValues(clientId, skipIgnore);

    if (performGetAllFormValuePostFunctions)
    {
        performGetAllFormValuePostFunctions(args);
    }

    return args;
}
function debugObject(o) {
  var str = '';
  var i = 0;
  for (p in o)
    if (p != 'outerText' && p != 'innerHTML' && p != 'innerText' && p!= 'outerHTML')
      str += 'o.' + p + ' = ' + o[p] + (i++ % 10 == 0 ? '\n\r' : ';');
  alert(str);
}  
function highlightRowByCheckbox(o, bgColor) {
  // Highlight row if checkbox is check.
  if (o.checked != null && o.checked) {
    highlightRow(o, bgColor);
  } else {
    unhighlightRow(o, "");
  }
}
function highlightRow(o, bgColor) {
  // Highlight the inner most <tr> tag.
  if (null == o) return;
  var e = o;
  while (e != null && e.tagName != null && e.tagName.toLowerCase() != "body") {
    if (e.tagName.toLowerCase() == "tr") {
      e.previousBackgroundColor = e.style.backgroundColor;
      bgColor = bgColor == null ? gHighlightRowColor : bgColor;
      e.style.backgroundColor = bgColor;
      break;
    }
    e = e.parentElement;
  }
}
function unhighlightRow(o, bgColor) {
  // unhighlight the inner most <tr> tag.
  if (null == o) return;
  var e = o;
  

  while (e != null && e.tagName != null && e.tagName.toLowerCase() != "body") {
    if (e.tagName.toLowerCase() == "tr") {
      if (null != bgColor)
        e.style.backgroundColor = bgColor;
      else
        e.style.backgroundColor= e.previousBackgroundColor == null ? "" : e.previousBackgroundColor;
      break;
    }
    e = e.parentElement;
  }

}

function smartZipcodeAuto(event) {
    var zipcode = retrieveEventTarget(event);
    if (zipcode == null) {
        return;
    }

    var retrieveLinkedInputForZipCode = function (zipcode, inputType) {
        var inputId = zipcode.getAttribute(inputType);
        if (inputId) {
            return document.getElementById(inputId);
        }

        return null;
    }

    var city = retrieveLinkedInputForZipCode(zipcode, "city");
    var state = retrieveLinkedInputForZipCode(zipcode, "state");
    var county = retrieveLinkedInputForZipCode(zipcode, "county");

    smartZipcode(zipcode, city, state, county, event);
}

function smartZipcode(zipcode, city, state, county, event) {
    event = event || window.event;
    if (gService == null || gService.utils == null || zipcode == null) return;
    // 3/3/04 dd - Don't perform lookup if zipcode does not change from previous value.//av 18913
    if (typeof(zipcode.oldValue) != null && zipcode.oldValue == zipcode.value)
    {
        if ( county != null && typeof(county.value) != null && county.value != "" ) 
            return; 
        else if ( county == null ) 
            return;
	
    }

    updateDirtyBit(event);

    var args = new Object();
    args.GetCounties = county != null && county.nodeName.toLowerCase() == "select" ? "true" : "false" ;
    
    args.zipcode = zipcode.value;
    gService.utils.callAsyncSimple("smartzipcode", args, function (result) { smartZipcode_postSeviceCall(result, zipcode, city, state, county) });
}

function smartZipcode_postSeviceCall(result, zipcode, city, state, county) {
  if (!result.error) {
    var val = result.value;
    if (val.IsValid == "1") {
    	if (state != null) {
			state.oldValue = val.State;
			state.value = val.State;
		}
		if (city != null) {
			if( state.selectedIndex != -1 )
			{
				city.value = val.City;
			} 
		}

		if (county != null && county.nodeName.toLowerCase() != "select") { 
			county.value = val.County;
		}
		
		else if ( county != null )  {
	
			county.options.length = 0; 
			
			if ( state.selectedIndex != -1 ) {
				county.options[0] = new Option( "", ""); 
				for( i = 0; i < val.CountyCount; i++ ) {
					p = "val.County" + i; 
					countyName = eval( p );
					selected = countyName.toLowerCase() == val.County.toLowerCase();
					county.options[i+1] = new Option( countyName, countyName, false, selected ); 
				}
			}

		}
        zipcode.oldValue = zipcode.value;
        var evt = createEvent("change", true, false);
        dispatchEvent(zipcode, evt);
    }
    else  if ( county != null && county.nodeName.toLowerCase() == "select" )  
    {
	
		county.selectedIndex =  0;
    }
  }

    // NOTE: The PML and ConsumerPortal versions of this function triggers the zipcode element's onChange event to handle callback duties.
    // I went with using a callback function for the LO version because in LO the smartZipcode function is often called from an element's
    // onchange event, where as in PML smartZipcode often gets called from the blur event.
  if (typeof(smartZipcode_callback) == 'function') {
      // Define this method on page if you need to run scripts after call to smartZipCode.
      smartZipcode_callback();
  }
}

function UpdateCounties(state,county, event, callback)
{
  event = event || window.event;

	if (gService == null || gService.utils == null || state == null || county == null) return;
    if (typeof(state.oldValue) != null && state.oldValue == state.selectedIndex ) return;


	updateDirtyBit(event);
	var args = new Object();
	args.state = state.options[state.selectedIndex].value;
	county.options.length = 0;

	if (args.state == "") {
	    return;
	}
	else {
        gService.utils.callAsyncSimple("GetCountiesForState", args, function (result) {
	        if (result.error)
	            return;

	        val = result.value;
	        county.options[0] = new Option("", "");
	        for (i = 0; i < val.CountyCount; i++) {
	            p = "val.County" + i;
	            countyName = eval(p);
	            county.options[i + 1] = new Option(countyName, countyName, false, false);
	        }

	        if (typeof (callback) == 'function') {
	            callback();
	        }
	    });
	}
}


// 7/11/2006 mb - js function for inputrange attribute, focus() on window
// until user enters valid value.

function f_validateRange(event) {
  event = event || window.event;
	var o = retrieveEventTarget(event);
	
	if (null == o)
	  return;
	
	if (o.readOnly)
	  return;
	    
	var bValid = true;
	var sErrorMessage = '';
	
	if ( o.min != null && o.max != null )
	{
	  if (o._initial == o.value)
	    return; // 8/14/2006 dd - Skip validation if value does not change from initial load.
	    
	  	var temp		= o.value.replace(/[,$\)]/g,'');
		temp = temp.replace(/\(/g, '-');
		var compare = parseFloat(temp);
		
		if (o.def == compare)
			return; // 05/08/08 fserra - Skip validation if value is equal to the default allowed one.
		
		var minimum = parseFloat(o.min);
		var maximum = parseFloat(o.max);
		if ( o.clu == "inc" )
		{			
			if ( compare < minimum || compare > maximum )
			{
			  bValid = false;
			  sErrorMessage = 'Value Error: Please enter a value between ' + minimum + ' and ' + maximum;
			}
		}
		else if ( o.clu == "exc" )
		{
			if ( compare <= minimum || compare >= maximum )
			{
			  bValid = false;
			  sErrorMessage = 'Value Error: Please enter a value between ' + (++minimum) + ' and ' + (--maximum) ;
			}
		}
	}
	
	if ( o.sList != null )
	{
		var temp	= o.value.replace('$','');
		temp		= temp.replace(/,/g,'');
		var compare = temp.toLowerCase();
		
		// mb - building accStr to let user know acceptable values
				
		var accStr = "Value Error: Acceptable Values are: ";
		
		var accept = o.sList.split("|");
		var part_num = 0;		

		while (part_num < accept.length)
		{
			accStr = accStr + accept[part_num] + ', ';
						
			part_num+=1;
		}	
		
		accStr = accStr + '\n' + '\n' + 'you entered: ' + compare;
		
		var bool = false;
		part_num = 0;
		while (part_num < accept.length)
		{
			if ( compare == accept[part_num] )
			{
				bool = true;
			}
						
			part_num+=1;
		}
		
		if ( bool == false )
		{
		  bValid = false;
		  sErrorMessage = accStr;
		}
		
	}

	gIsInputRangeValid = bValid;
	if (!bValid) {
	  alert(sErrorMessage);
	  event.returnValue = false;
	  o.focus();
	  o.select();
	}


}
function parentHasInfo() {
    try {
        return ((typeof (parent.info) != 'undefined') && (parent.info != null));
    }
    catch (e) {
        return false;
    }
}
function updateDirtyBit(event) {
  event = event || window.event;
  if ( null != event )
  {
	var src = retrieveEventTarget(event);
	var key = event.keyCode;
	if (null != src && (src.readOnly || src.disabled)) return; // DO NOT update dirty bit if field is readonly.
	if (null != src && src.hasAttribute && src.hasAttribute('doesntDirty')) return; // DO NOT update dirty bit if the field doesnt dirty anything.
    if (null != key && (key == 9 || key == 16 || key == 17)) return; // DO NOT update when user tabs. 09/12/06 mf OPM 7310. Updated 11/10/09 for opm 32915 vm - DO NOT update on ctrl
    if (null != key && event.ctrlKey && key == 67) return; // DO NOT update on Ctrl + C. 11/10/09 vm opm 32915
  }

  if (document.getElementById("_ReadOnly") != null && document.getElementById("_ReadOnly").value == "True")
    return; // 11/4/2004 dd - Temporary disregard all request for update dirty bit when _ReadOnly is set.
// Enable save button on the upper left frame. This only apply to /los/loanapp.aspx.
    if (parentHasInfo() && typeof (parent.info.f_enableSave) == 'function')
        parent.info.f_enableSave(true);


  if (document.getElementById("IsUpdate") != null) document.getElementById("IsUpdate").value = "1";

  if (typeof(updateDirtyBit_callback) == 'function') {
    // Define this method on page when you want notify when dirty bit is set.
    updateDirtyBit_callback(event);
  }  
}
function isDirty() {
    if (parentHasInfo() && typeof (parent.info.f_AppReload) == 'function') {
        if (parent.info.f_AppReload())
            return false;
    }
  if (document.getElementById("IsUpdate") != null) return document.getElementById("IsUpdate").value == "1";
  else return true;
}
function isReadOnly() {
    var r = document.getElementById('_ReadOnly');
  if (r != null) return r.value == "True";
  else return false;
}
function clearDirty() {
    // Enable save button on the upper left frame. This only apply to /los/loanapp.aspx.
  if (parentHasInfo() && typeof (parent.info.f_enableSave) == 'function')
    parent.info.f_enableSave(false);

  if (document.getElementById("IsUpdate") != null) document.getElementById("IsUpdate").value = "0";
  if (typeof (clearDirty_callback) == 'function') {
    // Define this method on page when you want notify when dirty bit is clear.
    clearDirty_callback();
  }    
}
function formatReadonlyField(o) {
    // 12/10/2004 dd - Hack so that combobox is white even when ReadOnly. Only if NotEditable is set. Only screen affect is Status/Funding.aspx
  if (o.getAttribute("NotEditable") == null)
    o.style.backgroundColor = gReadonlyBackgroundColor;
  o.oldTabIndex = o.tabIndex;
  o.tabIndex = -1;
}
function highlightField(event) {
  var e = retrieveEventTarget(event);
  if (e.NoHighlight != null || (e.getAttribute && e.getAttribute('NoHighlight') )) return; // 5/26/2004 dd - Explicitly request by component to not highlight
  
  if (!e.readOnly && e.tagName.toLowerCase() != 'select') {
      e.style.backgroundColor = gHighlightColor;
      if (typeof (e.select) != "undefined" && e.type != "textarea" && e.type != 'button' && e.type != 'submit') {
          setTimeout(function () {
              if (document.activeElement == e) {
                  e.select();
              }
          }, 0);
    }
  }

  if (gIsDebug || self.location.host == "localhost") 
    window.status = "Field ID: " + e.id;
}
function cancelMouseWheel(event) {
    if(event.preventDefault) {
        event.preventDefault();
    }
  // Only work in IE 6.0+
  event.returnValue = false;
}
function unhighlightField(event) {
    try {
        var e = retrieveEventTarget(event);
        if (e.NoHighlight != null) return; // 5/26/2004 dd - Explicitly request by component to not highlight
        if (!e.readOnly && e.tagName.toLowerCase() != 'select') e.style.backgroundColor = '';
    } catch (err) {
    }
}
function attachCommonEvents(o, readOnly) {
    readOnly = readOnly == null  ? isReadOnly() : readOnly; 
    if (readOnly) {
        if ((o.type == 'button') && o.getAttribute("AlwaysEnable") == null) o.disabled = true;

        // 08/24/06 mf 4901 - We allow ddls and textbox to be have AlwaysEnable,
        // so let them have events.
        if ( ! ( (o.tagName.toLowerCase() == 'select' || o.type == 'text' ) && o.getAttribute("AlwaysEnable") != null ) )
            return;
  
    }

    // 10/26/2007 dd - In IE7, if you highlight the background of drop down list, you will need to click twice to expand the list
    addEventHandler(o, "focus", highlightField, false);
    addEventHandler(o, "blur", unhighlightField, false);
  
    // Disable OPM 6125 for now
    if (o.type == 'text')
        addEventHandler(o, "blur", f_validateRange, false);
  
    if (o.tagName.toLowerCase() == 'select') {
        if (o.AllowMouseWheel == null) {
            addEventHandler(o, "mousewheel", cancelMouseWheel, false);
        }
    }
    // Change from 'onchange' to 'onkeyup'. As soon as user enter text in input
    // modify dirty bit.

    if (o.getAttribute("NotForEdit") == null) {
        if (o.type == 'checkbox' || o.type == 'radio') {
            addEventHandler(o, "click", updateDirtyBit, false);
        }
        else if (o.tagName.toLowerCase() == 'select') {
            addEventHandler(o, "change", updateDirtyBit, false);
        }
        else {
            addEventHandler(o, "keydown", updateDirtyBit, false);
            addEventHandler(o, "paste", updateDirtyBit, false);
        }
    }
}

function hasAttr(tag, attrName) {
    return tag.hasAttribute ? tag.hasAttribute(attrName)
                            : !!(tag.attributes[attrName] && tag.attributes[attrName].specified);
}

var validateFieldIdCache = {};

function event_keydown(event) {
    // Because IE will convert Backspace into a Back browse button. Therefore I will disable the backspace key.
    // Backspace only enable on INPUT type=text/password and TEXTAREA. dd
    // Added support for contenteditable elements. mp
    if (event.keyCode == 8) { // Backspace key press
        var o = retrieveEventTarget(event);
        //debugObject(o);
        if (o != null && ((o.tagName == "INPUT" && (o.type == "text" || o.type == "password" || o.type == "file")) || (o.tagName == "TEXTAREA") || o.contentEditable)) {
            if (o.readOnly || o.disabled) return false; // If textbox is readonly or disable then also disable backspace
            else return true; // Don't disable backspace
        } else {
            return false; // Disable backspace key
        }
    }
    
    if (event.ctrlKey && event.altKey && event.keyCode == 72) {
        if (null != gService && null != gService.utils) {
            var w = window.open(gVirtualRoot + '/common/ReportSurvey.aspx', 'Feedback', 'width=500, height=600,status=yes,toolbar=no,menubar=no,location=no');
            if (w != null) {
                w.focus();
            }
            return;
        }
    } else if (event.ctrlKey && event.altKey && event.keyCode == 81) {
        // Ctrl + Alt + Q - Copy object id to clipboard
        event.returnValue = false;
        var o = retrieveEventTarget(event);
        if (o == null) return false;

        var id = o.id;
        var prefix_match = /^(.+\$).+$/.exec(o.name);
        if (document.forms[0]["_ClientID"] != null && document.forms[0]["_ClientID"].value != "") {
            id = id.replace(document.forms[0]["_ClientID"].value + "_", "");
        }
        // Handle situations where we can't use the field name as the ID, 
        // and have to specify the field name manually.
        if (hasAttr(o, 'data-field-id')) {
            id = o.getAttribute('data-field-id');
        }
        // Otherwise, try to handle situations where ascx inclusion mangles the ID.
        else if (prefix_match != null) {
            var prefix = prefix_match[1].replace(/\$/g, "_");
            if (id.indexOf(prefix) == 0) {
                id = id.substring(prefix.length, id.length);
            }
        }
        
        if (!id) return true;
        
        if (validateFieldIdCache[id] != null) {
            // setTimeout in IE because directly calling copyToClipboard then IE will not prevent default event even return false
            if (isIEButEdge()) setTimeout(copyIdToClipboard, 100, validateFieldIdCache[id]);
            else copyIdToClipboard(validateFieldIdCache[id]);
            return false;
        }
        
        if (gService == null || gService.utils == null) {
            if (isIEButEdge()) setTimeout(copyIdToClipboard, 100, id);
            else copyIdToClipboard(id);
            return false;
        }
        
        // Chrome allow execCommand("copy") in setTimeout context
        var _id = null;
        var copyTimer = null;
        (function tryCopy() {
            if (_id == null) { return copyTimer = setTimeout(tryCopy, 300); }
            copyIdToClipboard(id);
        })();
        
        gService.utils.callAsyncSimple("ValidateFieldId", { Id: id }, function (result) {
            if (!!result.error) return;
            
            var rId = result.value.ActualId || '';

            if (!!rId) {
                validateFieldIdCache[id] = rId;
                _id = rId;
            } else {
                alert('Not a valid loan field id.');
                clearTimeout(copyTimer);
            }
        });
        return false;
    }
}

function _initInput() {
    addEventHandler(document, "keydown", event_keydown, false);
    var coll = document.getElementsByTagName("INPUT"), 
        readOnly = isReadOnly(),
        i = coll.length - 1;
  
  if(i > -1) {
      do {
        var o = coll[i];
        if (o.getAttribute(gInputInitializedAttr)) {
            continue;
        }
        if (o.readOnly) formatReadonlyField(o);
        if (o.getAttribute("preset") != null && typeof(_initMask) == 'function') _initMask(o);
        attachCommonEvents(o, readOnly);
        addAttributeChangeObserver(o, ['readOnly'], event_onpropertychange);
        o.setAttribute(gInputInitializedAttr, true);
    }while( i--); 
  }
  
  coll = document.getElementsByTagName("SELECT");
  i = coll.length - 1;
  
  if(i > - 1 ) {
      do {
      var o = coll[i];
      if (o.getAttribute(gInputInitializedAttr)) {
          continue;
      }
      if (o.disabled && !o.getAttribute("NotEditable")) disableDDL(o, true);
      attachCommonEvents(o, readOnly);
      o.setAttribute(gInputInitializedAttr, true);
    }while(i--);
  }
    
  coll = document.getElementsByTagName("TEXTAREA");
  i = coll.length - 1;
  if (i > -1) {
    do {
        var o = coll[i];
        if (o.getAttribute(gInputInitializedAttr)) {
            continue;
        }
        if (o.readOnly) formatReadonlyField(o);
        attachCommonEvents(o, readOnly);
        o.setAttribute(gInputInitializedAttr, true);
    }while(i--);
  }
  // While init some input set dirtybit.
  // clear dirty bit after finish init. dd 6/4/2003
  clearDirty();
}

function _initDynamicInput(o, doNotClearDirtyBit) {
    if (o.getAttribute(gInputInitializedAttr)) {
        return;
    }
    if (o.readOnly) formatReadonlyField(o);
    if (o.getAttribute("preset") != null && typeof(_initMask) == 'function') _initMask(o);
    attachCommonEvents(o);
    addAttributeChangeObserver(o, ['readOnly'], event_onpropertychange);
    
    // 09/14/17 - JE - I'm guessing the original idea was that dynamically created inputs
    // would always be created on init, hence why we didn't want the dirty bit set.
    // That's not always the case, especially where we can add row to tables with an
    // "Add" button. So I'm adding the option to NOT clear the dirty bit.
    if (!doNotClearDirtyBit) {
        clearDirty();
    }

    o.setAttribute(gInputInitializedAttr, true);
}
  
function event_onpropertychange(event, element, attributeName) {
    if (event) {
        element = retrieveEventTarget(event);
        attributeName = event.propertyName;
    }

    if (attributeName.toLowerCase() == "readonly") {
        if (isReadOnly() && element.getAttribute("AlwaysEnable") == null && !element.readOnly) element.readOnly = true;
        if (element.readOnly) formatReadonlyField(element);
        else {
            element.style.backgroundColor = "";
            element.tabIndex = element.oldTabIndex != null ? element.oldTabIndex : 0;
        }
    }
}

function disableDDL(ddl, b) {
  ddl.disabled = b;
  ddl.style.backgroundColor = b ? gReadonlyBackgroundColor : "";  
}
function event_onkeyup(event) {
  // Allow us to press Ctrl + Alt + S to save the source file of the current window.
  // This will help us to debug the source on modal dialog. To disable the save just
  // comment out the code.
  
  //alert('Ctrl = ' + event.ctrlKey + ', Alt = ' + event.altKey + ', keycode = ' + event.keyCode);
  if (event.ctrlKey && event.altKey && event.keyCode == 83) {
    window.document.execCommand('SaveAs', true);
  } else if (event.ctrlKey && event.altKey && event.keyCode == 86) {
     // Ctrl + Alt + V - Display viewstate size.
     if (window.document.getElementById("__VIEWSTATE") != null) {
       alert("ViewState size = " + window.document.getElementById("__VIEWSTATE").value.length);
     } else 
       alert("No viewstate available");
  } else if (event.ctrlKey && event.altKey && event.keyCode == 82) {
    // Ctrl + Alt + R - Resize window to 800x600. Unfortunately this will force the page to reload.
      parent.resizeTo(800,600);
  } else if (event.ctrlKey && event.keyCode == 83) {
    // Ctrl + S - Save loan information.
    if (typeof(f_saveMe) == 'function') f_saveMe();
  } else if (event.ctrlKey && event.altKey && event.keyCode == 68) {
    // Ctrl + Alt + D - Toggle gIsDebug
    var msg = 'You are in IE ' +  document.documentMode + (document.compatMode==='CSS1Compat'?' Standards':' Quirks') + ' mode.\nURL: ' + document.location;
    if (ML && ML.server){
        msg += ' server: ' + ML.server;
    }
    alert(msg);
    gIsDebug = !gIsDebug;
    if (gIsDebug)
      displayObnoxiousColor();
    alert('Debug for this frame is ' + gIsDebug);
  } else if (event.ctrlKey && event.altKey && event.keyCode == 66) {
     //ctrl-alt-b: show current window size
     alert(document.documentElement.offsetWidth + " x " + document.documentElement.offsetHeight);
  } else if (event.ctrlKey && event.altKey && event.keyCode == 69) {
      //ctrl-alt-e: copy current url and selected field if any to clipboard.
      var toClipboard = location.href.split("#", 1)[0];

      if (document.activeElement != document.body) {
          toClipboard = toClipboard + '\t' + document.activeElement.id;
      }
      
      copyStringToClipboard(toClipboard);
      alert("Copied [" + toClipboard + "] to clipboard");
  } else {
      if (typeof (f_customEventKeyup) == 'function') f_customEventKeyup(event);
  }
}

function displayObnoxiousColor() {
  var pattern = /(TextBox|SSNTextBox|DropDownList|DateTextBox|CheckBox|CheckBoxList|PhoneTextBox|ZipcodeTextBox|ComboBox|PercentTextBox|MoneyTextBox|PhoneTextBox)\d/;
  var coll = document.getElementsByTagName("input");
  var length = coll.length;
  if (length > 0) {
    for (i = 0; i < length; i++) {
      var o = coll[i];
      if (o.id.match(pattern))
        o.style.backgroundColor = 'blue';
    }
  }
  coll = document.getElementsByTagName("select");
  length = coll.length;
  if (length > 0) {
    for (i = 0; i < length; i++) {
      var o = coll[i];
      if (o.id.match(pattern))
        o.style.backgroundColor = 'blue';
    }
  }
  coll = document.getElementsByTagName("textarea");
  length = coll.length;
  if (length > 0) {
    for (i = 0; i < length; i++) {
      var o = coll[i];
      if (o.id.match(pattern))
        o.style.backgroundColor = 'blue';
    }
  }
  
}

// Define rolodex here. 
//
function cRolodex() {
  this.chooseFromRolodex = cRolodex_ChooseFromRolodex;
  this.addToRolodex = cRolodex_AddToRolodex;
  this.populateFromOfficialContactList = cRolodex_PopulateFromOfficialContactList;
}

// Display a list from rolodex. If 'type' is define then rolodex will set the filter
// on the tab by default.
// Return:
//
//     var rolodex = new cRolodex();
//     var args = rolodex.chooseFromRolodex();
//
//        args.OK
//        args.DisplayName // Name use to display in alert.
//        args.AgentName
//        args.AgentType
//        args.AgentStreetAddr
//        args.AgentCity
//        args.AgentState
//        args.AgentZip
//        args.AgentAltPhone
//        args.AgentCompanyName
//        args.AgentEmail
//        args.AgentFax
//        args.AgentPhone
//        args.AgentTitle 
//        args.AgentDepartmentName
//        args.AgentPager 
//        args.AgentLicenseNumber
//        args.AgentNote
//        args.PhoneOfCompany;
//        args.FaxOfCompany;
//        args.CompanyLicenseNumber
//        args.IsNotifyWhenLoanStatusChange  // '1' for true.
//        args.CommissionPointOfLoanAmount
//        args.CommissionPointOfGrossProfit
//        args.CommissionMinBase
//        args.TaxID

function cRolodex_ChooseFromRolodex(type, loanid, isFeeContactPicker, isCurrentAgentOnly, handler) {
  var top = findTopWithShowModal(window);
  if (top.showModal == null) {
    alert("showModal is not define. Please include /common/ModalDlg/CModalDlg.js in your page.");
    var args = new Object();
    args.OK = false;
    return args;
  }

  if (isFeeContactPicker == null) {
      isFeeContactPicker = false;
  }
      
  return top.showModal('/los/RolodexList.aspx?type=' + type + '&loanid=' + loanid + '&IsFeeContactPicker=' + isFeeContactPicker + '&isCurrentAgentOnly=' + isCurrentAgentOnly, null, null, null, handler, {hideCloseButton: true});
  
  function findTopWithShowModal(win){
    if (win.parent !== win) {
      var top = findTopWithShowModal(win.parent);
      if (top != null) return top;
    }
    return win.showModal != null && win.document.body.tagName === "BODY" ? win : null;
  }
}
function cRolodex_PopulateFromOfficialContactList(sLId, agentRoleT, callback) {
    if (gService == null || gService.utils == null) {
        alert("Your page need to inherit from LendersOffice.Common.BaseServicePage");
        return;
    }
    var args = new Object();
    args["LoanID"] = sLId;
    args["AgentRoleT"] = agentRoleT;
    gService.utils.callAsyncSimple("PopulateFromOfficialContactList", args, function (result) { PopulateFromOfficialContactList_postServiceCall(result, callback) });
}

function PopulateFromOfficialContactList_postServiceCall(result, callback) {
  var ret = new Object();
  if (!result.error) {

    ret.AgentName = result.value["AgentName"];
    ret.AgentStreetAddr = result.value["AgentStreetAddr"];
    ret.AgentCity = result.value["AgentCity"];
    ret.AgentState = result.value["AgentState"];
    ret.AgentZip = result.value["AgentZip"];
    ret.AgentCounty = result.value["AgentCounty"];
    ret.AgentCompanyName = result.value["AgentCompanyName"];
    ret.AgentEmail = result.value["AgentEmail"];
    ret.AgentFax = result.value["AgentFax"];
    ret.AgentPhone = result.value["AgentPhone"];
    ret.AgentTitle = result.value["AgentTitle"];
    ret.AgentDepartmentName = result.value["AgentDepartmentName"];
    ret.AgentPager = result.value["AgentPager"];
    ret.AgentLicenseNumber = result.value["AgentLicenseNumber"];
    ret.PhoneOfCompany = result.value["PhoneOfCompany"];
    ret.FaxOfCompany = result.value["FaxOfCompany"];
    ret.CompanyLicenseNumber = result.value["CompanyLicenseNumber"];
    ret.LoanOriginatorIdentifier = result.value["LoanOriginatorIdentifier"];
    ret.CompanyLoanOriginatorIdentifier = result.value["CompanyLoanOriginatorIdentifier"];
    ret.TaxID = result.value["TaxID"];
    ret.AgentSourceT = result.value["AgentSourceT"];
    ret.BrokerLevelAgentID = result.value["BrokerLevelAgentID"];
    
    // OPM 150537 - Added fields for funding page
    ret.BranchName = result.value["BranchName"];
    ret.PayToBankName = result.value["PayToBankName"];
    ret.PayToBankCityState = result.value["PayToBankCityState"];
    ret.PayToABANumber = result.value["PayToABANumber"];
    ret.PayToAccountName = result.value["PayToAccountName"];
    ret.PayToAccountNumber = result.value["PayToAccountNumber"];
    ret.FurtherCreditToAccountNumber = result.value["FurtherCreditToAccountNumber"];
    ret.FurtherCreditToAccountName = result.value["FurtherCreditToAccountName"];
    ret.CompanyId = result.value["CompanyId"];
  }

  if (typeof (callback) == 'function') {
      callback(ret);
  }
}

function cRolodex_PackageAddToRolodexResult(result) {
    var ret = new Object;
    if (!result.error) {
        ret.BrokerLevelAgentID = result.value["BrokerLevelAgentID"];
    }
    return ret;
}
function cRolodex_AddToRolodex(args, packagedResult) {
    if (gService == null || gService.utils == null) {
        alert("Your page need to inherit from LendersOffice.Common.BaseServicePage");
        return false;
    }
    
    args.AddCommand = "AddIfUnique";

    gService.utils.callAsyncSimple("AddToRolodex", args, function (result) { AddToRolodex_postServiceCall(result, args); });
}

function AddToRolodex_postServiceCall(result, args) {
  var name, comp, disp;

  packagedResult = cRolodex_PackageAddToRolodexResult(result);

  name = args.AgentName;
  comp = args.AgentCompanyName;

  if( name != null && name != "" && comp != null && comp != "" )
  {
    disp = "Agent: " + name + " of company " + comp;
  }
  else if( name != null && name != "" )
  {
    disp = "Agent: " + name;
  }
  else if( comp != null && comp != "" )
  {
    disp = "Entry: " + comp;
  }
  else
  {
    disp = "Entry";
  }

  if (!result.error)
  {
    if (result.value.AddResult == "Collided")
    {
      var o = new Object();

      if( result.value.ReasonWhy != null && result.value.ReasonWhy != "" )
      {
		    o.Msg = disp + " is already in your Contacts.<br><br>" + result.value.ReasonWhy;
      }
      else
      {
		    o.Msg = disp + " is already in your Contacts.";
	    }
      showModal("/inc/customdia.htm", o, "dialogHeight:200px;dialogwidth:450px;", null, function(o){
          var ret = o.ret;
      
          if (ret == 6) {
            args.AddCommand = "Overwrite";
          } else if (ret == 7) {
            args.AddCommand = "Add";
          } else {
            return false; 
          }

          gService.utils.callAsyncSimple("AddToRolodex", args, function (result2) {
              packagedResult = cRolodex_PackageAddToRolodexResult(result2);

              if (!result2.error && result2.value.AddResult == "Added") {
                  if (args.AddCommand == "Overwrite") {
                      alert(disp + " has been replaced in Contacts.");
                      return true;
                  }
                  else {
                      alert(disp + " added to Contacts.");
                      return true;
                  }
              }
              else if (!result2.error && result2.value.AddResult == "Denied") {
                  alert("Access denied.  You do not have permission to add contact entries.  Please consult your account administrator if you have any questions.");
                  return false;
              }
              else {
                  alert("Unable to add " + disp + " to Contacts.");
                  return false;
              }
          },{width:450,height:200});
      });
    }
	else if( result.value.AddResult == "Denied" )
	{
	    alert("Access denied.  You do not have permission to add contact entries.  Please consult your account administrator if you have any questions.");
	    return false;
	}
	else
    {
        var lic = '\r\nPlease note that the contact record does not store state licenses.\r\nThe licenses included in the Licenses tabs have not been propagated to the new contact.';
        alert(disp + " added to Contacts." + lic);
        return true;
    }
  }
  else
  {
      alert("Unable to add " + disp + " to Contacts.");
      return false;
  }
}

function pageY(elem) {
    return elem.offsetParent ? (elem.offsetTop + pageY(elem.offsetParent)) : elem.offsetTop;
}


//usage call on iframe load and on window resize. buffer is how many px before the absolute bottom the iframe should 
//grow
function f_resizeIframe(id, buffer) {
    if (!buffer) { buffer = 10; }
    var iframe = document.getElementById(id);
    var height = self.innerHeight || (document.documentElement.clientHeight || document.body.clientHeight);
    height -= pageY(iframe)+ buffer ;
    height = (height < 0) ? 0 : height;
    iframe.style.height = height + 'px';
}

// calendar
var gCalendar;
function selectedDate(cal, date, event) {
    event = event || window.event;

    cal.sel.value = date;
    cal.callCloseHandler();
    if (typeof (updateDirtyBit) == 'function') updateDirtyBit(event);

    var evt = createEvent("change", true, false);
    dispatchEvent(cal.sel, evt);
}
function closeCalHandler(cal) {
    cal.hide();
}
function displayCalendar(field, closeHandler) {
    var el = document.getElementById(field);
    if (el.readOnly || el.disabled)
        return false;
        
    if (gCalendar != null) {
        gCalendar.hide();
        gCalendar.onClose = closeHandler || gCalendar.onClose;
    } else {
        var cal = new Calendar(false, null, selectedDate, closeHandler || closeCalHandler);
        gCalendar = cal;
        cal.setRange(1900, 2070);
        cal.setDateFormat('mm/dd/y');
        cal.create();
    }
    gCalendar.sel = el;
    var dateVal = gCalendar.sel.value;
    var dateObj = new Date(dateVal);

    if (dateVal != '' && !isNaN(dateObj.valueOf())) {
        gCalendar.sel.blur();
        gCalendar._init(false, dateObj);
    }
    gCalendar.showAtElement(el);
    return false;
}
function getTimeStamp(user) {
    var currentTime = new Date();
    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();
    if (minutes < 10) {
        minutes = '0' + minutes;
    }
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var pm = hours > 11 ? 'PM' : 'AM';
    hours = hours % 12;
    if (hours == 0) {
        hours = 12;
    }

    return month + '/' + day + '/' + year + ' ' + hours + ':' + minutes + ' ' + pm + ' [' + user + '] - ' + "\r\n\r\n";
}

function copyStringToClipboard(stringData){
  if (window.clipboardData) {  // Internet Explorer
      window.clipboardData.setData('Text', stringData);
  } else if (document.body) {
      var input = document.createElement("textarea");
      input.value = stringData;
      document.body.appendChild(input);
      
      input.select();
      document.execCommand("copy");
      input.parentNode.removeChild(input);
  } else {
    window.prompt("Copy to clipboard: Ctrl+C, Enter", stringData);
  }
}
// getAllElementsByIdFromParent and getElementByIdFromParent is used for replacing queryselector and queryselector all in Quirk mode
function getAllElementsByIdFromParent(parent, elementId){
    var collection = [];
    var allElements = parent.getElementsByTagName("*");
    for (var i = 0, n = allElements.length; i < n; ++i) {
      var el = allElements[i];
      if (el.id == elementId) { collection.push(el); }
    }
    
    return collection;
}

function getElementByIdFromParent(parent, elementId){
  var allElements = parent.getElementsByTagName("*");
  for (var i = 0, n = allElements.length; i < n; ++i) {
    var el = allElements[i];
    if (el.id == elementId) { return el;}
  }
  
  return null;
}
function retrieveFrame(windowObj, id) {
    if (windowObj && windowObj.frames) {
        if (windowObj.frames[id]) {
            return windowObj.frames[id];
        }

        for (i = 0; i < windowObj.frames.length; i++) {
            var frame = windowObj.frames[i];
            if (frame && frame.name == id) {
                return frame;
            }
        }
    }

    return undefined;
}

function retrieveFrameProperty(windowObj, id, propertyName)
{
    var frame = retrieveFrame(windowObj, id);
    if(frame) {
        var frameContent = frame.contentWindow || frame;
        return frameContent[propertyName];
    }
}

function setFrameProperty(windowObj, id, propertyName, propertyValue)
{
    var frame = retrieveFrame(windowObj, id);
    if (frame) {
        var frameContent = frame.contentWindow || frame;
        frameContent[propertyName] = propertyValue;
        return true;
    }

    return false;
}

// Enter all arguments as an array
function callFrameMethod(windowObj, id, methodName, args) {
    var method = retrieveFrameProperty(windowObj, id, methodName);
    if (method) {
        return method.apply(retrieveFrame(windowObj, id), args || []);
    }
}

function swapNodePoly(element1, element2) {
    var parent1 = element1.parentElement;
    var parent2 = element2.parentElement;
    var sibling1 = element1.nextElementSibling || element1.nextSibling;

    if (element2 == sibling1) {
        parent1.insertBefore(element2, element1);
    }
    else if (element1 == element2.nextElementSibling) {
        parent1.insertBefore(element1, element2);
    }
    else {
        parent2.insertBefore(element1, element2);
        if (sibling1) {
            parent1.insertBefore(element2, sibling1);
        }
        else {
            parent1.appendChild(element2);
        }
    }
}

function moveRowPoly(tbody, from, to)
{
  if(typeof tbody.moveRow === "function"){
    tbody.moveRow(from, to);
  }
  else {
    var trFrom = tbody.rows[from]; // Make sure row stays referenced
    tbody.removeChild(trFrom); // Remove the row before inserting it (dupliate id's etc.)
    if (to == tbody.rows.length)
    {
        tbody.appendChild(trFrom);
    }
    else {
        var trTo = tbody.rows[to];
        tbody.insertBefore(trFrom, trTo);  
    }
  }
}

function f_getWindowDimensions(currentWindow) {
    var $doc = jQuery(currentWindow.document.documentElement);
    var $body = jQuery(currentWindow.document.body);
    var $win = jQuery(currentWindow);
    return {
        height: currentWindow.innerHeight || $body.innerHeight(),
        width: currentWindow.innerWidth || $body.innerWidth(),
        left: ($doc.scrollLeft() || $body.scrollLeft()),
        top: ($doc.scrollTop() || $body.scrollTop())
    };
}

function f_generateQueryString(obj) {
    var o = obj || '';
    var str = '';
    for (var p in o) {
        if (o.hasOwnProperty(p)) {
            str += p + '=' + encodeURIComponent(o[p]) + '&';
        }
    }
    return str;
}

function copyIdToClipboard(id){
    if (copyToClipboard(id)) {
        var activeElement = document.activeElement;
        alert("Field id: " + id + " is copied to your windows clipboard.");
        if (activeElement != null) activeElement.focus();
    } else {
        // window.prompt("Copy to clipboard: Ctrl+C, Enter", id);
    }
}
function copyToClipboard(text) {
    {
        var copySuccess;
        
        var activeElement = document.activeElement;
        var el = document.createElement("textarea");
        try {
            el.value = text;
            el.setAttribute('readonly', '');
            el.className = "move-offscreen";
            document.body.appendChild(el);
            el.select();
            
            copySuccess = document.execCommand("copy");
        }
        finally {
            document.body.removeChild(el);
            if (activeElement != null) activeElement.focus();
        }
        
        if (copySuccess) return true;
    }
    
    return false;
}


function injectArgumentsToWindow(newWindow, args){
	if (newWindow == null){
		return;
	}

	var a = args || new Object;
	a.opener = window;
    a.OK     = false;
	newWindow.modelessArgs = a;
	newWindow.opener = window;
}

function openWindowWithArguments(url, title, features, args){
    var convertModelessFeaturesToWindowOpenFeatures = function(showModelessFeature){
        if (typeof showModelessFeature != "string"){
            return null;
        }
    
        var args = showModelessFeature;
        args = args.replace(/;/g, ",");
        args = args.replace(/:/g, "=");
        args = args.replace(/px/g, "");
        args = args.replace(/dialogWidth/g, "width");
        args = args.replace(/dialogHeight/g, "height");
        return args;
    };

	var w = window.open(url, title, convertModelessFeaturesToWindowOpenFeatures(features))
	setTimeout(function () {
        injectArgumentsToWindow(w, args);
    });
	return w;	
}

function addTrimmingToAspRegExValidator(validator) {
    validator.evaluationfunction = TrimmingRegularExpressionValidatorEvaluateIsValid;
}

function TrimmingRegularExpressionValidatorEvaluateIsValid(validator) {
    var $control = $("#" + validator.controltovalidate);
    $control.val($.trim($control.val()));

    return RegularExpressionValidatorEvaluateIsValid(validator);
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


function resizeToScreen() {
    var w = screen.availWidth;
    var h = screen.availHeight;
    window.moveTo(0, 0);
    window.resizeTo(w, h);
    window.focus();
}

function openMaxSizeWindow(url, target, features, replace) {
    var w = screen.availWidth - 10;
    var h = screen.availHeight - 50;
    var featuresAddon = 'width=' + w + 'px,height=' + h + 'px,left=0px,top=0px,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes';
    features = features ? features + "," + featuresAddon : featuresAddon;
    window.open(url, target, features, replace);
}

function HtmlEncode(text) {
    var div = document.createElement('div');
    div.innerText = text;
    return div.innerHTML;
}

function HtmlUnencode(html) {
    var div = document.createElement('div');
    div.innerHTML = html;
    return div.innerText;
}

function JsEncode(text) {
    var encodedText = [];
    for (var i = 0; i < text.length; i++) {
        var regex = /[\w\d\s]/g;
        var char = text.charAt(i)
        if (!regex.test(char)) {
            encodedText.push('\\x' + text.charCodeAt(i).toString(16));
        }
        else {
            encodedText.push(char);
        }
    }

    return encodedText.join('');
}


function redirectToUladIfUladPage(url, onlyRedirectIfUlad) {
    var isUlad = callFrameMethod(window.top, 'treeview', 'isUladLoan');
    if (isUlad) {
        var uladPageName = callFrameMethod(window.top, 'treeview', 'getUladPageNameFromUrl', [url]);
        if (uladPageName != '') {
            redirectToUladPage(uladPageName);
            return true;
        }
    }
    else if (!onlyRedirectIfUlad) {
        linkMe(url);
        return true;
    }

    return false;
}

function redirectToUladPage(pageName, appId) {
    callFrameMethod(window.top, 'treeview', 'loadUladPage', [pageName, appId]);
}

function hypescriptDom(tagName, props, children) {
    var element = tagName == "<>" ? document.createDocumentFragment() : document.createElement(tagName);
    if (props) {
        for (var item in props) {
            if (item == "attrs") {
                var attrs = props["attrs"];
                if (attrs != null && typeof attrs == "object")
                    for (var a in attrs) element.setAttribute(a, attrs[a]);
                continue;
            }
            element[item] = props[item];
        }
    }
    if (children) {
        children = [].concat(children);
        for (var i = 0; i < children.length; i++) {
            var child = children[i];
            if (child == null || child === false) continue;
            if (isNode(child)) { element.appendChild(child); continue; }
            element.appendChild(document.createTextNode(String(child)));
        }
    }
    return element;
    
    function isNode(o){
        return (
            typeof Node === "object" ? o instanceof Node : 
            o && typeof o === "object" && typeof o.nodeType === "number" && typeof o.nodeName==="string"
        );
    }
}




function setCookie(sName, sValue)
{
    document.cookie = encodeURIComponent(sName) + "=" + encodeURIComponent(sValue)+ "; path=/; expires=Fri, 31 Dec 2024 23:59:59 GMT;";
}

function getCookie(cookieName)
{
    var encodedCookieName = encodeURIComponent(cookieName);
	var aCookie = document.cookie.split("; ");
	var aCrumb ;
	for (var i = aCookie.length - 1 ; i >=0 ; i--)
	{
		aCrumb = aCookie[i].split("=");
		if(encodedCookieName === (aCrumb[0]))
		{
            var cookieValue = decodeURIComponent(aCrumb[1]);
            return cookieValue == "" ? null : cookieValue;
		}
    }
    
	return null ;
}

function hasCookie(cookieName){
    return getCookie(cookieName) !== null;
}

function compareCookie(cookieName, compareValue){
    return getCookie(cookieName) == compareValue;
}