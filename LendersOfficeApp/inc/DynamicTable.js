﻿var DynamicTable = {
    SetReadOnly: function(table) {
        var isReadOnly = document.getElementById('_ReadOnly').value == 'True';
        var rows = table.tableData.rows;
        for (var i = 0; i < rows.length; i++) {
            var inputs = rows[i].getElementsByTagName('input');

            for (var x = 0; x < inputs.length; x++) {
                var input = inputs[x];
                if (input.type === 'hidden')
                    continue;
                if (input.type === 'checkbox')
                    input.disabled = isReadOnly;
                else
                    input.readOnly = isReadOnly;
            }

            var selects = rows[i].getElementsByTagName('select');
            for (var x = 0; x < selects.length; x++)
                selects[x].disabled = isReadOnly;
        }

        document.getElementById(table.selectAllCheckbox).disabled = isReadOnly;
    },

    instantiateTable: function(table, data) {
        table.tableData = $('#' + table.tableName + ' > tbody').get(0);
        this.calImg.src = gVirtualRoot + '/images/pdate.gif';

        if (!data || data.length < 1)
            return;

        for (var i = 0; i < data.length; i++)
            this.__createAndAppendRow(data[i], false, table);

        table.currentIndex = data.length;
        this.SetReadOnly(table);
    },

    __createAndAppendRow: function(data, newRow, table) {
        table.__createAndAppendRow(table.currentIndex, data, newRow);

        this.uncheckDeleteAllCheckbox(table);
        table.currentIndex++;
        this.setClass(table);
    },

    deleteSelected: function(table) {
        $('#' + table.tableName + ' tbody input[DeleteEntry]:checked').each(function(i, e) {
            table.deleteRow($(e));
        });

        update();
        this.uncheckDeleteAllCheckbox(table);
        this.setClass(table);
    },

    setDeleteCheckboxes: function(table) {
        var inputs = $('#' + table.tableName + ' tbody input[DeleteEntry]');
        inputs.prop('checked', $('#' + table.selectAllCheckbox).is(':checked'));

        this.setDeleteButtonStatus(table);
    },

    setDeleteButtonStatus: function(table) {
        var btn_delete = $('#' + table.deleteRowBtn);
        btn_delete.prop('disabled', $('#' + table.tableName + ' tbody input[DeleteEntry]:checked').length < 1);
    },

    uncheckDeleteAllCheckbox: function(table) {
        $('#' + table.selectAllCheckbox).prop('checked', false);
        this.setDeleteButtonStatus(table);
    },

    setClass: function(table) {
        var tBody = table.tableData;
        for (var i = 0; i < tBody.rows.length; i++) {
            var row = tBody.getElementsByTagName("tr")[i];
            if ((i / table.rowsPerItem) % 2 < 1)
                row.className = 'GridItem';
            else
                row.className = 'GridAlternatingItem';
        }
    },

    serialize: function(table) {
        var items = [];
        $('tr', table.tableData).each(function(i, row) {
            var index = row.getAttribute("ItemIndex");
            if (index != null)
                items.push(table.serialize(index));
        });
        return JSON.stringify(items);
    },

    __createAndAppendElement: function(oParent, tag, attributes) {
        var item = document.createElement(tag);

        for (var attr in attributes) {
            if (attributes.hasOwnProperty(attr)) {
                if (item[attr] !== undefined) {
                    if (attr === "class" || attr === "className") {
                        if(item[attr] === "")
                            item[attr] = attributes[attr];
                        else
                            item[attr] = item[attr] + " " + attributes[attr];
                    }
                    else
                        item[attr] = attributes[attr];
                }
                else
                    item.setAttribute(attr, attributes[attr]);
            }
        }
        oParent.appendChild(item);

        if (tag === 'input')
            _initDynamicInput(item);

        return item;
    },

    calImg: {
        'border': '0',
        'title': 'Open calendar',
        'align': 'AbsMiddle'
    }
}
