﻿var ServiceCredential = (function() {
    var sc_branchId = null;
    var sc_employeeId = null;
    var sc_employeeType = null;
    var sc_UserId = null;
    var sc_BrokerId = null;
    var sc_ocId = null;
    var sc_isUserProfile = false;
    var sc_sortColumn = 0;
    var sc_isDescending = true;
    var sc_serviceCredentialJsonId = null;
    var sc_serviceCredentialTableId = null;

    function _sortServiceList(services, sortColumn, isDesc) {
        services.sort(function (a, b) {
            var valA, valB;
            var namePropertyTargetA = 'ServiceProviderDisplayName';
            var namePropertyTargetB = 'ServiceProviderDisplayName';

            if (sortColumn == 0) {
                valA = a[namePropertyTargetA].toUpperCase();
                valB = b[namePropertyTargetB].toUpperCase();
            }
            else if (sortColumn == 1) {
                valA = a.AccountId.toUpperCase();
                valB = b.AccountId.toUpperCase();
            }
            else if (sortColumn == 2) {
                valA = a.UserName.toUpperCase();
                valB = b.UserName.toUpperCase();
            }
            else {
                valA = a.UserTypeRep.toUpperCase();
                valB = b.UserTypeRep.toUpperCase();
            }

            if (valA < valB) {
                return -1;
            }
            if (valA > valB) {
                return 1;
            }

            return 0;
        });

        if (!isDesc) {
            services.reverse();
        }
    }

    function _initialize(data) {
        sc_branchId = data.BranchId || sc_branchId;
        sc_employeeId = data.EmployeeId || sc_employeeId;
        sc_employeeType = data.EmployeeType || sc_employeeType;
        sc_UserId = data.UserId || sc_UserId;
        sc_BrokerId = data.BrokerId || sc_BrokerId;
        sc_isUserProfile = data.IsUserProfile || sc_isUserProfile;
        sc_serviceCredentialJsonId = data.ServiceCredentialJsonId;
        sc_serviceCredentialTableId = data.ServiceCredentialTableId;
        sc_ocId = data.OriginatingCompanyId || sc_ocId;
    }

    function _sortByHeader() {
        var column = $(this).data('column');
        sc_isDescending = sc_sortColumn == column ? !sc_isDescending : true;
        sc_sortColumn = column;

        _renderCredentials();
    }

    function _renderCredentials() {
        var serviceCredentialsJson = $('#' + sc_serviceCredentialJsonId).val() || "[]";
        if (serviceCredentialsJson) {
            var credentials = $.parseJSON(serviceCredentialsJson);
            var credentialsTable = $('#' + sc_serviceCredentialTableId).find('tbody');

            credentialsTable.empty();
            _sortServiceList(credentials, sc_sortColumn, sc_isDescending);

            // The header
            credentialsTable.append(
                $('<tr>').addClass('GridHeader')
                    .append($('<td>').text(''))
                    .append($('<td>').append($('<a>').data('column', 0).text('Service Provider').click(_sortByHeader)))
                    .append($('<td>').append($('<span>').text('Services')))
                    .append($('<td>').append($('<a>').data('column', 1).text('Account ID').click(_sortByHeader)))
                    .append($('<td>').append($('<a>').data('column', 2).text('Login').click(_sortByHeader)))
                    .append($('<td>').append($('<a>').data('column', 3).text('User Type').click(_sortByHeader)))
                    .append($('<td>').text(''))
            );

            // The rows
            var isAlt = false;
            $.each(credentials, function (count, item) {
                var serviceProviderName = item.ServiceProviderDisplayName;
                var serviceList = item.EnabledServiceTypesToDisplay.join(', ');

                credentialsTable.append(
                    $('<tr>').addClass(isAlt ? 'GridAlternatingItem' : 'GridItem')
                        .append($('<td>').append($('<a>').data('id', item.Id).text('edit').click(_editLinkClick)))
                        .append($('<td>').text(serviceProviderName))
                        .append($('<td>').text(serviceList))
                        .append($('<td>').text(item.AccountId))
                        .append($('<td>').text(item.UserName))
                        .append($('<td>').text(item.UserTypeRep))
                        .append($('<td>').append($('<a>').data('id', item.Id).click(_deleteLinkClick).append($('<img>').attr('src', ML.VirtualRoot + '/images/minus.png').css('border', '0'))))
                );

                isAlt = !isAlt
            });
        }
    }

    function _editLinkClick() {
        _editServiceCredential($(this).data('id'), 'edit');
    }

    function _deleteLinkClick() {
        _editServiceCredential($(this).data('id'), 'delete', 50);
    }

    function _addServiceCredential() {
        _editServiceCredential(0, 'add');
    }

    function _editServiceCredential(id, cmd, popupHeight, popupWidth) {
        var iup = sc_isUserProfile ? 't' : 'f';

        var link = ML.VirtualRoot + '/los/admin/EditServiceCredential.aspx?id=' + id + '&cmd=' + cmd + '&iup=' + iup;
        if (sc_branchId != null) {
            link += '&branch=' + sc_branchId;
        }

        if (sc_employeeId != null) {
            link += '&employee=' + sc_employeeId;
        }

        if (sc_employeeType != null) {
            link += '&eType=' + sc_employeeType;
        }

        if (sc_UserId != null) {
            link += '&uid=' + sc_UserId;
        }

        if (sc_BrokerId != null) {
            link += '&bid=' + sc_BrokerId;
        }

        if (sc_ocId != null) {
            link += '&oc=' + sc_ocId;
        }

        var width = 550;
        var height = 350;
        if (popupHeight) {
            height = popupHeight;
        }

        if (popupWidth) {
            width = popupWidth;
        }

        LQBPopup.Show(link, {
            hideCloseButton: true,
            popupClasses: 'Popup',
            width: width,
            height: height,
            onReturn: _popupReturn
        }, null);
    }

    function _popupReturn(results) {
        if (results != null && results.OK) {
            $('#' + sc_serviceCredentialJsonId).val(results.CredentialList);
            _renderCredentials();
        }
    }

    return {
        Initialize: _initialize,
        RenderCredentials: _renderCredentials,
        AddServiceCredential: _addServiceCredential
    };
})();