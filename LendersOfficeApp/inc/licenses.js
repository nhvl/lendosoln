﻿if (!this.LICENSES) {
    this.LICENSES = ({});

    LICENSES.Prefix = new Array();
    LICENSES.List = new Object();
    LICENSES.Index = new Object();
    LICENSES.fn = new Object();
    LICENSES.Validator = new Object();
    LICENSES.Validator.lastRun = -1;
    LICENSES.MAX_LICENSES = 100;
    LICENSES.IsDisabled = {};

    LICENSES.fn.Disabled = function(prefix, bDisabled) {
      document.getElementById(prefix + "AddBtn").disabled = bDisabled;
      for (var i = 0; i < LICENSES.Index[prefix]; i++) {
        var oLicenseNumber = document.getElementById(prefix + 'LendingLicenseNumber' + i);
        var oLicenseState = document.getElementById(prefix + 'LendingLicenseState' + i);
        var oLicenseExpiration = document.getElementById(prefix + 'LendingLicenseExpiration' + i);
        var oRemoveLink = document.getElementById(prefix + 'remove' + i);
        if (null == oLicenseNumber || null == oLicenseState || null == oLicenseExpiration || null == oRemoveLink) {
          continue;
        }

        oLicenseNumber.readOnly = bDisabled;
        oLicenseNumber.style.backgroundColor = bDisabled ? 'lightgrey' : '';
        oLicenseState.disabled = bDisabled;
        oLicenseExpiration.readOnly = bDisabled;
        oLicenseExpiration.style.backgroundColor = bDisabled ? 'lightgrey' : '';
        setDisabledAttr(oRemoveLink, bDisabled);

        var editLink = document.getElementById(prefix + 'edit' + i);
        if (editLink != null) {
            setDisabledAttr(editLink, bDisabled);
        }

        LICENSES.IsDisabled[prefix] = !!bDisabled;
      }
    }
    LICENSES.fn.Add = function(prefix, bDisabled) {
      if (typeof prefix === 'string' && prefix.length != 0) {
        if (typeof LICENSES.Prefix[prefix] != 'undefined') {
          alert('Unable to initialize the lending licenses panel: Duplicate existing prefix found.');
          window.close();
        }
        else {
          LICENSES.Prefix.push(prefix);
          LICENSES.IsDisabled[prefix] = !!bDisabled;
        }
      }
    }

    LICENSES.fn.__init = function() {
        for (var i = 0; i < LICENSES.Prefix.length; i++) {
            LICENSES.Index[LICENSES.Prefix[i]] = 0;


            if (typeof LICENSES.List[LICENSES.Prefix[i]] == 'undefined')
                LICENSES.List[LICENSES.Prefix[i]] = {};

            LICENSES.fn.initLendingTable(LICENSES.Prefix[i]);

            var nRowCount = document.getElementById(LICENSES.Prefix[i] + "LendingLicensesRows").rows.length;
            var nMaxRows = LICENSES.MAX_LICENSES;
            document.getElementById(LICENSES.Prefix[i] + 'AddBtn').disabled = (nRowCount >= nMaxRows);
            if (LICENSES.IsDisabled[LICENSES.Prefix[i]]) {
                LICENSES.fn.Disabled(LICENSES.Prefix[i], true);
            }
            if (LICENSES.Prefix[i] != "Branch") {

                document.getElementById(LICENSES.Prefix[i] + "editColumn").style.display = "none";
                if (LICENSES.Prefix[i] != "Company") {
                    document.getElementById(LICENSES.Prefix[i] + "addressColumn").style.display = "none";
                }
            }
        }

    }

    LICENSES.fn.setLicenses = function(prefix, setLicensesStr) {

        LICENSES.fn.clearLendingLicenseTable(prefix);

        var oTbody = document.getElementById(prefix + "LendingLicensesRows");
        var nRowCount = 0;

        eval(setLicensesStr);

        if (typeof (LICENSES.List[prefix]) != 'undefined' && LICENSES.List[prefix] != null) {
            nRowCount = LICENSES.List[prefix].length;
        }

        for (var i = 0; i < nRowCount; i++) {
            var oEntry = LICENSES.List[prefix][i];
            LICENSES.fn.__CreateLicenseRow(oTbody, oEntry[0], oEntry[1], oEntry[2], prefix, oEntry[3], oEntry[4], oEntry[5], oEntry[6], oEntry[7], oEntry[8], oEntry[9], oEntry[10]);
            //update the License Address Here
            if (prefix == "Branch" || prefix == "Company") {
                var street = document.getElementById(prefix + "Street" + i).value;
                var city = document.getElementById(prefix + "City" + i).value;
                var addrState = document.getElementById(prefix + "AddrState" + i).value;
                var zip = document.getElementById(prefix + "Zip" + i).value;
                document.getElementById(prefix + "LicenseAddress" + i).value = street + ", " + city + ", " + addrState + ", " + zip;
            }
            LICENSES.fn.__checkIfExpired(i, prefix);

        }
        document.getElementById(prefix + 'emptyPanel').style.display = (nRowCount == 0) ? '' : 'none';

        if (prefix == "Company") {
            $("#ShouldUpdateCompanyInfo").prop('checked', true);
        }
    }

    LICENSES.fn.attachLoadListener = function() {
        if (!LICENSES.bInit) {
            LICENSES.bInit = true;
            addEvent(window, 'load', LICENSES.fn.__init);
        }
    }

    LICENSES.fn.addLendingLicense = function(prefix) {
        var oTbody = document.getElementById(prefix + "LendingLicensesRows");
        var nRowCount = oTbody.rows.length;
        var nMaxRows = LICENSES.MAX_LICENSES;
        if (nRowCount < nMaxRows) {
            var oRow = LICENSES.fn.__CreateLicenseRow(oTbody, '', '', '', prefix);
            document.getElementById(prefix + 'emptyPanel').style.display = 'none';
            if ((nRowCount+1) == nMaxRows) {
                document.getElementById(prefix + 'AddBtn').disabled = true;
            }
            if (typeof updateDirtyBit === 'function') {
                updateDirtyBit();
            }
        }
    }

    LICENSES.fn.clearLendingLicenseTable = function(prefix) {
        if (typeof prefix == 'undefined') { var prefix = ''; }
        var oTbody = document.getElementById(prefix + "LendingLicensesRows");
        var oHidden = document.getElementById(prefix + 'LendingLicenseList');
        var oPanel = document.getElementById(prefix + 'emptyPanel');
        try {
            if (oHidden != null) {
                oHidden.value = '';
            }
            var nRowCount = oTbody.rows.length;
            for (var i = 0; i < nRowCount; i++) {
                oTbody.deleteRow(0);
            }
            LICENSES.Index[prefix] = 0;
            if (oPanel != null)
                oPanel.style.display = '';
        } catch (e) { }
    }

    LICENSES.fn.initLendingTable = function(prefix) {
        LICENSES.fn.clearLendingLicenseTable(prefix);

        var oTbody = document.getElementById(prefix + "LendingLicensesRows");
        var nRowCount = 0;
        if (typeof (LICENSES.List[prefix]) != 'undefined' && LICENSES.List[prefix] != null) {
            nRowCount = LICENSES.List[prefix].length;
            var hiddenField = document.getElementById(prefix + "LendingLicenseList");
            if (hiddenField) {
                hiddenField.value = JSON.stringify(LICENSES.List[prefix]);
            }
        }
        for (var i = 0; i < nRowCount; i++) {
            var oEntry = LICENSES.List[prefix][i];
            LICENSES.fn.__CreateLicenseRow(oTbody, oEntry[0], oEntry[1], oEntry[2], prefix, oEntry[3], oEntry[4], oEntry[5], oEntry[6], oEntry[7], oEntry[8], oEntry[9], oEntry[10]);
            LICENSES.fn.__checkIfExpired(i, prefix);
        }

        document.getElementById(prefix + 'emptyPanel').style.display = (nRowCount == 0) ? '' : 'none';
    }

    LICENSES.fn.__CreateAndAppend = function(sTag, oParent) {
        var oItem = document.createElement(sTag);
        oParent.appendChild(oItem);
        return oItem;
    }

    function editLicense(prefix, id, originalLicenseNum, originalState) {
        //technically, licenses don't have any ID, but License Numbers are supposed to be unique per state, so the original LicenseNum + State should be a unique id
        var array = {};
        array["LicenseNum"] = document.getElementById(prefix + "LendingLicenseNumber" + id).value;
        array["State"] = document.getElementById(prefix + "LendingLicenseState" + id).value;
        array["ExpD"] =  document.getElementById(prefix + "LendingLicenseExpiration" + id).value;

        array["DisplayName"] =  document.getElementById(prefix + "DisplayName" + id).value;
        array["Street"] =  document.getElementById(prefix + "Street" + id).value;
        array["City"] =  document.getElementById(prefix + "City" + id).value;
        array["AddrState"] =  document.getElementById(prefix + "AddrState" + id).value;
        array["Zip"] =  document.getElementById(prefix + "Zip" + id).value;
        array["Phone"] =  document.getElementById(prefix + "Phone" + id).value;
        array["Fax"] = document.getElementById(prefix + "Fax" + id).value;
        array["Editable"] = document.getElementById(prefix + "Editable" + id).value;
        array["OriginalLicenseNum"] = originalLicenseNum;
        array["OriginalState"] = originalState;
        array["BranchID"] = document.getElementById("BranchId").value;

        array["DefaultDisplayName"] = document.getElementById("DefaultDisplayName").value;
        array["DefaultStreet"] = document.getElementById("DefaultStreet").value;
        array["DefaultCity"] = document.getElementById("DefaultCity").value;
        array["DefaultAddrState"] = document.getElementById("DefaultAddrState").value;
        array["DefaultZip"] = document.getElementById("DefaultZip").value;
        array["DefaultPhone"] = document.getElementById("DefaultPhone").value;

        var url = "/los/BrokerAdmin/LendingLicenseEdit.aspx"
        showModal(url, array, null, null, function (ret) {
            //the values were edited
            if (ret !== null && ret !== undefined && ret.OK) {
                document.getElementById(prefix + "LicenseAddress" + id).value = ret["Street"] + ", " + ret["City"] + ", " + ret["AddrState"] + ", " + ret["Zip"];

                document.getElementById(prefix + "LendingLicenseNumber" + id).value = ret["LicenseNum"];
                document.getElementById(prefix + "LendingLicenseState" + id).value = ret["State"];
                document.getElementById(prefix + "LendingLicenseExpiration" + id).value = ret["ExpD"];

                document.getElementById(prefix + "DisplayName" + id).value = ret["DisplayName"];
                document.getElementById(prefix + "Street" + id).value = ret["Street"];
                document.getElementById(prefix + "City" + id).value = ret["City"];
                document.getElementById(prefix + "AddrState" + id).value = ret["AddrState"];
                document.getElementById(prefix + "Zip" + id).value = ret["Zip"];
                document.getElementById(prefix + "Phone" + id).value = ret["Phone"];
                document.getElementById(prefix + "Fax" + id).value = ret["Fax"];
                document.getElementById(prefix + "Editable" + id).value = ret["Editable"];

                var requiresDataBind = document.getElementById("m_Edit_requiresDataBind");
                if (requiresDataBind != null)
                    requiresDataBind.checked = true;
            }
        }, {hideCloseButton:true});
    }

    function addHiddenElement(id, value, isCheckbox, type, prefix, editable) {
        var input = document.getElementById(id);

        if (input == null) {
            input = document.createElement("input");
            input.id = id;
            input.style.display = "none";
            input.type = "hidden";
        }

            if (value != null)
                input.value = value;
            else if(prefix != "Company") {
                if (type == "DisplayName")
                    input.value = document.getElementById("DefaultDisplayName").value;
                else if (type == "Street")
                    input.value = document.getElementById("DefaultStreet").value;
                else if (type == "City")
                    input.value = document.getElementById("DefaultCity").value;
                else if (type == "AddrState")
                    input.value = document.getElementById("DefaultAddrState").value;
                else if (type == "Zip")
                    input.value = document.getElementById("DefaultZip").value;

            }

        document.body.appendChild(input);
    }

    LICENSES.fn.__CreateLicenseRow = function(oParent, sLicenseNumber, sLicenseState, sLicenseExpiration, prefix, DisplayName, Street, City, AddrState, Zip, Phone, Fax, Editable) {
        if (typeof prefix == 'undefined') { var prefix = ''; }
        var oRow = LICENSES.fn.__CreateAndAppend("tr", oParent);
        var oCell = null;
        var o;
        var id = LICENSES.Index[prefix]++;
        var isNewLicense = (sLicenseNumber == '') && (sLicenseState == '') && (sLicenseExpiration == '');
        oRow.className = ((oParent.rows.length % 2) == 0) ? "GridAlternatingItem" : "GridItem";



        if (prefix == "Branch" || prefix == "Company") {
            addHiddenElement(prefix + "DisplayName" + id, DisplayName, false, "DisplayName", prefix, Editable);
            addHiddenElement(prefix + "Street" + id, Street, false, "Street", prefix, Editable);
            addHiddenElement(prefix + "City" + id, City, false, "City", prefix, Editable);
            addHiddenElement(prefix + "AddrState" + id, AddrState, false, "AddrState", prefix, Editable);
            addHiddenElement(prefix + "Zip" + id, Zip, false, "Zip", prefix, Editable);
            addHiddenElement(prefix + "Phone" + id, Phone, false, prefix, Editable);
            addHiddenElement(prefix + "Fax" + id, Fax, false, prefix, Editable);
            addHiddenElement(prefix + "Editable" + id, Editable, true, prefix, Editable);

            if (prefix == "Branch") {
                oCell = LICENSES.fn.__CreateAndAppend("td", oRow);
                oCell.setAttribute("align", "center");
                oCell.setAttribute("class", "editColumn");
                o = LICENSES.fn.__CreateAndAppend("a", oCell);
                o.id = prefix + 'edit' + id;
                o.innerText = 'edit';
                o.href = '#';
                o.onclick = function() {
                    editLicense(prefix, id, sLicenseNumber, sLicenseState);
                };
            }
        }



        oCell = LICENSES.fn.__CreateAndAppend("td", oRow);
        o = LICENSES.fn.__CreateAndAppend("input", oCell);
        o.id = prefix + 'LendingLicenseNumber' + id;
        o.type = 'text';
        o.size = '30';
        o.maxLength = 70;
        o.value = sLicenseNumber;
        o.defaultValue = sLicenseNumber;
        attachCommonEvents(o);
        if (prefix == "Company") {
            $(o).change(function() { $("#ShouldUpdateCompanyInfo").prop('checked', true); })
        }
        addEvent(o, 'blur', function(idVal, prep) {
            return function() {
                var oLic = document.getElementById(prep + 'LendingLicenseNumber' + idVal);
                var oImg = document.getElementById(prep + 'errorLic' + idVal);
                var msg = 'License # cannot be blank.';
                LICENSES.fn.validateEmptyField(id, oLic, oImg, msg, prep);
            }
        } (id, prefix));
        addEvent(o, 'keyup', function(idVal, prep) {
            return function() {
                var oLic = document.getElementById(prep + 'LendingLicenseNumber' + idVal);
                var oImg = document.getElementById(prep + 'errorLic' + idVal);
                var msg = 'License # cannot be blank.';
                LICENSES.fn.validateEmptyField(id, oLic, oImg, msg, prep);
            }
        }(id, prefix));

        registerReSerializeOnChange(o, prefix);

        o = LICENSES.fn.__CreateAndAppend("img", oCell);
        o.id = prefix + 'errorLic' + id;
        o.src = '../../images/error_icon.gif';
        o.style.display = 'none';

        oCell = LICENSES.fn.__CreateAndAppend("td", oRow);
        o = LICENSES.fn.__CreateAndAppend("select", oCell);
        o.id = prefix + 'LendingLicenseState' + id;
        LICENSES.fn.__BindState(o, (isNewLicense ? "--" : ""));
        attachCommonEvents(o);
        o.value = isNewLicense ? "--" : sLicenseState;

        registerReSerializeOnChange(o, prefix);

        if (prefix == "Company") {
            $(o).change(function() { $("#ShouldUpdateCompanyInfo").prop('checked', true); })
        }
        o.options[o.selectedIndex].defaultSelected = true;
        if (!isNewLicense && o.value != '') {
            try {
                if (o.options[0].text == '' && o.options[0].value == '')
                    o.remove(0);
            } catch (e) { }
        }

        oCell = LICENSES.fn.__CreateAndAppend("td", oRow);
        o = LICENSES.fn.__CreateAndAppend("input", oCell);
        o.id = prefix + 'LendingLicenseExpiration' + id;
        o.type = 'text';
        o.className = 'mask';
        o.setAttribute('preset', 'date');
        o.dttype = 'DateTime'
        o.style.width = '70px';
        if (prefix == "Company") {
            $(o).change(function() { $("#ShouldUpdateCompanyInfo").prop('checked', true); })
        }
        registerReSerializeOnChange(o, prefix);
        attachCommonEvents(o);
        _initMask(o);
        o.value = sLicenseExpiration;
        o.defaultValue = sLicenseExpiration;

        addEvent(o, 'blur', function(idVal, prep) {
            return function() {
                var oLic = document.getElementById(prep + 'LendingLicenseExpiration' + idVal);
                var oImg = document.getElementById(prep + 'errorDate' + idVal);
                var msg = 'Expiration Date cannot be blank.';
                LICENSES.fn.validateEmptyField(id, oLic, oImg, msg, prep);
            }
        } (id, prefix));
        addEvent(o, 'keyup', function(idVal, prep) {
            return function() {
                var oLic = document.getElementById(prep + 'LendingLicenseExpiration' + idVal);
                var oImg = document.getElementById(prep + 'errorDate' + idVal);
                var msg = 'Expiration Date cannot be blank.';
                LICENSES.fn.validateEmptyField(id, oLic, oImg, msg, prep);
            }
        }(id, prefix));

        o = LICENSES.fn.__CreateAndAppend("img", oCell);
        o.id = prefix + 'errorDate' + id;
        o.src = '../../images/error_icon.gif';
        o.style.display = 'none';

        o = LICENSES.fn.__CreateAndAppend("span", oCell);
        o.id = prefix + 'expDate' + id;
        o.innerText = ' expired';
        o.style.color = 'red';
        o.style.display = 'none';

        if (prefix == "Branch" || prefix == "Company") {
            oCell = LICENSES.fn.__CreateAndAppend("td", oRow);
            oCell.setAttribute("class", "editColumn");
            o = LICENSES.fn.__CreateAndAppend("input", oCell);
            o.id = prefix + 'LicenseAddress' + id;
            o.type = 'text';
            o.style.width = '260px';
            o.readOnly = "true";
            o.style.backgroundColor = "lightGray";
            var fullAddress = document.getElementById(prefix + "Street" + id).value + ", " + document.getElementById(prefix + "City" + id).value + ", " + document.getElementById(prefix + "AddrState" + id).value + ", " + document.getElementById(prefix + "Zip" + id).value;
            if (fullAddress == ", , , ") {
                o.value = "";
            }
            else
                o.value = fullAddress;
        }

        oCell = LICENSES.fn.__CreateAndAppend("td", oRow);
        oCell.setAttribute("align", "center");
        o = LICENSES.fn.__CreateAndAppend("a", oCell);
        o.id = prefix + 'remove' + id;
        o.innerText = 'remove';
        o.href = '#';
        addEvent(o, 'click', function(prep, oId) {
            return function() {
                if (LICENSES.IsDisabled[prep]) {
                    return false;
                }
                var oLink = document.getElementById(oId);
                var iRowIndex = oLink.parentElement.parentElement.rowIndex;
                var oTbody = document.getElementById(prep + 'LendingLicensesRows');
                oTbody.deleteRow(iRowIndex - 1);
                for (var i = iRowIndex - 1; i < oTbody.rows.length; i++) {
                    oTbody.rows[i].className = ((i % 2) == 0) ? "GridItem" : "GridAlternatingItem";
                }
                document.getElementById(prefix + 'emptyPanel').style.display = (oTbody.rows.length == 0) ? '' : 'none';
                document.getElementById(prefix + 'AddBtn').disabled = false;
                if (typeof updateDirtyBit === 'function') {
                    updateDirtyBit();
                }
            }
        } (prefix, o.id));

        return oRow;
    }

    function registerReSerializeOnChange(element, prefix) {
        addEvent(element, 'blur', function () {
            LICENSES.fn.saveLicenses(prefix);
        });
    }

    LICENSES.fn.__GetLendingLicenseValues = function(prefix) {
        if (typeof prefix == 'undefined') { var prefix = ''; }
        var iCurrentIndex = 0;
        var oResults = [];
        for (var i = 0; i < LICENSES.Index[prefix]; i++) {
            var oLicenseNumber = document.getElementById(prefix + 'LendingLicenseNumber' + i);
            var oLicenseState = document.getElementById(prefix + 'LendingLicenseState' + i);
            var oLicenseExpiration = document.getElementById(prefix + 'LendingLicenseExpiration' + i);

            var DisplayName = document.getElementById(prefix + "DisplayName" + i);
            var Street = document.getElementById(prefix + "Street" + i);
            var City = document.getElementById(prefix + "City" + i);
            var AddrState = document.getElementById(prefix + "AddrState" + i);
            var Zip = document.getElementById(prefix + "Zip" + i);
            var Phone = document.getElementById(prefix + "Phone" + i);
            var Fax = document.getElementById(prefix + "Fax" + i);
            var Editable = document.getElementById(prefix + "Editable" + i);

            if (null == oLicenseNumber || null == oLicenseState || null == oLicenseExpiration) {
                continue;
            }
            if (oLicenseNumber.value == '' && oLicenseState.value == '' && oLicenseExpiration.value == '') {
                continue;
            }
            try { oLicenseNumber.value = oLicenseNumber.value.substring(0, 70); } catch (e) { }
            if (DisplayName != null && Street != null && City != null && AddrState != null && Zip != null && Phone != null && Fax != null && Editable != null) {
                oResults.push([oLicenseNumber.value, oLicenseState.value, oLicenseExpiration.value,
                DisplayName.value, Street.value, City.value, AddrState.value, Zip.value, Phone.value, Fax.value, Editable.value]);
            }
            else {
                oResults.push([oLicenseNumber.value, oLicenseState.value, oLicenseExpiration.value,
                '', '', '', '', '', '', '', '']);
            }
            try {
                oLicenseNumber.defaultValue = oLicenseNumber.value;
                oLicenseExpiration.defaultValue = oLicenseExpiration.value;

                LICENSES.fn.__clearDefaultValue(oLicenseState);
                oLicenseState.options[oLicenseState.selectedIndex].defaultSelected = true;
            } catch (e) { }

        }
        return oResults;
    }

    LICENSES.fn.__clearDefaultValue = function(o) {
        if (typeof o == 'undefined' || o == null)
            return;
        try {
            for (var i = 0; i < o.options.length; i++) {
                if (o.options[i].defaultSelected)
                    o.options[i].defaultSelected = false;
            }
        } catch (e) { }
    }

	LICENSES.fn.__BindState = function(oDropDown, empty) {
	    var states = [  "", "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL",
                      "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD",
                      "ME", "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE", "NH", "NJ",
                      "NM", "NV", "NY", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN",
                      "TX", "UT", "VA", "VT", "WA", "WI", "WV", "WY"];
	    for (var i = 0; i < states.length; i++) {
	        var opt = document.createElement('option');
	        opt.text = states[i];
	        opt.value = states[i];
	        oDropDown.options.add(opt);
	    }
	    if (typeof empty != 'undefined'){
	        oDropDown.options[0].value = empty;
	    }
	}

	LICENSES.fn.validateEmptyField = function(id, oLic, oImg, msg, prefix) {
	    if (typeof prefix == 'undefined') { var prefix = ''; }
	    oImg.style.display = oLic.value.length == 0 ? '' : 'none';
	    msg = (oLic.value.length == 0) ? msg : '';
	    LICENSES.fn.showErrMsg(msg, prefix);
	    if (oLic.dttype && oLic.value.length == 0) {
	        var oExpired = document.getElementById(prefix + 'expDate' + id);
	        oExpired.style.display = 'none';
	    }
	}

	LICENSES.fn.showErrMsg = function(msg, prefix) {
	    if (typeof prefix == 'undefined') { var prefix = ''; }
	    var oErr = document.getElementById(prefix + 'licErrMsg');
	    if (typeof msg != 'string' || msg == null)
	        return;
	    try{
	        oErr.style.display = (msg.length == 0) ? 'none' : '';
	        oErr.innerText = msg;
	    }catch(e){}
	}

	LICENSES.fn.IsLicenseValid = function(prefix) {
	    if (typeof prefix == 'undefined') { var prefix = ''; }
	    var oLicenses = LICENSES.fn.__GetLendingLicenseValues(prefix);
	    var oDuplicates = new Object();

	    try {
	        for (var i = 0; i < LICENSES.Index[prefix]; i++) {
	            var oLicDate = document.getElementById(prefix + 'LendingLicenseExpiration' + i);
	            try {
	                if (oLicDate != null && !LICENSES.fn.__dateVal(oLicDate)) {
	                    var msg = (oLicDate.value != null && oLicDate.value.length != 0) ? 'Invalid date found: ' + oLicDate.value : 'Expiration Date cannot be blank.';
	                    LICENSES.fn.showErrMsg(msg, prefix);
	                    return false;
	                }
	            }catch(e){}
	        }
	        var nLicenses = oLicenses.length;
	        for (var i = 0; i < nLicenses; i++) {
	            if (oLicenses[i][0].length == 0 || oLicenses[i][2].length == 0) {
	                LICENSES.fn.showErrMsg('License # cannot be blank.', prefix);
	                return false;
	            }
	            if (oLicenses[i][1] == '--'){
	                var msg = oLicenses[i][0];
	                if (msg.length > 20){
	                    msg = msg.substring(0, 20) + "...";
	                }
	                LICENSES.fn.showErrMsg('Please select a state for license "' + msg + '".', prefix);
	                return false;
	            }
	            var key = oLicenses[i][0] + '_' + oLicenses[i][1];
	            if (typeof oDuplicates[key] != 'undefined'){
	                LICENSES.fn.showErrMsg('License # needs to be unique within each state.', prefix);
	                return false;
	            }
	            else {
	                oDuplicates[key] = true;
	            }
	        }
	    }
	    catch (e) {
	        return false;
	    }
	    LICENSES.fn.showErrMsg('', prefix);
	    return true;
	}

	LICENSES.fn.isExpired = function(oDate) {
	    var todayDate = new Date(new Date().toDateString());
	    var isExpired = false;
	    try {
	        var licenseDate = new Date(new Date(oDate.value).toDateString());
	        isExpired = ((licenseDate - todayDate) <= 0);
	    } catch (e) { isExpired = false; }
	    return isExpired;
	}

	LICENSES.fn.__checkIfExpired = function(id, prefix) {
	    if (typeof prefix == 'undefined') { var prefix = ''; }
	    var oDate = document.getElementById(prefix + 'LendingLicenseExpiration' + id);
	    var oExpired = document.getElementById(prefix + 'expDate' + id);
	    if (oDate && oExpired) {
	        oExpired.style.display = LICENSES.fn.isExpired(oDate) ? '' : 'none';
	    }
	}

	LICENSES.fn.postPopulateLicenseForm = function(values, prefix) {
	    if (typeof prefix == 'undefined') { var prefix = ''; }
	    var _str = values[prefix + "LendingLicenseList"];
	    if (null != _str && "" != _str) {
	        LICENSES.List[prefix] = JSON.parse(values[prefix + "LendingLicenseList"]);
	        LICENSES.fn.initLendingTable(prefix);
	    } else {

	    }
	}

	LICENSES.fn.postGetLicenseFormValues = function(args, prefix) {
	    if (typeof prefix == 'undefined') { var prefix = ''; }
	    args[prefix + "LendingLicenseList"] = JSON.stringify(LICENSES.fn.__GetLendingLicenseValues(prefix));
	}

	LICENSES.fn.saveLicenses = function(prefix) {
	    if (typeof prefix == 'undefined') { var prefix = ''; }
	    var oHidden = document.getElementById(prefix + 'LendingLicenseList');
	    try {
	        if (oHidden != null)
	            oHidden.value = JSON.stringify(LICENSES.fn.__GetLendingLicenseValues(prefix));
	    } catch (e) { }
	}

	LICENSES.fn.runValidator = function() {
	    var nRuns = LICENSES.Prefix.length;
	    var nextRun = -1;
	    if ( LICENSES.Validator.lastRun >= (nRuns-1)){
	        nextRun = 0;
	    }
	    else {
	        nextRun = LICENSES.Validator.lastRun + 1;
	    }

	    var prefix = LICENSES.Prefix[nextRun];
	    LICENSES.Validator.lastRun = nextRun;
	    var bIsValid = LICENSES.fn.IsLicenseValid(prefix);
	    return bIsValid;
	}


	LICENSES.fn.__dateVal = function() {
	    var e = arguments[0];
	    if (typeof e.value == 'undefined') return LICENSES.fn.__getFocus(e);
	    if (e.value == '') return LICENSES.fn.__getFocus(e);
	    var minYear = 1754;
	    var maxYear = 9999;
	    var value = e.value;

	    value = value.replace(/[^0-9\/-]/g, '').replace(/-/g, '/');
	    var dt = new Date();
	    var mm;
	    var dd;
	    var yyyy;
	    if (value != '' && !isNaN(value) && parseInt(value, 10) <= 31) {
	        dt.setDate(parseInt(value, 10));
	    } else {
	        var str = value;
	        if (str == '') return LICENSES.fn.__getFocus(e);
	        var parts = str.split('/');
	        if (parts.length == 1) {
	            if (str.length == 6 || str.length == 8) {
	                yyyy = getYear(parseInt(str.substr(4), 10));
	                mm = parseInt(str.substr(0, 2), 10);
	                dd = parseInt(str.substr(2, 2), 10);
	                if (mm < 1 || mm > 12 || dd < 1 || dd > 31) {
	                    return LICENSES.fn.__getFocus(e);
	                }
	                if (yyyy < minYear || yyyy > maxYear) {
	                    dt.setFullYear(yyyy, mm - 1, dd);
	                    e.value = LICENSES.fn.__dateToString(dt);
	                    return LICENSES.fn.__getFocus(e);
	                }
	                dt.setFullYear(yyyy, mm - 1, dd);
	            } else {
	                return LICENSES.fn.__getFocus(e);
	            }
	        } else if (parts.length == 2 || parts.length == 3) {
	            for (var i = 0; i < parts.length; i++) {
	                if (parts[i] == '' || isNaN(parts[i])) {
	                    return LICENSES.fn.__getFocus(e);
	                }
	            }
	            mm = parseInt(parts[0], 10);
	            dd = parseInt(parts[1], 10);
	            yyyy = parts.length == 3 ? getYear(parseInt(parts[2], 10)) : dt.getFullYear();

	            if (mm < 1 || mm > 12 || dd < 1 || dd > 31 || yyyy < minYear || yyyy > maxYear) {
	                return LICENSES.fn.__getFocus(e);
	            }
	            if (!LICENSES.fn.__isMyValidDate(yyyy, mm, dd, e)) {
	                return LICENSES.fn.__getFocus(e);
	            }
	            dt.setFullYear(yyyy, mm - 1, dd);
	        } else {
	            return LICENSES.fn.__getFocus(e);
	        }
	    }
	    e.value = LICENSES.fn.__dateToString(dt);
	    return true;
	}

	LICENSES.fn.__dateToString = function(dt){
        return (dt.getMonth() + 1) + '/' + dt.getDate() + '/' + dt.getFullYear();
    }

    LICENSES.fn.__isMyValidDate = function(yyyy, mm, dd, o) {
        if ((mm == 4 || mm == 6 || mm == 9 || mm == 11) && dd > 30) return false;
        if (mm == 2 && ((yyyy % 4) > 0) && dd > 28) return false;
        if (mm == 2 && dd > 29) return false;
        return true;
    }

    LICENSES.fn.__getFocus = function(o) {
        try { o.focus(); o.select(); } catch (e) { }
        return false;
    }
}

function addEvent(obj, evType, fn, useCapture) {
        addEventHandler(obj, evType, fn, useCapture);
        return true;
}

$(document).ready(function () {
    //Background save
    registerPostPopulateFormCallback(function (args) {
        for (var i = 0; i < LICENSES.Prefix.length; i++) {
            LICENSES.fn.postPopulateLicenseForm(args, LICENSES.Prefix[i]);
        }
    });

    registerPostGetAllFormValuesCallback(function (args) {
        for (var i = 0; i < LICENSES.Prefix.length; i++) {
            LICENSES.fn.postGetLicenseFormValues(args, LICENSES.Prefix[i]);
        }
    });
});

//Postback save
function saveAllLicenses() {
    for (var i = 0; i < LICENSES.Prefix.length; i++) {
        LICENSES.fn.saveLicenses(LICENSES.Prefix[i]);
    }
}

//Validation
function AreLicensesValid() {
    var isValid = true;
    for (var i = 0; i < LICENSES.Prefix.length; i++) {
        if (!LICENSES.fn.IsLicenseValid(LICENSES.Prefix[i])) {
            isValid = false;
        }
    }
    return isValid
}

//Clear licenses
function clearAllLicenses(){
    for (var i = 0; i < LICENSES.Prefix.length; i++) {
        LICENSES.fn.clearLendingLicenseTable(LICENSES.Prefix[i])
    }
}

//.NET Validator
function __customLicensesValidator(val, args){
    var isValid = LICENSES.fn.runValidator();
    if (typeof args != 'undefined' && typeof args.IsValid != 'undefined'){
        args.IsValid = isValid;
    }
    return isValid;
}

function doAfterDateFormat(o) {
    try {
        var index = o.id.lastIndexOf('LendingLicenseExpiration');
        if (index == -1) return;

        var prefix = o.id.substring(0, index);
        var base = o.id.substring(index);

        var id = parseInt(base.replace(/LendingLicenseExpiration/g, ''), 10);
        if (isNaN(id)) {
            return;
        }
        var oExpired = document.getElementById(prefix + 'expDate' + id);
        oExpired.style.display = LICENSES.fn.isExpired(o) ? '' : 'none';
    } catch (e) { }
}
