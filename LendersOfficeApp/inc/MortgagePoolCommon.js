﻿if (typeof (LQBMortgagePools) == 'undefined') LQBMortgagePools = {};
LQBMortgagePools = (function($) {
    return {
        trace: function trace(s) {
            if ('console' in window && 'log' in console) console.log(s);
        },

        SetupTablesort: function SetupTablesort($table, noRowMessage, options) {
            if (!$) alert("Setting up tablesorter requires jQuery");
            if (!$.tablesorter) alert("Setting up tablesorter requires Tablesorter");

            $table.on("UpdateTableSorter", function() {
                updateTableSorter($(this), options);
            });

            noRowMessage = noRowMessage || "No entries available";
            if ($table.find('tbody').children().length == 0) {
                //Keep the empty message in the table footer, so it doesn't interfere with body content if some is added.
                var $footer = $table.find('tfoot');

                if ($footer.length == 0) {
                    $table.append('<tfoot>');
                    $footer = $table.find('tfoot');
                }

                $table.addClass("empty");
                $footer.append('<tr class="GridItem empty-message"><td colspan=20>' + noRowMessage + '</td></tr>');
                return;
            }

            setupTableSorterInternal($table, options);
        },

        //Serializes the inputs in an area into an object.

        //Value names will be determined based on nameSelector.
        //It takes a jQuery wrapped version of the element as its argument, and returns the value name.
        //By default it uses, in order:
        //a data field named "fieldname"
        //the element's id
        //the element's first class name.
        //If no value name is found, that input is not included in the object.

        //Values are usually the input's value.
        //<selects> are by default given a mini-object containing {txt: selected-text, val: selected-value}.

        //Name selection and value selection can be modified by passing in different functions.
        //The functions will take a jQuery wrapped version of the object for the first parameter,
        //and will have 'this' set up to point to the element.
        ToObject: function ToObject(area, sourceFilter, nameSelector, valueSelector) {
            var defaultNameSelector = function($e, defaultFxn) {
                var name = $e.data('fieldname');
                if (!name) name = $e.attr('id');
                if (!name) name = ($e.attr('class') || '').split(' ')[0];
                return name;
            };

            var defaultValueSelector = function($this, defaultFxn) {
                if ($this.is('input[type=checkbox]')) {
                    return $this.is(':checked');
                }

                if ($this.is('select')) {
                    return {
                        val: $this.val(),
                        txt: $(":selected", $this).text()
                    };
                }
                else {
                    return $this.val();
                }
            };

            nameSelector = nameSelector || defaultNameSelector;
            valueSelector = valueSelector || defaultValueSelector
            sourceFilter = sourceFilter || ":input";

            var ret = {};

            var $inputs = $(area).find(sourceFilter);
            $inputs.each(function() {
                var $this = $(this);
                var name = nameSelector.call(this, $this, defaultNameSelector);
                if (!name) return;

                var value = valueSelector.call(this, $this, defaultValueSelector);

                ret[name] = value;
            });
            return ret;
        },

        ApplyObject: function ApplyObject($context, object, elementSelector, textSelector, valueSelector) {
            //These two figure out what to do when given a ddl value
            textSelector = textSelector || function(val) { if (val.txt) return val.txt; return val };
            valueSelector = valueSelector || function(val) { if (val.val) return val.val; return val };

            elementSelector = elementSelector || function(name, $context) {
                return $('.' + name, $context);
            };
            $.each(object, function(name, value) {
                var $elem = elementSelector(name, $context);
                if ($elem.is('span, div, td')) {
                    $elem.text(textSelector(value));
                }
                if ($elem.is(':input')) {
                    $elem.val(valueSelector(value));
                }
                if ($elem.is('input[type=checkbox]')) {
                    $elem.prop('checked', valueSelector(value) == 'true');
                }
            });
        },

        //Only supports single-level arrays (no nested arrays!) and plain objects.
        //Delimit your replacements like :Name:
        //For example, if you have param = {Url: 'something'} and template = '<a href=":Url:">link</a>'
        //You'll get '<a href="something">link</a>'
        ApplyTemplate: function ApplyTemplate(param, template, leaveUnmapped) {
            if (!template) throw "template not provided";
            if ($.isArray(param)) return applyTemplateArray(param, template, leaveUnmapped);
            return applyTemplateObj(param, template, leaveUnmapped);
        },

        BeforePostback: function BeforePostback(fnPre) {
            //for whatever reason jQuery 1.8 has a hard time intercepting this, so we'll override it manually.
            var old_do = window.__doPostBack;
            window.__doPostBack = function(target, b) {
                if ($.isFunction(fnPre)) {
                    if (fnPre(target, b) === false) return false;
                }

                old_do(target, b);
            }
        },

        InitMirror: function InitMirror(triggerClass, toMirror, toSource) {
            toSource = toSource || toMirror;

            $('.Mirror').each(function() {
                var $mirror = $(this);
                var $source = $($mirror.data('mirrored'));

                if ($mirror.is('select')) {
                    $mirror.append($source.find('option').clone());
                }

                bindMirror($source, $mirror, triggerClass, toMirror);
                bindMirror($mirror, $source, triggerClass, toSource);
            });
        },
        
        FromRep: function FromRep(input) {
            if (input instanceof jQuery) input = input.val();

            var neg = 1;
            if (input.indexOf('(') !== -1 || input.indexOf('-') !== -1) neg = -1

            return +input.replace(/[^0-9.]/g, '') * neg;
        }
    }

    function bindMirror($a, $b, triggerClass, translate) {
        translate = translate || function(a) { return a };
        var triggerB = $b.is('.Mirror');
        if (triggerClass) {
            triggerB = $b.is('.' + triggerClass);
        }

        $a.on('change', function() {
            $b.val(translate($a.val()));
            if (triggerB) $b.trigger('blur').trigger('change').trigger('.mask');
        }).trigger('change');
    }

    function updateTableSorter($table, options) {
        if (!$table.is('.tablesorter-setup')) {
            setupTableSorterInternal($table, options);
        }

        $table.trigger("update")
              .trigger("sorton", [$table[0].config.sortList]);
    }

    function setupTableSorterInternal($table, options) {
        $.tablesorter.defaults.widgetZebra = {
            css: ["GridItem", "GridAlternatingItem"]
        };

        var sortableColumns = {};
        $('th', $table).each(function() {
            var $this = $(this);
            var idx = $this.index();
            sortableColumns[idx] = { sorter: $this.is('.sortable') };
        });

        var defaultOptions =
            {
                widgets: ['zebra'],
                header: sortableColumns
            };

        $.extend(true, defaultOptions, options);

        $table.tablesorter(defaultOptions).addClass('tablesorter-setup').removeClass('empty');
        $('.empty-message', $table).remove();
        $('th.sortable', $table).addClass('Action');
    }

    function applyTemplateObj(obj, template, leaveUnmapped) {
        $.each(obj, function(name, value) {
            template = template.replace(new RegExp(':' + name + ':', "g"), value);
        });

        if (!leaveUnmapped) {
            template = template.replace(new RegExp(':\\w+:', "g"), "");
        }

        return template;
    }

    function applyTemplateArray(arr, template, leaveUnmapped) {
        var ret = [];

        $.each(arr, function(idx, val) {
            ret.push(applyTemplateObj(val, template, leaveUnmapped));
        });

        return ret.join('');
    }
})(jQuery);

jQuery(function($) {
    //Init mask.js if present
    $('.mask').each(function() {
        var $this = $(this);
        if ($this.prop('preset')) return;
        if (!$this.prop('mask')) return;

        $this.on('keyup', mask_keyup);

    });
});
