﻿var oldAvailableHtml = "";
var oldSelectedHtml = "";
var lastSearchTerm = "";
var postApplyCallbacks = [];

function OpenProductCodeFilterPopup() {
    $("#AdvancedFilterPopup").dialog({
        title: 'Product Code Filter',
        resizable: false,
        modal: true,
        width: 'auto',
        height: 'auto',
        dialogClass: "ProductCodeFilters"
    });

    oldAvailableHtml = $('#availableProductCodes').html();
    oldSelectedHtml = $('#selectedProductCodes').html();
}

function GenerateTooltipColumn(list, codesList) {
    list.empty();
    if (typeof (codesList) === 'undefined' || codesList.length == 0) {
        list.parent('td').hide();
    }
    $(codesList).each(function (i, codes) {
        list.append($('<li>', { html: codes }));
    });
}

function GenerateTooltip() {
    var toolTipCodes = JSON.parse($(productCodeFileTypeJsonId).val());
    $('.TooltipList').parent('td').show();

    var convCodes = toolTipCodes['Conventional'];
    var convCodesList = $('#ConvCodes');

    var FHACodes = toolTipCodes['FHA'];
    var fhaCodesList = $('#FhaCodes');

    var VACodes = toolTipCodes['VA'];
    var vaCodesList = $('#VaCodes');

    var UsdaRuralCodes = toolTipCodes['UsdaRural'];
    var usdaRuralCodesList = $('#UsdaCodes');

    var OtherCodes = toolTipCodes['Other'];
    var otherCodesList = $('#OtherCodes');

    if ((typeof(convCodes) === 'undefined' || convCodes.length == 0) &&
        (typeof (FHACodes) === 'undefined' || FHACodes.length == 0) &&
        (typeof (VACodes) === 'undefined' || VACodes.length == 0) &&
        (typeof (UsdaRuralCodes) === 'undefined' || UsdaRuralCodes.length == 0) &&
        (typeof (OtherCodes) === 'undefined' || OtherCodes.length == 0))
    {
        $('#NoCodesCol').show();
        $('.TooltipList').parent('td').hide();
    }
    else {
        $('#NoCodesCol').hide();
        GenerateTooltipColumn(convCodesList, convCodes);
        GenerateTooltipColumn(fhaCodesList, FHACodes);
        GenerateTooltipColumn(vaCodesList, VACodes);
        GenerateTooltipColumn(usdaRuralCodesList, UsdaRuralCodes);
        GenerateTooltipColumn(otherCodesList, OtherCodes);
    }
}

function DisplayTooltip(e, parent) {
    var offset = parent.offset();
    var left = e.pageX - offset.left + 10;
    var top = e.pageY - offset.top + 10;
    $('#SelectedCodesPopup').css({ left: left, top: top }).removeClass('Hidden');
}

function HideTooltip() {
    $('#SelectedCodesPopup').addClass('Hidden');
}

function RetrieveSelectedProductCodes() {
    return $(selectedCodesJsonId).val();
}

function ReloadProductCodes(sAvailableProductCodeFilterJson, sSelectedProductCodeFilterJson, sProductCodesByFileTypeJson)
{
    $(availableCodesJsonId).val(sAvailableProductCodeFilterJson);
    $(selectedCodesJsonId).val(sSelectedProductCodeFilterJson);
    $(productCodeFileTypeJsonId).val(sProductCodesByFileTypeJson);

    LoadPickers();
    GenerateTooltip();
}

function LoadPickers() {
    var availablePickerOptions = JSON.parse($(availableCodesJsonId).val());
    var availablePicker = $('#availableProductCodes');
    availablePicker.empty();
    var availableOptions = [];
    for (var key in availablePickerOptions) {
        if (availablePickerOptions.hasOwnProperty(key)) {
            var visible = availablePickerOptions[key];
            if (visible) {
                availableOptions.push($('<option>', { value: key, text: key, title: key }));
            }
        }
    }

    var selectedPickerOptions = JSON.parse($(selectedCodesJsonId).val());
    var selectedPicker = $('#selectedProductCodes');
    selectedPicker.empty();
    var selectedOptions = [];
    for (var key in selectedPickerOptions) {
        if (selectedPickerOptions.hasOwnProperty(key)) {
            var visible = selectedPickerOptions[key];
            if (visible) {
                selectedOptions.push($('<option>', { value: key, text: key, title: key }));
            }
            else {
                selectedPicker.append($('<span>').append($('<option>', { value: key, text: key, title: key }).addClass('Hidden')));
            }
        }
    }

    var sortedAvailable = Sort($(availableOptions).map(function () {
        return this.toArray();
    }));
    availablePicker.empty();
    availablePicker.append(sortedAvailable);

    var sortedSelected = Sort($(selectedOptions).map(function () {
        return this.toArray();
    }));
    selectedPicker.append(sortedSelected);
}

function Sort(options) {
    options.sort(function (first, second) {
        return $(first).text() > $(second).text() ? 1 : -1;
    });

    return options;
}

function GetSelectedProductCodeValueForService(obj) {
    obj['sSelectedProductCodeFilter'] = $(selectedCodesJsonId).val();
}

function ReloadProductCodesFromServiceResult(resultValue) {
    ReloadProductCodes(resultValue['sAvailableProductCodeFilter'], resultValue['sSelectedProductCodeFilter'], resultValue['sProductCodesByFileType']);
}

function RegisterPostApplyProductCodesCallback(callback)
{
    for (var i = 0; i < postApplyCallbacks.length; i++) {
        if (postApplyCallbacks[i] == callback) {
            return;
        }
    }

    postApplyCallbacks.push(callback);
}

jQuery(function ($) {
    LoadPickers();
    GenerateTooltip();
    registerPostRefreshCalculationCallback(ReloadProductCodesFromServiceResult);
    registerPostGetAllFormValuesCallback(GetSelectedProductCodeValueForService);

    $('#cancelBtn').click(function () {
        var availablePicker = $('#availableProductCodes');
        availablePicker.empty();
        availablePicker.append(oldAvailableHtml);
        var selectedPicker = $('#selectedProductCodes');
        selectedPicker.empty();
        selectedPicker.append(oldSelectedHtml);
        $('#availableCodeSearchBox').val('');
        $('#AdvancedFilterPopup').dialog('close');
    });

    $('#applyBtn').click(function () {
        var selected = {};
        $('#selectedProductCodes').find('option').each(function () {
            selected[$(this).val()] = $(this).is(':visible');
        });

        $(selectedCodesJsonId).val(JSON.stringify(selected));
            
        $('#availableCodeSearchBox').val('');
        $('#AdvancedFilterPopup').dialog('close');

        updateDirtyBit();
        for (var i = 0; i < postApplyCallbacks.length; i++) {
            postApplyCallbacks[i]();
        }
    });

    $('#addBtn').click(function () {
        $('#availableProductCodes').find('option:selected').remove().prop('selected', false).appendTo('#selectedProductCodes');
        var selected = $('#selectedProductCodes');
        selected.append(Sort(selected.find("option:not('.Hidden')").remove()));
    });

    $("#addAllBtn").click(function () {
        $('#availableProductCodes').find("option:not('.Hidden')").remove().prop('selected', false).appendTo('#selectedProductCodes');
        var selected = $('#selectedProductCodes');
        selected.append(Sort(selected.find("option:not('.Hidden')").remove()));
    });

    $("#removeBtn").click(function () {
        $('#selectedProductCodes').find('option:selected').remove().prop('selected', false).appendTo('#availableProductCodes');
        lastSearchTerm = "";
        TriggerSearch();
        var selected = $('#availableProductCodes');
        selected.append(Sort(selected.find("option:not('.Hidden')").remove()));
    });

    $("#removeAllBtn").click(function () {
        $('#selectedProductCodes').find("option:not('.Hidden')").remove().prop('selected', false).appendTo('#availableProductCodes');
        lastSearchTerm = "";
        TriggerSearch();
        var selected = $('#availableProductCodes');
        selected.append(Sort(selected.find("option:not('.Hidden')").remove()));
    });

    $("#selectNoneLink").click(function () {
        $('#selectedProductCodes').find('option:selected').prop('selected', false);
        $('#availableProductCodes').find('option:selected').prop('selected', false);
    });

    var timer;
    var delay = 500;
    $("#availableCodeSearchBox").on('input propertychange', function () {
        clearTimeout(timer);
        timer = setTimeout(function () {
            TriggerSearch();
        }, delay);
    });

    function TriggerSearch()
    {
        var searchBox = $('#availableCodeSearchBox');
        var currentTerm = searchBox.val().toLowerCase();
        if (currentTerm === lastSearchTerm) {
            return;
        }

        $('#availableProductCodes').find('span').each(function () {
            var option = $(this).find('option').removeClass('Hidden');
            $(this).replaceWith(option);
        });

        var terms = currentTerm.split(",");
        if (currentTerm !== '') {
            $('#availableProductCodes').find('option').each(function () {
                var option = $(this);
                var value = option.val().toLowerCase();

                var hasExclude = false;
                var hasInclude = false;
                var matchIncludeTerms = false;
                var matchExcludeTerms = false;
                $(terms).each(function (i, searchTerm) {
                    var currentTerm = $.trim(searchTerm);
                    var isNegation = currentTerm.indexOf('-') === 0;

                    if (isNegation) {
                        hasExclude = true;
                        currentTerm = currentTerm.substring(1);
                        matchExcludeTerms = matchExcludeTerms || value.indexOf(currentTerm) === -1;
                    }
                    else {
                        hasInclude = true;
                        matchIncludeTerms = matchIncludeTerms || value.indexOf(currentTerm) !== -1;
                    }
                });

                if ((hasInclude && !matchIncludeTerms) || (hasExclude && !matchExcludeTerms)) {
                    option.addClass('Hidden').wrap('<span>');
                }
            });
        }

        lastSearchTerm = currentTerm;
    }
});
