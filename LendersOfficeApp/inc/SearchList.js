﻿"use strict";
/*
    This is code primarily for the SearchListUserControl. It is untested for any other purpose.
*/

//
// function:
//
// enableSearchPanel
//
// description:
//
// Enable the ui elements for use by client.
//

function enableSearchPanel(oControl, oRoot, sArg) {
        // Validate controls and base.

        if (oControl == null || oRoot == null) {
            return;
        }

        // Turn on the controls.

        oRoot.style.backgroundColor = "white";

        getElementByIdFromParent(oControl, "m_Next").disabled = false;
        getElementByIdFromParent(oControl, "m_Prev").disabled = false;

        setDisabledAttr(oRoot, false);
}

function enableSearchExtra(oControl, oText, sArg) {
        // Validate controls and base.

        if (oControl == null || oText == null) {
            return;
        }

        // Turn on the controls.

        oText.style.backgroundColor = "white";

        oText.disabled = false;
}

//
// function:
//
// dsableSearchPanel
//
// description:
//
// Prevent ui elements from interacting.
//

function dsableSearchPanel(oControl, oRoot, sArg) {
        // Validate controls and base.

        if (oControl == null || oRoot == null) {
            return;
        }

        // Turn off the controls.
        var items = _filterItem(oRoot, function(item, itemAttrs){ return itemAttrs.key != null && itemAttrs.type != null && item.className.search(/Picked/i) != -1 });
        for (var i = 0; i < items.length; i++) {
            var element = items[i];
            item.className = item.className.replace(/Picked/i, "Item");
        }

        oRoot.style.backgroundColor = "Gainsboro";

        getElementByIdFromParent(oControl, "m_Next").disabled = true;
        getElementByIdFromParent(oControl, "m_Prev").disabled = true;

        setDisabledAttr(oRoot, true);
}

function dsableSearchExtra(oControl, oText, sArg) {
        // Validate controls and base.

        if (oControl == null || oText == null) {
            return;
        }

        // Turn off the controls.

        oText.style.backgroundColor = "Gainsboro";

        oText.disabled = true;
}

//
// function:
//
// findSelectedItem
//
// description:
//
// Scroll to the currently selected item.  We always
// want to start the control this way, because it is
// too difficult to find the last selected item after
// a postback.
//

function findSelectedItem(oContainer, oStore, sCode) {
    if (oContainer == null || oStore == null) {
        return;
    }
        // Search for the match and scroll to it.

        var item = _findItem(oContainer, function(key, type, item){ return key == oStore.value && type == "Item" });
        if (item != null) item.previousSibling.scrollIntoView(true);
}

function _findItem(parent, predicate){
    for (var i = 0; i < parent.children.length; ++i) {
        var item = parent.children[i];
        var itemAttrs = _getItemAttrs(item);
        if (itemAttrs.key == null || itemAttrs.type == null) continue;

        if (predicate(item, itemAttrs, i)) return item;
    }
    return null;
}
function _findItemRight(parent, predicate){
    var n = parent.children.length;

    for (var i = n-1; i > 0; --i) {
        var item = parent.children[i];
        var itemAttrs = _getItemAttrs(item);
        if (itemAttrs.key == null || itemAttrs.type == null) continue;

        if (predicate(item, itemAttrs, i)) return item;
    }
    return null;
}
function _findItemIndex(parent, predicate){
    for (var i = 0; i < parent.children.length; ++i) {
        var item = parent.children[i];
        var itemAttrs = _getItemAttrs(item);
        if (itemAttrs.key == null || itemAttrs.type == null) continue;

        if (predicate(item, itemAttrs, i)) return i;
    }
    return null;
}
function _filterItem(parent, predicate) {
    var items = [];
    for (var i = 0; i < parent.children.length; ++i) {
        var item = parent.children[i];
        var itemAttrs = _getItemAttrs(item);
        if (itemAttrs.key == null || itemAttrs.type == null) continue;

        if (predicate(item, itemAttrs, i)) items.push(item);
    }
    return items;
}
function _getItemAttrs(item) {
    return {
        key    : item.getAttribute("key"),
        type   : item.getAttribute("type"),
        cid    : item.getAttribute("cid"),
        access : item.getAttribute("access"),
        column : item.getAttribute("column"),
        label  : item.getAttribute("label")
    };
}

//
// function:
//
// onSearchListItemPicked
//
// description:
//
// Handle current selection change events.
//

function onSearchListItemPicked(oContainer, oStore, oItem) {
    // Validate the item passed in.  It must conform
    // to our extra-info tag format.

    if (oContainer == null || oStore == null || oItem == null) {
        return;
    }

    var oItemAttrs = _getItemAttrs(oItem);
    if (oItemAttrs.cid == null || oItemAttrs.key == null) {
        return;
    }

    if (oItemAttrs.access != null && oItemAttrs.access === 'false') {
        return;
    }

    if (hasDisabledAttr(oContainer)) {
        return;
    }

    // Now, we turn off any sibling that is currently
    // picked.  We know this because the class would
    // have 'picked' appended to the name.
    var pickedItems = _filterItem(oContainer, function(item, itemAttrs){ return (itemAttrs.type == "Item" && (item.className.search(/Picked/i) != -1)) });
    for (var i = 0; i < pickedItems.length; i++) {
        var item = pickedItems[i];
        item.className = item.className.replace(/Picked/i, "Item");
    }

    if (oItemAttrs.type == "Item") {
        oItem.className = "SearchListPicked";
    }

    if (oStore.value != null) {
        oStore.value = oItemAttrs.key;
    }
}

//
// function:
//
// onSearchListNextMatchClick
//
// description:
//
// Find current match and continue with the given
// substring.
//

function onSearchListNextMatchClick(oContainer, oStore, oQuery) {
    // Validate the item passed in.  It must conform
    // to our extra-info tag format.

    if (oContainer == null || oStore == null || oQuery == null) {
        return;
    }

    if (hasDisabledAttr(oContainer)) {
        return;
    }

    // Get the containing panel.

    var panel = oContainer;
    var query = oQuery;
    var store = oStore;
    if (panel == null || query == null || store == null) return;

    // Search the children for the current selection,
    // then look for the next substring match.

    var c = -1, n = panel.children.length;
    c = _findItemIndex(panel, function(item, itemAttrs){ return (itemAttrs.key != null && itemAttrs.key == store.value) });

    var trimmedQuery = trim(query.value);
    var re = new RegExp(trimmedQuery, "i");

    var nextItem = _findItem(panel, function(item, itemAttrs, i){ return (c < i && itemAttrs.type == "Item" && (itemAttrs.label.search(re) != -1 || itemAttrs.key.search(re) != -1)) });
    if (nextItem == null) {
        if (confirm("Continue searching at the beginning?") == false) {
            return;
        }
        nextItem = _findItem(panel, function(item, itemAttrs, i){ return (itemAttrs.type == "Item" && (itemAttrs.label.search(re) != -1 || itemAttrs.key.search(re) != -1)) });
    }
    if (nextItem != null) {
        // Trigger a click event.  This should call
        // whatever picking we're doing.  We key off
        // of the previous item and scroll it into
        // view -- looks cleaner.

        var prev = nextItem.previousElementSibling;

        if (prev != null) prev.scrollIntoView(true);
        else nextItem.scrollIntoView(true);

        nextItem.click();

        return;
    }

    // We're here because we did not find a match,
    // so alert the user that we did not match.

    if (displayError != null) {
        displayError("Unable to find '" + trimmedQuery + "'");
    }
}

//
// function:
//
// onSearchListPrevMatchClick
//
// description:
//
// Find current match and continue with the given
// substring.
//

function onSearchListPrevMatchClick(oContainer, oStore, oQuery) {
    // Validate the item passed in.  It must conform
    // to our extra-info tag format.

    if (oContainer == null || oStore == null || oQuery == null) {
        return;
    }

    if (hasDisabledAttr(oContainer)) {
        return;
    }

    // Get the containing panel.

    var panel = oContainer;
    var query = oQuery;
    var store = oStore;
    if (panel == null || query == null || store == null) return;

    // Search the children for the current selection,
    // then look for the next substring match.

    var trimmedQuery = trim(query.value);
    var re = new RegExp(trimmedQuery, "i");

    // Get and validate the current item.  If the
    // key matches, we break out and continue the
    // search.
    var c = _findItemIndex(panel, function(item, itemAttrs){ return (itemAttrs.key == store.value) });

    var prevItem = _findItemRight(panel, function(item, itemAttrs, i){ return (i < c && itemAttrs.type == "Item" && (itemAttrs.label.search(re) != -1 || itemAttrs.key.search(re) != -1)) });
    if (prevItem == null) {
        if (confirm("Continue searching from the end?") == false) {
            return;
        }
        prevItem = _findItemRight(panel, function(item, itemAttrs, i){ return (itemAttrs.type == "Item" && (itemAttrs.label.search(re) != -1 || itemAttrs.key.search(re) != -1)) });
    }
    if (prevItem != null) {
        // Trigger a click event.  This should call
        // whatever picking we're doing.  We key off
        // of the previous item and scroll it into
        // view -- looks cleaner.

        var prev = prevItem.previousElementSibling;

        if (prev != null) prev.scrollIntoView(true);
        else prevItem.scrollIntoView(true);

        prevItem.click();

        return;
    }

    // We're here because we did not find a match,
    // so alert the user that we did not match.

    if (displayError != null) {
        displayError("Unable to find '" + trimmedQuery + "'");
    }
}

function onSelectDomain(oContainer, oStore, sDomain) {
    // Validate the control.

    if (oContainer == null || oStore == null || sDomain == null) {
        return;
    }

    if (hasDisabledAttr(oContainer)) {
        return;
    }

    if (sDomain == "") {
        return;
    }

    // Find the matching domain header and scroll
    // it into view.

    var selectedItem = _findItem(oStore, function(item, itemAttrs){ return (itemAttrs.label == sDomain || item.label == sDomain); });
    if (selectedItem != null) { selectedItem.scrollIntoView(true); return; }

    // We're here because we did not find a match,
    // so alert the user that we did not match.

    if (displayError != null) {
        displayError("Unable to find '" + sDomain + "'");
    }
}

//
// function:
//
// onPickCurrentListItem
//
// description:
//
// Submit the id to this control on the server.
//

function onPickCurrentListItem(oContainer, oStore, sPickId, sPickLabel, sUId, sCmd, sDoPostBack, sType, sAccess) {
    // Validate the control.
    if (oContainer == null || oStore == null || sPickId == null) {
        return;
    }

    if (hasDisabledAttr(oContainer)) {
        return;
    }

    if (sPickId == "") {
        return;
    }

    if (sType == 'Header')
        return;

    if (sAccess != null && sAccess === 'false')
        return

    // Submit the id to this control on the server.

    oStore.value = sPickId;

    // Forward the event to any listeners.

    if (fireReportEvent != null) {
        fireReportEvent(sCmd, sPickId + ":" + sPickLabel, oContainer);
    }

    // Postback if the designer associated a click
    // event with the pickable item.

    if (sDoPostBack != "False") {
        __doPostBack(sUId, "Pick:" + sPickId);
    }
}

//
// function:
//
// hidePickedFields
//
// description:
//
// Handle removing included from the list.
//

function hidePickedFields(oControl, oRoot, sArg) {
        // Validate controls and base.

        if (oControl == null || oRoot == null || sArg == null) {
            return;
        }

        // Turn off the controls.

        var i;

        if(typeof(stringSplitRemainder) != 'undefined')
        {
            sArg = stringSplitRemainder(sArg, ":", 2)[0];
        }
        else
        {
            sArg = sArg.split(":")[0];
        }

        // sArg = sArg.split(":")[0];

        var selectedItem = _findItem(oRoot, function(item, itemAttrs){ return (itemAttrs.type == "Item" && itemAttrs.key == sArg)});
        if (selectedItem != null) {
            selectedItem.className = "SearchListHidden";
            selectedItem.setAttribute("type", "Hidden");
        }
}

//
// function:
//
// showHiddenFields
//
// description:
//
// Handle returning removed to the list.
//

function showHiddenFields(oControl, oRoot, sArg) {
        // Validate controls and base.

        if (oControl == null || oRoot == null || sArg == null) {
            return;
        }

        // Remove the entry.

        var selectedItem = _findItem(oRoot, function(item, itemAttrs){ return (itemAttrs.type == "Hidden" && itemAttrs.key == sArg)});
        if (selectedItem != null) {
            selectedItem.className = "SearchListItem";
            selectedItem.setAttribute("type", "Item");

            var itemAttrs = _getItemAttrs(selectedItem);
            if (itemAttrs.access === 'false') {
                selectedItem.className += " disabledSearchlistitem";
            }
        }
}

//
// function:
//
// dropColumnFields
//
// description:
//
// Handle removing current field.
//

function dropColumnFields(oControl, oRoot, sArg) {
        // Validate controls and base.

        if (oControl == null || oRoot == null || sArg == null) {
            return;
        }

        var selectedItem = _findItem(oRoot, function(item, itemAttrs){ return (itemAttrs.type == "Item" && itemAttrs.key == sArg && itemAttrs.column != null)});
        if (selectedItem != null) {
            oRoot.removeChild(selectedItem);
        }
}
