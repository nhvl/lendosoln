﻿if (typeof(vbConfirmNo) != 'number') {
    //vbs is not working fall back to confirm
    vbConfirmNo = 7; 
    vbConfirmCancel = 2; //there is not a no option in confirm
    vbConfirmYes = 6; 
    
    ConfirmSave  = function(){
        var result = confirm("Do you want to save the changes?");
        
        if (result) {
            return vbConfirmYes;
        }
        else {
            return vbConfirmNo;
        }
    }
    
    ConfirmSaveNoCancel = ConfirmSave;
    
}