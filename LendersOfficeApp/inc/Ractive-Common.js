﻿
//This method will check for a web error.  If it exists, it will display the error, else, it will call the success function.
function checkWebError(d, successFunction) {
    if (d && d.d) {
        var m = JSON.parse(d.d);
        if (m.ErrorType == 'LoanFieldWritePermissionDenied') {
            f_displayFieldWriteDenied(m.UserMessage);
            return;
        }
        else if (m.ErrorType) {
            alert(m.UserMessage);
            return;
        }

        successFunction(d);
    }
}

function checkWebErrorWithName(d, webMethodName, successFunction) {
    if (d && d.d) {
        var m = JSON.parse(d.d);
        if (m.ErrorType == 'LoanFieldWritePermissionDenied') {
            f_displayFieldWriteDenied(m.UserMessage);
            return;
        }
        else if (m.ErrorType == "FieldInvalidValueException" && webMethodName.indexOf("CalculateData") > -1) {
            // We only want to notify the user that an invalid date
            // has been entered when that user attempts to save
            // the loan. Otherwise, the page will have already 
            // alerted the user through the ErrorMsg() function 
            // in mask.js.
            return;
        }
        else if (m.ErrorType) {
            alert(m.UserMessage);
            return;
        }

        successFunction(d);
    }
}

function queueRactiveUpdate(ractive, elementObject, newValue) {
    window.setTimeout(function () {
        elementObject.set("value", newValue);
        ractive.fire('recalc');
    }, 0);
}