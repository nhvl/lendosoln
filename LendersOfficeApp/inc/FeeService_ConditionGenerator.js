﻿var NewGfe_ValidValues = {
    desc: "Any text",
    //section: "One of the following: A1 / A2 / B3 / B4 / B5 / B6 / B7 / B8 / B9 / B10 / B11",
    apr: "One of the following: Yes / No",
    fha: "One of the following: Yes / No",
    bene: "A valid Agent role",
    dflp: "One of the following: Yes / No",
    tp: "One of the following: Yes / No",
    aff: "One of the following: Yes / No",
    can_shop: "One of the following: Yes / No",
    p: "Percent value formated to three decimal places",
    pt: "One of the following: Loan Amount / Purchase Price / Appraisal Value / Total Loan Amount",
    base: "Dollar value formatted to two decimals",
    paid_by: "One of the following: borr pd / seller / borr fin / lender / broker / other",
    pmt_at: "One of the following: at closing / outside of closing",
    remove: "One of the following: Yes / No",
    ssp: "A system ID and name of a Settlement Service Provider contact, separated by a colon"
}

var NewGfe_PropDescs = {
    desc: "Custom description for <fee>",
    //section: "Which GFE box is <fee> a part of?",
    apr: "Is <fee> included in APR calculation?",
    fha: "Is <fee> FHA allowable?",
    bene: "Who is <fee> paid to?",
    dflp: "Is <fee> deducted from loan proceeds?",
    tp: "Is <fee> paid to a third party?",
    aff: "Is that third party an affiliate?",
    can_shop: "Can the borrower shop for services covered by <fee>?",
    p: "Percent of selected base type component of <fee>",
    pt: "Base type used to calculate <fee>",
    base: "Fixed dollar component of <fee>",
    paid_by: "Who pays <fee>?",
    pmt_at: "Is <fee> paid at closing or outside of closing?",
    remove: "REMOVE <fee>",
    ssp: "Who is the Settlement Service Provider to associate with <fee>?"
}

var HousingExpenses_ValidValues = {
    expDesc: "Any text",
    taxT: "One of the following: Blank / Borough / City / County / Fire District / Local Improvement District / "
    + "Miscellaneous / Municipal Utility District / School / Special Assessment District / Town / Township / Utility / "
    + "Village / Waste Fee District / Water/Irrigation / Water/Sewer",
    annAmtCalcT: "One of the following: Calculator / Disbursements",
    isPrepaid: "One of the following: Yes / No",
    isEscrowed: "One of the following: Yes / No",
    annAmtPerc: "Percent value formated to three decimal places",
    annAmtBase: "One of the following: Loan Amount / Purchase Price / Appraisal Value / Total Loan Amount",
    monAmtFixed: "Dollar value formatted to two decimals",
    prepMnth: "Whole number",
    Cush: "Whole number",
    rsrvMonLckd: "One of the following: Yes / No",
    rsrvMon: "Whole number",
    RepInterval: "One of the following: Annual / Annually in closing month / Monthly",
    disbursementSchedule: "A disbursement schedule where the disbursements sum to 12."
}

var HousingExpenses_PropDescs = {
    expDesc: "Custom description for <fee>",
    taxT: "The classification of the taxing authority that is levying the tax or the classification of the tax itself.",
    annAmtCalcT: "Annual amount calculation type",
    isPrepaid: "Prepaid?",
    isEscrowed: "Escrowed?",
    annAmtPerc: "Percent of selected base type component of monthly <fee>",
    annAmtBase: "Base type used to calculate monthly <fee>",
    monAmtFixed: "Fixed dollar component of monthly <fee>",
    prepMnth: "How many months of <fee> must be paid in advance?",
    Cush: "The escrow cushion for the <fee> reserve.",
    rsrvMonLckd: "Manually enter number of reserve months.",
    rsrvMon: "Number of reserve months for <fee>.",
    RepInterval: "Disbursement repetition interval",
    disbursementSchedule: "The escrow disbursement schedule for the <fee> reserve."
}

function tf2yn(bool) {
    if (bool)
        return "Yes";
    else
        return "No";
}

function writeLine(fieldInfoHash, value, lineNumber, fieldId, conditions) {
    return writeLineImpl(value, lineNumber, fieldId, fieldInfoHash[fieldId].ValidValueEntryOptions,
                fieldInfoHash[fieldId].DescriptionOfField, conditions, "Legacy", "", "");
}

// Use tab seperated values so lines can be pasted directly into Excel
function writeLineImpl(value, lineNumber, fieldName, validValues, propDesc, conditions, ruleType, feeTypeId, feeProp)
{
    var line = '';
    line += value + '\t';
    line += lineNumber + '\t';
    line += fieldName + '\t';
    line += validValues + '\t';
    line += propDesc + '\t';
    line += conditions + (ML.IsEnableNewFeeService ? "\t" : "\n");

    // The following columns should only be included if new GFE is enabled
    if (ML.IsEnableNewFeeService) {
        line += ruleType + '\t';
        line += feeTypeId + '\t';
        line += feeProp + '\n';
    }
    
    return line;
}

function OldGfe_GenerateTSV(lineNumber, $selectedRow, escrowOnly, callback, errorCallback) {
    // Get Field Info
    gService.System.callAsyncSimple('GetFieldInfo', { LineNumber: lineNumber }, function (result) {
        var fieldInfoHash = JSON.parse(result.value.obj);

        var resultString = GenerateTSV(lineNumber, $selectedRow, fieldInfoHash, escrowOnly, errorCallback);

        if (typeof callback === 'function') {
            callback(resultString);
        }
    });    
}

function NewGfe_GenerateTSV($selectedRow, callback, errorCallback) {
    var feeTypeId = $(".feeType").val();
    
    if (feeTypeId == "Options")
        return OldGfe_GenerateTSV('Options', $selectedRow, false, callback, errorCallback);
    else if (feeTypeId == "LENDER_CREDITS")
        return NewGfe_GenerateLenderCreditsTSV(callback);
    else if (feeTypeId == "TITLE_SOURCE")
        return GenerateTitleSourceTSV(callback);
//    else if (feeTypeId == ML.Hud1000AggregateAdjustmentFeeTypeId)
//        return NewGfe_GenerateAggregateEscrowTSV(feeTypeId);
    else if (feeTypeId == ML.Hud1000MortgageInsuranceReserveFeeTypeId)
        return NewGfe_GenerateMortgageInsuranceTSV(feeTypeId, callback, errorCallback);
    else if ($.inArray(feeTypeId, HousingExpensesFeeTypeIds) != -1)
        return NewGfe_GenerateHousingExpensesTSV(feeTypeId, callback, errorCallback);
    else
        return NewGfe_GenerateClosingCostFeeTSV(feeTypeId, callback);
}

// Create tab separated values for Title Fee Source reules.
function GenerateTitleSourceTSV(callback) {
    var conditions = GetConditions();

    var titleFeeType;
    var fieldName;
    var feeType = $('.feeType option:selected').text().toLowerCase();
    switch (feeType) {
        case "title fees source":
            fieldName = "titleFeeSource";
            titleFeeType = 'Title';
            break;
        case 'closing fees source':
            fieldName = "closingFeeSource";
            titleFeeType = 'Closing';
            break;
        case 'recording fees source':
            fieldName = "recordingFeeSource";
            titleFeeType = 'Recording';
            break;
    }

    var selectedProvider = $('.TitleVendor option:selected');
    var value = selectedProvider.text();
    var vendorId = selectedProvider.val();

    // Grab valid values from drop down.
    var validValues = "One of the following: ";
    var titleVendorOptions = $('.TitleVendor option');
    for (var i = 1; i < titleVendorOptions.length; i++) {   // Note: skips blank option.
        validValues += titleVendorOptions[i].text;

        if (i < titleVendorOptions.length - 1) {
            validValues += " / ";
        }
    }

    var propDesc = "What is the source of <titleFeeType> Fees?";

    var out = writeLineImpl(value, "", fieldName, validValues, propDesc.replace("<titleFeeType>", titleFeeType), conditions, "TitleSource", vendorId, "");

    if (typeof callback === 'function') {
        callback(out);
    }
}

// Create tab separated values for CFPB Lender Credits
function NewGfe_GenerateLenderCreditsTSV(callback) {
    // Get Field Info asynchronously
    var ajaxCallsRemaining = 2;
    var returnedObject = { LcFieldInfoHash: null, l802FieldInfoHash: null};

    gService.System.callAsyncSimple('GetFieldInfo', { LineNumber: "Lender Credits" }, function (result) {
        returnedObject.LcFieldInfoHash = JSON.parse(result.value.obj);
        --ajaxCallsRemaining;
        if (ajaxCallsRemaining <= 0) {
            NewGfe_GenerateLenderCreditsTSVCallback(returnedObject, callback);
        }
    });

    gService.System.callAsyncSimple('GetFieldInfo', { LineNumber: "802" }, function (result) {
        returnedObject.l802FieldInfoHash = JSON.parse(result.value.obj);
        --ajaxCallsRemaining;
        if (ajaxCallsRemaining <= 0) {
            NewGfe_GenerateLenderCreditsTSVCallback(returnedObject, callback);
        }
    });
}

function NewGfe_GenerateLenderCreditsTSVCallback(returnedObject, callback) {
    // Get conditions
    var conditions = GetConditions();

    // Get shared line values
    var sLenderCreditCalculationMethodT = $('.sLenderCreditCalculationMethodT').children(':selected').text();

    // Write output.
    // NOTE: Since these fields don't belong to any fee we will be treating them like the older format fieldId based Fee Service rules.
    var out = writeLine(returnedObject.LcFieldInfoHash, sLenderCreditCalculationMethodT, "Lender Credits", 'sLenderCreditCalculationMethodT', conditions);
    if ($('.sLenderCreditCalculationMethodT').val() == "0") {  // Set Manually
        // Get property values
        var sLDiscntPc = $(".sLDiscntPc").val();
        var sLDiscntBaseT = $(".sLDiscntBaseT").children(':selected').text();
        var sLDiscntFMb = $(".sLDiscntFMb").val();
        var sGfeCreditLenderPaidItemT = $(".sGfeCreditLenderPaidItemT").children(':selected').text();
        var sLenderCustomCredit1Description = $(".sLenderCustomCredit1Description2").val();
        var sLenderCustomCredit1Amount = $(".sLenderCustomCredit1Amount2").val();
        var sLenderCustomCredit2Description = $(".sLenderCustomCredit2Description2").val();
        var sLenderCustomCredit2Amount = $(".sLenderCustomCredit2Amount2").val();
        //var sToleranceCureCalculationT = $(".sToleranceCureCalculationT").children(':selected').text();

        if (sLDiscntPc)
            out += writeLine(returnedObject.l802FieldInfoHash, sLDiscntPc, "802", 'sLDiscntPc', conditions);

        out += writeLine(returnedObject.l802FieldInfoHash, sLDiscntBaseT, "802", 'sLDiscntBaseT', conditions);

        if (sLDiscntFMb)
            out += writeLine(returnedObject.l802FieldInfoHash, sLDiscntFMb, "802", 'sLDiscntFMb', conditions);

        out += writeLine(returnedObject.LcFieldInfoHash, sGfeCreditLenderPaidItemT, "Lender Credits", 'sGfeCreditLenderPaidItemT', conditions);

        if (sLenderCustomCredit1Description)
            out += writeLine(returnedObject.LcFieldInfoHash, sLenderCustomCredit1Description, "Lender Credits", 'sLenderCustomCredit1Description', conditions);

        if (sLenderCustomCredit1Amount)
            out += writeLine(returnedObject.LcFieldInfoHash, sLenderCustomCredit1Amount, "Lender Credits", 'sLenderCustomCredit1Amount', conditions);

        if (sLenderCustomCredit2Description)
            out += writeLine(returnedObject.LcFieldInfoHash, sLenderCustomCredit2Description, "Lender Credits", 'sLenderCustomCredit2Description', conditions);

        if (sLenderCustomCredit2Amount)
            out += writeLine(returnedObject.LcFieldInfoHash, sLenderCustomCredit2Amount, "Lender Credits", 'sLenderCustomCredit2Amount', conditions);
        
        //out += writeLine(returnedObject.LcFieldInfoHash, sToleranceCureCalculationT, "Lender Credits", 'sToleranceCureCalculationT', conditions);
    } else { // Calculated from Front-end Rate Lock
        // Get property values
        var sLDiscntBaseT = $(".sLDiscntBaseT2").children(':selected').text();
        var sLenderCreditMaxT = $(".sLenderCreditMaxT").children(':selected').text();
        var sLenderPaidFeeDiscloseLocationT = $(".sLenderPaidFeeDiscloseLocationT").children(':selected').text();
        var sLenderGeneralCreditDiscloseLocationT = $(".sLenderGeneralCreditDiscloseLocationT").children(':selected').text();
        var sLenderCustomCredit1DiscloseLocationT = $(".sLenderCustomCredit1DiscloseLocationT").children(':selected').text();
        var sLenderCustomCredit1Description = $(".sLenderCustomCredit1Description").val();
        var sLenderCustomCredit1Amount = $(".sLenderCustomCredit1Amount").val();
        var sLenderCustomCredit2DiscloseLocationT = $(".sLenderCustomCredit2DiscloseLocationT").children(':selected').text();
        var sLenderCustomCredit2Description = $(".sLenderCustomCredit2Description").val();
        var sLenderCustomCredit2Amount = $(".sLenderCustomCredit2Amount").val();

        out += writeLine(returnedObject.l802FieldInfoHash, sLDiscntBaseT, "802", 'sLDiscntBaseT', conditions);
        out += writeLine(returnedObject.LcFieldInfoHash, sLenderCreditMaxT, "Lender Credits", 'sLenderCreditMaxT', conditions);
        out += writeLine(returnedObject.LcFieldInfoHash, sLenderPaidFeeDiscloseLocationT, "Lender Credits", 'sLenderPaidFeeDiscloseLocationT', conditions);
        out += writeLine(returnedObject.LcFieldInfoHash, sLenderGeneralCreditDiscloseLocationT, "Lender Credits", 'sLenderGeneralCreditDiscloseLocationT', conditions);
        out += writeLine(returnedObject.LcFieldInfoHash, sLenderCustomCredit1DiscloseLocationT, "Lender Credits", 'sLenderCustomCredit1DiscloseLocationT', conditions);

        if (sLenderCustomCredit1Description)
            out += writeLine(returnedObject.LcFieldInfoHash, sLenderCustomCredit1Description, "Lender Credits", 'sLenderCustomCredit1Description', conditions);

        if (sLenderCustomCredit1Amount)
            out += writeLine(returnedObject.LcFieldInfoHash, sLenderCustomCredit1Amount, "Lender Credits", 'sLenderCustomCredit1Amount', conditions);

        out += writeLine(returnedObject.LcFieldInfoHash, sLenderCustomCredit2DiscloseLocationT, "Lender Credits", 'sLenderCustomCredit2DiscloseLocationT', conditions);

        if (sLenderCustomCredit2Description)
            out += writeLine(returnedObject.LcFieldInfoHash, sLenderCustomCredit2Description, "Lender Credits", 'sLenderCustomCredit2Description', conditions);

        if (sLenderCustomCredit2Amount)
            out += writeLine(returnedObject.LcFieldInfoHash, sLenderCustomCredit2Amount, "Lender Credits", 'sLenderCustomCredit2Amount', conditions);
    }

    if (typeof callback === "function") {
        callback(out);
    }
}

//function NewGfe_GenerateAggregateEscrowTSV(feeTypeId) {
//    // Get Field Info
//    var lineNumber = "Aggregate Escrow";
//    var result = gService.System.call('GetFieldInfo', { LineNumber: lineNumber });
//    var fieldInfoHash = JSON.parse(result.value.obj);

//    // Get conditions
//    var conditions = GetConditions();

//    // Get property values.
//    var sAggEscrowCalcModeT = $(".sAggEscrowCalcModeT").children(':selected').text();
//    var sCustomaryEscrowImpoundsCalcMinT = $(".sCustomaryEscrowImpoundsCalcMinT").children(':selected').text();

//    // Write lines
//    var out = '';
//    out += writeLine(fieldInfoHash, sAggEscrowCalcModeT, lineNumber, 'sAggEscrowCalcModeT', conditions);

//    if (sAggEscrowCalcModeT == 'Customary') {
//        out += writeLine(fieldInfoHash, sCustomaryEscrowImpoundsCalcMinT, lineNumber, 'sCustomaryEscrowImpoundsCalcMinT', conditions);
//    }

//    return out;
//}

function NewGfe_GenerateMortgageInsuranceTSV(feeTypeId, callback, errorCallback) {
    // Get Field Info
    var lineNumber = "1003";
    gService.System.callAsyncSimple('GetFieldInfo', { LineNumber: lineNumber }, function (result) { NewGfe_GenerateMortgageInsuranceTSVCallback(result, lineNumber, callback, errorCallback); });
}

function NewGfe_GenerateMortgageInsuranceTSVCallback(result, lineNumber, callback, errorCallback) {
    var fieldInfoHash = JSON.parse(result.value.obj);

    // Get conditions
    var conditions = GetConditions();

    // Get property values
    var sMInsRsrvEscrowedTri = tf2yn($(".sMInsRsrvEscrowedTri").prop('checked'));
    var sMInsRsrvEscrowCushion = $(".sMInsRsrvEscrowCushion").val();
    var sMInsRsrvMonLckd = tf2yn($(".sMInsRsrvMonLckd").prop('checked'));
    var sMInsRsrvMon = $(".sMInsRsrvMon").val();
    var sMIPaymentRepeat = $("#sMIPaymentRepeat").children(':selected').text();

    var $tr = $(".sMInsRsrvEscrowDisbursementSchedule")
    var sMInsRsrvEscrowDisbursementSchedule = getEscrowDisbursementSchedule($tr);
    if ((sMInsRsrvEscrowCushion && !validateEscrowCushion(sMInsRsrvEscrowCushion)) || !sMInsRsrvEscrowDisbursementSchedule.Validate()) {
        if (typeof errorCallback === 'function') {
            errorCallback(new FeeServiceValidationError());
        }

        return;
    }

    // Write lines
    var out = '';
    out += writeLine(fieldInfoHash, sMInsRsrvEscrowedTri, lineNumber, 'sMInsRsrvEscrowedTri', conditions);

    if (sMInsRsrvEscrowCushion) {
        out += writeLine(fieldInfoHash, sMInsRsrvEscrowCushion, lineNumber, 'sMInsRsrvEscrowCushion', conditions);
    }

    if (sMInsRsrvMonLckd == "Yes" && sMInsRsrvMon) {
        out += writeLine(fieldInfoHash, sMInsRsrvMon, lineNumber, 'sMInsRsrvMon', conditions);
    }

    if (sMIPaymentRepeat) {
        out += writeLine(fieldInfoHash, sMIPaymentRepeat, lineNumber, "sMIPaymentRepeat", conditions);
    }

    if (sMIPaymentRepeat == "Annual" && sMInsRsrvEscrowDisbursementSchedule.IncludeInFeeServiceOutput()) {
        out += writeLine(fieldInfoHash, JSON.stringify(sMInsRsrvEscrowDisbursementSchedule), lineNumber, 'sMInsRsrvEscrowDisbursementSchedule', conditions);
    }

    if (typeof callback === "function") {
        callback(out);
    }
}

function NewGfe_GenerateHousingExpensesTSV(feeTypeId, callback, errorCallback) {
    // Get shared line values
    var conditions = GetConditions();
    var expName = $(".expName").text();

    // Get property values
    var expDesc = $(".expDesc").val();
    var taxT = $(".taxT").children(':selected').text();
    var annAmtCalcT = $(".annAmtCalcT").children(':selected').text();
    
    // Write lines
    var out = '';
    if (expDesc)
        out += writeLineImpl(expDesc, "", expName + ": Description", HousingExpenses_ValidValues.expDesc, HousingExpenses_PropDescs.expDesc.replace("<fee>", expName), conditions, "ClosingCost", feeTypeId, "Description");

    if ($.inArray(feeTypeId, TaxesFeeTypeIds) != -1)
        out += writeLineImpl(taxT, "", expName + ": Tax Type", HousingExpenses_ValidValues.taxT, HousingExpenses_PropDescs.taxT.replace("<fee>", expName), conditions, "ClosingCost", feeTypeId, "Tax Type");
    
    out += writeLineImpl(annAmtCalcT, "", expName + ": Calculation Source", HousingExpenses_ValidValues.annAmtCalcT, HousingExpenses_PropDescs.annAmtCalcT.replace("<fee>", expName), conditions, "ClosingCost", feeTypeId, "Calculation Source");

    // Only add the following fields if Calculation Source is Calculator.
    if ($(".annAmtCalcT").children(':selected').val() == 0) {
        // Get property values
        var isPrepaid = tf2yn($(".isPrepaid").prop('checked'));
        var isEscrowed = tf2yn($(".isEscrowed").prop('checked'));
        var annAmtPerc = $(".annAmtPerc").val();
        var annAmtBase = $(".annAmtBase").children(':selected').text();
        var monAmtFixed = $(".monAmtFixed").val();
        var prepMnth = $(".prepMnth").val();
        var Cush = $(".Cush").val();
        var rsrvMonLckd = tf2yn($(".rsrvMonLckd").prop('checked'));
        var rsrvMon = $(".rsrvMon").val();
        var RepInterval = $(".RepInterval").children(':selected').text();

        var $trDisbursements = $(".trDisbursements")
        var disbursementSchedule = getEscrowDisbursementSchedule($trDisbursements);
        if ((Cush && !validateEscrowCushion(Cush)) || !disbursementSchedule.Validate()) {
            if (typeof errorCallback === 'function') {
                errorCallback(new FeeServiceValidationError());
            }

            return;
        }

        // Write lines
        out += writeLineImpl(isPrepaid, "", expName + ": Prepaid", HousingExpenses_ValidValues.isPrepaid, HousingExpenses_PropDescs.isPrepaid.replace("<fee>", expName), conditions, "ClosingCost", feeTypeId, "Prepaid");
        out += writeLineImpl(isEscrowed, "", expName + ": Escrow", HousingExpenses_ValidValues.isEscrowed, HousingExpenses_PropDescs.isEscrowed.replace("<fee>", expName), conditions, "ClosingCost", feeTypeId, "Escrow");

        if (annAmtPerc)
            out += writeLineImpl(annAmtPerc, "", expName + ": Percent", HousingExpenses_ValidValues.annAmtPerc, HousingExpenses_PropDescs.annAmtPerc.replace("<fee>", expName), conditions, "ClosingCost", feeTypeId, "Percent");

        out += writeLineImpl(annAmtBase, "", expName + ": Base Value", HousingExpenses_ValidValues.annAmtBase, HousingExpenses_PropDescs.annAmtBase.replace("<fee>", expName), conditions, "ClosingCost", feeTypeId, "Base Value");

        if (monAmtFixed)
            out += writeLineImpl(monAmtFixed, "", expName + ": Amount", HousingExpenses_ValidValues.monAmtFixed, HousingExpenses_PropDescs.monAmtFixed.replace("<fee>", expName), conditions, "ClosingCost", feeTypeId, "Amount");

        if (isPrepaid == "Yes" && prepMnth)
            out += writeLineImpl(prepMnth, "", expName + ": Prepaid Months", HousingExpenses_ValidValues.prepMnth, HousingExpenses_PropDescs.prepMnth.replace("<fee>", expName), conditions, "ClosingCost", feeTypeId, "Prepaid Months");

        if (Cush)
            out += writeLineImpl(Cush, "", expName + ": Reserve Cushion", HousingExpenses_ValidValues.Cush, HousingExpenses_PropDescs.Cush.replace("<fee>", expName), conditions, "ClosingCost", feeTypeId, "Reserve Cushion");

        out += writeLineImpl(rsrvMonLckd, "", expName + ": Reserve Months Locked", HousingExpenses_ValidValues.rsrvMonLckd, HousingExpenses_PropDescs.expDesc.replace("<fee>", rsrvMonLckd), conditions, "ClosingCost", feeTypeId, "Reserve Months Locked");

        if (rsrvMonLckd == "Yes" && rsrvMon)
            out += writeLineImpl(rsrvMon, "", expName + ": Reserve Months", HousingExpenses_ValidValues.rsrvMon, HousingExpenses_PropDescs.rsrvMon.replace("<fee>", expName), conditions, "ClosingCost", feeTypeId, "Reserve Months");

        out += writeLineImpl(RepInterval, "", expName + ": Payment Repeat Interval", HousingExpenses_ValidValues.RepInterval, HousingExpenses_PropDescs.RepInterval.replace("<fee>", expName), conditions, "ClosingCost", feeTypeId, "Payment Repeat Interval");

        if (RepInterval == "Annual" && disbursementSchedule.IncludeInFeeServiceOutput())
            out += writeLineImpl(JSON.stringify(disbursementSchedule), "", expName + ": Disbursement Schedule", HousingExpenses_ValidValues.disbursementSchedule, HousingExpenses_PropDescs.disbursementSchedule.replace("<fee>", expName), conditions, "ClosingCost", feeTypeId, "Disbursement Schedule");
    }
    
    if (typeof callback === 'function') {
        callback(out);
    }
}

function NewGfe_GenerateClosingCostFeeTSV(feeTypeId, callback) {
    // Get shared line values
    var conditions = GetConditions();
    var lineNumber = $(".hudline").text();
    var org_desc = $(".org_desc").text();

    // If action is remove, just return single remove line
    var action = $(".action").val();
    if (action == "REMOVE") {
        var line = writeLineImpl("Yes", lineNumber, org_desc + ": Remove", NewGfe_ValidValues.remove, NewGfe_PropDescs.remove.replace("<fee>", org_desc), conditions, "ClosingCost", feeTypeId, "Remove");

        if (typeof callback === 'function') {
            callback(line);
        }

        return;
    }
    
    // Get property values
    var desc = $(".desc").val();
    //var section = $(".section").children(':selected').text();
    var apr = tf2yn($(".apr").prop('checked'));
    var fha = tf2yn($(".fha").prop('checked'));
    var bene = $(".bene").children(':selected').text();
    var dflp = tf2yn($(".dflp").prop('checked'));
    var tp = tf2yn($(".tp").prop('checked'));
    var aff = tf2yn($(".aff").prop('checked'));
    var can_shop = tf2yn($(".can_shop").prop('checked'));
    var p = $(".p").val();
    var pt = $(".pt").children(':selected').text();
    var base = $(".base").val();
    var paid_by = $(".paid_by").children(':selected').text();
    var pmt_at = $(".pmt_at").children(':selected').text();
    var ssp = $("#SelectedSsp").text();

    // Write lines
    var out = '';
    if (desc)
        out += writeLineImpl(desc, lineNumber, org_desc + ": Description", NewGfe_ValidValues.desc, NewGfe_PropDescs.desc.replace("<fee>", org_desc), conditions, "ClosingCost", feeTypeId, "Description");
        
    //out += writeLineImpl(section, lineNumber, org_desc + ": GFE Box", NewGfe_ValidValues.section, NewGfe_PropDescs.section.replace("<fee>", org_desc), conditions, "ClosingCost", feeTypeId, "GFE Box");
    out += writeLineImpl(apr, lineNumber, org_desc + ": APR", NewGfe_ValidValues.apr, NewGfe_PropDescs.apr.replace("<fee>", org_desc), conditions, "ClosingCost", feeTypeId, "APR");
    out += writeLineImpl(fha, lineNumber, org_desc + ": FHA", NewGfe_ValidValues.fha, NewGfe_PropDescs.fha.replace("<fee>", org_desc), conditions, "ClosingCost", feeTypeId, "FHA");
    out += writeLineImpl(bene, lineNumber, org_desc + ": Paid To", NewGfe_ValidValues.bene, NewGfe_PropDescs.bene.replace("<fee>", org_desc), conditions, "ClosingCost", feeTypeId, "Paid To");
    out += writeLineImpl(tp, lineNumber, org_desc + ": Third Party", NewGfe_ValidValues.tp, NewGfe_PropDescs.tp.replace("<fee>", org_desc), conditions, "ClosingCost", feeTypeId, "Third Party");
    out += writeLineImpl(aff, lineNumber, org_desc + ": Affiliate", NewGfe_ValidValues.aff, NewGfe_PropDescs.aff.replace("<fee>", org_desc), conditions, "ClosingCost", feeTypeId, "Affiliate");
    out += writeLineImpl(can_shop, lineNumber, org_desc + ": Can Shop", NewGfe_ValidValues.can_shop, NewGfe_PropDescs.can_shop.replace("<fee>", org_desc), conditions, "ClosingCost", feeTypeId, "Can Shop");
    out += writeLineImpl(dflp, lineNumber, org_desc + ": Deducted From Loan Proceeds", NewGfe_ValidValues.dflp, NewGfe_PropDescs.dflp.replace("<fee>", org_desc), conditions, "ClosingCost", feeTypeId, "Deducted From Loan Proceeds");

    // Don't output amount lines for mortage insurance
    if (feeTypeId != ML.Hud900DailyInterestFeeTypeId) {
        if (p)
            out += writeLineImpl(p, lineNumber, org_desc + ": Percent", NewGfe_ValidValues.p, NewGfe_PropDescs.p.replace("<fee>", org_desc), conditions, "ClosingCost", feeTypeId, "Percent");

        out += writeLineImpl(pt, lineNumber, org_desc + ": Base Value", NewGfe_ValidValues.pt, NewGfe_PropDescs.pt.replace("<fee>", org_desc), conditions, "ClosingCost", feeTypeId, "Base Value");

        if (base)
            out += writeLineImpl(base, lineNumber, org_desc + ": Amount", NewGfe_ValidValues.base, NewGfe_PropDescs.base.replace("<fee>", org_desc), conditions, "ClosingCost", feeTypeId, "Amount");
    }
    
    out += writeLineImpl(paid_by, lineNumber, org_desc + ": Paid By", NewGfe_ValidValues.paid_by, NewGfe_PropDescs.paid_by.replace("<fee>", org_desc), conditions, "ClosingCost", feeTypeId, "Paid By");
    out += writeLineImpl(pmt_at, lineNumber, org_desc + ": Payable", NewGfe_ValidValues.pmt_at, NewGfe_PropDescs.pmt_at.replace("<fee>", org_desc), conditions, "ClosingCost", feeTypeId, "Payable");

    if ($.trim(ssp) !== '') {
        out += writeLineImpl("\"" + ssp + "\"", lineNumber, org_desc + ": Settlement Service Provider", NewGfe_ValidValues.ssp, NewGfe_PropDescs.ssp.replace("<fee>", org_desc), conditions, "ClosingCost", feeTypeId, "Settlement Service Provider");
    }

    if (typeof callback === 'function') {
        callback(out);
    }
}

// Use tab seperated values so lines can be pasted directly into Excel
function GenerateTSV(lineNumber, $selectedRow, fieldInfoHash, escrowOnly, errorCallback) {
    //Get conditions
    var conditions = GetConditions();

    // Generate csv entries specific to selected line
    var out = '';
    switch (lineNumber) {
        case 'Options':
            var sDaysInYr = $selectedRow.find('input[data-fsid=sDaysInYr]').val();
            var sPricingEngineCostT = $selectedRow.find('select[data-fsid=sPricingEngineCostT]').children(':selected').text();
            var sPricingEngineCreditT = $selectedRow.find('select[data-fsid=sPricingEngineCreditT]').children(':selected').text();
            var sPricingEngineLimitCreditToT = $selectedRow.find('select[data-fsid=sPricingEngineLimitCreditToT]').children(':selected').text();

            if (sPricingEngineCreditT != 'None')
                sPricingEngineCreditT = sPricingEngineCreditT.substring(0, 3);

            if (sPricingEngineCostT != 'None')
                sPricingEngineCostT = sPricingEngineCostT.substring(0, 3);

            if (sDaysInYr)
                out += writeLine(fieldInfoHash, sDaysInYr, 'Calc Option', 'sDaysInYr', conditions);
            out += writeLine(fieldInfoHash, sPricingEngineCostT, 'Calc Option', 'sPricingEngineCostT', conditions);
            out += writeLine(fieldInfoHash, sPricingEngineCreditT, 'Calc Option', 'sPricingEngineCreditT', conditions);
            out += writeLine(fieldInfoHash, sPricingEngineLimitCreditToT, 'Calc Option', 'sPricingEngineLimitCreditToT', conditions);
            break;

        case '801':
            var sLOrigFPc = $selectedRow.find('input[data-fsid=sLOrigFPc]').val();
            var sLOrigFMb = $selectedRow.find('input[data-fsid=sLOrigFMb]').val();

            var sLOrigFProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sLOrigFProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sLOrigFProps_BF = tf2yn($selectedRow.find('input[data-fsid=Bf]').prop('checked'));
            var sLOrigFProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sSettlementLOrigFProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sLOrigFProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sLOrigFProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sLOrigFProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));

            if (sLOrigFPc)
                out += writeLine(fieldInfoHash, sLOrigFPc, lineNumber, 'sLOrigFPc', conditions);
            if (sLOrigFMb)
                out += writeLine(fieldInfoHash, sLOrigFMb, lineNumber, 'sLOrigFMb', conditions);

            out += writeLine(fieldInfoHash, sLOrigFProps_PdByT, lineNumber, 'sLOrigFProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sLOrigFProps_Apr, lineNumber, 'sLOrigFProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sLOrigFProps_BF, lineNumber, 'sLOrigFProps_BF', conditions);
            out += writeLine(fieldInfoHash, sLOrigFProps_FhaAllow, lineNumber, 'sLOrigFProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sSettlementLOrigFProps_Dflp, lineNumber, 'sSettlementLOrigFProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sLOrigFProps_PaidToThirdParty, lineNumber, 'sLOrigFProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sLOrigFProps_ThisPartyIsAffiliate, lineNumber, 'sLOrigFProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sLOrigFProps_ToBroker, lineNumber, 'sLOrigFProps_ToBroker', conditions);

            break;
        case '802':
            var sLDiscntPc = $selectedRow.find('input[data-fsid=sLDiscntPc]').val();
            var sLDiscntBaseT = $selectedRow.find('select[data-fsid=sLDiscntBaseT]').children(':selected').text();
            var sLDiscntFMb = $selectedRow.find('input[data-fsid=sLDiscntFMb]').val();

            var sGfeLenderCreditFProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').first().prop('checked'));
            var sGfeLenderCreditFProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').first().prop('checked'));

            var sGfeDiscountPointFProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').last().children(':selected').text();
            var sGfeDiscountPointFProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').last().prop('checked'));
            var sGfeDiscountPointFProps_BF = tf2yn($selectedRow.find('input[data-fsid=Bf]').prop('checked'));
            var sGfeDiscountPointFProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').last().prop('checked'));
            var sSettlementDiscountPointFProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').last().prop('checked'));
//            var sGfeDiscountPointFProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
//            var sGfeDiscountPointFProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            

            if (sLDiscntPc)
                out += writeLine(fieldInfoHash, sLDiscntPc, lineNumber, 'sLDiscntPc', conditions);

            out += writeLine(fieldInfoHash, sLDiscntBaseT, lineNumber, 'sLDiscntBaseT', conditions);

            if (sLDiscntFMb)
                out += writeLine(fieldInfoHash, sLDiscntFMb, lineNumber, 'sLDiscntFMb', conditions);


            out += writeLine(fieldInfoHash, sGfeLenderCreditFProps_Apr, lineNumber, 'sGfeLenderCreditFProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sGfeLenderCreditFProps_FhaAllow, lineNumber, 'sGfeLenderCreditFProps_FhaAllow', conditions);

            out += writeLine(fieldInfoHash, sGfeDiscountPointFProps_PdByT, lineNumber, 'sGfeDiscountPointFProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sGfeDiscountPointFProps_Apr, lineNumber, 'sGfeDiscountPointFProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sGfeDiscountPointFProps_BF, lineNumber, 'sGfeDiscountPointFProps_BF', conditions);
            out += writeLine(fieldInfoHash, sGfeDiscountPointFProps_FhaAllow, lineNumber, 'sGfeDiscountPointFProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sSettlementDiscountPointFProps_Dflp, lineNumber, 'sSettlementDiscountPointFProps_Dflp', conditions);
//            out += writeLine(fieldInfoHash, sGfeDiscountPointFProps_PaidToThirdParty, lineNumber, 'sGfeDiscountPointFProps_PaidToThirdParty', conditions);
//            out += writeLine(fieldInfoHash, sGfeDiscountPointFProps_ThisPartyIsAffiliate, lineNumber, 'sGfeDiscountPointFProps_ThisPartyIsAffiliate', conditions);

            break;
        case '804':
            var sApprF = $selectedRow.find('input[data-fsid=sApprF]').val();
            var sApprFProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sApprFProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sApprFProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sApprFProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sApprFPaid = tf2yn($selectedRow.find('input[data-fsid=Paid]').prop('checked'));
            var sSettlementApprFProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sApprFProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sApprFProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sApprFProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sApprFPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sApprF)
                out += writeLine(fieldInfoHash, sApprF, lineNumber, 'sApprF', conditions);

            out += writeLine(fieldInfoHash, sApprFProps_PdByT, lineNumber, 'sApprFProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sApprFProps_Apr, lineNumber, 'sApprFProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sApprFProps_FhaAllow, lineNumber, 'sApprFProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sApprFProps_Poc, lineNumber, 'sApprFProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sApprFPaid, lineNumber, 'sApprFPaid', conditions);
            out += writeLine(fieldInfoHash, sSettlementApprFProps_Dflp, lineNumber, 'sSettlementApprFProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sApprFProps_PaidToThirdParty, lineNumber, 'sApprFProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sApprFProps_ThisPartyIsAffiliate, lineNumber, 'sApprFProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sApprFProps_ToBroker, lineNumber, 'sApprFProps_ToBroker', conditions);

            if (sApprFPaidTo)
                out += writeLine(fieldInfoHash, sApprFPaidTo, lineNumber, 'sApprFPaidTo', conditions);

            break;
        case '805':
            var sCrF = $selectedRow.find('input[data-fsid=sCrF]').val();
            var sCrFProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sCrFProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sCrFProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sCrFProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sCrFPaid = tf2yn($selectedRow.find('input[data-fsid=Paid]').prop('checked'));
            var sSettlementCrFProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sCrFProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sCrFProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sCrFProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sCrFPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sCrF)
                out += writeLine(fieldInfoHash, sCrF, lineNumber, 'sCrF', conditions);

            out += writeLine(fieldInfoHash, sCrFProps_PdByT, lineNumber, 'sCrFProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sCrFProps_Apr, lineNumber, 'sCrFProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sCrFProps_FhaAllow, lineNumber, 'sCrFProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sCrFProps_Poc, lineNumber, 'sCrFProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sCrFPaid, lineNumber, 'sCrFPaid', conditions);
            out += writeLine(fieldInfoHash, sSettlementCrFProps_Dflp, lineNumber, 'sSettlementCrFProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sCrFProps_PaidToThirdParty, lineNumber, 'sCrFProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sCrFProps_ThisPartyIsAffiliate, lineNumber, 'sCrFProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sCrFProps_ToBroker, lineNumber, 'sCrFProps_ToBroker', conditions);

            if (sCrFPaidTo)
                out += writeLine(fieldInfoHash, sCrFPaidTo, lineNumber, 'sCrFPaidTo', conditions);

            break;
        case '806':
            var sTxServF = $selectedRow.find('input[data-fsid=sTxServF]').val();
            var sTxServFProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sTxServFProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sTxServFProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sTxServFProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementTxServFProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sTxServFProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sTxServFProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sTxServFProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sTxServFPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sTxServF)
                out += writeLine(fieldInfoHash, sTxServF, lineNumber, 'sTxServF', conditions);

            out += writeLine(fieldInfoHash, sTxServFProps_PdByT, lineNumber, 'sTxServFProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sTxServFProps_Apr, lineNumber, 'sTxServFProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sTxServFProps_FhaAllow, lineNumber, 'sTxServFProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sTxServFProps_Poc, lineNumber, 'sTxServFProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementTxServFProps_Dflp, lineNumber, 'sSettlementTxServFProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sTxServFProps_PaidToThirdParty, lineNumber, 'sTxServFProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sTxServFProps_ThisPartyIsAffiliate, lineNumber, 'sTxServFProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sTxServFProps_ToBroker, lineNumber, 'sTxServFProps_ToBroker', conditions);

            if (sTxServFPaidTo)
                out += writeLine(fieldInfoHash, sTxServFPaidTo, lineNumber, 'sTxServFPaidTo', conditions);

            break;
        case '807':
            var sFloodCertificationF = $selectedRow.find('input[data-fsid=sFloodCertificationF]').val();
            var sFloodCertificationDeterminationT = $selectedRow.find('select[data-fsid=sFloodCertificationDeterminationT]').children(':selected').text();

            var sFloodCertificationFProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sFloodCertificationFProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sFloodCertificationFProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sFloodCertificationFProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementFloodCertificationFProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sFloodCertificationFProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sFloodCertificationFProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sFloodCertificationFProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sFloodCertificationFPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sFloodCertificationF)
                out += writeLine(fieldInfoHash, sFloodCertificationF, lineNumber, 'sFloodCertificationF', conditions);

            out += writeLine(fieldInfoHash, sFloodCertificationDeterminationT, lineNumber, 'sFloodCertificationDeterminationT', conditions);

            out += writeLine(fieldInfoHash, sFloodCertificationFProps_PdByT, lineNumber, 'sFloodCertificationFProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sFloodCertificationFProps_Apr, lineNumber, 'sFloodCertificationFProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sFloodCertificationFProps_FhaAllow, lineNumber, 'sFloodCertificationFProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sFloodCertificationFProps_Poc, lineNumber, 'sFloodCertificationFProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementFloodCertificationFProps_Dflp, lineNumber, 'sSettlementFloodCertificationFProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sFloodCertificationFProps_PaidToThirdParty, lineNumber, 'sFloodCertificationFProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sFloodCertificationFProps_ThisPartyIsAffiliate, lineNumber, 'sFloodCertificationFProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sFloodCertificationFProps_ToBroker, lineNumber, 'sFloodCertificationFProps_ToBroker', conditions);

            if (sFloodCertificationFPaidTo)
                out += writeLine(fieldInfoHash, sFloodCertificationFPaidTo, lineNumber, 'sFloodCertificationFPaidTo', conditions);

            break;
        case '808':
            var sMBrokFPc = $selectedRow.find('input[data-fsid=sMBrokFPc]').val();
            var sMBrokFBaseT = $selectedRow.find('select[data-fsid=sMBrokFBaseT]').children(':selected').text();
            var sMBrokFMb = $selectedRow.find('input[data-fsid=sMBrokFMb]').val();

            var sMBrokFProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sMBrokFProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sMBrokFProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sMBrokFProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementMBrokFProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sMBrokFProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sMBrokFProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sMBrokFProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            //            var sMBrokFPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sMBrokFPc)
                out += writeLine(fieldInfoHash, sMBrokFPc, lineNumber, 'sMBrokFPc', conditions);

            if (sMBrokFBaseT)
                out += writeLine(fieldInfoHash, sMBrokFBaseT, lineNumber, 'sMBrokFBaseT', conditions);

            if (sMBrokFMb)
                out += writeLine(fieldInfoHash, sMBrokFMb, lineNumber, 'sMBrokFMb', conditions);

            out += writeLine(fieldInfoHash, sMBrokFProps_PdByT, lineNumber, 'sMBrokFProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sMBrokFProps_Apr, lineNumber, 'sMBrokFProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sMBrokFProps_FhaAllow, lineNumber, 'sMBrokFProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sMBrokFProps_Poc, lineNumber, 'sMBrokFProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementMBrokFProps_Dflp, lineNumber, 'sSettlementMBrokFProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sMBrokFProps_PaidToThirdParty, lineNumber, 'sMBrokFProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sMBrokFProps_ThisPartyIsAffiliate, lineNumber, 'sMBrokFProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sMBrokFProps_ToBroker, lineNumber, 'sMBrokFProps_ToBroker', conditions);

            //            if (sMBrokFPaidTo)
            //                out += writeLine(fieldInfoHash, sMBrokFPaidTo, lineNumber, 'sMBrokFPaidTo', conditions);

            break;
        case '809':
            var sInspectF = $selectedRow.find('input[data-fsid=sInspectF]').val();
            var sInspectFProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sInspectFProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sInspectFProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sInspectFProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementInspectFProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sInspectFProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sInspectFProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sInspectFProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sInspectFPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sInspectF)
                out += writeLine(fieldInfoHash, sInspectF, lineNumber, 'sInspectF', conditions);

            out += writeLine(fieldInfoHash, sInspectFProps_PdByT, lineNumber, 'sInspectFProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sInspectFProps_Apr, lineNumber, 'sInspectFProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sInspectFProps_FhaAllow, lineNumber, 'sInspectFProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sInspectFProps_Poc, lineNumber, 'sInspectFProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementInspectFProps_Dflp, lineNumber, 'sSettlementInspectFProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sInspectFProps_PaidToThirdParty, lineNumber, 'sInspectFProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sInspectFProps_ThisPartyIsAffiliate, lineNumber, 'sInspectFProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sInspectFProps_ToBroker, lineNumber, 'sInspectFProps_ToBroker', conditions);

            if (sInspectFPaidTo)
                out += writeLine(fieldInfoHash, sInspectFPaidTo, lineNumber, 'sInspectFPaidTo', conditions);

            break;
        case '810':
            var sProcF = $selectedRow.find('input[data-fsid=sProcF]').val();
            var sProcFProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sProcFProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sProcFProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sProcFProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sProcFPaid = tf2yn($selectedRow.find('input[data-fsid=Paid]').prop('checked'));
            var sSettlementProcFProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sProcFProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sProcFProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sProcFProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sProcFPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sProcF)
                out += writeLine(fieldInfoHash, sProcF, lineNumber, 'sProcF', conditions);

            out += writeLine(fieldInfoHash, sProcFProps_PdByT, lineNumber, 'sProcFProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sProcFProps_Apr, lineNumber, 'sProcFProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sProcFProps_FhaAllow, lineNumber, 'sProcFProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sProcFProps_Poc, lineNumber, 'sProcFProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sProcFPaid, lineNumber, 'sProcFPaid', conditions);
            out += writeLine(fieldInfoHash, sSettlementProcFProps_Dflp, lineNumber, 'sSettlementProcFProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sProcFProps_PaidToThirdParty, lineNumber, 'sProcFProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sProcFProps_ThisPartyIsAffiliate, lineNumber, 'sProcFProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sProcFProps_ToBroker, lineNumber, 'sProcFProps_ToBroker', conditions);

            if (sProcFPaidTo)
                out += writeLine(fieldInfoHash, sProcFPaidTo, lineNumber, 'sProcFPaidTo', conditions);

            break;
        case '811':
            var sUwF = $selectedRow.find('input[data-fsid=sUwF]').val();
            var sUwFProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sUwFProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sUwFProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sUwFProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementUwFProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sUwFProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sUwFProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sUwFProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sUwFPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sUwF)
                out += writeLine(fieldInfoHash, sUwF, lineNumber, 'sUwF', conditions);

            out += writeLine(fieldInfoHash, sUwFProps_PdByT, lineNumber, 'sUwFProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sUwFProps_Apr, lineNumber, 'sUwFProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sUwFProps_FhaAllow, lineNumber, 'sUwFProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sUwFProps_Poc, lineNumber, 'sUwFProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementUwFProps_Dflp, lineNumber, 'sSettlementUwFProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sUwFProps_PaidToThirdParty, lineNumber, 'sUwFProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sUwFProps_ThisPartyIsAffiliate, lineNumber, 'sUwFProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sUwFProps_ToBroker, lineNumber, 'sUwFProps_ToBroker', conditions);

            if (sUwFPaidTo)
                out += writeLine(fieldInfoHash, sUwFPaidTo, lineNumber, 'sUwFPaidTo', conditions);

            break;
        case '812':
            var sWireF = $selectedRow.find('input[data-fsid=sWireF]').val();
            var sWireFProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sWireFProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sWireFProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sWireFProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementWireFProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sWireFProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sWireFProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sWireFProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sWireFPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sWireF)
                out += writeLine(fieldInfoHash, sWireF, lineNumber, 'sWireF', conditions);

            out += writeLine(fieldInfoHash, sWireFProps_PdByT, lineNumber, 'sWireFProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sWireFProps_Apr, lineNumber, 'sWireFProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sWireFProps_FhaAllow, lineNumber, 'sWireFProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sWireFProps_Poc, lineNumber, 'sWireFProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementWireFProps_Dflp, lineNumber, 'sSettlementWireFProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sWireFProps_PaidToThirdParty, lineNumber, 'sWireFProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sWireFProps_ThisPartyIsAffiliate, lineNumber, 'sWireFProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sWireFProps_ToBroker, lineNumber, 'sWireFProps_ToBroker', conditions);

            if (sWireFPaidTo)
                out += writeLine(fieldInfoHash, sWireFPaidTo, lineNumber, 'sWireFPaidTo', conditions);

            break;
        case '813':
            var s800U1FDesc = $selectedRow.find('input[data-fsid=s800U1FDesc]').val();
            var s800U1F = $selectedRow.find('input[data-fsid=s800U1F]').val();

            var s800U1FGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var s800U1FProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var s800U1FProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var s800U1FProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var s800U1FProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlement800U1FProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var s800U1FProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var s800U1FProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var s800U1FProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var s800U1FPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (s800U1FDesc)
                out += writeLine(fieldInfoHash, s800U1FDesc, lineNumber, 's800U1FDesc', conditions);

            if (s800U1F)
                out += writeLine(fieldInfoHash, s800U1F, lineNumber, 's800U1F', conditions);

            if (s800U1FGfeSection)
                out += writeLine(fieldInfoHash, s800U1FGfeSection, lineNumber, 's800U1FGfeSection', conditions);

            out += writeLine(fieldInfoHash, s800U1FProps_PdByT, lineNumber, 's800U1FProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, s800U1FProps_Apr, lineNumber, 's800U1FProps_Apr', conditions);
            out += writeLine(fieldInfoHash, s800U1FProps_FhaAllow, lineNumber, 's800U1FProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, s800U1FProps_Poc, lineNumber, 's800U1FProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlement800U1FProps_Dflp, lineNumber, 'sSettlement800U1FProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, s800U1FProps_PaidToThirdParty, lineNumber, 's800U1FProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, s800U1FProps_ThisPartyIsAffiliate, lineNumber, 's800U1FProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, s800U1FProps_ToBroker, lineNumber, 's800U1FProps_ToBroker', conditions);

            if (s800U1FPaidTo)
                out += writeLine(fieldInfoHash, s800U1FPaidTo, lineNumber, 's800U1FPaidTo', conditions);

            break;
        case '814':
            var s800U2FDesc = $selectedRow.find('input[data-fsid=s800U2FDesc]').val();
            var s800U2F = $selectedRow.find('input[data-fsid=s800U2F]').val();

            var s800U2FGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var s800U2FProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var s800U2FProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var s800U2FProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var s800U2FProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlement800U2FProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var s800U2FProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var s800U2FProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var s800U2FProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var s800U2FPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (s800U2FDesc)
                out += writeLine(fieldInfoHash, s800U2FDesc, lineNumber, 's800U2FDesc', conditions);

            if (s800U2F)
                out += writeLine(fieldInfoHash, s800U2F, lineNumber, 's800U2F', conditions);

            if (s800U2FGfeSection)
                out += writeLine(fieldInfoHash, s800U2FGfeSection, lineNumber, 's800U2FGfeSection', conditions);

            out += writeLine(fieldInfoHash, s800U2FProps_PdByT, lineNumber, 's800U2FProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, s800U2FProps_Apr, lineNumber, 's800U2FProps_Apr', conditions);
            out += writeLine(fieldInfoHash, s800U2FProps_FhaAllow, lineNumber, 's800U2FProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, s800U2FProps_Poc, lineNumber, 's800U2FProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlement800U2FProps_Dflp, lineNumber, 'sSettlement800U2FProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, s800U2FProps_PaidToThirdParty, lineNumber, 's800U2FProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, s800U2FProps_ThisPartyIsAffiliate, lineNumber, 's800U2FProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, s800U2FProps_ToBroker, lineNumber, 's800U2FProps_ToBroker', conditions);

            if (s800U2FPaidTo)
                out += writeLine(fieldInfoHash, s800U2FPaidTo, lineNumber, 's800U2FPaidTo', conditions);

            break;
        case '815':
            var s800U3FDesc = $selectedRow.find('input[data-fsid=s800U3FDesc]').val();
            var s800U3F = $selectedRow.find('input[data-fsid=s800U3F]').val();

            var s800U3FGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var s800U3FProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var s800U3FProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var s800U3FProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var s800U3FProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlement800U3FProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var s800U3FProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var s800U3FProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var s800U3FProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var s800U3FPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (s800U3FDesc)
                out += writeLine(fieldInfoHash, s800U3FDesc, lineNumber, 's800U3FDesc', conditions);

            if (s800U3F)
                out += writeLine(fieldInfoHash, s800U3F, lineNumber, 's800U3F', conditions);

            if (s800U3FGfeSection)
                out += writeLine(fieldInfoHash, s800U3FGfeSection, lineNumber, 's800U3FGfeSection', conditions);

            out += writeLine(fieldInfoHash, s800U3FProps_PdByT, lineNumber, 's800U3FProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, s800U3FProps_Apr, lineNumber, 's800U3FProps_Apr', conditions);
            out += writeLine(fieldInfoHash, s800U3FProps_FhaAllow, lineNumber, 's800U3FProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, s800U3FProps_Poc, lineNumber, 's800U3FProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlement800U3FProps_Dflp, lineNumber, 'sSettlement800U3FProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, s800U3FProps_PaidToThirdParty, lineNumber, 's800U3FProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, s800U3FProps_ThisPartyIsAffiliate, lineNumber, 's800U3FProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, s800U3FProps_ToBroker, lineNumber, 's800U3FProps_ToBroker', conditions);

            if (s800U3FPaidTo)
                out += writeLine(fieldInfoHash, s800U3FPaidTo, lineNumber, 's800U3FPaidTo', conditions);

            break;
        case '816':
            var s800U4FDesc = $selectedRow.find('input[data-fsid=s800U4FDesc]').val();
            var s800U4F = $selectedRow.find('input[data-fsid=s800U4F]').val();

            var s800U4FGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var s800U4FProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var s800U4FProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var s800U4FProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var s800U4FProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlement800U4FProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var s800U4FProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var s800U4FProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var s800U4FProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var s800U4FPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (s800U4FDesc)
                out += writeLine(fieldInfoHash, s800U4FDesc, lineNumber, 's800U4FDesc', conditions);

            if (s800U4F)
                out += writeLine(fieldInfoHash, s800U4F, lineNumber, 's800U4F', conditions);

            if (s800U4FGfeSection)
                out += writeLine(fieldInfoHash, s800U4FGfeSection, lineNumber, 's800U4FGfeSection', conditions);

            out += writeLine(fieldInfoHash, s800U4FProps_PdByT, lineNumber, 's800U4FProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, s800U4FProps_Apr, lineNumber, 's800U4FProps_Apr', conditions);
            out += writeLine(fieldInfoHash, s800U4FProps_FhaAllow, lineNumber, 's800U4FProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, s800U4FProps_Poc, lineNumber, 's800U4FProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlement800U4FProps_Dflp, lineNumber, 'sSettlement800U4FProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, s800U4FProps_PaidToThirdParty, lineNumber, 's800U4FProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, s800U4FProps_ThisPartyIsAffiliate, lineNumber, 's800U4FProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, s800U4FProps_ToBroker, lineNumber, 's800U4FProps_ToBroker', conditions);

            if (s800U4FPaidTo)
                out += writeLine(fieldInfoHash, s800U4FPaidTo, lineNumber, 's800U4FPaidTo', conditions);

            break;
        case '817':
            var s800U5FDesc = $selectedRow.find('input[data-fsid=s800U5FDesc]').val();
            var s800U5F = $selectedRow.find('input[data-fsid=s800U5F]').val();

            var s800U5FGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var s800U5FProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var s800U5FProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var s800U5FProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var s800U5FProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlement800U5FProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var s800U5FProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var s800U5FProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var s800U5FProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var s800U5FPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (s800U5FDesc)
                out += writeLine(fieldInfoHash, s800U5FDesc, lineNumber, 's800U5FDesc', conditions);

            if (s800U5F)
                out += writeLine(fieldInfoHash, s800U5F, lineNumber, 's800U5F', conditions);

            if (s800U5FGfeSection)
                out += writeLine(fieldInfoHash, s800U5FGfeSection, lineNumber, 's800U5FGfeSection', conditions);

            out += writeLine(fieldInfoHash, s800U5FProps_PdByT, lineNumber, 's800U5FProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, s800U5FProps_Apr, lineNumber, 's800U5FProps_Apr', conditions);
            out += writeLine(fieldInfoHash, s800U5FProps_FhaAllow, lineNumber, 's800U5FProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, s800U5FProps_Poc, lineNumber, 's800U5FProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlement800U5FProps_Dflp, lineNumber, 'sSettlement800U5FProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, s800U5FProps_PaidToThirdParty, lineNumber, 's800U5FProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, s800U5FProps_ThisPartyIsAffiliate, lineNumber, 's800U5FProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, s800U5FProps_ToBroker, lineNumber, 's800U5FProps_ToBroker', conditions);

            if (s800U5FPaidTo)
                out += writeLine(fieldInfoHash, s800U5FPaidTo, lineNumber, 's800U5FPaidTo', conditions);

            break;
        case '901':
            var sIPiaDy = $selectedRow.find('input[data-fsid=sIPiaDy]').val();
            var sIPiaProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sIPiaProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sIPiaProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sIPiaProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementIPiaProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sIPiaProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sIPiaProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));

            if (sIPiaDy)
                out += writeLine(fieldInfoHash, sIPiaDy, lineNumber, 'sIPiaDy', conditions);

            out += writeLine(fieldInfoHash, sIPiaProps_PdByT, lineNumber, 'sIPiaProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sIPiaProps_Apr, lineNumber, 'sIPiaProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sIPiaProps_FhaAllow, lineNumber, 'sIPiaProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sIPiaProps_Poc, lineNumber, 'sIPiaProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementIPiaProps_Dflp, lineNumber, 'sSettlementIPiaProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sIPiaProps_PaidToThirdParty, lineNumber, 'sIPiaProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sIPiaProps_ThisPartyIsAffiliate, lineNumber, 'sIPiaProps_ThisPartyIsAffiliate', conditions);

            break;
        case '902':
            var sMipPiaProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sMipPiaProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sMipPiaProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sMipPiaProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sMipPiaProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
//            var sMipPiaProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
//            var sMipPiaProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sMipPiaPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            out += writeLine(fieldInfoHash, sMipPiaProps_PdByT, lineNumber, 'sMipPiaProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sMipPiaProps_Apr, lineNumber, 'sMipPiaProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sMipPiaProps_FhaAllow, lineNumber, 'sMipPiaProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sMipPiaProps_Poc, lineNumber, 'sMipPiaProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sMipPiaProps_Dflp, lineNumber, 'sMipPiaProps_Dflp', conditions);
//            out += writeLine(fieldInfoHash, sMipPiaProps_PaidToThirdParty, lineNumber, 'sMipPiaProps_PaidToThirdParty', conditions);
//            out += writeLine(fieldInfoHash, sMipPiaProps_ThisPartyIsAffiliate, lineNumber, 'sMipPiaProps_ThisPartyIsAffiliate', conditions);
            
            if (sMipPiaPaidTo)
                out += writeLine(fieldInfoHash, sMipPiaPaidTo, lineNumber, 'sMipPiaPaidTo', conditions);

            break;
        case '903':
            var sProHazInsR = $selectedRow.find('input[data-fsid=sProHazInsR]').val();
            var sProHazInsT = $selectedRow.find('select[data-fsid=sProHazInsT]').children(':selected').text();
            var sProHazInsMb = $selectedRow.find('input[data-fsid=sProHazInsMb]').val();
            var sHazInsPiaMon = $selectedRow.find('input[data-fsid=sHazInsPiaMon]').val();

            var sHazInsPiaProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sHazInsPiaProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sHazInsPiaProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sHazInsPiaProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementHazInsPiaProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sHazInsPiaProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sHazInsPiaProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sHazInsPiaPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sProHazInsR)
                out += writeLine(fieldInfoHash, sProHazInsR, lineNumber, 'sProHazInsR', conditions);

            out += writeLine(fieldInfoHash, sProHazInsT, lineNumber, 'sProHazInsT', conditions);

            if (sProHazInsMb)
                out += writeLine(fieldInfoHash, sProHazInsMb, lineNumber, 'sProHazInsMb', conditions);

            if (sHazInsPiaMon)
                out += writeLine(fieldInfoHash, sHazInsPiaMon, lineNumber, 'sHazInsPiaMon', conditions);

            out += writeLine(fieldInfoHash, sHazInsPiaProps_PdByT, lineNumber, 'sHazInsPiaProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sHazInsPiaProps_Apr, lineNumber, 'sHazInsPiaProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sHazInsPiaProps_FhaAllow, lineNumber, 'sHazInsPiaProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sHazInsPiaProps_Poc, lineNumber, 'sHazInsPiaProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementHazInsPiaProps_Dflp, lineNumber, 'sSettlementHazInsPiaProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sHazInsPiaProps_PaidToThirdParty, lineNumber, 'sHazInsPiaProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sHazInsPiaProps_ThisPartyIsAffiliate, lineNumber, 'sHazInsPiaProps_ThisPartyIsAffiliate', conditions);

            if (sHazInsPiaPaidTo)
                out += writeLine(fieldInfoHash, sHazInsPiaPaidTo, lineNumber, 'sHazInsPiaPaidTo', conditions);

            break;
        case '904':
            var s904PiaDesc = $selectedRow.find('input[data-fsid=s904PiaDesc]').val();
            var s904Pia = $selectedRow.find('input[data-fsid=s904Pia]').val();

            var s904PiaGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var s904PiaProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var s904PiaProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var s904PiaProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var s904PiaProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlement904PiaProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var s904PiaProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var s904PiaProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));

            if (s904PiaDesc)
                out += writeLine(fieldInfoHash, s904PiaDesc, lineNumber, 's904PiaDesc', conditions);

            if (s904Pia)
                out += writeLine(fieldInfoHash, s904Pia, lineNumber, 's904Pia', conditions);

            if (s904PiaGfeSection)
                out += writeLine(fieldInfoHash, s904PiaGfeSection, lineNumber, 's904PiaGfeSection', conditions);

            out += writeLine(fieldInfoHash, s904PiaProps_PdByT, lineNumber, 's904PiaProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, s904PiaProps_Apr, lineNumber, 's904PiaProps_Apr', conditions);
            out += writeLine(fieldInfoHash, s904PiaProps_FhaAllow, lineNumber, 's904PiaProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, s904PiaProps_Poc, lineNumber, 's904PiaProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlement904PiaProps_Dflp, lineNumber, 'sSettlement904PiaProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, s904PiaProps_PaidToThirdParty, lineNumber, 's904PiaProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, s904PiaProps_ThisPartyIsAffiliate, lineNumber, 's904PiaProps_ThisPartyIsAffiliate', conditions);

            break;
        case '905':
            var sVaFfProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sVaFfProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sVaFfProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sVaFfProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sVaFfProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sVaFfProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sVaFfProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sVaFfPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            out += writeLine(fieldInfoHash, sVaFfProps_PdByT, lineNumber, 'sVaFfProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sVaFfProps_Apr, lineNumber, 'sVaFfProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sVaFfProps_FhaAllow, lineNumber, 'sVaFfProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sVaFfProps_Poc, lineNumber, 'sVaFfProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sVaFfProps_Dflp, lineNumber, 'sVaFfProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sVaFfProps_PaidToThirdParty, lineNumber, 'sVaFfProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sVaFfProps_ThisPartyIsAffiliate, lineNumber, 'sVaFfProps_ThisPartyIsAffiliate', conditions);

            if (sVaFfPaidTo)
                out += writeLine(fieldInfoHash, sVaFfPaidTo, lineNumber, 'sVaFfPaidTo', conditions);
            break;
        case '906':
            var s900U1PiaDesc = $selectedRow.find('input[data-fsid=s900U1PiaDesc]').val();
            var s900U1Pia = $selectedRow.find('input[data-fsid=s900U1Pia]').val();

            var s900U1PiaGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var s900U1PiaProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var s900U1PiaProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var s900U1PiaProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var s900U1PiaProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlement900U1PiaProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var s900U1PiaProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var s900U1PiaProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));

            if (s900U1PiaDesc)
                out += writeLine(fieldInfoHash, s900U1PiaDesc, lineNumber, 's900U1PiaDesc', conditions);

            if (s900U1Pia)
                out += writeLine(fieldInfoHash, s900U1Pia, lineNumber, 's900U1Pia', conditions);

            if (s900U1PiaGfeSection)
                out += writeLine(fieldInfoHash, s900U1PiaGfeSection, lineNumber, 's900U1PiaGfeSection', conditions);

            out += writeLine(fieldInfoHash, s900U1PiaProps_PdByT, lineNumber, 's900U1PiaProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, s900U1PiaProps_Apr, lineNumber, 's900U1PiaProps_Apr', conditions);
            out += writeLine(fieldInfoHash, s900U1PiaProps_FhaAllow, lineNumber, 's900U1PiaProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, s900U1PiaProps_Poc, lineNumber, 's900U1PiaProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlement900U1PiaProps_Dflp, lineNumber, 'sSettlement900U1PiaProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, s900U1PiaProps_PaidToThirdParty, lineNumber, 's900U1PiaProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, s900U1PiaProps_ThisPartyIsAffiliate, lineNumber, 's900U1PiaProps_ThisPartyIsAffiliate', conditions);

            break;
        case '1002':
            var sHazInsRsrvMon = $selectedRow.find('input[data-fsid=sHazInsRsrvMon]').val();
            var sHazInsRsrvProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sHazInsRsrvProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sHazInsRsrvProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sHazInsRsrvProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementHazInsRsrvProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sHazInsRsrvProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sHazInsRsrvProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sHazInsRsrvEscrowedTri = tf2yn($selectedRow.find('input[data-fsid=Escrow]').is(':checked'));
            var sHazInsRsrvEscrowCushion = $.trim($selectedRow.find('input.Cushion').val());
            var sHazInsRsrvEscrowDisbursementSchedule = getEscrowDisbursementSchedule($selectedRow);
            if ((sHazInsRsrvEscrowCushion && !validateEscrowCushion(sHazInsRsrvEscrowCushion)) || !sHazInsRsrvEscrowDisbursementSchedule.Validate()) {
                if (typeof errorCallback === 'function') {
                    errorCallback(new FeeServiceValidationError());
                }

                return;
            }

            if (!escrowOnly) {
                if (sHazInsRsrvMon)
                    out += writeLine(fieldInfoHash, sHazInsRsrvMon, lineNumber, 'sHazInsRsrvMon', conditions);

                out += writeLine(fieldInfoHash, sHazInsRsrvProps_PdByT, lineNumber, 'sHazInsRsrvProps_PdByT', conditions);
                out += writeLine(fieldInfoHash, sHazInsRsrvProps_Apr, lineNumber, 'sHazInsRsrvProps_Apr', conditions);
                out += writeLine(fieldInfoHash, sHazInsRsrvProps_FhaAllow, lineNumber, 'sHazInsRsrvProps_FhaAllow', conditions);
                out += writeLine(fieldInfoHash, sHazInsRsrvProps_Poc, lineNumber, 'sHazInsRsrvProps_Poc', conditions);
                out += writeLine(fieldInfoHash, sSettlementHazInsRsrvProps_Dflp, lineNumber, 'sSettlementHazInsRsrvProps_Dflp', conditions);
                out += writeLine(fieldInfoHash, sHazInsRsrvProps_PaidToThirdParty, lineNumber, 'sHazInsRsrvProps_PaidToThirdParty', conditions);
                out += writeLine(fieldInfoHash, sHazInsRsrvProps_ThisPartyIsAffiliate, lineNumber, 'sHazInsRsrvProps_ThisPartyIsAffiliate', conditions);
            }

            out += writeLine(fieldInfoHash, sHazInsRsrvEscrowedTri, lineNumber, 'sHazInsRsrvEscrowedTri', conditions);

            if (sHazInsRsrvEscrowCushion) {
                out += writeLine(fieldInfoHash, sHazInsRsrvEscrowCushion, lineNumber, 'sHazInsRsrvEscrowCushion', conditions);
            }

            if (sHazInsRsrvEscrowDisbursementSchedule.IncludeInFeeServiceOutput()) {
                out += writeLine(fieldInfoHash, JSON.stringify(sHazInsRsrvEscrowDisbursementSchedule), lineNumber, 'sHazInsRsrvEscrowDisbursementSchedule', conditions);
            }
            break;
        case '1003':
            var sMInsRsrvMon = $selectedRow.find('input[data-fsid=sMInsRsrvMon]').val();
            var sMInsRsrvProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sMInsRsrvProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sMInsRsrvProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sMInsRsrvProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementMInsRsrvProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sMInsRsrvProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sMInsRsrvProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sMInsRsrvEscrowedTri = tf2yn($selectedRow.find('input[data-fsid=Escrow]').is(':checked'));
            var sMInsRsrvEscrowCushion = $.trim($selectedRow.find('input.Cushion').val());
            var sMInsRsrvEscrowDisbursementSchedule = getEscrowDisbursementSchedule($selectedRow);
            if ((sMInsRsrvEscrowCushion && !validateEscrowCushion(sMInsRsrvEscrowCushion)) || !sMInsRsrvEscrowDisbursementSchedule.Validate()) {
                if (typeof errorCallback === 'function') {
                    errorCallback(new FeeServiceValidationError());
                }

                return;
            }

            if (!escrowOnly) {
                if (sMInsRsrvMon)
                    out += writeLine(fieldInfoHash, sMInsRsrvMon, lineNumber, 'sMInsRsrvMon', conditions);

                out += writeLine(fieldInfoHash, sMInsRsrvProps_PdByT, lineNumber, 'sMInsRsrvProps_PdByT', conditions);
                out += writeLine(fieldInfoHash, sMInsRsrvProps_Apr, lineNumber, 'sMInsRsrvProps_Apr', conditions);
                out += writeLine(fieldInfoHash, sMInsRsrvProps_FhaAllow, lineNumber, 'sMInsRsrvProps_FhaAllow', conditions);
                out += writeLine(fieldInfoHash, sMInsRsrvProps_Poc, lineNumber, 'sMInsRsrvProps_Poc', conditions);
                out += writeLine(fieldInfoHash, sSettlementMInsRsrvProps_Dflp, lineNumber, 'sSettlementMInsRsrvProps_Dflp', conditions);
                out += writeLine(fieldInfoHash, sMInsRsrvProps_PaidToThirdParty, lineNumber, 'sMInsRsrvProps_PaidToThirdParty', conditions);
                out += writeLine(fieldInfoHash, sMInsRsrvProps_ThisPartyIsAffiliate, lineNumber, 'sMInsRsrvProps_ThisPartyIsAffiliate', conditions);
            }

            out += writeLine(fieldInfoHash, sMInsRsrvEscrowedTri, lineNumber, 'sMInsRsrvEscrowedTri', conditions);

            if (sMInsRsrvEscrowCushion) {
                out += writeLine(fieldInfoHash, sMInsRsrvEscrowCushion, lineNumber, 'sMInsRsrvEscrowCushion', conditions);
            }

            if (sMInsRsrvEscrowDisbursementSchedule.IncludeInFeeServiceOutput()) {
                out += writeLine(fieldInfoHash, JSON.stringify(sMInsRsrvEscrowDisbursementSchedule), lineNumber, 'sMInsRsrvEscrowDisbursementSchedule', conditions);
            }
            break;
        case '1004':
            var sProRealETxR = $selectedRow.find('input[data-fsid=sProRealETxR]').val();
            var sProRealETxT = $selectedRow.find('select[data-fsid=sProRealETxT]').children(':selected').text();
            var sProRealETxMb = $selectedRow.find('input[data-fsid=sProRealETxMb]').val();
            var sRealETxRsrvMon = $selectedRow.find('input[data-fsid=sRealETxRsrvMon]').val();

            var sRealETxRsrvProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sRealETxRsrvProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sRealETxRsrvProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sRealETxRsrvProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementRealETxRsrvProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sRealETxRsrvProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sRealETxRsrvProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sRealETxRsrvEscrowedTri = tf2yn($selectedRow.find('input[data-fsid=Escrow]').is(':checked'));
            var sRealETxRsrvEscrowCushion = $.trim($selectedRow.find('input.Cushion').val());
            var sRealETxRsrvEscrowDisbursementSchedule = getEscrowDisbursementSchedule($selectedRow);
            if ((sRealETxRsrvEscrowCushion && !validateEscrowCushion(sRealETxRsrvEscrowCushion)) || !sRealETxRsrvEscrowDisbursementSchedule.Validate()) {
                if (typeof errorCallback === 'function') {
                    errorCallback(new FeeServiceValidationError());
                }

                return;
            }

            if (!escrowOnly) {
                if (sProRealETxR)
                    out += writeLine(fieldInfoHash, sProRealETxR, lineNumber, 'sProRealETxR', conditions);

                out += writeLine(fieldInfoHash, sProRealETxT, lineNumber, 'sProRealETxT', conditions);

                if (sProRealETxMb)
                    out += writeLine(fieldInfoHash, sProRealETxMb, lineNumber, 'sProRealETxMb', conditions);

                if (sRealETxRsrvMon)
                    out += writeLine(fieldInfoHash, sRealETxRsrvMon, lineNumber, 'sRealETxRsrvMon', conditions);

                out += writeLine(fieldInfoHash, sRealETxRsrvProps_PdByT, lineNumber, 'sRealETxRsrvProps_PdByT', conditions);
                out += writeLine(fieldInfoHash, sRealETxRsrvProps_Apr, lineNumber, 'sRealETxRsrvProps_Apr', conditions);
                out += writeLine(fieldInfoHash, sRealETxRsrvProps_FhaAllow, lineNumber, 'sRealETxRsrvProps_FhaAllow', conditions);
                out += writeLine(fieldInfoHash, sRealETxRsrvProps_Poc, lineNumber, 'sRealETxRsrvProps_Poc', conditions);
                out += writeLine(fieldInfoHash, sSettlementRealETxRsrvProps_Dflp, lineNumber, 'sSettlementRealETxRsrvProps_Dflp', conditions);
                out += writeLine(fieldInfoHash, sRealETxRsrvProps_PaidToThirdParty, lineNumber, 'sRealETxRsrvProps_PaidToThirdParty', conditions);
                out += writeLine(fieldInfoHash, sRealETxRsrvProps_ThisPartyIsAffiliate, lineNumber, 'sRealETxRsrvProps_ThisPartyIsAffiliate', conditions);
            }
            
            out += writeLine(fieldInfoHash, sRealETxRsrvEscrowedTri, lineNumber, 'sRealETxRsrvEscrowedTri', conditions);

            if (sRealETxRsrvEscrowCushion) {
                out += writeLine(fieldInfoHash, sRealETxRsrvEscrowCushion, lineNumber, 'sRealETxRsrvEscrowCushion', conditions);
            }

            if (sRealETxRsrvEscrowDisbursementSchedule.IncludeInFeeServiceOutput()) {
                out += writeLine(fieldInfoHash, JSON.stringify(sRealETxRsrvEscrowDisbursementSchedule), lineNumber, 'sRealETxRsrvEscrowDisbursementSchedule', conditions);
            }
            break;
        case '1005':
            var sSchoolTxRsrvMon = $selectedRow.find('input[data-fsid=sSchoolTxRsrvMon]').val();
            var sProSchoolTx = $selectedRow.find('input[data-fsid=sProSchoolTx]').val();
            var sSchoolTxRsrvProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sSchoolTxRsrvProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sSchoolTxRsrvProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sSchoolTxRsrvProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementSchoolTxRsrvProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sSchoolTxRsrvProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sSchoolTxRsrvProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sSchoolTxRsrvEscrowedTri = tf2yn($selectedRow.find('input[data-fsid=Escrow]').is(':checked'));
            var sSchoolTxRsrvEscrowCushion = $.trim($selectedRow.find('input.Cushion').val());
            var sSchoolTxRsrvEscrowDisbursementSchedule = getEscrowDisbursementSchedule($selectedRow);
            if ((sSchoolTxRsrvEscrowCushion && !validateEscrowCushion(sSchoolTxRsrvEscrowCushion)) || !sSchoolTxRsrvEscrowDisbursementSchedule.Validate()) {
                if (typeof errorCallback === 'function') {
                    errorCallback(new FeeServiceValidationError());
                }

                return;
            }

            if (!escrowOnly) {
                if (sSchoolTxRsrvMon)
                    out += writeLine(fieldInfoHash, sSchoolTxRsrvMon, lineNumber, 'sSchoolTxRsrvMon', conditions);

                if (sProSchoolTx)
                    out += writeLine(fieldInfoHash, sProSchoolTx, lineNumber, 'sProSchoolTx', conditions);

                out += writeLine(fieldInfoHash, sSchoolTxRsrvProps_PdByT, lineNumber, 'sSchoolTxRsrvProps_PdByT', conditions);
                out += writeLine(fieldInfoHash, sSchoolTxRsrvProps_Apr, lineNumber, 'sSchoolTxRsrvProps_Apr', conditions);
                out += writeLine(fieldInfoHash, sSchoolTxRsrvProps_FhaAllow, lineNumber, 'sSchoolTxRsrvProps_FhaAllow', conditions);
                out += writeLine(fieldInfoHash, sSchoolTxRsrvProps_Poc, lineNumber, 'sSchoolTxRsrvProps_Poc', conditions);
                out += writeLine(fieldInfoHash, sSettlementSchoolTxRsrvProps_Dflp, lineNumber, 'sSettlementSchoolTxRsrvProps_Dflp', conditions);
                out += writeLine(fieldInfoHash, sSchoolTxRsrvProps_PaidToThirdParty, lineNumber, 'sSchoolTxRsrvProps_PaidToThirdParty', conditions);
                out += writeLine(fieldInfoHash, sSchoolTxRsrvProps_ThisPartyIsAffiliate, lineNumber, 'sSchoolTxRsrvProps_ThisPartyIsAffiliate', conditions);
            }

            out += writeLine(fieldInfoHash, sSchoolTxRsrvEscrowedTri, lineNumber, 'sSchoolTxRsrvEscrowedTri', conditions);

            if (sSchoolTxRsrvEscrowCushion) {
                out += writeLine(fieldInfoHash, sSchoolTxRsrvEscrowCushion, lineNumber, 'sSchoolTxRsrvEscrowCushion', conditions);
            }

            if (sSchoolTxRsrvEscrowDisbursementSchedule.IncludeInFeeServiceOutput()) {
                out += writeLine(fieldInfoHash, JSON.stringify(sSchoolTxRsrvEscrowDisbursementSchedule), lineNumber, 'sSchoolTxRsrvEscrowDisbursementSchedule', conditions);
            }
            break;
        case '1006':
            var sFloodInsRsrvMon = $selectedRow.find('input[data-fsid=sFloodInsRsrvMon]').val();
            var sProFloodIns = $selectedRow.find('input[data-fsid=sProFloodIns]').val();
            var sFloodInsRsrvProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sFloodInsRsrvProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sFloodInsRsrvProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sFloodInsRsrvProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementFloodInsRsrvProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sFloodInsRsrvProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sFloodInsRsrvProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sFloodInsRsrvEscrowedTri = tf2yn($selectedRow.find('input[data-fsid=Escrow]').is(':checked'));
            var sFloodInsRsrvEscrowCushion = $.trim($selectedRow.find('input.Cushion').val());
            var sFloodInsRsrvEscrowDisbursementSchedule = getEscrowDisbursementSchedule($selectedRow);
            if ((sFloodInsRsrvEscrowCushion && !validateEscrowCushion(sFloodInsRsrvEscrowCushion)) || !sFloodInsRsrvEscrowDisbursementSchedule.Validate()) {
                if (typeof errorCallback === 'function') {
                    errorCallback(new FeeServiceValidationError());
                }

                return;
            }

            if (!escrowOnly) {
                if (sFloodInsRsrvMon)
                    out += writeLine(fieldInfoHash, sFloodInsRsrvMon, lineNumber, 'sFloodInsRsrvMon', conditions);

                if (sProFloodIns)
                    out += writeLine(fieldInfoHash, sProFloodIns, lineNumber, 'sProFloodIns', conditions);

                out += writeLine(fieldInfoHash, sFloodInsRsrvProps_PdByT, lineNumber, 'sFloodInsRsrvProps_PdByT', conditions);
                out += writeLine(fieldInfoHash, sFloodInsRsrvProps_Apr, lineNumber, 'sFloodInsRsrvProps_Apr', conditions);
                out += writeLine(fieldInfoHash, sFloodInsRsrvProps_FhaAllow, lineNumber, 'sFloodInsRsrvProps_FhaAllow', conditions);
                out += writeLine(fieldInfoHash, sFloodInsRsrvProps_Poc, lineNumber, 'sFloodInsRsrvProps_Poc', conditions);
                out += writeLine(fieldInfoHash, sSettlementFloodInsRsrvProps_Dflp, lineNumber, 'sSettlementFloodInsRsrvProps_Dflp', conditions);
                out += writeLine(fieldInfoHash, sFloodInsRsrvProps_PaidToThirdParty, lineNumber, 'sFloodInsRsrvProps_PaidToThirdParty', conditions);
                out += writeLine(fieldInfoHash, sFloodInsRsrvProps_ThisPartyIsAffiliate, lineNumber, 'sFloodInsRsrvProps_ThisPartyIsAffiliate', conditions);
            }

            out += writeLine(fieldInfoHash, sFloodInsRsrvEscrowedTri, lineNumber, 'sFloodInsRsrvEscrowedTri', conditions);

            if (sFloodInsRsrvEscrowCushion) {
                out += writeLine(fieldInfoHash, sFloodInsRsrvEscrowCushion, lineNumber, 'sFloodInsRsrvEscrowCushion', conditions);
            }

            if (sFloodInsRsrvEscrowDisbursementSchedule.IncludeInFeeServiceOutput()) {
                out += writeLine(fieldInfoHash, JSON.stringify(sFloodInsRsrvEscrowDisbursementSchedule), lineNumber, 'sFloodInsRsrvEscrowDisbursementSchedule', conditions);
            }
            break;
        case '1007':
            var sAggregateAdjRsrvProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sAggregateAdjRsrvProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sAggregateAdjRsrvProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sAggregateAdjRsrvProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementAggregateAdjRsrvProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sAggregateAdjRsrvProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sAggregateAdjRsrvProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));

            out += writeLine(fieldInfoHash, sAggregateAdjRsrvProps_PdByT, lineNumber, 'sAggregateAdjRsrvProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sAggregateAdjRsrvProps_Apr, lineNumber, 'sAggregateAdjRsrvProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sAggregateAdjRsrvProps_FhaAllow, lineNumber, 'sAggregateAdjRsrvProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sAggregateAdjRsrvProps_Poc, lineNumber, 'sAggregateAdjRsrvProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementAggregateAdjRsrvProps_Dflp, lineNumber, 'sSettlementAggregateAdjRsrvProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sAggregateAdjRsrvProps_PaidToThirdParty, lineNumber, 'sAggregateAdjRsrvProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sAggregateAdjRsrvProps_ThisPartyIsAffiliate, lineNumber, 'sAggregateAdjRsrvProps_ThisPartyIsAffiliate', conditions);

            break;
        case '1008':
            var s1006ProHExpDesc = $selectedRow.find('input[data-fsid=s1006ProHExpDesc]').val();
            var s1006RsrvMon = $selectedRow.find('input[data-fsid=s1006RsrvMon]').val();
            var s1006ProHExp = $selectedRow.find('input[data-fsid=s1006ProHExp]').val();
            var s1006RsrvProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var s1006RsrvProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var s1006RsrvProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var s1006RsrvProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlement1008RsrvProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var s1006RsrvProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var s1006RsrvProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var s1006RsrvEscrowedTri = tf2yn($selectedRow.find('input[data-fsid=Escrow]').is(':checked'));
            var s1006RsrvEscrowCushion = $.trim($selectedRow.find('input.Cushion').val());
            var s1006RsrvEscrowDisbursementSchedule = getEscrowDisbursementSchedule($selectedRow);
            if ((s1006RsrvEscrowCushion && !validateEscrowCushion(s1006RsrvEscrowCushion)) || !s1006RsrvEscrowDisbursementSchedule.Validate()) {
                if (typeof errorCallback === 'function') {
                    errorCallback(new FeeServiceValidationError());
                }

                return;
            }

            if (!escrowOnly) {
                if (s1006ProHExpDesc)
                    out += writeLine(fieldInfoHash, s1006ProHExpDesc, lineNumber, 's1006ProHExpDesc', conditions);

                if (s1006RsrvMon)
                    out += writeLine(fieldInfoHash, s1006RsrvMon, lineNumber, 's1006RsrvMon', conditions);

                if (s1006ProHExp)
                    out += writeLine(fieldInfoHash, s1006ProHExp, lineNumber, 's1006ProHExp', conditions);

                out += writeLine(fieldInfoHash, s1006RsrvProps_PdByT, lineNumber, 's1006RsrvProps_PdByT', conditions);
                out += writeLine(fieldInfoHash, s1006RsrvProps_Apr, lineNumber, 's1006RsrvProps_Apr', conditions);
                out += writeLine(fieldInfoHash, s1006RsrvProps_FhaAllow, lineNumber, 's1006RsrvProps_FhaAllow', conditions);
                out += writeLine(fieldInfoHash, s1006RsrvProps_Poc, lineNumber, 's1006RsrvProps_Poc', conditions);
                out += writeLine(fieldInfoHash, sSettlement1008RsrvProps_Dflp, lineNumber, 'sSettlement1008RsrvProps_Dflp', conditions);
                out += writeLine(fieldInfoHash, s1006RsrvProps_PaidToThirdParty, lineNumber, 's1006RsrvProps_PaidToThirdParty', conditions);
                out += writeLine(fieldInfoHash, s1006RsrvProps_ThisPartyIsAffiliate, lineNumber, 's1006RsrvProps_ThisPartyIsAffiliate', conditions);
            }
            
            out += writeLine(fieldInfoHash, s1006RsrvEscrowedTri, lineNumber, 's1006RsrvEscrowedTri', conditions);

            if (s1006RsrvEscrowCushion) {
                out += writeLine(fieldInfoHash, s1006RsrvEscrowCushion, lineNumber, 's1006RsrvEscrowCushion', conditions);
            }

            if (s1006RsrvEscrowDisbursementSchedule.IncludeInFeeServiceOutput()) {
                out += writeLine(fieldInfoHash, JSON.stringify(s1006RsrvEscrowDisbursementSchedule), lineNumber, 's1006RsrvEscrowDisbursementSchedule', conditions);
            }
            break;
        case '1009':
            var s1007ProHExpDesc = $selectedRow.find('input[data-fsid=s1007ProHExpDesc]').val();
            var s1007RsrvMon = $selectedRow.find('input[data-fsid=s1007RsrvMon]').val();
            var s1007ProHExp = $selectedRow.find('input[data-fsid=s1007ProHExp]').val();
            var s1007RsrvProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var s1007RsrvProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var s1007RsrvProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var s1007RsrvProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlement1009RsrvProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var s1007RsrvProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var s1007RsrvProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var s1007RsrvEscrowedTri = tf2yn($selectedRow.find('input[data-fsid=Escrow]').is(':checked'));
            var s1007RsrvEscrowCushion = $.trim($selectedRow.find('input.Cushion').val());
            var s1007RsrvEscrowDisbursementSchedule = getEscrowDisbursementSchedule($selectedRow);
            if ((s1007RsrvEscrowCushion && !validateEscrowCushion(s1007RsrvEscrowCushion)) || !s1007RsrvEscrowDisbursementSchedule.Validate()) {
                if (typeof errorCallback === 'function') {
                    errorCallback(new FeeServiceValidationError());
                }

                return;
            }

            if (!escrowOnly) {
                if (s1007ProHExpDesc)
                    out += writeLine(fieldInfoHash, s1007ProHExpDesc, lineNumber, 's1007ProHExpDesc', conditions);

                if (s1007RsrvMon)
                    out += writeLine(fieldInfoHash, s1007RsrvMon, lineNumber, 's1007RsrvMon', conditions);

                if (s1007ProHExp)
                    out += writeLine(fieldInfoHash, s1007ProHExp, lineNumber, 's1007ProHExp', conditions);

                out += writeLine(fieldInfoHash, s1007RsrvProps_PdByT, lineNumber, 's1007RsrvProps_PdByT', conditions);
                out += writeLine(fieldInfoHash, s1007RsrvProps_Apr, lineNumber, 's1007RsrvProps_Apr', conditions);
                out += writeLine(fieldInfoHash, s1007RsrvProps_FhaAllow, lineNumber, 's1007RsrvProps_FhaAllow', conditions);
                out += writeLine(fieldInfoHash, s1007RsrvProps_Poc, lineNumber, 's1007RsrvProps_Poc', conditions);
                out += writeLine(fieldInfoHash, sSettlement1009RsrvProps_Dflp, lineNumber, 'sSettlement1009RsrvProps_Dflp', conditions);
                out += writeLine(fieldInfoHash, s1007RsrvProps_PaidToThirdParty, lineNumber, 's1007RsrvProps_PaidToThirdParty', conditions);
                out += writeLine(fieldInfoHash, s1007RsrvProps_ThisPartyIsAffiliate, lineNumber, 's1007RsrvProps_ThisPartyIsAffiliate', conditions);
            }

            out += writeLine(fieldInfoHash, s1007RsrvEscrowedTri, lineNumber, 's1007RsrvEscrowedTri', conditions);

            if (s1007RsrvEscrowCushion) {
                out += writeLine(fieldInfoHash, s1007RsrvEscrowCushion, lineNumber, 's1007RsrvEscrowCushion', conditions);
            }

            if (s1007RsrvEscrowDisbursementSchedule.IncludeInFeeServiceOutput()) {
                out += writeLine(fieldInfoHash, JSON.stringify(s1007RsrvEscrowDisbursementSchedule), lineNumber, 's1007RsrvEscrowDisbursementSchedule', conditions);
            }
            break;
        case '1010':
            var sU3RsrvDesc = $selectedRow.find('input[data-fsid=sU3RsrvDesc]').val();
            var sU3RsrvMon = $selectedRow.find('input[data-fsid=sU3RsrvMon]').val();
            var sProU3Rsrv = $selectedRow.find('input[data-fsid=sProU3Rsrv]').val();
            var sU3RsrvProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sU3RsrvProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sU3RsrvProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sU3RsrvProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementU3RsrvProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sU3RsrvProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sU3RsrvProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sU3RsrvEscrowedTri = tf2yn($selectedRow.find('input[data-fsid=Escrow]').is(':checked'));
            var sU3RsrvEscrowCushion = $.trim($selectedRow.find('input.Cushion').val());
            var sU3RsrvEscrowDisbursementSchedule = getEscrowDisbursementSchedule($selectedRow);
            if ((sU3RsrvEscrowCushion && !validateEscrowCushion(sU3RsrvEscrowCushion)) || !sU3RsrvEscrowDisbursementSchedule.Validate()) {
                if (typeof errorCallback === 'function') {
                    errorCallback(new FeeServiceValidationError());
                }

                return;
            }

            if (!escrowOnly) {
                if (sU3RsrvDesc)
                    out += writeLine(fieldInfoHash, sU3RsrvDesc, lineNumber, 'sU3RsrvDesc', conditions);

                if (sU3RsrvMon)
                    out += writeLine(fieldInfoHash, sU3RsrvMon, lineNumber, 'sU3RsrvMon', conditions);

                if (sProU3Rsrv)
                    out += writeLine(fieldInfoHash, sProU3Rsrv, lineNumber, 'sProU3Rsrv', conditions);

                out += writeLine(fieldInfoHash, sU3RsrvProps_PdByT, lineNumber, 'sU3RsrvProps_PdByT', conditions);
                out += writeLine(fieldInfoHash, sU3RsrvProps_Apr, lineNumber, 'sU3RsrvProps_Apr', conditions);
                out += writeLine(fieldInfoHash, sU3RsrvProps_FhaAllow, lineNumber, 'sU3RsrvProps_FhaAllow', conditions);
                out += writeLine(fieldInfoHash, sU3RsrvProps_Poc, lineNumber, 'sU3RsrvProps_Poc', conditions);
                out += writeLine(fieldInfoHash, sSettlementU3RsrvProps_Dflp, lineNumber, 'sSettlementU3RsrvProps_Dflp', conditions);
                out += writeLine(fieldInfoHash, sU3RsrvProps_PaidToThirdParty, lineNumber, 'sU3RsrvProps_PaidToThirdParty', conditions);
                out += writeLine(fieldInfoHash, sU3RsrvProps_ThisPartyIsAffiliate, lineNumber, 'sU3RsrvProps_ThisPartyIsAffiliate', conditions);
            }

            out += writeLine(fieldInfoHash, sU3RsrvEscrowedTri, lineNumber, 'sU3RsrvEscrowedTri', conditions);

            if (sU3RsrvEscrowCushion) {
                out += writeLine(fieldInfoHash, sU3RsrvEscrowCushion, lineNumber, 'sU3RsrvEscrowCushion', conditions);
            }

            if (sU3RsrvEscrowDisbursementSchedule.IncludeInFeeServiceOutput()) {
                out += writeLine(fieldInfoHash, JSON.stringify(sU3RsrvEscrowDisbursementSchedule), lineNumber, 'sU3RsrvEscrowDisbursementSchedule', conditions);
            }
            break;
        case '1011':
            var sU4RsrvDesc = $selectedRow.find('input[data-fsid=sU4RsrvDesc]').val();
            var sU4RsrvMon = $selectedRow.find('input[data-fsid=sU4RsrvMon]').val();
            var sProU4Rsrv = $selectedRow.find('input[data-fsid=sProU4Rsrv]').val();
            var sU4RsrvProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sU4RsrvProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sU4RsrvProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sU4RsrvProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementU4RsrvProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sU4RsrvProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sU4RsrvProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sU4RsrvEscrowedTri = tf2yn($selectedRow.find('input[data-fsid=Escrow]').is(':checked'));
            var sU4RsrvEscrowCushion = $.trim($selectedRow.find('input.Cushion').val());
            var sU4RsrvEscrowDisbursementSchedule = getEscrowDisbursementSchedule($selectedRow);
            if ((sU4RsrvEscrowCushion && !validateEscrowCushion(sU4RsrvEscrowCushion)) || !sU4RsrvEscrowDisbursementSchedule.Validate()) {
                if (typeof errorCallback === 'function') {
                    errorCallback(new FeeServiceValidationError());
                }

                return;
            }

            if (!escrowOnly) {
                if (sU4RsrvDesc)
                    out += writeLine(fieldInfoHash, sU4RsrvDesc, lineNumber, 'sU4RsrvDesc', conditions);

                if (sU4RsrvMon)
                    out += writeLine(fieldInfoHash, sU4RsrvMon, lineNumber, 'sU4RsrvMon', conditions);

                if (sProU4Rsrv)
                    out += writeLine(fieldInfoHash, sProU4Rsrv, lineNumber, 'sProU4Rsrv', conditions);

                out += writeLine(fieldInfoHash, sU4RsrvProps_PdByT, lineNumber, 'sU4RsrvProps_PdByT', conditions);
                out += writeLine(fieldInfoHash, sU4RsrvProps_Apr, lineNumber, 'sU4RsrvProps_Apr', conditions);
                out += writeLine(fieldInfoHash, sU4RsrvProps_FhaAllow, lineNumber, 'sU4RsrvProps_FhaAllow', conditions);
                out += writeLine(fieldInfoHash, sU4RsrvProps_Poc, lineNumber, 'sU4RsrvProps_Poc', conditions);
                out += writeLine(fieldInfoHash, sSettlementU4RsrvProps_Dflp, lineNumber, 'sSettlementU4RsrvProps_Dflp', conditions);
                out += writeLine(fieldInfoHash, sU4RsrvProps_PaidToThirdParty, lineNumber, 'sU4RsrvProps_PaidToThirdParty', conditions);
                out += writeLine(fieldInfoHash, sU4RsrvProps_ThisPartyIsAffiliate, lineNumber, 'sU4RsrvProps_ThisPartyIsAffiliate', conditions);
            }

            out += writeLine(fieldInfoHash, sU4RsrvEscrowedTri, lineNumber, 'sU4RsrvEscrowedTri', conditions);

            if (sU4RsrvEscrowCushion) {
                out += writeLine(fieldInfoHash, sU4RsrvEscrowCushion, lineNumber, 'sU4RsrvEscrowCushion', conditions);
            }

            if (sU4RsrvEscrowDisbursementSchedule.IncludeInFeeServiceOutput()) {
                out += writeLine(fieldInfoHash, JSON.stringify(sU4RsrvEscrowDisbursementSchedule), lineNumber, 'sU4RsrvEscrowDisbursementSchedule', conditions);
            }
            break;
        case '1102':
            var sEscrowFPc = $selectedRow.find('input[data-fsid=sEscrowFPc]').val();
            var sEscrowFBaseT = $selectedRow.find('select[data-fsid=sEscrowFBaseT]').children(':selected').text();
            var sEscrowFMb = $selectedRow.find('input[data-fsid=sEscrowFMb]').val();

            var sEscrowFGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var sEscrowFProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sEscrowFProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sEscrowFProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sEscrowFProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementEscrowFProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sEscrowFProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sEscrowFProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sEscrowFProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sEscrowFTable = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sEscrowFPc)
                out += writeLine(fieldInfoHash, sEscrowFPc, lineNumber, 'sEscrowFPc', conditions);

            out += writeLine(fieldInfoHash, sEscrowFBaseT, lineNumber, 'sEscrowFBaseT', conditions);

            if (sEscrowFMb)
                out += writeLine(fieldInfoHash, sEscrowFMb, lineNumber, 'sEscrowFMb', conditions);

            if (sEscrowFGfeSection)
                out += writeLine(fieldInfoHash, sEscrowFGfeSection, lineNumber, 'sEscrowFGfeSection', conditions);

            out += writeLine(fieldInfoHash, sEscrowFProps_PdByT, lineNumber, 'sEscrowFProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sEscrowFProps_Apr, lineNumber, 'sEscrowFProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sEscrowFProps_FhaAllow, lineNumber, 'sEscrowFProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sEscrowFProps_Poc, lineNumber, 'sEscrowFProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementEscrowFProps_Dflp, lineNumber, 'sSettlementEscrowFProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sEscrowFProps_PaidToThirdParty, lineNumber, 'sEscrowFProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sEscrowFProps_ThisPartyIsAffiliate, lineNumber, 'sEscrowFProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sEscrowFProps_ToBroker, lineNumber, 'sEscrowFProps_ToBroker', conditions);

            if (sEscrowFTable)
                out += writeLine(fieldInfoHash, sEscrowFTable, lineNumber, 'sEscrowFTable', conditions);

            break;
        case '1103':
            var sOwnerTitleInsFPc = $selectedRow.find('input[data-fsid=sOwnerTitleInsFPc]').val();
            var sOwnerTitleInsFBaseT = $selectedRow.find('select[data-fsid=sOwnerTitleInsFBaseT]').children(':selected').text();
            var sOwnerTitleInsFMb = $selectedRow.find('input[data-fsid=sOwnerTitleInsFMb]').val();

            var sOwnerTitleInsProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sOwnerTitleInsProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sOwnerTitleInsProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sOwnerTitleInsProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementOwnerTitleInsFProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sOwnerTitleInsProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sOwnerTitleInsProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sOwnerTitleInsProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sOwnerTitleInsPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sOwnerTitleInsFPc)
                out += writeLine(fieldInfoHash, sOwnerTitleInsFPc, lineNumber, 'sOwnerTitleInsFPc', conditions);

            out += writeLine(fieldInfoHash, sOwnerTitleInsFBaseT, lineNumber, 'sOwnerTitleInsFBaseT', conditions);

            if (sOwnerTitleInsFMb)
                out += writeLine(fieldInfoHash, sOwnerTitleInsFMb, lineNumber, 'sOwnerTitleInsFMb', conditions);

            out += writeLine(fieldInfoHash, sOwnerTitleInsProps_PdByT, lineNumber, 'sOwnerTitleInsProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sOwnerTitleInsProps_Apr, lineNumber, 'sOwnerTitleInsProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sOwnerTitleInsProps_FhaAllow, lineNumber, 'sOwnerTitleInsProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sOwnerTitleInsProps_Poc, lineNumber, 'sOwnerTitleInsProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementOwnerTitleInsFProps_Dflp, lineNumber, 'sSettlementOwnerTitleInsFProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sOwnerTitleInsProps_PaidToThirdParty, lineNumber, 'sOwnerTitleInsProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sOwnerTitleInsProps_ThisPartyIsAffiliate, lineNumber, 'sOwnerTitleInsProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sOwnerTitleInsProps_ToBroker, lineNumber, 'sOwnerTitleInsProps_ToBroker', conditions);

            if (sOwnerTitleInsPaidTo)
                out += writeLine(fieldInfoHash, sOwnerTitleInsPaidTo, lineNumber, 'sOwnerTitleInsPaidTo', conditions);

            break;
        case '1104':
            var sTitleInsFPc = $selectedRow.find('input[data-fsid=sTitleInsFPc]').val();
            var sTitleInsFBaseT = $selectedRow.find('select[data-fsid=sTitleInsFBaseT]').children(':selected').text();
            var sTitleInsFMb = $selectedRow.find('input[data-fsid=sTitleInsFMb]').val();

            //var sTitleInsFGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var sTitleInsFProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sTitleInsFProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sTitleInsFProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sTitleInsFProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementTitleInsFProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sTitleInsFProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sTitleInsFProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sTitleInsFProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sTitleInsFTable = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sTitleInsFPc)
                out += writeLine(fieldInfoHash, sTitleInsFPc, lineNumber, 'sTitleInsFPc', conditions);

            out += writeLine(fieldInfoHash, sTitleInsFBaseT, lineNumber, 'sTitleInsFBaseT', conditions);

            if (sTitleInsFMb)
                out += writeLine(fieldInfoHash, sTitleInsFMb, lineNumber, 'sTitleInsFMb', conditions);

            //if (sTitleInsFGfeSection)
            //    out += writeLine(fieldInfoHash, sTitleInsFGfeSection, lineNumber, 'sTitleInsFGfeSection', conditions);

            out += writeLine(fieldInfoHash, sTitleInsFProps_PdByT, lineNumber, 'sTitleInsFProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sTitleInsFProps_Apr, lineNumber, 'sTitleInsFProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sTitleInsFProps_FhaAllow, lineNumber, 'sTitleInsFProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sTitleInsFProps_Poc, lineNumber, 'sTitleInsFProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementTitleInsFProps_Dflp, lineNumber, 'sSettlementTitleInsFProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sTitleInsFProps_PaidToThirdParty, lineNumber, 'sTitleInsFProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sTitleInsFProps_ThisPartyIsAffiliate, lineNumber, 'sTitleInsFProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sTitleInsFProps_ToBroker, lineNumber, 'sTitleInsFProps_ToBroker', conditions);

            if (sTitleInsFTable)
                out += writeLine(fieldInfoHash, sTitleInsFTable, lineNumber, 'sTitleInsFTable', conditions);

            break;
        case '1109':
            var sDocPrepF = $selectedRow.find('input[data-fsid=sDocPrepF]').val();

            var sDocPrepFGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var sDocPrepFProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sDocPrepFProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sDocPrepFProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sDocPrepFProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementDocPrepFProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sDocPrepFProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sDocPrepFProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sDocPrepFProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sDocPrepFPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sDocPrepF)
                out += writeLine(fieldInfoHash, sDocPrepF, lineNumber, 'sDocPrepF', conditions);

            if (sDocPrepFGfeSection)
                out += writeLine(fieldInfoHash, sDocPrepFGfeSection, lineNumber, 'sDocPrepFGfeSection', conditions);

            out += writeLine(fieldInfoHash, sDocPrepFProps_PdByT, lineNumber, 'sDocPrepFProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sDocPrepFProps_Apr, lineNumber, 'sDocPrepFProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sDocPrepFProps_FhaAllow, lineNumber, 'sDocPrepFProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sDocPrepFProps_Poc, lineNumber, 'sDocPrepFProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementDocPrepFProps_Dflp, lineNumber, 'sSettlementDocPrepFProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sDocPrepFProps_PaidToThirdParty, lineNumber, 'sDocPrepFProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sDocPrepFProps_ThisPartyIsAffiliate, lineNumber, 'sDocPrepFProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sDocPrepFProps_ToBroker, lineNumber, 'sDocPrepFProps_ToBroker', conditions);

            if (sDocPrepFPaidTo)
                out += writeLine(fieldInfoHash, sDocPrepFPaidTo, lineNumber, 'sDocPrepFPaidTo', conditions);

            break;
        case '1110':
            var sNotaryF = $selectedRow.find('input[data-fsid=sNotaryF]').val();

            var sNotaryFGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var sNotaryFProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sNotaryFProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sNotaryFProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sNotaryFProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementNotaryFProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sNotaryFProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sNotaryFProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sNotaryFProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sNotaryFPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sNotaryF)
                out += writeLine(fieldInfoHash, sNotaryF, lineNumber, 'sNotaryF', conditions);

            if (sNotaryFGfeSection)
                out += writeLine(fieldInfoHash, sNotaryFGfeSection, lineNumber, 'sNotaryFGfeSection', conditions);

            out += writeLine(fieldInfoHash, sNotaryFProps_PdByT, lineNumber, 'sNotaryFProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sNotaryFProps_Apr, lineNumber, 'sNotaryFProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sNotaryFProps_FhaAllow, lineNumber, 'sNotaryFProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sNotaryFProps_Poc, lineNumber, 'sNotaryFProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementNotaryFProps_Dflp, lineNumber, 'sSettlementNotaryFProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sNotaryFProps_PaidToThirdParty, lineNumber, 'sNotaryFProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sNotaryFProps_ThisPartyIsAffiliate, lineNumber, 'sNotaryFProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sNotaryFProps_ToBroker, lineNumber, 'sNotaryFProps_ToBroker', conditions);

            if (sNotaryFPaidTo)
                out += writeLine(fieldInfoHash, sNotaryFPaidTo, lineNumber, 'sNotaryFPaidTo', conditions);

            break;
        case '1111':
            var sAttorneyF = $selectedRow.find('input[data-fsid=sAttorneyF]').val();

            var sAttorneyFGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var sAttorneyFProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sAttorneyFProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sAttorneyFProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sAttorneyFProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementAttorneyFProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sAttorneyFProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sAttorneyFProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sAttorneyFProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sAttorneyFPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sAttorneyF)
                out += writeLine(fieldInfoHash, sAttorneyF, lineNumber, 'sAttorneyF', conditions);

            if (sAttorneyFGfeSection)
                out += writeLine(fieldInfoHash, sAttorneyFGfeSection, lineNumber, 'sAttorneyFGfeSection', conditions);

            out += writeLine(fieldInfoHash, sAttorneyFProps_PdByT, lineNumber, 'sAttorneyFProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sAttorneyFProps_Apr, lineNumber, 'sAttorneyFProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sAttorneyFProps_FhaAllow, lineNumber, 'sAttorneyFProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sAttorneyFProps_Poc, lineNumber, 'sAttorneyFProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementAttorneyFProps_Dflp, lineNumber, 'sSettlementAttorneyFProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sAttorneyFProps_PaidToThirdParty, lineNumber, 'sAttorneyFProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sAttorneyFProps_ThisPartyIsAffiliate, lineNumber, 'sAttorneyFProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sAttorneyFProps_ToBroker, lineNumber, 'sAttorneyFProps_ToBroker', conditions);

            if (sAttorneyFPaidTo)
                out += writeLine(fieldInfoHash, sAttorneyFPaidTo, lineNumber, 'sAttorneyFPaidTo', conditions);

            break;
        case '1112':
            var sU1TcDesc = $selectedRow.find('input[data-fsid=sU1TcDesc]').val();
            var sU1Tc = $selectedRow.find('input[data-fsid=sU1Tc]').val();

            var sU1TcGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var sU1TcProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sU1TcProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sU1TcProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sU1TcProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementU1TcProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sU1TcProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sU1TcProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sU1TcProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sU1TcPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sU1TcDesc)
                out += writeLine(fieldInfoHash, sU1TcDesc, lineNumber, 'sU1TcDesc', conditions);

            if (sU1Tc)
                out += writeLine(fieldInfoHash, sU1Tc, lineNumber, 'sU1Tc', conditions);

            if (sU1TcGfeSection)
                out += writeLine(fieldInfoHash, sU1TcGfeSection, lineNumber, 'sU1TcGfeSection', conditions);

            out += writeLine(fieldInfoHash, sU1TcProps_PdByT, lineNumber, 'sU1TcProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sU1TcProps_Apr, lineNumber, 'sU1TcProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sU1TcProps_FhaAllow, lineNumber, 'sU1TcProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sU1TcProps_Poc, lineNumber, 'sU1TcProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementU1TcProps_Dflp, lineNumber, 'sSettlementU1TcProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sU1TcProps_PaidToThirdParty, lineNumber, 'sU1TcProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sU1TcProps_ThisPartyIsAffiliate, lineNumber, 'sU1TcProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sU1TcProps_ToBroker, lineNumber, 'sU1TcProps_ToBroker', conditions);

            if (sU1TcPaidTo)
                out += writeLine(fieldInfoHash, sU1TcPaidTo, lineNumber, 'sU1TcPaidTo', conditions);

            break;
        case '1113':
            var sU2TcDesc = $selectedRow.find('input[data-fsid=sU2TcDesc]').val();
            var sU2Tc = $selectedRow.find('input[data-fsid=sU2Tc]').val();

            var sU2TcGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var sU2TcProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sU2TcProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sU2TcProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sU2TcProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementU2TcProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sU2TcProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sU2TcProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sU2TcProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sU2TcPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sU2TcDesc)
                out += writeLine(fieldInfoHash, sU2TcDesc, lineNumber, 'sU2TcDesc', conditions);

            if (sU2Tc)
                out += writeLine(fieldInfoHash, sU2Tc, lineNumber, 'sU2Tc', conditions);

            if (sU2TcGfeSection)
                out += writeLine(fieldInfoHash, sU2TcGfeSection, lineNumber, 'sU2TcGfeSection', conditions);

            out += writeLine(fieldInfoHash, sU2TcProps_PdByT, lineNumber, 'sU2TcProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sU2TcProps_Apr, lineNumber, 'sU2TcProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sU2TcProps_FhaAllow, lineNumber, 'sU2TcProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sU2TcProps_Poc, lineNumber, 'sU2TcProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementU2TcProps_Dflp, lineNumber, 'sSettlementU2TcProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sU2TcProps_PaidToThirdParty, lineNumber, 'sU2TcProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sU2TcProps_ThisPartyIsAffiliate, lineNumber, 'sU2TcProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sU2TcProps_ToBroker, lineNumber, 'sU2TcProps_ToBroker', conditions);

            if (sU2TcPaidTo)
                out += writeLine(fieldInfoHash, sU2TcPaidTo, lineNumber, 'sU2TcPaidTo', conditions);

            break;
        case '1114':
            var sU3TcDesc = $selectedRow.find('input[data-fsid=sU3TcDesc]').val();
            var sU3Tc = $selectedRow.find('input[data-fsid=sU3Tc]').val();

            var sU3TcGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var sU3TcProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sU3TcProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sU3TcProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sU3TcProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementU3TcProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sU3TcProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sU3TcProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sU3TcProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sU3TcPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sU3TcDesc)
                out += writeLine(fieldInfoHash, sU3TcDesc, lineNumber, 'sU3TcDesc', conditions);

            if (sU3Tc)
                out += writeLine(fieldInfoHash, sU3Tc, lineNumber, 'sU3Tc', conditions);

            if (sU3TcGfeSection)
                out += writeLine(fieldInfoHash, sU3TcGfeSection, lineNumber, 'sU3TcGfeSection', conditions);

            out += writeLine(fieldInfoHash, sU3TcProps_PdByT, lineNumber, 'sU3TcProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sU3TcProps_Apr, lineNumber, 'sU3TcProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sU3TcProps_FhaAllow, lineNumber, 'sU3TcProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sU3TcProps_Poc, lineNumber, 'sU3TcProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementU3TcProps_Dflp, lineNumber, 'sSettlementU3TcProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sU3TcProps_PaidToThirdParty, lineNumber, 'sU3TcProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sU3TcProps_ThisPartyIsAffiliate, lineNumber, 'sU3TcProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sU3TcProps_ToBroker, lineNumber, 'sU3TcProps_ToBroker', conditions);

            if (sU3TcPaidTo)
                out += writeLine(fieldInfoHash, sU3TcPaidTo, lineNumber, 'sU3TcPaidTo', conditions);

            break;
        case '1115':
            var sU4TcDesc = $selectedRow.find('input[data-fsid=sU4TcDesc]').val();
            var sU4Tc = $selectedRow.find('input[data-fsid=sU4Tc]').val();

            var sU4TcGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var sU4TcProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sU4TcProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sU4TcProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sU4TcProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementU4TcProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sU4TcProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sU4TcProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sU4TcProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sU4TcPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sU4TcDesc)
                out += writeLine(fieldInfoHash, sU4TcDesc, lineNumber, 'sU4TcDesc', conditions);

            if (sU4Tc)
                out += writeLine(fieldInfoHash, sU4Tc, lineNumber, 'sU4Tc', conditions);

            if (sU4TcGfeSection)
                out += writeLine(fieldInfoHash, sU4TcGfeSection, lineNumber, 'sU4TcGfeSection', conditions);

            out += writeLine(fieldInfoHash, sU4TcProps_PdByT, lineNumber, 'sU4TcProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sU4TcProps_Apr, lineNumber, 'sU4TcProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sU4TcProps_FhaAllow, lineNumber, 'sU4TcProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sU4TcProps_Poc, lineNumber, 'sU4TcProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementU4TcProps_Dflp, lineNumber, 'sSettlementU4TcProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sU4TcProps_PaidToThirdParty, lineNumber, 'sU4TcProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sU4TcProps_ThisPartyIsAffiliate, lineNumber, 'sU4TcProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sU4TcProps_ToBroker, lineNumber, 'sU4TcProps_ToBroker', conditions);

            if (sU4TcPaidTo)
                out += writeLine(fieldInfoHash, sU4TcPaidTo, lineNumber, 'sU4TcPaidTo', conditions);
            break;
        case '1201':
            var sRecFPc = $selectedRow.find('input[data-fsid=sRecFPc]').val();
            var sRecBaseT = $selectedRow.find('select[data-fsid=sRecBaseT]').children(':selected').text();
            var sRecFMb = $selectedRow.find('input[data-fsid=sRecFMb]').val();

            var sRecFLckd = tf2yn($selectedRow.find('input[data-fsid=sRecFLckd]').prop('checked'));
            var sRecDeed = $selectedRow.find('input[data-fsid=sRecDeed]').val();
            var sRecMortgage = $selectedRow.find('input[data-fsid=sRecMortgage]').val();
            var sRecRelease = $selectedRow.find('input[data-fsid=sRecRelease]').val();

            var sRecFProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sRecFProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sRecFProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sRecFProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementRecFProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sRecFProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sRecFProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sRecFDesc = $selectedRow.find('input[data-fsid=PaidTo]').val();

            out += writeLine(fieldInfoHash, sRecFLckd, lineNumber, 'sRecFLckd', conditions);

            if (sRecFLckd == "Yes") {
                if (sRecFPc)
                    out += writeLine(fieldInfoHash, sRecFPc, lineNumber, 'sRecFPc', conditions);

                out += writeLine(fieldInfoHash, sRecBaseT, lineNumber, 'sRecBaseT', conditions);

                if (sRecFMb)
                    out += writeLine(fieldInfoHash, sRecFMb, lineNumber, 'sRecFMb', conditions);
            }
            
            if (sRecDeed)
                out += writeLine(fieldInfoHash, sRecDeed, lineNumber, 'sRecDeed', conditions);

            if (sRecMortgage)
                out += writeLine(fieldInfoHash, sRecMortgage, lineNumber, 'sRecMortgage', conditions);

            if (sRecRelease)
                out += writeLine(fieldInfoHash, sRecRelease, lineNumber, 'sRecRelease', conditions);

            out += writeLine(fieldInfoHash, sRecFProps_PdByT, lineNumber, 'sRecFProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sRecFProps_Apr, lineNumber, 'sRecFProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sRecFProps_FhaAllow, lineNumber, 'sRecFProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sRecFProps_Poc, lineNumber, 'sRecFProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementRecFProps_Dflp, lineNumber, 'sSettlementRecFProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sRecFProps_PaidToThirdParty, lineNumber, 'sRecFProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sRecFProps_ThisPartyIsAffiliate, lineNumber, 'sRecFProps_ThisPartyIsAffiliate', conditions);

            if (sRecFDesc)
                out += writeLine(fieldInfoHash, sRecFDesc, lineNumber, 'sRecFDesc', conditions);

            break;
        case '1204':
            var sCountyRtcPc = $selectedRow.find('input[data-fsid=sCountyRtcPc]').val();
            var sCountyRtcBaseT = $selectedRow.find('select[data-fsid=sCountyRtcBaseT]').children(':selected').text();
            var sCountyRtcMb = $selectedRow.find('input[data-fsid=sCountyRtcMb]').val();

            var sCountyRtcProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sCountyRtcProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sCountyRtcProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sCountyRtcProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementCountyRtcProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sCountyRtcProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sCountyRtcProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sCountyRtcDesc = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sCountyRtcPc)
                out += writeLine(fieldInfoHash, sCountyRtcPc, lineNumber, 'sCountyRtcPc', conditions);

            out += writeLine(fieldInfoHash, sCountyRtcBaseT, lineNumber, 'sCountyRtcBaseT', conditions);

            if (sCountyRtcMb)
                out += writeLine(fieldInfoHash, sCountyRtcMb, lineNumber, 'sCountyRtcMb', conditions);

            out += writeLine(fieldInfoHash, sCountyRtcProps_PdByT, lineNumber, 'sCountyRtcProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sCountyRtcProps_Apr, lineNumber, 'sCountyRtcProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sCountyRtcProps_FhaAllow, lineNumber, 'sCountyRtcProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sCountyRtcProps_Poc, lineNumber, 'sCountyRtcProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementCountyRtcProps_Dflp, lineNumber, 'sSettlementCountyRtcProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sCountyRtcProps_PaidToThirdParty, lineNumber, 'sCountyRtcProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sCountyRtcProps_ThisPartyIsAffiliate, lineNumber, 'sCountyRtcProps_ThisPartyIsAffiliate', conditions);

            if (sCountyRtcDesc)
                out += writeLine(fieldInfoHash, sCountyRtcDesc, lineNumber, 'sCountyRtcDesc', conditions);

            break;
        case '1205':
            var sStateRtcPc = $selectedRow.find('input[data-fsid=sStateRtcPc]').val();
            var sStateRtcBaseT = $selectedRow.find('select[data-fsid=sStateRtcBaseT]').children(':selected').text();
            var sStateRtcMb = $selectedRow.find('input[data-fsid=sStateRtcMb]').val();

            var sStateRtcProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sStateRtcProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sStateRtcProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sStateRtcProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementStateRtcProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sStateRtcProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sStateRtcProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sStateRtcDesc = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sStateRtcPc)
                out += writeLine(fieldInfoHash, sStateRtcPc, lineNumber, 'sStateRtcPc', conditions);

            out += writeLine(fieldInfoHash, sStateRtcBaseT, lineNumber, 'sStateRtcBaseT', conditions);

            if (sStateRtcMb)
                out += writeLine(fieldInfoHash, sStateRtcMb, lineNumber, 'sStateRtcMb', conditions);

            out += writeLine(fieldInfoHash, sStateRtcProps_PdByT, lineNumber, 'sStateRtcProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sStateRtcProps_Apr, lineNumber, 'sStateRtcProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sStateRtcProps_FhaAllow, lineNumber, 'sStateRtcProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sStateRtcProps_Poc, lineNumber, 'sStateRtcProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementStateRtcProps_Dflp, lineNumber, 'sSettlementStateRtcProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sStateRtcProps_PaidToThirdParty, lineNumber, 'sStateRtcProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sStateRtcProps_ThisPartyIsAffiliate, lineNumber, 'sStateRtcProps_ThisPartyIsAffiliate', conditions);

            if (sStateRtcDesc)
                out += writeLine(fieldInfoHash, sStateRtcDesc, lineNumber, 'sStateRtcDesc', conditions);

            break;
        case '1206':
            var sU1GovRtcDesc = $selectedRow.find('input[data-fsid=sU1GovRtcDesc]').val();
            var sU1GovRtcPc = $selectedRow.find('input[data-fsid=sU1GovRtcPc]').val();
            var sU1GovRtcBaseT = $selectedRow.find('select[data-fsid=sU1GovRtcBaseT]').children(':selected').text();
            var sU1GovRtcMb = $selectedRow.find('input[data-fsid=sU1GovRtcMb]').val();

            var sU1GovRtcGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var sU1GovRtcProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sU1GovRtcProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sU1GovRtcProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sU1GovRtcProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementU1GovRtcProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sU1GovRtcProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sU1GovRtcProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sU1GovRtcPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sU1GovRtcDesc)
                out += writeLine(fieldInfoHash, sU1GovRtcDesc, lineNumber, 'sU1GovRtcDesc', conditions);

            if (sU1GovRtcPc)
                out += writeLine(fieldInfoHash, sU1GovRtcPc, lineNumber, 'sU1GovRtcPc', conditions);

            out += writeLine(fieldInfoHash, sU1GovRtcBaseT, lineNumber, 'sU1GovRtcBaseT', conditions);

            if (sU1GovRtcMb)
                out += writeLine(fieldInfoHash, sU1GovRtcMb, lineNumber, 'sU1GovRtcMb', conditions);

            if (sU1GovRtcGfeSection)
                out += writeLine(fieldInfoHash, sU1GovRtcGfeSection, lineNumber, 'sU1GovRtcGfeSection', conditions);

            out += writeLine(fieldInfoHash, sU1GovRtcProps_PdByT, lineNumber, 'sU1GovRtcProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sU1GovRtcProps_Apr, lineNumber, 'sU1GovRtcProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sU1GovRtcProps_FhaAllow, lineNumber, 'sU1GovRtcProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sU1GovRtcProps_Poc, lineNumber, 'sU1GovRtcProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementU1GovRtcProps_Dflp, lineNumber, 'sSettlementU1GovRtcProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sU1GovRtcProps_PaidToThirdParty, lineNumber, 'sU1GovRtcProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sU1GovRtcProps_ThisPartyIsAffiliate, lineNumber, 'sU1GovRtcProps_ThisPartyIsAffiliate', conditions);

            if (sU1GovRtcPaidTo)
                out += writeLine(fieldInfoHash, sU1GovRtcPaidTo, lineNumber, 'sU1GovRtcPaidTo', conditions);

            break;
        case '1207':
            var sU2GovRtcDesc = $selectedRow.find('input[data-fsid=sU2GovRtcDesc]').val();
            var sU2GovRtcPc = $selectedRow.find('input[data-fsid=sU2GovRtcPc]').val();
            var sU2GovRtcBaseT = $selectedRow.find('select[data-fsid=sU2GovRtcBaseT]').children(':selected').text();
            var sU2GovRtcMb = $selectedRow.find('input[data-fsid=sU2GovRtcMb]').val();

            var sU2GovRtcGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var sU2GovRtcProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sU2GovRtcProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sU2GovRtcProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sU2GovRtcProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementU2GovRtcProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sU2GovRtcProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sU2GovRtcProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sU2GovRtcPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sU2GovRtcDesc)
                out += writeLine(fieldInfoHash, sU2GovRtcDesc, lineNumber, 'sU2GovRtcDesc', conditions);

            if (sU2GovRtcPc)
                out += writeLine(fieldInfoHash, sU2GovRtcPc, lineNumber, 'sU2GovRtcPc', conditions);

            out += writeLine(fieldInfoHash, sU2GovRtcBaseT, lineNumber, 'sU2GovRtcBaseT', conditions);

            if (sU2GovRtcMb)
                out += writeLine(fieldInfoHash, sU2GovRtcMb, lineNumber, 'sU2GovRtcMb', conditions);

            if (sU2GovRtcGfeSection)
                out += writeLine(fieldInfoHash, sU2GovRtcGfeSection, lineNumber, 'sU2GovRtcGfeSection', conditions);

            out += writeLine(fieldInfoHash, sU2GovRtcProps_PdByT, lineNumber, 'sU2GovRtcProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sU2GovRtcProps_Apr, lineNumber, 'sU2GovRtcProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sU2GovRtcProps_FhaAllow, lineNumber, 'sU2GovRtcProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sU2GovRtcProps_Poc, lineNumber, 'sU2GovRtcProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementU2GovRtcProps_Dflp, lineNumber, 'sSettlementU2GovRtcProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sU2GovRtcProps_PaidToThirdParty, lineNumber, 'sU2GovRtcProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sU2GovRtcProps_ThisPartyIsAffiliate, lineNumber, 'sU2GovRtcProps_ThisPartyIsAffiliate', conditions);

            if (sU2GovRtcPaidTo)
                out += writeLine(fieldInfoHash, sU2GovRtcPaidTo, lineNumber, 'sU2GovRtcPaidTo', conditions);

            break;
        case '1208':
            var sU3GovRtcDesc = $selectedRow.find('input[data-fsid=sU3GovRtcDesc]').val();
            var sU3GovRtcPc = $selectedRow.find('input[data-fsid=sU3GovRtcPc]').val();
            var sU3GovRtcBaseT = $selectedRow.find('select[data-fsid=sU3GovRtcBaseT]').children(':selected').text();
            var sU3GovRtcMb = $selectedRow.find('input[data-fsid=sU3GovRtcMb]').val();

            var sU3GovRtcGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var sU3GovRtcProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sU3GovRtcProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sU3GovRtcProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sU3GovRtcProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementU3GovRtcProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sU3GovRtcProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sU3GovRtcProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sU3GovRtcPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sU3GovRtcDesc)
                out += writeLine(fieldInfoHash, sU3GovRtcDesc, lineNumber, 'sU3GovRtcDesc', conditions);

            if (sU3GovRtcPc)
                out += writeLine(fieldInfoHash, sU3GovRtcPc, lineNumber, 'sU3GovRtcPc', conditions);

            out += writeLine(fieldInfoHash, sU3GovRtcBaseT, lineNumber, 'sU3GovRtcBaseT', conditions);

            if (sU3GovRtcMb)
                out += writeLine(fieldInfoHash, sU3GovRtcMb, lineNumber, 'sU3GovRtcMb', conditions);

            if (sU3GovRtcGfeSection)
                out += writeLine(fieldInfoHash, sU3GovRtcGfeSection, lineNumber, 'sU3GovRtcGfeSection', conditions);                

            out += writeLine(fieldInfoHash, sU3GovRtcProps_PdByT, lineNumber, 'sU3GovRtcProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sU3GovRtcProps_Apr, lineNumber, 'sU3GovRtcProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sU3GovRtcProps_FhaAllow, lineNumber, 'sU3GovRtcProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sU3GovRtcProps_Poc, lineNumber, 'sU3GovRtcProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementU3GovRtcProps_Dflp, lineNumber, 'sSettlementU3GovRtcProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sU3GovRtcProps_PaidToThirdParty, lineNumber, 'sU3GovRtcProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sU3GovRtcProps_ThisPartyIsAffiliate, lineNumber, 'sU3GovRtcProps_ThisPartyIsAffiliate', conditions);

            if (sU3GovRtcPaidTo)
                out += writeLine(fieldInfoHash, sU3GovRtcPaidTo, lineNumber, 'sU3GovRtcPaidTo', conditions);

            break;
        case '1302':
            var sPestInspectF = $selectedRow.find('input[data-fsid=sPestInspectF]').val();

            var sPestInspectFProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sPestInspectFProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sPestInspectFProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sPestInspectFProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementPestInspectFProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sPestInspectFProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sPestInspectFProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sPestInspectFProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sPestInspectPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sPestInspectF)
                out += writeLine(fieldInfoHash, sPestInspectF, lineNumber, 'sPestInspectF', conditions);

            out += writeLine(fieldInfoHash, sPestInspectFProps_PdByT, lineNumber, 'sPestInspectFProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sPestInspectFProps_Apr, lineNumber, 'sPestInspectFProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sPestInspectFProps_FhaAllow, lineNumber, 'sPestInspectFProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sPestInspectFProps_Poc, lineNumber, 'sPestInspectFProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementPestInspectFProps_Dflp, lineNumber, 'sSettlementPestInspectFProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sPestInspectFProps_PaidToThirdParty, lineNumber, 'sPestInspectFProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sPestInspectFProps_ThisPartyIsAffiliate, lineNumber, 'sPestInspectFProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sPestInspectFProps_ToBroker, lineNumber, 'sPestInspectFProps_ToBroker', conditions);

            if (sPestInspectPaidTo)
                out += writeLine(fieldInfoHash, sPestInspectPaidTo, lineNumber, 'sPestInspectPaidTo', conditions);

            break;
        case '1303':
            var sU1ScDesc = $selectedRow.find('input[data-fsid=sU1ScDesc]').val();
            var sU1Sc = $selectedRow.find('input[data-fsid=sU1Sc]').val();

            var sU1ScGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var sU1ScProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sU1ScProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sU1ScProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sU1ScProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementU1ScProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sU1ScProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sU1ScProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sU1ScProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sU1ScPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sU1ScDesc)
                out += writeLine(fieldInfoHash, sU1ScDesc, lineNumber, 'sU1ScDesc', conditions);

            if (sU1Sc)
                out += writeLine(fieldInfoHash, sU1Sc, lineNumber, 'sU1Sc', conditions);

            if (sU1ScGfeSection)
                out += writeLine(fieldInfoHash, sU1ScGfeSection, lineNumber, 'sU1ScGfeSection', conditions);

            out += writeLine(fieldInfoHash, sU1ScProps_PdByT, lineNumber, 'sU1ScProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sU1ScProps_Apr, lineNumber, 'sU1ScProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sU1ScProps_FhaAllow, lineNumber, 'sU1ScProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sU1ScProps_Poc, lineNumber, 'sU1ScProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementU1ScProps_Dflp, lineNumber, 'sSettlementU1ScProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sU1ScProps_PaidToThirdParty, lineNumber, 'sU1ScProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sU1ScProps_ThisPartyIsAffiliate, lineNumber, 'sU1ScProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sU1ScProps_ToBroker, lineNumber, 'sU1ScProps_ToBroker', conditions);

            if (sU1ScPaidTo)
                out += writeLine(fieldInfoHash, sU1ScPaidTo, lineNumber, 'sU1ScPaidTo', conditions);

            break;
        case '1304':
            var sU2ScDesc = $selectedRow.find('input[data-fsid=sU2ScDesc]').val();
            var sU2Sc = $selectedRow.find('input[data-fsid=sU2Sc]').val();

            var sU2ScGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var sU2ScProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sU2ScProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sU2ScProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sU2ScProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementU2ScProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sU2ScProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sU2ScProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sU2ScProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sU2ScPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sU2ScDesc)
                out += writeLine(fieldInfoHash, sU2ScDesc, lineNumber, 'sU2ScDesc', conditions);

            if (sU2Sc)
                out += writeLine(fieldInfoHash, sU2Sc, lineNumber, 'sU2Sc', conditions);

            if (sU2ScGfeSection)
                out += writeLine(fieldInfoHash, sU2ScGfeSection, lineNumber, 'sU2ScGfeSection', conditions);

            out += writeLine(fieldInfoHash, sU2ScProps_PdByT, lineNumber, 'sU2ScProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sU2ScProps_Apr, lineNumber, 'sU2ScProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sU2ScProps_FhaAllow, lineNumber, 'sU2ScProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sU2ScProps_Poc, lineNumber, 'sU2ScProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementU2ScProps_Dflp, lineNumber, 'sSettlementU2ScProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sU2ScProps_PaidToThirdParty, lineNumber, 'sU2ScProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sU2ScProps_ThisPartyIsAffiliate, lineNumber, 'sU2ScProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sU2ScProps_ToBroker, lineNumber, 'sU2ScProps_ToBroker', conditions);

            if (sU2ScPaidTo)
                out += writeLine(fieldInfoHash, sU2ScPaidTo, lineNumber, 'sU2ScPaidTo', conditions);

            break;
        case '1305':
            var sU3ScDesc = $selectedRow.find('input[data-fsid=sU3ScDesc]').val();
            var sU3Sc = $selectedRow.find('input[data-fsid=sU3Sc]').val();

            var sU3ScGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var sU3ScProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sU3ScProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sU3ScProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sU3ScProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementU3ScProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sU3ScProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sU3ScProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sU3ScProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sU3ScPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sU3ScDesc)
                out += writeLine(fieldInfoHash, sU3ScDesc, lineNumber, 'sU3ScDesc', conditions);

            if (sU3Sc)
                out += writeLine(fieldInfoHash, sU3Sc, lineNumber, 'sU3Sc', conditions);

            if (sU3ScGfeSection)
                out += writeLine(fieldInfoHash, sU3ScGfeSection, lineNumber, 'sU3ScGfeSection', conditions);

            out += writeLine(fieldInfoHash, sU3ScProps_PdByT, lineNumber, 'sU3ScProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sU3ScProps_Apr, lineNumber, 'sU3ScProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sU3ScProps_FhaAllow, lineNumber, 'sU3ScProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sU3ScProps_Poc, lineNumber, 'sU3ScProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementU3ScProps_Dflp, lineNumber, 'sSettlementU3ScProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sU3ScProps_PaidToThirdParty, lineNumber, 'sU3ScProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sU3ScProps_ThisPartyIsAffiliate, lineNumber, 'sU3ScProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sU3ScProps_ToBroker, lineNumber, 'sU3ScProps_ToBroker', conditions);

            if (sU3ScPaidTo)
                out += writeLine(fieldInfoHash, sU3ScPaidTo, lineNumber, 'sU3ScPaidTo', conditions);

            break;
        case '1306':
            var sU4ScDesc = $selectedRow.find('input[data-fsid=sU4ScDesc]').val();
            var sU4Sc = $selectedRow.find('input[data-fsid=sU4Sc]').val();

            var sU4ScGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var sU4ScProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sU4ScProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sU4ScProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sU4ScProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementU4ScProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sU4ScProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sU4ScProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sU4ScProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sU4ScPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sU4ScDesc)
                out += writeLine(fieldInfoHash, sU4ScDesc, lineNumber, 'sU4ScDesc', conditions);

            if (sU4Sc)
                out += writeLine(fieldInfoHash, sU4Sc, lineNumber, 'sU4Sc', conditions);

            if (sU4ScGfeSection)
                out += writeLine(fieldInfoHash, sU4ScGfeSection, lineNumber, 'sU4ScGfeSection', conditions);

            out += writeLine(fieldInfoHash, sU4ScProps_PdByT, lineNumber, 'sU4ScProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sU4ScProps_Apr, lineNumber, 'sU4ScProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sU4ScProps_FhaAllow, lineNumber, 'sU4ScProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sU4ScProps_Poc, lineNumber, 'sU4ScProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementU4ScProps_Dflp, lineNumber, 'sSettlementU4ScProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sU4ScProps_PaidToThirdParty, lineNumber, 'sU4ScProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sU4ScProps_ThisPartyIsAffiliate, lineNumber, 'sU4ScProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sU4ScProps_ToBroker, lineNumber, 'sU4ScProps_ToBroker', conditions);

            if (sU4ScPaidTo)
                out += writeLine(fieldInfoHash, sU4ScPaidTo, lineNumber, 'sU4ScPaidTo', conditions);

            break;
        case '1307':
            var sU5ScDesc = $selectedRow.find('input[data-fsid=sU5ScDesc]').val();
            var sU5Sc = $selectedRow.find('input[data-fsid=sU5Sc]').val();

            var sU5ScGfeSection = $selectedRow.find('input[data-fsid=Page2]:checked').next().text();

            var sU5ScProps_PdByT = $selectedRow.find('select[data-fsid=PdByT]').children(':selected').text();
            var sU5ScProps_Apr = tf2yn($selectedRow.find('input[data-fsid=Apr]').prop('checked'));
            var sU5ScProps_FhaAllow = tf2yn($selectedRow.find('input[data-fsid=Fha]').prop('checked'));
            var sU5ScProps_Poc = tf2yn($selectedRow.find('input[data-fsid=Poc]').prop('checked'));
            var sSettlementU5ScProps_Dflp = tf2yn($selectedRow.find('input[data-fsid=Dflp]').prop('checked'));
            var sU5ScProps_PaidToThirdParty = tf2yn($selectedRow.find('input[data-fsid=TrdPty]').prop('checked'));
            var sU5ScProps_ThisPartyIsAffiliate = tf2yn($selectedRow.find('input[data-fsid=Aff]').prop('checked'));
            var sU5ScProps_ToBroker = tf2yn($selectedRow.find('input[data-fsid=ToBrok]').prop('checked'));
            var sU5ScPaidTo = $selectedRow.find('input[data-fsid=PaidTo]').val();

            if (sU5ScDesc)
                out += writeLine(fieldInfoHash, sU5ScDesc, lineNumber, 'sU5ScDesc', conditions);

            if (sU5Sc)
                out += writeLine(fieldInfoHash, sU5Sc, lineNumber, 'sU5Sc', conditions);

            if (sU5ScGfeSection)
                out += writeLine(fieldInfoHash, sU5ScGfeSection, lineNumber, 'sU5ScGfeSection', conditions);

            out += writeLine(fieldInfoHash, sU5ScProps_PdByT, lineNumber, 'sU5ScProps_PdByT', conditions);
            out += writeLine(fieldInfoHash, sU5ScProps_Apr, lineNumber, 'sU5ScProps_Apr', conditions);
            out += writeLine(fieldInfoHash, sU5ScProps_FhaAllow, lineNumber, 'sU5ScProps_FhaAllow', conditions);
            out += writeLine(fieldInfoHash, sU5ScProps_Poc, lineNumber, 'sU5ScProps_Poc', conditions);
            out += writeLine(fieldInfoHash, sSettlementU5ScProps_Dflp, lineNumber, 'sSettlementU5ScProps_Dflp', conditions);
            out += writeLine(fieldInfoHash, sU5ScProps_PaidToThirdParty, lineNumber, 'sU5ScProps_PaidToThirdParty', conditions);
            out += writeLine(fieldInfoHash, sU5ScProps_ThisPartyIsAffiliate, lineNumber, 'sU5ScProps_ThisPartyIsAffiliate', conditions);
            out += writeLine(fieldInfoHash, sU5ScProps_ToBroker, lineNumber, 'sU5ScProps_ToBroker', conditions);

            if (sU5ScPaidTo)
                out += writeLine(fieldInfoHash, sU5ScPaidTo, lineNumber, 'sU5ScPaidTo', conditions);

            break;
    }

    return out;
}

function FeeServiceValidationError(message) {
    this.name = 'FeeServiceValidationError';
    this.message = message || 'Please enter valid escrow information. The reserve months cushion must be blank, 0, 1, or 2. If any of the disbursement months are non-blank, all disbursement month fields must be non-blank and the sum of the disbursements must be 12.';
}
FeeServiceValidationError.prototype = new Error();
FeeServiceValidationError.prototype.constructor = FeeServiceValidationError;

function validateEscrowCushion(cushion) {
    var parsedCushion = parseInt(cushion, 10);

    return cushion && parsedCushion >= 0 && parsedCushion <= 2;
}

function getEscrowDisbursementSchedule($rows) {
    return {
        Jan: $rows.find('input.Jan').val(),
        Feb: $rows.find('input.Feb').val(),
        Mar: $rows.find('input.Mar').val(),
        Apr: $rows.find('input.Apr').val(),
        May: $rows.find('input.May').val(),
        Jun: $rows.find('input.Jun').val(),
        Jul: $rows.find('input.Jul').val(),
        Aug: $rows.find('input.Aug').val(),
        Sep: $rows.find('input.Sep').val(),
        Oct: $rows.find('input.Oct').val(),
        Nov: $rows.find('input.Nov').val(),
        Dec: $rows.find('input.Dec').val(),
        Validate: function() {
            if (!this.IncludeInFeeServiceOutput()) {
                return true;
            }

            var numDisbursementsForMonth, totalDisbursements = 0;

            for (var property in this) {
                if (this.hasOwnProperty(property) && typeof this[property] === 'string') {
                    numDisbursementsForMonth = parseInt(this[property], 10);
                    if (!isFinite(numDisbursementsForMonth)) {
                        return false;
                    }

                    totalDisbursements += numDisbursementsForMonth;
                }
            }

            return totalDisbursements === 12;
        },
        IncludeInFeeServiceOutput: function() {
            for (var property in this) {
                if (this.hasOwnProperty(property) && typeof this[property] === 'string') {
                    if (this[property] !== '') {
                        return true;
                    }
                }
            }

            return false;
        }
    };
}

function GetConditions() {
    if ($("#ApplyAlways").prop('checked'))
        return '';

    var conditions = ''
    var $selectedItems = $('#SelectedFields ul li');
    $selectedItems.each(function(index) {
        var newCondition = '';

        // Get Field ID
        var fieldId = $(this).children('a.pick').data('id');

        // Get selected values
        var $divValues = $('div[data-id=' + fieldId + ']');
        var friendlyName = $divValues.data('rep')
        var dataType = $divValues.data('type');
        switch (dataType) {
            case 'Enumeration':
            case 'BrokerDefinedEnumeration':
            case 'NestedEnumeration':
                var isNested = dataType == 'NestedEnumeration';
                var $valuesList = $divValues.find('.listBox');

                // Get selected values from values list
                var values = '';
                var $selectedValues;
                if (isNested) {
                    $selectedValues = $divValues.find('div.sub-section[data-id=' + $valuesList.val() + "]").find('option:selected');
                } else {
                    $selectedValues = $valuesList.find('option:selected');
                }

                if ($selectedValues.length > 0) {
                    $selectedValues.each(function(index2) {
                        values += sanitizeForCsv($(this).text());
                        if (index2 != $selectedValues.length - 1)
                            values += ',';
                    });

                    newCondition += '(' + friendlyName + ' = ' + (isNested ? sanitizeForCsv($valuesList.find('option:selected').text()) + ": " : '') + values + ')';
                }

                break;
            case 'Range':
                var lowerBound = $divValues.find('input').first().val();
                var upperBound = $divValues.find('input').last().val();

                if (lowerBound && upperBound) {
                    newCondition += '(' + lowerBound + ' <= ' + friendlyName + ' <= ' + upperBound + ')'
                }

                break;
        }

        if (newCondition) {
            conditions += newCondition;

            if (index != $selectedItems.length - 1)
                conditions += ' and ';
        }
    });

    return conditions;
}

function sanitizeForCsv(strVal) {
    var sanitizedString = strVal;
    
    if (sanitizedString.indexOf('"') != -1) {
        sanitizedString = strVal.replace(/"/g, "\"\"");
    }
    
    if (sanitizedString.indexOf(",") != -1
            || sanitizedString.indexOf("\"") != -1
            || sanitizedString.indexOf("\n") != -1) {
        sanitizedString = "\"" + sanitizedString + "\"";
    }

    // OPM 226337 - Replace non-breaking spaces with regular space.
    if (sanitizedString.indexOf('\u00a0') != -1) {
        sanitizedString = sanitizedString.replace(/\u00a0/g, " ");
    }

    return sanitizedString;
}
