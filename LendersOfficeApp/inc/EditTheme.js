﻿function getServiceCallArgs() {
    return {
        name: $("#ThemeName").val(),
        themeId: ML.ThemeId,
        variables: JSON.stringify($('input.variable').map(function (index, el) { return { Key: el.id, Value: el.value }; }).get())
    };
}

function onPreview() {
    var args = getServiceCallArgs();

    gService.main.callAsyncSimple('SaveThemeToCacheForPreview', args, onPreviewCallback);    
}

function onPreviewCallback(result) {
    if (result.error) {
        alert(result.UserMessage);
        return;
    }

    $('#QuickPreviewFrame').prop('src', ML.VirtualRoot + '/los/BrokerAdmin/TpoColorTheme/PreviewTheme.aspx?quickpreview=1&themeid=' + ML.ThemeId + '&cacheid=' + result.value.CacheId);

    $('#EditorDiv').hide();
    $('#QuickPreviewDiv').show();
}

function onCloseQuickPreview() {
    $('#QuickPreviewFrame').prop('src', '');

    $('#EditorDiv').show();
    $('#QuickPreviewDiv').hide();
}

function onSave() {
    var args = getServiceCallArgs();

    gService.main.callAsyncSimple('SaveTheme', args, onSaveCallback);
}

function onSaveCallback(result) {
    if (result.error) {
        alert(result.UserMessage);
    }
    else {
        onClose(true);
    }
}

function onClose(shouldReturn) {
    if (isLqbPopup(window)) {
        if (shouldReturn) {
            window.parent.LQBPopup.Return({ Name: $("#ThemeName").val() });
        }
        else {
            window.parent.LQBPopup.Hide();
        }
    }
    else {
        window.close();
    }
}

$(document).ready(function () {
    $("#ThemeName").val(ML.ThemeName)
    $('#PreviewThemeName').text(ML.ThemeName);

    $('#PreviewBtn').click(onPreview);
    $('#SaveBtn').click(onSave);
    $('#CancelBtn').click(function () { onClose(false); });

    $('#QuickPreviewDiv').hide();

    $(".explicit-save-button").prop('disabled', !ML.isSaveButtonEnabled);

    $('input.picker').spectrum({ preferredFormat: "hex" }).on('change.spectrum', function (event, color) {
        $(event.target).parent().find('input[type="text"]').val(color.toHexString());
    });

    $.each(variables, function (index, variable) {
        var input = $('#' + variable.Id);
        input.val(variable.Value);

        input.change(function () {
            var input = $(this);
            input.parent().find('input.picker').spectrum('set', input.val());
        });

        input.change();
    });

    $('#ThemeName').focus();
})
