﻿if (!this.LOCKPOLICIES) {
    this.LOCKPOLICIES = ({});

    LOCKPOLICIES.fn = new Object();
    LOCKPOLICIES.DataSource = [];
    LOCKPOLICIES.fn.DataBind = function() {
        //Clear any existing rows
        var oTbody = document.getElementById("m_LockPoliciesTable");
        var nRowCount = oTbody.rows.length;
        for (var i = 0; i < nRowCount; i++) {
            oTbody.deleteRow(0);
        }

        //render every element in DataSource
        var tBody = document.getElementById("m_LockPoliciesTable");
        for (var i = 0; i < LOCKPOLICIES.DataSource.length; i++) {
            var policy = LOCKPOLICIES.DataSource[i];
            LOCKPOLICIES.fn.RenderLockPolicyRow(tBody, policy[0], policy[1], policy[2], policy[3], policy[4], policy[5], policy[6], policy[7]);
        }
    }

    LOCKPOLICIES.fn.RenderLockPolicyRow = function(oParent, policyid, name, hours, extensions, floatdowns, relocks, showHolidays, isUsingRateSheetExpirationFeature) {
        var oRow = LOCKPOLICIES.fn.__CreateAndAppend("tr", oParent);
        var oCell = null;
        var o;
        oRow.className = ((oParent.rows.length % 2) == 0) ? "GridAlternatingItem" : "GridItem";

        //edit link, duplicate link
        oCell = LOCKPOLICIES.fn.__CreateAndAppend("td", oRow);
        o = LOCKPOLICIES.fn.__CreateAndAppend("a", oCell);
        o.id = "edit_" + policyid;
        o.href = "javascript:void(0);";
        o.innerText = "edit";
        addEvent(o, 'click', function() {
            return f_editPolicy(policyid);
        });
        o = LOCKPOLICIES.fn.__CreateAndAppend("br", oCell);
        o = LOCKPOLICIES.fn.__CreateAndAppend("a", oCell);
        o.id = "dup_" + policyid;
        o.href = "javascript:void(0);";
        o.innerText = "duplicate";
        addEvent(o, 'click', function() {
            return f_duplicatePolicy(policyid);
        });

        //lock policy name
        oCell = LOCKPOLICIES.fn.__CreateAndAppend("td", oRow);
        oCell.innerHTML = name;

        //lock desk hours
        oCell = LOCKPOLICIES.fn.__CreateAndAppend("td", oRow);
        oCell.innerHTML = hours;
        if (hours == "Disabled") {
            o = LOCKPOLICIES.fn.__CreateAndAppend("br", oCell);
        }
        if (showHolidays == 'true') {
            o = LOCKPOLICIES.fn.__CreateAndAppend("a", oCell);
            o.id = "holiday_" + policyid;
            o.href = "javascript:void(0);";
            o.innerText = "View Upcoming Holiday Closures";
            addEvent(o, 'click', function() {
                return f_viewHolidays(policyid);
            });
            o = LOCKPOLICIES.fn.__CreateAndAppend("br", oCell);
        }
        if (isUsingRateSheetExpirationFeature == 'true') {
            o = LOCKPOLICIES.fn.__CreateAndAppend("a", oCell);
            o.id = "disablePricing_" + policyid;
            o.href = "javascript:void(0);";
            o.innerText = "Disable Pricing";
            o.className = "DisablePricingLink";
            addEvent(o, 'click', function() {
                return f_onDisablePricingClick(policyid);
            });
        }

        //lock extensions
        oCell = LOCKPOLICIES.fn.__CreateAndAppend("td", oRow);
        oCell.innerHTML = extensions;

        //float downs
        oCell = LOCKPOLICIES.fn.__CreateAndAppend("td", oRow);
        oCell.innerHTML = floatdowns;

        //re-locks
        oCell = LOCKPOLICIES.fn.__CreateAndAppend("td", oRow);
        oCell.innerHTML = relocks;

        //delete link
        oCell = LOCKPOLICIES.fn.__CreateAndAppend("td", oRow);
        if (name != "Default") {
            o = LOCKPOLICIES.fn.__CreateAndAppend("a", oCell);
            o.id = "delete_" + policyid;
            o.href = "javascript:void(0);";
            o.innerText = "delete";
            addEvent(o, 'click', function() {
                return f_deletePolicy(policyid, name);
            });
        }
        return oRow;
    }

    LOCKPOLICIES.fn.__CreateAndAppend = function(sTag, oParent) {
        var oItem = document.createElement(sTag);
        oParent.appendChild(oItem);
        return oItem;
    }


    LOCKPOLICIES.fn.ReloadTable = function() {
        var jsonData = document.getElementById("LockPolicies_LockPolicyList").value;
        LOCKPOLICIES.DataSource = JSON.parse(jsonData);
        LOCKPOLICIES.fn.DataBind();
    }
}

function addEvent(obj, evType, fn, useCapture) {
        addEventHandler(obj, evType, fn, useCapture);
        return true;
}