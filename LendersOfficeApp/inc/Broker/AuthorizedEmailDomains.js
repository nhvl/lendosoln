﻿function Domain(name) {
    this.Name = name;
}

function getScope(selector) {
    var appElement = $(selector);
    return angular.element(appElement).scope();
}

function LoadDomains() {
    var $scope = getScope("#AuthorizedEmailDomainsDiv");
    $scope.loadDomains();
}

$(document).ready(function () {
    initializeAngular();
});

function _init() {
    LoadDomains();
}

function initializeAngular() {
    var module = angular.module('AuthorizedEmailDomains', []);

    module.controller("AuthorizedEmailDomainsController", function AuthorizedEmailDomainsController($scope) {
        $scope.domains = [];

        $scope.loadDomains = function () {
            var args = {
                BrokerId: ML.BrokerId
            };

            gService.main.callAsyncSimple("LoadDomains", args, $scope.loadDomainsCallback);
        }

        $scope.loadDomainsCallback = function (result) {
            if (result.error) {
                alert("An unexpected error occurred while attempting to load authorized email domain data:\n" + result.UserMessage);
                window.close();
            }

            $scope.domains = $.parseJSON(result.value.Domains);
            $scope.$apply();
        }

        $scope.addDomain = function (event) {
            var name = $("#AddDomainText").val().trim();
            
            // Don't save if trimmed name is blank.
            if (!name) {
                // Clear textbox (it may contain whitespace).
                $("#AddDomainText").val("");
                return;
            }

            // Check for duplicates.
            for (var i = 0; i < $scope.domains.length; i++) {
                if ($scope.domains[i].Name == name) {
                    alert("\"" + name + "\"" + "is already in the list of authorized domain names.");
                    return;
                }
            }

            // Call save service method.
            var args = {
                BrokerId: ML.BrokerId,
                DomainName: name
            };

            gService.main.callAsyncSimple("SaveDomain", args, function (result) { $scope.addDomainCallback(result, event); });
        }

        $scope.addDomainCallback = function (result, event) {
            if (result.error) {
                alert("An unexpected error occured while attempting to add a new authorized email domain:\n" + result.UserMessage);
                return;
            }

            // Refresh view.
            $scope.domains = $.parseJSON(result.value.Domains);
            $scope.$apply();

            // Clear textbox.
            $("#AddDomainText").val("");

            if (typeof event !== 'undefined') {
                event.preventDefault();
            }
        }

        $scope.addOnEnter = function (event) {
            if (event.which === 13 /* enter */) {
                $scope.addDomain(event);
            }
        }

        $scope.removeDomain = function (index) {
            var args = {
                BrokerId: ML.BrokerId,
                DomainName: $scope.domains[index].Name
            };

            gService.main.callAsyncSimple("DeleteDomain", args, $scope.removeDomainCallback);
        }

        $scope.removeDomainCallback = function (result) {
            if (result.error) {
                alert("An unexpected error occured while attempting to remove an authorized email domain:\n" + result.UserMessage);
                return;
            }

            // Refresh view.
            $scope.domains = $.parseJSON(result.value.Domains);
            $scope.$apply();
        }

        $scope.closeDomain = function () {
            window.close();
        }
    });
}
