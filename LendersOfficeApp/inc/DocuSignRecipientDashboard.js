﻿jQuery(function ($) {
    var module = angular.module("DocuSignRecipientDashboard", []);
    module.controller("RecipientTableController", function RecipientTableController($scope) {
        $scope.Recipients = Recipients;
        $scope.EnvelopeName = ML.EnvelopeName;

        $scope.StatusDetails = {};
        $scope.ViewStatusDetails = function (recipient) {
            $scope.StatusDetails = {
                ParentName: recipient.Name,
                StatusDates: recipient.StatusDates,
                HasNotes: recipient.StatusDatesHasNotes
            };

            setTimeout(function () {
                LQBPopup.ShowElement($('#StatusDetails'), {
                    width: 500,
                    height: 300,
                    elementClasses: 'FullWidthHeight',
                    hideCloseButton: true
                });
            }, 100);
        }

        $scope.AuthResultDetails = {};
        $scope.ViewRecipientAuthResults = function (recipient) {
            $scope.AuthResultDetails = {
                RecipientName: recipient.Name,
                HasFailureNotes: recipient.HasFailureNotes,
                AuthResults: recipient.AuthResults,
                InitialMfaOption_rep: recipient.InitialMfaOption_rep,
                AccessCode: recipient.AccessCode,
                ProvidedPhoneNumber: recipient.ProvidedPhoneNumber,
                IsInitialMfaOptionAccessCode: recipient.IsInitialMfaOptionAccessCode,
                IsInitialMfaOptionPhone: recipient.IsInitialMfaOptionPhone
            };

            setTimeout(function () {
                LQBPopup.ShowElement($('#AuthResultDetails'), {
                    width: 500,
                    height: 300,
                    elementClasses: 'FullWidthHeight',
                    hideCloseButton: true
                });
            }, 100);
        }
    });

    module.directive('statusDetailsPopup', function () {
        return {
            restrict: 'A',
            templateUrl: 'Common/StatusDetailsPopup.html',
            scope: {
                StatusDetails: '<statusDetails'
            }
        };
    });

    module.directive('authDetailsPopup', function () {
        return {
            restrict: 'A',
            templateUrl: 'Common/AuthResultDetailsPopup.html',
            scope: {
                AuthResultDetails: '<authDetails'
            }
        };
    });
});