﻿jQuery(function($) {
    if ('console' in window && 'clear' in console) console.clear();

    var $rightCol = $('#MasterPageRightCol');
    var $leftCol = $('#MasterPageLeftCol');
    var $header = $('#MasterPageTopHeader');

    var rightCol = $rightCol[0];
    var leftCol = $leftCol[0];
    var header = $header[0];

    var colStackBorder = 700;
    var headerStackBorder = 935;


    //Keep track of window resizes so we know when to scroll vertically or stack the menu on top of content
    var heightPadding = 30;
    var topHeaderHeight = $header.height();

    var widthPadding = $leftCol.width();

    var sizeX = 0;
    var sizeY = 0;

    $(window).on('resize', function() {
        //Order of resizeWidth vs resizeHeight matters, since resizeHeight checks if we're in a narrow display mode.
        var newSizeX = $(window).width();
        if (newSizeX != sizeX) {
            sizeX = newSizeX;
            resizeWidth();
        }

        var newSizeY = $(window).height();
        if (newSizeY != sizeY) {
            sizeY = newSizeY
            resizeHeight();
        }
    });

    function resizeHeight() {
        var stackPadding = 0;
        if ($rightCol.hasClass("Stack")) stackPadding = 15;

        var maxHeight = sizeY - topHeaderHeight - heightPadding - stackPadding;
        $rightCol.css({ 'max-height': maxHeight + "px" });
        if ($rightCol.height() >= maxHeight) {
            $rightCol.addClass('OverflowY');
        }
        else {
            $rightCol.removeClass('OverflowY');
        }
    }

    function resizeWidth() {
        var $toStack = $([rightCol, leftCol]);
        var stacked = $rightCol.hasClass('Stack');
        if (sizeX < colStackBorder && !stacked) {
            $toStack.addClass('Stack');
        }
        else if (sizeX > colStackBorder && stacked) {
            $toStack.removeClass('Stack');
        }

        var headerStacked = $header.hasClass('Stack');
        if (sizeX < headerStackBorder && !headerStacked) {
            $header.addClass('Stack');
            topHeaderHeight = $header.height();
            heightPadding = 60;
        }
        else if (sizeX > headerStackBorder && headerStacked) {
            $header.removeClass('Stack');
            topHeaderHeight = $header.height();
            heightPadding = 30;
        }

    }

    $(window).resize();

    $('#MasterPageLeftCol ul li').on('click', function() {
        $('#MasterPageLeftCol ul li').removeClass('loading');
        $(this).addClass('loading');
        var url = $(this).children('.url').val();
        window.location = url;
    });
});