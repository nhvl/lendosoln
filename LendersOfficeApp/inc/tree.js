  function __expandAndScrollVisible(node) {

    var p = node.parentElement;
    while (p != null && p.tagName== 'DIV') {
      if (p.nodetype == 'childspan') {
        __expandCollapseNode(p.previousSibling, false);
      }
      p = p.parentElement;
    }
    // 3/2/2004 dd - Only scroll to view if node is out of range.
    if (node.offsetTop < document.body.scrollTop || node.offsetTop > document.body.clientHeight + document.body.scrollTop)
      document.body.scrollTop = node.offsetTop - 20;
  }
  // b = true --> Collapse, b = false --> Expand, b = null -> Toggle state
  function __expandCollapseNode(node, b) {
      if (!node) {
          return;
      }

      var nextSibling = node.nextSibling;
      while (nextSibling.nodeType !== 1) {
          nextSibling = nextSibling.nextSibling;
      }
    var bIsOpen = b == null ? nextSibling.style.display == "" : b;

    nextSibling.style.display= bIsOpen ? "none" : "";  
    var o = node.lastChild.previousSibling;
    if (null != o) {
      o.src = o.src.replace(/folder.*\.gif$/, bIsOpen ? "folder.gif" : "folderopen.gif");
      o = o.previousSibling;
      if (null != o) {
        o.src = o.src.replace(/.\.gif$/, bIsOpen ? "p.gif" : "m.gif");
      }
    
    }  
  }  
