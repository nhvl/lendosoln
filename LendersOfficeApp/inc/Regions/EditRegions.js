﻿var regionSets;
var stateAndCountyInfo;

function RegionSet(regionSetId) {
    this.Id = regionSetId;
    this.BrokerId = ML.BrokerId;
    this.Name = "";
    this.Description = "";
    this.Regions = [];
    this.isDirty = true; // new set so we know it's dirty.
    this.isDeleted = false; // marks set as deleted.
}

function Region(RegionSetId) {
    this.Id = -1;
    this.RegionSetId = RegionSetId;
    this.BrokerId = ML.BrokerId;
    this.Name = "";
    this.Description = "";
    this.FipsCountyCodes = [];
}

function getById(array, id) {
    for (var i = 0; i < array.length; i++) {
        if (array[i].Id == id) {
            return array[i];
        }
    }

    return null;
}

function getScope(selector) {
    var appElement = $(selector);
    return angular.element(appElement).scope();
}

function LoadRegionSets(callback) {
    gService.main.callAsyncSimple("LoadRegionSets", null, function (result) {
        if (result.error) {
            alert("An unexpected error occurred while attempting to load region data:\n" + result.UserMessage);
            window.close();
        }

        regionSets = $.parseJSON(result.value.RegionSets)

        // Reinitialize region list
        var $regionListScope = getScope("#RegionList");
        $regionListScope.init(true);

        if (typeof (callback) == 'function') {
            callback();
        }
    });
}

function LoadStateAndCountyInfo() {
    gService.main.callAsyncSimple("GetStatesAndCounties", null, function (result) {
        if (result.error) {
            alert("An unexpected error occured while attempting to load state and county info:\n" + result.UserMessage);
            window.close();
        }

        stateAndCountyInfo = $.parseJSON(result.value.StatesCountiesFipsMap);
    });
}

function openEditRegion(regionSetId, regionId) {
    var $editRegionScope = getScope("#dialog-EditRegion");
    $editRegionScope.init(regionSetId, regionId);

    $("#dialog-EditRegion").dialog({
        modal: true,

        closeOnEscape: false,
        width: "500",
        height: "400",
        draggable: false,
        dialogClass: "LQBDialogBox",
        resizable: false
    });
}

$(function () {
    initializeAngular();
});

function _init() {
    LoadRegionSets(LoadStateAndCountyInfo);
}

function initializeAngular() {
    var regionEditor = angular.module('regionEditor', []);

    regionEditor.directive('unique', function () {
        return {
            require: 'ngModel',

            scope: {
                unique: "=",
                uniqueWithin: "=",
                uniqueThis: "=",
            },

            link: function (scope, elem, attr, ctrl) {
                ctrl.$parsers.unshift(isUnique);     //For DOM -> model validation
                ctrl.$formatters.unshift(isUnique);  //For model -> DOM validation

                function isUnique(value) {
                    var isValid = true;

                    // Invalid if string is empty.
                    if (!value) {
                        ctrl.$setValidity('unique', false);
                        return value;
                    }

                    // Trim string.
                    strTrim = value.replace(/^\s+|\s+$/g, '')

                    // Invalid if trimmed string is empty.
                    if (strTrim.length == 0) {
                        ctrl.$setValidity('unique', false);
                        return value;
                    }

                    // Invalid if string is not unique.
                    for (var i = 0; i < scope.uniqueWithin.length; i++) {
                        var current = scope.uniqueWithin[i];

                        if (current.Id == scope.uniqueThis.Id) {
                            continue;
                        }

                        var strComp = current[scope.unique];

                        if (!strComp) {
                            continue;
                        }

                        // Invalid if trimmed strings are equal.
                        if (strTrim.toUpperCase() === strComp.replace(/^\s+|\s+$/g, '').toUpperCase()) {
                            ctrl.$setValidity('unique', false);
                            return value;
                        }
                    }

                    ctrl.$setValidity('unique', true);
                    return value;
                }
            }
        };
    });

    regionEditor.controller("ManageRegionSetsController", function ManageRegionSetsController($scope, $filter) {
        $scope.regionSets;
        $scope.newRegionSetIdCounter;

        $scope.markDirty = function (id) {
            getById($scope.regionSets, id).isDirty = true;
        }

        $scope.addItem = function () {
            $scope.regionSets.push(new RegionSet($scope.newRegionSetIdCounter));
            $scope.newRegionSetIdCounter--;
        };

        $scope.removeItem = function (id) {
            if (id >= 0) {
                // Block delete if set is used by Fee Service.
                var args = {
                    BrokerId: ML.BrokerId,
                    RegionSetId: id
                };

                gService.main.callAsyncSimple("CheckFeeServiceForRegionSetId", args, function (result) {
                    if (!result.error && result.value.FoundInFeeService == "True") {
                        alert("This region set is used by your fee service file. Please remove it from your active fee service file before removing it from the region setup.");
                        return;
                    }

                    $scope.removeItemImpl(id);
                    $scope.$apply();
                });
            } else {
                $scope.removeItemImpl(id);
            }
        }

        $scope.removeItemImpl = function (id) {
            // Confirm Deletion.
            if (!confirm("Are you sure you wish to delete this region set?\nAll associated regions will be deleted with it.")) {
                return;
            }

            // Mark region set for deletion.
            var deleted = getById($scope.regionSets, id);
            deleted.isDirty = true;
            deleted.isDeleted = true;
        }

        $scope.saveItems = function (callback) {
            var updatedSets = [];
            var deletedSets = [];

            for (var i = 0; i < $scope.regionSets.length; i++) {
                currentSet = $scope.regionSets[i];
                if (currentSet.isDeleted && currentSet.Id >= 0) {
                    deletedSets.push(currentSet);
                } else if (!currentSet.isDeleted && currentSet.isDirty) {
                    updatedSets.push(currentSet);
                }
            }

            var args = {
                BrokerId: ML.BrokerId,
                UpdatedSets: JSON.stringify(updatedSets),
                DeletedSets: JSON.stringify(deletedSets),
            };

            gService.main.callAsyncSimple("SaveRegionSets", args, function (result) {
                if (result.error) {
                    alert("Failed to save Region Sets:\n" + result.UserMessage);
                    return;
                }

                if (typeof (callback) == 'function') {
                    callback();
                }
            });
        }

        $scope.init = function () {
            $scope.regionSets = angular.copy(regionSets);   // Should already be sorted by name.

            $scope.newRegionSetIdCounter = -1;
        };
                
        $scope.onOk = (function () {
            $scope.saveItems(LoadRegionSets) // Reload region set data (because it's changed).
            $("#dialog-ManageRegionSets").dialog("close");
        });

        $scope.onCancel = (function () {
            $("#dialog-ManageRegionSets").dialog("close");
        });
    });

    regionEditor.controller("EditRegionController", function EditRegionController($scope, $timeout) {
        $scope.currentRegion;
        $scope.currentRegionSet;

        $scope.states;
        $scope.selectedState;

        $scope.stateFilterString;
        $scope.countyFilterString;

        $scope.DefaultRegionId = ML.DefaultRegionId;

        $scope.stateFilter = function (state) {
            return !$scope.stateFilterString || state.Abriv.toUpperCase().indexOf($scope.stateFilterString.toUpperCase()) != -1;
        }

        $scope.countyFilter = function (county) {
            return !$scope.countyFilterString || county.Name.toUpperCase().indexOf($scope.countyFilterString.toUpperCase()) != -1;
        }

        $scope.checkAllCounties = function (state) {
            for (var i = 0; i < state.Counties.length; i++) {
                var county = state.Counties[i];
                if (!county.assignedRegionName) {  // Check any counties not assigned to any other region.
                    county.isChecked = true;
                }
            }

            state.areAllCountiesChecked = true;
        }

        $scope.uncheckAllCounties = function (state) {
            for (var i = 0; i < state.Counties.length; i++) {
                var county = state.Counties[i];
                if (!county.assignedRegionName) {  // Check any counties not assigned to any other region.
                    county.isChecked = false;
                }
            }

            state.areAllCountiesChecked = false;
        }

        $scope.clickAllCountiesBox = function (state) {
            if (state.areAllCountiesChecked) {
                $scope.checkAllCounties(state);
            } else {
                $scope.uncheckAllCounties(state);
            }
        }

        $scope.clickSelectAllLink = function () {
            for (var i = 0; i < $scope.states.length; i++) {
                var state = $scope.states[i];
                if (state.isChecked) {
                    $scope.checkAllCounties(state);
                }
            }
        }

        $scope.clickDeselectAllLink = function () {
            for (var i = 0; i < $scope.states.length; i++) {
                var state = $scope.states[i];
                $scope.uncheckAllCounties(state);
                state.isChecked = false;
            }
        }

        $scope.clickStateBox = function (state) {
            if (state.isChecked) {
                return;
            }

            $scope.uncheckAllCounties(state);

            if ($scope.selectedState == state.Abriv) {
                $scope.selectedState = '';
            }
        }

        $scope.setAreAllCountiesChecked = function (state) {
            for (var i = 0; i < state.Counties.length; i++) {
                var county = state.Counties[i];
                if (!county.assignedRegionName && !county.isChecked) {  // search for unchecked counties not assigned to any other region.
                    state.areAllCountiesChecked = false;
                    return;
                }
            }

            state.areAllCountiesChecked = true;
        }

        $scope.saveRegion = function (callback) {
            // Get all checked regions.
            var selectedFipsCodes = [];
            for (var i = 0; i < $scope.states.length; i++) {
                var state = $scope.states[i];
                if (state.isChecked) {
                    for (var j = 0; j < state.Counties.length; j++) {
                        var county = state.Counties[j];
                        if (county.isChecked) {  // search for unchecked counties not assigned to any other region.
                            selectedFipsCodes.push(county.Fips);
                        }
                    }
                }
            }

            $scope.currentRegion.FipsCountyCodes = selectedFipsCodes;

            if ($scope.currentRegion.Id == -1) {
                $scope.currentRegionSet.Regions.push($scope.currentRegion);
            }

            var args = {
                BrokerId: ML.BrokerId,
                RegionId: $scope.currentRegion.Id,
                RegionSet: JSON.stringify($scope.currentRegionSet),
            };

            gService.main.callAsyncSimple("SaveRegion", args, function (result) {
                if (result.error) {
                    alert("Failed to save Region:\n" + result.UserMessage);
                }

                if (typeof (callback) == 'function') {
                    callback();
                }
            });
        }

        $scope.init = function (regionSetId, regionId) {
            // blank out selected state and search boxes.
            $scope.selectedState = '';
            $scope.stateFilterString = '';
            $scope.countyFilterString = '';

            // Get selected region set form drop down.
            var $regionListScope = getScope("#RegionList");
            $scope.currentRegionSet = angular.copy(getById($regionListScope.regionSets, regionSetId));

            if (regionId) {
                $scope.currentRegion = getById($scope.currentRegionSet.Regions, regionId);
            } else {
                $scope.currentRegion = new Region(regionSetId);
            }

            $scope.states = angular.copy(stateAndCountyInfo);

            // Check off state and county info based off of region data
            for (var i = 0; i < $scope.states.length; i++) {
                var state = $scope.states[i];

                for (var j = 0; j < state.Counties.length; j++) {
                    var county = state.Counties[j];

                    for (var k = 0; k < $scope.currentRegionSet.Regions.length; k++) {
                        var reg = $scope.currentRegionSet.Regions[k];

                        // Skip default region unless current region is default region.
                        if (reg.Id == $scope.DefaultRegionId && $scope.currentRegion.Id != $scope.DefaultRegionId) {
                            continue;
                        }

                        if (reg.FipsCountyCodes.indexOf(county.Fips) != -1) {
                            // Check boxes if this is the region we are editing
                            if ($scope.currentRegion.Id == reg.Id) {
                                state.isChecked = true;
                                county.isChecked = true;
                            } else {
                                county.assignedRegionName = reg.Name;
                            }
                        }
                    }
                }

                $scope.setAreAllCountiesChecked(state);
            }
        }
        
        $scope.onCancel = function(){
            $("#dialog-EditRegion").dialog("close");
        };
        $scope.onOk = function(){
            // Don't save if default region (it shouldn't have changed anyway), or no changes have been made.
            if ($scope.EditRegionForm.$pristine || $scope.currentRegion.Id == ML.DefaultRegionId) {
                $("#dialog-EditRegion").dialog("close");
                return;
            }
        
            $scope.saveRegion(LoadRegionSets); // Reload region set data (because it's changed).
        
            $("#dialog-EditRegion").dialog("close");
        };
        $scope.onStateGroupClick = (function onStateGroupClick(e) {
            var $this = $(e.currentTarget);
        
            // Don't select/unselect if we clicked on "All Counties" box.
            if (e.target !== this && e.target != $this.find(".stateAbriv")[0]) {
                return;
            }
        
            var $editRegionScope = getScope("#dialog-EditRegion");
        
            if ($this.hasClass("EditRegionStateGroupSelected")) {
                // If clicked on seleceted item, unselect.
                $this.removeClass("EditRegionStateGroupSelected");
                $editRegionScope.selectedState = "";
        
                // Apply changes to model;
                $editRegionScope.$apply();
            } else {
                // Unselect previous selection.
                $(".EditRegionStateGroup").removeClass("EditRegionStateGroupSelected");
        
                // Select current item.
                $this.addClass("EditRegionStateGroupSelected");
                $editRegionScope.selectedState = $this.find(".stateAbriv").text();
        
                // Apply changes to model;
                $editRegionScope.$apply();
        
                // Scroll so selected div is at top of parent div.
                // Note: This has to be done after model is updated.
                this.scrollIntoView();
            }
        });
    });

    regionEditor.controller("RegionListController", function RegionListController($scope, $filter) {
        $scope.regionSets;
        $scope.selectedSetId;

        $scope.DefaultRegionId = ML.DefaultRegionId;

        $scope.getSelectedSetRegions = function () {
            if ($scope.regionSets && $scope.selectedSetId) {
                return getById($scope.regionSets, $scope.selectedSetId).Regions;
            }
        }

        $scope.editRegion = function (id) { openEditRegion($scope.selectedSetId, id); }

        $scope.deleteRegion = function (id) {
            var feeServiceCheckargs = {
                BrokerId: ML.BrokerId,
                RegionId: id
            };

            gService.main.callAsyncSimple("CheckFeeServiceForRegionId", feeServiceCheckargs, function (feeServiceCheckResult) {
                if (!feeServiceCheckResult.error && feeServiceCheckResult.value.FoundInFeeService == "True") {
                    alert("This region is used by your fee service file. Please remove it from your active fee service file before removing it from the region setup.");
                    return;
                }

                if (!confirm("Are you sure you wish to delete this region?")) {
                    return;
                }

                var args = {
                    BrokerId: ML.BrokerId,
                    RegionId: id
                };

                gService.main.callAsyncSimple("DeleteRegion", args, function (result) {
                    if (result.error) {
                        alert("Failed to delete Region:\n" + result.UserMessage);
                        return false;
                    }

                    // Reload region set (recreates default region)
                    LoadRegionSets();
                });
            });
        }

        $scope.init = function (forceApply) {
            $scope.regionSets = angular.copy(regionSets);

            // Set selected set Id to first option if it is not set, or it's value is invalid.
            if ($scope.regionSets.length > 0 && (!$scope.selectedSetId || !getById($scope.regionSets, $scope.selectedSetId))) {
                var sorted = $filter("orderBy")($scope.regionSets, "Name", false);

                $scope.selectedSetId = sorted[0].Id;
            }

            if (forceApply) {
                $scope.$apply();
            }
        };
        
        $scope.onClose = function(){
            window.close();
        };
        $scope.onAddNew = (function () {
            openEditRegion($scope.selectedSetId);
        });
        $scope.onManageRegionSets = (function () {
            var $dialogScope = getScope("#dialog-ManageRegionSets");
            $dialogScope.init();
        
            $("#dialog-ManageRegionSets").dialog({
                modal: true,
        
                closeOnEscape: false,
                width: "500",
                height: "400",
                draggable: false,
                dialogClass: "LQBDialogBox",
                resizable: false
            });
        });
        
    });
}
