﻿// Dependencies: LQBPopup.js, SimplePopups.js
var LqbAsyncPdfDownloadHelper = (function ($) {
    var singleJobType = 0;
    var batchJobType = 1;
    var paymentStatementJobType = 2;

    var pollingHandle = null;
    var pollingAttempts = 0;

    var targetWindow;

    function getTargetWindow() {
        targetWindow = targetWindow ? targetWindow : window;

        // Check if targetWindow is accessable (opm 477933).
        try {
            targetWindow.document;
        } catch (e) {
            return null;
        }

        return targetWindow;
    }

    function _submitSinglePdfDownload(url, target) {
        targetWindow = target;
        var downloadSettings = {
            url: url + '&cmd=submit&jobType=' + singleJobType,
            displayOverlay: false,
            error: _handleError,
            success: function (result) { _handleSubmit(url, singleJobType, result); }
        };

        _submitPdfDownloadRequest(downloadSettings);
    }

    function _submitBatchPdfDownload(url) {
        var downloadSettings = {
            url: url + '&cmd=submit&jobType=' + batchJobType,
            displayOverlay: false,
            error: _handleError,
            success: function (result) { _handleSubmit(url, batchJobType, result); }
        };

        _submitPdfDownloadRequest(downloadSettings);
    }

    function _submitBatchPdfGeneration(url, callback) {
        var downloadSettings = {
            url: url + '&cmd=submit&jobType=' + batchJobType,
            displayOverlay: false,
            error: _handleError,
            success: function (result) { _handleSubmit(url, batchJobType, result, callback); }
        };

        _submitPdfDownloadRequest(downloadSettings);
    }

    function _submitPaymentStatementDownload(url, options) {
        _submitPaymentStatementGenerate(url, options)
    }

    function _submitPaymentStatementGenerate(url, options, callback) {
        var downloadSettings = {
            url: url + '&cmd=submit&jobType=' + paymentStatementJobType + '',
            data: JSON.stringify(options),
            displayOverlay: false,
            error: _handleError,
            success: function (result) { _handleSubmit(url, paymentStatementJobType, result, callback); }
        };

        _submitPdfDownloadRequest(downloadSettings);
    }

    function _submitPdfDownloadRequest(downloadSettings) {
        if (ML.UseSynchronousPdfGenerationFallback) {
            _submitSynchronousPdfGenerationRequest(downloadSettings);
        }
        else {
            this.pollingAttempts = 0;
            callWebMethodAsync(downloadSettings);
            _displayWaitPopup();  
        }
    }

    function _submitSynchronousPdfGenerationRequest(downloadSettings) {
        var url = downloadSettings['url'];
        window.open(url + '&' + ML.SynchronousPdfFallbackQueryStringParameterName + '=true');
    }

    function _displayWaitPopup() {
        var width, height, $waitPopupBody = $('<div></div>');

        if (typeof SimplePopups !== 'undefined') {
            var loadingPopup = SimplePopups.CreateLoadingPopup('Generating PDF...');
            $waitPopupBody.append(loadingPopup);

            width = 250;
            height = 60;
        }
        else {
            TPOStyleUnification.wrapByLoadingContent($waitPopupBody);

            var $loadingSpan = $('<br /><span>Generating PDF...</span>');

            // Certain pages may override the default loading content styling.
            // Ensure the content is centered for the PDF generation popup.
            $waitPopupBody.find('.loading-content').css('text-align', 'center').append($loadingSpan);

            width = 250;
            height = 50;
        }


        if (getTargetWindow() && getTargetWindow().LQBPopup != null) {
            getTargetWindow().LQBPopup.ShowElement($waitPopupBody, {
                width: width,
                height: height,
                elementClasses: 'FullWidthHeight',
                hideCloseButton: true
            });
        }
    }

    function _handleError() {
        if (this.pollingHandle != null) {
            window.clearInterval(this.pollingHandle);
        }

        if (getTargetWindow() && getTargetWindow().LQBPopup != null) {
            getTargetWindow().LQBPopup.Hide();
        }

        var alertMessage = 'System error. Please contact us if this happens again.';
        if (typeof simpleDialog !== 'undefined') {
            simpleDialog.alert(alertMessage);
        }
        else {
            alert(alertMessage);
        }
    }

    function _handleSubmit(url, jobType, result, callback) {
        if (result['HasError']) {
            _handleError();
            return;
        }

        var jobId = result['JobId'];

        var pollingIntervalMillis = ML.PdfGenerationBackgroundJobPollingIntervalSeconds * 1000;
        this.pollingHandle = window.setInterval(function () { _poll(url, jobType, jobId, callback); }, pollingIntervalMillis);
    }

    function _poll(url, jobType, jobId, callback) {
        var downloadSettings = {
            url: url + '&cmd=poll&jobType=' + jobType + '&jobid=' + jobId,
            displayOverlay: false,
            error: _handleError,
            success: function (result) { _handlePoll(url, jobType, result, callback); }
        };

        callWebMethodAsync(downloadSettings);
    }

    function _handlePoll(url, jobType, result, callback) {
        if (result['HasError']) {
            _handleError();
        }
        else if (result['IsComplete']) {
            var fileDbKey = result['FileDbKey'];
            _displayResult(url, jobType, fileDbKey, callback);
        }
        else
        {
            ++this.pollingAttempts;
            if (this.pollingAttempts > ML.PdfGenerationBackgroundJobMaxPollingAttempts) {
                _handleError();
            }
        }
    }

    function _displayResult(url, jobType, fileDbKey, callback) {
        window.clearInterval(this.pollingHandle);
        if (getTargetWindow() && getTargetWindow().LQBPopup != null) {
            getTargetWindow().LQBPopup.Hide();
        }

        if (typeof callback === 'function') {
            callback(fileDbKey);
        }
        else {
            fetchFileAsync(url + '&cmd=retrieve&jobType=' + jobType + '&fileDbKey=' + fileDbKey, 'application/pdf');
        }
    }

    return {
        SubmitSinglePdfDownload: _submitSinglePdfDownload,
        SubmitBatchPdfDownload: _submitBatchPdfDownload,
        SubmitBatchPdfGeneration: _submitBatchPdfGeneration,
        SubmitPaymentStatementDownload: _submitPaymentStatementDownload,
        SubmitPaymentStatementGenerate: _submitPaymentStatementGenerate
    };
})(jQuery);