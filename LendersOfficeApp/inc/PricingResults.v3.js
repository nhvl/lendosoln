﻿
var app;
var resultsScope;

function initializeAngular() {

    app = angular.module("pricingResults", []);

    $("#ResultsContainer").on("mouseover", ".disqualified", showTooltip);
    $("#ResultsContainer").on("mouseleave", ".disqualified", hideTooltip);

    app.controller("pricingResultsController", ['$scope', '$compile', '$element', function ($scope, $compile, $element) {

        $scope.selectOption = selectOption;
        $scope.viewRatePopup = function (rateOption, program, selector) {
            $scope.selectedRate = rateOption;
            $scope.selectedProgram = program;
            var height;
            if (selector == '.cash-to-close' && ML.IsUlad2019) {
                populateQualifyingBorrower(rateOption);
                selector = '.true-container';
                height = 700;
            }
            else {
                height = "auto";
            }
            

            $(selector).dialog({
                buttons: [
                    {
                        text: "Ok",
                        click: function () {
                            $(this).dialog("destroy");
                        }
                    }
                ],
                modal: true,
                height: height,
                width: "auto",
                maxHeight: 600
            });
        };


        $scope.setSelectedRate = function (rateOption) {
            $scope.selectedRate = rateOption;
        }

        $scope.compileResults = function (results, sDisclosureRegulationT, pricingSettings) {
            resultsScope = $scope.$new();
            resultsScope.results = results;


            resultsScope.sDisclosureRegulationT = sDisclosureRegulationT;
            resultsScope.pricingSettings = pricingSettings;
            createScrollEvent();
            var compiledDirective = $compile("<pricing-results-directive></pricing-results-directive>");
            var element = compiledDirective(resultsScope);
            $("#ResultsContainer").append(element);
        };

        $scope.hasFirstAmericanSource = function (rateOption) {
            if (rateOption) {
                var firstAmericanFees = rateOption.ClosingCostBreakdown.filter(function (fee) {
                    return fee.Source == "First American Quote";
                });

                return firstAmericanFees.length > 0;
            }

            return false;
        }

    }]);

    app.directive("pricingResultsDirective", function () {
        return {
            templateUrl: ML.VirtualRoot + "/newlos/Lockdesk/PricingResults.v3.html",
            restrict: "E",
            scope: true,
            link: function (scope, element, attrs) {
                scope.getPricingSetting = getPricingSetting;
            }
        };
    });

    app.directive("programTable", function () {
        return {
            templateUrl: ML.VirtualRoot + "/newlos/Lockdesk/PricingResultProgramTable.html",
            replace: true,
            scope: true,
            link: function (scope, element, attrs) {
                scope.programSet = attrs.programset;
                scope.limitTo = attrs.limitto;
            }
        }
    })

    app.directive("programRow", function () {
        return {
            templateUrl: ML.VirtualRoot + "/newlos/Lockdesk/PricingResultRow.html",
            replace: true,
            scope: true,
            link: function (scope, element, attrs) {
                scope.rateOption = scope.$parent.$eval(attrs.rateoption);
            }
        }
    });

    app.filter("sortWorst", function () {
        return function(rateOptions, isWorst) {
            if(!isWorst) {
                return rateOptions;
            }
            else {
                return rateOptions.sort(worstComparator);
            }
        }
    });
}

function worstComparator(a, b) {
    var rateA = parseFloat(a.RawRate);
    var rateB = parseFloat(b.RawRate);
    if (rateA < rateB) {
        return 1;
    }

    if (rateA > rateB) {
        return -1;
    }

    if(a.IsPaired && !b.IsPaired)
    {
        return -1;
    }

    if (!a.IsPaired && b.IsPaired) {
        return 1;
    }

    if(a.IsHistorical && !b.IsHistorical)
    {
        return -1;
    }

    if (!a.IsHistorical && b.IsHistorical) {
        return 1;
    }

    var priceA = parseFloat(a.price);
    var priceB = parseFloat(b.Price);
    if (priceA < priceB) {
        return 1;
    }

    if (priceA > priceB) {
        return -1;
    }

    return 0;
}

function getScope(selector) {
    var appElement = $(selector);
    return angular.element(appElement).scope();
}

function getPricingSetting(fieldId) {
    console.log($("#" + fieldId).prop("checked"));
    return $("#" + fieldId).prop("checked");
}

function updateResults(pricingSettings, sDisclosureRegulationT, results) {
    $(window).off("scroll", infiniteScrollHandler);

    if (resultsScope != null) {
        resultsScope.$destroy();
        $("#ResultsContainer").empty();
    }

    for (i = 0; i < results.IneligibleGroups.length; i++)
    {
        results.IneligibleGroups[i].IsCollapsed = true;
    }

    var $scope = getScope("#PricingResultsContainer");
    $scope.compileResults(results, sDisclosureRegulationT, pricingSettings);
    console.log(results);
}

function createScrollEvent() {
    $(window).on("scroll", infiniteScrollHandler);
    determineRepeatLimits();
}

function infiniteScrollHandler(event) {
    var $window = $(window);
    var $document = $(document);
    var scrollHeight = $document.height();
    var scrollPosition = $window.height() + $window.scrollTop();

    if ((scrollHeight - scrollPosition) < 200) {
        //We've reached the bottom of the page, so let's render some more programs
        determineRepeatLimits();
        resultsScope.$apply();
    }
}

var extraHeight = 200;
var programHeight = 63;
var rateHeight = 37;

var currentHeight = 0;

function determineRepeatLimits() {
    currentHeight = $("#ResultsContainer").height() + $(".results-filter").height() + $(".outermost-container > .header").height();

    if (resultsScope.limitEligible == undefined) {
        resultsScope.limitEligible = 0;
    }
    if (resultsScope.limitIneligible == undefined) {
        resultsScope.limitIneligible = 0;
    }

    var eligibleGroups = resultsScope.results.EligibleGroups;
    calculateLimitGroup("limitEligible", eligibleGroups, resultsScope);

    if (resultsScope.limitEligible == eligibleGroups.length) {
        var ineligibleGroups = resultsScope.results.IneligibleGroups;
        calculateLimitGroup("limitIneligible", ineligibleGroups, resultsScope);
    }
}

function calculateLimitGroup(limitFieldName, set, scope) {
    if (set.length > 0) {
        var documentHeight = $(document).height();
        var currentLimit = scope[limitFieldName];
        currentLimit = currentLimit == 0 || set[0].ResultRateOptions ? currentLimit : currentLimit - 1;
        if (currentLimit < set.length) {
            for (i = currentLimit; i < set.length; i++) {
                var group = set[i];

                if (group.IsCollapsed) {
                    currentHeight += 25;
                }
                else if (group.ResultLoanPrograms) {
                    calculateLimitProgram(group.ResultLoanPrograms, group, documentHeight);
                }
                else if (group.ResultRateOptions) {
                    currentHeight += programHeight + rateHeight * group.ResultRateOptions.length;
                }


                if (currentHeight > documentHeight + 200) {
                    scope[limitFieldName] = i + 1;
                    break;
                }
                else if (i == set.length - 1) {
                    scope[limitFieldName] = i + 1;
                }
            }
        }
    }
}

function calculateLimitProgram(programs, group, documentHeight) {
    var currentLimit = group.limitTo;
    if (currentLimit == undefined) {
        currentLimit = 0;
    }

    for (j = currentLimit; j < programs.length; j++) {
        var program = programs[j];
        var rateOptions = program.ResultRateOptions;

        if (rateOptions) {
            if (program.IsDisplayingAllRates) {
                currentHeight += programHeight + rateHeight * rateOptions.length;
            }
            else
            {
                currentHeight += programHeight + rateHeight;
            }

            if (currentHeight > documentHeight + 200) {
                group.limitTo = j + 1;
                break;
            }
        }
        
        if (j == programs.length - 1) {
            group.limitTo = j + 1;
        }
    }
}

function f_generateQueryString(obj) {
    var o = obj || '';
    var str = '';
    for (var p in o) {
        if (o.hasOwnProperty(p)) {
            str += p + '=' + encodeURIComponent(o[p]) + '&';
        }
    }
    return str;
}

function selectOption(program, rateOption) {
    var prodFilterDisplayrateMerge = resultsScope.pricingSettings.sProdFilterDisplayrateMerge;

    var obj = {
        loanid: ML.sLId,
        productId: prodFilterDisplayrateMerge ? rateOption.LpTemplateId : program.lLpTemplateId,
        lienQualifyModeT: '0',
        sPricingModeT: pricingMode,
        version: '',
        rate: rateOption.Rate,
        fee: rateOption.Point,
        UniqueChecksum: prodFilterDisplayrateMerge ? rateOption.UniqueChecksum : program.UniqueChecksum,
        snapshotJson: $('#HistoricalSubmissions').val(),
        isHistorical: rateOption.IsHistorical || resultsScope.pricingSettings.GetResultsUsing == 1
    };

    var url = '/newlos/lockdesk/InternalPricingSelectionOption.aspx?' + f_generateQueryString(obj);
    showModal(url, null, null, null, function(args){
        if (args.isSubmit) {
            navigateOnSubmit();
        }
    }, {hideCloseButton: true})
}


function hideTooltip() {
    $(".disqual-reason-popup").dialog("close");
}

function showTooltip(event) {
    $(".disqual-reason-popup").dialog({
        modal: false,
        closeOnEscape: true,
        width: 200,
        minHeight: 0,
        minWidth: 0,
        width: "auto",
        height: "auto",
        position: {
            my: "left+3 bottom-3",
            of: event,
            collision: "flip fit"
        }
    });
}

