﻿jQuery(function(jq) {
    var $main = jq('#TradeTableMain');
    var selectTradeOnly = false;
    //We don't want to accidentally touch anything outside the portlet,
    // so we'll restrict everything to searching within the portlet's main div
    // This should work exactly like using jquery normally as long as you don't
    // actually need to use the global jquery object (it's jq if you need it).
    function $(a) {
        return jq(a, $main);
    }

    function openTradeEditorWindow(params) {
        if (params) {
            params = '?' + params;
        }
        else {
            params = '';
        }

        var w = screen.availWidth - 10;
        var h = screen.availHeight - 50;


        var win = window.open('MortgagePools/TradeEditor.aspx' + params, 'TradeEdit', 'width=' + w + 'px,height=' + h + 'px,left=0px,top=0px,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes');
        win.focus();
    }

    //Cached selectors
    var $tables = $('table');


    if ($tables.length === 0) {
        $main.append('<div class="NoTrades">No trades found</div>');
    }
    //Tablesorter gets angry if you run it on a table with no rows, so make sure we don't do that.
    var tableOnlyHasTotalsRow = false;
    $tables.each(function() {
        var $table = $(this);
        var $children = $table.find('tbody').children();
        var tableIsEmpty = $children.length == 0;
        tableOnlyHasTotalsRow = $children.length == 1 && $children.hasClass('static');
        if (tableIsEmpty || tableOnlyHasTotalsRow) {
            $table.find('tbody tr.static').hide();
            $table.find('tbody').append('<tr><td colspan=20>No trades found</td></tr>');
        }
    });

    //Need to use global jquery to change tablesorter settings, because obvs our little $ fxn doesn't know about it.
    jq.tablesorter.defaults.widgetZebra = {
        css: ["GridItem", "GridAlternatingItem"]
    };

    var sorting = [[0, 0]];
    $tables.each(function() {
        var $table = $(this);
        $table.tablesorter({
            sortList: sorting,
            widgets: ['staticRow', 'zebra']
        });
    });

    $('tbody').on('click', '.trade-num', function() {
        var tradeId = $(this).find('.trade-id').val();
        var poolId = $(this).find('.pool-id').val();

        jq(window).trigger('TradeSelected', [tradeId, poolId]);
        if (selectTradeOnly) return;

        var poolPart = "";
        if (poolId) poolPart = "&PoolId=" + poolId;

        openTradeEditorWindow('TradeId=' + tradeId + poolPart);
    });

    $('tbody').on('click', 'td.delete', function() {
        var $this = $(this);
        var hasTransactions = $('.hasTransactions', this).val() == "True";
        var transactionsString = "";
        if (hasTransactions) {
            transactionsString = " Associated transactions will be deleted as well."
        }

        var $row = $this.closest('tr');
        var tradeNum = jq.trim($row.find('td.trade-num').text());
        var confirmMessage = "Are you sure you want to delete Trade " + tradeNum + "?" + transactionsString;
        if (!confirm(confirmMessage)) return;

        window.setTimeout(function() {
            $this.find('span').removeClass('action').text("deleting");
        }, 500);

        var DTO = {
            tradeId: $('.tradeId', this).val()
        }

        var $table = $this.closest('table');

        jq.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: 'CapitalMarkets/TradesService.aspx/DeleteTrade',
            data: JSON.stringify(DTO),
            dataType: 'json'
        }).then(
            function(msg) {
                $row.hide();
                $table.trigger("sorton", [$table[0].config.sortList]);
            },
            function(XMLHttpRequest, textStatus, errorThrown) {
                alert("An unexpected error happened, please try again later.");
            }
        );
    });

    if (jq('#HideDeleteLinks').length) {
        $('.delete').hide();
    }

    if (jq('#SelectTradeOnly').length) {
        selectTradeOnly = true;
    }

    var displayExpanded = !!jq('#DisplayExpanded').length;
    $('.expanded').toggle(displayExpanded && !tableOnlyHasTotalsRow);

    $tables.css({visibility:""});
});
