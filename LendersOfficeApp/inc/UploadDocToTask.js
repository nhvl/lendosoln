﻿function checkUploadButtonState() {
    var shouldDisable = false;
    $('.file-row').each(function (index, row) {
        var $row = $(row);

        var selectedDocType = $row.find('.SelectedDocTypeId').val();
        if (selectedDocType == null || selectedDocType.trim() === '' || selectedDocType === '-1') {
            shouldDisable = true;
            return false;
        }
    });

    setUploadButtonDisabled(shouldDisable);
}

function setUploadButtonDisabled(disabled) {
    $('#UploadButton').prop('disabled', disabled);
}

function uploadDocuments(iframeSelf) {
    var errorMessages = [];
    $('.file-row').each(function (index, row) {
        var $row = $(row);

        var selectedDocType = $row.find('.SelectedDocTypeId').val();
        if (selectedDocType == null || selectedDocType.trim() === '' || selectedDocType === '-1') {
            errorMessages.push('Please select a Doc Type for all documents.');
            return false;
        }

        if ($row.find('.InvalidTextareaLength').length !== 0) {
            errorMessages.push('Descriptions have a maxiumum of 200 characters.');
            return false;
        }
    });

    if (errorMessages.length !== 0) {
        LQBPopup.ShowString(errorMessages.join('<br />'));
        return;
    }

    var fileDataByName = {};
    $('.file-row').each(function (index, row) {
        var $row = $(row);
        var filename = $row.find('.Filename').text();

        fileDataByName[filename] = {
            aAppId: $row.find('.ApplicationDdl').val(),
            DocTypeId: $row.find('.SelectedDocTypeId').val(),
            PublicDescription: $row.find('.Description').val()
        };
    });

    if (iframeSelf) {
        iframeSelf.resolve({
            value: {
                OK: true,
                fileDataByName: fileDataByName
            }
        });
    }
    else {
        parent.LQBPopup.Return({
            OK: true,
            fileDataByName: fileDataByName
        });
    }
}

function removeDocument(event) {
    var target = retrieveEventTarget(event);
    var $link = $(target);
    $link.closest('tr').remove();

    if ($('.file-row').length === 0) {
        if (typeof closeTaskDocumentUploadPopup === 'function') {
            closeTaskDocumentUploadPopup();
        }
        else {
            parent.LQBPopup.Return({ OK: false });
        }
    }
    else {
        checkUploadButtonState();
    }
}