﻿tableUpdater.prototype.addRow = function(dataObject) {
    var id = this.getIDFromObject(dataObject);
    if (true === this.doesIdExist(id)) {
        alert('id ' + id + ' already exists in table.');
        return false;
    }
    var newRow = this.$rowTemplate.tmpl(dataObject);
    this.rowsByID[id] = newRow;
    this.dataObjectsByID[id] = dataObject;
    this.$tableBody.append(newRow);
    $(newRow).toggleClass(
        'alternateRow',
        -1 === $.inArray(
            'alternateRow',
            ($(newRow).prev().attr('class') || '').split(' ')));
    return true;
}

tableUpdater.prototype.doesIdExist = function(id) {
    if (typeof this.rowsByID[id] != 'undefined' &&
		typeof this.dataObjectsByID[id] != 'undefined') {
        return true;
    }
    return false;
}

tableUpdater.prototype.deleteRow = function(dataObject) {
    var id = this.getIDFromObject(dataObject);
    if (false === this.doesIdExist(id)) {
        alert('id ' + id + ' not in table.');
        return false;
    }
    $(this.rowsByID[id]).nextAll().toggleClass('alternateRow');
    $(this.rowsByID[id]).remove();
    delete this.rowsByID[id];
    delete this.dataObjectsByID[id];
    return true;
}

function tableUpdater($table, $rowTemplate, dataObjects, getIDFromObject) // at some point add the ability to add event handlers.
{
    this.$rowTemplate = $rowTemplate;
    this.$tableBody = $($table).children('tbody');
    this.rowsByID = {};
    this.dataObjectsByID = {};
    this.getIDFromObject = getIDFromObject;

    for (var dataObjKey in dataObjects) {
        this.addRow(dataObjects[dataObjKey]);
    }

    return this;
}
