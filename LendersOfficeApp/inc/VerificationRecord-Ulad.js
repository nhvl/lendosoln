﻿var uladToLegacyMapAssets = {
    Attention: 'Attention',

    CompanyName: 'DepositoryName',

    StreetAddress: 'DepositoryStAddr',
    City: 'DepositoryCity',
    State: 'DepositoryState',
    Zip: 'DepositoryZip',

    IsSeeAttachment: 'IsSeeAttachment',

    VerifSentDate: 'VerifSentD',
    VerifReorderedDate: 'VerifReorderedD',
    VerifRecvDate: 'VerifRecvD',
    VerifExpiresDate: 'VerifExpD'
};

var uladToLegacyMapLiabilities = {
    Attention: 'Attention',

    Bal : 'Bal0',
    CompanyName : 'ComNm',
    CompanyAddress : 'ComAddr',
    CompanyCity : 'ComCity',
    CompanyState : 'ComState',
    CompanyZip : 'ComZip',

    IsSeeAttachment: 'IsSeeAttachment',

    VerifSentDate: 'VerifSentD',
    VerifReorderedDate: 'VerifReorderedD',
    VerifRecvDate: 'VerifRecvD',
    VerifExpiresDate: 'VerifExpD'
};

function retrieveSelectedEntry(entries) {
    return $.grep(entries, function (el, index) { return el.Id == ML.recordId })[0];
}

function populateUladDataToLegacyUI(entries, otherData) {

    var fieldData = retrieveSelectedEntry(entries);

    if (typeof(createListOfEntries) != 'undefined') {
        createListOfEntries(entries, fieldData);
    }

    var keys = Object.keys(fieldData);
    var map = uladToLegacyMap;
    var legacyObject = {};
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        var correspondingId = map[key];

        if (correspondingId == 'SPECIAL') {
            if (key == 'VerifSignatureImgId') {
                $('#VerifHasSignature').val(fieldData[key] != '' && fieldData[key] != null);
            }
        }
        else if (correspondingId != null) {
            legacyObject[correspondingId] = fieldData[key];
        }
    }

    populateForm(legacyObject);

    if (fieldData.VerifHasSignature == 'True') {
        $('#SignatureImg').prop('src', 'UserSignature.aspx?eid=' + fieldData.VerifSigningEmployeeId);
    }

    if (typeof (pageSpecificPopulation) == 'function') {
        pageSpecificPopulation(entries, otherData);
    }

    updateButtons();
}

function isVod(asset) {
    var convertedAssetGroupT = Math.Pow(2, parseInt(asset.AssetType));
    return assetGroupT.HasFlag(convertedAssetGroupT);
}

function isVol(liability) {
    return liability.DebtType == 1 || liability.DebtType == 5;
}

function isVom(liability) {
    return liability.DebtType == 3 || liability.DebtType == 11;
}

function createUladObject() {
    var map = uladToLegacyMap;
    var keys = Object.keys(map);
    var uladObject = {};
    for (var i = 0; i < keys.length; i++) {
        {
            var uladKey = keys[i];
            var legacyKey = map[uladKey];
            uladObject[uladKey] = $("#" + legacyKey).val();
        }
    }

    uladObject.Id = ML.recordId;
    return uladObject;
}

function loadAfterML() {
    if (window.oldSaveMe == null) {
        window.oldSaveMe = saveMe;
        window.saveMe = saveUladPage;
        window.saveDataAndLoadNext = saveUladPageLoadNext;
        window.loadNext = loadNextUlad;
    }

    setTimeout(function () {
        if (typeof (ML) == 'object') {
            if ($('#IsUlad').val() == 'True') {
                updateData(populateUladDataToLegacyUI);
            }
        }
        else {
            loadAfterML();
        }
    }, 500);
}

function saveUladPage() {
    var uladData = createUladObject();
    updateData(null, true, uladData, ML.recordId);
    oldSaveMe();
    updateButtons();
    return true;
}

function saveUladPageLoadNext() {
    var uladData = createUladObject();
    var recordId = list[currentIndex];
    ML.recordId = recordId;
    updateData(populateUladDataToLegacyUI, true, uladData, recordId);
    oldSaveMe();
    updateButtons();
    return true;
}

function loadNextUlad() {
    var recordId = list[currentIndex];
    ML.recordId = recordId;
    updateData(populateUladDataToLegacyUI, false, null, recordId);
    updateButtons();
    return true;
}

loadAfterML();