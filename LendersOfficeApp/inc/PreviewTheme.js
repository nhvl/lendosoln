﻿function onClose() {
    if (isLqbPopup(window)) {
        window.parent.LQBPopup.Return();
    }
    else {
        window.close();
    }
}

function onCloseQuickPreview() {
    if (typeof window.parent !== "undefined" &&
        typeof window.parent.onCloseQuickPreview === "function") {
        window.parent.onCloseQuickPreview();
    }
}

$(document).ready(function () {
    $("#ThemeName").text(ML.ThemeName)

    if (ML.QuickPreview) {
        $('#CloseIcon').hide();
        $('#ClosePreview').click(onCloseQuickPreview);
        $('h4.page-header').hide();
    }
    else {
        $('#ClosePreview').hide();
        $('#CloseIcon').click(onClose);
    }

    $.each(variables, function (index, variable) {
        var variableInput = $('#' + variable.Id);
        variableInput.val(variable.Value);
        variableInput.prev('div.color').css('background-color', variable.Value);
    });
})
