﻿var ArchiveStatusPopupMode = {
    Unknown: 0,
    PendingFromCoC: 1,
    PendingFromManual: 2,
    PendingWithoutLastDisclosed: 3,
    PendingLoanEstimateForClosingDisclosure: 4,
    UnknownNoCancel: 5,
    PendingNoCancel: 6,
    UnknownFromCoC: 7,
    HasBothCDAndLEArchiveInUnknownStatus: 8
};

var ArchiveStatusPopupSource = {
    SeamlessDocumentGeneration: 0,
    ChangeOfCircumstance: 1,
    NonSeamlessDocumentGeneration: 2
};

var ArchiveStatusPopup = (function($) {
    var targetDomElementId, sLId, closeIfStatusSetToInvalid, mode, closeHandler, userCancelled, closeOnCancelForUnknown, sourcePage;
    var confirmUnknownInvalidationDivId = null, confirmUnknownInvalidationCheckboxId = null;

    var INVALID_STATUS = 1;
    var DISCLOSED_STATUS = 2;
    var PENDING_DOCUMENT_GENERATION_STATUS = 3;
    var SUPERSEDED_COC_STATUS = 4;
    var INCLUDED_IN_CLOSING_DISCLOSURE_STATUS = 5;
        
    function displayPopup() {
        var config = getDialogConfig(mode);
        $('#' + targetDomElementId).dialog(config);
    }

    function getDialogConfig(mode) {
        var sharedConfig = getCommonConfig();
        var modeSpecificConfig = null;

        if (mode === ArchiveStatusPopupMode.Unknown) {
            modeSpecificConfig = getUnknownConfig();
        } else if (mode === ArchiveStatusPopupMode.PendingFromCoC) {
            modeSpecificConfig = getPendingCoCConfig();
        } else if (mode === ArchiveStatusPopupMode.PendingFromManual) {
            modeSpecificConfig = getPendingManualConfig();
        } else if (mode === ArchiveStatusPopupMode.PendingWithoutLastDisclosed) {
            modeSpecificConfig = getPendingWithoutLastDisclosedConfig();
        } else if (mode === ArchiveStatusPopupMode.PendingLoanEstimateForClosingDisclosure) {
            modeSpecificConfig = getPendingLEForClosingDisclosureConfig();
        } else if (mode === ArchiveStatusPopupMode.PendingNoCancel) {
            modeSpecificConfig = getPendingNoCancelConfig();
        } else if (mode === ArchiveStatusPopupMode.UnknownNoCancel) {
            modeSpecificConfig = getUnknownNoCancelConfig();
        } else if (mode === ArchiveStatusPopupMode.UnknownFromCoC) {
            modeSpecificConfig = getUnknownFromCoCConfig();
        } else if (mode === ArchiveStatusPopupMode.HasBothCDAndLEArchiveInUnknownStatus) {
            modeSpecificConfig = getHasBothCDAndLEArchiveInUnknownStatus();
        } else {
            throw 'Unsupported popup mode.';
        }

        return $.extend(sharedConfig, modeSpecificConfig);
    }

    function getUnknownConfig() { 
        return {
            title: 'Unknown Archive Status',
            buttons: [
                {
                    width: 340,
                    text: 'Previously generated documents have been or will be provided to the borrower manually.',
                    click: function() {
                        setUnknownArchiveStatus(DISCLOSED_STATUS, $.proxy(function() { $(this).dialog('close'); }, this));
                    }
                },
                {
                    width: 340,
                    text: 'Previously generated documents are invalid and will not be disclosed.',
                    click: function() {
                        function closeUnknownPopup() {
                            $('#' + targetDomElementId).dialog('close');

                            if (closeIfStatusSetToInvalid && !closeHandler) {
                                window.close();
                            }
                        }

                        if (confirmUnknownInvalidationDivId && confirmUnknownInvalidationCheckboxId) {
                            $('#' + confirmUnknownInvalidationDivId).dialog({
                                closeOnEscape: false,
                                dialogClass: 'LQBDialogBox no-close',
                                modal: true,
                                title: 'Confirm Archive Invalidation',
                                resizable: false,
                                draggable: false,
                                buttons: [
                                    {
                                        text: 'OK',
                                        click: function () {
                                            if ($('#' + confirmUnknownInvalidationCheckboxId).prop('checked')) {
                                                setUnknownArchiveStatus(INVALID_STATUS, $.proxy(function() {
                                                    $(this).dialog('close');
                                                    closeUnknownPopup();
                                                }, this));
                                            } else {
                                                alert('You must confirm you have read the message.');
                                            }
                                        }
                                    },
                                    {
                                        text: 'Cancel',
                                        click: function () {
                                            $(this).dialog('close');
                                        }
                                    }
                                ]
                            });
                        } else {
                            setUnknownArchiveStatus(INVALID_STATUS, closeUnknownPopup);
                        }
                    }
                },
                {
                    width: 75,
                    text: 'Cancel',
                    click: function() {
                        userCancelled = true;

                        if (closeOnCancelForUnknown) {
                            window.close();
                        } else {
                            $(this).dialog('close');
                        }
                    }
                }
            ]
        };
    }

    function getUnknownNoCancelConfig() { 
        return {
            title: 'Unknown Archive Status',
            buttons: [
                {
                    width: 340,
                    text: 'The documents were provided to the borrower',
                    click: function() {
                        setUnknownArchiveStatus(DISCLOSED_STATUS, $.proxy(function() { $(this).dialog('close'); }, this));
                    }
                },
                {
                    width: 340,
                    text: 'The documents were NOT provided to the borrower',
                    click: function() {
                        setUnknownArchiveStatus(INVALID_STATUS, $.proxy(function() {
                            $(this).dialog('close');

                            if (closeIfStatusSetToInvalid && !closeHandler) {
                                window.close();
                            }
                        }, this));
                    }
                }
            ]
        };
    }

    function getUnknownFromCoCConfig() {
        var config = getUnknownConfig();
        var resubmitText = sourcePage === ArchiveStatusPopupSource.ChangeOfCircumstance ?
            'Contained CoC is valid, but previously generated documents will not be disclosed to the borrower. Additional CoC is being performed now.'
            : 'Contained CoC is valid, but previously generated documents will not be disclosed to the borrower. Pending CoC will be resubmitted now.';

        config.buttons[1].text = 'Previously generated documents and CoC are both invalid and will not be disclosed. Pending CoC will be discarded.';

        var resubmitButton = {
            width: 340,
            text: resubmitText,
            click: function () {
                var args = {
                    sLId: sLId
                };

                callServiceMethod('PrepareUnknownLoanEstimateArchiveForResubmission', args, $.proxy(function() { $(this).dialog('close'); }, this));
            }
        };

        // Insert it after the disclosed button and before the invalid button.
        config.buttons.splice(1, 0, resubmitButton);

        return config;
    }

    function getHasBothCDAndLEArchiveInUnknownStatus() {
        var resubmitText = sourcePage === ArchiveStatusPopupSource.ChangeOfCircumstance ?
            'Contained CoC is valid, but previously generated documents will not be disclosed to the borrower. Additional CoC is being performed now.'
            : 'Contained CoC is valid, but previously generated documents will not be disclosed to the borrower. Pending CoC will be resubmitted now.';

        return {
            title: 'Unknown Archive Status',
            buttons: [
                {
                    width: 340,
                    text: 'Previously generated documents have been or will be provided to the borrower manually.',
                    click: function () {
                        setBothLEAndCDUnknownArchiveStatuses(INCLUDED_IN_CLOSING_DISCLOSURE_STATUS, DISCLOSED_STATUS, $.proxy(function () { $(this).dialog('close'); }, this));
                    }
                },
                {
                    width: 340,
                    text: resubmitText,
                    click: function () {
                        setBothLEAndCDUnknownArchiveStatuses(PENDING_DOCUMENT_GENERATION_STATUS, INVALID_STATUS, $.proxy(function () {
                            $(this).dialog('close');

                            if (closeIfStatusSetToInvalid && !closeHandler) {
                                window.close();
                            }
                        }, this));
                    }
                },
                {
                    width: 340,
                    text: 'Previously generated documents and CoC are both invalid and will not be disclosed. Pending CoC will be discarded.',
                    click: function () {
                        setBothLEAndCDUnknownArchiveStatuses(INVALID_STATUS, INVALID_STATUS, $.proxy(function () {
                            $(this).dialog('close');

                            if (closeIfStatusSetToInvalid && !closeHandler) {
                                window.close();
                            }
                        }, this));
                    }
                }
            ]
        };
    }

    function getPendingCoCConfig() {
        return {
            title: 'Pending Archive Status',
            buttons: [
                {
                    width: 340,
                    text: 'The CoC is still valid and has NOT yet been disclosed.\r\n (This will move the archive to the "Superseded CoC" status).',
                    click: function () {
                        setPendingArchiveStatus(SUPERSEDED_COC_STATUS, $.proxy(function () { $(this).dialog('close'); }, this));
                    }
                },
                {
                    width: 340,
                    text: 'The CoC is still valid and was disclosed.\r\n (This will move the archive to the "Disclosed" status).',
                    click: function() {
                        setPendingArchiveStatus(DISCLOSED_STATUS, $.proxy(function() { $(this).dialog('close'); }, this));
                    }
                },
                {
                    width: 340,
                    text: 'The CoC is not valid.\r\n (This will move the archive to the "Invalid" status).',
                    click: function() {
                        setPendingArchiveStatus(INVALID_STATUS, $.proxy(function() { $(this).dialog('close'); }, this));
                    }
                },
                {
                    width: 75,
                    text: 'Cancel',
                    click: function() {
                        userCancelled = true;
                        $(this).dialog('close');
                    }
                }
            ]
        };
    }

    function getPendingManualConfig() {
        return {
            title: 'Pending Archive Status',
            buttons: [
                {
                    width: 340,
                    text: 'The documents were provided to the borrower',
                    click: function() {
                        setPendingArchiveStatus(DISCLOSED_STATUS, $.proxy(function() { $(this).dialog('close'); }, this));
                    }
                },
                {
                    width: 340,
                    text: 'The documents were NOT provided to the borrower',
                    click: function() {
                        setPendingArchiveStatus(INVALID_STATUS, $.proxy(function() {
                            $(this).dialog('close');

                            if (closeIfStatusSetToInvalid && !closeHandler) {
                                window.close();
                            }
                        }, this));
                    }
                },
                {
                    width: 75,
                    text: 'Cancel',
                    click: function() {
                        userCancelled = true;
                        $(this).dialog('close');
                    }
                }
            ]
        };
    }

    function getPendingWithoutLastDisclosedConfig() {
        return {
            title: 'Pending Archive Status',
            buttons: [
                {
                    width: 340,
                    text: 'Disclose from the pending archive.',
                    click: function() {
                        $(this).data('tempArchiveSource', 'pending');
                        $(this).dialog('close');
                    }
                },
                {
                    width: 340,
                    text: 'Use live loan data and set pending archive to invalid.',
                    click: function() {
                        setPendingArchiveStatus(INVALID_STATUS, $.proxy(function() {
                            $(this).data('tempArchiveSource', 'live');

                            $(this).dialog('close');
                        }, this));                        
                    }
                },
                {
                    width: 75,
                    text: 'Cancel',
                    click: function() {
                        userCancelled = true;
                        $(this).dialog('close');
                    }
                }
            ]
        }
    }

    function getPendingLEForClosingDisclosureConfig() {
        return {
            title: 'Pending Archive Status',
            buttons: [
                {
                    width: 340,
                    text: 'Include the data. This will set the status to "Included in Closing Disclosure" after document generation.',
                    click: function() {
                        $(this).data('tempArchiveSource', 'pending');
                        $(this).dialog('close');
                    }
                },
                {
                    width: 340,
                    text: 'Do not include the data. This will set the status to "Invalid" after document generation.',
                    click: function() {
                        $(this).data('tempArchiveSource', 'live');
                        $(this).dialog('close');
                    }
                },
                {
                    width: 75,
                    text: 'Cancel',
                    click: function() {
                        userCancelled = true;
                        $(this).dialog('close');
                    }
                }
            ]
        };
    }

    function getPendingNoCancelConfig() {
        return {
            title: 'Pending Archive Status',
            buttons: [
                {
                    width: 340,
                    text: 'The documents were provided to the borrower',
                    click: function() {
                        setPendingArchiveStatus(DISCLOSED_STATUS, $.proxy(function() { $(this).dialog('close'); }, this));
                    }
                },
                {
                    width: 340,
                    text: 'The documents were NOT provided to the borrower',
                    click: function() {
                        setPendingArchiveStatus(INVALID_STATUS, $.proxy(function() {
                            $(this).dialog('close');

                            if (closeIfStatusSetToInvalid && !closeHandler) {
                                window.close();
                            }
                        }, this));
                   }
                }
            ]
        };
    }

    function getCommonConfig() {
        var config = {
            closeOnEscape: false,
            dialogClass: 'LQBDialogBox no-close',
            modal: true,
            width: 380,
            resizable: false,
            draggable: false
        };

        if (closeHandler) {
            $.extend(config, { close: closeHandler });
        }

        return config;
    }

    function setBothLEAndCDUnknownArchiveStatuses(leStatus, cdStatus, callback) {
        var args = {
            sLId: sLId,
            LEStatus: leStatus,
            CDStatus: cdStatus
        };

        callServiceMethod('UpdateStatusesOfBothLeAndCdArchivesInUnknownStatus', args, callback);
    }

    function setUnknownArchiveStatus(status, callback) {
        var args = {
            sLId: sLId,
            status: status
        };

        callServiceMethod('UpdateStatusOfArchiveInUnknownStatus', args, callback);
    }

    function setPendingArchiveStatus(status, callback) {
        var args = {
            sLId: sLId,
            status: status
        };

        callServiceMethod('UpdatePendingLoanEstimateArchiveStatus', args, callback);
    }

    function callServiceMethod(methodName, args, callback) {
        gService.utils.callAsyncSimple(methodName, args, function (result) { callServiceMethodCallback(result, callback); });
    }

    function callServiceMethodCallback(result, callback) {
        if (result.error) {
            if (result.UserMessage) {
                alert(result.UserMessage);
            } else {
                alert('Encountered unexpected error while updating archive status. Unable to proceed.');
            }

            window.close();
        }

        callback();
    }

    function validateMode(popupMode) {
        return popupMode === ArchiveStatusPopupMode.Unknown ||
            popupMode === ArchiveStatusPopupMode.PendingFromCoC ||
            popupMode === ArchiveStatusPopupMode.PendingFromManual ||
            popupMode === ArchiveStatusPopupMode.PendingWithoutLastDisclosed ||
            popupMode === ArchiveStatusPopupMode.PendingLoanEstimateForClosingDisclosure ||
            popupMode === ArchiveStatusPopupMode.PendingNoCancel ||
            popupMode === ArchiveStatusPopupMode.UnknownNoCancel ||
            popupMode === ArchiveStatusPopupMode.UnknownFromCoC ||
            popupMode == ArchiveStatusPopupMode.HasBothCDAndLEArchiveInUnknownStatus;
    }

    function validateSourcePage(source) {
        return source === ArchiveStatusPopupSource.SeamlessDocumentGeneration ||
            source === ArchiveStatusPopupSource.ChangeOfCircumstance ||
            source === ArchiveStatusPopupSource.NonSeamlessDocumentGeneration;
    }

    return {
        create: function(id, loanId, closeIfInvalid, popupMode, source, onClose, closeIfCancelUnknown, confirmUnknownInvalidationDiv, confirmUnknownInvalidationCheckbox) {
            targetDomElementId = id;
            sLId = loanId;
            closeIfStatusSetToInvalid = closeIfInvalid;
            closeHandler = onClose;
            closeOnCancelForUnknown = closeIfCancelUnknown;
            userCancelled = false;
            confirmUnknownInvalidationDivId = confirmUnknownInvalidationDiv;
            confirmUnknownInvalidationCheckboxId = confirmUnknownInvalidationCheckbox;

            if (validateMode(popupMode)) {
                mode = popupMode;
            } else {
                throw 'Unsupported popup mode';
            }

            if (validateSourcePage(source)) {
                sourcePage = source;
            } else {
                throw 'Unsupported source page'
            }
        },
        displayPopup: displayPopup,
        getUserCancelled: function() {
            return userCancelled;
        },
        getMode: function () {
            return mode;
        }
    };
})(jQuery);
