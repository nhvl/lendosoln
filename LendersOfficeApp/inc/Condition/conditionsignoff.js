﻿// DEPENDENCIES: jquery, jquery tablesorter, jquery templates

var ConditionTable = (function($) {

    var options = {
        ShowHiddenData: false,
        RowTemplateSelector: "#condition_template",
        RowClass: "GridItem",
        Alternating_RowClass: "GridAlternatingItem",
        minHeight: 60 // pixels
    };
    var table, $table, tbody, $tbody, requiresTableUpdate, rowTemplate;
    var isDirty = false;

    CONDITION_DATA_NODE = "ConditionData";

    function setPageDirty(dirty) {
        // when adding conditions, onbeforeunload breaks for some reason, so
        // we have to rebind it
        window.onbeforeunload = ConditionTable.beforeUnload;
        isDirty = dirty;
        $('#saveBtn').prop('disabled', !dirty);
        if (dirty) {
            $table.trigger("update"); // Update for the tablesorter
        }
    }

    function isPageDirty() {
        return isDirty;
    }

    function reorder() {
        $tbody.detach();
        var isAlt = false;
        var i = 1;
        $tbody.find('tr').each(function() {
            var $this = $(this);
            $this.removeClass(options.RowClass)
                   .removeClass(options.Alternating_RowClass) // for each row, apply the appropriate class
                   .addClass(isAlt ? options.Alternating_RowClass : options.RowClass)
                   .find('span.number').text(i); // then update the row number
            // Update the condition number to be in sync with what is displayed.
            $this.data(CONDITION_DATA_NODE).number = i;
            isAlt = !isAlt;
            i++;
        });
        $table.append($tbody);
    }

    function reloadPage() {
        // TODO: instead, just reload the table and set the scroll position
        location.reload(true);
    }

    function savePage(callback) {
        var saveData = {
            sLId: options.sLId,
            ConditionData: null,
            SortOrder: null,
            DeletedConditions: JSON.stringify([])
        };

        var rows = $tbody.children("tr");

        var sortOrder = []; // The order the rows should be in
        var dirtyConditions = []; // The conditions that we want to save
        rows.each(function() {
            var c = $(this).data(CONDITION_DATA_NODE);
            sortOrder.push(c.Id);

            if (c.isDirty) {
                dirtyConditions.push(concentrateChanges($(this)));
            }
        });

        // Should call SavePage
        saveData.ConditionData = JSON.stringify(dirtyConditions);
        saveData.SortOrder = JSON.stringify(sortOrder);

        gService.ConditionService.callAsyncSimple('SavePage', saveData, function (results) { savePageCallback(results, rows, callback); });
    }

    function savePageCallback(results, rows, callback) {
        if (results.error) {
            alert(results.UserMessage);
            setPageDirty(true);
        } else if (!!results.value.ErrorCacheId) {
            showModal('/newlos/Tasks/BatchTaskErrorSummary.aspx?id=' + results.value.ErrorCacheId, null, null, null, function () {
                setPageDirty(false);    // Need to do this or IE will prompt to save page before reload.
                reloadPage();
            },
            { hideCloseButton: true });
            return;
        } else {
            setPageDirty(false);
        }

        // Set them clean
        rows.each(function () {
            var c = $(this).data(CONDITION_DATA_NODE);
            c.isDirty = false;
        });

        if (typeof callback === 'function') {
            callback();
        }
    }

    // Should only be used for status changes
    function saveCondition($row, saveType) {
        var pos = $row.data(CONDITION_DATA_NODE).number;

        var data = {
            ConditionChanges: JSON.stringify(concentrateChanges($row)),
            TaskId: $row.data(CONDITION_DATA_NODE).Id,
            sLId: $row.data(CONDITION_DATA_NODE).LoanId
        };

        gService.ConditionService.callAsyncSimple(saveType, data, function (results) { saveConditionCallback(results, $row, pos); });
    }

    function saveConditionCallback(results, $row, pos) {
        if (results.value && results.value.Condition) {
            var condition = JSON.parse(results.value.Condition);

            if (results.value && results.value.Associations) {
                condition.Associations = JSON.parse(results.value.Associations);
            }

            condition.number = pos;
            populateRow($row, condition);
            updateEditableControls($row, condition.Status, condition.Permission);
        } else {
            alert(results.UserMessage);
        }

    }

    // Returns an object containing the changes to a row
    function concentrateChanges($row) {
        var data = $row.data(CONDITION_DATA_NODE);
        var conditionChanges = $.extend(
            {
                IsDirty: data.isDirty,
                CategoryId: data.CategoryId,
                Subject: data.Subject,
                TaskId: data.Id,
                Permission: data.Permission,
                InternalNotes: data.InternalNotes,
                ResolutionBlockTriggerName: data.ResolutionBlockTriggerName,
                ResolutionDateSetterFieldId: data.ResolutionDateSetterFieldId
            },
            data.changes);

        return conditionChanges;
    }

    function stopSort(e) {
        e.stopImmediatePropagation();
        e.stopPropagation();
        return false;
    }

    function save_onclick(e) {
        e.preventDefault();
        savePage();
    }

    function reactivate_onclick(e) {
        e.preventDefault();
        if (this.disabled) {
            return false;
        }
        var $row = $(this).parents('tr');
        saveCondition($row, "ReactivateCondition");
        return;
    }

    function close_onclick(e) {
        e.preventDefault();
        if (this.disabled) {
            return false;
        }
        var $row = $(this).parents('tr');
        var condition = $row.data(CONDITION_DATA_NODE);
        if (!condition.ConditionFulfilsAssociatedDocs) {
            if (condition.Permission === 'Manage') {
                if (!confirm('This condition is missing an association with a required document type. Are you sure you want to sign off?')) {
                    return;
                }
            }
            else {
                alert('This condition is missing an association with a required document type. Please associate with the required document type in order to sign off.');
                return;
            }
        }
        saveCondition($row, "CloseCondition");
    }

    function disable($row, controls) {
        if (controls.length === 0) {
            return;
        }

        $row.find(controls.join(',')).each(function () {
            if (this.tagName === "DIV") {
                this.contentEditable = false;
            }
            this.disabled = 'disabled';
            this.setAttribute("disabled", true);
            this.readonly = 'readonly';
            this.style.cursor = 'default';
        });
    }

    function hide($row, controls) {
        if (controls.length === 0) {
            return;
        }

        $row.find(controls.join(',')).each(function(i) {
            $(this).hide();
        });
    }

    function isReadOnly() {
        return $('#_ReadOnly').val();
    }

    var _controlNames = [
        'SelectBox',
        'AssignLink',
        'Category',
        'CloseLink',
        'InternalNotes',
        'ReactivateLink',
        'Subject',
        'AssociatedDocsLink'
     ];

    var _controlIdentifiers = [
        'input.selectBox',
        'a.assignlink',
        'select.categories',
        'a.closelink',
        'div.internalNotes',
        'a.reactivatelink',
        'div.subject',
        'a.associatedDocsLinkAssign'
     ];

    //updateEditableControls assumes that all the controls are shown
    //and enabled when called
    function updateEditableControls($row, status, permission) {
        toggleBatchButtons();
        var controls = {};

        if (permission !== 'Manage' || isReadOnly() === "True") {
            controls.SelectBox = permission == 'Close' ? '' : 'disabled';
            controls.AssignLink = 'disabled';
            controls.Category = 'disabled';
            controls.CloseLink = permission == 'Close' ? '' : 'disabled';
            controls.InternalNotes = 'disabled';
            controls.Subject = 'disabled';
            controls.ReactivateLink = 'disabled';
        }
        // permission === 'Close'?

        if (permission === 'None') {

        }

        if (status === Const.ConditionStatus.Closed) {
            controls.SelectBox = 'disabled';
            controls.AssignLink = 'disabled';
            controls.Category = 'disabled';
            controls.InternalNotes = 'disabled';
            controls.Subject = 'disabled';
            controls.CloseLink = 'hidden';
            controls.AssociatedDocsLink = 'disabled';
        }
        else if (status === "Resolved") {
            controls.AssignLink = 'hidden';
        }
        else { //status === "Active"
            controls.ReactivateLink = 'hidden';
        }

        var controlNames = _controlNames;
        var classesToHide = [], classesToDisable = [], i = controlNames.length - 1;
        do {
            controlStatus = controls[controlNames[i]];
            if (controlStatus === 'disabled') {
                classesToDisable.push(_controlIdentifiers[i]);
            }
            else if (controlStatus === 'hidden') {
                classesToHide.push(_controlIdentifiers[i]);
            }
        } while (i--);

        hide($row, classesToHide);
        disable($row, classesToDisable, permission);
    }

    // Create the row using the data and template
    function populateRow($row, condition) {
        if (condition) {

            var categories = $.extend([], options.Categories);

            if (categories.filter(function (e) {
                return e.Id == condition.CategoryId;
            }).length == 0) {
                categories.push({
                    Category: condition.Category,
                    Id: condition.CategoryId
                });
            }

            $.extend(condition,
                    {
                        isDirty: false,
                        changes: {},
                        Resources: options.Resources,
                        Categories: categories,
                        ShowHiddenData: options.ShowHiddenData
                    });

            populateAssociatedDocs(condition);

            $row.empty().data(CONDITION_DATA_NODE, condition);

            // Make the condition data div-friendly
            condition.SubjectText = encodeHtml(condition.Subject);
            condition.InternalNotesText = encodeHtml(condition.InternalNotes);

            var $template = $.tmpl("ConditionRow", condition); // tmpl will use the condition object to populate the template

            $template.appendTo($row);
        }
        else {
            alert("error populating row " + condition.Id);
        }
    }

    function populateAssociatedDocs(condition) {
        if (condition.Associations && condition.Associations.length > 0) {
            var $container = $('<div>'); // this contains each association description

            var isSatisfied = true; // first, assume all docs are satisfied
            var isUnsatisfied = false;

            for (var i = 0; i < condition.Associations.length; i++) {
                var assoc = condition.Associations[i];

                var $status = $('<span>');

                if (assoc.DocumentStatus === Const.DocumentStatus.Rejected) {
                    // first, check the edoc status to see if it has been rejected
                    if (condition.Status !== Const.ConditionStatus.Closed)
                        $status.addClass('associatedDocsBad');

                    $status.text('Rejected')
                           .append('  ');
                    isSatisfied = false;
                    isUnsatisfied = true;
                } else if (assoc.Status === Const.AssociationStatus.Undefined) {
                    // if there is at least one undefined-status document, then the condition is not satisfied
                    isSatisfied = false;
                } else if (assoc.Status === Const.AssociationStatus.Satisfied) {
                    if (condition.Status !== Const.ConditionStatus.Closed)
                        $status.addClass('associatedDocsGood');

                    $status.text('Satisfied')
                           .append('  '); // two spaces
                } else if (assoc.Status === Const.AssociationStatus.Unsatisfied) {
                    if (condition.Status !== Const.ConditionStatus.Closed)
                        $status.addClass('associatedDocsBad');

                    $status.text('Not Satisfied')
                           .append('  ');

                    isSatisfied = false;
                    isUnsatisfied = true;
                }

                var $docDesc = $('<span>').text(assoc.DocumentFolderName + ': ' + assoc.DocumentTypeName);

                var $assoc = $('<div>').append($status)
                                       .append($docDesc);

                $container.append($assoc);
            }
            condition.AssociatedDocs = $container.html();

            // Set Highlights
            if (condition.Status === Const.ConditionStatus.Closed) {
                condition.AssociatedDocsClasses = 'associatedDocsCellDisabled';
            } else if (isSatisfied) {
                condition.AssociatedDocsClasses = 'associatedDocsCellGood';
            } else if (isUnsatisfied) {
                condition.AssociatedDocsClasses = 'associatedDocsCellBad';
            } else {
                condition.AssociatedDocsClasses = 'associatedDocsCellNeutral';
            }

            condition.AssociatedDocsLinkClass = 'associatedDocsLinkOpen';
            condition.AssociatedDocsDescription = 'Open Docs';
        } else { // else there are no associated docs
            condition.AssociatedDocs = "<span>None</span>";
            condition.AssociatedDocsLinkClass = 'associatedDocsLinkAssign';
            condition.AssociatedDocsDescription = 'Assign Docs';

            // Set Highlights
            if (condition.Status === Const.ConditionStatus.Closed)
                condition.AssociatedDocsClasses = 'associatedDocsCellDisabled';
            else
                condition.AssociatedDocsClasses = 'associatedDocsCellBad';
        }
    }

    var $dummyTextArea = $('<textarea>');

    function decodeEntity(sEntity) {
        $dummyTextArea.html(sEntity);
        return $dummyTextArea.val();
    }

    function decodeHtml(html) {
        return html
        //.replace(/\s+/g, " ")
            .replace(/<(?:\/(?:p))>\s*/gi, "\r\n") // p tag
            .replace(/<br>\s*/gi, "\r\n") // br tag
            .replace(/<(?:[^>'"]*|"[^"]*"|'[^']*')*>/gi, "") // ignore other tags... such as the font tag
            .replace(/&#?[a-z0-9]+;/gi, decodeEntity); // decode entities... such as &nbsp;
    }

    function encodeHtml(val) {
        var htmls = [];
        var lines = val.split(/\n/);
        for (var i = 0; i < lines.length; i++) {
            var line = lines[i];
            line = line.replace(/\n/g, '')
                       .replace(/\r/g, ''); // we're adding <br> elements, so we don't need these anymore
            htmls.push($('<div>').text(line).html());
        }
        return htmls.join("<br>");
    }

    function sortField_onfocus(e) {
        var $this = $(this);
        $this.data('before', $this.html()); // store the data before the edit, so we can check if something actually changed
        return $this;
    }

    function sortField_onchange(e) {
        var $this = $(this), $tr = $this.parents('tr'); data = $tr.data(CONDITION_DATA_NODE);

        if ($this.data('before') !== null && $this.data('before') === $this.html()) { // we may get a change event even though the data's not dirty
            return;
        }

        data.isDirty = true;
        if ($this.is('select.categories')) {
            data.changes.CategoryId = $this.val();
        }
        else if ($this.is('.subject')) { // these are special divs with the contenteditable attribute
            data.changes.Subject = decodeHtml($this.html())
        }
        else if ($this.is('.internalNotes')) { // these are special divs with the contenteditable attribute
            data.changes.InternalNotes = decodeHtml($this.html())
        }
        requiresSortUpdate = true;
        setPageDirty(true);
    }

    // Open Docs
    function openDoc_onclick(e) {
        if (options.Permissions.CanEditEdocs === false || options.Permissions.AllowEditingApprovedEDocs === false) {
            alert("This requires the 'Allow editing EDocs' and 'Allow editing protected EDocs' permissions. Please contact your account administrator for more information.");
            return;
        }
        var $this = $(this), data = $this.parents('tr').data(CONDITION_DATA_NODE);

        var result = window.open(VRoot + '/newlos/ElectronicDocs/SignOffEditor.aspx?loanid=' + data.LoanId + '&conditionid=' + data.Id, null, 'toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes');
        // the signoff editor itself will handle reloading, using window.opener
    }

    // Assign Docs
    function assignDoc_onclick(e) {
        var $this = $(this), data = $this.parents('tr').data(CONDITION_DATA_NODE);
        showModal('/newlos/ElectronicDocs/EdocPicker.aspx?loanid=' + data.LoanId + '&taskid=' + data.Id, null, null, null, function(windowResult) {
                if (windowResult && windowResult.DocIds && windowResult.DocIds.length > 0) {
                    gService.ConditionService.callAsyncSimple("SaveAssociations", { 'sLId': data.LoanId, 'TaskId': data.Id, 'DocIds': JSON.stringify(windowResult.DocIds) }, assignDoc_onclickCallback);
                }
            },
            { hideCloseButton: true });
    }

    function assignDoc_onclickCallback() {
        savePage(reloadPage);
    }

    function field_onchange(e) {
        $(this).parents('tr').data(CONDITION_DATA_NODE).isDirty = true;
        setPageDirty(true);
    }

    // For use with tablesorter
    function textExtractor(node) {
        node = $(node);

        var probe = null;

        // Figure out what part of the node we want to sort on
        if (node.hasClass('numberCell')) {
            // Find the child that is NOT hidden. Necessary because some lenders may be using static condition IDs.
            probe = node.children('span').not(':hidden').text();
        }
        else if (node.hasClass('statusCell')) {
            var status = node.children('span').text();
            if (status === Const.ConditionStatus.Closed) {
                probe = '1';
            } else {
                probe = '0';
            }
        }
        else if (node.hasClass('categoryCell')) {
            probe = node.find('select option:selected').text();
        }
        else if (node.hasClass('subjectCell')) {
            probe = decodeHtml(node.html());
        }
        else if (node.hasClass('internalNotesCell')) {
            probe = decodeHtml(node.html());
        }
        else if (node.hasClass('associatedDocsCell')) {
            probe = decodeHtml(node.children('div').html());
        }

        if (probe === null) {
            return '';
        }
        return probe;
    }

    // This will allow the header to stay with the scrollbar by creating a clone
    // of the existing header. We will still need to mess with this header to get
    // the onclick binding working.
    function initFixedHeader() {
        var $conditions = $('#conditions');
        var headerOffset = $conditions.offset().top;

        var $colgroupClone = $('#conditions > colgroup').clone();

        var $tableHeader = $('#conditions > thead');
        var $tableHeaderClone = $tableHeader.clone();

        $tableHeader.find('th').each(function() {
            $(this).addClass('realHeader');
        });
        $tableHeaderClone.find('th').each(function() {
            $(this).addClass('cloneHeader');
        });

        var $fixedHeaderTable = $('#fixedHeaderTable');
        $fixedHeaderTable.append($colgroupClone);
        $fixedHeaderTable.append($tableHeaderClone);

        $tableHeader.hide();
    }

    // Bind the columns in the fixed header to correspond with those of the
    // condition table.
    function bindFixedHeader() {
        var $fixedHeaderTable = $('#fixedHeaderTable');
        var $realHeaderTable = $('#conditions');

        // Have the clone headers click on the real headers
        function rewireHeader(name) {
            $realHeader = $(name + ".realHeader"); // take the intersection of the 2 classes, so no space
            $cloneHeader = $(name + ".cloneHeader");

            // If the original header has no link, nothing to do here
            var $realLink = $realHeader.find('.headerLink')
            if ($realLink.length === 0) {
                return;
            }

            // Otherwise, wire up the clone link to click on the real link
            var $cloneLink = $cloneHeader.find('.headerLink');
            $cloneLink.click(function() { $realLink.click(); });
        }

        rewireHeader(".checkboxHeader");
        rewireHeader(".numberHeader");
        rewireHeader(".statusHeader");
        rewireHeader(".categoryHeader");
        rewireHeader(".subjectHeader");
        rewireHeader(".internalNotesHeader");
        rewireHeader(".associatedDocsHeader");
    }

    // Initialize tablesorter, call the initial sort, bind the fixed header
    function initSorting() {
        $tbody.sortable({
            axis: 'y',
            handle: '.grippy',
            placeholder: 'force',
            start: function(e, ui) {
                ui.placeholder.html('<td colspan=' + ui.item.children().length + '>&nbsp;</td>');
            },
            stop: function(e, ui) {
                reorder();
            }
        });

        $table.tablesorter({
            textExtraction: textExtractor,
            cssHeader: 'sortableHeader',
            headers: {
                0: { sortable: false, sorter: "number" }, //checkbox
                1: { sortable: true, sorter: "number" }, //number
                2: { sortable: false, sorter: "text" }, //status
                3: { sortable: true, sorter: "text" }, //category
                4: { sortable: true, sorter: "text" }, //subject
                5: { sortable: true, sorter: "text" }, //internal notes
                6: { sortable: true, sorter: "text" }, //associated docs
                7: { sortable: false, sorter: "text"} //associated docs link
            },
            sortForce: [[2, 0]]
        });

        $table.on('sortEnd', function() { reorder(); });

        $table.trigger('sorton', [[[2, 0], [3, 0]]]); // sort on status and category
    }

    function init($elem, configData, conditions) {
        $.extend(options, configData); // join the options variable with configData
        $table = $elem;
        table = $elem[0];
        tbody = table.tBodies[0];
        $tbody = $(tbody).detach();
        $(options.RowTemplateSelector).template("ConditionRow");

        // Save button
        $save = $('#saveBtn');
        $save.prop('disabled', true);
        $save.on('click', save_onclick);

        $('.BatchOperationButton').prop('disabled', true);

        // Add the rows
        var isAlt = false;
        var i;
        for (i = 0; i < conditions.length; i++) {
            var condition = conditions[i];
            condition.number = i + 1;
            var $row = $('<tr>');
            populateRow($row, condition);

            // Style
            $row.removeClass(options.RowClass)
                .removeClass(options.Alternating_RowClass) // for each row, apply the appropriate class
                .addClass(isAlt ? options.Alternating_RowClass : options.RowClass)
                .find('span.number').text(i + 1); // then update the row number

            isAlt = !isAlt;

            // Status
            var data = $row.data(CONDITION_DATA_NODE);
            updateEditableControls($row, data.Status, data.Permission);
            $tbody.append($row);
        }

        // Add events
        $("th.stopClick", table).click(stopSort);
        $(".SelectAll").click( function() {
            $(".selectBox", table).filter(function() {
                return $(this).prop('disabled') != true;
            }).prop("checked", this.checked);
            toggleBatchButtons();
        });
        //setup event handling for the conditions
        $tbody.on("click", "a.reactivatelink", reactivate_onclick);
        $tbody.on("change", "select.categories", sortField_onchange);
        $tbody.on("focus", ".subject,.internalNotes", sortField_onfocus);
        $tbody.on("blur", ".subject,.internalNotes", sortField_onchange);
        $tbody.on("click", "a.closelink", close_onclick);
        $tbody.on("click", "a.associatedDocsLinkOpen", openDoc_onclick);
        $tbody.on("click", "a.associatedDocsLinkAssign", assignDoc_onclick);
        //$tbody.on("click", ".growboxcontainer", growboxcontainer_click);
        $tbody.on("click", '.selectBox', toggleBatchButtons);

        $table.append($tbody);

        //initFixedHeader();
        //making the table sortable takes a long time, so delay it till later
        window.setTimeout(function () {
            if ($tbody.find('tr').length > 0) {
                initSorting(); //dont bother will just throw an error.
                //bindFixedHeader();
            }
        }, 100);
    }

    // Returns jQuery collection of selected table rows.
    function getSelectedRows() {
        return $tbody.find('.selectBox')
            .filter(':checked')
            .closest('tr');
    }

    function toggleBatchButtons() {
        var $batchOperationButtons = $('.BatchOperationButton'),
            enableButtons = getSelectedRows().length > 0;

        if (!enableButtons) {
            $batchOperationButtons.css('backgroundColor', '');
        }

        $batchOperationButtons.prop('disabled', !enableButtons);
    }

    function batchClose() {
        var $selectedRows = getSelectedRows(),
            conditionChanges = [],
            managedConditionsWithoutRequiredDocAssociated = [];

        $selectedRows.each(function() {
            var $this = $(this),
                condition = $this.data(CONDITION_DATA_NODE),
                taskId = condition.Id;

            // If the condition does not have the required doc association and the user will be
            // able to close, confirm. If the user doesn't have permission to close they will be
            // notified via batch error window.
            if (!condition.ConditionFulfilsAssociatedDocs && condition.Permission === 'Manage') {
                managedConditionsWithoutRequiredDocAssociated.push({
                    Number: $this.find('.number,.CondRowId').not(':hidden').text(),
                    Subject: condition.Subject
                });
            }
            conditionChanges.push(concentrateChanges($this));
        });

        if (managedConditionsWithoutRequiredDocAssociated.length > 0) {
            var confirmMsg = 'The following conditions are missing an association with a required document type. Are you sure you want to sign off?\n';
            var conditionNumbers = $.map(managedConditionsWithoutRequiredDocAssociated, function(elementOfArray, indexInArray) {
                var conditionDesc = elementOfArray.Number + ': ' + elementOfArray.Subject.substr(0, 50);
                if (elementOfArray.Subject.length > 50) {
                    conditionDesc += '...'
                }

                return conditionDesc;
            });

            confirmMsg += conditionNumbers.join('\n');

            if (!confirm(confirmMsg)) {
                return;
            }
        }

        var args = {
            sLId: ML.sLId,
            ConditionChanges: JSON.stringify(conditionChanges)
        };

        // batch close and get all associations for given conditions
        gService.ConditionService.callAsyncSimple('BatchCloseConditions', args, function (serviceResult) {
            if (!serviceResult.error) {
                if (serviceResult.value.ErrorCacheId != null) {
                    var queryString = '?id=' + serviceResult.value.ErrorCacheId;
                    showModal('/newlos/Tasks/BatchTaskErrorSummary.aspx' + queryString, null, null, null, null, { hideCloseButton: true });
                }

                if (serviceResult.value.Conditions != null) {
                    var conditions = JSON.parse(serviceResult.value.Conditions);
                    $selectedRows.each(function (index, element) {
                        var $this = $(this),
                                condition = $this.data(CONDITION_DATA_NODE),
                                taskId = condition.Id,
                                pos = condition.number,
                                i;

                        for (i = 0; i < conditions.length; i++) {
                            if (taskId === conditions[i].Id) {
                                conditions[i].number = pos;
                                populateRow($this, conditions[i]);
                                updateEditableControls($this, conditions[i].Status, condition.Permission);
                                break;
                            }
                        }
                    });
                }
            }
            else {
                alert('Error: ' + serviceResult.UserMessage);
            }

            var pageIsDirty = false;
            $tbody.find('tr').each(function () {
                if ($(this).data(CONDITION_DATA_NODE).isDirty) {
                    pageIsDirty = true;
                    return false;
                }
            });
            setPageDirty(pageIsDirty);
        });
    }

    return {
        isPageDirty: function() { return isPageDirty(); },
        initialize: function($elem, configData, conditions) { return init($elem, configData, conditions); },
        addCondition: function(condition) {
            var $row = $('<tr>');
            populateRow($row, condition);
            $tbody.prepend($row);

            updateEditableControls($row, condition.Status, condition.Permission);

            if ($tbody.find('tr').length == 1) {
                initSorting();
                bindFixedHeader();
            }

            reorder();
            savePage(); // This will take care of sort order
        },
        beforeUnload: function() {
            if (isPageDirty()) {
                return UnloadMessage;
            }
            return;
        },
        save: function() { savePage(); },
        reload: function() { reloadPage(); },
        batchClose: function() { batchClose(); }
    };
} (jQuery));


(function($) {
    $(window).on("load", function() {
        var _readOnly = $("#_ReadOnly").val() === "True",
            _hiddenInfo = $("#CanViewHiddenInformation").val() === "True",
            loanid = $("#loanid").val(),
            isLoanTemplate = $("#IsLoanTemplate").val() === "True";

        var start = new Date().getTime();

        // Populate the table
        var table = $("#conditions");
        ConditionTable.initialize(table, {
            Categories: ConditionData.Categories,
            Permissions: ConditionData.Permissions,
            ShowHiddenData: _hiddenInfo,
            sLId: loanid,
            Resources: ConditionData.Resources,
            IsTemplate: isLoanTemplate
        }, ConditionData.Conditions);

        // Bind the AddCondition button
        $('#AddCondition').click(function() {
            var url = '/newlos/Tasks/TaskEditorV2.aspx?isCond=true&loanid=' + $('#loanid').val() + '&IsTemplate=';
            if (isLoanTemplate) {
                url += 'true';
            } else {
                url += 'false';
            }
            showModal(url, null, null, true/*forceEdge*/, function (args) {
                if (!args.taskId) {
                    return;
                }
                gService.ConditionService.callAsyncSimple("LoadCondition", { 'TaskId': args.taskId }, addConditionCallback);
            },
            { hideCloseButton: true });
        });

        function addConditionCallback(results) {
            if (results && results.value) {
                ConditionTable.addCondition(JSON.parse(results.value.Condition));
            }
        }

        // Bind the batch sign off button
        $('#CloseConditions').click(ConditionTable.batchClose);
    });
    window.onbeforeunload = ConditionTable.beforeUnload;
})(jQuery);


