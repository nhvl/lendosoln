﻿//self executing function stops us from polluting the global namespace.

(function($) {

    var CUSTOM_EVENTS = {
        UPDATE_DIRTY_BIT: 'UpdateDirtyBit',
        CONDITION_CHECKED: 'ConditionChecked',
        CONDITION_DIRTY_CHANGE: 'ConditionDirtyChange',
        CLOSE_SELECTED_CONDITIONS: 'CloseSelectedConditions',
        DELETED_SELECTED_CONDITIONS: 'DeleteSelectedConditions',
        SAVE_PAGE: 'SaveConditionPage',
        UPDATE_SORT_DATA: 'UpdateSortData',
        REORDER: 'ReorderCondition'
    };

    function alog(msg) {
        if (typeof (console) != 'object') {
            return;
        }
        if (typeof msg === 'object') {
            msg = JSON.stringify(msg);
        }

        console.log(msg + ' at ' + (new Date()));
    }

    var $dummyTextArea = $('<textarea>');

    function decodeEntity(sEntity) {
        $dummyTextArea.html(sEntity);
        return $dummyTextArea.val();
    }

    function decodeHtml(html) {
        return html
        //.replace(/\s+/g, " ")
            .replace(/<(?:\/(?:p))>\s*/gi, "\r\n") // p tag
            .replace(/<br>\s*/gi, "\r\n") // br tag
            .replace(/<(?:[^>'"]*|"[^"]*"|'[^']*')*>/gi, "") // ignore other tags... such as the font tag
            .replace(/&#?[a-z0-9]+;/gi, decodeEntity); // decode entities... such as &nbsp;
    }

    function encodeHtml(val) {
        var htmls = [];
        var lines = val.split(/\n/);
        for (var i = 0; i < lines.length; i++) {
            var line = lines[i];
            line = line.replace(/\n/g, '')
                       .replace(/\r/g, ''); // we're adding <br> elements, so we don't need these anymore
            htmls.push($('<div>').text(line).html());
        }
        return htmls.join("<br>");
    }

    function saveCallback(results, thisObject, callback) {
        if (results.value && results.value.Condition) {
            thisObject.rebuild(results.value.Condition);
        }
        else {
            if (results.UserMessage) {
                alert(results.UserMessage);
            }
            else if (results.value && results.value.Error) {
                alert(results.value.Error);
            }
            else {
                alert('error');
            }
            thisObject.$elem.trigger(CUSTOM_EVENTS.CONDITION_DIRTY_CHANGE);
        }

        if (typeof callback === 'function') {
            callback();
        }
    }

    function showAuditCallback(thisObject) {
        var url = '/newlos/Tasks/TaskViewer.aspx?taskid=' + thisObject.options.Id + '&loanid=' + thisObject.options.sLId + '&IsTemplate=';
        url += thisObject.options.IsTemplate ? 'true' : 'false';

        showModal(url, null, null, null, function (args) {
                thisObject.reload();
            }, { context: thisObject, hideCloseButton: true });
    }

    function reloadCallback(results, thisObject) {
        if (results.value && results.value.Condition) {
            thisObject.rebuild(results.value.Condition);
        }
        else {
            alert('error');
        }
    }

    function addConditionCallback(results) {
        if (results && results.value) {
            $('#conditions').ConditionTable('add', JSON.parse(results.value.Condition));
        }
    }

    function insertCallback(results) {
        if (!!results && !!results.value) {
            var position = (+$('#InsertPosition').val());
            var conditions = JSON.parse(results.value.NewTasks);
            var failures = JSON.parse(results.value.Failures);
            var hasDupes = results.value.HasDupes === 'True';

            $('#conditions').ConditionTable('addMulti', conditions, position);

            if (failures.length != 0) {
                var failureRow = [], currentRow = null;

                $.each(failures, function (i, o) {
                    currentRow = $('#ConditionChoices tbody .ChoiceId[value=' + o + ']').parents('tr');
                    failureRow.push(currentRow);
                    currentRow.css('background-color', '#FF9494');
                });
                alert('Failed to ' + (ids.length - conditions.length) + ' condition choice(s).');

                $.each(failureRow, function (i, row) {
                    row.css('background-color', '');
                });
            }

            $('#ConditionChoices').hide();
            if (hasDupes) {
                alert('One or more conditions that you selected were already on file. No duplicates were added.');
            }
        }
        else {
            alert('Failed to add 1 or more condition choices.');
            return;
        }
    }

    function importFromTemplateCallback(results) {
        if (results.value.OK) {
            var newConditions = JSON.parse(results.value.NewCondition);
            if (newConditions.length > 0) {
                $('#conditions').ConditionTable('addMulti', newConditions);
            }
        }
        else {
            alert('There was an error importing the conditions from the template');
        }
    }

    var Condition = {
        init: function(element, newoptions) {
            this.options = $.extend({}, newoptions);
            this.controls = {};
            this.changes = {};
            this.$elem = $(element);
            this.build();
            return this;
        },


        enable: function(controls) {
            $.each(controls, function(i, o) {
                o.prop('disabled', false);
                o.css('cursor', '');
            });
        },

        disable: function(controls) {
            var msg = this.options.Permission === 'None' ? 'You do not have permission to work on this task.' : "You do not have permission to manage this task. Click 'view' for more details.";
            //if we dont show hidden data it wont be here
            $.each(controls, function(i, o) {
                o.prop({ 'disabled': true, 'readonly': true });
                o.css('cursor', 'default');
                if (o.is('a')) {
                    o.attr({ 'title': msg });
                }
                if (o.is('div')) {
                    o.prop('contenteditable', false);
                }
            });
        },
        onfieldchange: function() {
            this.setDirty(true);
            this.$elem.trigger(CUSTOM_EVENTS.UPDATE_SORT_DATA);
        },

        isReadOnly: function() {
            return $('#_ReadOnly').val();
        },


        updateEdibleControls: function() {

            var options = this.options, status = this.changes.Status || options.Status, controls = this.controls;


            var actionControls = [
                    controls.AssignLink,
                    controls.Category,
                    controls.CloseLink,
                    controls.DeleteBox,
                    controls.DueDateLink,
                    controls.HiddenBox,
                    controls.InternalNotes,
                    controls.ReactivateLink,
                    controls.Subject
                ];

            this.enable(actionControls);

            if (status === "Closed") {
                this.disable([
                        controls.AssignLink,
                        controls.Category,
                        controls.DueDateLink,
                        controls.HiddenBox,
                        controls.InternalNotes,
                        controls.Subject
                ]);

                if (!options.isCurrentUserOwner && options.Permission != "Manage") {
                    this.disable([controls.DeleteBox]);
                }

                controls.ReactivateLink.show();
                $.each([controls.CloseLink, controls.DueDateLink, controls.AssignLink], function(i, o) {
                    o.hide();
                });
            }
            else if (status === "Resolved") {
                $.each([controls.ReactivateLink, controls.CloseLink, controls.DueDateLink], function(i, o) {
                    o.show();
                });

                controls.AssignLink.hide();
            }
            else { //status === "Active"
                controls.ReactivateLink.hide();
                $.each([controls.CloseLink, controls.AssignLink, controls.DueDateLink], function(i, o) {
                    o.show();
                });
            }

            if (options.Permission === 'Close') {
                this.disable([
                        controls.AssignLink,
                        controls.Category,
                // 3/11/14 gf - opm 170314, the delete box is now used for batch sign off as well.
                //controls.DeleteBox, 
                        controls.DueDateLink,
                        controls.HiddenBox,
                        controls.InternalNotes,
                        controls.Subject,
                        controls.ReactivateLink
                    ]);
            }
            if (options.Permission === 'WorkOn') {
                this.disable([
                        controls.Category,
                        controls.CloseLink,
                        controls.DeleteBox,
                        controls.DueDateLink,
                        controls.HiddenBox,
                        controls.InternalNotes,
                        controls.Subject,
                        controls.ReactivateLink
                    ]);
            }

            if (options.Permission === 'None') {
                this.disable([controls.AuditLink]);
            }

            if (this.isReadOnly() === "True") {
                $(".grippy, .moveContainer").hide();

                this.disable([
                        controls.AssignLink,
                        controls.Category,
                        controls.CloseLink,
                        controls.DeleteBox,
                        controls.DueDateLink,
                        controls.HiddenBox,
                        controls.InternalNotes,
                        controls.Subject,
                        controls.ReactivateLink
                    ]);
            }
        },

        build: function(number) {
            var options = this.options, $elem = this.$elem, elem = $elem[0], condition = this, controls = this.controls, tds = $('td', elem), onDirtyChange = function() { condition.onfieldchange() };
            var onEditableSubjectDivBlur = function() {
                if (condition.options.Subject !== null && condition.options.Subject === this.innerText)  // we may get a change event even though the data's not dirty
                    return;
                condition.onfieldchange()
                condition.options.Subject = this.innerText;
            };
            var onEditableInternalNotesDivBlur = function() {
                if (condition.options.InternalNotes !== null && condition.options.InternalNotes === this.innerText)  // we may get a change event even though the data's not dirty
                    return;
                condition.onfieldchange()
                condition.options.InternalNotes = this.innerText;
            };

            var onEditableSubjectDivFocus = function(event) {
                event.stopPropagation();
                condition.options.Subject = this.innerText;
            };
            var onEditableInternalNotesDivFocus = function(event) {
                event.stopPropagation();
                condition.options.InternalNotes = this.innerText;
            };

            var onContainerClick = function() {
                this.lastChild.focus();
                this.lastChild.innerText = this.lastChild.innerText;
            }

            if (tds.length === 0) {
                $.tmpl('ConditionRow', options).appendTo(elem);
                tds = $('td', elem);
            }
            //TD0 Select Box
            controls.DeleteBox = $('input.deleteBox', tds[0]).click(function() {
                $elem.trigger(CUSTOM_EVENTS.CONDITION_CHECKED);
            });
            //TD1 Number
            controls.Number = $('.number', tds[1]);
            if (number) {
                this.controls.Number.text(number);
            }
            //TD2 Reorder
            $('.moveArrow', tds[2]).click(function() {
                var triggerEvent = '', anchor = $(this);

                $('input[type="checkbox"]', elem).each(function(i, input) {
                    if (typeof (input.defaultChecked) != 'undefined') {
                        //if this is not set all the checkboxes lose their checked status on move
                        input.defaultChecked = input.checked;
                    }
                });

                if (anchor.hasClass('Up')) {
                    triggerEvent = 'MoveRowUp';
                } else if (anchor.hasClass('Top')) {
                    triggerEvent = 'MoveRowTop';
                } else if (anchor.hasClass('Bottom')) {
                    triggerEvent = 'MoveRowBottom';
                } else if (anchor.hasClass('Down')) {
                    triggerEvent = 'MoveRowDown';
                } else {
                    alert('error missing movearrow event');
                    return;
                }
                anchor.trigger('mouseout');
                $elem.trigger(triggerEvent, elem);

                return false;
            });
            //TD3 Audit
            controls.AuditLink = $('a.audit', tds[3]).click(function() {
                if (this.disabled) {
                    return false;
                }
                condition.showAudit();
                return false;
            });
            //TD4 Status
            controls.ReactivateLink = $('a.reactivatelink', tds[4]).click(function() {
                if (this.disabled) {
                    return false;
                }
                condition.reactivate();
                return false;
            });

            controls.CloseLink = $('a.closelink', tds[4]).click(function() {

                if (this.disabled) {
                    return false;
                }

                condition.close();
                return false;
            });

            //TD5 Closed

            //TD6 CAtegory
            controls.Category = $('select.categories', tds[6]).change(onDirtyChange);
            //TD7 ConditionSubject
            controls.Subject = $('div.subject', tds[7]).blur(onEditableSubjectDivBlur);
            $('div.subject', tds[7]).focus(onEditableSubjectDivFocus);
            $('div.subject', tds[7]).parent().focus(onContainerClick);
            //TD8 Internal Notes
            controls.InternalNotes = $('div.internalNotes', tds[8]).blur(onEditableInternalNotesDivBlur);
            $('div.internalNotes', tds[8]).focus(onEditableInternalNotesDivFocus);
            $('div.internalNotes', tds[8]).parent().focus(onContainerClick);
            //TD9 AssignedTo
            controls.AssignTo = $('span.assignedToText', tds[9]);
            controls.AssignLink = $('a.assignlink', tds[9]).click(function() {
                if (this.disabled) {
                    return false;
                }
                condition.assign();
                return false;
            });

            //TD10 DueDate
            controls.DueDateText = $('span', tds[10]);
            controls.DueDateLink = $('a', tds[10]).click(function() {
                if (this.disabled) {
                    return false;
                }
                condition.calculateDueDate();
                return false;
            });

            //TD11 Hidden  optional
            controls.HiddenBox = $('td > input.isHidden', elem).click(function() { condition.setDirty(true); });

            if (this.options.Status === 'Closed') {
                $('span.closeByLabel', elem).show();
            }

            this.updateEdibleControls();
        },

        updateNumber: function(number) {

            this.controls.Number.html(number);
        },

        // This will return either the relative number or the static condition row id.
        getDisplayedNumber: function() {
            return this.$elem.find('.number,.CondRowId').not(':hidden').text();
        },

        resetChanges: function() {
            var changes = this.changes;
            changes.AssignedUserId = null;
            changes.AssignedRoleId = null;
            changes.DueDateField = null;
            changes.DueDateBuisnessDays = null;
        },
         setDirty: function(isDirty) {
            this.isDirty = isDirty;
            this.$elem.trigger(CUSTOM_EVENTS.CONDITION_DIRTY_CHANGE);
        },

        resetDirtyStatus: function() {
            this.isDirty = false;
            this.resetChanges();
        },

        getDirty: function() {
            return !!this.isDirty;
        },

        isSelected: function(fn) {
            return fn.call(this, this.controls.DeleteBox.is(':checked'));
        },

        setSelected: function(selected) {
            if (this.controls.DeleteBox.is(':disabled')) {
                return;
            }
            this.controls.DeleteBox.prop('checked', selected);
        },


        serialize: function(f) {
            var options = this.options, controls = this.controls, changes = this.changes;

            var d = $.extend({}, changes, {
                CategoryId: controls.Category.val(),
                Subject: decodeHtml(controls.Subject[0].innerHTML.replace(/\r|\r?\n/g, "\r\n")), // text() seems to strip out \n on <IE9, add it back in
                TaskId: options.Id,
                Permission: options.Permission,
                InternalNotes: decodeHtml(controls.InternalNotes[0].innerHTML.replace(/\r|\r?\n/g, "\r\n")),
                IsDirty: this.isDirty,
                ResolutionBlockTriggerName: options.ResolutionBlockTriggerName,
                ResolutionDateSetterFieldId: options.ResolutionDateSetterFieldId
            });



            if (options.ShowHiddenData) {
                d.IsHidden = controls.HiddenBox.is(':checked');
            }

            if (f) {
                f.call(this, d);
            }
            return d;
        },

        rebuild: function(newData) {
            var condition = typeof newData === 'string' ? JSON.parse(newData) : newData, number = $('.number', this.$elem).text();
            $.extend(this.options, condition); //reset the options since we just reloaded
            this.resetChanges();
            this.setDirty(false);
            this.$elem.empty();
            this.build(number);
            var category = this.controls.Category;
            if (category.val() != condition.CategoryId) {
                $('<option>').text(condition.Category).attr({ 'value': condition.CategoryId }).prependTo(category);
                category.val(condition.CategoryId);
            }
            this.$elem.trigger(CUSTOM_EVENTS.UPDATE_SORT_DATA).trigger(CUSTOM_EVENTS.CONDITION_CHECKED);
        },

        save: function(methodName, callback) {
            var data = {
                ConditionChanges: JSON.stringify(this.serialize()),
                TaskId: this.options.Id,
                sLId: this.options.sLId
            };

            var thisObject = this;

            gService.ConditionService.callAsyncSimple(methodName, data, function (results) { saveCallback(results, thisObject, callback); });
        },

        showAudit: function () {
            var thisObject = this;
            if (this.getDirty() && $('#LoanIsNotWriteable').val() !== 'True' && confirm('This condition has changed. Do you want to save?')) {
                this.save("SaveCondition", function () { showAuditCallback(thisObject); });
            }
            else {
                showAuditCallback(this);
            }
        },

        getTaskId: function() {
            return this.options.Id;
        },

        assign: function() {
            var url = ["/newlos/Tasks/RoleAndUserPicker.aspx?loanid=", this.options.sLId, '&PermLevel=', this.options.PermissionLevelId, '&IsTemplate='];
            if (this.options.IsTemplate) {
                url.push('true');
            }
            else {
                url.push('false');
            }
            showModal(url.join(''), null, null, null, function(args){
                if (args.OK) {
                    this.controls.AssignTo.text(args.name);
                    if (args.type == 'user') {
                        this.changes.AssignedUserId = args.id;
                        this.changes.AssignedRoleId = null;
                    } else {
                        this.changes.AssignedUserId = null;
                        this.changes.AssignedRoleId = args.id;
                    }
                    this.$elem.trigger(CUSTOM_EVENTS.UPDATE_SORT_DATA);
                    this.setDirty(true);
                }
            }, { context: this, hideCloseButton: true })
        },

        calculateDueDate: function() {
            var url = '/newlos/Tasks/DateCalculator.aspx?loanid=' + this.options.sLId;
            if (false == this.options.IsDueDateLocked) {
                url += '&duration=' + this.options.DueDateFieldOffset + '&fieldId=' + this.options.DueDateFieldId;
            }
            showModal(url, null, null, null, function(args){ 
                if (args.OK) {
                    this.changes.DueDateField = args.id;
                    this.options.DueDateFieldId = args.id;
                    this.changes.DueDateBuisnessDays = args.offset;
                    this.options.DueDateFieldOffset = args.offset;
                    this.controls.DueDateText.text(args.date || args.dateDescription);
                    this.setDirty(true);
                    this.$elem.trigger(CUSTOM_EVENTS.UPDATE_SORT_DATA);
    
                }
            }, { context: this, hideCloseButton: true })
        },





        reactivate: function() {
            this.save("ReactivateCondition");
        },


        close: function() {
            if (!this.options.ConditionFulfilsAssociatedDocs) {
                if (this.options.Permission === 'Manage') {
                    if (!confirm('This condition is missing an association with a required document type. Are you sure you want to sign off?')) {
                        return;
                    }
                }
                else {
                    alert('This condition is missing an association with a required document type. Please associate with the required document type in order to sign off.');
                    return;
                }
            }
            this.save('CloseCondition');
        },


        reload: function () {
            var thisObject = this;
            gService.ConditionService.callAsyncSimple("LoadCondition", { 'TaskId': this.options.Id }, function (results) { reloadCallback(results, thisObject); });
        }
    };




    var ConditionTable = {

        alternate: false, //if we use another instance of this need to remove this field since its value will persist

        length: function() {
            return $('tbody > tr', this.$elem).length;
        },

        //will be true when conditions are deleted or re arranged
        setDirty: function(dirty) {
            this.isTableDirty = dirty;
        },

        getDirty: function(fn) {
            if (fn) {
                fn(!!this.isTableDirty);
                return;
            }
            return !!this.isTableDirty;
        },



        init: function(element, options) {

            this.options = $.extend({
                ShowHiddenData: false,
                RowTemplateSelector: '#condition_template',
                RowClass: 'GridItem',
                Alternating_RowClass: 'GridAlternatingItem'
            }, options);

            this.elem = element;
            this.$elem = $(element);
            this.build();
            this.deletedConditions = [];
            this.setDirty(false);
            this.$tbody = this.$elem.children('tbody');
            this.requiresTableUpdate = false;
            //cache the condition template
            $(this.options.RowTemplateSelector).template('ConditionRow');
            //this.$tbody.on("blur", ".subject,.internalNotes", sortField_onchange);
            return this;
        },

        addImpl: function(data) {
            var rowClass = this.alternate ? 'GridAlternatingItem' : 'GridItem', options = this.options;
            this.alternate = !this.alternate;
            $.extend(data, {
                'ShowHiddenData': options.ShowHiddenData,
                'readonly': options.readonly,
                'Categories': options.Categories,
                'sLId': options.sLId,
                'Resources': options.Resources,
                'IsTemplate': options.IsTemplate
            });

            var row = $('<tr></tr>').addClass(rowClass);
            row.Condition(data);
            $('div.subject', row).html(encodeHtml(data.Subject));
            $('div.internalNotes', row).html(encodeHtml(data.InternalNotes));
            return row;
        },

        add: function(data) {
            row = this.addImpl(data);
            this.$tbody.append(row);
            this.$elem.trigger(CUSTOM_EVENTS.UPDATE_SORT_DATA).trigger(CUSTOM_EVENTS.REORDER);
        },

        addMulti: function(data, startPos) {
            var table = this, tbody = this.$tbody, curRows = $('tr', tbody), maxPosition = curRows.length + 1, newRows = [];
            $.each(data, function(i, o) {
                newRows.push(table.addImpl(o));
            });

            if (!!startPos && startPos > 0 && startPos < maxPosition) {
                $.each(newRows, function(i, o) {
                    o.insertBefore(curRows[startPos - 1]);
                });
            }
            else {
                $.each(newRows, function(i, o) {
                    tbody.append(o);
                });
            }

            this.$elem.trigger(CUSTOM_EVENTS.UPDATE_SORT_DATA).trigger(CUSTOM_EVENTS.REORDER);
        },

        setupInitialConditions: function(data) {
            var table = this, tbody = this.$tbody, alt = false, options = this.options;

            tbody.children('tr').each(function(i, o) {

                var conditionData = $.extend({}, data[i], {
                    ShowHiddenData: options.ShowHiddenData,
                    readonly: options.readonly,
                    Categories: options.Categories,
                    sLId: options.sLId,
                    Resources: options.Resources,
                    IsTemplate: options.IsTemplate
                });

                $(o).Condition(conditionData).addClass(alt ? 'GridAlternatingItem' : 'GridItem');
                alt = !alt;
            });
        },

        build: function() {
            var table = this, elem = this.elem, $elem = this.$elem, options = this.options;
            //needed for tablesorter -- trying to sort columns   
            $('th.stopClick', elem).click(function(e) {
                e.stopImmediatePropagation();
                e.stopPropagation();
                return false;
            });

            $('input.SelectAll', elem).click(function(e) {
                e.stopImmediatePropagation();
                e.stopPropagation();
                $('tbody > tr', elem).Condition('setSelected', this.checked);
                $elem.trigger(CUSTOM_EVENTS.CONDITION_CHECKED);
            });

            $elem.bind(CUSTOM_EVENTS.SAVE_PAGE, function(e) {

                var saveData = {
                    sLId: table.options.sLId,
                    DeletedConditions: JSON.stringify(table.deletedConditions),
                    ConditionData: null
                }, conditions = [];


                var trConditions = $('tbody > tr', elem);
                var sortOrder = [];
                trConditions.each(function() {
                    var c = $(this).data('Condition');
                    sortOrder.push(c.getTaskId());

                    if (c.getDirty()) {
                        conditions.push(c.serialize());
                    }
                });
                saveData.ConditionData = JSON.stringify(conditions);
                saveData.SortOrder = JSON.stringify(sortOrder);
                gService.ConditionService.callAsyncSimple('SavePage', saveData, function (results) {
                    $("#dialog-message").hide();
                    if (results.error) {
                        alert(results.UserMessage);
                        table.setDirty(true);
                    }
                    else if (!!results.value.ErrorCacheId) {
                        showModal('/newlos/Tasks/BatchTaskErrorSummary.aspx?id=' + results.value.ErrorCacheId, null, null, null, function (args) {
                            // Clear dirty bit to prevent save prompt before reload.
                            table.setDirty(false);
                            trConditions.Condition('resetDirtyStatus');
                            $elem.trigger(CUSTOM_EVENTS.UPDATE_DIRTY_BIT);

                            window.location.reload();
                        },
                        { hideCloseButton: true });

                        return;
                    }
                    else {
                        table.setDirty(false);
                        trConditions.Condition('resetDirtyStatus');
                        table.deletedConditions = [];
                    }

                    $elem.trigger(CUSTOM_EVENTS.UPDATE_DIRTY_BIT);
                });
            });

            $elem.bind(CUSTOM_EVENTS.DELETED_SELECTED_CONDITIONS, function(e) {
                var insufficientPermissionTaskNums = [];
                var itemsRemoved = $('tbody > tr', elem).filter(function() {
                    var include = false;
                    $(this).Condition('isSelected', function(selected) {
                        include = selected && this.options.Permission === 'Manage';
                        if (include) {
                            table.deletedConditions.push(this.options.Id);
                        }
                        else if (selected) {
                            var displayedTaskNum = parseInt(this.getDisplayedNumber(), 10);
                            insufficientPermissionTaskNums.push(displayedTaskNum);
                        }
                    });
                    return include;
                }).remove().length;
                if (itemsRemoved > 0) {
                    table.setDirty(true);
                    $elem.trigger(CUSTOM_EVENTS.UPDATE_DIRTY_BIT).trigger(CUSTOM_EVENTS.CONDITION_CHECKED).trigger(CUSTOM_EVENTS.UPDATE_SORT_DATA).trigger(CUSTOM_EVENTS.REORDER);
                }

                var numPermissionErrors = insufficientPermissionTaskNums.length;
                if (numPermissionErrors > 0) {
                    var insufficientPermissionMsg = 'You do not have the required permissions to delete the following condition';
                    insufficientPermissionMsg += (numPermissionErrors > 1 ? 's' : '') + ': ';
                    insufficientPermissionMsg += insufficientPermissionTaskNums.join(', ');
                    alert(insufficientPermissionMsg);
                }
            });

            $elem.bind(CUSTOM_EVENTS.CLOSE_SELECTED_CONDITIONS, function(e) {
                var conditionChanges = [];
                var managedConditionsWithoutRequiredDocAssociated = [];

                var $rows = $('tbody > tr', elem);
                var $rowsToSignOff = $rows.filter(function() {
                    var include = false;
                    $(this).Condition('isSelected', function(isSelected) {
                        include = isSelected;
                        if (isSelected) {
                            // If the condition does not have the required doc association and the user will be 
                            // able to close, confirm. If the user doesn't have permission to close they will be
                            // notified via batch error window.
                            if (!this.options.ConditionFulfilsAssociatedDocs && this.options.Permission === 'Manage') {
                                managedConditionsWithoutRequiredDocAssociated.push({
                                    Number: this.getDisplayedNumber(),
                                    Subject: this.options.Subject
                                });
                            }
                            conditionChanges.push(this.serialize());
                        }
                    });
                    return include;
                });

                if (managedConditionsWithoutRequiredDocAssociated.length > 0) {
                    var confirmMsg = 'The following conditions are missing an association with a required document type. Are you sure you want to sign off?\n';
                    var conditionNumbers = $.map(managedConditionsWithoutRequiredDocAssociated, function(elementOfArray, indexInArray) {
                        var conditionDesc = elementOfArray.Number + ': ' + elementOfArray.Subject.substr(0, 50);
                        if (elementOfArray.Subject.length > 50) {
                            conditionDesc += '...'
                        }

                        return conditionDesc;
                    });

                    confirmMsg += conditionNumbers.join('\n');

                    if (!confirm(confirmMsg)) {
                        return;
                    }
                }

                var args = {
                    sLId: ML.sLId,
                    ConditionChanges: JSON.stringify(conditionChanges)
                };

                gService.ConditionService.callAsyncSimple('BatchCloseConditions', args, function (serviceResult) {
                    if (!serviceResult.error) {
                        // Prep and bring up the error modal
                        if (serviceResult.value.ErrorCacheId != null) {
                            var queryString = '?id=' + serviceResult.value.ErrorCacheId;
                            showModal('/newlos/Tasks/BatchTaskErrorSummary.aspx' + queryString, null, null, null, null, { hideCloseButton: true });
                        }

                        if (serviceResult.value.Conditions != null) {
                            var conditions = JSON.parse(serviceResult.value.Conditions);
                            $rowsToSignOff.each(function (index, element) {
                                var condition = $(this).data('Condition'),
                                    taskId = condition.getTaskId(),
                                    i;

                                for (i = 0; i < conditions.length; i++) {
                                    if (taskId === conditions[i].Id) {
                                        condition.rebuild(conditions[i]);
                                        break;
                                    }
                                }
                            });
                        }
                    } else {
                        alert('Error: ' + serviceResult.UserMessage);
                    }
                });
            });

            $elem.bind(CUSTOM_EVENTS.UPDATE_DIRTY_BIT, function(e) {
                var sv = $('#saveBtn');

                var returnNow = false;
                if (table.getDirty()) {
                    if ($('#LoanIsNotWriteable').val() !== 'True') {
                        sv.prop('disabled', false);
                    }

                    return;
                }
                $('tbody  tr', elem).each(function(i, o) {
                    if ($(o).data('Condition').getDirty()) {
                        if ($('#LoanIsNotWriteable').val() !== 'True') {
                            sv.prop('disabled', false);
                        }
                        returnNow = true;

                        return false;
                    }

                });
                if (returnNow) {
                    return;
                }

                sv.prop('disabled', true);
            });


            $elem.bind(CUSTOM_EVENTS.CONDITION_DIRTY_CHANGE, function(e) {
                $elem.trigger(CUSTOM_EVENTS.UPDATE_DIRTY_BIT);
            });


            var tBody = $('tbody', elem);

            tBody.sortable({
                axis: 'y',
                handle: '.grippy',
                placeholder: 'force',
                start: function(e, ui) {
                    ui.placeholder.html('<td colspan=' + ui.item.children().length + '>&nbsp;</td>');
                },
                stop: function(e, ui) {
                    table.setDirty(true);
                    tBody.trigger(CUSTOM_EVENTS.UPDATE_DIRTY_BIT);
                    $elem.trigger(CUSTOM_EVENTS.REORDER);
                }
            });

            //setup the table for row up movement
            $elem.bind('MoveRowTop MoveRowUp', function(e, row) {
                var rowIndex = row.sectionRowIndex, newRowIndex = 0;
                //at top do nothing
                if (rowIndex <= 0) {
                    return;
                }
                if (e.type === 'MoveRowUp') {
                    newRowIndex = rowIndex - 1;
                }
                moveRowPoly(tBody[0],rowIndex, newRowIndex);
                table.setDirty(true);
                $elem.trigger(CUSTOM_EVENTS.UPDATE_DIRTY_BIT);
                $elem.trigger(CUSTOM_EVENTS.REORDER);
            });

            //setup the table for row down movement
            $elem.bind('MoveRowBottom MoveRowDown', function(e, row) {
                var rowIndex = row.sectionRowIndex, newRowIndex = tBody[0].rows.length - 1;
                //at bottom nothing to do;
                if (rowIndex == newRowIndex) {
                    return;
                }

                if (e.type === 'MoveRowDown') {
                    newRowIndex = rowIndex + 1;
                }
                moveRowPoly(tBody[0],rowIndex, newRowIndex);
                table.setDirty(true);
                $elem.trigger(CUSTOM_EVENTS.UPDATE_DIRTY_BIT);
            });

            $elem.bind(CUSTOM_EVENTS.UPDATE_SORT_DATA, function() {
                this.requiresTableUpdate = true;
            });

            $elem.bind(CUSTOM_EVENTS.REORDER, function() {
                var isAlt = false;
                $('tr', tBody).each(function(i, o) {
                    $(o).Condition('updateNumber', i + 1).removeClass('GridItem GridAlternatingItem').addClass(isAlt ? 'GridAlternatingItem' : 'GridItem');
                    isAlt = !isAlt;
                });
            });

            $elem.bind("sortStart", function() {
                if (this.requiresTableUpdate) {
                    $elem.trigger('update');
                    this.requiresTableUpdate = false;
                }
            });

            //tablesorter triggers this when its done sorting  since we have no notion of rank  in the ui 
            //we have to always flag as dirty if they sort. 
            $elem.bind("sortEnd", function() {
                table.setDirty(true);
                $elem.trigger(CUSTOM_EVENTS.UPDATE_DIRTY_BIT).trigger(CUSTOM_EVENTS.REORDER);
            });

            $elem.tablesorter({
                textExtraction: this.textExtractor,
                headers: {
                    0: { sortable: false, sorter: "text" }, //cb
                    1: { sortable: false, sorter: "text" }, //number
                    2: { sortable: false, sorter: "text" }, //reorder
                    3: { sortable: false, sorter: "text" }, //audit
                    4: { sortable: true, sorter: "text" }, //status
                    5: { sortable: true, sorter: "shortDate" }, //closed
                    6: { sortable: true, sorter: "text" }, //category
                    7: { sortable: true, sorter: "text" }, //subject
                    8: { sortable: true, sorter: "text" }, //internal notes
                    9: { sortable: true, sorter: "text" }, //Assigned to 
                    10: { sortable: !this.options.ShowHiddenData, sorter: "text" }, //is hidden 
                    11: { sortable: true, sorter: "shortDate" }, //due date
                    12: { sortable: true, sorter: "shortDate"} //created date 
                }
            });
        },

        getSortOrder: function() {
            var trConditions = $('tbody > tr', this.elem);
            var sortOrder = [];

            trConditions.each(function () {
                var c = $(this).data('Condition');
                sortOrder.push(c.getTaskId());
            });

            return sortOrder;
        },

        textExtractor: function(node) {
            node = $(node);

            if (node.hasClass('statusCell')) {
                return node.children('span').text();
            }
            if (node.hasClass('closeCell')) {
                return node.children('span.closeDate').text();
            }
            if (node.hasClass('categoryCell')) {
                return node.find('select option:selected').text();
            }
            if (node.hasClass('subjectCell')) {
                return node.children('div').text();
            }
            if (node.hasClass('internalNotesCell')) {
                return node.children('div').text();
            }
            if (node.hasClass('assigedToCell')) {
                return node.children('span').text();
            }
            if (node.hasClass('dueCell')) {
                return node.children('span').text();
            }
            if (node.hasClass('createdDateCell')) {
                return node.text();
            }
            return '';
        }
    }

    // Make sure Object.create is available in the browser (for our prototypal inheritance)
    // Courtesy of  Crockford
    if (typeof Object.create !== 'function') {
        Object.create = function(o) {
            function F() { }
            F.prototype = o;
            return new F();
        };
    }

    $.plugin = function(name, object) {
        $.fn[name] = function(options) {
            var args = Array.prototype.slice.call(arguments, 1);
            return this.each(function() {
                var instance = $.data(this, name);
                if (instance) {
                    instance[options].apply(instance, args);
                } else {
                    instance = $.data(this, name, Object.create(object).init(this, options));
                }
            });
        };
    };


    $.plugin('ConditionTable', ConditionTable);
    $.plugin('Condition', Condition);


    $(function() {
        function isNumber(o) {
            return !isNaN(o - 0);
        }

        var _readOnly = $('#_ReadOnly').val() === 'True', _hiddenInfo = $('#CanViewHiddenInformation').val() === 'True';
        var loanid = $('#loanid').val(), $conditionChoices = $('#ConditionChoices'), $window = $(window), conditions = $('#conditions');
        var isLoanTemplate = $('#IsLoanTemplate').val() === 'True';

        conditions.bind(CUSTOM_EVENTS.CONDITION_CHECKED, function() {
            var selected = false;
            $('tbody > tr', conditions).Condition('isSelected', function(s) {
                if (s) {
                    selected = true;
                }
            });

            $('.BatchOperationButton').prop('disabled', !(selected && $('#LoanIsNotWriteable').val() !== 'True'));
        });

        if ($('#LoanIsNotWriteable').val() === 'True') { $('.BatchOperationButton').prop('disabled', true) };
        $('#CloseSelectedConditions').click(function(e) {
            conditions.trigger(CUSTOM_EVENTS.CLOSE_SELECTED_CONDITIONS);
        });
        $('#DeleteSelectedCondtions').click(function(e) {
            conditions.trigger(CUSTOM_EVENTS.DELETED_SELECTED_CONDITIONS);
        });
        var conditionTable = conditions.ConditionTable({
            Categories: ConditionData.Categories,
            ShowHiddenData: _hiddenInfo,
            sLId: loanid,
            Resources: ConditionData.Resources,
            IsTemplate: isLoanTemplate
        }).data('ConditionTable');

        conditionTable.setupInitialConditions(ConditionData.Conditions, ConditionData.Resources);
        conditions.trigger(CUSTOM_EVENTS.CONDITION_CHECKED);

        if ($('#CanCreate').val() !== 'True') { $('#ConditionChoiceList').prop('disabled', true); }
        $('#ConditionChoiceList').click(function(e) {
            $('select.CatFilter').triggerHandler('change');
            var maxPosition = conditionTable.length() + 1;
            //reset the value when user reshows window.
            if ($conditionChoices.is(':hidden')) {
                $conditionChoices.find('textarea').each(function() {
                    this.value = this.defaultValue;
                });
            }
            $('#ConditionChoices').show();
            $('#InsertPosition').val(maxPosition);
            $('#ConditionCount').html(maxPosition);
            $('Insert').prop('disabled', false);

            var winH = $window.height();
            var winW = $window.width();

            //center $conditionChoices 
            $conditionChoices.css({
                top: $window.scrollTop() + winH / 2 - $conditionChoices.height() / 2,
                left: winW / 2 - $conditionChoices.width() / 2
            });
        });

        $('#Cancel').click(function(e) {
            $conditionChoices.hide();
        });

        $('select.CatFilter,select.ConFilter').change(function(e) {
            var catDll = $('select.CatFilter'), conDll = $('select.ConFilter');
            var alt = true;
            $('#ConditionChoices table tbody > tr').each(function(i, o) {
                var $o = $(o), show = doesChoiceRowMatch(catDll, conDll, $o);
                $o.toggle(show);
                if (!show) {
                    $('input.ConditionChoiceSelected', o).prop('checked', false);
                }
            });
        });

        if ($('#CanCreate').val() !== 'True') { $('#AddCondition').prop('disabled', true); }
        $('#AddCondition').click(function() {

            if ($('#CanCreate').val() !== 'True') {
                alert('You do not have the necessary permissions to add a condition.'); return;
            }
            var url = '/newlos/Tasks/TaskEditorV2.aspx?isCond=true&loanid=' + $('#loanid').val() + '&IsTemplate=';
            if (isLoanTemplate) {
                url += 'true';
            } else {
                url += 'false';
            }
            showModal(url, null, null, null, function(args){ 
                if (!args.taskId) {
    
                    return;
                }
                gService.ConditionService.callAsyncSimple("LoadCondition", { 'TaskId': args.taskId }, addConditionCallback);
            },
            { hideCloseButton: true })
        });

        $('#Insert').click(function() {
            var ids = [];
            $('#ConditionChoices tbody > tr:visible').each(function(i, o) {
                if ($('input.ConditionChoiceSelected:checked', o).length > 0) {
                    $('input.ConditionChoiceSelected', o).prop('checked', false);
                    ids.push({ Id: $('input.ChoiceId', o).val(), Subject: $('textarea', o).val().replace(/\r?\n/g, "\r\n")
                    });
                }
            });

            if (ids.length === 0) {
                $conditionChoices.hide();
                return;
            }

            gService.ConditionService.callAsyncSimple('CreateConditionFromChoice', {
                ConditionChoices: JSON.stringify(ids),
                sLId: loanid
            }, insertCallback);
        });

        $('#InsertPosition').keyup(function(e) {
            $(this).triggerHandler('change');
        }).change(function(e) {
            if (isNumber(this.value)) {
                var num = (+this.value);
                if (num > 0 && num <= (conditionTable.length() + 1)) {
                    $('#Insert').prop('disabled', false);
                    return;
                }
            }
            $('#Insert').prop('disabled', true);
        });

        if ($('#CanCreate').val() !== 'True') { $('#ImportFromTemplate').prop('disabled', true); }
        $('#ImportFromTemplate').click(function() {
            showModal('/newlos/Underwriting/TemplateChooser.aspx', null, "dialogHeight:375px; dialogWidth:400px;center:yes; resizable:no; scroll:no; status=no; help=no;", null, function(args){ 
                if (args.OK) {
                    var data = {
                        TemplateId: args.TemplateGuid,
                        sLId: loanid,
                        SortOrder: JSON.stringify(conditionTable.getSortOrder())
                    };
                    gService.ConditionService.callAsyncSimple("ImportFromTemplate", data, importFromTemplateCallback);
                }
            }, 
            { width: 400, height:350, hideCloseButton: true });
        });

        if ($('#CanCreate').val() !== 'True') { $('#RestoreConditions').prop('disabled', true); }
        $('#RestoreConditions').click(function() {
            var btn = $('#saveBtn');

            PolyShouldShowConfirmSave(btn.is(':not(:disabled)'), function(confirmed){
                if (confirmed){
                    conditions.trigger(CUSTOM_EVENTS.SAVE_PAGE);
                }

                showModal('/newlos/Underwriting/Conditions/DeletedConditions.aspx?loanid=' + loanid, null, null, null, function (args) {
                    if (args.refresh) {
                        window.location.reload();
                    }
                },{ hideCloseButton: true });
            });
        });

        $('table.ConditionChoiceTable tr').not(":first").hover(function() { $(this).addClass('highlight'); }, function() { $(this).removeClass('highlight'); });

        $('#ConditionChoices').draggable({ cursor: 'move' });
        $(".dontPropagateMouse").bind('mousedown mouseup', function(e) { e.stopPropagation(); });

        function doesChoiceRowMatch(catDll, conDll, tr) {
            var category = $('td.CatChoice', tr).html().toLowerCase(), type = $('td.Type', tr).html().toLowerCase(), sLT = $('#sLT').val().toLowerCase(),
                choice_sLT = $('input.Choice_sLT', tr).val().toLowerCase(), categorySelected = catDll.val().toLowerCase(), conTypeSelected = conDll.val().toLowerCase();

            var catMatches = categorySelected === category || categorySelected === 'all';
            var conMatches = conTypeSelected === type || conTypeSelected === 'all';
            var sLTMatches = choice_sLT === sLT || choice_sLT === 'any';
            return catMatches && conMatches && sLTMatches;
        }

        function showSave() {
            var winH = $window.height();
            var winW = $window.width();
            var $dialog = $("#dialog-message");
            $dialog.css({
                top: $window.scrollTop() + winH / 2 - $dialog.height() / 2,
                left: winW / 2 - $dialog.width() / 2
            }).show();
        }

        $('#saveBtn').click(function() {
            showSave();
            window.setTimeout(function() {
                conditions.trigger(CUSTOM_EVENTS.SAVE_PAGE);
            }, 0);
        }).prop('disabled', true);

        $(window).bind('beforeunload', function() {
            var btn = $('#saveBtn');
            if (btn.is(':not(:disabled)')) {
                return "Leave site?  Changes you made may not be saved.";
            }
        });
    });


})(jQuery);


