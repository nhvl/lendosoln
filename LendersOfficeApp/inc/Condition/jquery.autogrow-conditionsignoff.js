(function($)
{
    /**
     * This plugin adapted from...
     *
     *
     *
     * Auto-growing textareas; technique ripped from Facebook
     * (c) 2008 Jason Frame (jason@onehackoranother.com)
     * http://github.com/jaz303/jquery-grab-bag/tree/master/javascripts/jquery.autogrow-textarea.js
     */
    $.fn.autogrow = function(options)
    {
        return this.filter('.growbox').each(function()
        {
            var self                                = this;
            var autogrowId                          = '#autogrow';
            var $self                               = $(self);
            var tagName                             = $(self).prop('tagName');
            var minHeight                           = options.minHeight ? options.minHeight : $self.height();
            
            var noFlickerPad                        = $self.hasClass('autogrow-short') ? 30 :
                                                        typeof $self.css('lineHeight') !== "number" ? 30 : parseInt($self.css('lineHeight'));

            var shadow = $('<div>').css({
                position:   'absolute',
                top:        -10000,
                left:       -10000,
                resize:     'none',
                fontSize:   $self.css('fontSize'),
                fontFamily: $self.css('fontFamily'),
                fontWeight: $self.css('fontWeight'),
                lineHeight: $self.css('lineHeight'),
                wordWrap:   'break-word'
            }).appendTo(document.body);
            
            var active = false;
            
            var times = function(string, number) {
                for (var i=0, r=''; i<number; i++) r += string;
                return r;
            };

            var update = function() {
                if (!active) {
                    active = true;                    
                    setTimeout(function() {
                        var val;
                        if (tagName === 'DIV') {
                            val = $self.html();
                        } else {
                            val = self.value.replace(/</g, '&lt;')
                                            .replace(/>/g, '&gt;')
                                            .replace(/&/g, '&amp;')
                                            .replace(/\n$/, '<br/>&nbsp;')
                                            .replace(/\n/g, '<br/>')
                                            .replace(/ {2,}/g, function(space){ return times('&nbsp;', space.length - 1) + ' ' });
                        }

                        shadow.css('width', $self.width() - 4); // magic number to account for border and padding
                        shadow.html(val);
                        var newHeight = Math.max(shadow.height() + noFlickerPad, minHeight);
                        $self.css('height', newHeight);
                        
                        // Now, adjust the height of the other textboxes in the same row
                        var $tr = $self.closest('tr');
                        $tr.find('.growbox').each(function() {
                            var probeHeight = $(this).height();
                            if (probeHeight < newHeight) {
                                $(this).css('height', newHeight);
                            } else {
                                $self.css('height', probeHeight);
                            }
                        })
                        
                        setTimeout(function() { active = false; }, 500);
                    }, 100);
                }
            }

            $self.change(update).keyup(update).keydown(update);
            $(window).resize(update);
            
            update();
        });
    };
})(jQuery);