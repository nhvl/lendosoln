var gService = new SimpleService();

function SimpleService() {
  this.register = f_register;
}

function f_register(url, name) {
  this[name] = new UrlService(url);
}

function UrlService(url) {
    this.url = url;
    this.callAsync = f_callAjax;
    this.callAsyncSimple = function (methodName, args, successCallback, errorCallback, timeout) {
        this.callAsync(methodName, args, true, null, true, true, successCallback, errorCallback, timeout);
    };
    this.callAsyncBypassOverlay = function (methodName, args, successCallback, errorCallback, timeout) {
        this.callAsync(methodName, args, true, null, true, true, successCallback, errorCallback, timeout, true);
    };
    if (!window.jQuery || !(url.toLowerCase().indexOf("loadmin") > -1))
		this.call = f_callXml;
	else
		this.call = f_callAjax;
}

function f_callAjax(methodName, args, keepQuiet, bUseParentForError, dontHandleError, isAsync, successCallback, errorCallback, timeout, bypassOverlay)
{
    var result = {};
    var timeoutTrue = 0;
    if (typeof (timeout) === 'number') {
        timeoutTrue = timeout;
    }

    isAsync = isAsync || false;

    if (isAsync && !bypassOverlay)
    {
        if (typeof (triggerOverlay) == "function") {

            triggerOverlay();
        }
    }

    jQuery.ajax({
        type: "POST",
        url: this.url + "?method=" + methodName,
        async: isAsync,
        data: JSON.stringify(args),
        contentType: "application/json",
        dataType: "json",
        cache: false,
        timeout: timeoutTrue
    }).then(
        function(data, textStatus, jqXHR)
        {
            if (jqXHR.getResponseHeader('IsLogin') == 'true') {
                getHighestParentSameDomain().location = jqXHR.responseURL;
                return;
            }

            result.error = false;
            result.value = data.d;
            
            attemptRemoveOverlay(isAsync && !bypassOverlay);

            resetLeftNav(result);
            if (typeof successCallback === 'function') {
                successCallback(result);
            }
        },
        function(jqXHR, textStatus, errorThrown)
        {
            if (jqXHR.getResponseHeader('IsLogin') == 'true') {
                getHighestParentSameDomain().location = jqXHR.responseURL;
                return;
            }

            var sErrorUrl = gVirtualRoot + '/common/AppError.aspx';
            result.error = true;
            result.UserMessage = 'INTERNAL SERVER ERROR';
            if (jqXHR.responseText.indexOf("LP_f6d1d39dee3c435d9d14f1dc245e316d") != -1)
            {
                if (typeof clearDirty === 'function') clearDirty();

                result.UserMessage = 'Your session is expired. Please login again.';
                top.location = gVirtualRoot + '/SessionExpired.aspx';

            }
            else
            {
                try
                {
                    var errorObject = jQuery.parseJSON(jqXHR.responseText);
                    result.UserMessage = errorObject.UserMessage;
                    if (!!!dontHandleError)
                    {
                        if (typeof clearDirty === 'function') clearDirty();

                        var errorUrl = sErrorUrl + '?id=' + errorObject.ErrorReferenceNumber + '&type=' + errorObject.type;

                        if (bUseParentForError != null && bUseParentForError)
                            parent.location = errorUrl;
                        else
                            top.location = errorUrl;
                    }
                    else
                    {
                        result.type = errorObject.type;
                        result.url = sErrorUrl + '?id=' + errorObject.ErrorReferenceNumber + '&type=' + errorObject.type;

                    }
                }
                catch (e)
                {
                    var jqXHRDebug = "textStatus = " + textStatus + ". errorThrown = " + errorThrown + ". "
                        + (jqXHR.readyState     ? " readyState = "      + jqXHR.readyState      + ".\n" : " no readyState.\n")
                        + (jqXHR.status         ? " status = "          + jqXHR.status          + ".\n" : " no status.\n")
                        + (jqXHR.statusText     ? " statusText = "      + jqXHR.statusText      + ".\n" : " no statusText.\n")
                        + (jqXHR.responseText   ? " responseText = "    + jqXHR.responseText    + ".\n" : " no responseText.\n")
                        + (jqXHR.responseXML    ? " responseXML = "     + jqXHR.responseXML     + ".\n" : " no responseXML.\n")
                    //logJSException(e, "problem with f_call's error handling. Got response: {" + jqXHRDebug + "}");
                    var maxLength = jqXHRDebug.length < 500 ? jqXHRDebug.length : 500;
                    top.location = sErrorUrl + '?id=AJAX&msg=' + escape(jqXHRDebug.substring(0, maxLength));
                    result.errorDetail = 'INTERNAL SERVER ERROR';
                }
            }
            
            attemptRemoveOverlay(isAsync && !bypassOverlay);

            if (typeof errorCallback === 'function') {
                errorCallback(result);
            }
            else if (typeof successCallback === 'function') {
                successCallback(result);
            }
        }
    );

    return result;
}

function resetLeftNav(result) {
    if (result && result.value && result.value['ShouldResetLeftNav'] == 'True') {
        forceResetLeftNav();
    }
}

function forceResetLeftNav() {
    var location = retrieveFrameProperty(parent, 'treeview', 'location');
    location.reload();
}

function f_asyncCall(methodName, args, successCallback, errorCallback, keepQuiet, bUseParentForError, dontHandleError) {
    this.call(methodName, args, keepQuiet, bUseParentForError, dontHandleError, true,
        function (data) { attemptRemoveOverlay(); successCallback(data);},
        function (data) { attemptRemoveOverlay(); errorCallback(data); });
}

function attemptRemoveOverlay(isAsync) {
    if (isAsync && typeof(removeOverlay) == 'function') {
        removeOverlay();
    }
}


function buildXmlRequest(args) {
  if (args == null) {
    return '<request><data/></request>'
  } else {
    var str = '';
    for (p in args) {
      str += p + '="' + encodeValue(args[p]) + '" ';
    }
    if (gIsDebug) alert('<request><data ' + str + '/></request>');
    return '<request><data ' + str + '/></request>';

  }
}

function encodeValue(str) {
  var  replaceItems = [
    {r : /&/g, s : '&amp;'},
    {r : /</g, s : '&lt;'},
    {r : />/g, s : '&gt;'},
    {r : /"/g, s : '&quot;'},
    {r : /'/g, s : '&apos;'}
  ];

  if (str == null)
  {
      str = '';
  }
  else
  {
      str = str.toString();
  }
    
    for( var i = 0; i < replaceItems.length; i++ ) {
        str = str.replace(replaceItems[i].r, replaceItems[i].s);
    }
    
    
   return str;
 
}

// Only do synchronous call.
function f_callXml(methodName, args, keepQuiet, bReturnError) {
  var ret = new Object();
  ret.error = true;
  var sErrorUrl = gVirtualRoot + '/common/AppError.aspx';
  var sErrorMsg = '';
  var dtStarted = new Date();
  for (var i = 0; i < 3; i++) {
      try {
          var oHttp = null, requestedWith;
          var fullUrl = this.url + '?method=' + methodName;
          var isRedirect = false;

      if (window.XMLHttpRequest) {
          oHttp = new XMLHttpRequest();
          requestedWith = 'XMLHttpRequest';
          oHttp.onload = function () {
              if (oHttp.getResponseHeader('IsLogin') == 'true') {
                  isRedirect = true;
                getHighestParentSameDomain().location = oHttp.responseURL;
              }
          };
      } else {
        oHttp = new ActiveXObject("Microsoft.XMLHTTP");
        requestedWith = 'Microsoft.XMLHTTP';
      }
      oHttp.open("POST", fullUrl, false);
      oHttp.setRequestHeader('X-Requested-With', requestedWith);
      oHttp.setRequestHeader('Content-Type', 'text/xml');
      
      try { oHttp.responseType = 'msxml-document'; } catch(e){}
      oHttp.send(buildXmlRequest(args));

      if (gIsDebug) alert(oHttp.responseText);

      var doc = oHttp.responseXML;
      if (isRedirect) {
          return;
      }

      if (doc == null) throw ("doc is null." + oHttp.responseText);
      if (doc.documentElement == null) {
        //cross domain redirects use oHttp.status = 0
          if (oHttp.responseText.indexOf("LP_f6d1d39dee3c435d9d14f1dc245e316d") != -1 || oHttp.status == 0)
          { 
              clearDirty();
              ret.error = true;
              ret.UserMessage = 'Your session is expired. Please login again.';
              top.location = gVirtualRoot + '/website/index.aspx';
              return ret;
          } 
          else if (oHttp.responseText.indexOf("id=DUP_LOGIN") != -1)
          {
              // Only applicable to Duplicate login detection for LO.
              clearDirty();
              ret.error = true;
              ret.UserMessage = 'You have been logged out because you logged in on a different computer.';
              top.location = gVirtualRoot + '/website/index.aspx';
              return ret;
          }
          else if (oHttp.status > 12000) {
            ret.error = true;
            ret.UserMessage = 'You are experiencing connection issues. Please try again. Error Code : ' + oHttp.status;
            return ret;
          }
          else
              throw ("doc.documentElement is null. Status: " +  oHttp.status +' ' + oHttp.responseText);
      }
      
      var rec;

      if ('selectSingleNode' in doc.documentElement) {
          rec = doc.documentElement.selectSingleNode("data");
      }
      else {
          rec = doc.documentElement.getElementsByTagName("data")[0];
      }

      if (rec != null) {
        ret.value = new Object();
        ret.error = false;
        var coll = rec.attributes;
        for (var i = 0; i < coll.length; i++) {
          var a = coll.item(i);
          ret.value[a.name] = a.value;
      }

      var childNodes = rec.children || rec.childNodes; // IE XML cannot use children
      if (childNodes != null)
      {
          for (var i = 0; i < childNodes.length; i++)
          {
              var a = childNodes[i];
              ret.value[a.nodeName] = a.text || a.textContent; // a.text in IE, a.textContent for Chrome
          }
      }
      }

      if ('selectSingleNode' in doc.documentElement) {
          rec = doc.documentElement.selectSingleNode("error");
      }
      else {
          rec = doc.documentElement.getElementsByTagName("error")[0];
      }

      if (rec != null)
      {
        if (typeof (clearDirty) == "function") clearDirty();
        var type = rec.getAttribute("type");
        ret.ErrorType = type;

        if (type == "PageDataLoadDenied") {
          alert(rec.getAttribute("UserMessage"));
          top.close();
          return ret;
        }
        if (bReturnError != null && !bReturnError) {
            top.location = sErrorUrl + '?id=' + rec.getAttribute('ErrorReferenceNumber') + '&type=' + type;
        }
        else {
          ret.UserMessage = rec.getAttribute("UserMessage");
        }
      }
      // TODO: Handle error.

          resetLeftNav(ret);
      return ret;

    } catch (e) {
      if (null != e) {
        if (typeof (e) === 'string') {
          sErrorMsg = e;
        } else {
          sErrorMsg = e.message;
        }
      }
    }
  }
  if (typeof (clearDirty) == "function") clearDirty();
  var duration = new Date() - dtStarted;
  var tmp = this.url + '::' + methodName + '. Duration=' + duration + '. Err=' + sErrorMsg;

  var maxLength = tmp.length < 500 ? tmp.length : 500;

  top.location = sErrorUrl + '?id=AJAX&msg=' + escape(tmp.substring(0,maxLength));
  ret.error = true;
  ret.errorDetail = 'Internal error';
  return ret;

}


