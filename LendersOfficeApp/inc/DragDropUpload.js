﻿var droppedFiles = {};
var allowedExtensions = {};
var scope;
var hasAdditionalInfo = false;
var appNames = [];
var isPml = false;
var dragDropAdditionalInfoApp;
var showBarcodes = true;
var hideFilesInFileList = false;
var fileUploadSizeLimit;
var dropzoneActive = false;

var defaultSettings = {
    compact: false,
    hasAdditionalInfoParam: false,
    appNamesParam: [],
    isPmlParam: false,
    showBarcodesParam: false,
    bypassFileUploadSizeLimit: false,
    onChangeCallback: null,
    onDocTypeSelectCallback: null,
    onProcessAllFilesCallback: null,
    blockUpload: false,
    blockUploadMsg: null,
    hideFilesInFileListParam: false,
    checkForUnprocessedUploadsBeforeUnload: false
}

function fileHasDocType(file) {
    return file.docTypeId != null && file.docTypeId != '' && file.docTypeId != '-1';
}

function getFriendlyFileUploadSizeLimitWarning() {
    return "Total upload size is limited to " + fileUploadSizeLimit / (1000 * 1000) + " MB.";
}

function skipDropZoneFile(dropZone, cumulativeUploadSize, limit, invalidFiles, file) {
    if (getFilesCount(dropZone.id) + 1 > limit) {
        invalidFiles.push(file.name + ': Maximum file count reached.');
        return true;
    }

    if (cumulativeUploadSize + file.size > fileUploadSizeLimit) {
        invalidFiles.push(file.name + ': ' + getFriendlyFileUploadSizeLimitWarning());
        return true;
    }

    if (!checkExtension(dropZone, file)) {
        invalidFiles.push(file.name + " is not in the correct format. Please select " + dropZone.getAttribute("extensions") + " files for upload.");
        return true;
    }

    return false;
}

function alertInvalidDropzoneFiles(invalidFiles) {
    if (typeof simpleDialog !== 'undefined') {
        simpleDialog.alert(invalidFiles.join('<br/>'), 'Unable to Select All Files');
    }
    else {
        alert('Unable to select all files:\n\n' + invalidFiles.join('\n'));
    }
}

function checkForPendingFiles(dropzone, oldOnBeforeUnload) {
    if (getFilesCount(dropzone.id) !== 0) {
        return 'You have documents that are still pending upload. Are you sure you want to leave this screen?';
    }

    if (oldOnBeforeUnload) {
        return oldOnBeforeUnload();
    }
}

function addUnprocessedUploadsUnloadCheck(dropZone) {
    if (typeof window.f_smartConfirm === 'function') {
        var oldSmartConfirm = window.f_smartConfirm;
        window.f_smartConfirm = function (normalHandler, postbackHandler, bCancel, postSaveHandler) {
            if (getFilesCount(dropZone.id) !== 0) {
                PolyConfirmShow(
                    'You have documents that are still pending upload. Are you sure you want to leave this screen?',
                    function (isConfirmed) { if (isConfirmed) { oldSmartConfirm(normalHandler, postbackHandler, bCancel, postSaveHandler); } },
                    false/*shouldShowCancel*/,
                    null/*cancelCallback*/,
                    true/*forceLqbPopup*/);
            }
            else {
                oldSmartConfirm(normalHandler, postbackHandler, bCancel, postSaveHandler);
            }
        }
    }
    else {
        var oldOnBeforeUnload = window.onbeforeunload;
        window.onbeforeunload = function () { return checkForPendingFiles(dropZone, oldOnBeforeUnload); };
    }
}

function addActiveDropZoneClass(ev) {
    if (dropzoneActive) {
        return;
    }

    dropzoneActive = true;

    var target = retrieveEventTarget(ev);
    while (!target.classList.contains('drop-zone')) {
        target = target.parentElement;
    }

    target.classList.add('drop-zone-active');
}

function removeActiveDropZoneClass(ev) {
    var target = retrieveEventTarget(ev);
    if (dropzoneActive && !target.classList.contains('drop-zone')) {
        return;
    }

    dropzoneActive = false;

    while (!target.classList.contains('drop-zone')) {
        target = target.parentElement;
    }

    target.classList.remove('drop-zone-active');
}

function storeFile(dropZoneId, file) {
    var files = retrieveFiles(dropZoneId);

    var dupeFiles = files.filter(function (storedFile) { return checkIsDuplicate(file, storedFile); });
    if (dupeFiles.length > 0) {
        return false;
    }

    files.push(file);
    droppedFiles[dropZoneId] = files;
    return true;
}

function checkIsDuplicate(file1, file2) {
    return file1.name == file2.name && file1.size == file2.size && file1.lastModifiedDate.getTime() == file2.lastModifiedDate.getTime();
}

function getFilesCount(dropZoneId) {
    var files = retrieveFiles(dropZoneId);
    return files.length;
}

function retrieveFiles(dropZoneId) {
    return droppedFiles[dropZoneId] || [];
}

function readFileWithCallback(files, index, callback) {
    if (index === files.length) {
        return;
    }

    var reader = new FileReader();
    var file = files[index];

    addEventHandler(reader, "load",
        function (file, reader) {
            return function (event) {
                if (typeof callback === "function") {
                    if (!!reader.result) {
                        callback(file, reader.result.split(',')[1]);
                    }
                    else {
                        callback(file, "");
                    }
                }

                readFileWithCallback(files, ++index, callback);
            }
        }(file, reader));

    reader.readAsDataURL(file);
}

function applyCallbackFileObjects(dropZoneId, callback) {
    var files = retrieveFiles(dropZoneId);

    for (var i = 0; i < files.length; ++i) {
        var file = files[i];
        file.index = i;
        callback(file);
    }
}

function applyCallbackBase64Files(dropZoneId, callback) {
    var files = retrieveFiles(dropZoneId);

    for (var i = 0; i < files.length; ++i) {
        files[i].index = i;
    }

    readFileWithCallback(files, 0, callback);
    return files;
}

function addFilesToUploadLabel(dropZoneId, fileListContainer) {
    var fileUploadLabel = document.querySelector('#' + dropZoneId + ' .files-to-upload-label');
    if (fileUploadLabel == null) {
        var filesToUploadLabel = document.createElement('span');
        filesToUploadLabel.classList.add('files-to-upload-label');
        filesToUploadLabel.innerText = 'Files to Upload:';

        fileListContainer.appendChild(filesToUploadLabel);
    }
}

function resetDragDrop(dropZoneId)
{
    var dropZone = document.getElementById(dropZoneId);
    var fileListContainer = dropZone.querySelector(".file-list");
    droppedFiles[dropZoneId] = [];

    if (hasAdditionalInfo) {
        scope.files = null;
    }

    while (fileListContainer.lastChild != null && !fileListContainer.lastChild.classList.contains('files-to-upload-label')) {
        fileListContainer.removeChild(fileListContainer.lastChild);
    }

    addFilesToUploadLabel(dropZoneId, fileListContainer);

	updateTotalFilesUi(dropZoneId);

	if (hasAdditionalInfo) {
		scope.updateAdditionalInfoUI(dropZoneId);
	}

    // Adapted from https://stackoverflow.com/a/35323290
    if (typeof getBrowserVersion === 'function' && getBrowserVersion() > 10) {
        dropZone.fileUpload.value = null;
    }
    else {
        dropZone.fileUpload.type = '';
        dropZone.fileUpload.type = 'file';
    }
}

function removeFile(dropZoneId, file) {
    var files = retrieveFiles(dropZoneId);
    files.splice(files.indexOf(file), 1);
	updateTotalFilesUi(dropZoneId);
	if (hasAdditionalInfo) {
		scope.updateAdditionalInfoUI(dropZoneId);
	}
}

function checkExtension(dropZone, file) {
    var extensions = allowedExtensions[dropZone.id] || dropZone.getAttribute("extensions").toLowerCase().split(", ");
    allowedExtensions[dropZone.id] = extensions;
    var extension = file.name.substring(file.name.lastIndexOf(".")).toLowerCase();
    return !extensions || extensions.length == 0 || extensions.indexOf(extension) != -1;
}

function addFileToUi(dropZone, file) {
    if (hideFilesInFileList) {
        return;
    }

    var fileUploadLabel = document.querySelector('#' + dropZone.id + ' .files-to-upload-label');
    if (fileUploadLabel != null) {
        dropZone.firstChild.lastChild.removeChild(fileUploadLabel);
    }

    var container = document.createElement("div");
    container.classList.add("file-description-container");

    var label = document.createElement("span");
    label.className = "label";
    label.innerText = file.name;

    var removeButton = document.createElement("img");
    removeButton.classList.add("remove-file");
    removeButton.src = ML.VirtualRoot + '/images/delete.svg';
    addEventHandler(removeButton, "click", function () {
        removeFile(dropZone.id, file);
        dropZone.firstChild.lastChild.removeChild(container);

        if (getFilesCount(dropZone.id) === 0) {
            addFilesToUploadLabel(dropZone.id, dropZone.firstChild.lastChild);
        }

        if (dropZone.onChangeCallback) {
            dropZone.onChangeCallback();
        }
    });

    container.appendChild(label);
    container.appendChild(removeButton);
    dropZone.firstChild.lastChild.appendChild(container);
}

function updateTotalFilesUi(dropZoneId) {
    var dropZone = document.getElementById(dropZoneId);
    var currentFilesSpan = dropZone.querySelector(".selected-files-count");

    if (currentFilesSpan) {
        // Current files not displayed for the compact dropzone.
        currentFilesSpan.innerText = getFilesCount(dropZone.id);
    }
}

function processFile(dropZone, file) {
    if (storeFile(dropZone.id, file)) {
        addFileToUi(dropZone, file);

        if (dropZone.onChangeCallback) {
            dropZone.onChangeCallback();
        }
    }
}

function retrieveDropZoneParent(element) {
    while (!element.classList.contains("drop-zone")) {
        element = element.parentNode;
    }

    return element;
}

function drop_handler(ev) {
    ev.preventDefault();
    dropzoneActive = false;
    removeActiveDropZoneClass(ev);
    
    var dt = ev.dataTransfer;
    var dropZone = retrieveDropZoneParent(retrieveEventTarget(ev));
    var limit = dropZone.getAttribute("limit");

    var cumulativeUploadSize = 0;
    var invalidFiles = [];

    if (dt.items) {
        // Use DataTransferItemList interface to access the file(s)
        for (var i = 0; i < dt.items.length; i++) {
            // If dropped items aren't files, reject them
            if (dt.items[i].kind == "file") {
                var file = dt.items[i].getAsFile();
                if (skipDropZoneFile(dropZone, cumulativeUploadSize, limit, invalidFiles, file)) {
                    continue;
                }

                cumulativeUploadSize += file.size;
                processFile(dropZone, file);
            }
        }
    } else {
        // Use DataTransfer interface to access the file(s)
        for (var i = 0; i < dt.files.length; i++) {
            var file = dt.files[i];
            if (skipDropZoneFile(dropZone, cumulativeUploadSize, limit, invalidFiles, file)) {
                continue;
            }

            cumulativeUploadSize += file.size;
            processFile(dropZone, file);
        }
    }

    if (invalidFiles.length !== 0) {
        alertInvalidDropzoneFiles(invalidFiles);
    }

	updateTotalFilesUi(dropZone.id);

    if (dropZone.onProcessAllFilesCallback) {
        dropZone.onProcessAllFilesCallback();
    }

	if (hasAdditionalInfo) {
		scope.updateAdditionalInfoUI(dropZone.id);
	}
}

function enterzone_handler(ev) {
    // Prevent default select and drag behavior
    ev.preventDefault();
    addActiveDropZoneClass(ev);
}

function dragleave_handler(ev) {
    // Prevent default select and drag behavior
    ev.preventDefault();
    removeActiveDropZoneClass(ev);
}

function dragend_handler(ev) {
    // Remove all of the drag data
    var dt = ev.dataTransfer;
    if (dt.items) {
        // Use DataTransferItemList interface to remove the drag data
        for (var i = 0; i < dt.items.length; i++) {
            dt.items.remove(i);
        }
    } else {
        // Use DataTransfer interface to remove the drag data
        ev.dataTransfer.clearData();
    }

    removeActiveDropZoneClass(ev);
}

function registerDragDropUpload(dropZone, dragDropSettings) {
    dropZone.classList.add("drop-zone");

    var settings = dragDropSettings || defaultSettings;

    if (settings.blockUpload)
    {
        initBlockedDragDropUi(dropZone, dragDropSettings);
        return; // Upload blocked. Do Nothing.
    }

    if (settings.checkForUnprocessedUploadsBeforeUnload) {
        addUnprocessedUploadsUnloadCheck(dropZone);
    }

    hasAdditionalInfo = settings.hasAdditionalInfoParam;
    appNames = settings.appNamesParam;
    isPml = settings.isPmlParam;
    showBarcodes = settings.showBarcodesParam;
    hideFilesInFileList = settings.hideFilesInFileListParam;

    if (settings.bypassFileUploadSizeLimit) {
        fileUploadSizeLimit = Number.MAX_SAFE_INTEGER;
    }
    else {
        // Restrict uploads to 100 MB.
        fileUploadSizeLimit = 100 * 1000 * 1000;
    }

    if (isPml) {
        dropZone.classList.add('drop-zone-pml');
    }

    dropZone.onChangeCallback = settings.onChangeCallback;
    dropZone.onProcessAllFilesCallback = settings.onProcessAllFilesCallback;
    dropZone.onDocTypeSelectCallback = settings.onDocTypeSelectCallback;

    if (settings.compact) {
        initCompactDragDropUi(dropZone);
    }
    else {
        initDragDropUi(dropZone);
    }

    //Feature detect here.  Is it available?
    addEventHandler(dropZone, "dragenter", enterzone_handler);
    addEventHandler(dropZone, "dragover", enterzone_handler);
    addEventHandler(dropZone, "dragleave", dragleave_handler);
    addEventHandler(dropZone, "dragend", dragend_handler);
	addEventHandler(dropZone, "drop", drop_handler);

    $("#dragDropAdditionalInfo").hide();

    if (hasAdditionalInfo) {
        initializeDragDropAdditionalInfoAngular(dropZone.id);
    }
}

function createFileUpload(dropZone) {
    var fileUpload = document.createElement("input");

    fileUpload.id = dropZone.id + "_ManualUpload";
    fileUpload.type = "file";
    fileUpload.setAttribute('skip-unification', true);
    fileUpload.setAttribute("multiple", true);
    fileUpload.setAttribute("accept", dropZone.getAttribute("extensions"));
    addEventHandler(fileUpload, "change", onManualUploadSelect);

    return fileUpload;
}

function createManualUpload(fileUpload) {
    var manualUploadButton = document.createElement("button");

    manualUploadButton.type = "button";
    manualUploadButton.innerText = "Or Select Files to Upload";
    manualUploadButton.classList.add("select-button");
    addEventHandler(manualUploadButton, "click", function () { fileUpload.click() });

    return manualUploadButton;
}

function initCompactDragDropUi(dropZone) {
    dropZone.classList.add("drop-zone-compact");

    if (isPml) {
        dropZone.classList.add("drop-zone-compact-pml");
    }

    var childContainer = document.createElement("div");
    childContainer.classList.add("child-container");

    var explanationSpan;
    if (isPml) {
        explanationSpan = document.createElement("span");
        explanationSpan.innerText = "Drag and Drop Files Here";
        explanationSpan.classList.add('drag-drop-explanation-pml');
    }
    else {
        explanationSpan = document.createElement("div");
    explanationSpan.innerText = "Drag and Drop Files Here to Upload";
    }

    var fileUpload = createFileUpload(dropZone);
    dropZone.fileUpload = fileUpload;

    var manualUploadButton = createManualUpload(fileUpload);

    var explanationDiv = document.createElement("div");
    explanationDiv.classList.add("explanation");

    var uploadIcon = document.createElement('img');
    uploadIcon.classList.add('cloud-icon');
    uploadIcon.src = ML.VirtualRoot + '/images/backup.svg';

    var dropIcon = document.createElement('img');
    dropIcon.classList.add('drop-icon');
    dropIcon.src = ML.VirtualRoot + '/images/drag_drop_hover.svg';

    explanationDiv.appendChild(uploadIcon);
    explanationDiv.appendChild(dropIcon);

    if (isPml) {
        explanationDiv.appendChild(explanationSpan);
    explanationDiv.appendChild(manualUploadButton);
    }
    else {
        explanationDiv.appendChild(manualUploadButton);
    explanationDiv.appendChild(explanationSpan);
    }

    var fileListDiv = document.createElement("div");
    fileListDiv.classList.add("file-list");

    var filesToUploadLabel = document.createElement('span');
    filesToUploadLabel.classList.add('files-to-upload-label');
    filesToUploadLabel.innerText = 'Files to Upload (Max ' + dropZone.getAttribute("limit") + '):';
    fileListDiv.appendChild(filesToUploadLabel);

    childContainer.appendChild(explanationDiv);
    childContainer.appendChild(fileListDiv);
    dropZone.appendChild(childContainer);
    dropZone.appendChild(fileUpload);
}

function initDragDropUi(dropZone) {
    var childContainer = document.createElement("div");
    childContainer.classList.add("child-container");

    var explanationSpan = document.createElement("div");
    explanationSpan.innerText = "Drag and Drop Files Here to Upload";

    var fileUpload = createFileUpload(dropZone);
    dropZone.fileUpload = fileUpload;

    var manualUploadButton = createManualUpload(fileUpload);

    var explanationDiv = document.createElement("div");
    explanationDiv.classList.add("explanation");

    var uploadIcon = document.createElement('img');
    uploadIcon.classList.add('cloud-icon');
    uploadIcon.src = ML.VirtualRoot + '/images/backup.svg';

    var dropIcon = document.createElement('img');
    dropIcon.classList.add('drop-icon');
    dropIcon.src = ML.VirtualRoot + '/images/drag_drop_hover.svg';

    var totalFilesContainer = document.createElement("div");
    totalFilesContainer.classList.add("total-files-container");
    var currentFilesSpan = document.createElement("span");
    currentFilesSpan.classList.add("selected-files-count");
    currentFilesSpan.innerText = "0";
    var maxFilesSpan = document.createElement("span");
    maxFilesSpan.classList.add("max-files-count");
    maxFilesSpan.innerText = dropZone.getAttribute("limit");

    totalFilesContainer.appendChild(currentFilesSpan);
    totalFilesContainer.appendChild(document.createTextNode(" of "));
    totalFilesContainer.appendChild(maxFilesSpan);
    totalFilesContainer.appendChild(document.createTextNode(" files selected for upload"));

    explanationDiv.appendChild(uploadIcon);
    explanationDiv.appendChild(dropIcon);
    explanationDiv.appendChild(explanationSpan);
    explanationDiv.appendChild(manualUploadButton);
    explanationDiv.appendChild(totalFilesContainer);

    var fileListDiv = document.createElement("div");
    fileListDiv.classList.add("file-list");

    var filesToUploadLabel = document.createElement('span');
    filesToUploadLabel.classList.add('files-to-upload-label');
    filesToUploadLabel.innerText = 'Files to Upload:';
    fileListDiv.appendChild(filesToUploadLabel);

    childContainer.appendChild(explanationDiv);
    childContainer.appendChild(fileListDiv);
    dropZone.appendChild(childContainer);
    dropZone.appendChild(fileUpload);
}

function initBlockedDragDropUi(dropZone, dragDropSettings) {
    addEventHandler(dropZone, "dragover", function (e) { e.preventDefault(); });
    addEventHandler(dropZone, "dragend", function (e) { e.preventDefault(); });
    addEventHandler(dropZone, "drop", function (e) { e.preventDefault(); });

    if (dragDropSettings.compact) {
        initBlockedDragDropUi_compact(dropZone, dragDropSettings);
    }
    else {
        initBlockedDragDropUi_full(dropZone, dragDropSettings);
    }
}

function initBlockedDragDropUi_compact(dropZone, dragDropSettings) {
    dropZone.classList.add("drop-zone-compact");

    var childContainer = document.createElement("div");
    childContainer.classList.add("child-container");

    var stopIcon = document.createElement("img");
    stopIcon.classList.add("cloud-icon");
    stopIcon.classList.add("v-align-middle");
    stopIcon.src = ML.VirtualRoot + "/images/warning.svg";

    var explanationText = document.createElement("span");
    explanationText.classList.add("dark-red-text");
    explanationText.classList.add("v-align-middle");
    explanationText.innerText = "You Do Not Have Permission To Upload Files To This Page.";

    var explanationDiv = document.createElement("div");
    explanationDiv.classList.add("explanation");

    explanationDiv.appendChild(stopIcon);
    explanationDiv.appendChild(explanationText);

    childContainer.appendChild(explanationDiv);
    dropZone.appendChild(childContainer);

    if (dragDropSettings.blockUploadMsg) {
        var reasons = "Reasons:\n" + dragDropSettings.blockUploadMsg;
        dropZone.title = reasons;
    }
}

function initBlockedDragDropUi_full(dropZone, dragDropSettings) {
    var childContainer = document.createElement("div");
    childContainer.classList.add("child-container");

    var stopIcon = document.createElement("img");
    stopIcon.classList.add("cloud-icon");
    stopIcon.src = ML.VirtualRoot + "/images/warning.svg";

    var explanationText = document.createElement("div");
    explanationText.classList.add("dark-red-text");
    explanationText.innerText = "You Do Not Have Permission To Upload Files To This Page.";

    var explanationDiv = document.createElement("div");
    explanationDiv.classList.add("explanation");

    explanationDiv.appendChild(stopIcon);
    explanationDiv.appendChild(explanationText);

    if (dragDropSettings.blockUploadMsg) {
        var reasons = "Reasons:\n" + dragDropSettings.blockUploadMsg;
        var reasonText = document.createElement("div");
        reasonText.classList.add("blocked-reason");
        reasonText.innerText = reasons;

        explanationDiv.appendChild(reasonText);
    }

    childContainer.appendChild(explanationDiv);
    dropZone.appendChild(childContainer);
}

function onManualUploadSelect(event) {
    var input = retrieveEventTarget(event);
    var dropZone = retrieveDropZoneParent(input);
    var limit = dropZone.getAttribute("limit");

    var cumulativeUploadSize = 0;
    var invalidFiles = [];

    for (var i = 0; i < input.files.length; i++) {
        var file = input.files[i];
        if (skipDropZoneFile(dropZone, cumulativeUploadSize, limit, invalidFiles, file)) {
            continue;
        }

        cumulativeUploadSize += file.size;
        processFile(retrieveDropZoneParent(input), file);
    }

    if (invalidFiles.length !== 0) {
        alertInvalidDropzoneFiles(invalidFiles);
    }

	updateTotalFilesUi(dropZone.id);

    if (dropZone.onProcessAllFilesCallback) {
        dropZone.onProcessAllFilesCallback();
    }
	
    if (hasAdditionalInfo) {
		scope.updateAdditionalInfoUI(dropZone.id);
	}
}

function setDefaultDragDropDocType(dropzoneId, docTypeId, docTypeName) {
    if (!hasAdditionalInfo) {
        return;
    }

    var files = retrieveFiles(dropzoneId);
    for (var index = 0; index < files.length; ++index) {
        document.getElementById('selectedDocType_' + index).innerHTML = docTypeName;
        files[index].docTypeId = docTypeId;
    }

    var dropzone = document.getElementById(dropzoneId);
    if (dropzone.onDocTypeSelectCallback) {
        dropzone.onDocTypeSelectCallback();
    }
}

function initializeDragDropAdditionalInfoAngular(dropzoneId) {
	dragDropAdditionalInfoApp = angular.module("dragDropAdditionalInfo", []);

	dragDropAdditionalInfoApp.controller("dragDropAdditionalInfoController", ["$scope",
		function DragDropAdditionalInfoController($scope) {
			scope = $scope;

			$scope.updateAdditionalInfoUI = function (dropZoneId) {
				if (!hasAdditionalInfo) {
					return;
				}

                $scope.dropZoneId = dropZoneId;
				$scope.isPml = isPml;
				$scope.showBarcodes = showBarcodes;
				$scope.files = retrieveFiles(dropZoneId);
				$scope.appNames = appNames;
				$("#dragDropAdditionalInfo").toggle($scope.files.length > 0);
				$scope.$apply();
			};

			$scope.selectDocType = function (index, file) {
				if (!hasAdditionalInfo) {
					return;
				}

				var docTypePickerLink = $scope.isPml ? "/main/EDocs/DocTypePicker.aspx" : "/newlos/ElectronicDocs/DocTypePicker.aspx";

				if ($scope.isPml) {
                    LQBPopup.Show(ML.VirtualRoot + docTypePickerLink, {
						popupClasses: 'modal-select-doctype',
						draggable: false,
						'onReturn': function (id, name) {
							document.getElementById('selectedDocType_' + index).innerHTML = name;
                            file.docTypeId = id;

                            var dropzone = document.getElementById($scope.dropZoneId);
                            if (dropzone.onDocTypeSelectCallback) {
                                dropzone.onDocTypeSelectCallback();
                            }
						}
					});
				}
				else {
					showModal(docTypePickerLink, null, null, null, function (result) {
						if (result && result.docTypeId && result.docTypeName) { // New doc type selected
							document.getElementById('selectedDocType_' + index).innerHTML = result.docTypeName;
                            file.docTypeId = result.docTypeId;

                            var dropzone = document.getElementById($scope.dropZoneId);
                            if (dropzone.onDocTypeSelectCallback) {
                                dropzone.onDocTypeSelectCallback();
                            }
						}
					});
				}
			};

			$scope.scanBarcode = function (index, file) {
				if (!hasAdditionalInfo) {
					return;
				}

				document.getElementById('selectedDocType_' + index).innerHTML = "--Assign Using Barcodes--";
                file.docTypeId = ML.ScanDocMagicBarcodesDocTypeId;

                var dropzone = document.getElementById($scope.dropZoneId);
                if (dropzone.onDocTypeSelectCallback) {
                    dropzone.onDocTypeSelectCallback();
                }
			}
		}]);

	dragDropAdditionalInfoApp.directive('dragDropAdditionalInfoDir', function () {
		return {
			templateUrl: ML.VirtualRoot + "/common/DragDropAdditionalInfo.html"
		};
	});

    angular.bootstrap(document.getElementById('DragDropZoneParent'), ['dragDropAdditionalInfo']);
    scope.updateAdditionalInfoUI(dropzoneId);
}

function doAllFilesHaveDocType(dropZoneId) {
	var files = retrieveFiles(dropZoneId);

	for (var i = 0; i < files.length; i++) {
		if (typeof(files[i].docTypeId) == 'undefined') {
			return false;
		}
	}

	return true;
}