﻿var CredentialsPopup = (function () {
    var credentialTemplate;

    jQuery(function ($) {
        $.get(ML.VirtualRoot + "/newlos/Common/CredentialsPopup.html", function (data) {
            credentialTemplate = $.template(data);
        });

        $('body').on('click', '.OkCredentialsBtn', function () {
            var popup = $(this).closest('#Credentials');
            var accountId = popup.find('#AccountId').val() || '';
            var userName = popup.find('#Username').val() || '';
            var password = popup.find('#Password').val() || '';
            if ((popup.find('#AccountIdRow').is(':visible') && popup.find('#AccountId').hasClass('RequiredInput') && accountId === '') ||
                (popup.find('#UsernameRow').is(':visible') && userName === '') ||
                (popup.find('#PasswordRow').is(':visible') && password === '')) {
                alert("Please enter values for all required fields.");
            }
            else {
                LQBPopup.Return({ AccountId: accountId, Username: userName, Password: password });
            }
        });

        $('body').on('click', '.CancelCredentialsBtn', function () {
            LQBPopup.Return(null);
        });

        $('body').on('change', '.RequiredInput', function () {
            $(this).siblings('.RequiredImg').toggle(this.value === '');
        });
    });

    function _createCredentialsPopup(header, usesAcctId, requiresAcctId, needBasicCreds) {
        return $.tmpl(credentialTemplate,
            {
                Header: header,
                UsesAcctId: usesAcctId,
                RequresAcctId: requiresAcctId,
                NeedBasicCreds: needBasicCreds
            });
    }

    return {
        CreateCredentialsPopup: _createCredentialsPopup
    };
})();

