"use strict";

(function ($) {
    $(document).on("DOMContentLoaded", function () {
        if (document.getElementById("m_errorMessage") != null) {
            alert(document.getElementById("m_errorMessage").value);
        }

        var pricePolicyTree = $("#pricePolicyTree");
        var id2Policy = state.policies.reduce(function (id2Policy, policy) {
            id2Policy[policy.PricePolicyID] = policy;
            return id2Policy;
        }, {});
        $("#btnOK").on("click", function (event) {
            event.currentTarget.disabled = true;
            f_save();
            return false;
        });

        if (m_Collapse != null) {
            $(m_Collapse).on("change", function (event) {
                state.collapseToAssociated = event.currentTarget.checked;
                pricePolicyTree.dynatree("redraw");
                return false;
            });
        }

        state.collapseToAssociated = m_Collapse == null || m_Collapse.checked;
        renderTree(state.mode, state.policies);

        function renderTree(mode, policies) {
            try {
                pricePolicyTree.dynatree("destroy");
            } catch (e) { }

            var dTree = list2DynaTree(policies);
            var onCustomRender;

            switch (mode) {
                case AssociationTypeForUi.Program:
                    onCustomRender = renderTree4Program;
                    break;

                case AssociationTypeForUi.PriceGroup:
                    onCustomRender = renderTree4PriceGroup;
                    break;

                case AssociationTypeForUi.ProgramInPriceGroup:
                case AssociationTypeForUi.ProgramInPriceGroupBatch:
                    onCustomRender = renderTree4ProgramInPriceGroup;
                    break;

                case AssociationTypeForUi.PmiProduct:
                    onCustomRender = renderTree4PmiProduct;
                    break;

                default:
                    break;
            }

            pricePolicyTree.dynatree({
                children: [dTree],
                onCustomRender: onCustomRender,
                onPostInit: function onPostInit() {
                    pricePolicyTree.dynatree("getRoot").visit(function (node) {
						if (node.getLevel() < 2) node.expand(true);
                    });
                }
            });
        }

        function renderTree4Program(node) {
            if (node.data.key == "root") return node.data.title;
            var policy = id2Policy[node.data.key];

            if (policy == null) {
                return node.data.title;
            }

            policy.parentExpanded = state.collapseToAssociated && policy.hasAssociations;
            policy.childExpanded = !state.collapseToAssociated && policy.IsExpandedNode;
            if (policy.parentExpanded) node.getParent().expand(true);
            var pre = policy.IsPublished ? [
              "<span class=\"pre-node ".concat(!policy.parentExpanded ? "" : "expanded", "\">"),
                "<span class=\"pre-node-2\">",
                  "<input type=\"radio\" disabled ".concat(policy.bSel ? "checked" : "", ">b"),
                  "<input type=\"radio\" disabled ".concat(policy.mSel ? "checked" : "", ">m"),
                "</span>",
                "<input type=\"checkbox\" value=\"Y\" ".concat(policy.ySel ? "checked" : "", " ").concat(policy.yDis ? "disabled" : "", " onchange=\"f_onSet(event)\">Y"),
                "<input type=\"checkbox\" value=\"N\" ".concat(policy.nSel ? "checked" : "", " ").concat(policy.nDis ? "disabled" : "", " onchange=\"f_onSet(event)\">N"),
                "".concat(!state.isMaster ? "" : "<input type=\"checkbox\" value=\"FN\" ".concat(policy.forceNo ? "checked" : "", " ").concat(policy.forceNoDisable ? "disabled" : "", " onchange=\"f_onSet(event)\">FN")),
              "</span>"].join("\n")
              : policy.parentExpanded ? "<span class=\"policy-no-attachable\">Policy is no longer attachable</span>"
              : "";
            var content = "".concat(pre, " ").concat(policy.PricePolicyDescription);
            return policy.FNProgramId == guidEmpty ? content : "<span class=\"policy-force-no\">".concat(content, " ( Disassociated via master )<span>");
        }

        function renderTree4PriceGroup(node) {
            if (node.data.key == "root") return node.data.title;
            var policy = id2Policy[node.data.key];

            if (policy == null) {
                return node.data.title;
            }

            policy.parentExpanded = policy.PriceGroupAssociateType != E_TriState.Blank && (!policy.IsPublished || state.collapseToAssociated);
            policy.childExpaned = !state.collapseToAssociated && policy.IsExpandedNode;
            var pre = policy.IsPublished ? [
              "<span class=\"pre-node ".concat(!policy.parentExpanded ? "" : "expanded", "\">"),
                "<input type=\"checkbox\" value=\"Y\" ".concat(policy.ySel ? "checked" : "", " onchange=\"f_onSet(event)\">Y"),
                "<input type=\"checkbox\" value=\"N\" ".concat(policy.nSel ? "checked" : "", " onchange=\"f_onSet(event)\">N"),
              "</span>"].join("\n")
              : policy.PriceGroupAssociateType != E_TriState.Blank ? "<span class=\"policy-no-attachable\">Policy is no longer attachable</span>"
              : "";
            return "".concat(pre, " ").concat(policy.PricePolicyDescription);
        }

        function renderTree4ProgramInPriceGroup(node) {
            if (node.data.key == "root") return node.data.title;
            var policy = id2Policy[node.data.key];

            if (policy == null) {
                return node.data.title;
            }

            var isBatchMode = state.mode == AssociationTypeForUi.ProgramInPriceGroupBatch;
            policy.parentExpanded = state.collapseToAssociated && (policy.hasAssociations || !policy.IsPublished && !isBatchMode && policy.PriceGroupProgramAssociateType != E_TriState.Blank);
            policy.childExpaned = !state.collapseToAssociated && policy.IsExpandedNode;
            if (policy.parentExpanded) node.getParent().expand(true);
            var pre = policy.IsPublished ? [
              "<span class=\"pre-node ".concat(!policy.parentExpanded ? "" : "expanded", "\">"),
                "<span class=\"pre-node-2\">", "".concat(isBatchMode ? "" :
                  "<input type=\"radio\" disabled ".concat(policy.lSel ? "checked" : "", ">L")),
                  "&nbsp;&nbspPG:",
                  "<input type=\"radio\" disabled ".concat(policy.pGySel ? "checked" : "", ">Y"),
                  "<input type=\"radio\" disabled ".concat(policy.pGySel ? "checked" : "", ">N"),
                "</span>",
                "<input type=\"checkbox\" value=\"Y\" ".concat(policy.ySel ? "checked" : "", " onchange=\"f_onSet(event)\">Y"),
                "<input type=\"checkbox\" value=\"N\" ".concat(policy.nSel ? "checked" : "", " onchange=\"f_onSet(event)\">N"),
                "".concat(!isBatchMode ? "" : "<input type=\"checkbox\" value=\"C\" ".concat(policy.clearSel ? "checked" : "", " onchange=\"f_onSet(event)\">Clear")),
              "</span>"].join("\n")
              : !isBatchMode && policy.PriceGroupProgramAssociateType != E_TriState.Blank ? "<span class=\"policy-no-attachable\">Policy is no longer attachable</span>"
              : "";
            return "".concat(pre, " ").concat(policy.PricePolicyDescription);
        }

        function renderTree4PmiProduct(node) {
            if (node.data.key == "root") return node.data.title;
            var policy = id2Policy[node.data.key];

            if (policy == null) {
                return node.data.title;
            }

            policy.hasAssociations = policy.AssocOverrideTri == E_TriState.Yes || policy.AssocOverrideTri == E_TriState.No || policy.InheritVal;
            policy.parentExpanded = state.collapseToAssociated && policy.hasAssociations;
            if (policy.parentExpanded) node.getParent().expand(true);
            var pre = policy.IsPublished ? [
              "<span class=\"pre-node ".concat(!policy.parentExpanded ? "" : "expanded", "\">"),
                "<span class=\"pre-node-2\">",
                  "<input type=\"radio\" disabled ".concat(policy.InheritVal ? "checked" : "", ">m"),
                "</span>",
                "<input type=\"checkbox\" value=\"Y\" ".concat(policy.ySel ? "checked" : "", " onchange=\"f_onSet(event)\">Y"),
                "<input type=\"checkbox\" value=\"N\" ".concat(policy.nSel ? "checked" : "", " onchange=\"f_onSet(event)\">N"),
              "</span>"].join("\n")
              : policy.parentExpanded ? "<span class=\"policy-no-attachable\">Policy is no longer attachable</span>"
              : "";
            return "".concat(pre, " ").concat(policy.PricePolicyDescription);
        }

        function setY(node, value) {
            var policy = id2Policy[node.data.key];

            if (policy == null) {
                return;
            }

            policy.ySel = value;

            if (value) {
                policy.nSel = false;
                if (policy.forceNoDisable === true) policy.forceNo = false;
                policy.clearSel = false;
            }
        }

        function setN(node, value) {
            var policy = id2Policy[node.data.key];

            if (policy == null) {
                return;
            }

            policy.nSel = value;

            if (value) {
                policy.ySel = false;
                if (node.forceNoDisable === true) policy.forceNo = false;
                policy.clearSel = false;
            }
        }

        function setFN(node, value) {
            var policy = id2Policy[node.data.key];

            if (policy == null) {
                return;
            }

            policy.forceNo = value;

            if (value) {
                policy.ySel = policy.nSel = false;
            }
        }

        function setClear(node, value) {
            var policy = id2Policy[node.data.key];

            if (policy == null) {
                return;
            }

            policy.clearSel = value;

            if (value) {
                policy.ySel = policy.nSel = false;
            }
        }

        function list2DynaTree(policies) {
            var p2cMap = {}; // Dictionary map parentPolicy.PricePolicyID to it's children DynatreeNode[]

            var id2Node = {}; // Dictionary map       policy.PricePolicyID to coresponding  DynatreeNode

            var rootChildren = policies.map(function (policy) {
                var node = {
                    key: policy.PricePolicyID,
                    title: policy.PricePolicyDescription,
					icon: false,
					expand: policy.childExpaned === true,
					parentExpanded: policy.parentExpanded || policy.ySel || policy.bSel || policy.mSel || policy.nSel
                };
                id2Node[policy.PricePolicyID] = node;
                if (policy.ParentPolicyId == null) return node;
                var children = p2cMap[policy.ParentPolicyId] = p2cMap[policy.ParentPolicyId] || [];
                children.push(node);
                return null;
            }).filter(function (n) {
                return n != null;
            });
            Object.keys(p2cMap).forEach(function (pId) {
                var p = id2Node[pId];

                if (p == null) {
                    return;
                }

                p.children = p2cMap[pId];
            });
            rootChildren.forEach(setExpand);

            function setExpand(p) {
                if (p.children != null) {
                    p.children.forEach(setExpand);
                }

                p.expand = p.expand || p.children != null && p.children.some(function (c) {
                    return c.expand || c.parentExpanded;
                }); // return p.expand || (p.children != null && (p.children.some(c => c.expand || c.parentExpanded === true) || p.children.some(checkExpand)));
            }

            return {
                key: "root",
                title: "All Pricing Rule Groups",
                icon: false,
                children: rootChildren
            };
        }

        window.serializeChanges = function serializeChanges() {
            if (state.mode == AssociationTypeForUi.ProgramInPriceGroupBatch) {
                return state.policies.map(function (policy) {
                    return !(policy.ySel || policy.nSel || policy.clearSel) ? "" : "".concat(policy.PricePolicyID, ":").concat(policy.ySel ? "Y" : policy.nSel ? "N" : "C");
                }).filter(function (x) {
                    return !!x;
                }).join(",");
            }

            return state.policies.map(function (policy) {
				return !policy.IsPublished || !(policy.ySel || policy.nSel || policy.forceNo) ? "" : "".concat(policy.PricePolicyID, ":").concat((state.isMaster && policy.forceNo) ? "FN" : policy.ySel ? "Y" : "N");
            }).filter(function (x) {
                return !!x;
            }).join(",");
        };

        window.f_save = function f_save() {
            var args = {
                ProductId: productId,
                EditorMode: state.mode.toString(),
                Value: serializeChanges()
            };

            switch (state.mode) {
                case AssociationTypeForUi.Program:
                    args.TurnOptimizerOffChk = TurnOptimizerOffChk == null || TurnOptimizerOffChk.checked ? "True" : "False";
                    break;

                case AssociationTypeForUi.PmiProduct:
                    args.PmiProductID = m_pmiProductId;
                    break;

                case AssociationTypeForUi.ProgramInPriceGroupBatch:
                    args.ProgramList = m_ProgramList;
                    args.KeepInSync = m_KeepInSync != null && m_KeepInSync.checked;

                case AssociationTypeForUi.ProgramInPriceGroup:
                case AssociationTypeForUi.PriceGroup:
                    args.PriceGroupId = m_priceGroupID;
                    break;
            }

            var result = gService.main.call("Save", args, true, true);
            document.getElementById("btnOk").disabled = false;

            if (!result.error) {
                onClosePopup();
            } else {
                alert(result.UserMessage);
            }
        };

        window.f_onSet = function f_onSet(event) {
            var checkbox = event.currentTarget;
            var node = $.ui.dynatree.getNode(checkbox);

            switch (checkbox.value) {
                case "Y":
                    setY(node, checkbox.checked);
                    break;

                case "N":
                    setN(node, checkbox.checked);
                    break;

                case "FN":
                    setFN(node, checkbox.checked);
                    break;

                case "C":
                    setClear(node, checkbox.checked);
                    break;

                default:
                    break;
            }

            node.render();
            return false;
        };
    });
})(jQuery);
