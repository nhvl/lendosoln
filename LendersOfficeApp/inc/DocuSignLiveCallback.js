﻿angular.module('DocuSignCallback', ['LqbForms'])

.controller('LiveCallbackController', function($scope, $window, $interval, gService, LqbUrl) {
    gService.register(LqbUrl.root + '/newlos/services/docusign/SubmitDocuSignEnvelopeService.aspx', 'main');

    var search = LqbUrl.getSearchObject($window.location.href);
    $scope.state = search.state;
    $scope.status = 'loading';
    $scope.secondsToAutoRedirect = 10;

    $scope.closeWindow = function() { $window.close(); };
    $scope.continueSigning = function() {
        if ($scope.status === 'success') {
            $window.location.href = $scope.recipientViewUrl;
        }
        else if ($scope.status === 'stateError') {
            var args = {
                loanid: search.loanid,
                envelopeId: search.envelopeId,
                signingOrder: search.signingOrder,
                event: search.event,
                state: $scope.state
            };
            gService.main.callAsyncSimple('GetRecipientView', args, getViewSuccess, getViewFailure);
        }
        else if ($scope.status === 'systemError' || $scope.status === 'eventError') {
            var args = {
                loanid: search.loanid,
                envelopeId: search.envelopeId,
                signingOrder: search.signingOrder,
                state: $scope.state
            };
            gService.main.callAsyncSimple('GetRecipientView', args, getViewSuccess, getViewFailure);
        }
    };

    $scope.continueMessage = function() {
        var message = '';
        if ($scope.status === 'success') {
            message = 'Continue Signing with ' + $scope.nextRecipient.name;
            if ($scope.autoRedirect)
                message = message + ' (' + $scope.secondsToAutoRedirect + ')';
            return message;
        }
        else if ($scope.status === 'stateError') {
            return 'Begin Signing with ' + $scope.nextRecipient.name;
        }

        return 'Continue Signing';
    };

    $scope.closeMessage = function() {
        var message = '';
        if ($scope.status === 'complete') {
            message = 'Close';
            if ($scope.autoRedirect)
                message = message + ' (' + $scope.secondsToAutoRedirect + ')';
        }
        else
            message = 'Cancel';

        return message;
    };

    function getViewSuccess(response)
    {
        $scope.$apply(function() {
            $scope.loading = false;
            if (response.value) {
                var view = angular.fromJson(response.value.RecipientViewModel);
                if (view.status === 'error' && view.errorType === 'state') {
                    $scope.status = 'stateError';
                    $scope.state = view.state;
                    $scope.nextRecipient = view.nextRecipient;
                }
                else if (view.status === 'error' && view.errorType === 'signingEvent') {
                    $scope.status = 'eventError';
                    $scope.state = view.state;
                    $scope.signingError = { code: view.eventCode, description: view.errorMessage };
                    $scope.lastRecipient = view.lastRecipient;
                }
                else if (view.status === 'error' && view.errorType === 'system') {
                    $scope.status = 'systemError';
                    $scope.state = view.state;
                    $scope.errorMessage = view.errorMessage;
                    $scope.lastRecipient = view.lastRecipient;
                    $scope.nextRecipient = view.nextRecipient;
                }
                else if (view.status === 'complete') {
                    $scope.status = 'complete';
                    $scope.lastRecipient = view.lastRecipient;
                    var seconds = $scope.secondsToAutoRedirect;
                    $interval(function() { $scope.secondsToAutoRedirect--; }, 1000, seconds)
                        .then($window.close);
                    $scope.autoRedirect = true;
                }
                else if (view.status === 'success') {
                    $scope.status = 'success';
                    $scope.lastRecipient = view.lastRecipient;
                    $scope.nextRecipient = view.nextRecipient;
                    $scope.recipientViewUrl = view.recipientViewUrl;
                    var seconds = $scope.secondsToAutoRedirect;
                    $interval(function() { $scope.secondsToAutoRedirect--; }, 1000, seconds)
                        .then($scope.continueSigning);
                    $scope.autoRedirect = true;
                }
            }
        });
    }

    function getViewFailure(response)
    {
        $scope.status = 'systemError';
        $scope.errorMessage = response.UserMessage;
    }
    var args = {
        loanid: search.loanid,
        envelopeId: search.envelopeId,
        signingOrder: search.signingOrder,
        event: search.event,
        state: $scope.state
    };
    gService.main.callAsyncSimple('GetRecipientView', args, getViewSuccess, getViewFailure);
});