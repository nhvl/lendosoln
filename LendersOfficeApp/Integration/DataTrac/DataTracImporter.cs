using System;
using System.Collections;
using System.Reflection;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web.SessionState;
using LendersOfficeApp.common;
using DataAccess;
using DataTrac;
using LendersOffice.Common;
using LendersOffice.Conversions;
using System.IO;
using LendersOffice.Admin;
using LendersOffice.Security;
using System.Collections.Generic;
using LendersOffice.Constants;

namespace LendersOfficeApp.Integration.DataTrac
{
	public class DataTracImporter : DataTracExchange
	{
		#region Variables
		protected override string FilePath
		{
			set { m_filePath = value; }
		}
		#endregion

        public DataTracImporter(Guid loanId, Guid brokerId, string dtLoginName, string dtPassword, string dtLoanNumber, E_DataTracOriginatorType origType)
            : base(loanId, brokerId, dtLoginName, dtPassword, dtLoanNumber, origType)
		{
			FilePath = m_pathToDataTrac + "/" + Guid.NewGuid() + ".FNM";
		}

		public void Import()
		{
            DataTracServer dtServer = null;

			try
			{
				if(!DataTracValidationService.IsValidPath(m_filePath))
					throw new DataTracException("Invalid Loan Number - please enter a DataTrac Loan Number without invalid filename characters.", "");
				
				DataTracData dtData = new DataTracData(m_loanId);
				dtData.InitSave(ConstAppDavid.SkipVersionCheck);
				dtServer = new DataTracServer(m_dtLoginNm, m_dtPassword, m_pathToDataTrac, m_dataTracWebserviceURL);
                m_webserviceVersion = dtServer.WebserviceVersion;
				CAppData dataApp = null;
				if (dtData.nApps != 0)
					dataApp = dtData.GetAppData(0);

				ArrayList extraMappedFields = dtData.Fields;

				string borrowerSSN = "";
				if(dataApp != null)
					borrowerSSN = dataApp.aBSsn;

                GridFieldHandler gfh = new GridFieldHandler(E_DataTracExchangeType.Import, dtData);
                System.Xml.XmlDocument gridRequestXml = gfh.CreateGridRequestXml();
                XmlDataHandler xmlDH = new XmlDataHandler(dtData);
                string[] exportResult = dtServer.Import(m_filePath, m_dtLoanNum, E_DataTracExportType.THTW, ref extraMappedFields, borrowerSSN, gridRequestXml, xmlDH.CreateImportXmlData());
				string fannieMaeFileContent = exportResult[0];
                string gridXml = exportResult[1];
                
                byte[] binaryData = Convert.FromBase64String(fannieMaeFileContent);
				System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
				
				// First pull in the data from the fannie mae file that was exported from DataTrac
				FannieMae32Importer fnmImporter = new FannieMae32Importer();
				fnmImporter.ImportIntoExisting(enc.GetString(binaryData), m_loanId);

				//Now pull in the extra mapped fields that are not part of the fannie mae file exported from DataTrac
				Type loanDataType = typeof(DataTracData);
				Type borrowerDataType = typeof(CAppData);

				foreach(IntegrationField field in extraMappedFields)
				{
					PropertyInfo myPropertyInfo = null;
					string fieldName = field.PmlFieldName + "_rep";
					if(field.PmlFieldName.ToLower().StartsWith("a"))
					{
                        myPropertyInfo = borrowerDataType.GetProperty(fieldName);
						if(myPropertyInfo != null)
						{
							if(!myPropertyInfo.CanWrite)
								continue;

							myPropertyInfo.SetValue(dataApp, field.GetPmlFieldValue(field.ThirdPartyValue), null);
						}
						else
						{
							if((myPropertyInfo = borrowerDataType.GetProperty(field.PmlFieldName)) != null)
							{
								if(!myPropertyInfo.CanWrite)
									continue;

								myPropertyInfo.SetValue(dataApp, Cast(myPropertyInfo.PropertyType.ToString(), field.GetPmlFieldValue(field.ThirdPartyValue), field.PmlFieldName), null);
							}
							else
								Tools.LogBug("(db) Unable to load field in DataTrac importer (myPropertyInfo is null).  FieldName: " + field.PmlFieldName);
						}
					}
					else if(field.PmlFieldName.ToLower().StartsWith("s"))
					{
						myPropertyInfo = loanDataType.GetProperty(fieldName);
						
						if(myPropertyInfo != null)
						{
							if(!myPropertyInfo.CanWrite)
								continue;
							myPropertyInfo.SetValue(dtData, field.GetPmlFieldValue(field.ThirdPartyValue), null);
						}
						else
						{
							if((myPropertyInfo = loanDataType.GetProperty(field.PmlFieldName)) != null)
							{
								if(!myPropertyInfo.CanWrite)
									continue;
								myPropertyInfo.SetValue(dtData, Cast(myPropertyInfo.PropertyType.ToString(), field.GetPmlFieldValue(field.ThirdPartyValue), field.PmlFieldName), null);
							}
							else
								Tools.LogBug("(db) Unable to load field in DataTrac importer (myPropertyInfo is null).  FieldName: " + field.PmlFieldName);
						}
					}
					else
						Tools.LogBug("(db) Unknown field type in DataTrac importer.  FieldName: " + field.PmlFieldName);
				}

                m_warningMessagesFromDT = dtServer.UserWarningMessages;
				m_errorMessagesFromDT = dtServer.UserErrorMessages;
                dtData.Save();
                
                try
                {
                    gfh.SetFieldsFromGridXml(gridXml, m_loanId);
                }
                catch (Exception e)
                {
                    Tools.LogError("Unable to save repeating data while performing DataTrac import.  Error details: " + e.ToString());
                    m_warningMessagesFromDT.Add("Unable to save all repeating data (assets, underwriting conditions, etc) - please let us know if this happens again.");
                }
			}
			catch(System.Reflection.TargetInvocationException t)
			{
				throw t.InnerException;
			}
			catch(Exception)
			{
				throw;
			}
		}

		private object Cast(string type, string data, string fieldName)
		{
			object returnData = null;
			if(data == null || data.TrimWhitespaceAndBOM().Equals(""))
				return null;
			try
			{
				switch(type)
				{
					case "System.Decimal":
						returnData = System.Decimal.Parse(data);
						break;
					case "System.Int32":
						returnData = System.Int32.Parse(data);
						break;
					case "System.String":
						returnData = data;
						break;
					case "System.Boolean":
						if(data.Equals("0"))
							data = "false";
						else if(data.Equals("1"))
							data = "true";
						returnData = System.Boolean.Parse(data);
						break;
					case "System.DateTime":
						returnData = System.DateTime.Parse(data);
						break;
					case "System.Double":
						returnData = System.Double.Parse(data);
						break;
					case "System.Char":
						returnData = System.Char.Parse(data);
						break;
					case "System.Byte":
						returnData = System.Byte.Parse(data);
						break;
					case "DataAccess.E_sProdDocT":
						returnData = Enum.Parse(typeof(DataAccess.E_sProdDocT), data, true);
						break;
					case "DataAccess.E_sGseSpT":
						returnData = Enum.Parse(typeof(DataAccess.E_sGseSpT), data, true);
						break;
					case "DataAccess.E_aOccT":
						returnData = Enum.Parse(typeof(DataAccess.E_aOccT), data, true);
                        break;
					default:
						Tools.LogBug("Type " + type + " not mapped in DataTrac Interface - field will be skipped");
						break;
				}
			}
			catch(Exception e)
			{
				string msg = string.Format("DataTrac Interface Data Parsing Error - field '{0}' with value '{1}' will be skipped.  Details: {2}", fieldName, data, e.ToString());
				Tools.LogBug(msg);	
			}

			return returnData;
		}
	}
}