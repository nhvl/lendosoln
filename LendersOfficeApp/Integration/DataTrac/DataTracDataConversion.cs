using System;
using System.Collections;
using DataAccess;
using System.Xml;

namespace LendersOfficeApp.Integration.DataTrac
{
	/*public enum E_IntegrationFieldType
	{
		LoanFields = 0,
		BorrowerFields = 1
	}
	
	public class IntegrationFields
	{
		#region Variables
		private ArrayList m_loanFields = null;
		private ArrayList m_borrowerFields = null;

		private ArrayList m_pmlLoanFieldIds = null;
		private ArrayList m_pmlBorrowerFieldIds = null;
		private ArrayList m_thirdPartyFieldIds = null;

		public ArrayList PmlBorrowerFields
		{
			get { return m_pmlBorrowerFieldIds; }
		}

		public ArrayList PmlLoanFields
		{
			get { return m_pmlLoanFieldIds; }
		}

		public ArrayList ThirdPartyFields
		{
			get { return m_thirdPartyFieldIds; }
		}

		public ArrayList LoanFields
		{
			get { return m_loanFields; }
		}

		public ArrayList BorrowerFields
		{
			get { return m_borrowerFields; }
		}
		#endregion

		public IntegrationFields()
		{
			m_loanFields = new ArrayList();
			m_borrowerFields = new ArrayList();
			m_pmlLoanFieldIds = new ArrayList();
			m_pmlBorrowerFieldIds = new ArrayList();
			m_thirdPartyFieldIds = new ArrayList();
		}

		private bool IsDuplicate(string pmlfieldName, string thirdPartyFieldName)
		{
			// diana check to make sure there are none where both are mapped - maybe take this out completely and allow duplicates
			// Return true if both the PML field and 3rd party field are already mapped.  If only one is, that's ok, some fields are used for multiple things.
			return (m_pmlLoanFieldIds.Contains(pmlfieldName) || m_pmlBorrowerFieldIds.Contains(pmlfieldName)) && m_thirdPartyFieldIds.Contains(thirdPartyFieldName);
		}

		public void AddField(IntegrationField iField, E_IntegrationFieldType fieldType)
		{
			if(fieldType == E_IntegrationFieldType.BorrowerFields)
			{
				if(!IsDuplicate(iField.PmlFieldName, iField.ThirdPartyFieldName))
				{
					m_borrowerFields.Add(iField);
					m_pmlBorrowerFieldIds.Add(iField.PmlFieldName);
					m_thirdPartyFieldIds.Add(iField.ThirdPartyFieldName);
				}
			}
			else if(fieldType == E_IntegrationFieldType.LoanFields)
			{
				if(!IsDuplicate(iField.PmlFieldName, iField.ThirdPartyFieldName))
				{
					m_loanFields.Add(iField);
					m_pmlLoanFieldIds.Add(iField.PmlFieldName);
					m_thirdPartyFieldIds.Add(iField.ThirdPartyFieldName);
				}
			}
			else
				Tools.LogBug(String.Format("(DB) Invalid E_IntegrationFieldType enum type {0} - Unable to add field to Integration Fields list with the following attributes: {1}", fieldType, iField.ToString()));
		}
	}
	
	public class IntegrationField
	{
		#region Variables
		private string		m_pmlFieldName = ""; //required
		private string		m_thirdPartyFieldName = ""; //required
		private string		m_description = "";
		private string		m_defaultPmlMapValue = "";
		private string		m_defaultThirdPartyMapValue = "";
		private string		m_whereToFindIt = "";
		private string		m_notes = "";
		private bool		m_isValid = true;
		private Hashtable	m_pmlFieldMap = null;
		private Hashtable	m_thirdPartyFieldMap = null;
		private bool		m_mappingExists = false;

		public string PmlFieldName
		{
			get { return m_pmlFieldName; }
		}

		public string ThirdPartyFieldName
		{
			get { return m_thirdPartyFieldName; }
		}

		public string Description
		{
			get { return m_description; }
		}

		public string WhereToFindIt
		{
			get { return m_whereToFindIt; }
		}

		public string Notes
		{
			get { return m_notes; }
		}

		public bool IsValid
		{
			get { return m_isValid; }
		}

		#endregion

		public IntegrationField(string pmlFieldName, string thirdPartyFieldName, string description, string defaultPmlMapValue, string defaultThirdPartyMapValue, string whereToFindIt, string notes)
		{
			m_pmlFieldName = pmlFieldName;
			m_thirdPartyFieldName = thirdPartyFieldName;
			m_description = description;
			m_defaultPmlMapValue = defaultPmlMapValue;
			m_defaultThirdPartyMapValue = defaultThirdPartyMapValue;
			m_whereToFindIt = whereToFindIt;
			m_notes = notes;
			m_pmlFieldMap = new Hashtable();
			m_thirdPartyFieldMap = new Hashtable();

			ValidateFields();
		}

		//diana check if lowercase will mess this up
		public void InsertPmlFieldMap(string pmlFieldValue, string thirdPartyFieldValue)
		{
			m_mappingExists = true;
			pmlFieldValue = pmlFieldValue.ToLower();
			thirdPartyFieldValue = thirdPartyFieldValue.ToLower();
			if(!m_pmlFieldMap.Contains(pmlFieldValue))
				m_pmlFieldMap.Add(pmlFieldValue, thirdPartyFieldValue);
		}

		//diana check if lowercase will mess this up
		public void InsertThirdPartyFieldMap(string pmlFieldValue, string thirdPartyFieldValue)
		{
			m_mappingExists = true;
			pmlFieldValue = pmlFieldValue.ToLower();
			thirdPartyFieldValue = thirdPartyFieldValue.ToLower();
			if(!m_thirdPartyFieldMap.Contains(thirdPartyFieldValue))
				m_thirdPartyFieldMap.Add(thirdPartyFieldValue, pmlFieldValue);
		}

		//diana check if lowercase will mess this up
		public string GetThirdPartyFieldValue(string pmlFieldValue)
		{
			pmlFieldValue = pmlFieldValue.ToLower();
			
			if(m_mappingExists)
				Tools.LogWarning("Diana pmlfieldValue: " + pmlFieldValue + ", " + m_pmlFieldMap[pmlFieldValue].ToString());
			
			if(m_mappingExists == false)
				return pmlFieldValue;
			else if(m_pmlFieldMap.Contains(pmlFieldValue))
				return m_pmlFieldMap[pmlFieldValue].ToString();
			else
			{
				Tools.LogWarning("Diana couldn't find pml field value: " + pmlFieldValue + ", defaulting to: " + m_defaultThirdPartyMapValue);
				return m_defaultThirdPartyMapValue;
			}
		}

		//diana check if lowercase will mess this up
		//diana check the order to make sure things below don't overwrite things above unless they need to
		public string GetPmlFieldValue(string thirdPartyFieldValue)
		{
			thirdPartyFieldValue = thirdPartyFieldValue.ToLower();
			if(m_mappingExists == false)
				return thirdPartyFieldValue;
			else if(m_thirdPartyFieldMap.Contains(thirdPartyFieldValue))
				return m_thirdPartyFieldMap[thirdPartyFieldValue].ToString();
			else
				return m_defaultPmlMapValue; //diana - baaaaaad set ignore type
		}

		private void ValidateFields()
		{
			if(m_pmlFieldName.Trim().Equals(""))
			{
				m_isValid = false;
				string msg = String.Format("(DB) Unable to add field to Third Party integration field list from xml config file - the PmlField attribute is blank.{0}  Fields: {1}", Environment.NewLine, this.ToString());
				Tools.LogBug(msg);
			}
			if(m_thirdPartyFieldName.Trim().Equals(""))
			{
				m_isValid = false;
				string msg = String.Format("(DB) Unable to add field to Third Party integration field list from xml config file - the ThirdPartyField attribute is blank.{0}  Fields: {1}", Environment.NewLine, this.ToString());
				Tools.LogBug(msg);
			}
		}

		public override string ToString()
		{
			return String.Format("PmlFieldName = {0}, ThirdPartyFieldName = {1}, Description = {2}, WhereToFindIt = {3}, Notes = {4}", m_pmlFieldName, m_thirdPartyFieldName, m_description, m_whereToFindIt, m_notes);
		}
	}
	
	
	public class ThirdPartyConversion
	{
		#region Variables
		//diana pull in dynamically
		private static string m_fieldXmlFilePath = System.AppDomain.CurrentDomain.BaseDirectory + "/Integration/DataTrac/PmlAndDataTracFields.xml.config";
		private IntegrationFields m_integrationFields = null;

		public ArrayList LoanFields
		{
			get 
			{ 
				if(m_integrationFields == null)
					return new ArrayList();
				else
					return m_integrationFields.LoanFields;
			}
		}

		public ArrayList BorrowerFields
		{
			get 
			{ 
				if(m_integrationFields == null)
					return new ArrayList();
				else
					return m_integrationFields.BorrowerFields;
			}
		}
		
		public ArrayList PmlBorrowerFieldIDs
		{
			get 
			{ 
				if(m_integrationFields == null)
					return new ArrayList();
				else
					return m_integrationFields.PmlBorrowerFields;
			}
		}

		public ArrayList PmlLoanFieldIDs
		{
			get 
			{ 
				if(m_integrationFields == null)
					return new ArrayList();
				else
					return m_integrationFields.PmlLoanFields;
			}
		}
		#endregion
		
		public ThirdPartyConversion()
		{
			m_integrationFields = new IntegrationFields();
			LoadFields();
		}

		private string GetFieldElementValue(XmlElement fieldElement, string fieldName)
		{
			if(fieldElement.HasAttribute(fieldName))
				return fieldElement.GetAttribute(fieldName);
			else
				return "";
		}

		private void LoadFields()
		{
			XmlDocument doc = new XmlDocument();
			doc.Load(m_fieldXmlFilePath);

			XmlElement root = (XmlElement) doc.SelectSingleNode("//IntegrationFields");
			
			foreach (XmlElement categoryElement in root.ChildNodes) //IntegrationFields//category
			{
				try
				{
					string categoryName = categoryElement.GetAttribute("name");
					E_IntegrationFieldType fieldType = (E_IntegrationFieldType)Enum.Parse(typeof(E_IntegrationFieldType), categoryName, true);

					//diana make this only read the <field> ones
					foreach (XmlElement fieldElement in categoryElement.ChildNodes) //IntegrationFields//category//field
					{   
						try
						{
							IntegrationField field = new IntegrationField(fieldElement.GetAttribute("PMLField"), fieldElement.GetAttribute("ThirdPartyField"), GetFieldElementValue(fieldElement, "Description"), GetFieldElementValue(fieldElement, "DefaultPmlMapValue"), GetFieldElementValue(fieldElement, "DefaultDTMapValue"), GetFieldElementValue(fieldElement, "WhereToFindIt"), GetFieldElementValue(fieldElement, "Notes"));		
							
							foreach (XmlElement optionElement in fieldElement.ChildNodes) //IntegrationFields//category//field//value_map_type
							{
								string subCategoryName = optionElement.GetAttribute("Type");
								
								foreach (XmlElement mapElement in optionElement.ChildNodes) //IntegrationFields//category//field//value_map_type//value_map
								{
									if(subCategoryName.Equals("PmlValueMap"))
									{
										field.InsertPmlFieldMap(mapElement.GetAttribute("PMLValue"), mapElement.GetAttribute("ThirdPartyValue"));
									}
									else if(subCategoryName.Equals("ThirdPartyValueMap"))
									{
										field.InsertThirdPartyFieldMap(mapElement.GetAttribute("PMLValue"), mapElement.GetAttribute("ThirdPartyValue"));
									}
									else
									{
										// do nothing - we don't know how to handle this element
										Tools.LogWarning("(db) Unknown element type '" + subCategoryName + "' found in Third Party Conversion - ignoring.");
									}
								}
							}
							
							m_integrationFields.AddField(field, fieldType);
						}
						catch(Exception e)
						{
							Tools.LogError("Error while loading a Pml/IntegrationFields field from the XML field list in file: " + m_fieldXmlFilePath, e);
						}
					}
				}
				catch(Exception e)
				{
					Tools.LogError("Error while loading a Pml/IntegrationFields category from the XML field list in file: " + m_fieldXmlFilePath, e);
				}
			}
		}
	}*/
}