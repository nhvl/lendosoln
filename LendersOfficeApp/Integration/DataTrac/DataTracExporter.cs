using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Web.SessionState;
using LendersOfficeApp.common;
using DataAccess;
using DataTrac;
using LendersOffice.Common;
using LendersOffice.Conversions;
using System.IO;
using LendersOffice.Admin;
using LendersOffice.Security;
using LendersOfficeApp.LegacyLink.CalyxPoint;
using System.Xml;
using System.Data.Common;
using System.Data.SqlClient;

namespace LendersOfficeApp.Integration.DataTrac
{
	public class DataTracExporter: DataTracExchange
	{
		#region Variables
		private int m_numApps = 0;
		private bool m_updateExistingFile = false;
        private bool m_saveDtLpId = false; // We only want to save the DataTrac loan program ID if it has been changed in the UI
        private string m_dtProgramName = "";
        private bool m_exportPricingInfoTo3rdParty = true;

		protected override string FilePath
		{
			set { m_filePath = value; }
		}
		#endregion

        public DataTracExporter(Guid loanId, Guid brokerId, string dtLoginName, string dtPassword, string dtLoanNumber, bool updateExistingFile, E_DataTracOriginatorType origType, string dtProgramName, bool saveDtLpId)
            : base(loanId, brokerId, dtLoginName, dtPassword, dtLoanNumber, origType)
		{
			FilePath = m_pathToDataTrac + "/" + Guid.NewGuid() + ".BRW";
			m_updateExistingFile = updateExistingFile;
            m_dtProgramName = dtProgramName;
            m_saveDtLpId = saveDtLpId;
		}

        public DataTracExporter(Guid loanId, Guid brokerId, string dtLoginName, string dtPassword, string dtLoanNumber, bool updateExistingFile, E_DataTracOriginatorType origType)
            : base(loanId, brokerId, dtLoginName, dtPassword, dtLoanNumber, origType)
        {
            FilePath = m_pathToDataTrac + "/" + Guid.NewGuid() + ".BRW";
            m_updateExistingFile = updateExistingFile;
        }
		
		private string ExportPointFileToString() 
		{
			CPointData dataLoan = new CPointData(m_loanId);
			dataLoan.SetFormatTarget(FormatTarget.PointNative);
			dataLoan.InitLoad();

			NameValueCollectionWrapper rg = new NameValueCollectionWrapper(new NameValueCollection());
			PointInteropLib.DataFile dataFile = new PointInteropLib.DataFile();
			PointDataConversion pointConversion = new PointDataConversion(m_brokerId, dataLoan.sLId);
			using (MemoryStream outputStream = new MemoryStream(100000)) 
			{
				XmlTextWriter xmlWriter = new XmlTextWriter(outputStream, System.Text.Encoding.ASCII);
				xmlWriter.WriteStartElement("PointFormat");
				m_numApps = dataLoan.nApps;
				for (int nApps = 0; nApps < dataLoan.nApps; nApps++) 
				{
					using (MemoryStream stream = new MemoryStream(100000)) 
					{
						if (nApps == 0) 
							xmlWriter.WriteStartElement("Borrower");
						else
							xmlWriter.WriteStartElement("Coborrower");

                        #region OPM 31335 - add description to any liabilities that don't have one
                        CAppData dataApp = dataLoan.GetAppData(nApps);
                        ILiaCollection coll = dataApp.aLiaCollection;

                        foreach (ILiabilityRegular debt in coll.GetSubcollection(true, E_DebtGroupT.Regular))
                        {
                            if (debt.Desc.TrimWhitespaceAndBOM() == "")
                            {
                                debt.Desc = debt.DebtT_rep;
                            }
                        }
                        #endregion

                        pointConversion.TransferDataToPoint(dataLoan, rg, nApps);
						dataFile.WriteSortedStream(stream, rg.GetCollection());

						xmlWriter.WriteCData(Convert.ToBase64String(stream.GetBuffer(), 0, (int) stream.Position));

						xmlWriter.WriteEndElement();
					}
				}
				xmlWriter.WriteEndElement(); // </PointFormat>
				xmlWriter.Flush();
				return System.Text.ASCIIEncoding.ASCII.GetString(outputStream.GetBuffer(), 0, (int)outputStream.Position) ;
			}
		}

        // This function is being created for case 34444, but can be used for others if we need them.  The
        // purpose is to do some overriding of the field mapping from the PmlAndDataTracFields.xml.config file
        // so that we can decide if we want to send the values or not based on broker settings.
        private bool ShouldExportField(string fieldName)
        {
            if (m_exportPricingInfoTo3rdParty == false)
            {
                switch (fieldName.ToLower().TrimWhitespaceAndBOM())
                {
                    case "sbrokcomp1pc":
                    case "snoteirsubmitted":
                    case "snoteir":
                    case "sradjmarginr":
                    case "squalir":
                    case "srlckdd":
                    case "srlckdexpiredd":
                    case "srlckddays":
                        return false;
                    default:
                        return true;
                }
            }

            return true;
        }

		public void Export()
		{
			DataTracServer dtServer = null;
			ArrayList extraMappedFields = new ArrayList();

			try
			{
				string pointData = ExportPointFileToString();
				DataTracData dtData = new DataTracData(m_loanId);
				dtData.InitLoad();
				dtServer = new DataTracServer(m_dtLoginNm, m_dtPassword, m_pathToDataTrac, m_dataTracWebserviceURL);
                m_webserviceVersion = dtServer.WebserviceVersion;
                dtServer.WritePointDataToPath(m_filePath, pointData);

				Type loanDataType = typeof(DataTracData);

				CAppData dataApp = null;
                if (dtData.nApps != 0)
                {
                    dataApp = dtData.GetAppData(0);
                }

                string borrowerSSN = "";
                if (dataApp != null)
                {
                    borrowerSSN = dataApp.aBSsn;
                }

				Type borrowerDataType = typeof(CAppData);

                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerID", m_brokerId)
                                            };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_brokerId, "Get3rdPartyExportSettingsByBrokerID", parameters ))
                {
                    if (reader.Read())
                    {
                        m_exportPricingInfoTo3rdParty = (bool)reader["IsExportPricingInfoTo3rdParty"];
                    }
                }

				foreach(IntegrationField field in dtData.Fields)
				{
					PropertyInfo myPropertyInfo = null;
					string myValue = "";

                    if(!ShouldExportField(field.PmlFieldName))
                    {
                        continue;
                    }
					else if(field.PmlFieldName.ToLower().StartsWith("a"))
					{
                        myPropertyInfo = borrowerDataType.GetProperty(field.PmlFieldName);
						if(myPropertyInfo == null)
						{
							Tools.LogBug("(db) Unable to load field in DataTrac exporter (myPropertyInfo is null).  FieldName: " + field.PmlFieldName);
							continue;
						}
						myValue = myPropertyInfo.GetValue(dataApp, null).ToString();
					}
					else if(field.PmlFieldName.ToLower().StartsWith("s"))
					{
                        myPropertyInfo = loanDataType.GetProperty(field.PmlFieldName);
						if(myPropertyInfo == null)
						{
							Tools.LogBug("(db) Unable to load field in DataTrac exporter (myPropertyInfo is null).  FieldName: " + field.PmlFieldName);
							continue;
						}
						myValue = myPropertyInfo.GetValue(dtData, null).ToString();
					}
					else
					{
						Tools.LogBug("(db) Unknown field type in DataTrac exporter.  FieldName: " + field.PmlFieldName);
					}
					
					string myValueMapped = field.GetThirdPartyFieldValue(myValue);
					field.PmlValue = myValueMapped;

					if((myValueMapped != IGNORE_FIELD) && (myValueMapped.TrimWhitespaceAndBOM() != ""))
					{
						extraMappedFields.Add(field);
					}
                }

                #region Extra fields that must be manually mapped outside of the Point and XML file
                
                IntegrationField newField = new IntegrationField("","und.months_of_reserves_overw", "true", "");
				extraMappedFields.Add(newField);
				
				newField = new IntegrationField("","gen.qual_rate_overw", "true", "");
				extraMappedFields.Add(newField);
				
                if (m_dtProgramName.Equals("") && !string.IsNullOrEmpty(dtData.sDataTracLpId))
                    m_dtProgramName = dtData.sDataTracLpId;

                // OPM 27411 - we are now allowing the users to type in the DataTrac loan program name, so we will save
                // whatever they enter, even if there is no mapping for it in the Price Groups page.  We are also saving it
                // even if it doesn't exist yet in DataTrac, because they might go to DataTrac and enter it upon realizing
                // it doesn't exist yet, and even if not, we don't want to second guess them - if they REALLY want to set it,
                // we will let them.
                if (m_saveDtLpId)
                    DataTracExchange.SaveDataTracLoanProgramName(m_loanId, m_dtProgramName);
                
                string dtProgramID = dtServer.GetProgramIdFromProgramName(m_dtProgramName);
				newField = new IntegrationField("", "gen.programs_id", dtProgramID, "");
				extraMappedFields.Add(newField);

                newField = new IntegrationField("", "gen.branch_type", m_originatorType.ToString(), "");
                extraMappedFields.Add(newField);

                GridFieldHandler gridFieldHandler = new GridFieldHandler(E_DataTracExchangeType.Export, dtData);
                
                #endregion

                XmlDocument exportDataXML = CreateExportDataXml(dtData, borrowerSSN);
                XmlDocument adjustmentData = null;
                if (m_exportPricingInfoTo3rdParty)
                {
                    adjustmentData = AdjustmentsHandler.CreateAdjustmentXml(m_loanId);
                }

                dtServer.Export(m_filePath, m_dtLoanNum, E_DataTracExportType.CALYXFULL, false /*forceAdd*/, extraMappedFields, m_updateExistingFile, gridFieldHandler.CreateGridXml(), adjustmentData, exportDataXML);
				
                m_warningMessagesFromDT = dtServer.UserWarningMessages;
				m_errorMessagesFromDT = dtServer.UserErrorMessages;
			}
			catch(Exception)
			{
				throw;
			}
		}


        public string ExportXmlData()
        {
            DataTracData dtData = new DataTracData(m_loanId);
            dtData.InitLoad();
                       CAppData dataApp = dtData.GetAppData(0);

            GridFieldHandler gridFieldHandler = new GridFieldHandler(E_DataTracExchangeType.Export, dtData);
                            
                string borrowerSSN = "";
                if (dataApp != null)
                {
                    borrowerSSN = dataApp.aBSsn;
                }

            XmlDocument exportDataXML = CreateExportDataXml(dtData, borrowerSSN);
            XmlDocument adjustmentData = null;
            if (m_exportPricingInfoTo3rdParty)
            {
                adjustmentData = AdjustmentsHandler.CreateAdjustmentXml(m_loanId);
            }

            return gridFieldHandler.CreateGridXml().OuterXml;
        }

        private XmlDocument CreateExportDataXml(DataTracData dtData, string borrowerSSN)
        {
            XmlDataHandler xmlDH = new XmlDataHandler(dtData);
            XmlDocument exportDataXML = xmlDH.CreateExportXmlData();
            XmlDataHandler.CreateAndAppendElement(exportDataXML.DocumentElement, "BorrowerSSN", borrowerSSN);

            
            return exportDataXML;
        }
	}
}