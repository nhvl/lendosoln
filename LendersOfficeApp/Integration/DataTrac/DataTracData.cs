namespace LendersOfficeApp.Integration.DataTrac
{
    using System;
    using System.Collections;
    using System.Data;
    using System.IO;
    using System.Web;
    using DataAccess;
    using global::DataTrac;
    using LendersOffice.Drivers.Gateways;

    public class DataTracData : CPageData
    {
        private static CSelectStatementProvider s_selectProvider;
        private static ArrayList m_list = null;
        private static ArrayList m_fields = null;
        private static ThirdPartyConversion m_dtConversion = null;

        public ArrayList FieldNames
        {
            get { return m_list; }
        }

        public ArrayList Fields
        {
            get { return m_fields; }
        }

        public ThirdPartyConversion DataTracConversionData
        {
            get { return m_dtConversion; }
        }

        static DataTracData()
        {
            String form = "";
            string xmlFieldsPath = "";

            if ((HttpContext.Current != null))
                xmlFieldsPath = Tools.GetServerMapPath("~/Integration/DataTrac/PmlAndDataTracFields.xml.config");
            else
                xmlFieldsPath = LendersOffice.Constants.ConstSite.TestDataFolder + "PmlAndDataTracFields.xml.config";

            if (!FileOperationHelper.Exists(xmlFieldsPath))
            {
                xmlFieldsPath = Tools.GetServerMapPath("~/Integration/DataTrac/PmlAndDataTracFields.xml.config");
            }

            m_dtConversion = new ThirdPartyConversion(xmlFieldsPath);
            m_list = m_dtConversion.PmlFieldIDs; // Adds all of the fields from the xml file

            // Still need to manually add some more for fields that are not part of the xml file
            #region Manually added fields outside of XML file
            m_list.Add("aBSsn");
            m_list.Add("sDataTracLpId");
            m_list.Add("aAssetCollection"); //  OPM 27411

            // OPM 30083
            m_list.Add("sLOrigFPc");
            m_list.Add("sLOrigFProps");
            m_list.Add("sLOrigFMb");
            m_list.Add("sLDiscntPc");
            m_list.Add("sLDiscntFMb");
            m_list.Add("sLDiscntProps");
            m_list.Add("sApprF");
            m_list.Add("sApprFProps");
            m_list.Add("sInspectF");
            m_list.Add("sInspectFProps");
            m_list.Add("sMBrokFPc");
            m_list.Add("sMBrokFProps");
            m_list.Add("sMBrokFMb");
            m_list.Add("sMBrokFProps");
            m_list.Add("sCrF");
            m_list.Add("sCrFProps");
            m_list.Add("sTxServF");
            m_list.Add("sTxServFProps");
            m_list.Add("sProcF");
            m_list.Add("sProcFProps");
            m_list.Add("sUwF");
            m_list.Add("sUwFProps");
            m_list.Add("sWireF");
            m_list.Add("sWireFProps");
            m_list.Add("sEscrowF");
            m_list.Add("sEscrowFProps");
            m_list.Add("sDocPrepF");
            m_list.Add("sDocPrepFProps");
            m_list.Add("sTitleInsF");
            m_list.Add("sTitleInsFProps");
            m_list.Add("sRecFPc");
            m_list.Add("sRecFPcProps");
            m_list.Add("sRecFMb");
            m_list.Add("sRecFMbProps");
            m_list.Add("sRecFProps");
            m_list.Add("sfTransformLoPropertyTypeToPml");
            m_list.Add("sLNm");
            m_list.Add("sPrimaryNm");
            m_list.Add("sEmployeeLoanRep");
            m_list.Add("sEmployeeUnderwriter");
            #endregion

            m_fields = m_dtConversion.Fields; // Field names in DataTrac

            s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, m_list);

            foreach (string s in m_list)
            {
                form += " " + s;
            }

            Tools.LogInfo("Fields of DataTracData:" + form);
        }

        public DataTracData(Guid fileId)
            : base(fileId, "DataTracData", s_selectProvider)
        {
        }
    }

    // OPM 27411
    public class CDataTracLoanProgramData : CPageData
    {
        private static CSelectStatementProvider s_selectProvider;

        static CDataTracLoanProgramData()
        {
            StringList list = new StringList();
            list.Add("sDataTracLpId");
            s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

        }
        public CDataTracLoanProgramData(Guid fileId)
            : base(fileId, "CDataTracLoanProgramData", s_selectProvider)
        {
        }
    }
}