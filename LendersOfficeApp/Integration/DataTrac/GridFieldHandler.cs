﻿using System;
using System.Collections;
using System.Data;
using DataAccess;
using DataTrac;
using System.Web;
using System.Xml;

namespace LendersOfficeApp.Integration.DataTrac
{
    // OPM 27411 - map the Asset fields
    // There are many other grid fields that can be mapped, so this class can be extended to handle them
    public class GridFieldHandler
    {
        #region Variables
        E_DataTracExchangeType m_exchangeType;
        CPageData m_dataTracPageData;
        #endregion
        
        public GridFieldHandler(E_DataTracExchangeType exchangeType, CPageData dataTracPageData)
        {
            m_exchangeType = exchangeType; // For future use
            m_dataTracPageData = dataTracPageData;
        }

        public XmlDocument CreateGridXml()
        {
            XmlDocument doc = new XmlDocument();
            XmlElement gridData = doc.CreateElement("GRID_DATA");
            doc.AppendChild(gridData);

            AssetGridHandler.CreateRecordXml(gridData, m_dataTracPageData);
            ConditionsGridHandler.CreateRecordXml(gridData, m_dataTracPageData.sBrokerId, m_dataTracPageData.sLId);
            return doc;
        }

        public XmlDocument CreateGridRequestXml()
        {
            XmlDocument doc = new XmlDocument();
            XmlElement gridData = doc.CreateElement("GRID_REQUEST");
            doc.AppendChild(gridData);
            AssetGridHandler.CreateGridRequestXml(gridData);
            ConditionsGridHandler.CreateGridRequestXml(gridData);
            FeesGridHandler.CreateGridRequestXml(gridData); // OPM 30083
            return doc;
        }

        public void SetFieldsFromGridXml(string gridXml, Guid loanId)
        {
            if (string.IsNullOrEmpty(gridXml))
                return;

            XmlDocument doc = Tools.CreateXmlDoc(gridXml);
            XmlElement root = doc.DocumentElement;

            foreach (XmlElement gridTypeElement in root.ChildNodes) //GRID_DATA//GRID
            {
                string gridName = gridTypeElement.GetAttribute("Name");
                switch (gridName.ToUpper())
                {
                    case "ASSETS":
                        break; // Assets are already being pulled from the FNM file, so we don't need to call this until we stop using the FNM file
                    case "CONDITIONS":
                        ConditionsGridHandler.SaveRecordsFromGridXml(gridTypeElement, m_dataTracPageData);
                        break;
                    case "FEES":
                        FeesGridHandler.SaveRecordsFromGridXml(gridTypeElement, m_dataTracPageData); // OPM 30083
                        break;
                    default:
                        Tools.LogBug("Unhandled grid type '" + gridName.ToUpper() + "' in DataTrac GridFieldHandler.");
                        break;
                }
            }
        }

        internal static void CreateAndAppendAttribute(XmlElement element, string attributeName, object attributeValue)
        {
            if (element == null || attributeName == null || attributeValue == null)
                return;

            XmlAttribute attrib = element.OwnerDocument.CreateAttribute(attributeName);
            attrib.Value = attributeValue.ToString();
            element.Attributes.Append(attrib);
        }
    }
}