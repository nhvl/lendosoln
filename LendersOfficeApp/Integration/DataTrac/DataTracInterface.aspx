<%@ Import namespace="LendersOffice.Security"%>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss"%>
<%@ Page language="c#" Codebehind="DataTracInterface.aspx.cs" AutoEventWireup="false" ValidateRequest="false" Inherits="LendersOfficeApp.Integration.DataTrac.DataTracInterface" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>DataTracInterface</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
    <style>
		.userData { BEHAVIOR: url(#default#userdata) }
		</style>
	
	<script language="JavaScript">
		function _init()
		{
			<% if (!/*((BrokerUserPrincipal) Page.User)*/ BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanExportLoan)) { %>   

				document.getElementById("m_CanExportTable").style.display = "none";
				document.getElementById("errMsg").style.display="";

				return;

			<% } %>
			
			f_enableButtons();
			
			try
			{
				f_toggleLPField(true);
				if("<%=AspxTools.JsNumeric(m_resultMessage.Length)%>" > 0)
				{
					alert("<%=AspxTools.JsStringUnquoted(m_resultMessage)%>");
				}
				
				if ( <%= AspxTools.JsGetElementById(DTLoanNum) %>.value == "" )
				{
					<%= AspxTools.JsGetElementById(DTLoanNum) %>.value = "<%=AspxTools.JsStringUnquoted(LoanNumber)%>";
				}
			}
			catch( e )
			{
			}
			
			if(document.getElementById("m_loginNm").value == "")
				document.getElementById("m_loginNm").focus();
			else
				document.getElementById("m_password").focus();
		}	
		
		function f_closeDTLPDetails()
		{
			document.getElementById("DTLPDetails").style.display = "none";
			<%=AspxTools.JsGetElementById(m_originatorTypeDDL)%>.style.display = "";
		}
		
		function f_showDTLPDetails(event) 
		{
			<%=AspxTools.JsGetElementById(m_originatorTypeDDL)%>.style.display = "none";
			var msg = document.getElementById("DTLPDetails");
			msg.style.display = "";
			msg.style.top = (event.clientY - 10)+ "px";
			msg.style.left = (event.clientX + 10 ) + "px";
		}
		
		function f_disableButtons()
		{
		    document.getElementById("m_ExportButtonVisible").disabled = true;
		    document.getElementById("m_ImportButtonVisible").disabled = true;
		}
		
		function f_enableButtons()
		{
		    document.getElementById("m_ExportButtonVisible").disabled = false;
		    document.getElementById("m_ImportButtonVisible").disabled = false;
		}
		
		function f_toggleLPField(readOnlyVal)
		{
		    <%=AspxTools.JsGetElementById(m_dtLp)%>.readOnly = readOnlyVal;
		}
		
		function f_onExportClick()
		{
		    if(f_validatePage() == true)
		    {
		        f_disableButtons();
		        <%=AspxTools.JsGetElementById(m_ExportButton)%>.click();
		    }
		}
		
		function f_onImportClick()
		{
		    if(f_validatePage() == true)
		    {
		        f_disableButtons();
		        <%=AspxTools.JsGetElementById(m_ImportButton)%>.click();
		    }
		}
		
		function f_resetLPField()
		{
		    try
			{
				var args = new Object();
				args["lpId"] = <%=AspxTools.JsString(LoanProgramID)%>;
				args["loanId"] = <%=AspxTools.JsString(LoanID)%>;
				gService.main.callAsyncSimple("GetMappedLoanProgramNameAndRevert", args, function(result) {
                    if(result.value && result.value["DTLpId"])
				    {
				        <%=AspxTools.JsGetElementById(m_dtLp)%>.value = trim(result.value["DTLpId"]);
				    }
				    else
				    {
				        alert("Unable to reset DataTrac Loan Program Name - Please let us know if this happens again.");
				    }
				});				
			}
			catch( e ){}
			
			f_toggleLPField(true);
		}
		
		function f_validatePage() 
		{
			var isValid = true;
			if (typeof(Page_ClientValidate) == 'function' || typeof(Page_ClientValidate) == 'object')
			{
				isValid = Page_ClientValidate();
			}
			
			return isValid;
		}
	</script>
</HEAD>
  <body MS_POSITIONING="FlowLayout" class="EditBackground" scroll="no">
    <form id="DataTracInterface" method="post" runat="server">
		<div id="errMsg" style="DISPLAY:none"><font color="red"><br><b>&nbsp;&nbsp;You do not have permission to export loans. &nbsp;Contact your administrator for more information.</b></font></div>
		<table id="m_CanExportTable" cellSpacing="0" cellPadding="5" border="0" width="100%">
			<tr>
				<td nowrap colspan="4" width="100%" class="MainRightHeader">DataTrac</td>
			</tr>
			<tr>
				<td class="fieldlabel" nowrap>&nbsp;&nbsp;Update if the file already exists?</td>
				<td>
					<asp:RadioButtonList ID="m_updateExisting" runat="server" RepeatDirection="Horizontal">
						<asp:ListItem Value="Y">Yes</asp:ListItem>
						<asp:ListItem Value="N">No</asp:ListItem>
					</asp:RadioButtonList>
				</td>
			</tr>
			<tr>
				<td class="fieldlabel" nowrap>&nbsp;&nbsp;DataTrac Loan Number
				</td>
				<td width="100%"><asp:TextBox ID="DTLoanNum" size="20" maxlength=15 Runat=server Width="157"></asp:TextBox>
				(max 15 characters)
				<asp:requiredfieldvalidator id="m_loanNumVal" runat="server" Width="240px" Font-Bold="True" ErrorMessage="Please enter a DataTrac Loan Number" Display="Dynamic" ControlToValidate="DTLoanNum" ></asp:requiredfieldvalidator>
				</td>
			</tr>
			<tr>
				<td class="fieldlabel" nowrap>&nbsp;&nbsp;DataTrac Loan Program Name&nbsp;<a href='#"' onclick='f_showDTLPDetails(event);'?>?</a></td>
				<td width="100%"><asp:TextBox ID="m_dtLp" size="20" maxlength="12" Runat=server Width="157"></asp:TextBox>
				<a href='#"' onclick='f_toggleLPField(false);'?>change</a>
				&nbsp;<a href='#"' onclick='f_resetLPField();'?>reset</a>
				</td>
			</tr>
			<tr>
				<td class="fieldlabel" nowrap>&nbsp;&nbsp;Originator Type (export only)</td>
				<td width="100%" style="height:30"><asp:DropDownList ID="m_originatorTypeDDL" Runat=server Width=157></asp:DropDownList></td>
			</tr>
			<tr>
				<td class="fieldlabel" nowrap>&nbsp;&nbsp;DataTrac User Name</td>
				<td width="100%"><asp:TextBox ID="m_loginNm" size="20" Runat=server Width="157"></asp:TextBox>
				<asp:requiredfieldvalidator id="m_loginVal" runat="server" Width="240px" Font-Bold="True" ErrorMessage="Please enter your DataTrac Login Name" Display="Dynamic" ControlToValidate="m_loginNm" ></asp:requiredfieldvalidator>
				</td>
			</tr>
			<tr>
				<td class="fieldlabel" nowrap>&nbsp;&nbsp;DataTrac Password
				</td>
				<td width="100%"><asp:TextBox ID="m_password" TextMode="Password" size="20" Runat=server Width="157"></asp:TextBox>
				<asp:requiredfieldvalidator id="m_pwVal" runat="server" Width="240px" Font-Bold="True" ErrorMessage="Please enter your DataTrac Password" Display="Dynamic" ControlToValidate="m_password" ></asp:requiredfieldvalidator>
				</td>
			</tr>
			<tr>
				<td class="fieldlabel" colspan=2 style="PADDING-TOP:1px">
					<asp:CheckBox ID=m_rememberMyUserName Runat=server></asp:CheckBox>&nbsp;<span id=RememberLabel>Remember My User Name</span>
				</td>
			</tr>
			
			<tr>
				<td style="PADDING-LEFT:35px">
					<input type=button id=m_ExportButtonVisible name="m_ExportButtonVisible" value="Export to DataTrac" onclick="f_onExportClick(); return false;"/>
					<asp:Button ID=m_ExportButton style="display:none" Runat=server Text="Export to DataTrac" NoHighlight OnClick="ExportToDataTrac"></asp:Button>
				</td>
				<td width="100%">
					<input type=button id=m_ImportButtonVisible name="m_ImportButtonVisible" value="Import from DataTrac" onclick="f_onImportClick(); return false;"/>
					<asp:Button ID=m_ImportButton style="display:none" Runat=server NoHighlight Text="Import from DataTrac" OnClick="ImportFromDataTrac" Width="150"></asp:Button>
				</td>
			</tr>
		</table>
		<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
     </form>
     <div id="DTLPDetails" style="BORDER-RIGHT:black 3px solid; PADDING-RIGHT:5px; BORDER-TOP:black 3px solid; DISPLAY:none; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:black 3px solid; WIDTH:360px; PADDING-TOP:5px; BORDER-BOTTOM:black 3px solid; POSITION:absolute; HEIGHT:90px; BACKGROUND-COLOR:whitesmoke">
			<table width="100%"><tr><td>This reflects the program as it appears in DataTrac.<br /><br />If you've selected a program and the name does not appear automatically, please contact your administrator.</td></tr>
			<tr><td align=center>[ <a href="#" onclick="f_closeDTLPDetails();">Close</a> ]</td></tr>
			</table>
			</div>
  </body>
</HTML>
