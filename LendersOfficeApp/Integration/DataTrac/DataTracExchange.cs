using System;
using System.Collections;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Events;
using System.Reflection;
using LendersOffice.Constants;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOfficeApp.Integration.DataTrac
{
    public enum E_DataTracExchangeType
    {
        Import = 0,
        Export = 1
    }

    public abstract class DataTracExchange
	{
		#region Variables
		protected static string IGNORE_FIELD = "INTEGRATION_IGNORE";
		protected Guid m_loanId = Guid.Empty;
		protected Guid m_brokerId = Guid.Empty;
		protected string m_dtLoginNm = "";
		protected string m_dtPassword = "";
		protected string m_dtLoanNum = "";
		protected string m_pathToDataTrac = "";
		protected string m_dataTracWebserviceURL = "";
		protected string m_filePath = "";
        protected E_DataTracOriginatorType m_originatorType = E_DataTracOriginatorType.R;
		protected ArrayList m_warningMessagesFromDT = new ArrayList();
		protected ArrayList m_errorMessagesFromDT = new ArrayList();
        protected double m_webserviceVersion = 1.0;

		protected abstract string FilePath{ set; }

		protected static BrokerUserPrincipal BrokerUser 
		{
			get 
			{ 
				BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal; //Page.User as BrokerUserPrincipal;
				if (null == principal)
					RequestHelper.Signout(); // 10/12/2005 dd - If unable to cast to BrokerUserPrincipal redirect to log out.

				return principal; 
			}
		}

		public ArrayList WarningMessagesFromDataTrac
		{
			get { return m_warningMessagesFromDT; }
		}

		public ArrayList ErrorMessagesFromDataTrac
		{
			get { return m_errorMessagesFromDT; }
		}

        public double WebserviceVersion
        {
            get { return m_webserviceVersion; }
        }
		#endregion

        public DataTracExchange(Guid loanId, Guid brokerId, string dtLoginName, string dtPassword, string dtLoanNumber, E_DataTracOriginatorType originatorType)
		{
			m_loanId = loanId;
			m_brokerId = brokerId;
			m_dtLoginNm = dtLoginName.TrimWhitespaceAndBOM();
			m_dtPassword = dtPassword.TrimWhitespaceAndBOM();
			m_dtLoanNum = dtLoanNumber.TrimWhitespaceAndBOM();
            m_originatorType = originatorType;
			SetPathToDataTracAndWebserviceURL();
		}

		private void SetPathToDataTracAndWebserviceURL()
		{
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", m_brokerId)
                                        };

			using(DbDataReader	reader = StoredProcedureHelper.ExecuteReader(m_brokerId, "GetPathToDataTracByBrokerId", parameters ) )
			{
				if(reader.Read())
				{
					m_pathToDataTrac = reader["PathToDataTrac"].ToString();
					m_dataTracWebserviceURL = reader["DataTracWebServiceUrl"].ToString();
				}
			}
		}

		#region Static Functions/Fields
		// 10/02/08 db - This function determines which messages we do not want to log to PB even though they 
		// will prevent the exchange of data between DataTrac and LO.  DataTrac does not currently publish their
		// set of error messages, and each of them come back as strings, so we need to compare based on the
		// message text itself whether or not we want to log it.  Hopefully these messages will not change too much,
		// and we will try and pull out the smallest amount of text necessary to try and minimize the impact if
		// the messages do change.
		public static bool ShouldLogErrorMessage(string msgText)
		{
			string msg = msgText.ToLower().TrimWhitespaceAndBOM();

			if(msg.IndexOf("login information is invalid") >= 0)
				return false;

			if(msg.IndexOf("a loan currently exists in datatrac") >= 0)
				return false;

			if(msg.IndexOf("number already exists in datatrac") >= 0)
				return false;

			if(msg.IndexOf("loan number not found") >= 0)
				return false;

			if(msg.IndexOf("social security number in datatrac does not match") >= 0)
				return false;

            if (msg.IndexOf("the loan cannot be exported to datatrac because it is in the") >= 0) 
                return false;

			return true;
		}

		public static string GetSavedUserName()
		{
			string userName = "";
			try
			{
                SqlParameter[] parameters = {
                                                new SqlParameter("@UserId", BrokerUser.UserId)
                                            };

				using(DbDataReader	reader = StoredProcedureHelper.ExecuteReader(BrokerUser.BrokerId, "RetrieveDataTracLoginNmByUserId", parameters ) )
				{
					if(reader.Read())
					{
						userName = reader["DataTracLoginNm"].ToString();
					}
				}
			}
			catch{}
			
			return userName;
		}

		public static void SaveUserName(string userNameToSave)
		{
			try
			{
				SqlParameter[] parameters = {
												new SqlParameter("@UserId", BrokerUser.UserId),
												new SqlParameter("@DataTracLoginNm", userNameToSave),
				};
				StoredProcedureHelper.ExecuteNonQuery(BrokerUser.BrokerId, "SetDataTracLoginNmByUserId", 0, parameters);
			}
			catch{}
		}

        public static string GetDataTracLoanProgramName(Guid brokerID, Guid loanID, Guid productID)
        {
            if (Guid.Empty == productID)
                return "";

            try
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "RetrieveDataTracLpId", new SqlParameter("@lLpTemplateId", productID)))
                {
                    if (reader.Read())
                        return reader["lDataTracLpId"].ToString();
                }
            }
            catch { }

            return "";
        }

        // OPM 27411 - Allow manual setting of the DataTrac loan program name, even if there is no existing
        // mapping for it.
        public static void SaveDataTracLoanProgramName(Guid loanID, string dtLpId)
        {
            try
            {
                CPageData dataLoan = new CDataTracLoanProgramData(loanID);
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataLoan.sDataTracLpId = dtLpId;
                dataLoan.Save();
            }
            catch {}
        }

        // OPM 27411
        public static void SaveDataTracLoanProgramName(Guid brokerID, Guid loanID, Guid productID)
        {
            if (Guid.Empty == productID)
                return;

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", brokerID)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerID, "GetIsDataTracIntegrationEnabledByBrokerID", parameters))
            {
                if (!reader.Read())
                    return;
                else
                {
                    if (((bool)reader["IsDataTracIntegrationEnabled"]) == false)
                        return;
                }
            }

            try
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "RetrieveDataTracLpId", new SqlParameter("@lLpTemplateId", productID)))
                {
                    if (reader.Read())
                        SaveDataTracLoanProgramName(loanID, reader["lDataTracLpId"].ToString());
                }
            }
            catch { }
        }

		public static void SendNotifications(Guid brokerId, E_DataTracInteractionT interactionType, Guid LoanID, string LendersOfficeLoanNumber, string DataTracLoanNumber)
		{
            LoanAssignmentContactTable loanAssignmentTable = new LoanAssignmentContactTable(brokerId, LoanID);
			ArrayList       empList = new ArrayList();


            foreach (var assignedEmployee in loanAssignmentTable.Items)
			{
                if (assignedEmployee.RoleT != E_RoleT.Administrator)
				{
                    if (empList.Contains(assignedEmployee.EmployeeId) == false)
					{
                        empList.Add(assignedEmployee.EmployeeId);
					}
				}
			}
			foreach( Guid sendEmployeeId in empList )
			{
				try
				{
					DataTracNotification dtNotif = new DataTracNotification();
					dtNotif.Initialize(sendEmployeeId, LoanID, interactionType, DataTracLoanNumber, LendersOfficeLoanNumber, BrokerUser.DisplayName, E_ApplicationT.LendersOffice);
					dtNotif.Send();
				}
				catch( Exception e )
				{
					Tools.LogError( "Failed to send DataTrac notifications: " + e );
				}
			}
		}

        // To add additional entries into the DataTrac UI, just add another enum value here with the description attribute
        // set to what you want the dropdown display to say, and the value being the value that DataTrac expects
        // for the originator type.
        public enum E_DataTracOriginatorType
        {
            [Description("Broker")]
            B,
            [Description("Branch")]
            R,
            [Description("Lender (P)")]
            L,
            [Description("Lender (B)")]
            K
        }
        
        public static string GetDescription(Enum en)
        {
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());
            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(Description), false);
                if (attrs != null && attrs.Length > 0)
                    return ((Description)attrs[0]).Text;
            }
            return en.ToString();
        }

        #endregion
	}

    class Description : Attribute
    {
        public string Text;
        public Description(string text)
        {
            Text = text;
        }
    }
}