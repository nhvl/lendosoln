using System;
using DataAccess;

namespace LendersOfficeApp.Integration.DataTrac
{
	public class DataTracUIData : CPageData
	{
		private static CSelectStatementProvider s_selectProvider;
		
		static DataTracUIData()
		{
			StringList list = new StringList();
	        
	        #region Target Fields
			list.Add("sLNm");
			list.Add("sDataTracLpId");
            list.Add("sLpTemplateId");
            list.Add("sLId");
	        #endregion
	        
			s_selectProvider = CSelectStatementProvider.GetProviderFor( E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection );
		}        
	        
		public DataTracUIData(Guid fileId) : base(fileId, "DataTracUIData", s_selectProvider)
		{
		}
	}
}
