﻿using System;
using System.Collections;
using System.Data;
using DataAccess;
using DataTrac;
using System.Web;
using System.Xml;
using System.Data.SqlClient;
using LendersOffice.Common;

namespace LendersOfficeApp.Integration.DataTrac
{
    /// <summary>
    /// OPM 30163 - Export our rate and price adjustments to DataTrac.  
    /// DataTrac erases the existing set of adjustments and adds only the ones we send over.
    /// </summary>
    public class AdjustmentsHandler
    {
        public AdjustmentsHandler()
        {
        }

        public static XmlDocument CreateAdjustmentXml(Guid loanId)
        {
            if (loanId == null || loanId == Guid.Empty)
            {
                return new XmlDocument();
            }

            CPageData dataLoan = new CSavedCertificateData(loanId);
            dataLoan.InitLoad();

            if (dataLoan.sPmlCertXmlContent.Value != "")
            {
                XmlDocument doc = Tools.CreateXmlDoc(dataLoan.sPmlCertXmlContent.Value);

                XmlElement adjustmentRoot = (XmlElement)doc.SelectSingleNode("//pml_cert/pricing/adjustments");
                if (adjustmentRoot != null)
                {
                    return CreateDataTracAdjustmentXml(doc);
                }
            }

            return new XmlDocument();
        }

        private static XmlDocument CreateDataTracAdjustmentXml(XmlDocument pmlCert)
        {
            if (pmlCert == null)
            {
                return new XmlDocument();
            }

            XmlDocument doc = new XmlDocument();
            XmlElement root = doc.CreateElement("ADJUSTMENTS");
            GridFieldHandler.CreateAndAppendAttribute(root, "ShowHiddenAdjInBuyPrice", bool.FalseString);  //av   opm 70601 -- dont remove required by webservice
           
            doc.AppendChild(root);
            foreach (XmlNode adjustment in pmlCert.SelectNodes("//pml_cert/pricing/adjustments/adjustment"))
            {
                XmlElement rateElement = adjustment["rate"];
                XmlElement priceElement = adjustment["fee"];
                XmlElement descriptionElement = adjustment["description"];

                if (!IsEmpty(rateElement) || !IsEmpty(priceElement))
                {
                    CreateAndAppendNode(root, rateElement, priceElement, descriptionElement);
                }
            }

            foreach (XmlNode adjustment in pmlCert.SelectNodes("//pml_cert/pricing/hiddendadjustments/adjustment"))
            {
                XmlElement rateElement = adjustment["rate"];
                XmlElement priceElement = adjustment["fee"];
                XmlElement descriptionElement = adjustment["description"];

                if (!IsEmpty(rateElement) && !IsEmpty(priceElement))
                {
                    CreateAndAppendNode(root, rateElement, priceElement, descriptionElement, true /*isHidden*/);
                }
            }

            return doc;
        }

        private static bool IsEmpty(XmlElement adjustmentValue)
        {
            if (adjustmentValue == null)
            {
                return true;
            }

            if (adjustmentValue.InnerText.TrimWhitespaceAndBOM().Equals(""))
            {
                return true;
            }

            try
            {
                if (decimal.Parse(adjustmentValue.InnerText.TrimWhitespaceAndBOM().Trim('%')) == 0)
                {
                    return true;
                }
            }
            catch { }
            return false;
        }

        private static void CreateAndAppendNode(XmlElement root, XmlElement rateElement, XmlElement priceElement, XmlElement descriptionElement)
        {
            CreateAndAppendNode(root, rateElement, priceElement, descriptionElement, false /*isHidden*/);
        }

        private static void CreateAndAppendNode(XmlElement root, XmlElement rateElement, XmlElement priceElement, XmlElement descriptionElement, bool isHidden)
        {
            if (root == null)
            {
                return;
            }

            XmlElement adjustmentElement = root.OwnerDocument.CreateElement("adjustment");
            GridFieldHandler.CreateAndAppendAttribute(adjustmentElement, "IsHidden", isHidden);
            root.AppendChild(adjustmentElement);

            XmlElement outRateNode = adjustmentElement.OwnerDocument.CreateElement("rate");
            outRateNode.InnerText = rateElement.InnerText;
            adjustmentElement.AppendChild(outRateNode);

            XmlElement outPriceNode = adjustmentElement.OwnerDocument.CreateElement("price");
            outPriceNode.InnerText = priceElement.InnerText;
            adjustmentElement.AppendChild(outPriceNode);

            XmlElement outDescriptionNode = adjustmentElement.OwnerDocument.CreateElement("description");
            outDescriptionNode.InnerText = descriptionElement.InnerText;
            adjustmentElement.AppendChild(outDescriptionNode);
        }
    }
}