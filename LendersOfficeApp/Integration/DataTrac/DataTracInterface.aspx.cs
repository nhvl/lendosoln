using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOfficeApp.common;
using DataAccess;
using DataTrac;
using System.Reflection;
using LendersOffice.Common;
using LendersOffice.Conversions;
using System.IO;
using LendersOffice.Admin;
using LendersOffice.Security;
using LendersOffice.Events;
using LendersOffice.Audit;

namespace LendersOfficeApp.Integration.DataTrac
{
	public partial class DataTracInterface : LendersOfficeApp.newlos.BaseLoanPage
	{
		#region Variables
		
		protected string m_sLNm = "";
		protected string m_resultMessage = "";
        protected System.Web.UI.WebControls.DropDownList m_originatorTypeDDL;
		
		private ArrayList m_resultMessages = new ArrayList();

		protected string LoanNumber
		{
			set { ViewState["LoanNumber"] = value; }
			get 
			{ 
				if (ViewState["LoanNumber"] != null)
					return (string) ViewState["LoanNumber"]; 
				else
					return "";
			}
		}

        protected string LoanProgramID
        {
            set { ViewState["LoanProgramID"] = value; }
            get
            {
                if (ViewState["LoanProgramID"] != null)
                    return (string)ViewState["LoanProgramID"];
                else
                    return "";
            }
        }

        // We will save the DataTrac loan program ID that is initially loaded so that we can check if the user changes it
        // That way, we won't be needlessly hitting the database to save the loan program ID every time.
        protected string OriginalLoanProgramID
        {
            set { ViewState["OriginalLoanProgramID"] = value; }
            get
            {
                if (ViewState["OriginalLoanProgramID"] != null)
                    return (string)ViewState["OriginalLoanProgramID"];
                else
                    return "";
            }
        }
        
		#endregion
		
		protected void PageInit(object sender, System.EventArgs e) 
		{
			this.PageID = "DataTracInterface";
		}

        private void LoadOriginatorDDLFromEnum()
        {
            int numElementsInEnum = Enum.GetValues(typeof(DataTracExchange.E_DataTracOriginatorType)).Length;
            DataTracExchange.E_DataTracOriginatorType origType;
            for (int i = 0; i < numElementsInEnum; ++i)
            {
                origType = (DataTracExchange.E_DataTracOriginatorType)Enum.GetValues(typeof(DataTracExchange.E_DataTracOriginatorType)).GetValue(i);
                m_originatorTypeDDL.Items.Insert(i, new ListItem(DataTracExchange.GetDescription(origType), origType.ToString()));
            }

            if (numElementsInEnum > 0)
                m_originatorTypeDDL.SelectedIndex = 0;
        }

        protected void PageLoad(object sender, System.EventArgs e)
		{
			RegisterService("main", "/Integration/DataTrac/DataTracValidationService.aspx");

			string userName = Utilities.SafeHtmlString(DataTracExchange.GetSavedUserName());
			
			if(!Page.IsPostBack)
			{
				// Load originator type dropdown values from the enum in DataTracExchange.cs
                LoadOriginatorDDLFromEnum();

                if(userName.TrimWhitespaceAndBOM() != "")
				{
					m_rememberMyUserName.Checked = true;
					m_loginNm.Text = userName;
				}
				else
				{
					m_rememberMyUserName.Checked = false;
				}

				m_updateExisting.SelectedIndex = 0; // Sets the update existing DataTrac loan file value to true
			}
		}

		protected override void LoadData() 
		{
			CPageData dataLoan = new DataTracUIData(LoanID);
			dataLoan.InitLoad();
			string dtLoanNumber = (dataLoan.sLNm.Length > 15)?dataLoan.sLNm.Substring(0, 15):dataLoan.sLNm;
            LoanNumber = Utilities.SafeJsLiteralString(Utilities.SafeHtmlString(dtLoanNumber));
            m_dtLp.Text = Utilities.SafeHtmlString(dataLoan.sDataTracLpId); // This one is not getting displayed with JS, only html
            OriginalLoanProgramID = dataLoan.sDataTracLpId;
            LoanProgramID = Utilities.SafeJsLiteralString(Utilities.SafeHtmlString(dataLoan.sLpTemplateId.ToString()));
		}

		private void SaveUserName()
		{
			string userNameToSave = "";
			if(m_rememberMyUserName.Checked == true)
				userNameToSave = m_loginNm.Text;
			
			DataTracExchange.SaveUserName(userNameToSave);
		}

        private string CreateErrorMsgForLogging(string errorText, double webserviceVersion)
        {
            return string.Format("{0}{5}{5}LO Loan Number: {6}{5}LoginName: {1}{5}UserId: {2}{5}BrokerName: {3}{5}BrokerId: {4}{5}Webservice Version: {7}", errorText, BrokerUser.LoginNm, BrokerUser.UserId, BrokerUser.BrokerName, BrokerUser.BrokerId, Environment.NewLine, LoanNumber, webserviceVersion);
        }

		protected void ImportFromDataTrac(object sender, System.EventArgs e)
		{
			SaveUserName();
			
			bool returnMessagesFromDT = true; // set this to false if there's some fatal error here and we do not want the users to see any success messages from DT
			ArrayList dtErrorMessages = null;
			ArrayList dtWarningMessages = null;
            DataTracExchange.E_DataTracOriginatorType origType = (DataTracExchange.E_DataTracOriginatorType)Enum.Parse(typeof(DataTracExchange.E_DataTracOriginatorType), m_originatorTypeDDL.SelectedValue);
            DataTracImporter dtImporter = new DataTracImporter(LoanID, BrokerUser.BrokerId, Utilities.SafeJsLiteralString(m_loginNm.Text), Utilities.SafeJsLiteralString(m_password.Text), Utilities.SafeJsLiteralString(DTLoanNum.Text), origType);
			
			try
			{
				dtImporter.Import();
				dtErrorMessages = dtImporter.ErrorMessagesFromDataTrac;
				dtWarningMessages = dtImporter.WarningMessagesFromDataTrac;
				DataTracExchange.SendNotifications(BrokerUser.BrokerId, E_DataTracInteractionT.ImportFromDataTrac, LoanID, LoanNumber, DTLoanNum.Text.TrimWhitespaceAndBOM());
				m_resultMessages.Add("Successfully imported data from DataTrac Loan Number " + DTLoanNum.Text.ToUpper());
				
				LendersOffice.Audit.AbstractAuditItem audit = new LendersOffice.Audit.DataTracAuditItem(BrokerUser, E_DataTracInteractionT.ImportFromDataTrac, LoanNumber,  DTLoanNum.Text, m_dtLp.Text);
				LendersOffice.Audit.AuditManager.RecordAudit(LoanID, audit);
			}
			catch(DataTracException dte)
			{
				m_resultMessages.Add(dte.UserMessage);
				if(DataTracExchange.ShouldLogErrorMessage(dte.UserMessage) == true)
				{
                    Tools.LogError(CreateErrorMsgForLogging(dte.ToString(), dtImporter.WebserviceVersion));
				}
			}
			catch(CBaseException c)
			{
				returnMessagesFromDT = false;
                Tools.LogError(CreateErrorMsgForLogging("Error importing from DataTrac: " + c.ToString(), dtImporter.WebserviceVersion));
				m_resultMessages.Add(c.UserMessage);
			}
			catch(Exception exc)
			{
				returnMessagesFromDT = false;
                Tools.LogError(CreateErrorMsgForLogging("Error importing from DataTrac: " + exc.ToString(), dtImporter.WebserviceVersion));
				m_resultMessages.Add(ErrorMessages.DataTrac_GenericFieldErrorMessage);
			}
			finally
			{
				foreach(string resultMsg in m_resultMessages)
				{
                    m_resultMessage += Utilities.SafeJsLiteralString(resultMsg + "\\n");
				}

				if(dtErrorMessages != null && returnMessagesFromDT)
				{
					foreach(string msg in dtErrorMessages)
					{
                        m_resultMessage += Utilities.SafeJsLiteralString(msg + "\\n");
					}
				}

				if(dtWarningMessages != null && returnMessagesFromDT)
				{
					foreach(string msg in dtWarningMessages)
					{
                        m_resultMessage += Utilities.SafeJsLiteralString(msg + "\\n");
					}
				}
			}
		}

		protected void ExportToDataTrac(object sender, System.EventArgs e)
		{
			SaveUserName();
			
			bool returnMessagesFromDT = true; // set this to false if there's some fatal error here and we do not want the users to see any success messages from DT
			ArrayList dtErrorMessages = null;
			ArrayList dtWarningMessages = null;
			bool updateExisting = (m_updateExisting.SelectedIndex == 0)?true:false;
            DataTracExchange.E_DataTracOriginatorType origType = (DataTracExchange.E_DataTracOriginatorType)Enum.Parse(typeof(DataTracExchange.E_DataTracOriginatorType), m_originatorTypeDDL.SelectedValue);
            DataTracExporter dtExporter = new DataTracExporter(LoanID, BrokerUser.BrokerId, Utilities.SafeJsLiteralString(m_loginNm.Text), Utilities.SafeJsLiteralString(m_password.Text), Utilities.SafeJsLiteralString(DTLoanNum.Text), updateExisting, origType, Utilities.SafeJsLiteralString(m_dtLp.Text), (OriginalLoanProgramID != m_dtLp.Text));
            
			try
			{
				dtExporter.Export();
				dtErrorMessages = dtExporter.ErrorMessagesFromDataTrac;
				dtWarningMessages = dtExporter.WarningMessagesFromDataTrac;
				DataTracExchange.SendNotifications(BrokerUser.BrokerId, E_DataTracInteractionT.ExportToDataTrac, LoanID, LoanNumber, DTLoanNum.Text.TrimWhitespaceAndBOM());
				m_resultMessages.Add("Successfully exported data to DataTrac Loan Number " + DTLoanNum.Text.ToUpper());
				
				LendersOffice.Audit.AbstractAuditItem audit = new LendersOffice.Audit.DataTracAuditItem(BrokerUser, E_DataTracInteractionT.ExportToDataTrac, LoanNumber,  DTLoanNum.Text, m_dtLp.Text);
				LendersOffice.Audit.AuditManager.RecordAudit(LoanID, audit);
			}
			catch(DataTracException dte)
			{
				m_resultMessages.Add(dte.UserMessage);
				if(DataTracExchange.ShouldLogErrorMessage(dte.UserMessage) == true)
				{
                    Tools.LogError(CreateErrorMsgForLogging(dte.ToString(), dtExporter.WebserviceVersion));
				}
			}
			catch(CBaseException c)
			{
				returnMessagesFromDT = false;
                Tools.LogError(CreateErrorMsgForLogging("Exception in DataTrac standalone exporter: " + c.ToString(), dtExporter.WebserviceVersion));
				m_resultMessages.Add(c.UserMessage);
			}
			catch(Exception exc)
			{
				returnMessagesFromDT = false;
                Tools.LogError(CreateErrorMsgForLogging("Exception in DataTrac standalone exporter: " + exc.ToString(), dtExporter.WebserviceVersion));
				m_resultMessages.Add(ErrorMessages.DataTrac_GenericFieldErrorMessage);
			}
			finally
			{
				foreach(string resultMsg in m_resultMessages)
				{
                    m_resultMessage += Utilities.SafeJsLiteralString(resultMsg + "\\n");
				}

				if(dtErrorMessages != null && returnMessagesFromDT)
				{
					foreach(string msg in dtErrorMessages)
					{
                        m_resultMessage += Utilities.SafeJsLiteralString(msg + "\\n");
					}
				}

				if(dtWarningMessages != null && returnMessagesFromDT)
				{
					foreach(string msg in dtWarningMessages)
					{
                        m_resultMessage += Utilities.SafeJsLiteralString(msg + "\\n");
					}
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion
	}
}