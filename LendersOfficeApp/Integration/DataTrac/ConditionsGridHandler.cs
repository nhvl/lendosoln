﻿using System;
using DataAccess;
using DataTrac;
using System.Web;
using System.Xml;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Reminders;
using LendersOffice.ObjLib.Task;
using System.Collections.Generic;
using LendersOffice.Constants;
using System.Linq;

namespace LendersOfficeApp.Integration.DataTrac
{
    public class ConditionsGridHandler
    {
        public ConditionsGridHandler()
        {
        }

        public static void CreateRecordXml(XmlElement gridData, Guid brokerId, Guid loanId)
        {
            XmlElement conditionsRoot = gridData.OwnerDocument.CreateElement("GRID");
            GridFieldHandler.CreateAndAppendAttribute(conditionsRoot, "Name", "CONDITIONS");
            gridData.AppendChild(conditionsRoot);
            BrokerDB db = BrokerDB.RetrieveById(brokerId);
            if (db.IsUseNewTaskSystem)
            {
                MapConditionsDataUsingNewTaskSystem(brokerId, loanId, conditionsRoot);
            }
            else
            {
                MapConditionsData(brokerId, loanId, conditionsRoot);
            }
        }

        public static void CreateGridRequestXml(XmlElement gridRequestElement)
        {
            XmlElement conditionRequestElement = gridRequestElement.OwnerDocument.CreateElement("Grid");
            GridFieldHandler.CreateAndAppendAttribute(conditionRequestElement, "Name", "CONDITIONS");
            gridRequestElement.AppendChild(conditionRequestElement);

            XmlElement field1 = gridRequestElement.OwnerDocument.CreateElement("Field");
            GridFieldHandler.CreateAndAppendAttribute(field1, "Name", "PRIOR_TO");
            conditionRequestElement.AppendChild(field1);

            XmlElement field2 = gridRequestElement.OwnerDocument.CreateElement("Field");
            GridFieldHandler.CreateAndAppendAttribute(field2, "Name", "CONDITION");
            conditionRequestElement.AppendChild(field2);

            XmlElement field3 = gridRequestElement.OwnerDocument.CreateElement("Field");
            GridFieldHandler.CreateAndAppendAttribute(field3, "Name", "RECEIVED");
            conditionRequestElement.AppendChild(field3);

            XmlElement field4 = gridRequestElement.OwnerDocument.CreateElement("Field");
            GridFieldHandler.CreateAndAppendAttribute(field4, "Name", "SIGNOFF");
            conditionRequestElement.AppendChild(field4);

            XmlElement field5 = gridRequestElement.OwnerDocument.CreateElement("Field");
            GridFieldHandler.CreateAndAppendAttribute(field5, "Name", "SO_INITIALS");
            conditionRequestElement.AppendChild(field5);
        }

        public static void SaveRecordsFromGridXml(XmlElement gridRoot, CPageData data)
        {
            if (data.BrokerDB.IsUseNewTaskSystem) 
            {
                SaveRecordsFromGridXmlTask(gridRoot, data);
                return;
            }
            try
            {
                using (CStoredProcedureExec spExec = new CStoredProcedureExec(data.BrokerDB.BrokerID))
                {
                    LoanConditionSetObsolete ldSet = new LoanConditionSetObsolete();
                    ldSet.Retrieve(data.sLId, true, false, false);
                    spExec.BeginTransactionForWrite();
                    foreach (XmlElement applicationElement in gridRoot.ChildNodes)
                    {
                        foreach (XmlElement recordElement in applicationElement.ChildNodes)
                        {
                            ConditionDesc condInfo = new ConditionDesc();
                            string dtFieldName = "";
                            string value = "";
                            foreach (XmlElement fieldElement in recordElement.ChildNodes)
                            {
                                dtFieldName = fieldElement.GetAttribute("DataTracFieldName").ToUpper();
                                value = fieldElement.GetAttribute("Value");

                                switch (dtFieldName)
                                {
                                    case "CONDITION":
                                        condInfo.Description = value;
                                        break;
                                    case "PRIOR_TO":
                                        condInfo.Category = value;
                                        break;
                                    case "SIGNOFF":
                                        DateTime tryParse;
                                        if (DateTime.TryParse(value, out tryParse))
                                            condInfo.DateDone = value;
                                        break;
                                    case "SO_INITIALS":
                                        if (value != null && value != "")
                                            condInfo.CondCompletedByUserNm = value + " (DataTrac Import)";
                                        break;
                                    case "RECEIVED":
                                        //For now, we aren't mapping this to anything, because our created date is read only
                                        break;
                                    default:
                                        Tools.LogError("Unhandled UW Conditions field name '" + dtFieldName + "' in DataTrac importer.");
                                        break;
                                }
                            }

                            string priorTo = GetLoCategory(condInfo.Category).ToUpper();
                            CLoanConditionObsolete existingLC = ldSet.GetMatch(priorTo, condInfo.Description);
                            if (existingLC == null) // add a new condition since no existing ones match
                            {
                                ldSet.AddUniqueCondition(priorTo.TrimWhitespaceAndBOM(), condInfo.Description.TrimWhitespaceAndBOM());
                                if (condInfo.DateDone != "")
                                {
                                    CLoanConditionObsolete matchedLoanCondition = ldSet.GetMatch(priorTo, condInfo.Description);
                                    if (matchedLoanCondition != null)
                                    {
                                        matchedLoanCondition.CondStatus = E_CondStatus.Done;
                                        matchedLoanCondition.CondStatusDate = DateTime.Parse(condInfo.DateDone);
                                        matchedLoanCondition.CondCompletedByUserNm = condInfo.CondCompletedByUserNm;
                                    }
                                }
                            }
                            else
                            {
                                // The only pieces of data we could be updating are the Done date and the 
                                // initials of the person who marked the condition as Done in DataTrac,
                                // since we have matched the condition text and category, 
                                // which means they haven't changed.
                                if (condInfo.DateDone == "")
                                {
                                    existingLC.CondStatus = E_CondStatus.Active;
                                }
                                else
                                {
                                    existingLC.CondStatus = E_CondStatus.Done;
                                    existingLC.CondStatusDate = DateTime.Parse(condInfo.DateDone);
                                    existingLC.CondCompletedByUserNm = condInfo.CondCompletedByUserNm;
                                }
                            }
                        }

                        break;// Conditions aren't separated into different applications, so we only need to grab them once for the entire loan
                    }
                    ldSet.Save(spExec);
                    spExec.CommitTransaction();
                }
            }
            catch (Exception e)
            {
                Tools.LogError("Unable to save underwriting conditions to loanId '" + data.sLId + "' when performing DataTrac import.  Error details: " + e.ToString());
                throw new DataTracException("Unable to save Underwriting Conditions.  Please contact us if this happens again.", e);
            }
        }

        private static void SaveRecordsFromGridXmlTask(XmlElement gridRoot, CPageData data)
        {
            data.InitSave(ConstAppDavid.SkipVersionCheck);
            IConditionImporter importer = data.GetConditionImporter("DataTrac");
            foreach (XmlElement applicationElement in gridRoot.ChildNodes)
            {
                ConditionDesc condInfo = new ConditionDesc();
                string dtFieldName = "";
                string value = "";
                foreach (XmlElement recordElement in applicationElement.ChildNodes)
                {
                    switch (dtFieldName)
                    {
                        case "CONDITION":
                            condInfo.Description = value;
                            break;
                        case "PRIOR_TO":
                            condInfo.Category = GetLoCategory(value).ToUpper();
                            break;
                        case "SIGNOFF":
                            DateTime tryParse;
                            if (DateTime.TryParse(value, out tryParse))
                            {
                                condInfo.DateDone = value;
                                condInfo.IsDone = true;
                            }
                                break;
                        case "SO_INITIALS":
                            if (value != null && value != "")
                                condInfo.CondCompletedByUserNm = value + " (DataTrac Import)";
                            break;
                        case "RECEIVED":
                            //For now, we aren't mapping this to anything, because our created date is read only
                            break;
                        default:
                            Tools.LogError("Unhandled UW Conditions field name '" + dtFieldName + "' in DataTrac importer.");
                            break;
                    }
                }

                importer.MergeIn(condInfo);
            }
            data.Save();
        }

        // DataTrac has a pre-defined set of categories, but we allow free text
        // We will assume the following LO -> DT convention and let the clients know:
        // Prior To Docs or Prior To Doc -> DOC
        // Prior To Funding -> FUNDING
        // Prior To Investor -> INVESTOR
        // Prior to Wire -> WIRE
        // Anything else will not get sent over and therefore will not show up in DataTrac
        private static string GetDataTracPriorToType(string LoCategory)
        {
            string dataTracPriorToType = "";
            switch (LoCategory.TrimWhitespaceAndBOM().ToLower())
            {
                case "prior to doc":
                case "prior to docs":
                    dataTracPriorToType = "D";
                    break;
                case "prior to funding":
                    dataTracPriorToType = "F";
                    break;
                case "prior to investor":
                    dataTracPriorToType = "I";
                    break;
                case "prior to wire":
                    dataTracPriorToType = "W";
                    break;
                default:
                    break;
            }

            return dataTracPriorToType;
        }

        private static string GetLoCategory(string dataTracPriorTo)
        {
            string loCategory = "";
            switch (dataTracPriorTo.TrimWhitespaceAndBOM().ToLower())
            {
                case "d":
                    loCategory = "prior to doc";
                    break;
                case "f":
                    loCategory = "prior to funding";
                    break;
                case "i":
                    loCategory = "prior to investor";
                    break;
                case "w":
                    loCategory = "prior to wire";
                    break;
                default:
                    Tools.LogError("Unhandled DataTrac PriorTo type: " + dataTracPriorTo + " while performing conditions import.  This means that DataTrac has changed their PriorTo types (perhaps added a new one), and we need to update our mapping.");
                    break;
            }

            return loCategory;
        }

        private static string GetDoneByInitials(CLoanConditionObsolete cond)
        {
            if (string.IsNullOrEmpty(cond.CondCompletedByUserNm))
                return "..";
            else if (cond.CondCompletedByUserNm.ToLower().IndexOf("datatrac") >= 0)
            {
                return cond.CondCompletedByUserNm.Substring(0, 3);
            }
            else if (cond.CondCompletedByUserNm.ToLower().IndexOf("import") >= 0)
            {
                return "..";
            }
            else
            {
                string[] name = cond.CondCompletedByUserNm.Split(' ');
                if (name.Length < 2)
                    return "..";
                else
                    return name[0].Substring(0, 1) + " " + name[1].Substring(0, 1);
            }
        }

        private static string GetDoneByInitials(string completedBy)
        {
            if (string.IsNullOrEmpty(completedBy))
                return "..";
            else if (completedBy.ToLower().IndexOf("datatrac") >= 0)
            {
                return completedBy.Substring(0, 3);
            }
            else if (completedBy.ToLower().IndexOf("import") >= 0)
            {
                return "..";
            }
            else
            {
                string[] name = completedBy.Split(' ');
                if (name.Length < 2)
                    return "..";
                else
                    return name[0].Substring(0, 1) + " " + name[1].Substring(0, 1);
            }
        }


        private static void MapConditionsDataUsingNewTaskSystem(Guid brokerId, Guid loanId, XmlElement element)
        {
            XmlElement appRoot = element.OwnerDocument.CreateElement("Application");
            GridFieldHandler.CreateAndAppendAttribute(appRoot, "Number", 0);
            element.AppendChild(appRoot);

            List<Task> tasks = Task.GetActiveConditionsByLoanId(brokerId, loanId, false);
            Dictionary<int, ConditionCategory> categories = ConditionCategory.GetCategories(brokerId).ToDictionary(c => c.Id);

            foreach (Task condtion in tasks)
            {
                string dataTracPriorToType = GetDataTracPriorToType(categories[condtion.CondCategoryId.Value].Category);
                if (string.IsNullOrEmpty(dataTracPriorToType))
                {
                    continue;
                }

                XmlElement conditionElement = element.OwnerDocument.CreateElement("Record");
                appRoot.AppendChild(conditionElement);

                XmlElement field = element.OwnerDocument.CreateElement("Field");
                GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "PRIOR_TO");
                GridFieldHandler.CreateAndAppendAttribute(field, "Value", dataTracPriorToType);
                conditionElement.AppendChild(field);

                field = element.OwnerDocument.CreateElement("Field");
                GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "CONDITION");
                GridFieldHandler.CreateAndAppendAttribute(field, "Value", condtion.TaskSubject);


                if (condtion.TaskStatus == E_TaskStatus.Closed )
                {
                    field = element.OwnerDocument.CreateElement("Field");
                    GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "SIGNOFF");
                    GridFieldHandler.CreateAndAppendAttribute(field, "Value", condtion.TaskClosedDate.Value);
                    conditionElement.AppendChild(field);

                    // DataTrac requires that the initials be sent over, otherwise it will not save the Signoff date
                    // If we do not know who performed the signoff, we will send ..
                    field = element.OwnerDocument.CreateElement("Field");
                    GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "SO_INITIALS");
                    string initials = GetDoneByInitials(condtion.ClosingUserFullName);
                    GridFieldHandler.CreateAndAppendAttribute(field, "Value", initials);
                    conditionElement.AppendChild(field);
                }

                if (condtion.TaskCreatedDate > SmallDateTime.MinValue)
                {
                    field = element.OwnerDocument.CreateElement("Field");
                    GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "RECEIVED");
                    GridFieldHandler.CreateAndAppendAttribute(field, "Value", condtion.TaskCreatedDate);
                    conditionElement.AppendChild(field);

                    // DataTrac requires that the initials be sent over, otherwise it will not save the Received date
                    // Since we do not know who performed the creation, we will send ..
                    field = element.OwnerDocument.CreateElement("Field");
                    GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "REC_INITIALS");
                    GridFieldHandler.CreateAndAppendAttribute(field, "Value", "..");
                    conditionElement.AppendChild(field);
                }
                
            }
        }

        private static void MapConditionsData(Guid brokerId, Guid loanId, XmlElement element)
        {
            XmlElement appRoot = element.OwnerDocument.CreateElement("Application");
            GridFieldHandler.CreateAndAppendAttribute(appRoot, "Number", 0);
            element.AppendChild(appRoot);

            // CConditionSet does not have the Created Date field, so we will use LoanConditionSet
            LoanConditionSetObsolete ldSet = new LoanConditionSetObsolete();
            ldSet.Retrieve(loanId, true, false, false);

            foreach (CLoanConditionObsolete cond in ldSet)
            {
                string dataTracPriorToType = GetDataTracPriorToType(cond.CondCategoryDesc);
                if (string.IsNullOrEmpty(dataTracPriorToType))
                    continue;

                XmlElement conditionElement = element.OwnerDocument.CreateElement("Record");
                appRoot.AppendChild(conditionElement);
                
                XmlElement field = element.OwnerDocument.CreateElement("Field");
                GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "PRIOR_TO");
                GridFieldHandler.CreateAndAppendAttribute(field, "Value", dataTracPriorToType);
                conditionElement.AppendChild(field);

                field = element.OwnerDocument.CreateElement("Field");
                GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "CONDITION");
                GridFieldHandler.CreateAndAppendAttribute(field, "Value", cond.CondDesc);
                conditionElement.AppendChild(field);

                if (cond.CondStatus == E_CondStatus.Done && cond.CondStatusDate > SmallDateTime.MinValue)
                {
                    field = element.OwnerDocument.CreateElement("Field");
                    GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "SIGNOFF");
                    GridFieldHandler.CreateAndAppendAttribute(field, "Value", cond.CondStatusDate);
                    conditionElement.AppendChild(field);

                    // DataTrac requires that the initials be sent over, otherwise it will not save the Signoff date
                    // If we do not know who performed the signoff, we will send ..
                    field = element.OwnerDocument.CreateElement("Field");
                    GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "SO_INITIALS");
                    string initials = GetDoneByInitials(cond);
                    GridFieldHandler.CreateAndAppendAttribute(field, "Value", initials);
                    conditionElement.AppendChild(field);
                }

                if (cond.CreatedD > SmallDateTime.MinValue)
                {
                    field = element.OwnerDocument.CreateElement("Field");
                    GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "RECEIVED");
                    GridFieldHandler.CreateAndAppendAttribute(field, "Value", cond.CreatedD);
                    conditionElement.AppendChild(field);

                    // DataTrac requires that the initials be sent over, otherwise it will not save the Received date
                    // Since we do not know who performed the creation, we will send ..
                    field = element.OwnerDocument.CreateElement("Field");
                    GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "REC_INITIALS");
                    GridFieldHandler.CreateAndAppendAttribute(field, "Value", "..");
                    conditionElement.AppendChild(field);
                }
            }
        }
    }
}