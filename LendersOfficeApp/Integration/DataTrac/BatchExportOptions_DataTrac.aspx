<%@ Import namespace="LendersOffice.Security"%>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss"%>
<%@ Page language="c#" Codebehind="BatchExportOptions_DataTrac.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.Integration.DataTrac.BatchExportOptions_DataTrac" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>DataTrac Export</title>
		<link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet"> 
		<style>
		  .userData { BEHAVIOR: url(#default#userdata) }
		  </style>
		<script language="javascript" src="../../LegacyLink/ServerProc.js"></script>
		<script language=javascript>
		<!--
			var gCurrentIndex = 0;
			var gStopExport = false;
			
			function _init() 
			{
				if(gFileNames.length == 0)
				    return;
				    
				f_hideErrorGrid();

				if (gFileNames.length == 1) 
				{
				    document.getElementById('m_multipleLoanNumNote').style.display = "none";
				    
				    if (gFileNames[0].length > 15)
				        <%=AspxTools.JsGetElementById(DTLoanNum)%>.value = gFileNames[0].substring(0, 15);
				    else
				        <%=AspxTools.JsGetElementById(DTLoanNum)%>.value = gFileNames[0];

				    document.getElementById('DtLpTR').style.display = "";
				}
				else 
				{
				    document.getElementById('m_multipleLoanNumNote').style.display = "";
				    document.getElementById('DtLpTR').style.display = "none";
				    <%=AspxTools.JsGetElementById(DTLoanNum)%>.disabled = true;
				    <%=AspxTools.JsGetElementById(DTLoanNum)%>.style.backgroundColor = "gainsboro";
				}
				
				document.getElementById('StatusMessage').innerHTML = "";
				f_toggleLPField(true);
				resize(580, 280);
				
				if(<%=AspxTools.JsGetElementById(m_showDataGrid)%>.value == "1")
				{
				    f_showErrorGrid();
				}
				else
				{
				   f_hideErrorGrid(); 
				}
				
				if("<%=AspxTools.JsNumeric(m_resultMessage.Length)%>" > 0)
				{
					var isError = ("<%=AspxTools.JsStringUnquoted(m_resultMessage)%>".indexOf("Success") < 0);
					WriteResultMessage("<%=AspxTools.JsStringUnquoted(m_resultMessage)%>", isError);
				}
					
				if(<%=AspxTools.JsGetElementById(m_loginNm)%>.value == "")
					<%=AspxTools.JsGetElementById(m_loginNm)%>.focus();
				else
					<%=AspxTools.JsGetElementById(m_password)%>.focus();
			}
			
			function onDeleteClick()
			{
				if(confirm("Remove selected loans from list?") == true)
					<%=AspxTools.JsGetElementById(m_delButton)%>.click();
			}			
			
			function WriteExportingStatus()
			{
				document.getElementById('StatusMessage').innerHTML = "<br><font color='green'>Performing export of all selected loans (this may take a few minutes)...</font>";
			}
			
			function WriteResultMessage(msg, isError)
			{
				if(isError && isError == true)
				{
				    document.getElementById('StatusMessage').innerHTML = "<br><font color='red'>" + msg + "</font>";
				}
			    else
                {
				    document.getElementById('StatusMessage').innerHTML = "<br><font color='green'>" + msg + "</font>";
				}
			}
			
			function f_showErrorGrid()
			{
			    resize(580, 520);
			    <%=AspxTools.JsGetElementById(m_failedResultsPanel)%>.style.display = "";
			    document.getElementById('m_delButtonSeen').style.display = "";
			}
			
			function f_hideErrorGrid()
			{
			    resize(580, 280);
			    <%=AspxTools.JsGetElementById(m_failedResultsPanel)%>.style.display = "none";
			    document.getElementById('m_delButtonSeen').style.display = "none";
			}
			
			function onStartExport() 
			{
				if(!Page_ClientValidate())
				{
					return;
				}
				
				if(<%=AspxTools.JsGetElementById(m_showDataGrid)%>.value == "1")
				{
				    if( anyAreChecked() == false )
				    {
				       WriteResultMessage("No loans selected for export.", true)
				       return; 
				    }
				}
				
				gStopExport = false;
				gCurrentIndex = 0;
				
				document.getElementById('btnStartExport').disabled = true;
				document.getElementById('btnClose').disabled = true;
				document.getElementById('StatusMessage').innerHTML = "";
				
				var args = new Object();
				args["login"] = <%=AspxTools.JsGetElementById(m_loginNm)%>.value;
				args["pw"] = <%=AspxTools.JsGetElementById(m_password)%>.value;
				gService.main.callAsyncSimple("ValidateCredentials", args, function(result) {
				    if(result.value && result.value["ErrorMessage"])
				    {
				        document.getElementById('btnStartExport').disabled = false;
				        document.getElementById('btnClose').disabled = false;
				        alert(result.value["ErrorMessage"]);
				        return false;
				    }
				
				    Export();
				});								
			}
			
			function Export() 
			{
				document.getElementById('btnStartExport').disabled = true;		  
				document.getElementById('btnClose').disabled = false;
				<%=AspxTools.JsGetElementById(m_ExportButton)%>.click();
				WriteExportingStatus();
            }
            
		    function f_closeDTLPDetails()
		    {
			    document.getElementById('DTLPDetails').style.display = "none";
			    <%=AspxTools.JsGetElementById(m_originatorTypeDDL)%>.style.display = "";
		    }
    		
		    function f_showDTLPDetails(event) 
		    {
			    <%=AspxTools.JsGetElementById(m_originatorTypeDDL)%>.style.display = "none";
			    var msg = document.getElementById('DTLPDetails');
			    msg.style.display = "";
			    msg.style.top = (event.clientY - 10)+ "px";
			    msg.style.left = (event.clientX + 10 ) + "px";
		    }
    		
		    function f_toggleLPField(readOnlyVal)
		    {
		        <%=AspxTools.JsGetElementById(m_dtLp)%>.readOnly = readOnlyVal;
		    }
    		
		    function f_resetLPField()
		    {
		        try
			    {
				    var args = new Object();
				    args["lpId"] = <%=AspxTools.JsString(LoanProgramID)%>
				    args["loanId"] = gLoanIDs[0];
		            gService.main.callAsyncSimple("GetMappedLoanProgramNameAndRevert", args, function(result) {
                        if(result.value && result.value["DTLpId"])
				        {
				            <%=AspxTools.JsGetElementById(m_dtLp)%>.value = trim(result.value["DTLpId"]);
				        }
				        else
				        {
				            alert("Unable to reset DataTrac Loan Program Name - Please let us know if this happens again.");
				        }
		            });				    
			    }
			    catch( e ){}
    			
			    f_toggleLPField(true);
		    }
		    
		    function onRowClick(cb)
			{
				highlightRowByCheckbox(cb);
			}
			
			function f_selectAll(cb)
			{
				var oItem = cb.children;
				var box = (cb.type=="checkbox")?cb:cb.children.item[0];
				xState=box.checked;
				
				elm = box.form.elements;
				for(i=0; i<elm.length; ++i)
				{
					if(elm[i].type == "checkbox" && elm[i] != box.id && elm[i].id != "m_rememberMyUserName")
					{
						if(elm[i].checked != xState)
							elm[i].click();
					}
				}
			}
			
			function anyAreChecked()
			{
				var inputs = document.getElementsByTagName("INPUT");
				var controlStatus = false;
				for (var i = 0; i < inputs.length; i++)
				{
					if ( inputs[i].id && inputs[i].id.indexOf('m_dg') == 0 
						&& inputs[i].checked != null && inputs[i].checked == true )
					{
						return true;
					}
				}
				
				return false;
			}
	   //-->
    </script>
</head>
  <body MS_POSITIONING="FlowLayout" class="EditBackground">
  	<h4 class="page-header">DataTrac Export</h4>
    <form id="BatchExportOptions_DataTrac" method="post" runat="server" class="little-indent">
    <input id="m_showDataGrid" Runat="server" type="hidden">
    <asp:Panel ID="m_exportWindow" runat=server>
		<div id="m_multipleLoanNumNote">
		    <br><b>Note:</b> The selected loans will be exported to DataTrac using their current LendingQB loan number.
		<br>
		</div>
		<br>
		<table>
			<tr>
				<td style="PADDING-LEFT: 50px" class="fieldlabel" nowrap>&nbsp;&nbsp;Update if the files already exist?</td>
				<td>
					<asp:RadioButtonList ID="m_updateExisting" runat="server" RepeatDirection="Horizontal">
						<asp:ListItem Value="Y">Yes</asp:ListItem>
						<asp:ListItem Value="N">No</asp:ListItem>
					</asp:RadioButtonList>
				</td>
			</tr>
			<tr>
				<td style="PADDING-LEFT: 50px" class="fieldlabel" nowrap>&nbsp;&nbsp;DataTrac Loan Number</td>
				<td><asp:TextBox ID="DTLoanNum" size="20" Runat="server" MaxLength="15" Width="157"></asp:TextBox>
				(max 15 characters)
				</td>
			</tr>
			<tr id="DtLpTR">
				<td style="PADDING-LEFT: 50px" class="fieldlabel" nowrap>&nbsp;&nbsp;DataTrac Loan Program Name&nbsp;<a href='#"' onclick='f_showDTLPDetails(event);'?>?</a></td>
				<td width="100%"><asp:TextBox ID="m_dtLp" maxlength="12" size="20" Runat="server" Width="157"></asp:TextBox>
				<a href='#"' onclick='f_toggleLPField(false);'?>change</a>
				&nbsp;<a href='#"' onclick='f_resetLPField();'?>reset</a>
				</td>
			</tr>
			<tr>
				<td style="PADDING-LEFT: 50px" class="fieldlabel" nowrap>&nbsp;&nbsp;Originator Type</td>
				<td style="height:25"><asp:DropDownList ID="m_originatorTypeDDL" runat="server" width="157"></asp:DropDownList></td>
			</tr>
			<tr>
				<td style="PADDING-LEFT: 50px" class="fieldlabel" nowrap>&nbsp;&nbsp;DataTrac User Name</td>
				<td>
					<asp:TextBox ID="m_loginNm" size="20" runat="server" Width="157"></asp:TextBox>
					<asp:requiredfieldvalidator id="m_loginVal" runat="server" Width="20px" Font-Bold="True" Display="Dynamic" ControlToValidate="m_loginNm">
                        <IMG runat="server" src="../../images/error_icon.gif">
					</asp:requiredfieldvalidator>
				</td>
			</tr>
			<tr>
				<td style="PADDING-LEFT: 50px" class="fieldlabel" nowrap>&nbsp;&nbsp;DataTrac Password</td>
				<td>
					<asp:TextBox ID="m_password" TextMode="Password" size="20" Runat=server Width="157"></asp:TextBox>
					<asp:requiredfieldvalidator id="m_pwVal" runat="server" Width="20px" Font-Bold="True" Display="Dynamic" ControlToValidate="m_password">
                        <IMG runat="server" src="../../images/error_icon.gif">
					</asp:requiredfieldvalidator>
				</td>
			</tr>
			<tr>
				<td style="PADDING-LEFT: 50px" class="fieldlabel" colspan="2" style="padding-top:1">
					<asp:CheckBox ID="m_rememberMyUserName" runat="server"></asp:CheckBox>&nbsp;<span id="RememberLabel">Remember My User Name</span>
				</td>
			</tr>
		</table>
		</asp:Panel>
		<div id="StatusMessage"></div>
		<asp:Panel ID="m_failedResultsPanel" runat="server">
            <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td class="FormTable"><br>
                        <table cellspacing="0" cellpadding="0" border="0">
					        <tr>
						        <td>
							        <div style="HEIGHT:200px; BORDER-RIGHT: medium none; PADDING-RIGHT: 2px; BORDER-TOP: 2px groove; OVERFLOW-Y: scroll; PADDING-LEFT: 2px; BACKGROUND: gainsboro; PADDING-BOTTOM: 2px; MARGIN: 8px; BORDER-LEFT: 2px groove; WIDTH: 520px; BORDER-BOTTOM: 2px groove; ">
							            <ml:CommonDataGrid id="m_dg" CellPadding="2" CssClass="DataGrid" runat="server">
								            <columns>
									            <asp:TemplateColumn HeaderText="<input type=checkbox runat=server onclick=f_selectAll(this);>" ItemStyle-Width="1px">
										            <ItemTemplate>
											            <asp:checkbox id="m_cb" value='<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sLId").ToString())%>' runat="server" checked = "false" onclick="onRowClick(this);"></asp:checkbox>
										            </ItemTemplate>
									            </asp:TemplateColumn>
									            <asp:BoundColumn DataField="sLNm" HeaderText="<b>Loan Number</b>" runat="server" ItemStyle-Width="1px"></asp:BoundColumn>
									            <asp:BoundColumn DataField="ErrorInfo" HeaderText="<b>Error Message</b>" runat="server"></asp:BoundColumn>
								            </columns>
							            </ml:CommonDataGrid>
							        </div>
						        </td>
					        </tr>
						</table>
                    </td>
                </tr>
        </table>
    </asp:Panel>
    <table>
        <tr>
			<td colspan="2" style="PADDING-LEFT: 180px" >
				<br/>
				<input type="button" name="export" id="btnStartExport" NoHighlight onclick="onStartExport(); " value="Start Export"/>
				<input type="button" id="m_delButtonSeen" style="display:none" value="Remove" onclick="onDeleteClick();"/>
				<input type="button" onclick="onClosePopup();" id="btnClose" NoHighlight value="Close"/>
				<asp:Button ID="m_ExportButton" style="DISPLAY:none" runat="server" Text="Export to DataTrac" OnClick="ExportToDataTrac"></asp:Button>
				<asp:Button id="m_delButton" style="display:none" Runat="server" causesvalidation="false" onclick="RemoveFromList" />
			</td>
		</tr>
    </table>
	<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
    </form>
     <div id="DTLPDetails" style="BORDER-RIGHT:black 3px solid; PADDING-RIGHT:5px; BORDER-TOP:black 3px solid; DISPLAY:none; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:black 3px solid; WIDTH:300px; PADDING-TOP:5px; BORDER-BOTTOM:black 3px solid; POSITION:absolute; HEIGHT:50px; BACKGROUND-COLOR:whitesmoke">
			<table width="100%"><tr><td>This reflects the program as it appears in DataTrac.<br /><br />If you've selected a program and the name does not appear automatically, please contact your administrator.</td></tr>
			<tr><td align="center">[ <a href="#" onclick="f_closeDTLPDetails();">Close</a> ]</td></tr>
			</table>
			</div>
  </body>
</html>
