﻿using System;
using System.Collections;
using System.Data;
using DataAccess;
using DataTrac;
using System.Web;
using System.Xml;

namespace LendersOfficeApp.Integration.DataTrac
{
    public class XmlDataHandler
    {
        CPageData m_dataTracPageData;
        
        public XmlDataHandler(CPageData dataTracPageData)
        {
            m_dataTracPageData = dataTracPageData;
        }

        public XmlDocument CreateExportXmlData()
        {
            XmlDocument doc = new XmlDocument();
            XmlElement exportRoot = doc.CreateElement("DATATRAC_EXPORT_DATA");
            doc.AppendChild(exportRoot);
            ExportXmlDataHandler.CreateExportXmlData(exportRoot);
            return doc;
        }

        public XmlDocument CreateImportXmlData()
        {
            XmlDocument doc = new XmlDocument();
            XmlElement importRoot = doc.CreateElement("DATATRAC_IMPORT_DATA");
            doc.AppendChild(importRoot);
            ImportXmlDataHandler.CreateImportXmlData(importRoot);
            return doc;
        }

        internal static void CreateAndAppendAttribute(XmlElement element, string attributeName, object attributeValue)
        {
            if (element == null || string.IsNullOrEmpty(attributeName) || attributeValue == null)
                return;

            XmlAttribute attrib = element.OwnerDocument.CreateAttribute(attributeName);
            attrib.Value = attributeValue.ToString();
            element.Attributes.Append(attrib);
        }

        internal static XmlElement CreateAndAppendElement(XmlElement root, string elementName, string elementData)
        {
            if (root == null || string.IsNullOrEmpty(elementName) || elementData == null)
                return null;

            XmlElement newElement = root.OwnerDocument.CreateElement(elementName);
            newElement.InnerText = elementData;
            root.AppendChild(newElement);

            return newElement;
        }
    }
}