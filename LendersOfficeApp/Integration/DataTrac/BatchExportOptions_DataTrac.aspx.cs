using System;
using System.Collections;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using System.IO;
using DataTrac;
using System.Text;
using LendersOffice.Security;
using LendersOffice.Events;
using LendersOffice.Admin;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using LendersOffice.Constants;
using LendersOffice.ConfigSystem;
using LendersOffice.ConfigSystem.Operations;

namespace LendersOfficeApp.Integration.DataTrac
{
	public partial class BatchExportOptions_DataTrac : LendersOffice.Common.BaseServicePage
	{
		#region Variables
        protected System.Web.UI.WebControls.Button m_ImportButton;
		protected System.Web.UI.WebControls.RequiredFieldValidator m_loanNumVal;
        private DataTable m_dataTable;

		string[] ids = null;
		string[] names = null;

		protected string m_resultMessage = "";

		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

        protected string LoanProgramID
        {
            set { ViewState["LoanProgramID"] = value; }
            get
            {
                if (ViewState["LoanProgramID"] != null)
                    return (string)ViewState["LoanProgramID"];
                else
                    return "";
            }
        }

        protected Dictionary<Guid, ExportLoanData> FailedExportLoans
        {
            set { ViewState["FailedExportLoans"] = value; }
            get
            {
                if (ViewState["FailedExportLoans"] == null)
                {
                    ViewState["FailedExportLoans"] = new Dictionary<Guid, ExportLoanData>();
                }

                return (Dictionary<Guid, ExportLoanData>)ViewState["FailedExportLoans"];
            }
        }

        // We will save the DataTrac loan program ID that is initially loaded so that we can check if the user changes it
        // That way, we won't be needlessly hitting the database to save the loan program ID every time.
        protected string OriginalLoanProgramID
        {
            set { ViewState["OriginalLoanProgramID"] = value; }
            get
            {
                if (ViewState["OriginalLoanProgramID"] != null)
                    return (string)ViewState["OriginalLoanProgramID"];
                else
                    return "";
            }
        }
		#endregion

        private void LoadOriginatorDDLFromEnum()
        {
            int numElementsInEnum = Enum.GetValues(typeof(DataTracExchange.E_DataTracOriginatorType)).Length;
            DataTracExchange.E_DataTracOriginatorType origType;
            for (int i = 0; i < numElementsInEnum; ++i)
            {
                origType = (DataTracExchange.E_DataTracOriginatorType)Enum.GetValues(typeof(DataTracExchange.E_DataTracOriginatorType)).GetValue(i);
                m_originatorTypeDDL.Items.Insert(i, new ListItem(DataTracExchange.GetDescription(origType), origType.ToString()));
            }

            if (numElementsInEnum > 0)
                m_originatorTypeDDL.SelectedIndex = 0;
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
            RegisterService("main", "/Integration/DataTrac/DataTracValidationService.aspx");

			string userName = DataTracExchange.GetSavedUserName();
			if(!Page.IsPostBack)
			{
                // Load originator type dropdown values from the enum in DataTracExchange.cs
                LoadOriginatorDDLFromEnum();
                
                if(userName.TrimWhitespaceAndBOM() != "")
				{
					m_rememberMyUserName.Checked = true;
					m_loginNm.Text = userName;
				}
				else
				{
					m_rememberMyUserName.Checked = false;
				}

				m_updateExisting.SelectedIndex = 0; // Sets the update existing DataTrac loan file value to true
			}
			
			// 06/02/06 mf - OPM 2583.  We do not use server cache anymore.
			// We pull the list of loans from our custom cache.
			
			string loanList =  DataAccess.AutoExpiredTextCache.GetFromCache("BatchExport" + RequestHelper.GetSafeQueryString("id"));
			
			if (loanList == null) 
			{
				LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this.Page, new string[] {"OK=true"});
				return;
			}

			string[] items = loanList.Split(',');
			int count = items.Length;
			ids = new string[count];
			names = new string[count];

			for (int index = 0; index < count; index++)
			{
				string loanId = items[index].Substring(0, 36);
                // Put back single quote and put comma back if needed.
                string borrAndLoanName = items[index].Substring(36).Replace("&quote", "'").Replace("&comma", ",");
                string[] nameTokens = borrAndLoanName.Split(';');
                string borrName = "";
                string loanName = "";

                if (nameTokens.Length == 1)
                {
                    loanName = nameTokens[0];
                }
                else if (nameTokens.Length > 1)
                {
                    loanName = nameTokens[1];
                    borrName = Utilities.SafeJsLiteralString(nameTokens[0]) + "_" + loanName;
                }

				ids[index] = "'" + loanId + "'";
                names[index] = "'" + Utilities.SafeJsLiteralString(loanName) + "'";
			}

            // Put user code to initialize the page here
            ClientScript.RegisterArrayDeclaration("gLoanNames", string.Join(",", names));
            ClientScript.RegisterArrayDeclaration("gLoanIDs", string.Join(",", ids));
            ClientScript.RegisterArrayDeclaration("gFileNames", string.Join(",", names));

            if (count == 1 && !Page.IsPostBack)
            {
                try
                {
                    Guid loanId = new Guid(items[0].Substring(0, 36));
                    SetLoanProgramIdInfo(loanId);
                }
                catch (Exception exc)
                {
                    Tools.LogError("Unable to set DataTrac loan program information in DataTrac batch exporter.", exc);
                }
            }

            if (FailedExportLoans.Count > 0)
            {
                PopulateDataTable();
            }
		}

        private void SetLoanProgramIdInfo(Guid loanID)
        {
            CPageData dataLoan = new DataTracUIData(loanID);
            dataLoan.InitLoad();
            m_dtLp.Text = dataLoan.sDataTracLpId;
            OriginalLoanProgramID = dataLoan.sDataTracLpId;
            LoanProgramID = Utilities.SafeJsLiteralString(dataLoan.sLpTemplateId.ToString());
        }

		private void SaveUserName()
		{
			string userNameToSave = "";
			if(m_rememberMyUserName.Checked == true)
				userNameToSave = m_loginNm.Text;

			DataTracExchange.SaveUserName(userNameToSave);
		}

        protected void RemoveFromList(object sender, System.EventArgs e)
        {
            foreach (DataGridItem dgi in m_dg.Items)
            {
                CheckBox cb = (CheckBox)dgi.Cells[0].Controls[1];

                if (cb.Checked == true)
                {
                    Guid loanId = new Guid(cb.Attributes["value"].ToString());
                    FailedExportLoans.Remove(loanId);
                }
            }

            PopulateDataTable();
        }

        private void ReExportLoans()
        {
            ArrayList exportLoanData = new ArrayList();
            
            foreach (DataGridItem dgi in m_dg.Items)
            {
                CheckBox cb = (CheckBox)dgi.Cells[0].Controls[1];

                if (cb.Checked == true)
                {
                    Guid loanId = new Guid(cb.Attributes["value"].ToString());
                    string loanNum = ((TableCell)dgi.Cells[1]).Text;
                    ExportLoanData eld = new ExportLoanData(loanId, loanNum, loanNum, ""); //DataTrac loan num is the same as LO loan num when re-exporting from the grid list
                    exportLoanData.Add(eld);
                }
            }

            PerformExport(exportLoanData);
        }

		protected void ExportToDataTrac(object sender, System.EventArgs e)
		{
            try
            {
                SaveUserName();

                foreach (DataGridItem dgi in m_dg.Items)
                {
                    CheckBox cb = (CheckBox)dgi.Cells[0].Controls[1];

                    if (cb.Checked == true)
                    {
                        ReExportLoans();
                        PopulateDataTable();
                        return;
                    }
                }

                if (ids == null || (ids.Length == 1 && DTLoanNum.Text.TrimWhitespaceAndBOM() == ""))
                    return;

                string loanNum = "";
                string dataTracLoanNum = "";
                ArrayList exportLoanData = new ArrayList();

                for (int i = 0; i < ids.Length; ++i)
                {
                    loanNum = names[i].Substring(1, names[i].Length - 2);
                    dataTracLoanNum = string.Copy(loanNum);

                    if (ids.Length == 1)
                    {
                        dataTracLoanNum = DTLoanNum.Text.TrimWhitespaceAndBOM();
                    }

                    Guid loanId = new Guid(ids[i].Substring(1, ids[i].Length - 2));
                    ExportLoanData eld = new ExportLoanData(loanId, loanNum, dataTracLoanNum, "");
                    exportLoanData.Add(eld);
                }

                PerformExport(exportLoanData);
            }
            catch (Exception exc)
            {
                Tools.LogError("Exception in DataTrac batch exporter.", exc);
                m_resultMessage = ErrorMessages.DataTrac_GenericExportErrorMessage;
            }
		}

        private void PerformExport(ArrayList exportLoanData)
        {
            ArrayList dtErrorMessages = null;
            ArrayList dtWarningMessages = null;
            bool updateExisting = (m_updateExisting.SelectedIndex == 0) ? true : false;
            DataTracExchange.E_DataTracOriginatorType origType = (DataTracExchange.E_DataTracOriginatorType)Enum.Parse(typeof(DataTracExchange.E_DataTracOriginatorType), m_originatorTypeDDL.SelectedValue);

            bool saveDtLpId = false;
            string dtLpId = "";

            if (m_dtLp != null)
            {
                dtLpId = m_dtLp.Text;
                saveDtLpId = (OriginalLoanProgramID != m_dtLp.Text);
            }

            Dictionary<Guid, ExportLoanData> failedExportLoans = FailedExportLoans;
            bool exportError = false;
            foreach (ExportLoanData eld in exportLoanData)
            {
                Guid loanId = eld.LoanId;
                string loanNum = eld.LoanNumber;
                string dataTracLoanNum = eld.DataTracLoanNumber;

                DataTracExporter dtExporter = new DataTracExporter(loanId, BrokerUser.BrokerId, Utilities.SafeJsLiteralString(m_loginNm.Text), Utilities.SafeJsLiteralString(m_password.Text), Utilities.SafeJsLiteralString(dataTracLoanNum), updateExisting, origType, Utilities.SafeJsLiteralString(dtLpId), saveDtLpId);
                
                try
                {
                    if (loanId != Guid.Empty)
                    {
                        LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(BrokerUser.BrokerId, loanId, WorkflowOperations.ReadLoanOrTemplate);
                        valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(BrokerUser));

                        bool canRead = LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, valueEvaluator);

                        // The user must at least have read access to export these loans (they do not need write access)
                        if (canRead == false)
                        {
                            throw new DataTracException("You do not have permission to export this loan.", "The user does not have permission to export this loan.");
                        }
                    }

                    dtExporter.Export();
                   
                    dtErrorMessages = dtExporter.ErrorMessagesFromDataTrac;
                    dtWarningMessages = dtExporter.WarningMessagesFromDataTrac;
                    DataTracExchange.SendNotifications(BrokerUser.BrokerId, E_DataTracInteractionT.ExportToDataTrac, loanId, loanNum, dataTracLoanNum);
                    failedExportLoans.Remove(loanId);
                    m_resultMessage = "Successfully exported all selected loans.";
                }
                catch (DataTracException dte)
                {
                    if (ErrorCausesImmediateFailure(dte.UserMessage))
                    {
                        m_resultMessage = dte.UserMessage;
                        return;
                    }
                    exportError = true;
                    if (failedExportLoans.ContainsKey(loanId))
                        failedExportLoans[loanId] = new ExportLoanData(loanId, loanNum, dataTracLoanNum, dte.UserMessage);
                    else
                        failedExportLoans.Add(loanId, new ExportLoanData(loanId, loanNum, dataTracLoanNum, dte.UserMessage));

                    if (DataTracExchange.ShouldLogErrorMessage(dte.UserMessage) == true)
                    {
                        Tools.LogError(dte);
                    }
                }
                catch (CBaseException c)
                {
                    exportError = true;
                    if (failedExportLoans.ContainsKey(loanId))
                        failedExportLoans[loanId] = new ExportLoanData(loanId, loanNum, dataTracLoanNum, c.UserMessage);
                    else
                        failedExportLoans.Add(loanId, new ExportLoanData(loanId, loanNum, dataTracLoanNum, c.UserMessage));
                    Tools.LogError(ErrorMessages.DataTrac_GenericExportErrorMessage, c);
                }
                catch (Exception exc)
                {
                    exportError = true;
                    if (failedExportLoans.ContainsKey(loanId))
                        failedExportLoans[loanId] = new ExportLoanData(loanId, loanNum, dataTracLoanNum, ErrorMessages.DataTrac_GenericExportErrorMessage);
                    else
                        failedExportLoans.Add(loanId, new ExportLoanData(loanId, loanNum, dataTracLoanNum, ErrorMessages.DataTrac_GenericExportErrorMessage));

                    string msg = "(db) Exception in DataTrac batch exporter: " + exc;
                    Tools.LogError(msg);
                }
                finally
                {
                    if (exportError == true)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.Append("The following loan(s) failed to export (Select loan(s) and press 'Start Export' to retry): ");
                        m_resultMessage = sb.ToString();
                    }
                } // end finally
            }// end for loop

            FailedExportLoans = failedExportLoans;

            if (failedExportLoans.Count > 0)
            {
                PopulateDataTable();
            }
        }

        // Certain errors will apply to all loans being exported and will cause an immediate failure, 
        // and therefore we don't need to continue trying subsequent loans.
        private static bool ErrorCausesImmediateFailure(string errorMsg)
        {
            if (errorMsg.ToLower().Contains("cannot connect to datatrac"))
                return true;
            else if (errorMsg.ToLower().Contains("path to datatrac is invalid"))
                return true;
            else if (errorMsg.ToLower().Contains("not have permission"))
                return true;
            else
                return false;
        }

        private void BuildDataTable()
        {
            m_dataTable = new DataTable();
            DataColumn[] keys = new DataColumn[1];
            keys[0] = m_dataTable.Columns.Add("sLId", typeof(string));
            m_dataTable.PrimaryKey = keys;
            m_dataTable.Columns.Add("sLNm", typeof(string));
            m_dataTable.Columns.Add("ErrorInfo", typeof(string));
        }

        private void PopulateDataTable()
        {
            BuildDataTable();
            string loanId = "";
            string loanNum = "";
            string errors = "";

            foreach (ExportLoanData failedLoan in FailedExportLoans.Values)
            {
                if (failedLoan.LoanId != Guid.Empty)
                {
                    loanId = failedLoan.LoanId.ToString();
                    loanNum = failedLoan.LoanNumber;
                    errors = failedLoan.ErrorInfo;
                    AddRow(loanId, loanNum, errors);
                }
            }

            m_dg.DataSource = m_dataTable.DefaultView;
            m_dg.DataBind();

            if (FailedExportLoans.Count == 0)
                m_showDataGrid.Value = "0";
            else
                m_showDataGrid.Value = "1";
        }

        private void AddRow(string loanId, string loanName, string errors)
        {
            if (m_dataTable.Rows.Contains(loanId))
            {
                DataRow row = m_dataTable.Rows.Find(loanId);
                row["ErrorInfo"] = errors;
            }
            else
            {
                DataRow row = m_dataTable.NewRow();

                row["sLId"] = loanId;
                row["sLNm"] = loanName;
                row["ErrorInfo"] = errors;

                m_dataTable.Rows.Add(row);
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}

    [Serializable]
    public class ExportLoanData : ISerializable
    {
        private Guid m_loanId = Guid.Empty;
        private string m_loanNumber = "";
        private string m_dataTracLoanNumber = "";
        private string m_errorInfo = "";

        internal Guid LoanId
        {
            get { return m_loanId; }
        }

        public string LoanNumber
        {
            get { return m_loanNumber; }
        }

        public string ErrorInfo
        {
            get { return m_errorInfo; }
        }

        public string DataTracLoanNumber
        {
            get { return m_dataTracLoanNumber; } 
        }
        
        public ExportLoanData(Guid loanId, string loanNumber, string dataTracLoanNumber, string errorInfo)
        {
            m_loanId = loanId;
            m_loanNumber = loanNumber;
            m_errorInfo = errorInfo.Replace("\\n", "");
            m_dataTracLoanNumber = dataTracLoanNumber;
        }

        #region ISerializable implementation
        protected ExportLoanData(SerializationInfo info, StreamingContext ctxt)
		{
            m_loanId = (Guid)info.GetValue("m_loanId", typeof(Guid));
            m_loanNumber = (string)info.GetValue("m_loanNumber", typeof(string));
            m_errorInfo = (string)info.GetValue("m_errorInfo", typeof(string));
            m_dataTracLoanNumber = (string)info.GetValue("m_dataTracLoanNumber", typeof(string));
		}

		public void GetObjectData(SerializationInfo info, StreamingContext ctxt )
		{
            info.AddValue("m_loanId", m_loanId);
            info.AddValue("m_loanNumber", m_loanNumber);
            info.AddValue("m_errorInfo", m_errorInfo);
            info.AddValue("m_dataTracLoanNumber", m_dataTracLoanNumber);
        }
        #endregion
    }
}
