﻿using System;
using System.Collections;
using System.Data;
using DataAccess;
using DataTrac;
using System.Web;
using System.Xml;

namespace LendersOfficeApp.Integration.DataTrac
{
    public class AssetGridHandler
    {
        public AssetGridHandler()
        {
        }

        public static void CreateRecordXml(XmlElement gridData, CPageData dataTracPageData)
        {
            XmlElement assetRoot = gridData.OwnerDocument.CreateElement("GRID");
            GridFieldHandler.CreateAndAppendAttribute(assetRoot, "Name", "Assets");
            gridData.AppendChild(assetRoot);

            // Loop through each application and map the asset data for each
            CAppData dataApp = null;
            if (dataTracPageData.nApps != 0)
            {
                for (int i = 0; i < dataTracPageData.nApps; ++i)
                {
                    dataApp = dataTracPageData.GetAppData(i);
                    MapAssetDataByApplication(dataApp, i, assetRoot);
                }
            }
        }

        public static void CreateGridRequestXml(XmlElement gridRequestElement)
        {
            XmlElement assetRequestElement = gridRequestElement.OwnerDocument.CreateElement("Grid");
            GridFieldHandler.CreateAndAppendAttribute(assetRequestElement, "Name", "ASSETS");
            gridRequestElement.AppendChild(assetRequestElement);

            XmlElement field1 = gridRequestElement.OwnerDocument.CreateElement("Field");
            GridFieldHandler.CreateAndAppendAttribute(field1, "Name", "BORROWER_FLAG");
            assetRequestElement.AppendChild(field1);

            XmlElement field2 = gridRequestElement.OwnerDocument.CreateElement("Field");
            GridFieldHandler.CreateAndAppendAttribute(field2, "Name", "ASSET_TYPE");
            assetRequestElement.AppendChild(field2);

            XmlElement field3 = gridRequestElement.OwnerDocument.CreateElement("Field");
            GridFieldHandler.CreateAndAppendAttribute(field3, "Name", "HOLDER_NAME");
            assetRequestElement.AppendChild(field3);

            XmlElement field4 = gridRequestElement.OwnerDocument.CreateElement("Field");
            GridFieldHandler.CreateAndAppendAttribute(field4, "Name", "HOLDER_ADDR");
            assetRequestElement.AppendChild(field4);

            XmlElement field5 = gridRequestElement.OwnerDocument.CreateElement("Field");
            GridFieldHandler.CreateAndAppendAttribute(field5, "Name", "HOLDER_CITY");
            assetRequestElement.AppendChild(field5);

            XmlElement field6 = gridRequestElement.OwnerDocument.CreateElement("Field");
            GridFieldHandler.CreateAndAppendAttribute(field6, "Name", "HOLDER_STATE");
            assetRequestElement.AppendChild(field6);

            XmlElement field7 = gridRequestElement.OwnerDocument.CreateElement("Field");
            GridFieldHandler.CreateAndAppendAttribute(field7, "Name", "HOLDER_ZIP");
            assetRequestElement.AppendChild(field7);

            XmlElement field8 = gridRequestElement.OwnerDocument.CreateElement("Field");
            GridFieldHandler.CreateAndAppendAttribute(field8, "Name", "DESCRIPTN");
            assetRequestElement.AppendChild(field8);

            XmlElement field9 = gridRequestElement.OwnerDocument.CreateElement("Field");
            GridFieldHandler.CreateAndAppendAttribute(field9, "Name", "ACCOUNT_NO");
            assetRequestElement.AppendChild(field9);

            XmlElement field10 = gridRequestElement.OwnerDocument.CreateElement("Field");
            GridFieldHandler.CreateAndAppendAttribute(field10, "Name", "ASSET_AMT");
            assetRequestElement.AppendChild(field10);
        }

        // Future use
        public static void SaveRecordsFromGridXml(XmlElement recordRoot, CPageData dataTracPageData)
        {
            /*foreach (XmlElement applicationElement in recordRoot.ChildNodes) //GRID_DATA//GRID//Application
            {
                int appNum = int.Parse(applicationElement.GetAttribute("Number"));
                if (dataTracPageData.nApps >= appNum)
                {
                    Tools.LogBug("Invalid application number '" + appNum + "' requested while attempting to save asset grid data from DataTrac.");
                    return;
                }

                CAppData dataApp = dataTracPageData.GetAppData(appNum);
                
                foreach (XmlElement recordElement in applicationElement.ChildNodes) //GRID_DATA//GRID//Application//Record
                {
                    foreach (XmlElement fieldElement in recordElement.ChildNodes) //GRID_DATA//GRID//Application//Record//Field
                    {

                    }
                }
            }*/
        }

        private static void DeleteExistingAssets(CAppData dataApp)
        {
            var assetColl = dataApp.aAssetCollection.GetSubcollection(true, E_AssetGroupT.All);
            dataApp.aAssetCollection.ClearAll();
        }

        private static E_AssetOwnerT GetPmlOwnerType(string dataTracOwnerType)
        {
            switch (dataTracOwnerType)
            {
                case "B":
                    return E_AssetOwnerT.Borrower;
                case "C":
                    return E_AssetOwnerT.CoBorrower;
                case "J":
                    return E_AssetOwnerT.Joint;
                default:
                    Tools.LogBug("Unhandled DataTrac asset owner type '" + dataTracOwnerType + "' in DataTrac importer - Defaulting to Borrower.");
                    return E_AssetOwnerT.Borrower;
            }
        }

        private static string GetDataTracOwnerType(E_AssetOwnerT assetOwnerType)
        {
            switch (assetOwnerType)
            {
                case E_AssetOwnerT.Borrower:
                    return "B";
                case E_AssetOwnerT.CoBorrower:
                    return "S";
                case E_AssetOwnerT.Joint:
                    return "J";
                default:
                    Tools.LogError("Unhandled Pml asset owner type '" + assetOwnerType + "' in DataTrac AssetGridHandler, defaulting to 'borrower'." );
                    return "B";
            }
        }

        /*
         * DataTrac asset IDs (with exact text from DataTrac dropdown) as of 3/2009:
         * Automobile - 01
         * Bond - 02
         * Bridge Loan Not Dep - 03
         * Cash On Hand - 04
         * Certificate Of Dep - 05
         * Checking Account - 06
         * Earnest Money - 07
         * Gifts Total - 08
         * Gifts Not Deposited - 09
         * Life Insurance - 10
         * Money Market Fund - 11
         * Mutual Fund - 12
         * Net Worth Business - 13
         * Other Liquid Assets - 14
         * Other NonLiq Assets - 15
         * Pending Proceeds - 16
         * Retirement Fund - 17
         * Savings Account - 18
         * Borrowed Funds - 19
         * Stock - 20
         * Trust Account - 21
         */
        private static string GetDataTracAssetType(E_AssetT assetType)
        {
            string dataTracAssetType = "15";
            switch (assetType)
            {
                case E_AssetT.Auto:
                    dataTracAssetType = "01";
                    break;
                case E_AssetT.Bonds:
                    dataTracAssetType = "02";
                    break;
                case E_AssetT.Business:
                    dataTracAssetType = "13";
                    break;
                case E_AssetT.CashDeposit:
                    dataTracAssetType = "04";
                    break;
                case E_AssetT.Checking:
                    dataTracAssetType = "06";
                    break;
                case E_AssetT.GiftEquity:
                    dataTracAssetType = "14";
                    break;
                case E_AssetT.GiftFunds:
                    dataTracAssetType = "14";
                    break;
                case E_AssetT.LifeInsurance:
                    dataTracAssetType = "10";
                    break;
                case E_AssetT.OtherIlliquidAsset:
                    dataTracAssetType = "15";
                    break;
                case E_AssetT.OtherLiquidAsset:
                    dataTracAssetType = "14";
                    break;
                case E_AssetT.PendingNetSaleProceedsFromRealEstateAssets:
                    dataTracAssetType = "16";
                    break;
                case E_AssetT.Retirement:
                    dataTracAssetType = "17";
                    break;
                case E_AssetT.Savings:
                    dataTracAssetType = "18";
                    break;
                case E_AssetT.Stocks:
                    dataTracAssetType = "20";
                    break;
                default:
                    Tools.LogBug("Unhandled asset type " + assetType + " in DataTrac AssetGridHandler mapping class - defaulting to 'Other non-liquid assets'.");
                    dataTracAssetType = "15"; //Default to other non-liquid assets
                    break;
            }
            return dataTracAssetType;
        }

        private static void MapAssetDataByApplication(CAppData dataApp, int numApp, XmlElement element)
        {
            XmlElement appRoot = element.OwnerDocument.CreateElement("Application");
            GridFieldHandler.CreateAndAppendAttribute(appRoot, "Number", numApp);
            element.AppendChild(appRoot);

            // Each field contains:
            // Borrower Type
            // Asset Type
            // Holder Name
            // Holder Street
            // Holder City
            // Holder State
            // Holder Zip
            // Description
            // Account Number
            // Asset Amount
            // Except for our "special" types such as Business, Retirement Funds, and Life Insurance

            string borrowerType = "";
            string assetType = "";
            string holderName = "";
            string holderStreet = "";
            string holderCity = "";
            string holderState = "";
            string holderZip = "";
            string description = "";
            string accountNumber = "";
            string assetAmount = "";
            
            var assetColl = dataApp.aAssetCollection.GetSubcollection(true, E_AssetGroupT.All);

            foreach (var item in assetColl)
            {
                var fields = (IAsset)item;
                if (fields.IsRegularType)
                {
                    XmlElement asset = element.OwnerDocument.CreateElement("Record");
                    appRoot.AppendChild(asset);
                    var loAssetField = (IAssetRegular)fields;

                    XmlElement field = element.OwnerDocument.CreateElement("Field");
                    borrowerType = GetDataTracOwnerType(loAssetField.OwnerT);
                    GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "BORROWER_FLAG");
                    GridFieldHandler.CreateAndAppendAttribute(field, "Value", borrowerType);
                    asset.AppendChild(field);

                    field = element.OwnerDocument.CreateElement("Field");
                    assetType = GetDataTracAssetType(fields.AssetT);
                    GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "ASSET_TYPE");
                    GridFieldHandler.CreateAndAppendAttribute(field, "Value", assetType);
                    asset.AppendChild(field);

                    field = element.OwnerDocument.CreateElement("Field");
                    holderName = loAssetField.ComNm;
                    GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "HOLDER_NAME");
                    GridFieldHandler.CreateAndAppendAttribute(field, "Value", holderName);
                    asset.AppendChild(field);

                    field = element.OwnerDocument.CreateElement("Field");
                    holderStreet = loAssetField.StAddr;
                    GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "HOLDER_ADDR");
                    GridFieldHandler.CreateAndAppendAttribute(field, "Value", holderStreet);
                    asset.AppendChild(field);

                    field = element.OwnerDocument.CreateElement("Field");
                    holderCity = loAssetField.City;
                    GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "HOLDER_CITY");
                    GridFieldHandler.CreateAndAppendAttribute(field, "Value", holderCity);
                    asset.AppendChild(field);

                    field = element.OwnerDocument.CreateElement("Field");
                    holderState = loAssetField.State;
                    GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "HOLDER_STATE");
                    GridFieldHandler.CreateAndAppendAttribute(field, "Value", holderState);
                    asset.AppendChild(field);

                    field = element.OwnerDocument.CreateElement("Field");
                    holderZip = loAssetField.Zip;
                    GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "HOLDER_ZIP");
                    GridFieldHandler.CreateAndAppendAttribute(field, "Value", holderZip);
                    asset.AppendChild(field);

                    field = element.OwnerDocument.CreateElement("Field");
                    description = loAssetField.Desc;
                    GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "DESCRIPTN");
                    GridFieldHandler.CreateAndAppendAttribute(field, "Value", description);
                    asset.AppendChild(field);

                    field = element.OwnerDocument.CreateElement("Field");
                    accountNumber = loAssetField.AccNum.Value;
                    GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "ACCOUNT_NO");
                    GridFieldHandler.CreateAndAppendAttribute(field, "Value", accountNumber);
                    asset.AppendChild(field);

                    field = element.OwnerDocument.CreateElement("Field");
                    assetAmount = loAssetField.Val_rep;
                    GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "ASSET_AMT");
                    GridFieldHandler.CreateAndAppendAttribute(field, "Value", assetAmount);
                    asset.AppendChild(field);
                }
                else
                {
                    if (fields.Val != 0) // The 'special' fields are blank by default with a value of 0, so we only send them if they have a non-zero value 
                    {
                        XmlElement asset = element.OwnerDocument.CreateElement("Record");
                        appRoot.AppendChild(asset);
                        
                        XmlElement field = element.OwnerDocument.CreateElement("Field");
                        borrowerType = GetDataTracOwnerType(fields.OwnerT);
                        GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "BORROWER_FLAG");
                        GridFieldHandler.CreateAndAppendAttribute(field, "Value", borrowerType);
                        asset.AppendChild(field);

                        field = element.OwnerDocument.CreateElement("Field");
                        assetType = GetDataTracAssetType(fields.AssetT);
                        GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "ASSET_TYPE");
                        GridFieldHandler.CreateAndAppendAttribute(field, "Value", assetType);
                        asset.AppendChild(field);

                        field = element.OwnerDocument.CreateElement("Field");
                        description = fields.Desc;
                        GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "DESCRIPTN");
                        GridFieldHandler.CreateAndAppendAttribute(field, "Value", description);
                        asset.AppendChild(field);

                        field = element.OwnerDocument.CreateElement("Field");
                        assetAmount = fields.Val_rep;
                        GridFieldHandler.CreateAndAppendAttribute(field, "DataTracFieldName", "ASSET_AMT");
                        GridFieldHandler.CreateAndAppendAttribute(field, "Value", assetAmount);
                        asset.AppendChild(field);
                    }
                }
            }
        }
    }
}