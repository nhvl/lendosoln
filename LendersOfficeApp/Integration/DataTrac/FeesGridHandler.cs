﻿using System;
using System.Collections;
using System.Data;
using DataAccess;
using DataTrac;
using System.Web;
using System.Xml;
using LendersOffice.Constants;
using LendersOffice.Common;

namespace LendersOfficeApp.Integration.DataTrac
{
    /// <summary>
    /// Fees are located in DataTrac's DOC1 (Fees & Impounds) page
    /// </summary>
    public class FeesGridHandler
    {
        public FeesGridHandler()
        {
        }

        /// <summary>
        /// Creates the XML document that will be sent to our clients via the DataTrac webservice.  Once
        /// on the client side, this XML document will be used to pull the appropriate fields from DataTrac's
        /// system, and those values will then be imported into our system.
        /// </summary>
        /// <param name="gridRequestElement">The top element in the XML Document that will get populated
        /// with the field names being requested from DataTrac's grids.</param>
        public static void CreateGridRequestXml(XmlElement gridRequestElement)
        {
            XmlElement conditionRequestElement = gridRequestElement.OwnerDocument.CreateElement("Grid");
            GridFieldHandler.CreateAndAppendAttribute(conditionRequestElement, "Name", "FEES");
            gridRequestElement.AppendChild(conditionRequestElement);

            XmlElement field1 = gridRequestElement.OwnerDocument.CreateElement("Field");
            GridFieldHandler.CreateAndAppendAttribute(field1, "Name", "DESCRIPTN");
            conditionRequestElement.AppendChild(field1);

            XmlElement field2 = gridRequestElement.OwnerDocument.CreateElement("Field");
            GridFieldHandler.CreateAndAppendAttribute(field2, "Name", "PAID_BY");
            conditionRequestElement.AppendChild(field2);

            XmlElement field3 = gridRequestElement.OwnerDocument.CreateElement("Field");
            GridFieldHandler.CreateAndAppendAttribute(field3, "Name", "PAID_TO");
            conditionRequestElement.AppendChild(field3);

            XmlElement field4 = gridRequestElement.OwnerDocument.CreateElement("Field");
            GridFieldHandler.CreateAndAppendAttribute(field4, "Name", "TOTAL");
            conditionRequestElement.AppendChild(field4);

            XmlElement field5 = gridRequestElement.OwnerDocument.CreateElement("Field");
            GridFieldHandler.CreateAndAppendAttribute(field5, "Name", "POINTS");
            conditionRequestElement.AppendChild(field5);
        }


        /// <summary>
        /// Maps DataTrac's Paid By value to ours.
        /// </summary>
        /// <param name="dataTracPaidByValue">Paid By value in DataTrac has the following options:
        /// Buyer, 
        /// Broker, 
        /// Branch, 
        /// Lender, 
        /// Seller
        /// </param>
        /// <returns>The value of our Paid by dropdown, which has the following values:
        /// 0 - borr pd
        /// 1 - borr fin
        /// 2 - seller
        /// 3 - lender
        /// 4 - broker
        /// </returns>
        private static int GetLOPaidByValue(string dataTracPaidByValue)
        {
            switch (dataTracPaidByValue.ToLower())
            {
                case "u":
                    return 0;
                case "b":
                    return 4;
                case "r":
                    return 3;
                case "l":
                    return 3;
                case "s":
                    return 2;
                default: // default to Borrower Paid, which is the default for our dropdown in the GFE UI
                    Tools.LogWarning("Unhandled DataTrac 'Paid By' value: " + dataTracPaidByValue.ToLower() + " - defaulting to an LO value of 'Borrower Paid'.");
                    return 0;
            }
        }

        // Taken from CAMLDSService with some slight modifications
        private static int RetrieveItemProps(bool toBroker, int iPayer)
        {
            bool apr = false; //GetBool(name + "_ctrl_Apr_chk");
            bool toBr = toBroker; //GetBool(name + "_ctrl_ToBrok_chk");
            int payer = iPayer; //GetInt(name + "_ctrl_PdByT_dd");
            bool fhaAllow = false; //GetBool(name + "_ctrl_Fha_chk");
            bool poc = false; //GetBool(name + "_ctrl_Poc_chk");
            return LosConvert.GfeItemProps_Pack(apr, toBr, payer, fhaAllow, poc);
        }

        public static void SaveRecordsFromGridXml(XmlElement gridRoot, CPageData dataTracPageData)
        {
            dataTracPageData.InitSave(ConstAppDavid.SkipVersionCheck);

            // These fees allow either a Points value or a Total value.  If the file has an existing Points value,
            // but the value coming in from DataTrac is not a Points value, but just a total value, then the Points
            // value will not get erased, and the total value will then be higher than it should be.  To fix this,
            // we will clear out any existing point or total values the first time we see any of the discount points
            // fields, so that only the data coming from DataTrac will be on the file.
            bool hasSeenDiscountPoints = false;

            foreach (XmlElement applicationElement in gridRoot.ChildNodes)
            {
                foreach (XmlElement recordElement in applicationElement.ChildNodes)
                {
                    FeesInfo feesInfo = new FeesInfo();

                    string dtFieldName = "";
                    string value = "";
                    foreach (XmlElement fieldElement in recordElement.ChildNodes)
                    {
                        dtFieldName = fieldElement.GetAttribute("DataTracFieldName").ToUpper();
                        value = fieldElement.GetAttribute("Value");

                        switch (dtFieldName)
                        {
                            case "DESCRIPTN":
                                feesInfo.Description = value;
                                break;
                            case "PAID_BY":
                                feesInfo.PaidBy = value;
                                break;
                            case "PAID_TO":
                                feesInfo.PaidTo = value;
                                break;
                            case "TOTAL":
                                feesInfo.Total = value;
                                break;
                            case "POINTS":
                                feesInfo.Points = value;
                                break;
                            default:
                                Tools.LogError("Unhandled Fees field name '" + dtFieldName + "' in DataTrac importer.");
                                break;
                        }
                    }

                    switch (feesInfo.Description.ToUpper())
                    {
                        case "ORIGINATION FEE":
                            
                            dataTracPageData.sLOrigFPc_rep = "";
                            dataTracPageData.sLOrigFMb_rep = "";
                            
                            if (!string.IsNullOrEmpty(feesInfo.Points) && feesInfo.Points != "0")
                            {
                                dataTracPageData.sLOrigFPc_rep = feesInfo.Points;
                            }
                            else
                            {
                                // db - sLOrigF_rep (basically, the origination fee total), is read-only 
                                // So we must use sLOrigFMb_rep instead to get the total value into the 
                                // sLOrigF_rep field.
                                dataTracPageData.sLOrigFMb_rep = feesInfo.Total;
                            }
                            
                            dataTracPageData.sLOrigFProps = RetrieveItemProps(feesInfo.PaidToBroker, GetLOPaidByValue(feesInfo.PaidBy));
                            break;
                        case "DISCOUNT POINTS":
                        case "DISCOUNT POINT PAID": // This is the only non-plural form of the word Point in the DataTrac fees, I'm not sure why
                        case "DISCOUNT POINTS (FINANCED)":
                            if (hasSeenDiscountPoints == false)
                            {
                                dataTracPageData.sLDiscntPc_rep = "";
                                dataTracPageData.sLDiscntFMb_rep = "";
                            }

                            try
                            {
                                if (!string.IsNullOrEmpty(feesInfo.Points) && feesInfo.Points != "0")
                                {
                                    dataTracPageData.sLDiscntPc += decimal.Parse(feesInfo.Points);
                                }
                                else
                                {
                                    dataTracPageData.sLDiscntFMb += decimal.Parse(feesInfo.Total);
                                }
                            }
                            catch (Exception e)
                            {
                                Tools.LogError("Unable to parse discount point value in DataTrac fees handler.  FeesInfo Points = " + feesInfo.Points + ", FeesInfo Total = " + feesInfo.Total, e);
                            }
                            dataTracPageData.sLDiscntProps = RetrieveItemProps(feesInfo.PaidToBroker, GetLOPaidByValue(feesInfo.PaidBy));
                            hasSeenDiscountPoints = true;
                            break;
                        case "APPRAISAL FEE":
                            dataTracPageData.sApprF_rep = feesInfo.Total;
                            dataTracPageData.sApprFProps = RetrieveItemProps(feesInfo.PaidToBroker, GetLOPaidByValue(feesInfo.PaidBy));
                            break;
                        case "INSPECTION FEE":
                            dataTracPageData.sInspectF_rep = feesInfo.Total;
                            dataTracPageData.sInspectFProps = RetrieveItemProps(feesInfo.PaidToBroker, GetLOPaidByValue(feesInfo.PaidBy));
                            break;
                        case "MORTGAGE BROKER FEE":
                            
                            dataTracPageData.sMBrokFPc_rep = "";
                            dataTracPageData.sMBrokFMb_rep = "";
                            
                            if (!string.IsNullOrEmpty(feesInfo.Points) && feesInfo.Points != "0")
                                dataTracPageData.sMBrokFPc_rep = feesInfo.Points;
                            else
                            {
                                // db - sMBrokF_rep (basically, the MB fee total), is read-only 
                                // So we must use sMBrokFMb_rep instead to get the total value into the 
                                // sMBrokF_rep field.
                                dataTracPageData.sMBrokFMb_rep = feesInfo.Total;
                            }
                            dataTracPageData.sMBrokFProps = RetrieveItemProps(feesInfo.PaidToBroker, GetLOPaidByValue(feesInfo.PaidBy));
                            break;
                        case "CREDIT REPORT FEE":
                            dataTracPageData.sCrF_rep = feesInfo.Total;
                            dataTracPageData.sCrFProps = RetrieveItemProps(feesInfo.PaidToBroker, GetLOPaidByValue(feesInfo.PaidBy));
                            break;
                        case "TAX SERVICE FEE":
                            dataTracPageData.sTxServF_rep = feesInfo.Total;
                            dataTracPageData.sTxServFProps = RetrieveItemProps(feesInfo.PaidToBroker, GetLOPaidByValue(feesInfo.PaidBy));
                            break;
                        case "PROCESSING FEE":
                            dataTracPageData.sProcF_rep = feesInfo.Total;
                            dataTracPageData.sProcFProps = RetrieveItemProps(feesInfo.PaidToBroker, GetLOPaidByValue(feesInfo.PaidBy));
                            break;
                        case "UNDERWRITING FEE":
                            dataTracPageData.sUwF_rep = feesInfo.Total;
                            dataTracPageData.sUwFProps = RetrieveItemProps(feesInfo.PaidToBroker, GetLOPaidByValue(feesInfo.PaidBy));
                            break;
                        case "WIRE FEE":
                            dataTracPageData.sWireF_rep = feesInfo.Total;
                            dataTracPageData.sWireFProps = RetrieveItemProps(feesInfo.PaidToBroker, GetLOPaidByValue(feesInfo.PaidBy));
                            break;
                        case "CLOSING/FUNDING FEE":
                            dataTracPageData.sEscrowF_rep = feesInfo.Total;
                            dataTracPageData.sEscrowFProps = RetrieveItemProps(feesInfo.PaidToBroker, GetLOPaidByValue(feesInfo.PaidBy));
                            break;
                        case "DOCUMENT PREPARATION FEE":
                            dataTracPageData.sDocPrepF_rep = feesInfo.Total;
                            dataTracPageData.sDocPrepFProps = RetrieveItemProps(feesInfo.PaidToBroker, GetLOPaidByValue(feesInfo.PaidBy));
                            break;
                        case "TITLE POLICY":
                            dataTracPageData.sTitleInsF_rep = feesInfo.Total;
                            dataTracPageData.sTitleInsFProps = RetrieveItemProps(feesInfo.PaidToBroker, GetLOPaidByValue(feesInfo.PaidBy));
                            break;
                        case "RECORDING FEE":
                            dataTracPageData.sRecFPc_rep = "";
                            dataTracPageData.sRecFMb_rep = feesInfo.Total;
                            dataTracPageData.sRecFProps = RetrieveItemProps(feesInfo.PaidToBroker, GetLOPaidByValue(feesInfo.PaidBy));
                            break;
                        default:
                            //1/09 db - Ignore any unmapped fees because we will soon be using a Point file and will no longer need this mapping
                            break;
                    } // end switch
                } // end foreach (XmlElement recordElement in applicationElement.ChildNodes)
                break; // We only need to grab the data from the first application since this data is the same for all applications
            } // end foreach (XmlElement applicationElement in gridRoot.ChildNodes)
            dataTracPageData.Save();
        } // end function
    }

    public class FeesInfo
    {
        private string m_description = "";
        private string m_paidTo = "";
        private string m_paidBy = "";
        private string m_total = "";
        private string m_points = "";

        public string Description
        {
            set { m_description = value.TrimWhitespaceAndBOM(); }
            get { return m_description; }
        }

        public bool PaidToBroker
        {
            get { return (PaidTo.ToLower().Equals("broker")); }
        }

        public string PaidTo
        {
            set { m_paidTo = value.TrimWhitespaceAndBOM(); }
            get { return m_paidTo; }
        }

        public string PaidBy
        {
            set { m_paidBy = value.TrimWhitespaceAndBOM(); }
            get { return m_paidBy; }
        }

        public string Total
        {
            set { m_total = value; }
            get { return m_total; }
        }

        public string Points
        {
            set { m_points = value; }
            get { return m_points; }
        }
    }
}