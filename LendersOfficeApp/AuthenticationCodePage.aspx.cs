﻿using System;
using System.Web;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;

namespace LendersOfficeApp
{
    public partial class AuthenticationCodePage : BasePage
    {
        private Guid m_brokerId = Guid.Empty;

        private Guid m_userId = Guid.Empty;

        protected override bool EnableIEOnlyCheck
        {
            get
            {
                return false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            EmployeeDB employee = EmployeeDB.RetrieveByUserId(m_brokerId, m_userId);
            BrokerDB broker = BrokerDB.RetrieveById(this.m_brokerId);

            if (!broker.AllowDeviceRegistrationViaAuthCodePage)
            {
                m_registerCb.Visible = false;
            }

            string step1AuthenticationMode;

            if (string.IsNullOrEmpty(employee.PrivateCellPhone) && employee.UserType == 'B')
            {
                CellphoneContainer.Visible = true;
                step1AuthenticationMode = "email address on file";
                btnSendAuthenticationCode.Text = "Email Me An Authentication Code";
            }
            else
            {
                CellphoneContainer.Visible = false;
                step1AuthenticationMode = "cell phone on file via SMS";
                btnSendAuthenticationCode.Text = "Text Me An Authentication Code";
            }

            
            if (employee.EnableAuthCodeViaAuthenticator)
            {
                Step1Text.InnerText = $"Please get your one time code from your configured authenticator app.";

                if (employee.EnableAuthCodeViaSms)
                {
                    Step1Text.InnerText += $" You may still request a one time code be sent to your {step1AuthenticationMode}.";
                }
                else
                {
                    btnSendAuthenticationCode.Visible = false;
                    CellphoneContainer.Visible = false;
                }
            }

            else if (employee.EnableAuthCodeViaSms)
            {
                Step1Text.InnerText = $"A code was sent to the {step1AuthenticationMode}. If you do not receive the code within the next few minutes, the button directly below can be used to request a new one. ";
            }
            else
            {
                ErrorUtilities.DisplayErrorPage(new AccessDenied(ErrorMessages.SmsAuthenticationNotEnabled, $"User <{m_userId}> does not have EnableAuthCodeViaSms == true"), false, m_brokerId, m_userId);
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            btnSubmit.Click += new EventHandler(btnSubmit_Click);
            btnCancel.Click += new EventHandler(btnCancel_Click);
            btnSendAuthenticationCode.Click += new EventHandler(btnSendAuthenticationCode_Click);

            RegisterJsScript("mask.js");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");

            InitializeDataFromCookie();
        }

        private void InitializeDataFromCookie()
        {
            HttpCookie cookie = Request.Cookies.Get(ConstApp.CookieMultiFactorAuthentication);
            if (cookie == null)
            {
                // 4/28/2014 dd - This mean someone access this page without go through login control.
                RequestHelper.Signout(PrincipalFactory.CurrentPrincipal);
            }

            string encryptBrokerIdUserId = cookie.Value;
            string[] parts = EncryptionHelper.Decrypt(encryptBrokerIdUserId).Split(':');

            m_brokerId = new Guid(parts[0]);
            m_userId = new Guid(parts[1]);
        }

        void btnSendAuthenticationCode_Click(object sender, EventArgs e)
        {
            MultiFactorAuthCodeUtilities.GenerateAndSendNewCode(m_brokerId, m_userId, true);
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
	         RequestHelper.Signout(PrincipalFactory.CurrentPrincipal);   
        }

        void btnSubmit_Click(object sender, EventArgs e)
        {

            if (MultiFactorAuthCodeUtilities.Verify(m_brokerId, m_userId, SecurityPin.Text))
            {
                PrincipalFactory.Create(m_brokerId, m_userId, "?", false /* allowDuplicateLogin */, true /* isStoreToCookie */);

                BrokerDB broker = PrincipalFactory.CurrentPrincipal.BrokerDB;
                if (broker == null)
                {
                    broker = BrokerDB.RetrieveById(this.m_brokerId);
                }

                if (broker.AllowDeviceRegistrationViaAuthCodePage && this.m_registerCb.Visible)
                {
                    UserRegisteredIp.AddKnownIpAddress(this.m_brokerId, m_userId, RequestHelper.ClientIP, m_registerCb.Checked);
                }

                RequestHelper.StoreToCookie(ConstApp.CookieMultiFactorAuthentication, string.Empty, false);
                // Pass empty Guid for PML site id for B-Users.
                RequestHelper.InsertAuditRecord(PrincipalFactory.CurrentPrincipal, brokerPmlSiteId: Guid.Empty);
                EmployeeDB employee = EmployeeDB.RetrieveByUserId(m_brokerId, m_userId);

                if (string.IsNullOrEmpty(employee.PrivateCellPhone) && !string.IsNullOrEmpty(Cellphone.Text))
                {
                    employee.PrivateCellPhone = Cellphone.Text;
                    employee.IsCellphoneForMultiFactorOnly = true;
                    employee.Save(PrincipalFactory.CurrentPrincipal);
                }

                RequestHelper.DoNextPostLoginTask(PostLoginTask.Login);
            }
            else
            {
                DisplayErrorMessage("Invalid authentication code");
                SecurityPin.Text = string.Empty;
            }
        }

        private void DisplayErrorMessage(string errorMessage)
        {
            ErrorMessagePanel.Visible = true;
            ErrorMessage.Text = errorMessage;
        }
    }
}
