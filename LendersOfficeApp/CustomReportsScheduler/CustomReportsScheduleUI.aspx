<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomReportsScheduleUI.aspx.cs" Inherits="LendersOfficeApp.CustomReportsScheduler.CustomReportsScheduleUI" %>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc" TagName="CModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Custom Reports Scheduler</title>
    <LINK href="../css/stylesheet.css" type="text/css" rel="stylesheet"/>
</head>
<body onload ="onInit();">
<script type="text/javascript">

    var editMode = false;
    function onInit() {
        self.resizeTo(640, 700);

        if ('<%= AspxTools.HtmlStringFromQueryString("EditReport") %>' != '') {
            editMode = true;
        
            // Uncomment this and all code mentioning FinishButton
            // to allow user to finish early when editing. Feature still 
            // needs more work for cases when the user might have invalid data
            // in the upcoming pages but tries to finish early
            /*document.getElementById('Button_Finish_Daily').disabled = "";
            document.getElementById('Button_Finish_Weekly').disabled = "";
            document.getElementById('Button_Finish_Monthly').disabled = "";
            document.getElementById('Button_Finish_Once').disabled = "";
            document.getElementById('Button_Finish_Email').disabled = "";*/
        }
        
        <%= AspxTools.JsGetElementById(Next_run_daily)%>.readOnly = true;
        <%= AspxTools.JsGetElementById(Next_run) %>.readOnly = true;
        <%= AspxTools.JsGetElementById(Next_run_monthly) %>.readOnly = true;
        showList(<%= AspxTools.JsNumeric(selectedTabIndex)%>);
        
        
        // Disabling the necessary buttons/fields when editing
        // For daily
        if (<%= AspxTools.JsGetElementById(Everyday) %>.checked)
            <%= AspxTools.JsGetElementById(Frequency_daily) %>.disabled = true;
        if (<%= AspxTools.JsGetElementById(Run_indefinitely_daily) %>.checked)
            <%= AspxTools.JsGetElementById(End_date_daily) %>.disabled = true;
        
        // For weekly
        if (<%= AspxTools.JsGetElementById(Every_x_weeks) %>.checked == false)
            <%= AspxTools.JsGetElementById(Frequency_Monthly) %>.disabled = true;
        if (<%= AspxTools.JsGetElementById(Run_indefinitely_weekly) %>.checked)
            <%= AspxTools.JsGetElementById(End_date) %>.disabled = true;
        
        // For monthly
        if (<%= AspxTools.JsGetElementById(Week_no_week_day) %>.checked)
            <%= AspxTools.JsGetElementById(Days_of_the_month) %>.disabled = true;
        else {
            <%= AspxTools.JsGetElementById(week_no) %>.disabled = true;
            <%= AspxTools.JsGetElementById(Day_of_the_week) %>.disabled = true;
        }
        if (<%= AspxTools.JsGetElementById(run_infinitely) %>.checked)
            <%= AspxTools.JsGetElementById(end_date_textbox) %>.disabled = true;
        
        switch (document.getElementById("m_CurrentPanel").value) {
            case "Step1":
                document.getElementById("Step1").style.display = "";
                document.getElementById("Step2").style.display = "none";
                document.getElementById("Step4").style.display = "none";
                break;

            case "Step2":
                document.getElementById("Step2").style.display = "";
                document.getElementById("Step1").style.display = "none";
                document.getElementById("Step4").style.display = "none";
                break;

            case "Daily":
                document.getElementById("Step3").style.display = "";
                document.getElementById("Step1").style.display = "none";
                document.getElementById("Step4").style.display = "none";
                break;

            case "Weekly":
                document.getElementById("Step4").style.display = "";
                document.getElementById("Step1").style.display = "none";
                document.getElementById("Step3").style.display = "none";
                document.getElementById("Step4").style.display = "none";
                break;

            default:
                document.getElementById("Step1").style.display = "";
                document.getElementById("Step2").style.display = "none";
                document.getElementById("Daily").style.display = "none";
                document.getElementById("Weekly").style.display = "none";
                document.getElementById("Monthly").style.display = "none";
                document.getElementById("OneTime").style.display = "none";
                document.getElementById("Step4").style.display = "none";
                document.getElementById("Step5").style.display = "none";
                break;
        }
    }
       
    function ontextchange_daily() {
         document.getElementById('InvalidNextrunDaily').innerText = "";
        <%= AspxTools.JsGetElementById(Next_run_daily) %>.value = "";
        var today = new Date();
        var valid = validate_daily();
        //var validEnd = validate_DailyEndDate();
        if (valid == true) {
            var args = new Object();
            var freq;
            args["effective_date"] = <%= AspxTools.JsGetElementById(Effective_date_daily) %>.value;
            args["CalculateFrom"] = <%= AspxTools.JsGetElementById(m_CalculateFrom) %>.value;
            args["useEffectiveDate"] = "true";
            if(editMode) 
            {
                if(<%= AspxTools.JsGetElementById(m_cbUseEffectiveDateDaily) %>.checked == false)
                    args["useEffectiveDate"] = "false";
            }
            
            if (<%= AspxTools.JsGetElementById(Everyday) %>.checked)
                freq = 1
            else freq = <%= AspxTools.JsGetElementById(Frequency_daily) %>.value;

            args["frequency"] = freq;
            if (<%= AspxTools.JsGetElementById(Run_indefinitely_daily) %>.checked)
                end_date = "12/31/9999";
            else
                end_date = <%= AspxTools.JsGetElementById(End_date_daily) %>.value;
                      if (end_date == "")
                   end_date = "12/31/9999";

               if (end_date == "12/31/9999")
                  var end_date_confirm = "N/A";
              else 
                    end_date_confirm =end_date;
            args["enddate"] = end_date;

            if (<%= AspxTools.JsGetElementById(CheckBox1) %>.checked)
                args["include_weekend"] = "true";
            else
                args["include_weekend"] = "false";
            var result = gService.main.call("calculate_daily_run", args);
            if (result.value["nextrun"] == "The next run is greater than the end date, this report will never run.") {
                <%= AspxTools.JsGetElementById(Next_run_daily) %>.value = "";
                (document.getElementById('InvalidNextrunDaily').innerText) = result.value["nextrun"];
               }
            else{
                (<%= AspxTools.JsGetElementById(Next_run_daily) %>).value = result.value["nextrun"];
                (document.getElementById('InvalidNextrunDaily').innerText) = "";
              }
                     (document.getElementById('nextRunConfirm').innerText) = result.value["nextrun"];
                     (document.getElementById('endDateConfirm').innerText) = end_date_confirm;
                     if (<%= AspxTools.JsGetElementById(CheckBox1) %>.checked)
                         var sat_sun = "(including Saturday and Sunday).";
                     else
                         var sat_sun = "";
                    
                     switch (freq) {
                         case 1:
                             var schedule = "Everyday" + " " + sat_sun;
                             break;
                         default:
                             var schedule = "Every" + " " + freq + " " + "days" + " " + sat_sun;
                             break;
                     }
                     (document.getElementById('scheduleConfirm').innerText) = schedule;
                     (document.getElementById('runTimeConfirm').innerText) =(<%= AspxTools.JsGetElementById(Run_time_daily) %>).value;
                     (document.getElementById('retryUntilConfirm').innerText) =(<%= AspxTools.JsGetElementById(Retry_until_daily) %>).value;
        }
    }

   
       function validate_one_time() {
        document.getElementById('validate_effdt').innerText = "";
        var today = new Date();
        var next_year = new Date();
        //var FinishButton = document.getElementById('Button_Finish_Once');
        next_year.setFullYear(today.getFullYear() + 1);
        next_year.setDate(today.getDate());
        next_year.setMonth(today.getMonth());
        var effective_date = new Date(<%= AspxTools.JsGetElementById(onetime_text) %>.value);
        if ((effective_date > today) || (today.getFullYear() == effective_date.getFullYear() && today.getMonth() == effective_date.getMonth() && today.getDate() == effective_date.getDate())) {
            if ((effective_date < next_year)) {
                //FinishButton.disabled = editMode ? false : true;
                document.getElementById("one_time_next").disabled = false;
                document.getElementById('validate_effdt').innerText = "";
                (document.getElementById('nextRunConfirm').innerText) = <%= AspxTools.JsGetElementById(onetime_text) %>.value;
                (document.getElementById('endDateConfirm').innerText) = "N/A";
                (document.getElementById('scheduleConfirm').innerText) = "One Time";
                (document.getElementById('runTimeConfirm').innerText) ="11:00 PM";
                (document.getElementById('retryUntilConfirm').innerText) ="1:00 AM";
           
            }
            else {
                document.getElementById('validate_effdt').innerText = "Run date should be within one year of today's date";
                //FinishButton.disabled = true;
                document.getElementById("one_time_next").disabled = true;
            }
        }
        else {
            document.getElementById('validate_effdt').innerText = "Run date cannot be in the past.";
            //FinishButton.disabled = true;
            document.getElementById("one_time_next").disabled = true;
        }
    }
    
    function validate_week(source, args) {
        document.getElementById('effective_date_weekly').innerText = "";
        document.getElementById('MonthlyEnddate').innerText = "";
        //var FinishButton = document.getElementById('Button_Finish_Weekly');        
        var is_effective_date_valid;
        var is_checkbox_list_valid;
        var today = new Date();
        var next_year = new Date();
        next_year.setFullYear(today.getFullYear() + 1);
        next_year.setDate(today.getDate());
        next_year.setMonth(today.getMonth());
        var effective_date = new Date(<%= AspxTools.JsGetElementById(Effective_Date) %>.value);
        if ((effective_date > today) || (today.getFullYear() == effective_date.getFullYear() && today.getMonth() == effective_date.getMonth() && today.getDate() == effective_date.getDate())) {
            if ((effective_date < next_year)) {
                //FinishButton.disabled = editMode ? false : true;
                document.getElementById("weekly_next").disabled = false;
                document.getElementById('effective_date_weekly').innerText = "";
                is_effective_date_valid = true;
            }
            else {
                document.getElementById('effective_date_weekly').innerText = "Effective date should be within one year of today's date.";
                //FinishButton.disabled = true;
                document.getElementById("weekly_next").disabled = true;
                is_effective_date_valid = false;
            }

        }
        else if(editMode) {
            is_effective_date_valid = true;
        }
        else {
            document.getElementById('effective_date_weekly').innerText = "Effective date cannot be in the past.";
            //FinishButton.disabled = true;
            document.getElementById("weekly_next").disabled = true;
            is_effective_date_valid = false;
        }

        is_checkbox_list_valid = false;
        var list = <%= AspxTools.JsGetElementById(ListBox_weekdays) %>;
        for (var i = 0; i < list.options.length; i++) {
            if (list[i].selected)
                is_checkbox_list_valid = true;
        }

        var is_end_date_valid= true;
        if (<%= AspxTools.JsGetElementById(End_date_weekly) %>.checked)
        {

            if (<%= AspxTools.JsGetElementById(End_date) %>.value == "")
             {
                document.getElementById('MonthlyEnddate').innerText = "Please enter an end date.";
                //FinishButton.disabled = true;
                <%= AspxTools.JsGetElementById(weekly_next) %>.disabled = true;
                is_end_date_valid = false;
               }
        }


        if (is_checkbox_list_valid == false) {
            document.getElementById('checkbox_weekly').innerText = "Please select at least one day";
            //FinishButton.disabled = true;
            document.getElementById('weekly_next').disabled = true;
        }
        if (is_effective_date_valid == false && is_checkbox_list_valid == true) {
            document.getElementById('checkbox_weekly').innerText = "";
        }
        if (is_checkbox_list_valid == true && is_effective_date_valid == true) {
            document.getElementById('effective_date_weekly').innerText = "";
            document.getElementById('checkbox_weekly').innerText = "";
            if (is_end_date_valid == true) {
                document.getElementById('MonthlyEnddate').innerText = "";
                //FinishButton.disabled = editMode ? false : true;
                document.getElementById('weekly_next').disabled = false;
                return true;
            }
        }
        else return false;
    }
    //function validate_DailyEndDate()
     //{
      //   
       //  document.getElementById("daily_next").disabled = false;
        //var is_end_date_valid = true;
    //if (<%= AspxTools.JsGetElementById(End_date_radiobutton) %>.checked) {

    //    if (<%= AspxTools.JsGetElementById(End_date_daily) %>.value == "") {
          //      document.getElementById('DailyEndDate').innerText = "Please enter an end date.";
            //    document.getElementById("daily_next").disabled = true;
              //  is_end_date_valid = false;
            //}
       // }

    //    return is_end_date_valid;
     //}
  function validate_daily() {
      document.getElementById("daily_next").disabled = false;
      document.getElementById('eff_date_daily').innerText = "";
      document.getElementById('DailyEndDate').innerText = "";
      //var FinishButton = document.getElementById('Button_Finish_Daily');
      var today = new Date();
      var next_year = new Date(), flag;
      is_end_date_valid = true; 
      next_year.setFullYear(today.getFullYear() + 1);
      next_year.setDate(today.getDate());
      next_year.setMonth(today.getMonth());
      var effective_date = new Date(<%= AspxTools.JsGetElementById(Effective_date_daily) %>.value);

      if (<%= AspxTools.JsGetElementById(End_date_radiobutton) %>.checked) {
          if (<%= AspxTools.JsGetElementById(End_date_daily) %>.value == "") {
              document.getElementById('DailyEndDate').innerText = "Please enter an end date.";
              document.getElementById("daily_next").disabled = true;
              //FinishButton.disabled = true;
              is_end_date_valid = false;
          }
      }
      if ((effective_date > today) || (today.getFullYear() == effective_date.getFullYear() && today.getMonth() == effective_date.getMonth() && today.getDate() == effective_date.getDate())) 
      {
          if ((effective_date < next_year))
          {
             document.getElementById('eff_date_daily').innerText = "";
          if (is_end_date_valid) {
              document.getElementById('DailyEndDate').innerText = "";
              document.getElementById("daily_next").disabled = false;
              //FinishButton.disabled = editMode ? false : true;
              return true;
                }
          }
          else {
              document.getElementById('eff_date_daily').innerText = "Effective date should be within one year of today's date.";
              document.getElementById("daily_next").disabled = true;
              //FinishButton.disabled = true;
               return false;
          }
      }
      else if(editMode) {
          return true;
      }
      else {
          document.getElementById('eff_date_daily').innerText = "Effective date cannot be in the past.";
          document.getElementById("daily_next").disabled = true;
          //FinishButton.disabled = true;
          return false;
      }
          
        }
        function validate_month(source, args) {
            document.getElementById('monthly_date_validation').innerText = "";
            document.getElementById('Monthly_checkbox_validation').innerText = "";
            document.getElementById('MonthlyEnddateValidation').innerText = "";
            //var FinishButton = document.getElementById('Button_Finish_Monthly');
            var is_effective_date_valid;
            var is_checkbox_list_valid;
            var today = new Date();
            var next_year = new Date();
            document.getElementById('monthly_next').disabled = true;
            //FinishButton.disabled = true;
            next_year.setFullYear(today.getFullYear() + 1);
            next_year.setDate(today.getDate());
            next_year.setMonth(today.getMonth());
            var effective_date = new Date(<%= AspxTools.JsGetElementById(Effective_date_monthly) %>.value);
            if ((effective_date > today) || (today.getFullYear() == effective_date.getFullYear() && today.getMonth() == effective_date.getMonth() && today.getDate() == effective_date.getDate())) {
                if (effective_date < next_year) {
                    document.getElementById('monthly_date_validation').innerText = "";
                    is_effective_date_valid = true;
                }
                else {
                    document.getElementById('monthly_date_validation').innerText = "Effective date should be within one year of today's date.";
                    is_effective_date_valid = false;
                }
            }
            else if(editMode) {
                is_effective_date_valid = true;
            }
            else {
                document.getElementById('monthly_date_validation').innerText = "Effective date cannot be in the past.";
                
                is_effective_date_valid = false;
            }
            is_checkbox_list_valid = false;
            var list = <%= AspxTools.JsGetElementById(ListBox_months) %>;
            for (var i = 0; i < list.options.length; i++) {
                if (list[i].selected)
                    is_checkbox_list_valid = true;
            }
        
           if (is_checkbox_list_valid == false) {
                document.getElementById('Monthly_checkbox_validation').innerText = "Please select at least one month.";
               
               }
            if ( is_checkbox_list_valid == true) {
            document.getElementById('Monthly_checkbox_validation').innerText = "";
           
        }
        
        var daySelected = true;
        if (<%= AspxTools.JsGetElementById(Days_of_Month_monthly) %>.checked) {
          var list = <%= AspxTools.JsGetElementById(Days_of_the_month) %>;
          daySelected = false;
            for (var i = 0; i < list.options.length; ++i) {
              if (list[i].selected)
                daySelected = true;
          }
          
          if (!daySelected)
            document.getElementById('Monthly_checkbox_validation').innerText = "Please select a day of the month.";
        }

        var endDateValid = true;
        if (<%= AspxTools.JsGetElementById(end_date_monthly) %>.checked) {
            if (<%= AspxTools.JsGetElementById(end_date_textbox) %>.value == "") {
                document.getElementById('MonthlyEnddateValidation').innerText = "Please enter an end date.";
                endDateValid = false;
           }
       }
       
       if (is_checkbox_list_valid == true && is_effective_date_valid == true && endDateValid == true && daySelected == true)  {
            document.getElementById('monthly_date_validation').innerText = "";
            document.getElementById('Monthly_checkbox_validation').innerText = "";
            document.getElementById('MonthlyEnddateValidation').innerText = "";
            document.getElementById('monthly_next').disabled = false;
            //FinishButton.disabled = editMode ? false : true;
            return true;
        }
       else return false;   
        }

function ontextchange_weekly() {
    <%= AspxTools.JsGetElementById(Next_run) %>.value = "";
        //var FinishButton = document.getElementById('Button_Finish_Weekly');        
        var valid = validate_week();
        if (valid == true) {
          //FinishButton.disabled = editMode ? false : true;
          <%= AspxTools.JsGetElementById(weekly_next) %>.disabled = false;
          var args = new Object();
          args["effective_date"] = <%= AspxTools.JsGetElementById(Effective_Date) %>.value;
          args["CalculateFrom"] = <%= AspxTools.JsGetElementById(m_CalculateFrom) %>.value;
          args["useEffectiveDate"] = "true";
          if(editMode) 
          {
              if(<%= AspxTools.JsGetElementById(m_cbUseEffectiveDateWeekly) %>.checked == false)
                  args["useEffectiveDate"] = "false";
          }
          if (<%= AspxTools.JsGetElementById(Run_indefinitely_weekly) %>.checked)
              end_date = "12/31/9999";
          else
              end_date = <%= AspxTools.JsGetElementById(End_date) %>.value;
          if (end_date == "")
              end_date = "12/31/9999";
         
          args["enddate"] = end_date;
          var freq;
          var weekdays = "";
          //The next four lines retrieve values from the textboxes and drop box in the UI.
          if (<%= AspxTools.JsGetElementById(Weekly_weekly) %>.checked)
              freq = 1;
          else if (<%= AspxTools.JsGetElementById(Biweekly) %>.checked)
              freq = 2;
          else
              freq = <%= AspxTools.JsGetElementById(Frequency_Monthly) %>.value;
          args["frequency"] = freq;
          
          var list = <%= AspxTools.JsGetElementById(ListBox_weekdays) %>;
          if (list[0].selected)
          { args["Monday"] = "true"; weekdays += "Monday, "; }
          else
              args["Monday"] = "false";
          if (list[1].selected)
          { args["Tuesday"] = "true"; weekdays += "Tuesday, "; }
          else
              args["Tuesday"] = "false";
          if (list[2].selected)
          { args["Wednesday"] = "true"; weekdays += "Wednesday, "; }
          else
              args["Wednesday"] = "false";
          if (list[3].selected)
          { args["Thursday"] = "true"; weekdays += "Thursday, "; }
          else 
             args["Thursday"] = "false";
          if (list[4].selected)
          { args["Friday"] = "true"; weekdays += "Friday, "; }
          else
              args["Friday"] = "false";
          if (list[5].selected)
          { args["Saturday"] = "true"; weekdays += "Saturday, "; }
          else
              args["Saturday"] = "false";
          if (list[6].selected)
          { args["Sunday"] = "true"; weekdays += "Sunday, "; }
          else
              args["Sunday"] = "false";
              
          if (end_date == "12/31/9999")
              var end_date_confirm = "N/A";
          else 
              end_date_confirm = end_date;
             
                    weekdays=weekdays.substring(0,weekdays.length-2);
                    ptr = weekdays.lastIndexOf(",");
                    if (ptr > 0) {
                        weekdays = weekdays.substring(0, ptr) + " and" + weekdays.substring(ptr + 1, weekdays.length);
                    }
                switch (freq) {

                    case 1:
                        var schedule = "Every week on " + weekdays;
                        break;
                    case 2:
                        var schedule = "Biweekly on " + weekdays;
                        break;
                    default:
                        var schedule = "Every " + freq + " weeks on " + weekdays;
                        break;
                      }

                      var result = gService.main.call("calculate_weekly_run", args);
                      if (result.value["nextrun"] == "The next run is greater than the end date, this report will never run.") {
                          (<%= AspxTools.JsGetElementById(Next_run) %>).value = "";
                      }
                      else { (<%= AspxTools.JsGetElementById(Next_run) %>).value = result.value["nextrun"]; }
          (document.getElementById('nextRunConfirm').innerText) = result.value["nextrun"];
     (document.getElementById('endDateConfirm').innerText) = end_date_confirm;
     (document.getElementById('scheduleConfirm').innerText) =schedule;
     (document.getElementById('runTimeConfirm').innerText) =(<%= AspxTools.JsGetElementById(Run_time_weekly) %>).value;
     (document.getElementById('retryUntilConfirm').innerText) =(<%= AspxTools.JsGetElementById(Retry_until_weekly) %>).value;
     
          }
      else {
          //FinishButton.disabled = true;      
          <%= AspxTools.JsGetElementById(weekly_next) %>.disabled = true;
      }
  }


  function ontextchange_monthly() {
      document.getElementById('EndDateMonthlyCheck').innerText = "";
      <%= AspxTools.JsGetElementById(Next_run_monthly) %>.value = "";
      //var FinishButton = document.getElementById('Button_Finish_Monthly');
      var valid = validate_month();
      
      if (valid == true) {
          <%= AspxTools.JsGetElementById(monthly_next) %>.disabled = false;
          //FinishButton.disabled = editMode ? false : true;
          var args = new Object();
          args["effective_date"] = <%= AspxTools.JsGetElementById(Effective_date_monthly) %>.value;
          args["CalculateFrom"] = <%= AspxTools.JsGetElementById(m_CalculateFrom) %>.value;
          args["useEffectiveDate"] = "true";
          if(editMode) 
          {
              if(<%= AspxTools.JsGetElementById(m_cbUseEffectiveDateMonthly) %>.checked == false)
                  args["useEffectiveDate"] = "false";
          }
          if (<%= AspxTools.JsGetElementById(run_infinitely) %>.checked)
              end_date = "12/31/9999";
          else
              end_date = <%= AspxTools.JsGetElementById(end_date_textbox) %>.value;
          if (end_date == "")
              end_date = "12/31/9999";
          args["end_date"] = end_date;
          var month_list = <%= AspxTools.JsGetElementById(ListBox_months) %>;
          var months = "";
          var months_selected ="";
          for (var j = 0; j < month_list.options.length; j++) {
              comma = ",";
              if (j == (month_list.options.length) - 1)
                  comma = "";
              if (month_list[j].selected) {
                  months += 1 + comma;
                  months_selected += month_list[j].text + ", ";
              }
              else
                  months += 0 + comma;
          }

          months_selected = months_selected.substring(0, months_selected.length - 2);
          ptr = months_selected.lastIndexOf(",");
          if (ptr > 0) {
              months_selected = months_selected.substring(0, ptr) + " and" + months_selected.substring(ptr +1, months_selected.length);
          }
          args["months_of_year"] = months;


          if (<%= AspxTools.JsGetElementById(Week_no_week_day) %>.checked) {
              args["option_monthly"] = "option1";
              args["week_no"] = <%= AspxTools.JsGetElementById(week_no) %>.value;
              args["Day_of_the_week"] = <%= AspxTools.JsGetElementById(Day_of_the_week) %>.value;
              var weekno = <%= AspxTools.JsGetElementById(week_no) %>.value;
              switch (weekno) {
                  case "1":
                      weekno = "First";
                      break;
                  case "2":
                      weekno = "Second";
                      break;
                  case "3":
                      weekno = "Third";
                      break;
                  case "4":
                      weekno = "Fourth";
                      break;
                  case "5":
                      weekno = "Last";
                      break;
              }
              var weekday = <%= AspxTools.JsGetElementById(Day_of_the_week) %>.value;
           Confirm_months = "The " + weekno + " " + weekday + " of " + months_selected;
                   }
          else {
              var dates_selected = "";
              args["option_monthly"] = "option2";
              var list = <%= AspxTools.JsGetElementById(Days_of_the_month) %>;
              var textstring = "";
              for (var i = 0; i < list.options.length; ++i) {
                  comma = ",";
                  if (i == (list.options.length) - 1)
                      comma = "";
                  if (list[i].selected) {
                      textstring += 1 + comma;
                      dates_selected += list[i].text + ", ";
                  }
                  else
                      textstring += 0 + comma;
              }
                dates_selected = dates_selected.substring(0, dates_selected.length - 2);
              ptr = dates_selected.lastIndexOf(",");
              if (ptr > 0) {
                  dates_selected = dates_selected.substring(0, ptr) + " and" + dates_selected.substring(ptr + 1, dates_selected.length);
              }
              
              args["Days_of_month"] = textstring;
              Confirm_months = dates_selected + " of " + months_selected;
         
         
          }
              //   var result = gService.main.call("calculate_monthly_run", args);
              //   alert(result.value["daysselected"]);
          //   // (<%= AspxTools.JsGetElementById(Next_run) %>).value = result.value["nextrun"];
          //    (<%= AspxTools.JsGetElementById(Next_run_monthly) %>).value = result.value["daysselected"];
         if (end_date == "12/31/9999")
                  var end_date_confirm = "N/A";
              else 
                    end_date_confirm =end_date;
                var result = gService.main.call("calculate_monthly_run", args);
                if ((result.value["nextrun"] == "The next run is greater than the end date, this report will never run.") || (result.value["nextrun"] == "Next run cannot be an invalid date or more than two years from today.")) {
                    (<%= AspxTools.JsGetElementById(Next_run) %>).value = "";
                    (document.getElementById('EndDateMonthlyCheck').innerText) = result.value["nextrun"];
                    
                }
                else {
                    (<%= AspxTools.JsGetElementById(Next_run_monthly) %>).value = result.value["nextrun"];
                    (document.getElementById('EndDateMonthlyCheck').innerText) = "";
                }
          
          (document.getElementById('nextRunConfirm').innerText) = result.value["nextrun"];
          (document.getElementById('endDateConfirm').innerText) = end_date_confirm;
          (document.getElementById('scheduleConfirm').innerText) = Confirm_months;
          (document.getElementById('runTimeConfirm').innerText) =(<%= AspxTools.JsGetElementById(Run_time_monthly) %>).value;
          (document.getElementById('retryUntilConfirm').innerText) =(<%= AspxTools.JsGetElementById(Retry_until_monthly) %>).value;
          
      }
      else {
          //FinishButton.disabled = true;
          <%= AspxTools.JsGetElementById(monthly_next) %>.disabled = true;
      }
  }
        
  function fournext(){
    if(validate_mail_list() == true) {
      var ddl = document.getElementById(document.getElementById("m_ReportTabSelected").value);
      var selectedOption = ddl.options[ddl.selectedIndex];
      var selectedText = selectedOption.text;

      document.getElementById('recipientConfirm').innerText = (<%= AspxTools.JsGetElementById(sender_name) %>).value;
      document.getElementById('CCConfirm').innerText = (<%= AspxTools.JsGetElementById(CC) %>).value;
      document.getElementById('crQueryConfirm').innerText = selectedText;
      document.getElementById('scheduleNameConfirm').innerText = (<%= AspxTools.JsGetElementById(Schedule_name) %>).value;
      showPanel('Step5');
    }
  }

    function getFrequencyValue(){
        var frequencylist = document.getElementsByName("Frequencylist");
        for (var i = 0; i < frequencylist.length; i++) {
            if (!frequencylist[i].checked) continue;
            return frequencylist[i].value;
        }
    }


    function Onclose() {
        if (confirm("Do you really want to cancel?" == true))
            window.close();
    }
    function showPanel(sPanel) {
        document.getElementById("Step1").style.display = "none";
        document.getElementById("Step2").style.display = "none";
        document.getElementById("Step4").style.display = "none";
        document.getElementById("Daily").style.display = "none";
        document.getElementById("Weekly").style.display = "none";
        document.getElementById("Monthly").style.display = "none";
        document.getElementById("OneTime").style.display = "none";
        document.getElementById("Step5").style.display = "none";
        
        if (sPanel != null) {
            switch (sPanel) {
                case "Step1":
                    document.getElementById("Step1").style.display = "";
                    break;

                case "Step2":
                    document.getElementById("Step2").style.display = "";
                    break;

                case "twoclicked":
                    var frequencyValue = getFrequencyValue();
                    if (frequencyValue == "Daily") {
                        document.getElementById("Daily").style.display = "";
                        ontextchange_daily();
                    } else if (frequencyValue == "Weekly") {
                        document.getElementById("Weekly").style.display = "";
                        ontextchange_weekly();
                    } else if (frequencyValue == "Monthly") {
                        document.getElementById("Monthly").style.display = "";
                        ontextchange_monthly();
                    } else if (frequencyValue == "OneTime") {
                        document.getElementById("OneTime").style.display = "";
                        validate_one_time();
                    } else if (frequencyValue == "Never") {
                        self.close();
                    }

                    break;

                case "Step4":
                    validate_mail_list();
                    document.getElementById("Step4").style.display = "";
                    break;

                case "Step5":
                    document.getElementById("Step5").style.display = "";

                    break;

                case "FourBack":
                    var frequencyValue = getFrequencyValue();
                    if (frequencyValue == "Daily") {
                        document.getElementById("Daily").style.display = "";
                    } else if (frequencyValue == "Weekly") {
                        document.getElementById("Weekly").style.display = "";
                    } else if (frequencyValue == "Monthly") {
                        document.getElementById("Monthly").style.display = "";
                    } else if (frequencyValue == "OneTime") {
                        document.getElementById("OneTime").style.display = "";
                    }

                    break;

                default:
                    document.getElementById("Step1").style.display = "";
                    document.getElementById("Step2").style.display = "none";
                    document.getElementById("Step4").style.display = "none";
                    document.getElementById("Step5").style.display = "none";
                    break;
            }

            document.getElementById("m_CurrentPanel").value = sPanel;

        }

        if (sPanel == null) {
            document.getElementById("Step1").style.display = "";
            document.getElementById("Step2").style.display = "none";
            document.getElementById("Step3").style.display = "none";
            document.getElementById("Step4").style.display = "none";
        }
    }


    function disable_enable()
    {
        var frequencyValue = getFrequencyValue();

        var isNever = frequencyValue == "Never";

        document.getElementById("Button_Next").disabled = isNever ? "true" : "";
        document.getElementById("Button_Finish").disabled = isNever ? "" : "true";
    }

    function InsertIntoDatabase() 
    {
        //This function inserts the values into the database
        var args = new Object();
        var origEffectiveDate = <%= AspxTools.JsGetElementById(m_EffectiveDate) %>.value;
        args["ReportId"] = <%= AspxTools.SafeUrlFromQueryString("EditReport") %>;
        args["LastRun"] = <%= AspxTools.JsGetElementById(m_LastRun) %>.value;

        choice = getFrequencyValue();
        args["choice"] = choice;
        switch (choice) {
          
            case "Daily":
                 var frequency;
                var includeSatSun;
                var endDate;
                if (<%= AspxTools.JsGetElementById(Run_indefinitely_daily) %>.checked == true)
                    endDate = "12/31/9999";
                else endDate = <%= AspxTools.JsGetElementById(End_date_daily) %>.value;
                if (<%= AspxTools.JsGetElementById(Everyday) %>.checked == true)
                    frequency = 1;
                else
                    frequency = <%= AspxTools.JsGetElementById(Frequency_daily) %>.value;
                if (<%= AspxTools.JsGetElementById(CheckBox1) %>.checked == true)
                    includeSatSun = "yes";
                else
                    includeSatSun = "no";
                      args["frequency"] = frequency;
                args["includeSatSun"] = includeSatSun;
                args["endDate"] = endDate;
                var effectiveDate = <%= AspxTools.JsGetElementById(Effective_date_daily) %>.value;
                if (effectiveDate == "" || effectiveDate == null)
                    args["effectiveDate"] = origEffectiveDate;
                else
                    args["effectiveDate"] = effectiveDate;
                args["nextRun"] = <%= AspxTools.JsGetElementById(Next_run_daily) %>.value;
                args["runTime"]=<%= AspxTools.JsGetElementById(Run_time_daily) %>.value;
                args["retryUntil"]=<%= AspxTools.JsGetElementById(Retry_until_daily) %>.value;
                var result = gService.main.call("InsertIntoDatabase", args);
                break;
            case "Weekly":
                var weeklyFrequency;
                var endDateWeekly;
                if (<%= AspxTools.JsGetElementById(Weekly_weekly) %>.checked == true)
                    weeklyFrequency = 1;
                else if (<%= AspxTools.JsGetElementById(Biweekly) %>.checked == true)
                    weeklyFrequency = 2;
                else
                    weeklyFrequency = <%= AspxTools.JsGetElementById(Frequency_Monthly) %>.value;
                var arr = new Array();
                var weeks = "";
                var list = <%= AspxTools.JsGetElementById(ListBox_weekdays) %>;
                for (var i = 0; i < list.options.length; i++) {
                    if (list[i].selected)
                        weeks += list[i].text + ",";
                }
                if (<%= AspxTools.JsGetElementById(Run_indefinitely_weekly) %>.checked == true)
                    endDateWeekly = "12/31/9999";
                else endDateWeekly = <%= AspxTools.JsGetElementById(End_date) %>.value;
                weeks = weeks.substring(0, weeks.length - 1);
                args["frequency"] = weeklyFrequency;
                args["weeks"] = weeks;
                var effectiveDate = <%= AspxTools.JsGetElementById(Effective_Date) %>.value;
                if (effectiveDate == "" || effectiveDate == null)
                    args["effectiveDate"] = origEffectiveDate;
                else
                    args["effectiveDate"] = effectiveDate;
                args["endDate"] = endDateWeekly;
                args["nextRun"] = <%= AspxTools.JsGetElementById(Next_run) %>.value;
                args["runTime"]=<%= AspxTools.JsGetElementById(Run_time_weekly) %>.value;
                args["retryUntil"]=<%= AspxTools.JsGetElementById(Retry_until_weekly) %>.value;
                var result = gService.main.call("InsertIntoDatabase", args);
                break;
            case "Monthly":
                if (<%= AspxTools.JsGetElementById(run_infinitely) %>.checked == true)
                    endDateWeekly = "12/31/9999";
                else endDateWeekly = <%= AspxTools.JsGetElementById(end_date_textbox) %>.value;
                var effectiveDate = <%= AspxTools.JsGetElementById(Effective_date_monthly) %>.value;
                if (effectiveDate == "" || effectiveDate == null)
                    args["effectiveDate"] = origEffectiveDate;
                else
                    args["effectiveDate"] = effectiveDate;
                args["endDate"] = endDateWeekly;
                args["nextRun"] = <%= AspxTools.JsGetElementById(Next_run_monthly) %>.value;
                args["runTime"]=<%= AspxTools.JsGetElementById(Run_time_monthly) %>.value;
                args["retryUntil"]=<%= AspxTools.JsGetElementById(Retry_until_monthly) %>.value;
                
                var month_list = <%= AspxTools.JsGetElementById(ListBox_months) %>;
                var months_selected = "";
                for (var j = 0; j < month_list.options.length; j++) {
                    comma = ",";
                    if (j == (month_list.options.length) - 1)
                        comma = "";
                    if (month_list[j].selected) {
                        months_selected += month_list[j].text + comma;
                    }
                    else
                        months_selected += "0" + comma;
                }
                
               // months_selected = months_selected.substring(0, months_selected.length - 1);
                args["months"] = months_selected;

                if (<%= AspxTools.JsGetElementById(Week_no_week_day) %>.checked)
                 {
                    args["option_monthly"] = "option1";
                    args["week_no"] = <%= AspxTools.JsGetElementById(week_no) %>.value;
                    args["weekday"] = <%= AspxTools.JsGetElementById(Day_of_the_week) %>.value;
                    var weekno = <%= AspxTools.JsGetElementById(week_no) %>.value;
                   
                }
                else {
                    var dates_selected = "";
                    args["option_monthly"] = "option2";
                    var list = <%= AspxTools.JsGetElementById(Days_of_the_month) %>;
                    var textstring = "";
                    for (var i = 0; i < list.options.length; ++i) {
                        comma = ",";
                        if (i == (list.options.length) - 1)
                            comma = "";
                        if (list[i].selected) {
                            textstring += 1 + comma;
                        }
                        else
                            textstring += 0 + comma;
                    }
                   args["Days_of_month"] = textstring;
                   
                }
                var result = gService.main.call("InsertIntoDatabase", args)
                break;
            case "OneTime":
                var effectiveDate = <%= AspxTools.JsGetElementById(onetime_text) %>.value;
                if (effectiveDate == "" || effectiveDate == null)
                    args["effectiveDate"] = origEffectiveDate;
                else
                    args["effectiveDate"] = effectiveDate;
                var result = gService.main.call("InsertIntoDatabase", args)
                break;
        }
            
    }
   
    function validate_mail_list() {
        var ErrMsg = document.getElementById('Step4Error');
        ErrMsg.innerText = "";
        //var FinishButton = document.getElementById('Button_Finish_Email');
        document.getElementById("step4_next").disabled = false;
        //FinishButton.disabled = editMode ? false : true;
        var args = new Object();
        args["CC_list"] = <%= AspxTools.JsGetElementById(CC) %>.value;
        args["Your_address"] = <%= AspxTools.JsGetElementById(sender_name) %>.value;
        var result = gService.main.call("validate_mail_list", args);
        if (result.value["valid"] != "true") {
            document.getElementById("step4_next").disabled = true;
            //FinishButton.disabled = true;
            ErrMsg.innerText = result.value["valid"];
            return false;
        }
        else {
            //FinishButton.disabled = editMode ? false : true;
            document.getElementById("step4_next").disabled = false;
            return true;
        }
    }

    function insertIntoScheduledReportUserInfo() {
    var args = new Object();
    args["mailId"] = <%= AspxTools.JsGetElementById(sender_name) %>.value;
    args["CC"] = <%= AspxTools.JsGetElementById(CC) %>.value;
    var customquery = document.getElementById(document.getElementById("m_ReportTabSelected").value);
    var selectedOption = customquery.options[customquery.selectedIndex];
    args["cRQ"] = selectedOption.text;
    args["scheduleName"] = <%= AspxTools.JsGetElementById(Schedule_name) %>.value;
    args["queryId"] = selectedOption.value;
    args["ReportId"] = <%= AspxTools.SafeUrlFromQueryString("EditReport") %>;
    choice = getFrequencyValue();
    args["choice"] = choice;
    var result = gService.main.call("insertIntoScheduledReportUserInfo", args);
    }

    function validateStep1() {

        var ScheduleName = <%= AspxTools.JsGetElementById(Schedule_name) %>.value;
        var Reports = document.getElementById(document.getElementById("m_ReportTabSelected").value);
        var ErrMsg = document.getElementById('Step1Error');
        ErrMsg.innerText = "";
        
        if (ScheduleName == "" || ScheduleName == null) {
            ErrMsg.innerText = "Please enter a Schedule Name";
            document.getElementById("Step1Next").disabled = true;
        }
        else if (Reports.selectedIndex == -1) {
            ErrMsg.innerText = "Please select a report to schedule";
        }
        else {
            document.getElementById("Step1Next").disabled = false;
            showPanel('Step2');
            disable_enable();
        }

    }

    function enableNext() {
        document.getElementById("Step1Next").disabled = false;
    }
    
    function saveSchedule() {
         insertIntoScheduledReportUserInfo();
         InsertIntoDatabase();
         window.location.href ='ScheduledCustomReports.aspx';
    }

    function showList(selection) {
        var yourReports = document.getElementById('YourReportsTab');
        var othersReports = document.getElementById('OthersReportsTab');
        var publishedReports = document.getElementById('PublishedReportsTab');
        var favReports = document.getElementById('FavReportsTab');
        var sListId;
        
        favReports.style.display = "none";
        yourReports.style.display = "none";
        othersReports.style.display = "none";
        publishedReports.style.display = "none";
        
        for(var i = 0; i < 4; i++)
        {
            document.getElementById("tab" + i).className = "";
        }
        
        document.getElementById("tab" + selection).className = "selected";
        switch (selection) {
            case 0:
            case '0':
                sListId = "FavReports";
                f_validate(document.getElementById('FavReports'));
                favReports.style.display = "";
                break;
            case 1:
            case '1':
                sListId = "YourReports";
                f_validate(document.getElementById('YourReports'));
                yourReports.style.display = "";
                break;

            case 2:
            case '2':
                sListId = "OthersReports";
                f_validate(document.getElementById('OthersReports'));
                othersReports.style.display = "";
                break;

            case 3:
            case '3':
                sListId = "PublishedReports";
                f_validate(document.getElementById('PublishedReports'));
                publishedReports.style.display = "";
                break;
        }
        document.getElementById("m_ReportTabSelected").value = sListId;
    }
    
    function updateEffectiveDate(status) {
        var daily = document.getElementById('Effective_date_daily');
        var weekly = document.getElementById('Effective_Date');
        var monthly = document.getElementById('Effective_date_monthly');
        var date = document.getElementById('m_EffectiveDate');
        
        daily.readOnly = !status;
        weekly.readOnly = !status;
        monthly.readOnly = !status;
        
        if (status) {
            daily.value = date.value;
            weekly.value = date.value;
            monthly.value = date.value;
        }
        else {
            daily.value = "";
            weekly.value = "";
            monthly.value = "";
        }
    }

    function ModifyListSelection(name, value) {
        var list = document.getElementById(name);
        for (var i = 0; i < list.options.length; i++) {
            list[i].selected = value;
        }
    }

    function f_validate(obj){
        if (!obj || !obj.options || typeof(obj.selectedIndex) == "undefined" || obj.selectedIndex == -1)
            return;
        var nextBtn = document.getElementById("Step1Next");
        var entry = obj.options[obj.selectedIndex];
        if (typeof(entry.restricted) != "undefined"){
            nextBtn.disabled = true;
        }
        else{
            nextBtn.disabled = false;
        }
    }
    <% //Long Nguyen
    // From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
    //To add compatible Object.keys support in older environments that do not natively support it
    %>
    if (!Object.keys) {
        Object.keys = (function() {
            'use strict';
            var hasOwnProperty = Object.prototype.hasOwnProperty,
                hasDontEnumBug = !({ toString: null }).propertyIsEnumerable('toString'),
                dontEnums = [
                  'toString',
                  'toLocaleString',
                  'valueOf',
                  'hasOwnProperty',
                  'isPrototypeOf',
                  'propertyIsEnumerable',
                  'constructor'
                ],
                dontEnumsLength = dontEnums.length;

            return function(obj) {
                if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
                    throw new TypeError('Object.keys called on non-object');
                }

                var result = [], prop, i;

                for (prop in obj) {
                    if (hasOwnProperty.call(obj, prop)) {
                        result.push(prop);
                    }
                }

                if (hasDontEnumBug) {
                    for (i = 0; i < dontEnumsLength; i++) {
                        if (hasOwnProperty.call(obj, dontEnums[i])) {
                            result.push(dontEnums[i]);
                        }
                    }
                }
                return result;
            };
        }());
    }


    function generateRetryUntil(value)
    {
        var minTS=stringToDate(value);
        var maxTS=stringToDate("5:00 AM");
        var tempTS=minTS;
        tempTS.setHours(tempTS.getHours()+2);
        var retryList={};
        while(tempTS <= maxTS){
            var val=dateToString(tempTS);
            var text=dateToString(tempTS);
            retryList[val]=text;
            tempTS.setHours(tempTS.getHours()+1);
        }
        return retryList;
    };
    function FillUpRetryUntil(item,options)
    {
        item.find('option').remove().end();
        $.each(options, function(val, text) {
            item.append(
                $('<option></option>').val(val).text(text)
            );
        });
        item.val(options[Object.keys(options)[0]]);
    }
    function RunTimeTextChange(item)
    {
        switch(item.id){
            case "Run_time_daily":
                FillUpRetryUntil($('#Retry_until_daily'),generateRetryUntil(item.value));
                ontextchange_daily();
                break;
            case "Run_time_weekly":
                FillUpRetryUntil($('#Retry_until_weekly'),generateRetryUntil(item.value));
                ontextchange_weekly();
                break;
            case "Run_time_monthly":
                FillUpRetryUntil($('#Retry_until_monthly'),generateRetryUntil(item.value));
                ontextchange_monthly();
                break;
        }
    };
    function stringToDate(str){
        var d = new Date();
        <%//http://stackoverflow.com/questions/141348/what-is-the-best-way-to-parse-a-time-into-a-date-object-from-user-input-in-javas/141504#141504
        %>
        var time = str.match(/^(\d+)(:(\d\d))?\s*((a|(p))m?)?$/i);
        if(time[1]!=="12")
        {
            if(time[4]==="AM")d.setDate(d.getDate()+1);
            d.setHours( parseInt(time[1])+(time[4]==="PM" ? 12 : 0));
        }
        else{
            //12AM
            d.setDate(d.getDate()+1);
            d.setHours(0);
        }
        d.setMinutes( parseInt(time[3]) || 0 );
        return d;
    }
    function dateToString(d)
    {
        var hours=d.getHours();
        var min=d.getMinutes();
        var ampm = "AM";
        if(hours===0)
        {
            ampm = "AM";
            hours=12;
        }
        else{
            if(hours<12&&hours>0){
                ampm = "AM";
            }
            if(hours===12)
            {
                ampm = "PM";
                hours=12;
            }
            if(hours>12){
                ampm = "PM";
                hours=hours-12;
            }
        }
        return hours+":"+pad(min,2)+" "+ampm;
    }
    function pad(num, size) {
        <%//http://stackoverflow.com/questions/2998784/how-to-output-integers-with-leading-zeros-in-javascript
        %>
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    }
    function showRetryUntilInfo(){
        //var div=$(item).find("div").get(0);
        var div=$(".RetryUntilNotif");
        div.html("If there is an issue running your report, it will be re-run every 30 minutes until the time you set here. <br />"+
                "<br />"+
                "At this set time an email notification will be sent to you and the LendingQB support team if the issue persists.<br /><br />"+
                "<div style=\"text-align:center\"><a href=\"javascript:void(0)\" onmousedown=\"showRetryUntilInfo();\">close</a></div>");
        if (false == div.is(':visible')) {
            div.show();
        }
        else {
            div.hide();
        }
    }
    $(document).mouseup(function (e)
    {
        //console.log(e);
        if(e.target.nodeName==="IMG")
        {
                if(e.target.nameProp==="warn.png") return;
        }
        <%//http://stackoverflow.com/questions/1403615/use-jquery-to-hide-a-div-when-the-user-clicks-outside-of-it
        %>
        var container = $(".RetryUntilNotif");

        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.hide();
        }
    });
    
</script>
<style type="text/css">
.RetryUntilNotif
{
	margin: 22 0 0 -100;
	padding: 5px;
	width:180px;
	height:200px;
	background-color:White;
    position: absolute;
    border: 1px solid #000000;
}
</style>

<form id="form1" runat="server">
    <input id="m_EffectiveDate" type="hidden" runat="server" value="" />
    <input id="m_LastRun" type="hidden" runat="server" value="" />
    <input id="m_CalculateFrom" type="hidden" runat="server" value="" />
    <input id="m_ReportTabSelected" type="hidden" runat="server" name="m_ReportTabSelected" value="FavReports" />
    <input id="m_CurrentPanel" runat="server" style="DISPLAY:none" />
    
    <table cellspacing="2" cellpadding="3" width="100%" border="0">
	<tr><td class="FormTableHeader" style="font-size:12px">Scheduled Reports</td></tr>
	<tr>
	    <td>
	<div id="Step1">
        <h5>Step 1: Choose a Report</h5>
        <table style="margin:10px">
            <tr>
                <td valign="top">
            <table align="center" cellSpacing="0" cellPadding="0" border="0" width="100%">
                <tr>
                  <td>
                    <div class = "Tabs">
                      <ul class="tabnav">
                          <li id="tab0" runat="server"><a href="#" onclick="showList(0);">Favorite reports</a></li>
                          <li id="tab1" runat="server"><a href="#" onclick="showList(1);">Your reports</a></li>
                          <li id="tab2" runat="server"><a href="#" onclick="showList(2);">Others' reports</a></li>
                          <li id="tab3" runat="server"><a href="#" onclick="showList(3);">Published reports</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
                <tr>
                    <td style="height: 250px; BORDER-TOP-WIDTH: 2px; BORDER-RIGHT: 2px outset; PADDING-BOTTOM: 5px; BORDER-LEFT: 2px outset; PADDING-TOP: 5px; BORDER-BOTTOM: 2px outset" align="center" valign="middle">
                       
                                    
                                
                                    <div id="FavReportsTab" style="display:none; margin-top: 5px;">
                                        <asp:ListBox ID="FavReports" onchange="f_validate(this);" onclick="f_validate(this);" runat="server" Height="188px" Width="256px" DataTextField="QueryName" DataValueField="QueryId" />
                                        <br/>
                                        <span style="font-style:italic;">** represents reports containing restricted fields</span>
                                   </div>
                                    <div id="YourReportsTab" style="display:none; margin-top: 5px;">
                                        <asp:ListBox ID="YourReports" onchange="f_validate(this);" onclick="f_validate(this);" runat="server" Height="188px" Width="256px" DataTextField="QueryName" DataValueField="QueryId" />
                                        <br/>
                                        <span style="font-style:italic;">** represents reports containing restricted fields</span>
                                   </div>
                                    <div id="OthersReportsTab" style="display:none; margin-top: 5px;">
                                        <asp:ListBox ID="OthersReports" onchange="f_validate(this);" onclick="f_validate(this);" runat="server" Height="188px" Width="256px" DataTextField="QueryName" DataValueField="QueryId" />
                                        <br/>
                                        <span style="font-style:italic;">** represents reports containing restricted fields</span>
                                  </div>
                                    <div id="PublishedReportsTab" style="display:none; margin-top: 5px;">
                                        <asp:ListBox ID="PublishedReports" onchange="f_validate(this);" onclick="f_validate(this);" runat="server" Height="188px" Width="256px" DataTextField="NamePublishedAs" DataValueField="QueryId" />
                                        <br/>
                                        <span style="font-style:italic;">** represents reports containing restricted fields</span>
                                    </div>
                                
                    </td>
                </tr>
            </table><br /><br />
        
        <div style="margin-left: 10px">
            Schedule Name
            <asp:TextBox ID="Schedule_name" onclick="enableNext();" runat="server" Width="418px" Height="20px" Style="margin-left: 13px" MaxLength="30" /><br /><br />
            <span id="Step1Error" style="color: #FF0000; text-transform: none;"></span>
        </div>
        
        </td></tr></table>
        <hr />
        <div style="padding: 8px; text-align: right; width: 580px;">
            <input disabled="disabled" style="width: 60px" type="button" value="&lt; Back" />
            <input type="button" id="Step1Next" value="Next &gt;" style="width: 60px" onclick="validateStep1();">
            <input id="Button_Finish0" runat="server" disabled="disabled" style="width: 60px" type="button" value="Finish" onclick="saveSchedule();" />
            <input type="button" value="Cancel" style="width: 60px" onclick="window.location ='ScheduledCustomReports.aspx'">
        </div>
    </div>
    
    <div id="Step2" runat="server">
        <h5>Step 2: Report Interval</h5>
        <table height="470px" style="margin:10px"><tr><td valign="top">
        
	    <asp:RadioButtonList id="Frequencylist" runat="server" CellSpacing="20" Height="34px" >
            <asp:ListItem value="Daily"   onclick="disable_enable();" text="Daily" />
            <asp:ListItem value="Weekly"  onclick="disable_enable();" text="Weekly" Selected="True" />
            <asp:ListItem value="Monthly" onclick="disable_enable();" text="Monthly" />
            <asp:ListItem value="OneTime" onclick="disable_enable();" text="One Time" />
            <asp:ListItem value="Never"   onclick="disable_enable();" text="Never"  />
        </asp:RadioButtonList>
            
	    </td></tr></table>
	    <hr />
        <div style="padding: 8px; text-align: right; width: 580px;" dir="ltr">
		    <input type="button" value="&lt; Back" style="WIDTH: 60px" onclick="showPanel( 'Step1' );">
		    <input type="button" runat= "server" id="Button_Next" value="Next &gt;" style="WIDTH: 60px" onclick="showPanel( 'twoclicked' );">
		    <input type="button" id="Button_Finish" runat="server" value="Finish" style="WIDTH: 60px" onclick="saveSchedule();" disabled="disabled">
		    <input type="button" value="Cancel" style="WIDTH: 60px" onclick="window.location ='ScheduledCustomReports.aspx'">
        </div>
	</div>
					
	<div id="Daily" runat="server"> 
		<h5>Step 3: Report Schedule (Note : All reports are run after business hours Pacific Time)</h5>
        <table height="470px" width="90%" style="margin:10px"><tr><td valign="top"><table width="100%">
            <tr>
                <td colspan="2" class="FieldLabel">
                    Daily Report
                </td>
            </tr>
            <tr><td colspan="6"><hr style="color:lightgrey" /></td></tr>
            <tr>
                <td colspan="2">
                    <asp:RadioButton ID="Everyday" Text="Everyday" GroupName="Group1" runat="server" onclick="javascript:document.form1.Frequency_daily.disabled=true;ontextchange_daily();" CausesValidation="True" Checked="True"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:RadioButton ID="Every__days" Text="Every" GroupName="Group1" runat="server" onclick="javascript:document.form1.Frequency_daily.disabled=false;ontextchange_daily();" CausesValidation="True"/>
                    &nbsp;
                    <asp:DropDownList ID="Frequency_daily" onchange="ontextchange_daily();" onselectedindexchange="calculate_daily_run();" runat="server" CausesValidation="True">
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                        <asp:ListItem Value="6">6</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;days
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br /><asp:CheckBox ID="CheckBox1" runat="server" Text=" Include Saturday and Sunday" onclick="ontextchange_daily();" CausesValidation="True" />
                </td>
            </tr>
            <tr><td colspan="6"><hr style="color:lightgrey; margin:10px:0px:10px:0px" /></td></tr>
            <tr>
                <td style="width:1%" noWrap>
                    Effective date
                </td>
                <td style="width:1%">
                    <ML:DateTextBox ID="Effective_date_daily" CausesValidation="true" runat="server" preset="date" onchange="ontextchange_daily();"></ML:DateTextBox>
                </td>
                <td>
                    <asp:CheckBox ID="m_cbUseEffectiveDateDaily" Checked="true" runat="server" Text="Use effective date " onclick="updateEffectiveDate(this.checked);ontextchange_daily();"/>                
                </td>
                <td>&nbsp;</td>
                <td style="width:3%">
                    Run Time
                </td>
                <td>
                    <asp:DropDownList ID="Run_time_daily" runat="server" onchange="RunTimeTextChange(this);" >
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Next run
                </td>
                <td>
                    <asp:TextBox ID="Next_run_daily" runat="server" Width="74px"></asp:TextBox>
                </td>
                <td>
                    <span id="eff_date_daily" style="color: #FF0000; text-transform: none;"></span>
                </td>
                <td>&nbsp;</td>
                <td style="width:3%;">
                    Retry Until&nbsp;<span><a href="javascript:void(0)" onmousedown="showRetryUntilInfo();"><img alt="" src="../images/warn.png" style="border:none;"/></a>
                    <div class="RetryUntilNotif" style="display:none"></div>
                    </span>
                </td>
                <td>
                    <asp:DropDownList ID="Retry_until_daily" runat="server" onchange="ontextchange_daily();"></asp:DropDownList>
                </td>
            </tr>
            <tr><td colspan="6"><hr style="color:lightgrey; margin:10px:0px:10px:0px" /></td></tr>
            <tr>
                <td colspan="2">
                    <asp:RadioButton ID="Run_indefinitely_daily" runat="server" GroupName="Group2" Text="Run indefinitely" onclick="javascript:document.form1.End_date_daily.disabled=true;ontextchange_daily();" Checked="True" CausesValidation="True"/>
                </td>
                <td>
                    <span id="InvalidNextrunDaily" style="color: #FF0000; text-transform: none;"></span>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RadioButton ID="End_date_radiobutton" Text="End date" runat="server" GroupName="Group2" onclick="javascript:document.form1.End_date_daily.disabled=false;ontextchange_daily();" CausesValidation="True"/>
                </td>
                <td>
                    <ML:DateTextBox ID="End_date_daily" runat="server" Width="74px" CausesValidation="false" onclick="javascript:document.form1.End_date_daily.disabled=false" onchange="ontextchange_daily();"></ML:DateTextBox>
                </td>
                <td>
                    <span id="DailyEndDate" style="color: #FF0000; text-transform: none;"></span>        
                </td>
            </tr>
        </table></td></tr></table>
        <hr />
        <div style="padding: 8px; TEXT-ALIGN: right; width: 580px;">
			<input type="button" value="&lt; Back" style="WIDTH: 60px" onclick="showPanel( 'Step2' );">
			<input type="button" id ="daily_next" value="Next &gt;" style="WIDTH: 60px" onclick="ontextchange_daily();showPanel( 'Step4' );">
            <input id="Button_Finish_Daily" type="button" value="Finish" style="WIDTH: 60px" disabled="DISABLED" onclick="saveSchedule();">
			<input type="button" value="Cancel" style="WIDTH: 60px" onclick="window.location ='ScheduledCustomReports.aspx'">
		</div>
	</div>
						
	<div id="Weekly" runat="server" >
		<h5>Step 3: Report Schedule (Note : All reports are run after business hours Pacific Time)</h5>
        <table height="470px" width="90%" style="margin:10px"><tr><td valign="top"><table width="100%">
            <tr>
                <td colspan="3" class="FieldLabel">
                    Weekly Report
                </td>
            </tr>
            <tr><td colspan="6"><hr style="color:lightgrey" /></td></tr>
            <tr>
                <td colspan="3">
                    <table>
                        <tr>
                            <td valign="middle" style="width:40%">
                                <asp:RadioButton ID="Weekly_weekly" runat="server" GroupName="Group3" Text="Weekly" onclick="javascript:document.form1.Frequency_Monthly.disabled=true;ontextchange_weekly();" Checked="True" CausesValidation="True"/><br />
                                <asp:RadioButton ID="Biweekly" runat="server" GroupName="Group3" Text="Biweekly" onclick="javascript:document.form1.Frequency_Monthly.disabled=true;ontextchange_weekly();"  Checked="False" CausesValidation="True"/><br />                                
                                <asp:RadioButton ID="Every_x_weeks" Text="Every" runat="server" GroupName ="Group3" onclick="javascript:document.form1.Frequency_Monthly.disabled=false;ontextchange_weekly();" CausesValidation="True" />
                                <asp:DropDownList ID="Frequency_Monthly" onchange="ontextchange_weekly();" runat="server" CausesValidation="True" Width="36px" Height="18px">
                                    <asp:ListItem value="3">3</asp:ListItem>
                                    <asp:ListItem value="4">4</asp:ListItem>
                                    <asp:ListItem value="5">5</asp:ListItem>
                                    <asp:ListItem value="6">6</asp:ListItem>
                                </asp:DropDownList>
		                        weeks
                            </td>
                            <td align="center">
                                <span id="checkbox_weekly" style="color: #FF0000; font-variant: normal; text-transform: none;" ></span><br />
                                <asp:ListBox ID="ListBox_weekdays" runat="server" onchange="ontextchange_weekly();" SelectionMode="Multiple" Rows="7" >
                                    <asp:ListItem Value="Monday" Text="Monday" />
                                    <asp:ListItem Value="Tuesday" Text="Tuesday" />
                                    <asp:ListItem Value="Wednesday" Text="Wednesday" />
                                    <asp:ListItem Value="Thursday" Text="Thursday" />
                                    <asp:ListItem Value="Friday" Text="Friday" />
                                    <asp:ListItem Value="Saturday" Text="Saturday" />
                                    <asp:ListItem Value="Sunday" Text="Sunday" />
                                </asp:ListBox><br />
                                To select multiple days, click while holding down control<br />
                                Select <a href="#" onclick="ModifyListSelection('ListBox_weekdays', true);ontextchange_weekly();">all</a>, <a href="#" onclick="ModifyListSelection('ListBox_weekdays', false);ontextchange_weekly();">none</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><td colspan="6"><hr style="color:lightgrey; margin:10px:0px:10px:0px" /></td></tr>
            <tr>
                <td style="width:1%" noWrap>
                    Effective date
                </td>
                <td style="width:1%">
                    <ML:DateTextBox ID="Effective_Date" CausesValidation="true" runat="server" preset="date" onchange="ontextchange_weekly();"></ML:DateTextBox>
                </td>
                <td>
                    <asp:CheckBox ID="m_cbUseEffectiveDateWeekly" Checked="true" runat="server" Text="Use effective date " onclick="updateEffectiveDate(this.checked);ontextchange_weekly();"/>
                </td>
                <td>&nbsp;</td>
                <td style="width:3%">
                    Run Time
                </td>
                <td>
                    <asp:DropDownList ID="Run_time_weekly" runat="server" onchange="RunTimeTextChange(this);" >
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Next run
                </td>
                <td>
                    <asp:TextBox ID="Next_run" runat="server" Width="74px"></asp:TextBox>
                </td>
                <td>
                    <span id="effective_date_weekly" style="color: #FF0000; font-variant: normal; text-transform: none"></span>
                </td>
                <td>&nbsp;</td>
                <td style="width:3%">
                    Retry Until&nbsp;<span><a href="javascript:void(0)" onmousedown="showRetryUntilInfo();"><img alt="" src="../images/warn.png" style="border:none;"/></a>
                    <div class="RetryUntilNotif" style="display:none"></div>
                    </span>
                </td>
                <td>
                    <asp:DropDownList ID="Retry_until_weekly" runat="server" onchange="ontextchange_weekly();"></asp:DropDownList>
                </td>
            </tr>
            <tr><td colspan="6"><hr style="color:lightgrey; margin:10px:0px:10px:0px" /></td></tr>
            <tr>
                <td colspan="2">
                    <asp:RadioButton ID="Run_indefinitely_weekly" runat="server" GroupName="Group7" Text="Run indefinitely" onclick="javascript:document.form1.End_date.disabled=true;ontextchange_weekly();" Checked="True" CausesValidation="True"/>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RadioButton ID="End_date_weekly" Text="End date" runat="server" GroupName ="Group7" onclick="javascript:document.form1.End_date.disabled=false;ontextchange_weekly();" CausesValidation="True"/>
                </td>
                <td>
                    <ML:DateTextBox ID="End_date" onclick="javascript:document.form1.End_date.disabled=false;javascript:document.form1.End_date_weekly.checked=true;" CausesValidation="true" runat="server" preset="date" onchange="ontextchange_weekly();"></ML:DateTextBox>
                </td>
                <td>
                    <span id="MonthlyEnddate" style="color: #FF0000; font-variant: normal; text-transform: none"></span>
                </td>
            </tr>
        </table></td></tr></table>
        <hr />
        <div style="padding: 8px; TEXT-ALIGN: right; width: 580px;">
            <input type="button" value="&lt; Back" style="WIDTH: 60px" onclick="showPanel( 'Step2' );">   
		    <input type="button" id="weekly_next" disabled="true" runat="server" value="Next &gt;" style="WIDTH: 60px" onclick="showPanel( 'Step4' );">
			<input id="Button_Finish_Weekly" type="button" value="Finish" style="WIDTH: 60px" disabled="disabled" onclick="saveSchedule();">
			<input type="button" value="Cancel" style="WIDTH: 60px" onclick="window.location ='ScheduledCustomReports.aspx'">
		</div>
	</div>
						
	<div id="Monthly" runat="server">
		<h5>Step 3: Report Schedule (Note : All reports are run after business hours Pacific Time)</h5>
        <table height="470px" width="90%" style="margin:10px"><tr><td valign="top"><table width="100%">
            <tr>
                <td colspan="3" class="FieldLabel">
                    Monthly Report
                </td>
            </tr>
            <tr><td colspan="6"><hr style="color:lightgrey" /></td></tr>
            <tr>
                <td colspan="6">
                    <table>
                        <tr>
                            <td align="center" colspan="6">
                                <span id="Monthly_checkbox_validation" style="color: #FF0000; font-variant: normal; text-transform: none"></span><br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Report runs on the
                            </td>
                            <td>
                                <asp:RadioButton ID="Week_no_week_day" runat="server" onclick="javascript:document.form1.week_no.disabled=false;javascript:document.form1.Day_of_the_week.disabled=false;javascript:document.form1.Days_of_the_month.disabled=true;ontextchange_monthly();" GroupName="grp1" Checked="True" />                             
                            </td>
                            <td>
                                <asp:DropDownList ID="week_no" runat="server" onchange="ontextchange_monthly();" >
                                    <asp:ListItem Value ="1">1st</asp:ListItem>
                                    <asp:ListItem Value ="2">2nd</asp:ListItem>
                                    <asp:ListItem Value ="3" Text="3rd"></asp:ListItem>
                                    <asp:ListItem Value ="4">4th</asp:ListItem>
                                    <asp:ListItem Value ="5">Last</asp:ListItem>
                                </asp:DropDownList>   
                            </td>
                            <td>
                                <asp:DropDownList ID="Day_of_the_week" onchange="ontextchange_monthly();" runat="server" >
                                    <asp:ListItem Value="Monday">Monday</asp:ListItem>
                                    <asp:ListItem Value="Tuesday">Tuesday</asp:ListItem>
                                    <asp:ListItem Value="Wednesday">Wednesday</asp:ListItem>
                                    <asp:ListItem Value="Thursday">Thursday</asp:ListItem>
                                    <asp:ListItem Value="Friday">Friday</asp:ListItem>
                                    <asp:ListItem Value="Saturday">Saturday</asp:ListItem>
                                    <asp:ListItem Value="Sunday">Sunday</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width:50px" align="center">
                                of
                            </td>
                            <td valign="top" rowspan="2">
                                <asp:ListBox ID="ListBox_months" runat="server" SelectionMode="Multiple" onclick="ontextchange_monthly();" Rows="12" >
                                    <asp:ListItem Value="January" Text="January" />
                                    <asp:ListItem Value="February" Text="February" />
                                    <asp:ListItem Value="March" Text="March" />
                                    <asp:ListItem Value="April" Text="April" />
                                    <asp:ListItem Value="May" Text="May" />
                                    <asp:ListItem Value="June" Text="June" />
                                    <asp:ListItem Value="July" Text="July" />
                                    <asp:ListItem Value="August" Text="August" />
                                    <asp:ListItem Value="September" Text="September" />
                                    <asp:ListItem Value="October" Text="October" />
                                    <asp:ListItem Value="November" Text="November" />
                                    <asp:ListItem Value="December" Text="December" />
                                </asp:ListBox><br />
                                Select <a href="#" onclick="ModifyListSelection('ListBox_months', true);ontextchange_monthly();">all</a>, <a href="#" onclick="ModifyListSelection('ListBox_months', false);ontextchange_monthly();">none</a>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td valign="top">
                                <asp:RadioButton id="Days_of_Month_monthly" onclick="javascript:document.form1.week_no.disabled=true;javascript:document.form1.Day_of_the_week.disabled=true;javascript:document.form1.Days_of_the_month.disabled=false;ontextchange_monthly();" runat="server" GroupName="grp1" /> 
                            </td>
                            <td>
                                <asp:ListBox ID="Days_of_the_month" runat="server" onchange="ontextchange_monthly();" SelectionMode="Multiple" Rows="11">
                                    <asp:ListItem Value="1">1st</asp:ListItem>
                                    <asp:ListItem Value="2">2nd</asp:ListItem>
                                    <asp:ListItem Value="3">3rd</asp:ListItem>
                                    <asp:ListItem Value="4">4th</asp:ListItem>
                                    <asp:ListItem Value="5">5th</asp:ListItem>
                                    <asp:ListItem Value="6">6th</asp:ListItem>
                                    <asp:ListItem Value="7">7th</asp:ListItem>
                                    <asp:ListItem Value="8">8th</asp:ListItem>
                                    <asp:ListItem Value="9">9th</asp:ListItem>
                                    <asp:ListItem Value="10">10th</asp:ListItem>
                                    <asp:ListItem Value="11">11th</asp:ListItem>
                                    <asp:ListItem Value="12">12th</asp:ListItem>
                                    <asp:ListItem Value="13">13th</asp:ListItem>
                                    <asp:ListItem Value="14">14th</asp:ListItem>
                                    <asp:ListItem Value="15">15th</asp:ListItem>
                                    <asp:ListItem Value="16">16th</asp:ListItem>
                                    <asp:ListItem Value="17">17th</asp:ListItem>
                                    <asp:ListItem Value="18">18th</asp:ListItem>
                                    <asp:ListItem Value="19">19th</asp:ListItem>
                                    <asp:ListItem Value="20">20th</asp:ListItem>
                                    <asp:ListItem Value="21">21st</asp:ListItem>
                                    <asp:ListItem Value="22">22nd</asp:ListItem>
                                    <asp:ListItem Value="23">23rd</asp:ListItem>
                                    <asp:ListItem Value="24">24th</asp:ListItem>
                                    <asp:ListItem Value="25">25th</asp:ListItem>
                                    <asp:ListItem Value="26">26th</asp:ListItem>
                                    <asp:ListItem Value="27">27th</asp:ListItem>
                                    <asp:ListItem Value="28">28th</asp:ListItem>
                                    <asp:ListItem Value="29">29th</asp:ListItem>
                                    <asp:ListItem Value="30">30th</asp:ListItem>
                                    <asp:ListItem Value="31">31st</asp:ListItem>
                                </asp:ListBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                            </td>
                            <td align="center" colspan="4">
                                For multiple selections, click while holding down control
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><td colspan="6"><hr style="color:lightgrey; margin:10px:0px:10px:0px" /></td></tr>
            <tr>
                <td style="width:1%" noWrap>
                    Effective date
                </td>
                <td style="width:1%">
                    <ML:DateTextBox ID="Effective_date_monthly" CausesValidation="true" runat="server" preset="date" onchange="ontextchange_monthly();"></ML:DateTextBox>
                </td>
                <td>
                    <asp:CheckBox ID="m_cbUseEffectiveDateMonthly" Checked="true" runat="server" Text="Use effective date " onclick="updateEffectiveDate(this.checked);ontextchange_monthly();"/>                          
                </td>
                 <td>&nbsp;</td>
                <td style="width:3%">
                    Run Time
                </td>
                <td>
                    <asp:DropDownList ID="Run_time_monthly" runat="server" onchange="RunTimeTextChange(this);" >
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Next run
                </td>
                <td>
                    <asp:TextBox ID="Next_run_monthly" runat="server" Width="74px" />
                </td>
                <td>
                    <span id="monthly_date_validation" style="color: #FF0000; font-variant: normal; text-transform: none"></span>
                </td>
                <td>&nbsp;</td>
                <td style="width:3%">
                    Retry Until&nbsp;<span><a href="javascript:void(0)" onmousedown="showRetryUntilInfo();"><img alt="" src="../images/warn.png" style="border:none;"/></a>
                    <div class="RetryUntilNotif" style="display:none"></div>
                    </span>
                </td>
                <td>
                    <asp:DropDownList ID="Retry_until_monthly" runat="server" onchange="ontextchange_monthly();"></asp:DropDownList>
                </td>
            </tr>
            <tr><td colspan="6"><hr style="color:lightgrey; margin:10px:0px:10px:0px" /></td></tr>
            <tr>
                <td colspan="2">
                    <asp:RadioButton ID="run_infinitely" GroupName="grp2" text="Run indefinitely" onclick="javascript:document.form1.end_date_textbox.disabled=true;ontextchange_monthly();" runat="server" Checked="True" />
                </td>
                <td>
                    <span id="EndDateMonthlyCheck" style="color: #FF0000; font-variant: normal; text-transform: none"></span>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RadioButton ID="end_date_monthly" onclick="javascript:document.form1.end_date_textbox.disabled=false;ontextchange_monthly();" runat="server" text="End date" GroupName="grp2" />
                </td>
                <td>
                    <ML:DateTextBox ID="end_date_textbox" CausesValidation="true" runat="server" preset="date" onchange="ontextchange_monthly();"></ML:DateTextBox>
                </td>
                <td>
                    <span id="MonthlyEnddateValidation" style="color: #FF0000; font-variant: normal; text-transform: none"></span>
                </td>
            </tr>
        </table></td></tr></table>
        <hr />
        <div style="padding: 8px; TEXT-ALIGN: right; width: 580px;">
			<input type="button" value="&lt; Back" style="WIDTH: 60px" onclick="showPanel( 'Step2' );">
			<input type="button" runat="server" id="monthly_next" value="Next &gt;" style="WIDTH: 60px" onclick="showPanel( 'Step4' );" disabled="disabled">
            <input id="Button_Finish_Monthly" type="button" value="Finish" style="WIDTH: 60px" disabled="disabled" onclick="saveSchedule();">
            <input type="button" value="Cancel" style="WIDTH: 60px" onclick="window.location ='ScheduledCustomReports.aspx'">
		</div>
	</div>
						
	<div id="OneTime" runat="server">
	    <h5>Step 3: Report Schedule (Note : All reports are run after business hours Pacific Time)</h5>
	    <table height="470px" width="90%" style="margin:10px"><tr><td valign="top"><table width="100%">
            <tr>
                <td class="FieldLabel">
                    One Time Report
                </td>
            </tr>
            <tr><td><hr style="color:lightgrey" /></td></tr>
            <tr>
                <td noWrap>
                    Run date
                    <ML:DateTextBox ID="onetime_text" preset="date"  onchange="validate_one_time();" runat="server"/>
                    <span id ="validate_effdt" style="color: #FF0000; text-transform: none"></span>
                </td>
            </tr>
        </table></td></tr></table>
        <hr />
        <div style="padding: 8px; TEXT-ALIGN: right; width: 580px;" dir="ltr">
			<input type="button" value="&lt; Back" style="WIDTH: 60px" onclick="showPanel( 'Step2' );">
            <input type="button" value="Next &gt;" id="one_time_next" style="WIDTH: 60px" onclick="validate_one_time();showPanel( 'Step4' );">
            <input id="Button_Finish_Once" type="button" style="WIDTH: 60px" disabled="disabled" value="Finish" onclick="saveSchedule();" >
            <input type="button" value="Cancel" style="WIDTH: 60px" onclick="window.location ='ScheduledCustomReports.aspx'">
		</div>
	</div>

	<div id="Step4" runat="server">
        <h5>Step 4: E-mail Recipients</h5>
        <table height="470px" style="margin:10px"><tr><td valign="top">
        
	    <br /><br />
        <table id="EmailAddresses">
            <tr>
                <td>Your e-mail address</td>
                <td><asp:TextBox ID="sender_name" runat="server" Width="209px" onchange="validate_mail_list();" ReadOnly="False" MaxLength="200" /></td>
            </tr>
		    <tr valign="top" align="right">
                <td>CC *</td>
                <td><asp:TextBox runat="server" ID="CC" onchange="validate_mail_list();" Height="91px" Width="436px" MaxLength="400" TextMode="MultiLine"/></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><span>* Enter e-mail addresses separated by comma (' , ') or semi-colon (' ; ') </span></td>
            </tr>            
            <tr>
                <td colspan="2"><span id="Step4Error" style="color: #FF0000; text-transform: none;"></span></td>
            </tr>
        </table>
		
		</td></tr></table>
		<hr />
        <div style="padding: 8px; TEXT-ALIGN: right; width: 580px;">
			<input type="button" value="&lt; Back" style="WIDTH: 60px" onclick="showPanel( 'FourBack' );">
            <input id="step4_next" onclick="fournext();" style="WIDTH: 60px" type="button" value="Next &gt;" />
            <input id="Button_Finish_Email" type="button" value="Finish" style="WIDTH: 60px" disabled="DISABLED" onclick="saveSchedule();">
            <input type="button" value="Cancel" style="WIDTH: 60px" onclick="window.location ='ScheduledCustomReports.aspx'">
		</div>
	</div>

	<div id="Step5" runat="server">
        <h5>Step 5: Confirm</h5>
        <table height="470px" style="margin:10px"><tr><td valign="top">
        
        <br /><br />
	    <table style="border-style: solid; border-collapse: collapse; height: 245px; width: 500px; background-color: #DDDDDD" 
            runat="server" frame="border" id="ScheduleSummary">
            <tr>
                <td width="183px">
                    Schedule Name
                </td>
                <td>
                    <span id="scheduleNameConfirm"></span>
                </td>
            </tr>
            <tr>
                <td>
                    Custom Report Query
                </td>
                <td>
                    <span id="crQueryConfirm"></span>
                </td>
            </tr>
            <tr>
                <td>
                    Schedule
                </td>
                <td >
                    <span id="scheduleConfirm"></span>
                </td>
            </tr>
            <tr>
                <td>
                    Next Run
                </td>
                <td>
                    <span id="nextRunConfirm"></span>
                </td>
            </tr>
            <tr>
                <td>
                    Run Time
                </td>
                <td>
                    <span id="runTimeConfirm"></span>
                </td>
            </tr>
            <tr>
                <td>
                    Retry Until
                </td>
                <td>
                    <span id="retryUntilConfirm"></span>
                </td>
            </tr>
            <tr>
                <td>
                     End Date
                </td>
                <td>
                   <span id="endDateConfirm"></span>
                </td>
            </tr>
            <tr>
                <td>
                     Recipient
                </td>
                <td>
                   <span id="recipientConfirm"></span>
                </td>
            </tr>
            <tr>
                <td>
                     CC
                </td>
                <td>
                   <span id="CCConfirm"></span>
                </td>
            </tr>
        </table>
		
		</td></tr></table>
		<hr />
        <div style="padding: 8px; TEXT-ALIGN: right; width: 580px;">
		    <input id="Button1" type="button" runat="server" value="&lt; Back" style="WIDTH: 60px" onclick="showPanel( 'Step4' );" />
			<input id="Button2" style="WIDTH: 60px" type="button" value="Next &gt;" disabled="true" />
            <input id="Finish" type="button" runat="server" value="Finish" style="WIDTH: 60px" onclick="saveSchedule();" />
			<input type="button" value="Cancel" style="WIDTH: 60px" onclick="window.location ='ScheduledCustomReports.aspx'" />
		</div>
	</div>
	
	</td></tr></table>
    <uc:CModalDlg ID="CModalDlg1" runat="server"></uc:CModalDlg> 	
   		
    </form>

</body>
</html>
