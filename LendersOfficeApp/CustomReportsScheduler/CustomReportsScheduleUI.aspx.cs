﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Web.SessionState;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOfficeApp.los.admin;
using MeridianLink.CommonControls;
using LendersOfficeApp.los.common;
using LendersOffice.Reports;
using LendersOffice.QueryProcessor;
using System.IO;
using System.Text;

namespace LendersOfficeApp.CustomReportsScheduler
{
    public partial class CustomReportsScheduleUI : LendersOffice.Common.BaseServicePage
    {
        //<This is the UI of the scheduled custom reports>
        private LendersOffice.Reports.LoanReporting m_Loans;
        protected int selectedTabIndex;

        protected void Page_Load(object sender, EventArgs e)
        {
            EnableJqueryMigrate = false;
            InitializeRunTimeRetryUntil();
            IncludeStyleSheet(VirtualRoot + "/css/Tabs.css");

            //  CompareValidator_onetime.ValueToCompare = DateTime.Now.ToShortDateString();
            Step2.Attributes.Add("style", "display:none");
            Daily.Attributes.Add("style", "display:none");
            Weekly.Attributes.Add("style", "display:none");
            Monthly.Attributes.Add("style", "display:none");
            OneTime.Attributes.Add("style", "display:none");
            Step4.Attributes.Add("style", "display:none");
            Step5.Attributes.Add("style", "display:none");

            string ReportId = Request.QueryString["EditReport"];
            if (!String.IsNullOrEmpty(ReportId))
            {
                List<SqlParameter> pn = new List<SqlParameter>();
                DataSet dn = new DataSet();

                BrokerUserPrincipal user = BrokerUserPrincipal.CurrentPrincipal;
                pn.Add(new SqlParameter("@ReportId", Convert.ToInt32(ReportId)));
                pn.Add(new SqlParameter("@BrokerId", user.BrokerId));

                DataSetHelper.Fill(dn, user.BrokerId, "GetScheduledReportDetails", pn);

                if (dn.Tables.Count < 1 || dn.Tables[0].Rows.Count < 1)
                    throw new CBaseException(ErrorMessages.Generic, "Unable to find a scheduled report. ReportId = " + ReportId + ", UserId = " + user.UserId);

                DataRow row = dn.Tables[0].Rows[0];
                string OwnerId = row["UserId"].ToString();

                if (m_UserId != new Guid(OwnerId) && !user.IsInRole(ConstApp.ROLE_ADMINISTRATOR) && !user.IsInRole(ConstApp.ROLE_MANAGER))
                    throw new CBaseException(ErrorMessages.GenericAccessDenied, String.Format("Attempt to edit scheduled report not owned by user. UserId = {0}. OwnerId = {1}", m_UserId, OwnerId));

                string EffectiveDate;
                string ReportType = row["ReportType"].ToString();
                Schedule_name.Text = row["ScheduleName"].ToString();
                BindGrid(row["QueryId"].ToString());

                pn.Clear();
                pn.Add(new SqlParameter("@ReportId", Convert.ToInt32(ReportId)));

                if (ReportType != "Never")
                {
                    pn.Add(new SqlParameter("@ReportType", ReportType));
                    using (DbDataReader report = StoredProcedureHelper.ExecuteReader(user.BrokerId, "GetScheduledReportData", pn))
                    {
                        if (report.Read())
                        {
                            int Frequency = 0;
                            if (ReportType != "Monthly")
                                Frequency = Convert.ToInt32(report["Frequency"]);

                            EffectiveDate = (Convert.ToDateTime(report["EffectiveDate"])).ToString("d");
                            string EndDate = (Convert.ToDateTime(report["EndDate"])).ToString("d");
                            string NextRun = (Convert.ToDateTime(report["NextRun"])).ToString("d");
                            string RunTime = GetRunTimeString(report);
                            string RetryUntil = GetRetryUntilString(report);

                            sender_name.Text = report["Email"].ToString();
                            CC.Text = report["CcIds"].ToString();
                            Frequencylist.SelectedValue = ReportType;

                            End_date_radiobutton.Checked = false;
                            Run_indefinitely_daily.Checked = false;
                            Run_indefinitely_weekly.Checked = false;
                            End_date_weekly.Checked = false;
                            run_infinitely.Checked = false;
                            end_date_monthly.Checked = false;

                            if (DateTime.MaxValue.ToString("d") == EndDate)
                            {
                                Run_indefinitely_daily.Checked = true;
                                Run_indefinitely_weekly.Checked = true;
                                run_infinitely.Checked = true;
                            }
                            else
                            {
                                End_date_radiobutton.Checked = true;
                                End_date_daily.Text = EndDate;

                                End_date_weekly.Checked = true;
                                End_date.Text = EndDate;

                                end_date_monthly.Checked = true;
                                end_date_textbox.Text = EndDate;
                            }

                            string LastRun = report["LastRun"].ToString();
                            if (!String.IsNullOrEmpty(LastRun))
                            {
                                m_CalculateFrom.Value = Convert.ToDateTime(LastRun).ToString("d");
                                m_LastRun.Value = m_CalculateFrom.Value;
                            }
                            else
                                m_CalculateFrom.Value = EffectiveDate;

                            m_EffectiveDate.Value = EffectiveDate;
                            switch (ReportType)
                            {
                                case "Daily":
                                    if (Frequency > 6) // One time report
                                    {
                                        Frequencylist.SelectedValue = "OneTime";
                                    }
                                    else // Daily report
                                    {
                                        Retry_until_daily.Items.Clear();
                                        Retry_until_daily.Items.AddRange(RetryUntilList(RunTime));
                                        Run_time_daily.SelectedValue = RunTime;
                                        Retry_until_daily.SelectedValue = RetryUntil;

                                        Next_run_daily.Text = NextRun;
                                        Everyday.Checked = false;
                                        Every__days.Checked = false;
                                        if (Frequency == 1)
                                            Everyday.Checked = true;
                                        else
                                        {
                                            Every__days.Checked = true;
                                            Frequency_daily.SelectedValue = Frequency.ToString();
                                        }

                                        CheckBox1.Checked = Convert.ToBoolean(report["IncludeSatSun"]);

                                    }
                                    break;

                                case "Weekly":
                                    Retry_until_weekly.Items.Clear();
                                    Retry_until_weekly.Items.AddRange(RetryUntilList(RunTime));
                                    Run_time_weekly.SelectedValue = RunTime;
                                    Retry_until_weekly.SelectedValue = RetryUntil;

                                    Next_run.Text = NextRun;
                                    Weekly_weekly.Checked = false;
                                    Biweekly.Checked = false;
                                    Every_x_weeks.Checked = false;

                                    if (Frequency == 1)
                                        Weekly_weekly.Checked = true;
                                    else if (Frequency == 2)
                                        Biweekly.Checked = true;
                                    else
                                    {
                                        Every_x_weeks.Checked = true;
                                        Frequency_Monthly.SelectedValue = Frequency.ToString();
                                    }

                                    do
                                    {
                                        string day = report["Weekday"].ToString();
                                        foreach (ListItem i in ListBox_weekdays.Items)
                                        {
                                            if (i.Value == day)
                                                i.Selected = true;
                                        }

                                    } while (report.Read());
                                    break;

                                case "Monthly":
                                    Retry_until_monthly.Items.Clear();
                                    Retry_until_monthly.Items.AddRange(RetryUntilList(RunTime));
                                    Run_time_monthly.SelectedValue = RunTime;
                                    Retry_until_monthly.SelectedValue = RetryUntil;

                                    Next_run_monthly.Text = NextRun;
                                    Week_no_week_day.Checked = false;
                                    Days_of_Month_monthly.Checked = false;
                                    string Weekday = report["Weekday"].ToString();
                                    string Month = report["Month"].ToString();
                                    List<string> list = new List<string>();
                                    list.Add(Month);
                                    bool DataInReport = true;

                                    if (String.IsNullOrEmpty(Weekday))
                                    {
                                        Days_of_Month_monthly.Checked = true;
                                        while (DataInReport && Month == report["Month"].ToString())
                                        {
                                            Days_of_the_month.Items[Convert.ToInt32(report["DateOfTheMonthOrWeekNo"]) - 1].Selected = true;
                                            if (!report.Read())
                                                DataInReport = false;
                                        }
                                        Days_of_the_month.Enabled = true;
                                        Day_of_the_week.Enabled = false;
                                        week_no.Enabled = false;
                                    }
                                    else
                                    {
                                        Week_no_week_day.Checked = true;
                                        week_no.SelectedValue = report["DateOfTheMonthOrWeekNo"].ToString();
                                        Day_of_the_week.SelectedValue = Weekday;
                                    }

                                    if (DataInReport)
                                    {
                                        do
                                        {
                                            string NextMonth = report["Month"].ToString();
                                            if (NextMonth != Month)
                                            {
                                                list.Add(NextMonth);
                                                NextMonth = Month;
                                            }
                                        } while (report.Read());
                                    }

                                    foreach (ListItem i in ListBox_months.Items)
                                    {
                                        if (list.Contains(i.Value))
                                            i.Selected = true;
                                    }
                                    break;

                                default:
                                    throw new CBaseException(ErrorMessages.Generic, "Unknown report type : " + ReportType);
                            }
                        }
                        else
                            throw new CBaseException(ErrorMessages.Generic, "GetScheduledReportData did not return any data.");
                    }
                }
                else
                {
                    Frequencylist.SelectedValue = ReportType;
                    EffectiveDate = DateTime.Today.ToString("d");
                    m_CalculateFrom.Value = EffectiveDate;
                    m_EffectiveDate.Value = EffectiveDate;
                }

                onetime_text.Text = EffectiveDate;
                Effective_date_daily.Text = EffectiveDate;
                Effective_Date.Text = EffectiveDate;
                Effective_date_monthly.Text = EffectiveDate;
            }
            else
            {
                String today = DateTime.Now.ToString("d");
                Effective_date_daily.Text = today;
                Effective_Date.Text = today;
                Effective_date_monthly.Text = today;
                onetime_text.Text = today;
                Next_run_daily.Text = today;
                Next_run.Text = today;
                Next_run_monthly.Text = today;
                //m_cbUseEffectiveDateDaily.Visible = false;
                //m_cbUseEffectiveDateMonthly.Visible = false;
                //m_cbUseEffectiveDateWeekly.Visible = false;
                m_cbUseEffectiveDateDaily.Attributes["style"] = "visibility: hidden;";
                m_cbUseEffectiveDateMonthly.Attributes["style"] = "visibility: hidden;";
                m_cbUseEffectiveDateWeekly.Attributes["style"] = "visibility: hidden;";
                BindGrid("");
            }

        }

        protected void PageInit(object sender, System.EventArgs a)
        {

            ClientScript.RegisterHiddenField("tabNames", "");

            try
            {
                m_Loans = new LoanReporting();
                this.RegisterService("main", "/CustomReportsScheduler/CustomReportsService.aspx");

                BrokerUserPrincipal user = BrokerUserPrincipal.CurrentPrincipal;
                bool isAdmin = user.IsInRole(ConstApp.ROLE_ADMINISTRATOR);
                bool canRunCustomReports = user.HasPermission(Permission.CanRunCustomReports);
                if (!(isAdmin || canRunCustomReports))
                    Response.Redirect("~/los/main.aspx?showAlert=t");
            }

            catch (Exception e)
            {
                // Oops!

                m_ExceptionDetails = "Error initializing web form.";

                Tools.LogError("Loan reporting: Exception caught during page init.");
                Tools.LogError(e);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion



        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        private Guid m_BrokerId
        {
            get
            {
                BrokerUserPrincipal broker = BrokerUserPrincipal.CurrentPrincipal;

                if (broker == null)
                {
                    return Guid.Empty;
                }

                return broker.BrokerId;
            }
        }

        private Guid m_BranchId
        {
            get
            {
                BrokerUserPrincipal broker = BrokerUserPrincipal.CurrentPrincipal;

                if (broker == null)
                {
                    return Guid.Empty;
                }

                return broker.BranchId;
            }
        }

        private Guid m_BAdminId
        {
            get
            {
                return BrokerDB.AdminBr;
            }
        }

        private String m_EmployeeNm
        {
            get
            {
                // Lookup current principal.

                BrokerUserPrincipal broker = BrokerUserPrincipal.CurrentPrincipal;

                if (broker == null)
                {
                    return "";
                }

                return broker.DisplayName;
            }
        }

        private Guid m_EmployeeId
        {
            get
            {
                // Lookup current principal.

                BrokerUserPrincipal broker = BrokerUserPrincipal.CurrentPrincipal;

                if (broker == null)
                {
                    return Guid.Empty;
                }

                return broker.EmployeeId;

            }
        }

        private String m_ExceptionDetails
        {
            set
            {
                if (value.EndsWith(".") == false)
                {
                    Page.ClientScript.RegisterHiddenField("m_ErrorMessage", value + ".");
                }
                else
                {
                    Page.ClientScript.RegisterHiddenField("m_ErrorMessage", value);
                }
            }
        }

        private Guid m_UserId
        {
            get
            {
                // Lookup current principal.
                BrokerUserPrincipal broker = BrokerUserPrincipal.CurrentPrincipal;
                if (broker == null)
                {
                    return Guid.Empty;
                }
                return broker.UserId;

            }
        }

        private String m_FeedbackMessage
        {
            // Access member.

            set
            {
                if (value.EndsWith(".") == false)
                {
                    Page.ClientScript.RegisterHiddenField("m_Feedback", value + ".");
                }
                else
                {
                    Page.ClientScript.RegisterHiddenField("m_Feedback", value);
                }
            }
        }

        protected void BindGrid(string ReportId)
        {
            // Bind ui to latest persistent state.

            try
            {
                // Get the loans for this broker.  At some point, we need to
                // use a view specification using the user's credentials and
                // a security filter to get per-report access privelages.
                SqlParameter[] parameters = null;

                DataSet dS = new DataSet();
                parameters = new SqlParameter[] {
                                                    new SqlParameter("@EmployeeId", m_EmployeeId)
                                                };

                DataSetHelper.Fill(dS, m_BrokerId, "UserCustomReports", parameters);

                DataSet dS2 = new DataSet();
                parameters = new SqlParameter[] {
                                                    new SqlParameter("@BrokerId", m_BrokerId), 
                                                    new SqlParameter("@IsPublished", 1)
                                                };

                DataSetHelper.Fill(dS2, m_BrokerId, "ListReportQuery", parameters);

                DataSet dS3 = new DataSet();
                parameters = new SqlParameter[] {
                                                    new SqlParameter("@ExcludeEmployeeId", m_EmployeeId)
                                                    , new SqlParameter("@BrokerID", m_BrokerId)
                                                };
                DataSetHelper.Fill(dS3, m_BrokerId, "ListReportQuery", parameters);

                DataSet dS4 = new DataSet();
                parameters = new SqlParameter[] {
                                                    new SqlParameter("@EmployeeId", m_EmployeeId)
                                                };
                DataSetHelper.Fill(dS4, m_BrokerId, "GetFavoriteReportsForEmployee", parameters);

                // Extend the data table by adding columns for last saved by
                // info and query comments.  These values are stored within
                // the text of the query, so we must crack each open and
                // pull out the content.
                DataTable dt = dS.Tables[0];
                DataTable dt2 = dS2.Tables[0];
                DataTable dt3 = dS3.Tables[0];
                DataTable dt4 = dS4.Tables[0];

                // Apply the data view.
                FavReports.DataSource = dt4.DefaultView;
                FavReports.DataBind();
                SetPermissionFields(FavReports);

                //YourReports is the name of the listbox which is populated with the name of custom reports
                YourReports.DataSource = dt.DefaultView;
                YourReports.DataBind();
                SetPermissionFields(YourReports);

                PublishedReports.DataSource = dt2.DefaultView;
                PublishedReports.DataBind();
                SetPermissionFields(PublishedReports);

                OthersReports.DataSource = dt3.DefaultView;
                OthersReports.DataBind();
                SetPermissionFields(OthersReports);

                if (dS.Tables.Count > 0 && dS.Tables[0].Rows.Count > 0)
                {
                    sender_name.Text = dS.Tables[0].Rows[0]["Email"].ToString();
                }
                else
                {
                    parameters = new SqlParameter[] {
                                                    new SqlParameter("@EmployeeId", m_EmployeeId),
                                                    new SqlParameter("@BrokerID", m_BrokerId)
                                                };

                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_BrokerId, "RetrieveEmployeeByID", parameters))
                    {
                        if (reader.Read())
                            sender_name.Text = reader["Email"].ToString();
                    }
                }

                if (!String.IsNullOrEmpty(ReportId))
                {
                    int pos = FindReportInGrid(dt, ReportId, YourReports);
                    if (pos == -1)
                    {
                        pos = FindReportInGrid(dt2, ReportId, PublishedReports);
                        if (pos == -1)
                            pos = FindReportInGrid(dt3, ReportId, OthersReports);
                    }
                }

            }
            catch (Exception e)
            {
                // Oops!
                m_ExceptionDetails = "Error binding list to web form.";

                Tools.LogError("Loan reporting: Exception caught during grid binding.", e);
            }
        }

        private void SetPermissionFields(ListBox lb)
        {
            foreach (ListItem li in lb.Items)
            {
                Guid reportId = new Guid(li.Value);
                Query query = GetQueryInfo(reportId);
                if (query != null && query.hasSecureFields && !query.CanUserRun(BrokerUser))
                {
                    li.Text += " **";
                    li.Attributes.Add("restricted", "true");
                    li.Attributes.CssStyle.Add(HtmlTextWriterStyle.Color, "#BBB");
                    li.Attributes.CssStyle.Add(HtmlTextWriterStyle.BackgroundColor, "#FFF");
                }
            }
        }

        private Query GetQueryInfo(Guid QueryId)
        {
            Query retQuery = null;
            try
            {
                SqlParameter[] parameters = {
                        new SqlParameter("@QueryID", QueryId),
                        new SqlParameter("@BrokerID", BrokerUser.BrokerId)
                };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerUser.BrokerId, "GetReportQuery", parameters))
                {
                    if (reader.Read())
                    {
                        retQuery = reader["XmlContent"].ToString();
                    }
                }

            }
            catch { }

            return retQuery;
        }

        protected int FindReportInGrid(DataTable data, string reportId, ListBox grid)
        {
            int position = 0;
            foreach (DataRow d in data.Rows)
            {
                if (d["QueryId"].ToString() == reportId)
                {
                    grid.SelectedIndex = position;

                    // m_ReportsTab.Items includes seperators so every other item is a tab
                    switch (grid.ID + "Tab")
                    {
                        case "FavReportsTab":
                            selectedTabIndex = 0;
                            tab0.Attributes.Add("class", "selected");
                            break;
                        case "YourReportsTab":
                            selectedTabIndex = 1;
                            tab1.Attributes.Add("class", "selected");
                            break;
                        case "OthersReportsTab":
                            selectedTabIndex = 2;
                            tab2.Attributes.Add("class", "selected");
                            break;
                        case "PublishedReportsTab":
                            selectedTabIndex = 3;
                            tab3.Attributes.Add("class", "selected");
                            break;

                    }

                    return position;
                }
                position++;
            }
            return -1;
        }
        
        //Long Nguyen
        //https://lqbopm/default.asp?215368
        private void InitializeRunTimeRetryUntil() {
            Run_time_daily.Items.Clear();
            Run_time_daily.Items.AddRange(runList);
            Run_time_daily.Text = DefaultRunTime;
            Retry_until_daily.Items.Clear();
            Retry_until_daily.Items.AddRange(RetryUntilList(DefaultRunTime));
            Retry_until_daily.Text = DefaultRetryUntil;

            Run_time_weekly.Items.Clear();
            Run_time_weekly.Items.AddRange(runList);
            Run_time_weekly.Text = DefaultRunTime;
            Retry_until_weekly.Items.Clear();
            Retry_until_weekly.Items.AddRange(RetryUntilList(DefaultRunTime));
            Retry_until_weekly.Text = DefaultRetryUntil;

            Run_time_monthly.Items.Clear();
            Run_time_monthly.Items.AddRange(runList);
            Run_time_monthly.Text = DefaultRunTime;
            Retry_until_monthly.Items.Clear();
            Retry_until_monthly.Items.AddRange(RetryUntilList(DefaultRunTime));
            Retry_until_monthly.Text = DefaultRetryUntil;
        }
        public static ListItem[] runList = new ListItem[]{
                new ListItem("5:00 PM","5:00 PM"),
                new ListItem("6:00 PM","6:00 PM"),
                new ListItem("7:00 PM","7:00 PM"),
                new ListItem("8:00 PM","8:00 PM"),
                new ListItem("9:00 PM","9:00 PM"),
                new ListItem("10:00 PM","10:00 PM"),
                new ListItem("11:00 PM","11:00 PM"),
                new ListItem("12:00 AM","12:00 AM"),
                new ListItem("1:00 AM","1:00 AM"),
                new ListItem("2:00 AM","2:00 AM"),
                new ListItem("3:00 AM","3:00 AM")
            };
        public static ListItem[] RetryUntilList(string runTimeStr) {
            DateTime result;
            if (!DateTime.TryParse(runTimeStr, out result))
            {
                // Handle
                //11:00 PM
                result = new DateTime(0, 0, 0, 23, 0, 0);
            }
            var minTS = result.Add(new TimeSpan(2, 0, 0));
            var maxTS = DateTime.Parse("5:00:00 AM");
            if (minTS > maxTS)
            {
                //Next day
                maxTS = maxTS.AddDays(1);
            }

            var tempTS = minTS;
            var retryList = new List<ListItem>();
            while (tempTS <= maxTS)
            {
                //DateTime time = DateTime.Today.Add(tempTS);
                var str = tempTS.ToString("t");
                retryList.Add(new ListItem(str, str));
                tempTS = tempTS.Add(new TimeSpan(1, 0, 0));
            }
            return retryList.ToArray();
        }
        public static string DefaultRunTime = "11:00 PM";
        public static string DefaultRetryUntil = "1:00 AM";
        private string GetRunTimeString(DbDataReader reader)
        {
            if (reader["RunTime"] == DBNull.Value)
                return DefaultRunTime;
            DateTime dt;
            if (DateTime.TryParse(reader["RunTime"].ToString(), out dt))
            {
                // It was assigned.
                //http://stackoverflow.com/questions/13044603/convert-time-span-value-to-format-hhmm-am-pm-using-c-sharp
                //http://www.dotnetperls.com/datetime-format
                DateTime time = DateTime.Today.Add(dt.TimeOfDay);
                return time.ToString("t");
            }
            return DefaultRunTime;
        }
        private string GetRetryUntilString(DbDataReader reader)
        {
            if (reader["RetryUntil"] == DBNull.Value)
                return DefaultRetryUntil;
            DateTime dt;
            if (DateTime.TryParse(reader["RetryUntil"].ToString(), out dt))
            {
                // It was assigned.
                //http://stackoverflow.com/questions/13044603/convert-time-span-value-to-format-hhmm-am-pm-using-c-sharp
                DateTime time = DateTime.Today.Add(dt.TimeOfDay);
                return time.ToString("t");
            }
            return DefaultRetryUntil;
        }
    }
}