﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using System.Globalization;

namespace LendersOfficeApp.CustomReportsScheduler
{
    public partial class CustomReportsService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "calculate_daily_run":
                    calculate_daily_run();
                    break;
                case "calculate_weekly_run":
                    calculate_weekly_run();
                    break;
                case "calculate_monthly_run":
                    calculate_monthly_run();
                    break;
                case "validate_mail_list":
                    validate_mail_list();
                    break;
                case "InsertIntoDatabase":
                    InsertIntoDatabase();
                    break;
                case "insertIntoScheduledReportUserInfo":
                    InsertIntoScheduledReportUserInfo();
                    break;
            }
        }

        #region Calculations
        private void calculate_daily_run()
        {
            //This function is called to calculate the next run for a daily report

            int freq;
            System.DateTime today;
            System.DateTime next_run;
            string effective_date, lst_run;
            System.DateTime end_date, last_run;
            //In other cases last_run will come from the database.
            //Last run is null since a new report is being created. 
            //Since the datetime object does not accept a null value 1/1/1000 is considered unll
            lst_run = "01/01/1000";
            last_run = Convert.ToDateTime(lst_run);
            // This loops sets the frequency    
            string frequency;
            frequency = GetString("frequency");
            freq = Convert.ToInt32(frequency);

            bool UseEffectiveDate = Convert.ToBoolean(GetString("useEffectiveDate"));
            if (UseEffectiveDate)
                effective_date = GetString("effective_date");
            else
                effective_date = GetString("CalculateFrom");

            System.DateTime effective_date_dt_tm_format = Convert.ToDateTime(effective_date).Date;
            today = System.DateTime.Now.Date;

            if (effective_date_dt_tm_format > today.Date)
                next_run = effective_date_dt_tm_format.Date;
            else
                next_run = today.Date;
            string include_weekend = GetString("include_weekend");
            if (include_weekend == "false")
            {
                if(today.DayOfWeek.ToString() == "Saturday")
                    next_run = next_run.AddDays(2);
                if(today.DayOfWeek.ToString() == "Sunday")
                    next_run = next_run.AddDays(1);
            }
            string enddate = GetString("enddate");
            end_date = Convert.ToDateTime(enddate).Date;

            // cHECK IF THE NEXT RUN IS BEYOND THE END DATE
            if (next_run > end_date)
            {
                //Next_run_daily.Text = "The next run is greater than the end date ";
                SetResult("nextrun", "The next run is greater than the end date, this report will never run.");
            }
            else
            {
                SetResult("nextrun", Convert.ToString(next_run.ToShortDateString()));
            }
        }

        private void calculate_weekly_run()
        {

            System.DateTime today, next_run, effective_date, end_date, last_run;
            String effective_date_from_UI, end_date_from_UI;
            int freq;


            //The next four lines retrieve values from the textboxes and drop box in the UI.
            freq = Convert.ToInt32(GetString("frequency"));

            bool UseEffectiveDate = Convert.ToBoolean(GetString("useEffectiveDate"));
            if (UseEffectiveDate)
                effective_date_from_UI = GetString("effective_date");
            else
                effective_date_from_UI = GetString("CalculateFrom");
            //today_from_UI = today_Textbox.Text;
            end_date_from_UI = GetString("enddate");
            // Assign the retrieved values todatetime variables.The null value of last_run is 1st January 1000.
            last_run = Convert.ToDateTime("01/01/1000");
            today = System.DateTime.Now.Date;
            // date is taken as current date if the textbox is empty.    
            if (effective_date_from_UI == "")
                effective_date = System.DateTime.Now;
            else effective_date = Convert.ToDateTime(effective_date_from_UI);
            end_date = Convert.ToDateTime(end_date_from_UI).Date;
            
            
            /// If this is the first time that the report will run,
            /// We start checking from the effective date , else we start checking from one day past last run.
            if (last_run == Convert.ToDateTime("01/01/1000"))
                next_run = effective_date.Date;
            else
                next_run = last_run.Date.AddDays(1);

            while (next_run <= end_date)
            {
                if ((last_run == (Convert.ToDateTime("01/01/1000")).Date) && (Convert.ToString(effective_date.DayOfWeek) == "Sunday"))
                { }
                else
                {
                    if ((Convert.ToString(next_run.DayOfWeek) == "Sunday"))
                        next_run = next_run.AddDays((7 * (freq - 1)));
                }

                string checkd;
                checkd = "false";
                string day = Convert.ToString(next_run.DayOfWeek);

                // Through this switch checkd is set to 
                //true if the chechbox for that particular day is checked.
                switch (day)
                {
                    case "Monday":
                        string mon = GetString("Monday");
                        if (mon == "true")
                            checkd = "true";
                        break;
                    case "Tuesday":
                        string tue = GetString("Tuesday");
                        if (tue == "true")
                            checkd = "true";
                        break;

                    case "Wednesday":
                        string wed = GetString("Wednesday");
                        if (wed == "true")
                            checkd = "true";
                        break;
                    case "Thursday":
                        string thu = GetString("Thursday");
                        if (thu == "true")
                            checkd = "true";
                        break;
                    case "Friday":
                        string fri = GetString("Friday");
                        if (fri == "true")
                            checkd = "true";
                        break;
                    case "Saturday":
                        string sat = GetString("Saturday");
                        if (sat == "true")
                            checkd = "true";
                        break;
                    case "Sunday":
                        string sun = GetString("Sunday");
                        if (sun == "true")
                            checkd = "true";
                        break;
                }

                if ((checkd == "true") && (next_run >= today) && (next_run >= effective_date))
                {

                    SetResult("nextrun", Convert.ToString(next_run.ToShortDateString()));
                    return;
                }

                else
                {
                    next_run = next_run.Date.AddDays(1);
                }
            }

            SetResult("nextrun", "The next run is greater than the end date, this report will never run.");
        }
        //##############################################################################################
        //###################################################################

        private void calculate_monthly_run()
        {

            string option = GetString("option_monthly");
           
            string last_rn = ("01/01/1000");
            DateTime today = System.DateTime.Now;
            string effctve_dt = GetString("effective_date");
            string end_date = GetString("end_date");
            DateTime effective_date, last_run_monthy;
            Regex reg = new Regex(",");
            string months_from_UI = GetString("months_of_year");
            string[] months_string = reg.Split(months_from_UI);
            int[] months_of_year = new int[12];

            for (int j = 0; j < 12; j++)
            {
                months_of_year[j] = Convert.ToInt32(months_string[j]);
            }
            bool UseEffectiveDate = Convert.ToBoolean(GetString("useEffectiveDate"));
            if (UseEffectiveDate)
                effctve_dt = GetString("effective_date");
            else
                effctve_dt = GetString("CalculateFrom");
            
            if (effctve_dt == "")
                effective_date = System.DateTime.Now.Date;
            else
                effective_date = Convert.ToDateTime(effctve_dt).Date;

            last_run_monthy = Convert.ToDateTime(last_rn);
            DateTime start_looking;
            //Populate the array that contains the days of the month which have been enabled.
            if (option == "option2")
            {
                string textstring = GetString("Days_of_month");
                Regex reg1 = new Regex(",");
                string[] days = reg1.Split(textstring);
                int[] days_of_month = new int[32];
                for (int i = 0; i < 31; i++)
                {
                    days_of_month[i + 1] = Convert.ToInt32(days[i]);
                }
                //It checks if today is the last day of the month or not , 
                //if it is  it starts checking for dates from the next checked month else from the 
                //next day
                if (last_rn == "01/01/1000")
                {
                    start_looking = effective_date;
                }
                else
                {
                    int getlast = find_last_date_of_the_month(last_run_monthy).Day;
                    if (getlast == last_run_monthy.Day)
                        start_looking = first_day_of_next_checked_month(last_run_monthy, months_of_year);
                    else
                        start_looking = last_run_monthy.AddDays(1);
                }
                // Special case when its running once an year and the effective date is same as next run date
                int todaysmonth = today.Month;
                todaysmonth--;
                if ((last_rn == "01/01/1000") && (effective_date.Date == today.Date) && (days_of_month[today.Day] == 1) && (months_of_year[todaysmonth] == 1))
                {
                    DateTime next_run = today;
                    SetResult("nextrun", Convert.ToString(next_run.ToShortDateString()));
                    return;
                }
                while (start_looking < Convert.ToDateTime(end_date))
                {
                    int MONTH = start_looking.Month;
                    MONTH = MONTH - 1;
                    if ((days_of_month[start_looking.Day] == 1) && (start_looking > today) && months_of_year[MONTH] == 1 && (start_looking < Convert.ToDateTime(end_date)))
                    {
                        SetResult("nextrun", Convert.ToString(start_looking.ToShortDateString()));
                        return;
                    }
                    else
                    {
                        int last_date = find_last_date_of_the_month(start_looking).Day;
                        if (last_date == start_looking.Day)
                        {
                            start_looking = first_day_of_next_checked_month(start_looking, months_of_year);
                        }
                        else
                        {
                            start_looking = start_looking.AddDays(1).Date;
                        }
                    }
                    if (start_looking > ((today.AddYears(2)).AddDays(1)))
                    {
                        SetResult("nextrun", "Next run cannot be an invalid date or more than two years from today.");
                        return;
                    }
                }
                SetResult("nextrun", "The next run is greater than the end date, this report will never run.");
                return;
            }
            else
            {
                DateTime start_checking;

                DateTime last_run;

                last_run = Convert.ToDateTime("01/01/1000");
                int weekno = Convert.ToInt32(GetString("week_no"));
                String weekday = GetString("Day_of_the_week");


                if (effective_date.Date >= today.Date)
                {
                    start_checking = effective_date.AddMonths(-1);

                }
                else
                    start_checking = last_run;

                while (start_checking < Convert.ToDateTime(end_date))
                {
                    DateTime week = calculate_week_start(start_checking, months_of_year);
                    for (int i = 0; i < 7; i++)
                    {
                        if (Convert.ToString(week.DayOfWeek) == weekday && (week.Date >= today.Date) && (week.Date >= effective_date.Date) && (week < Convert.ToDateTime(end_date)))
                        {
                            SetResult("nextrun", Convert.ToString(week.ToShortDateString()));
                            //Next_run_monthly.Text =Convert.ToString(week) ;
                            return;
                        }
                        week = week.AddDays(1);
                    }
                    start_checking = week;
                }
                SetResult("nextrun", "The next run is greater than the end date, this report will never run.");
            }
        }
        DateTime calculate_week_start(DateTime somedate, int[] months_of_year)
        {
            // This function is used for type 1 of monthly reports. Given a date it first find outs the next checked month 
            //and then the beginning of the specified week in that month


            DateTime start_checking;
            DateTime next_checked_month = first_day_of_next_checked_month(somedate, months_of_year);

            int weekno = Convert.ToInt32(GetString("week_no"));

            if (weekno == 5)
            {
                DateTime last_date = find_last_date_of_the_month(next_checked_month);
                start_checking = last_date.AddDays(-6);
                // we start checking 7 days before the month ends because that is the last week.
            }

            else
            {
                start_checking = next_checked_month.AddDays(7 * (weekno - 1));
                    }
            return start_checking;
        }

        DateTime calculate_week_start_for_database(DateTime somedate, int[] months_of_year, int weekno)
        {
            DateTime start_checking;
            DateTime next_checked_month = first_day_of_next_checked_month(somedate, months_of_year);

            if (weekno == 5)
            {
                DateTime last_date = find_last_date_of_the_month(next_checked_month);
                start_checking = last_date.AddDays(-6);
                
            }

            else
            {
                start_checking = next_checked_month.AddDays(7 * (weekno - 1));
                   }
            return start_checking;
        }


        DateTime first_day_of_next_checked_month(DateTime lastrun, int[] months_of_year)
        {
            
            DateTime trial_month = lastrun.AddMonths(1);
            int j = trial_month.Month;

            for (int x = j - 1, ii = 0; ii < 12; ii++)
            {
                if (months_of_year[x] == 1)
                {
                    DateTime return_date = new DateTime(trial_month.Year, trial_month.Month, 1, 0, 0, 0);
                    return return_date;
                }
                trial_month = trial_month.AddMonths(1);
                x = trial_month.Month;
                x--;
            }
            return System.DateTime.Now;
        }


        DateTime find_last_date_of_the_month(DateTime last_run)
        {
            // given_date = Convert.ToDateTime(given_date_string);
            int year = last_run.Year;
            int month = (last_run.Month) % 12;
            if (last_run.Month == 12)
                year = year + 1;
            DateTime new_date = new DateTime(year, month + 1, 1, 0, 0, 0);
            new_date = new_date.AddDays(-1);
            return new_date;
            // labelabc.Text = Convert.ToString(new_date);
        }
        #endregion

        private void validate_mail_list()
        {   
            const string EMAIL_REGEX_PATTERN = @"^[\w._%+-]+@[\w.-]+\.[A-Za-z]{2,6}$";

            Regex reEmail = new Regex(EMAIL_REGEX_PATTERN);
            
            string youradd = GetString("Your_address").TrimWhitespaceAndBOM();            

            bool your_add_check = reEmail.IsMatch(youradd);
            if (your_add_check == false)
            {
                SetResult("valid", "Please enter a valid e-mail id in the 'Your e-mail address' field");
                return;
            }

            string ids = GetString("CC_list");

            if (ids.TrimWhitespaceAndBOM() != string.Empty)
            {
                Regex reSep = new Regex(@"\s?[,;]\s?");
                string[] arrIds = reSep.Split(ids);

                for (int i = 0; i < arrIds.Length; i++)
                {
                    var true_or_false = reEmail.IsMatch(arrIds[i].TrimWhitespaceAndBOM());
                    if (true_or_false == false)
                    {
                        SetResult("valid","One or more e-mail addresses in the 'CC' column are incorrect.");
                        return;
                    }
                }
            }
            SetResult("valid", "true");
        }

        private void InsertIntoDatabase()
        {
            List<SqlParameter> list = new List<SqlParameter>();
            DataSet ds = new DataSet();

            int ReportId = -1;
            string ReportIdArg = GetString("ReportId");
            if (!String.IsNullOrEmpty(ReportIdArg))
            {
                ReportId = Convert.ToInt32(ReportIdArg);
                if (!CheckUser(Convert.ToInt32(ReportId)))
                    return;
            }
            //Problem is that this function is being called twice. Once from The thing InsertIntoDatabase wrote and 
            //a second time from code that already exists. Solution - Remove my stuff calling it
            //and then User get string and then set result to get the ReportId to edit

            if (ReportId < 0)
            {
                DataSetHelper.Fill(ds, this.BrokerId, "autoincrement", null);

                string ReportIdString = ds.Tables[0].Rows[0]["ReportId"].ToString();

                if (string.IsNullOrEmpty(ReportIdString))
                {
                    throw new CBaseException(ErrorMessages.FailedToSave, "Could not find report ID");
                }
                ReportId = Convert.ToInt32(ReportIdString);
            }
                
            list.Add(new SqlParameter("@ReportId", ReportId));
                        
            DateTime EffectiveDate = Convert.ToDateTime(GetString("effectiveDate"));
            DateTime NextRun = Convert.ToDateTime("1/1/1900"); 
            int Frequency = 0;
            DateTime EndDate = NextRun;
            string LastRun = GetString("LastRun");
            if (!String.IsNullOrEmpty(LastRun))
            {
                list.Add(new SqlParameter("@LastRun", LastRun));
            }
            string runTime;
            string retryUntil;
            string choice = GetString("choice");
            if (choice != "OneTime")
            {
                string nextRun = GetString("nextRun");
                if (nextRun != "The next run is greater than the end date, this report will never run." &&
                    nextRun != "Next run cannot be an invalid date or more than two years from today." &&
                    nextRun != "")
                    NextRun = Convert.ToDateTime(nextRun);
                EndDate = Convert.ToDateTime(GetString("endDate"));
                runTime = GetString("runTime");
                retryUntil = GetString("retryUntil");
                if (choice != "Monthly")
                {
                    Frequency = Convert.ToInt32(GetString("frequency"));
                    list.Add(new SqlParameter("@Frequency", Frequency));
                }
            }
            else
            {
                NextRun = EffectiveDate;
                EndDate = EffectiveDate.AddDays(1);
                runTime = CustomReportsScheduleUI.DefaultRunTime;
                retryUntil = CustomReportsScheduleUI.DefaultRetryUntil;
                list.Add(new SqlParameter("@Frequency", 100));
            }
            list.Add(new SqlParameter("@EffectiveDate", EffectiveDate));
            
            list.Add(new SqlParameter("@EndDate", EndDate));

            
            //Check validation before updating to database
            ValidateRunTimeRetryUntil(ref runTime, ref retryUntil);
            DateTime dateTime = DateTime.ParseExact(runTime, "h:mm tt", CultureInfo.InvariantCulture);
            list.Add(new SqlParameter("@RunTime", dateTime));
            //if (dateTime.Hour < 12)
            //    NextRun = NextRun.AddDays(1) + dateTime.TimeOfDay;
            //else 
            NextRun = NextRun + dateTime.TimeOfDay;
            if (NextRun < DateTime.Now) NextRun = NextRun.AddDays(1);
            dateTime = DateTime.ParseExact(retryUntil, "h:mm tt", CultureInfo.InvariantCulture);
            list.Add(new SqlParameter("@RetryUntil", dateTime));
            
            list.Add(new SqlParameter("@NextRun", NextRun));
            try
            {
                switch (choice)
                {
                    case "Daily":
                        list.Add(new SqlParameter("@IncludeSatSun", GetString("includeSatSun") == "yes"));

                        StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "CreateDailyReport", 0, list.ToList());
                        break;

                    case "Weekly":
                        string[] sweeks = GetString("weeks").Split(',');
                        for (int i = 0; i < sweeks.Length; i++)
                        {
                            SqlParameter Weekday = new SqlParameter("@Weekday", sweeks[i]);
                            list.Add(Weekday);
                            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "CreateWeeklyReport", 0, list.ToList());
                            list.Remove(Weekday);
                        }
                        break;

                    case "Monthly":
                        string[] months = GetString("months").Split(',');
                        SqlParameter Month;

                        if (GetString("option_monthly") == "option1")
                        {
                            int weekno = Convert.ToInt32(GetString("week_no"));
                            String weekday = GetString("weekday");
                            list.Add(new SqlParameter("@Weekday", weekday));
                            list.Add(new SqlParameter("@DateOfTheMonthOrWeekNo", weekno));

                            for (int i = 0; i < 12; i++)
                            {
                                if (months[i] != "0")
                                {
                                    Month = new SqlParameter("@Month", months[i]);
                                    list.Add(Month);
                                    StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "CreateMonthlyReport", 0, list.ToList());
                                    list.Remove(Month);
                                }
                            }
                        }
                        else
                        {
                            string[] days_of_month = GetString("Days_of_month").Split(',');
                            int[] daysOfMonth = new int[32];
                            for (int i = 0; i < 31; i++)
                            {
                                daysOfMonth[i + 1] = Convert.ToInt32(days_of_month[i]);
                            }

                            for (int i = 0; i < 12; i++)
                            {
                                if (months[i] != "0")
                                {
                                    Month = new SqlParameter("@Month", months[i]);
                                    list.Add(Month);
                                    for (int j = 1; j < 32; j++)
                                    {
                                        if (daysOfMonth[j] == 1)
                                        {
                                            SqlParameter day = new SqlParameter("@DateOfTheMonthOrWeekNo", j);
                                            list.Add(day);
                                            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "CreateMonthlyReport", 0, list.ToList());
                                            list.Remove(day);
                                        }
                                    }
                                    list.Remove(Month);
                                }
                            }
                        }
                        break;

                    case "OneTime":
                        list.Add(new SqlParameter("@IncludeSatSun", false));
                        StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "CreateDailyReport", 0, list.ToList());
                        break;

                    default:
                        throw new CBaseException(ErrorMessages.FailedToSave, "Invalid choice for the schedule period.");
                    //break;
                }
            }
            catch (Exception f)
            {
                Tools.LogError(f);
            }
        }

        private void InsertIntoScheduledReportUserInfo()
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@Email", GetString("mailId")));
            parameters.Add(new SqlParameter("@CcIds", GetString("CC")));
            parameters.Add(new SqlParameter("@ScheduleName", GetString("scheduleName")));
            parameters.Add(new SqlParameter("@QueryId", new Guid(GetString("queryId"))));
            parameters.Add(new SqlParameter("@ReportType", GetString("choice")));

            string ReportId = GetString("ReportId");
            if (String.IsNullOrEmpty(ReportId))
            {
                parameters.Add(new SqlParameter("@UserId", m_UserId));
                StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "InsertScheduledReportUserInfo", 0, parameters);
            }
            else
            {
                if (!CheckUser(Convert.ToInt32(ReportId)))
                {
                    return;
                }

                parameters.Add(new SqlParameter("@ReportId", ReportId));
                StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "UpdateScheduledReportUserInfo", 0, parameters);

                SqlParameter[] parameters1 = {
                    new SqlParameter("@ReportId", ReportId)
                };

                StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "RemoveScheduledReportPartially", 0, parameters1);
            }
        }

        protected bool CheckUser(int ReportId)
        {

            BrokerUserPrincipal user = BrokerUserPrincipal.CurrentPrincipal;

            // Locate all queries and compare employee identifiers.
            SqlParameter[] parameters = {
                                            new SqlParameter("@ReportId", ReportId),
                                            new SqlParameter("@BrokerId", user.BrokerId)
                                        };
            DataSet ds = new DataSet();

            try
            {
                DataSetHelper.Fill(ds, user.BrokerId, "GetScheduledReportDetails", parameters);
                if (ds.Tables.Count < 1 || ds.Tables[0].Rows.Count < 1)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Unable to find a scheduled report. ReportId = " + ReportId + ", UserId = " + user.UserId);
                }
            }
            catch (Exception e)
            {
                Tools.LogError("Loan reporting: Failed to get report query details.", e);
                throw;
            }

            try
            {
                DataRow row = ds.Tables[0].Rows[0];


                if (user.HasRole(E_RoleT.Manager) || user.HasRole(E_RoleT.Administrator))
                {
                    return true;
                }

                string OwnerId = row["UserId"].ToString();
                if (m_UserId != new Guid(OwnerId))
                {
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                // Note the error and report to user.
                Tools.LogError(e);
                return false;
            }
        }

        private Guid m_EmployeeId
        {
            get
            {
                // Lookup current principal.
                BrokerUserPrincipal broker = BrokerUserPrincipal.CurrentPrincipal;
                if (broker == null)
                {
                    return Guid.Empty;
                }
                return broker.EmployeeId;
            }
        }

        private Guid m_UserId
        {
            get
            {
                // Lookup current principal.
                BrokerUserPrincipal broker = BrokerUserPrincipal.CurrentPrincipal;
                if (broker == null)
                {
                    return Guid.Empty;
                }
                return broker.UserId;
            }
        }

        private Guid BrokerId
        {
            get
            {
                BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
                if (principal == null)
                {
                    return Guid.Empty;
                }
                return principal.BrokerId;
            }

        }
        //Long Nguyen
        private void ValidateRunTimeRetryUntil(ref string runTime, ref string retryUntil) {
            var found=false;
            //Check RunTime validation
            for(var i=0;i<CustomReportsScheduleUI.runList.Length;i++){
                if(runTime==CustomReportsScheduleUI.runList[i].Value)
                {
                    found=true;
                    break;
                }
            }
            if(!found) {
                //return default if invalid
                runTime=CustomReportsScheduleUI.DefaultRunTime;
                retryUntil=CustomReportsScheduleUI.DefaultRetryUntil;
                return;
            }
            var list=CustomReportsScheduleUI.RetryUntilList(runTime);
            //Check RetryUntil Validation
            found=false;
            for(var i=0;i<list.Length;i++)
            {
                if(retryUntil==list[i].Value)
                {
                    found=true;
                    break;
                }
            }
             if(!found) {
                //return default retryUntil if invalid
                retryUntil=list[0].Value;
            }
        }
    }
}