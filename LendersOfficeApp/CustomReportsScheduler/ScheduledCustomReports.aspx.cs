﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Threading;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.UI.HtmlControls;
using LendersOffice.QueryProcessor;
using LendersOffice.Reports;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;

namespace LendersOfficeApp.CustomReportsScheduler
{
    public partial class ScheduledCustomReports : LendersOffice.Common.BasePage
    {
        private LendersOffice.Reports.LoanReporting m_Loans;
        protected int selectedTabIndex = 0;

        protected void PageInit( object sender, System.EventArgs a )
		{
            IncludeStyleSheet("~/css/Tabs.css");
            try 
            {
                m_Loans = new LoanReporting();

                BrokerUserPrincipal user = BrokerUserPrincipal.CurrentPrincipal;
                bool canRunCustomReports = user.HasPermission(Permission.CanRunCustomReports);
                if (!(m_UserIsAdmin || canRunCustomReports))
                    Response.Redirect("~/los/main.aspx?showAlert=t");
			}
            catch( Exception e )
            {
				// Oops!

                m_ExceptionDetails = "Error initializing web form.";
                Tools.LogError( "Loan reporting: Exception caught during page init." );
                Tools.LogError( e );
            }
		}

        protected void PageLoad(object sender, System.EventArgs a)
        {
            // Initialize page objects.

            try
            {
                // Bind to the grid.
                BindGrid();
            }
            catch (Exception e)
            {
                // Oops!

                m_ExceptionDetails = "Error loading web form.";

                Tools.LogError("Loan reporting: Exception caught during page load.");
                Tools.LogError(e);
            }
        }

        #region Web Form Designer generated code


        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            // Pre-Render is the first event after the postback completes, so
            // deletion uses this event to load the updated data.
            this.PreRender += new System.EventHandler(this.PagePreRender); 
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion

        #region Getters and Setters
        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        private Guid m_BrokerId
        {
            get
            {
                BrokerUserPrincipal broker = BrokerUserPrincipal.CurrentPrincipal;

                if (broker == null)
                {
                    return Guid.Empty;
                }

                return broker.BrokerId;
            }
        }

        private Guid m_BranchId
        {
            get
            {
                BrokerUserPrincipal broker = BrokerUserPrincipal.CurrentPrincipal;

                if (broker == null)
                {
                    return Guid.Empty;
                }

                return broker.BranchId;
            }
        }

        private Guid m_BAdminId
        {
            get
            {
                return LendersOffice.Admin.BrokerDB.AdminBr;
            }
        }

        private String m_EmployeeNm
        {
            get
            {
                // Lookup current principal.

                BrokerUserPrincipal broker = BrokerUserPrincipal.CurrentPrincipal;

                if (broker == null)
                {
                    return "";
                }

                return broker.DisplayName;
            }
        }

        private Guid m_EmployeeId
        {
            get
            {
                // Lookup current principal.

                BrokerUserPrincipal broker = BrokerUserPrincipal.CurrentPrincipal;

                if (broker == null)
                {
                    return Guid.Empty;
                }

                return broker.EmployeeId;

            }
        }

        private Guid m_UserId
        {
            get
            {
                // Lookup current principal.
                BrokerUserPrincipal broker = BrokerUserPrincipal.CurrentPrincipal;
                if (broker == null)
                {
                    return Guid.Empty;
                }
                return broker.UserId;

            }
        }

        protected bool m_UserIsAdmin
        {
            get
            {
                // Lookup current principal.
                BrokerUserPrincipal broker = BrokerUserPrincipal.CurrentPrincipal;
                if (broker == null)
                {
                    return false;
                }
                return broker.IsInRole(ConstApp.ROLE_ADMINISTRATOR);

            }
        }

        protected bool m_SchedulesExist
        {
            get; set;
        }

        private String m_ExceptionDetails
        {
            set
            {
                if (value.EndsWith(".") == false)
                {
                    Page.ClientScript.RegisterHiddenField("m_ErrorMessage", value + ".");
                }
                else
                {
                    Page.ClientScript.RegisterHiddenField("m_ErrorMessage", value);
                }
            }
        }

        private String m_FeedbackMessage
        {
            // Access member.

            set
            {
                if (value.EndsWith(".") == false)
                {
                    Page.ClientScript.RegisterHiddenField("m_Feedback", value + ".");
                }
                else
                {
                    Page.ClientScript.RegisterHiddenField("m_Feedback", value);
                }
            }
        }
        #endregion

        protected void OpenNew(object sender, System.EventArgs a)
        { 
           Response.Redirect("CustomReportsScheduleUI.aspx");
         }

        protected void HandleGridOperation(object sender, System.Web.UI.WebControls.DataGridCommandEventArgs a)
        {
            Guid task = Guid.NewGuid();
            // Tools.LogInfo( "Loan reporting: Processing grid request (" + task.ToString() + ")..." );

            try
            {
                List<SqlParameter> pn = new List<SqlParameter>();
                BrokerUserPrincipal user = BrokerUserPrincipal.CurrentPrincipal;
                if (a.CommandName != "Run")
                {
                    // Search all queries for this broker and check the
                    // matching one against our current employee's id.
                    // Only the creator can remove it.
                    int ReportId = Convert.ToInt32(a.CommandArgument.ToString());

                    // Locate all queries and compare employee identifiers.
                    DataSet dn = new DataSet();

                    pn.Add(new SqlParameter("@ReportId", ReportId));
                    pn.Add(new SqlParameter("@BrokerId", user.BrokerId));

                    try
                    {
                        DataSetHelper.Fill(dn, user.BrokerId, "GetScheduledReportDetails", pn.ToList());
                        if (dn.Tables.Count < 1 || dn.Tables[0].Rows.Count < 1)
                            throw new CBaseException(ErrorMessages.Generic, "Unable to find a scheduled report. ReportId = " + ReportId + ", UserId = " + user.UserId);
                    }
                    catch (Exception e)
                    {
                        Tools.LogError("Loan reporting: Failed to get report query details.");

                        throw e;
                    }

                    // We have a lock.  Delete this report query and bail out.
                    try
                    {
                        DataRow row = dn.Tables[0].Rows[0];
                        string OwnerId = row["UserId"].ToString();
                        if (m_UserId != new Guid(OwnerId) && !m_UserIsAdmin && !user.IsInRole(ConstApp.ROLE_MANAGER))
                            throw new CBaseException(ErrorMessages.GenericAccessDenied, String.Format("Attempt to delete or edit scheduled report not owned by user. UserId = {0}. OwnerId = {1}", m_UserId, OwnerId));
                        else
                        {
                            // Add parameters for the delete command
                            pn.Clear();
                            pn.Add(new SqlParameter("@ReportId", ReportId));
                            pn.Add(new SqlParameter("@ReportType", row["ReportType"]));
                        }
                    }
                    catch (Exception e)
                    {
                        // Note the error and report to user.
                        m_ExceptionDetails = "Unable to remove\\edit report.";
                        Tools.LogError(e);
                        return;
                    }
                }

                // Lookup particular command and then execute it on
                // the given query.

                switch (a.CommandName)
                {
                    case "Run":
                        {
                            Guid id = new Guid(a.CommandArgument.ToString());

                            if (id == Guid.Empty)
                            {
                                break;
                            }
                            SqlParameter[] parameters = { new SqlParameter("@QueryId", id) };
                            Query query = new Query();

                            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(user.BrokerId, "GetReportQueryContentById", parameters))
                            {
                                if (reader.Read())
                                {
                                    query = reader["XmlContent"].ToString();

                                }
                                else
                                {
                                    throw new CBaseException("Loan reporting: Failed to get report query.", "Unable to load query_id=" + id);
                                }
                            }

                            // Run the query.  We check ids to see if the query was
                            // actually found.  If the query was not streamed in from
                            // the db, then its id is just a random sequence of bits.
                            // It would only match the passed in id if it came from
                            // the database.  Process this query directly.  Everything
                            // needed to run the query is persisted out when we save
                            // them in the editor.

                            if (query.Columns.Count > 0)
                            {
                                Report ra = new Report();

                                if (query.hasSecureFields && !query.CanUserRun(BrokerUser))
                                {
                                    string userMsg = "User does not have the permissions to run report: '" + query.Label.Title + "' which contains restricted fields.";
                                    string devMsg = "User '" + BrokerUser.LoginNm + "' (" + BrokerUser.UserId + ") from broker '" + BrokerUser.BrokerId + "' cannot run report '" + query.Label.Title + "' (" + query.Id + ") which contains restricted fields.";
                                    throw new PermissionException(userMsg, devMsg);
                                }

                                ra = m_Loans.RunReport(query, BrokerUser, !BrokerUser.HasPermission(Permission.AllowExportingFullSsnViaCustomReports), E_ReportExtentScopeT.Default);



                                if (ra != null)
                                {
                                    ra.Label.Title = query.Label.Title;
                                    ra.Id = Guid.NewGuid();

                                    // 05/12/06 mf - OPM 2582 & 4668.  To reduce server memory usage
                                    // do not use server cache for custom reports.

                                    AutoExpiredTextCache.AddToCacheByUser(BrokerUser, (string)ra, TimeSpan.FromMinutes(30), ra.Id);

                                    Page.ClientScript.RegisterHiddenField("m_ReportCache", ra.Id.ToString());


                                }
                                else
                                {
                                    m_ExceptionDetails = "Unable to run report.  Please check query parameters";

                                    return;
                                }
                            }
                            else
                            {
                                m_ExceptionDetails = "Unable to run report.  Please specify fields in the report's layout";

                                return;
                            }
                        }
                        break;
                    case "Delete":
                        try
                        {
                            StoredProcedureHelper.ExecuteNonQuery(user.BrokerId, "RemoveScheduledReport", 0, pn.ToList());
                        }
                        catch (Exception e)
                        {
                            // Note the error and report to user.
                            m_ExceptionDetails = "Unable to remove report.";
                            Tools.LogError(e);
                            return;
                        }
                        break;
                    case "edit":
                        Response.Redirect("CustomReportsScheduleUI.aspx?EditReport=" + Uri.EscapeDataString(a.CommandArgument.ToString()));
                        break;
                }
            }
            catch (ThreadAbortException)
            {
                // TODO: Do something about this exception. It occurs everytime Response.Redirect is used.
            }
            catch (PermissionException pe)
            {
                m_ExceptionDetails = "User does not have the permissions to run the selected report containing restricted fields.";

                Tools.LogError("Loan reporting: Permission exception caught during handling scheduled report related request.");
                Tools.LogError(pe);
            }
            catch (Exception e)
            {
                m_ExceptionDetails = "Failed to handle request.  Please try again.";

                Tools.LogError("Loan reporting: Exception caught during handling scheduled report related request.");
                Tools.LogError(e);
            }
        }
      
        protected void BindGrid()
        {
            // Bind ui to latest persistent state.
            try
            {
                DataSet dS = new DataSet();

                String eventArg = Request.Params.Get("__EVENTARGUMENT");
                try
                {
                    if (eventArg != null && eventArg != "")
                    {
                        selectedTabIndex = Int32.Parse(eventArg);
                    }
                    else if (ViewState["selectedTabIndex"] != null)
                    {
                        selectedTabIndex = (int)ViewState["selectedTabIndex"];
                    }
                    else
                        selectedTabIndex = 0;
                }
                catch (System.FormatException)
                {
                    selectedTabIndex = (int)ViewState["selectedTabIndex"];
                }
                ViewState["selectedTabIndex"] = selectedTabIndex;

                if (m_UserIsAdmin && selectedTabIndex == 1)
                {
                    SqlParameter[] parameters = {
                                                    new SqlParameter("@BrokerId", m_BrokerId)
                                                };
                    DataSetHelper.Fill(dS, m_BrokerId, "ScheduledReportQuery", parameters);
                    tab1.Attributes.Add("class", "selected");
                    tab0.Attributes.Remove("class");
                }
                else
                {
                    SqlParameter[] parameters = {
                                                    new SqlParameter("@BrokerId", m_BrokerId), 
                                                    new SqlParameter("@UserId", m_UserId)
                                                };
                    DataSetHelper.Fill(dS, m_BrokerId, "ScheduledReportQuery", parameters);
                    tab0.Attributes.Add("class", "selected");
                    tab1.Attributes.Remove("class");
                }
                    
                // Apply the data view.
                DataTable dt = dS.Tables[0];
                if (dt.Rows.Count == 0)
                {
                    m_SchedulesExist = false;
                }
                else
                {
                    Scheduledreports.DataSource = dt.DefaultView;
                    Scheduledreports.ItemDataBound += ScheduledReportsItemDataBound;
                    Scheduledreports.ItemDataBound += OnLastRunNull;
                    Scheduledreports.DataBind();
                    m_SchedulesExist = true;
                }
            }
            catch( Exception e )
            {
				// Oops!

                m_ExceptionDetails = "Error binding list to web form.";

                Tools.LogError( "Loan reporting: Exception caught during grid binding." );
                Tools.LogError( e );
            }
        }

        /// <summary>
        /// Bind our datagrid to the current reports.
        /// </summary>
        protected void PagePreRender(object sender, System.EventArgs a)
        {
            // Initialize page objects.

            try
            {
                // Bind to the grid.

                BindGrid();

            }
            catch (Exception e)
            {
                // Oops!

                m_ExceptionDetails = "Error prerendering web form.";

                Tools.LogError("Loan reporting: Exception caught during page prerender.");
                Tools.LogError(e);
            }
        }

        private void ScheduledReportsItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.DataItem == null)
            {
                return;
            }

            var row = (DataRowView)e.Item.DataItem;
            string reportId = row["ReportId"].ToString();
            Guid queryId = new Guid(row["QueryId"].ToString());

            var editButton = (LinkButton)e.Item.FindControl("edit");
            editButton.CommandName = "edit";
            editButton.CommandArgument = reportId;

            var deleteButton = (LinkButton)e.Item.FindControl("Delete");
            deleteButton.CommandName = "Delete";
            deleteButton.CommandArgument = reportId;

            var runButton = (LinkButton)e.Item.FindControl("Run");
            runButton.CommandName = "Run";
            runButton.CommandArgument = queryId.ToString();
        }

        private Query GetReportById(Guid reportId)
        {
            Query rQ = null;

            SqlParameter[] parameters = {
                                            new SqlParameter("@QueryId", reportId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_BrokerId, "GetReportQueryContentById", parameters))
            {
                if (reader.Read())
                {
                    rQ = reader["XmlContent"].ToString();
                }
                else
                {
                    throw new ArgumentException("Report not found.", "'" + reportId + "'");
                }
            }

            return rQ;
        }

        protected void OnLastRunNull(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
                return;

            LinkButton run = e.Item.FindControl("Run") as LinkButton;
            DataRowView row = e.Item.DataItem as DataRowView;
            Guid reportId = new Guid(row["QueryId"].ToString());
            Query query = GetReportById(reportId);
            bool isDisabled = false;

            if (query.hasSecureFields && !query.CanUserRun(BrokerUser))
                isDisabled = true;

            run.Visible = !isDisabled;

            object LastRunMaybe = DataBinder.GetPropertyValue(e.Item.DataItem, "LastRun");
            if (LastRunMaybe == System.DBNull.Value)
            {
                e.Item.Cells[5].Text = "N/A";
            }
            else
            {
                DateTime LastRun = (DateTime)LastRunMaybe;
                //e.Item.Cells[5].Text = LastRun.AddHours(23).AddMinutes(49).ToString("g");
                //Long Nguyen: No need to add hours. 
                //https://lqbopm/default.asp?215368
                e.Item.Cells[5].Text = LastRun.ToString("g");
            }
            
            object NextRunMaybe = DataBinder.GetPropertyValue(e.Item.DataItem, "NextRun");
            if (NextRunMaybe == DBNull.Value)
            {
                NextRunMaybe = null;
            }

            DateTime NeverRunDatePast = new DateTime(1900, 1, 1);
            DateTime NeverRunDateFuture = new DateTime(9999, 1, 1);
            DateTime NextRun = (DateTime)(NextRunMaybe ?? NeverRunDateFuture);
            
            if (NextRun == NeverRunDatePast || NextRun == NeverRunDateFuture)
            {
                e.Item.Cells[6].Text = "N/A";
            }
            else
            {
                //Make sure to indicate to the user that this will be run near midnight, not in the morning.
                //This datetime format specifier doesn't include seconds, which makes the formatting better.
                //e.Item.Cells[6].Text = NextRun.AddHours(23).AddMinutes(49).ToString("g");
                //Long Nguyen: No need to add hours. 
                //https://lqbopm/default.asp?215368
                e.Item.Cells[6].Text = NextRun.ToString("g");
            }
            if (isDisabled)
            {
                e.Item.Cells[6].Text = "restricted";
                e.Item.Cells[6].ForeColor = Color.Gray;
            }
                
        }
    }
}
