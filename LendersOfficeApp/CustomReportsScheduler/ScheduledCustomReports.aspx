<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScheduledCustomReports.aspx.cs" Inherits="LendersOfficeApp.CustomReportsScheduler.ScheduledCustomReports" %>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<html>
<head runat="server">
    <title>Scheduled Reports</title>
    <LINK href="../css/stylesheet.css" type="text/css" rel="stylesheet"/>
    <STYLE>
        .visible { DISPLAY: block }
        .hidden { DISPLAY: none }
    </STYLE>

    <script>
        function onInit() {
            try {
                window.resizeTo(650, 700);
            }
            catch(e) {
                // access denied for resizing, carry-on.
            }

            try {
                // Check if we have defined a report and cached it.  If so, then
                // launch the report in its own window.
                if (document.getElementById("m_ReportCache") != null) {
                    window.open("../los/Reports/ReportResult.aspx" + "?" + "id" + "=" + document.getElementById("m_ReportCache").value, "_blank", "resizable=yes,status=no,menubar=yes,scrollbars=yes,width=780,height=500");
                }
            }
            catch (e) {
                window.status = e.message;
            }
        }

        function f_displayAnimatedGif() {
            try {
                window.setTimeout("m_Wait.className = 'Visible';", 500);
            }
            catch (e) {
                window.status = e.message;
            }
        }
     </script>
</head>
<body onload="onInit();">
    <form id="form1" method ="post" runat="server">

    <table cellspacing="2" cellpadding="3" width="100%" border="0">
		<tr><td class="FormTableHeader" style="font-size:12px">Scheduled Reports</td></tr>
		<tr><td>
	        <table cellSpacing="0" cellPadding="0" border="0" width="100%">
        	    <tr><td>
                    <asp:Button ID="Button1" runat="server" onclick="OpenNew" Text="Add New Report" /> &nbsp;
                    <input type="button" value="Close" style="WIDTH: 60px" onclick="javascript:window.close();">
                    <hr>
		        </td></tr>



		        <% if (m_UserIsAdmin) { %>
		         <tr><td style=" BORDER-RIGHT: 2px outset; PADDING-BOTTOM: 5px; BORDER-LEFT: 2px outset;" >
		            <div class="Tabs">
		                <ul class="tabnav">
		                    <li runat="server" id="tab0"><a href="#" onclick="__doPostBack('changeTab', '0')">My Schedules</a></li>
		                    <li runat="server" id="tab1"><a href="#" onclick="__doPostBack('changeTab', '1')">All Schedules</a></li>
		                </ul>
		            </div>
		        </td></tr>
		        <tr><td style="BORDER-TOP-WIDTH: 2px; BORDER-RIGHT: 2px outset; PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: 2px outset; PADDING-TOP: 5px; BORDER-BOTTOM: 2px outset" vAlign="top">
                <% } else { %>
                <tr><td>
                <% } if (m_SchedulesExist) { %>
                    <ML:CommonDataGrid id="Scheduledreports" runat="server" CellPadding="2" AutoGenerateColumns="False" AllowSorting="True" Width="100%" OnItemCommand="HandleGridOperation">
                        <alternatingitemstyle cssclass="GridAlternatingItem"></alternatingitemstyle>
		                <itemstyle cssclass="GridItem"></itemstyle>
		                <headerstyle cssclass="GridHeader"></headerstyle>
		                <Columns>
                            <ASP:TemplateColumn HeaderText="" ItemStyle-CssClass="LookupRowLink" HeaderStyle-CssClass="LookupRowHeader" ItemStyle-Width="15px">
                                <ItemTemplate>
                                    <ASP:LinkButton id="edit" runat="server">edit</ASP:LinkButton>
                                </ItemTemplate>
                            </ASP:TemplateColumn>
                            <ASP:TemplateColumn HeaderText="" ItemStyle-CssClass="LookupRowLink" HeaderStyle-CssClass="LookupRowHeader" ItemStyle-Width="15px">
                                <ItemTemplate>
                                    <ASP:LinkButton id="Delete" runat="server" OnClientClick="return confirmDelete() == 1;">delete</ASP:LinkButton>
                                </ItemTemplate>
                            </ASP:TemplateColumn>
                            <ASP:TemplateColumn HeaderText="" ItemStyle-CssClass="LookupRowLink" HeaderStyle-CssClass="LookupRowHeader" ItemStyle-Width="15px">
                                <ItemTemplate>
                                    <ASP:LinkButton id="Run" runat="server" OnClientClick="f_displayAnimatedGif();">run</ASP:LinkButton>
                                </ItemTemplate>
                            </ASP:TemplateColumn>
                            <ASP:BoundColumn DataField="ScheduleName" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader" SortExpression="ScheduleName" HeaderText="Schedule Name">
                            </ASP:BoundColumn>
                            <ASP:BoundColumn DataField="QueryName" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader" SortExpression="CustomReportQuery" HeaderText="Custom Report Query">
                            </ASP:BoundColumn>
                            <ASP:BoundColumn DataField="LastRun" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader" SortExpression="LastRun" HeaderText="Last Run" ReadOnly="False" >
                            </ASP:BoundColumn>
                            <ASP:BoundColumn DataField="NextRun" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader" SortExpression="NextRun" HeaderText="Next Run">
                            </ASP:BoundColumn>
                            <ASP:BoundColumn DataField="RunTime" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader" SortExpression="RunTime" HeaderText="Run Time" DataFormatString="{0:t}">
                            </ASP:BoundColumn>
                            <ASP:BoundColumn DataField="RetryUntil" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader" SortExpression="RetryUntil" HeaderText="Retry Until"  DataFormatString="{0:t}">
                            </ASP:BoundColumn>
                        </Columns>
                    </ML:CommonDataGrid>
                <% } else { %>
                    There are no schedules to show
                <% } %>
                    <DIV class="BasePanel">
                        <DIV id="m_Wait" class="hidden" style="BORDER-RIGHT: gainsboro 2px solid; PADDING-RIGHT: 0.3in; BORDER-TOP: gainsboro 2px solid; PADDING-LEFT: 0.3in; Z-INDEX: 9; BACKGROUND: whitesmoke; LEFT: 80px; PADDING-BOTTOM: 0.3in; BORDER-LEFT: gainsboro 2px solid; PADDING-TOP: 0.3in; BORDER-BOTTOM: gainsboro 2px solid; POSITION: absolute; TOP: 40px" onclick="this.className='Hidden';">
                            <DIV style="BORDER-RIGHT: gainsboro 2px solid; PADDING-RIGHT: 16px; BORDER-TOP: gainsboro 2px solid; PADDING-LEFT: 16px; PADDING-BOTTOM: 16px; BORDER-LEFT: gainsboro 2px solid; WIDTH: 3in; PADDING-TOP: 16px; BORDER-BOTTOM: gainsboro 2px solid; BACKGROUND-COLOR: white">
                                <TABLE cellpadding="0" cellspacing="0">
                                    <TR>
                                    <TD>
                                        <IMG src="../los/Reports/Waiting.gif">
                                    </TD>
                                    <TD>
                                        <SPAN style="FONT: bold 11px arial">
                                            Processing request.  Please wait...
                                        </SPAN>
                                    </TD>
                                    </TR>
                                </TABLE>
                            </DIV>
                        </DIV>
                    </DIV>
                </td></tr>
            </table>
        </td></tr>
    </table>
    </form>

    <SCRIPT language="vbscript">
    function confirmDelete

        ' Display Yes, No, Cancel with Exclaimation icon alert box.
        ' Ret = 1 - Ok, 2 - Cancel

        Select Case MsgBox( "Delete Schedule?" , 1 + 48 , "LendingQB" )
            Case 1
                confirmDelete = 1
            Case Else
                confirmDelete = 0
        End Select

    end function
   </script>
</body>
</html>
