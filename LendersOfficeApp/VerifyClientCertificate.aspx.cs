﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;

namespace LendersOfficeApp
{
    public partial class VerifyClientCertificate : MinimalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string cacheId = RequestHelper.GetMFACacheKey(RequestHelper.GetSafeQueryString("id"));

            string str = AutoExpiredTextCache.GetFromCache(cacheId);

            string[] parts = str.Split('|');

            if (parts.Length != 3)
            {
                throw CBaseException.GenericException("Unexpected format for ClientCertificate string.");
            }
            // This page only verify if user has proper client certificate.
            // If user certificate is valid then redirect to successful url.
            Guid brokerId = new Guid(parts[0]);
            Guid userId = new Guid(parts[1]);
            bool redirectToAuthenticationCodePageOnInvalid = parts[2] == "1";

            var certificate = Request.ClientCertificate;
            
            if (ClientCertificateUtilities.IsValidClientCertificate(certificate, PrincipalFactory.CurrentPrincipal))
            {
                // Passing Guid.Empty for PML Site ID since this will be a B User.
                RequestHelper.InsertAuditRecord(PrincipalFactory.CurrentPrincipal, brokerPmlSiteId: Guid.Empty);
                RequestHelper.DoNextPostLoginTask(PostLoginTask.Login);
            }
            else
            {
                if (redirectToAuthenticationCodePageOnInvalid)
                {
                    string url = ConstApp.LendingQBAuthenticationCodePage;

                    RequestHelper.GenerateMultiFactorAuthenticationCode(brokerId, userId);
                    url += "?id=" + RequestHelper.GetSafeQueryString("id");

                    RequestHelper.RedirectToNextTaskPage(HttpContext.Current, url);
                }
                else
                {
                    RequestHelper.ClearAuthenticationCookies();
                    // 5/11/2014 dd - Assume when get redirect to error page, it is because of IP restriction.
                    string devMessage = "Access failed due to IP restriction. Current IP :" + RequestHelper.ClientIP + " UserId:" + userId;
                    ErrorUtilities.DisplayErrorPage(new CBaseException(ErrorMessages.IPRestrictionAccessDenied, devMessage), false, Guid.Empty, Guid.Empty);
                    Response.Redirect(ConstApp.LendingQBErrorPage);
                }
            }
        }
    }
}
