using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;

namespace LendersOfficeApp.LegacyLink
{
	/// <summary>
	/// Summary description for ImportSummary.
	/// </summary>
	public partial class ImportSummary : MinimalPage
	{
	
		protected void PageLoad(object sender, System.EventArgs e)
		{
			string sImportFile = DataAccess.TempFileUtils.Name2Path(Request["filename"]) ;

			DataSet ds = new DataSet() ;
			ds.ReadXml(sImportFile) ;
			DataView dv = ds.Tables[0].DefaultView ;
			dgSessionHistory.DataSource = dv ;
			dgSessionHistory.DataBind() ;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}
}
