using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;

namespace LendersOfficeApp.LegacyLink
{
	/// <summary>
	/// Summary description for Processing.
	/// </summary>
	public partial class Processing : MinimalPage
	{
	
		protected void PageLoad(object sender, System.EventArgs e)
		{
			if (null != Request["cancelURL"])
				btnCancel.Visible = true ;
			else
				btnCancel.Visible = false ;

			if (null != Request["message"])
				labMessage.Text = Request["message"] ;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

	}
}
