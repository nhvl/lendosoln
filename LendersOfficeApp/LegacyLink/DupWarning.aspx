<%@ Page language="c#" Codebehind="DupWarning.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LegacyLink.DupWarning" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>DupWarning</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script type="text/javascript" src="../common/ModalDlg/CModalDlg.js"></script>
		<LINK href="../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<script type="text/javascript">
	function init()
	{
		resize(350, 280);
	}
	function onContinue()
	{	var choice = getDupChoice() ;
		if (null == choice)
			alert('Please select an option') ;
		else
		{
			var arg = window.dialogArguments || {};
			arg.dup_choice = choice ;
			arg.apply_to_all = document.DupWarning.apply_to_all.checked ;
			onClosePopup(arg) ;
		}
	}
	function getDupChoice()
	{
		for (var i = 0 ; i < document.DupWarning.dup_choice.length ; i++)
			if (document.DupWarning.dup_choice[i].checked)
				return document.DupWarning.dup_choice[i].value ;
		return null ;
	}
	</script>
	<body MS_POSITIONING="FlowLayout" onload="init();">
		<form id="DupWarning" method="post" runat="server">
		<table border=0 cellpadding=5><tr><td>
			Loan <b>
				<%= AspxTools.HtmlString( Request[ "filename" ] ) %>
			</b>already exists in the system.  What would you like to do?
			<br>
			<br>
			<input type="radio" value="create"    name="dup_choice" onclick="document.getElementById('m_Continue').disabled = false">Create a new loan with a new name<br>
			<input type="radio" value="overwrite" name="dup_choice" onclick="document.getElementById('m_Continue').disabled = false">Overwrite the existing loan<br>
			<input type="radio" value="skip" 	  name="dup_choice" onclick="document.getElementById('m_Continue').disabled = false">Skip this loan<br>
			<input type="radio" value="abort" 	  name="dup_choice" onclick="document.getElementById('m_Continue').disabled = false">Abort the import process<br>
			<br>
			<input type="checkbox" name="apply_to_all" id="apply_to_all">apply my choice to all that already exist
			<br>
			<br>
			<input id="m_Continue" type="button" value="Continue" disabled onclick="onContinue();">
			</td></tr></table>
		</form>
	</body>
</HTML>
