<%@ Page language="c#" Codebehind="Processing.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LegacyLink.Processing" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Processing</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../css/stylesheet.css" type="text/css" rel="stylesheet">
		<script type="text/javascript">
		function OnCancel()
		{
//			document.Processing.btnCancel.disabled = true ;
			location.href = <%=AspxTools.SafeUrl(Request["cancelURL"])%> ;
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="Processing" method="post" runat="server">
			<br>
			<br>
			<br>
			<br>
			<center>
				<b>
					<asp:Label id="labMessage" runat="server"></asp:Label></b>
				<br>
				<br>
				<IMG src="../images/status.gif">&nbsp;
				<br>
				<br>
				<INPUT id="btnCancel" type="button" value="Cancel" name="btnCancel" runat="server" onclick="OnCancel();">
			</center>
		</form>
	</body>
</HTML>
