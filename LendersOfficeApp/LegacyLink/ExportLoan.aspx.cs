using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Xml;
using System.IO;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Common;

namespace LendersOfficeApp.LegacyLink
{
	public partial class ExportLoan : MinimalPage
	{
		protected void PageLoad(object sender, System.EventArgs e)
		{
			switch(Request["format"].ToUpper())
			{
				case "LONXML":
				{
					CPageData dataLoan = new CLoanLONXmlWriterData(new Guid(Request["loanid"]));
					dataLoan.SetFormatTarget( FormatTarget.PointNative );
					dataLoan.InitLoad();

					using (MemoryStream memStream = new MemoryStream(100000))
					{
						XmlTextWriter xmlWriter = new XmlTextWriter(memStream, System.Text.Encoding.ASCII) ;

						LendersOffice.Conversions.LON.LoanLONXmlWriter lonWriter = new LendersOffice.Conversions.LON.LoanLONXmlWriter() ;
						lonWriter.Write(xmlWriter, dataLoan) ;
						xmlWriter.Flush() ;

						// return requested data
						Response.Clear() ;
						Response.ClearContent();
						Response.ContentType = "text/xml";
						Response.OutputStream.Write(memStream.GetBuffer(), 0, (int)memStream.Position) ;
                        Response.Flush();
						Response.End() ;
					}
					break ;
				}
				case "EXPORT_TO_LON":
				{
					CPageData dataLoan = new CLoanNameOnlyData(new Guid(Request["loanid"]));
					dataLoan.InitSave(ConstAppDavid.SkipVersionCheck) ;
					// this should really be E_IntegrationT.LON but...
					dataLoan.IntegrationTypeAdd( DataAccess.E_IntegrationT.NHC );
					dataLoan.Save() ;
					break ;
				}
			}
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
