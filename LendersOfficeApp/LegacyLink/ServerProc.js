/*
	Example: 
	
	var proc = new ServerProc("IMPORT", "~/LegacyLink/MyInterface/ServerProc.aspx") ;
	proc.AddTextParam("param1", "value1") ;
	alert(proc.Execute()) ;
*/
function ServerProc(sProcName, sServerURL)
{
	this.m_xd = new ActiveXObject("MSXML2.DOMDocument.3.0") ;
	this.m_xd.documentElement = this.m_xd.createElement("SERVER_PROC") ;
	this.m_xd.documentElement.setAttribute("name", sProcName) ;
	this.m_sServerURL = sServerURL ;
	this.AddTextParam = ServerProc_AddTextParam ;
	this.AddBinaryParam = ServerProc_AddBinaryParam ;
	this.Execute = ServerProc_Execute ;
	return this ;
}
function ServerProc_AddTextParam(sName, sValue)
{
	var xeParam = this.m_xd.createElement("PARAM") ;
	xeParam.setAttribute("name", sName) ;
	xeParam.text = sValue ;
	this.m_xd.documentElement.appendChild(xeParam) ;
}
function ServerProc_AddBinaryParam(sName, sValue)
{
	var xeParam = this.m_xd.createElement("PARAM") ;
	
	// we use bin.base64 and not bin.hex because it's easier to
	// convert base64 than hex on the server side.
	xeParam.setAttribute("name", sName) ;
	xeParam.dataType = "bin.base64" ;
	xeParam.nodeTypedValue = sValue ;
	
	this.m_xd.documentElement.appendChild(xeParam) ;
}
function ServerProc_Execute()
{
	var oHttp = new ActiveXObject("Microsoft.XMLHTTP") ;
	oHttp.open("POST", this.m_sServerURL, false) ;
	oHttp.send(this.m_xd) ;
	return new ServerResult(oHttp.responseText) ;
}
function ServerResult(data)
{
	this.data = data ;
	this.m_xd = new ActiveXObject("MSXML2.DOMDocument.3.0") ;
	if (!this.m_xd.loadXML(data))
	{
		alert(this.m_xd.parseError.reason) ;
		return ;
	}
	this.proc_return = this.m_xd.selectSingleNode("STATUS/RETURN").text ;
	this.proc_statuscode = this.m_xd.selectSingleNode("STATUS/CODE").text ;
	this.proc_message = this.m_xd.selectSingleNode("STATUS/MESSAGE").text ;
	return this ;
}
