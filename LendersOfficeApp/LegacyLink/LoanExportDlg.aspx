<%@ Page language="c#" Codebehind="LoanExportDlg.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LegacyLink.LoanExportDlg" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.Security"%>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>LoanExportDlg</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript">
		// NOTE: This dialog box does not WORK in a Modal environment.
		// The reason is the window.open in the Export function causes IE to hang.
		// If someone wants to make this window modal then they will have to sovle
		// that problem first.
		function init()
		{
		  <% if (! /*((BrokerUserPrincipal) Page.User)*/ BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanExportLoan)) { %>
		    alert(<%= AspxTools.JsString(JsMessages.NoExportPermission) %>);
		    self.close();
		  <% } %>

			resize(400, 350);
		}
		function Export(ExportType)
		{
			if ("POINT" == ExportType)
			{
				location.href = <%= AspxTools.JsString(DataAccess.Tools.VRoot) %> + '/LegacyLink/CalyxPoint/ExportOptions.aspx?loanid=' + <%= AspxTools.JsString(Request["loanid"]) %> + '&ispopup=t' ;
			}
			else if ("LONXML" == ExportType)
			{
				window.open(<%= AspxTools.JsString(DataAccess.Tools.VRoot) %> + '/LegacyLink/ExportLoan.aspx?format=LONXML&loanid=' + <%= AspxTools.JsString(Request["loanid"]) %>) ;
				self.close() ;
			}
			else if ("EXPORT_TO_LON" == ExportType)
			{
				var oHttp = new ActiveXObject("Microsoft.XMLHTTP");
				oHttp.open("POST", <%= AspxTools.JsString(DataAccess.Tools.VRoot) %> + '/LegacyLink/ExportLoan.aspx?format=EXPORT_TO_LON&loanid=' + <%= AspxTools.JsString(Request["loanid"]) %>, false);
				oHttp.send();
				self.close() ;
			}
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="FlowLayout" onload="init();">
		<form id="LoanExportDlg" method="post" runat="server">
			<b>Export to what format?
				<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg></b><hr>
			<table cellspacing=5><tr><td>
			<%if (m_isLON) {%>
			<a href="#" onclick='Export("LONXML");'>LON XML</a>
			<br>
			<br>
			<a href="#" onclick='Export("EXPORT_TO_LON");'>Export to LON</a>
			<br>
			<br>
			<%}%>
			</td></tr>
			<tr><td>
			Calyx Point, Fannie Mae and Freddie Mac formats can be exported by editing this loan and then going to File -> Export menu
			</td></tr>
			<tr><td align=center>
			<INPUT type="button" value="Close" onclick="onClosePopup()">
			</td></tr></table>
		</form>
	</body>
</HTML>
