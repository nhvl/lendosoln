<%
	Option Explicit
	
	Response.Buffer = true
%>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
<%
' exec main
main

sub main
	SendScript "CalyxPoint/ClientCode.asp"
end sub

sub SendScript(sFileName)
	const ForReading = 1
	dim oFS: set oFS = server.CreateObject("Scripting.FileSystemObject")
	dim oTS: set oTS = oFS.OpenTextFile(server.MapPath(sFileName), ForReading)
	dim sCode: sCode = oTS.ReadAll
	
	' strip out the ASP tags if necessary
	dim nStart: nStart = InStr(1, sCode, "<%") + 2
	dim nEnd: nEnd = InStrRev(sCode, "%" & ">")
	if nEnd > 0 then
		sCode = Mid(sCode, nStart, nEnd - nStart)
	end if
	
	' add more global vars
	sCode = "option explicit" & vbCrlf & "dim AppRoot: AppRoot = """ & GetAppRoot & """" & sCode
	
	' send code down to the client
	Response.Clear
	Response.Write sCode
end sub

function GetAppRoot
	dim sURL: sURL = UCase(Request.ServerVariables("URL"))
	dim n: n = InStr(1, sURL, "LEGACYLINK")
	if n = 0 then
		GetAppRoot = "/"
	else
		GetAppRoot = Left(sURL, n-1)
	end if
end function
%>
