using System;
using System.Collections;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.LegacyLink
{
	public partial class LoanExportDlg : MinimalPage
	{
		protected bool m_isLON = false;
		protected void PageLoad(object sender, System.EventArgs e)
		{
            m_isLON = /*((BrokerUserPrincipal) Page.User) */ BrokerUserPrincipal.CurrentPrincipal.HasLONIntegration;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}
}
