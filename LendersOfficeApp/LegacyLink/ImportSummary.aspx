<%@ Page language="c#" Codebehind="ImportSummary.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LegacyLink.ImportSummary" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ImportSummary</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="ImportSummary" method="post" runat="server">
			<TABLE cellSpacing="5" border="0">
				<TR>
					<td class="FormTable">
						<center><font class="FieldLabel">Import History</font></center>
						<br>
						<asp:DataGrid id="dgSessionHistory" runat="server" AutoGenerateColumns="False">
							<SelectedItemStyle CssClass="GridSelectedItem"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="GridAlternatingItem"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem"></ItemStyle>
							<HeaderStyle CssClass="GridHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="filename" HeaderText="FileName"></asp:BoundColumn>
								<asp:BoundColumn DataField="status_message" HeaderText="Status Message"></asp:BoundColumn>
								<asp:BoundColumn DataField="status_code" HeaderText="OK?"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
						<br>
						<br>
						<center>
							<input type="button" value="Close" onclick="location.href='closeapp.asp'">
						</center>
					</td>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
