﻿namespace LendersOfficeApp.LegacyLink.CalyxPoint
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// This export page will return a zip file containing Point files for the specified loan(s).
    /// </summary>
    public partial class CalyxPointExport : BaseServicePage
    {
        /// <summary>
        /// Retrieves the data provided from the request, and calls the appropriate export helper functions.
        /// </summary>
        /// <param name="sender">The object that triggered the event.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var loanId = RequestHelper.GetGuid("loanid", Guid.Empty);
            var extension = RequestHelper.GetSafeQueryString("extension");
            var fileName = HttpUtility.HtmlDecode(RequestHelper.GetSafeQueryString("filename"));

            if (!string.IsNullOrEmpty(fileName))
            {
                fileName = string.Join("_", fileName.Split(Path.GetInvalidFileNameChars()));
            }

            var nameToFileDict = new Dictionary<string, byte[]>();
            var isBatch = RequestHelper.GetBool("IsBatch");
            var cacheId = RequestHelper.GetSafeQueryString("id");
            var useBorrName = RequestHelper.GetBool("useBorrName");

            string content;
            if (isBatch)
            {
                content = PointExportHelper.CreateCalyxExportZip(cacheId, PrincipalFactory.CurrentPrincipal.BrokerId, useBorrName);
            }
            else
            {
                content = PointExportHelper.CreateCalyxExportZip(loanId, PrincipalFactory.CurrentPrincipal.BrokerId, fileName, extension);
            }

            Response.Clear();
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", $"attachment; filename=\"CalyxPointFiles.zip\"");
            Response.Write(content);
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
    }
}