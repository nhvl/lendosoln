using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using LendersOffice.Common;

namespace LendersOfficeApp.LegacyLink.CalyxPoint
{
	/// <summary>
	/// Summary description for BrowseFile.
	/// </summary>
	public partial class BrowseFile : MinimalPage
	{

		string m_sImportFileNames = "" ;

		protected void PageLoad(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				if (null != Request["path"] && "" != Request["path"])
					txtPath.Text = Request["path"].Replace("/", @"\") ;

				if ("loadindex" == Request["cmd"])
				{
					ViewState["indexfile"] = Request["indexfile"] ;

					LoadFile() ;
				}
			}
			btnImport.Enabled = dgFileList.Items.Count > 0 ;
			cbOverwrite.Enabled = btnImport.Enabled ;
			btnImport.Attributes.Add("onclick", "this.disabled = true; __doPostBack('" + btnImport.ClientID + "', '');") ;
		}

		void LoadFile()
		{
			// default sort method is by filename
			if (null == ViewState["sortkey"])
			{
				ViewState["sortkey"] = "FileName" ;
				ViewState["sortdir"] = " ASC" ;
			}

			// must convert the filename to a tempfile w/ paths
			string sXmlFileName = DataAccess.TempFileUtils.Name2Path(ViewState["indexfile"].ToString()) ;

			DataSet ds = new DataSet() ;
			ds.ReadXml(sXmlFileName) ;
			DataView dv = ds.Tables[0].DefaultView ;
			dv.Sort = ViewState["sortkey"].ToString() + ViewState["sortdir"].ToString() ;
			
			dgFileList.DataSource = dv ;
			dgFileList.DataBind() ;

			if (dgFileList.Items.Count > 0)
			{
				string sVal = null ;
				foreach (DataGridItem item in dgFileList.Items) 
				{
					CheckBox cb = (CheckBox)item.FindControl("cbSelect") ;
					if (null == sVal)
						sVal = "'" + cb.ClientID + "'" ;
					else
						sVal += ", '" + cb.ClientID + "'" ;
				}
				
				ClientScript.RegisterArrayDeclaration("CheckBoxes", sVal) ;
				CheckBox cbHeader = (CheckBox)dgFileList.FindControl("_ctl1:cbSelectHeader") ;
				cbHeader.Enabled = true ;
				cbHeader.Attributes.Add("onclick", "SetCheckbox(" + cbHeader.ClientID + ");") ;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgFileList.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgFileList_SortCommand);
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

		private void dgFileList_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			if (ViewState["sortkey"].ToString() == e.SortExpression.ToString())
			{
				// change sort direction
				if (" ASC" == ViewState["sortdir"].ToString())
					ViewState["sortdir"] = " DESC" ;
				else
					ViewState["sortdir"] = " ASC" ;
			}
			else
			{
				ViewState["sortkey"] = e.SortExpression.ToString() ;
				ViewState["sortdir"] = " ASC" ;
			}
			LoadFile() ;
		}

		protected void btnImport_Click(object sender, System.EventArgs e)
		{
			string sFileNames = "" ;
			for (int i = 0 ; i < dgFileList.Items.Count ; i++)
			{
				CheckBox chkbox = (CheckBox)dgFileList.Items[i].FindControl("cbSelect") ;
				if (chkbox.Checked)
				{
					string sFileName = dgFileList.DataKeys[i].ToString() ;
					if (sFileNames.Length == 0)
						sFileNames = sFileName ;
					else
						sFileNames += ";" + sFileName ;
				}
			}

			m_sImportFileNames = sFileNames ;
		}
		public string ImportFileNames
		{
			get { return m_sImportFileNames ; }
		}
	}
}
