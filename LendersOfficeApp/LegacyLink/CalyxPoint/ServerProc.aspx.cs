namespace LendersOfficeApp.LegacyLink.CalyxPoint
{
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Text;
    using System.Web.SessionState;
    using System.Web.UI;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Events;
    using LendersOffice.Roles;
    using LendersOffice.Security;
    using LendersOfficeApp.los.admin;

    public partial class ServerProc : MinimalPage
    {
        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            Response.ContentType = "text/xml";
            XmlDocument doc = new XmlDocument();
            doc.Load(Request.InputStream);
            switch (doc.DocumentElement.GetAttribute("name"))
            {
                case "CacheIndexFile":
                    CacheIndexFile(doc);
                    break;
                case "UploadTempFile":
                    UploadTempFile(doc);
                    break;
                case "ImportFile2":
                    ImportFile2(doc);
                    break;
                case "ExportFile":
                    ExportFile(doc);
                    break;
                default:
                    Response.Write(new ServerProcReturn(ServerProcStatus.ERROR, "Invalid procedure call").ToString());
                    break;
            }
        }

        private NameValueCollectionWrapper GetNameValueData(XmlElement xeData)
        {
            // make a temp filename
            string sFilePath = TempFileUtils.NewTempFilePath();

            // decode the data
            byte[] bytes = Convert.FromBase64String(xeData.InnerText);

            // write the data to the temporary file
            BinaryFileHelper.WriteAllBytes(sFilePath, bytes);

            // * Parse the Point file
            PointInteropLib.DataFile reader = new PointInteropLib.DataFile();
            NameValueCollection colInput = reader.ReadFile(sFilePath);

            //            foreach (string key in colInput.Keys) 
            //            {
            //                Tools.LogError(string.Format("[{0}] - {1}", key, colInput[key]));
            //            }
            return new NameValueCollectionWrapper(colInput);
        }

        /// <summary>
        /// Export a single Point file from LendingQB.
        /// </summary>
        /// <param name="doc"></param>
        void ExportFile(XmlDocument doc)
        {
            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal; //(BrokerUserPrincipal) User;
                                                                                  // * Get the loan ID
            XmlElement xeLoanID = (XmlElement)doc.SelectSingleNode("SERVER_PROC/PARAM[@name='loanid']");
            if (null == xeLoanID)
            {
                Response.Write(new ServerProcReturn(ServerProcStatus.ERROR, "Data is missing").ToString());
                return;
            }
            CPointData loanData;
            try
            {
                loanData = new CPointData(XmlConvert.ToGuid(xeLoanID.InnerText));
                loanData.SetFormatTarget(FormatTarget.PointNative);
                loanData.InitLoad();
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                Response.Write(new ServerProcReturn(ServerProcStatus.ERROR, "Error loading loan file: " + e.Message).ToString());
                return;
            }

            NameValueCollectionWrapper rg = new NameValueCollectionWrapper(new NameValueCollection());
            XmlDocument xdRet = new XmlDocument();
            xdRet.LoadXml("<APPLICANTS/>");
            PointInteropLib.DataFile datafile = new PointInteropLib.DataFile();
            PointDataConversion pointConversion = new PointDataConversion(principal.BrokerId, loanData.sLId);

            for (int iApp = 0; iApp < loanData.nApps; iApp++)
            {
                using (MemoryStream stream = new MemoryStream(100000))
                {
                    pointConversion.TransferDataToPoint(loanData, rg, iApp);

                    //OPM 22255 OPM 23870
                    datafile.WriteSortedStream(stream, rg.GetCollection());
                    XmlElement xeApp = xdRet.CreateElement("APPLICANT");
                    xeApp.InnerText = Convert.ToBase64String(stream.GetBuffer(), 0, (int)stream.Position);
                    xdRet.DocumentElement.AppendChild(xeApp);
                }
            }

            // reset stream, loop again

            Response.Write(new ServerProcReturn(ServerProcStatus.OK, "File exported", xdRet.OuterXml).ToString());
        }


        /// <summary>
        /// Import a single Point file into LendingQB.
        /// Return "OK" or an error message if necessary.
        /// </summary>
        /// <param name="doc"></param>
        void ImportFile2(XmlDocument doc)
        {
            try
            {
                BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal; //(BrokerUserPrincipal) User;
                bool bOverwritten = false;  // this will be changed to TRUE if an existing file is overwritten

                // * Save the point data to a file
                XmlElement xeData = (XmlElement)doc.SelectSingleNode("SERVER_PROC/PARAM[@name='dataBorrower']");
                if (null == xeData)
                {
                    Response.Write(new ServerProcReturn(ServerProcStatus.ERROR, "Data is missing").ToString());
                    return;
                }

                XmlElement xeOverwrite = (XmlElement)doc.SelectSingleNode("SERVER_PROC/PARAM[@name='overwrite']");
                bool bOverwrite = false;
                if (null != xeOverwrite && "T" == xeOverwrite.InnerText)
                    bOverwrite = true;

                XmlElement xeForceCreate = (XmlElement)doc.SelectSingleNode("SERVER_PROC/PARAM[@name='force_create']");
                bool bForceCreate = false;
                if (null != xeForceCreate && "T" == xeForceCreate.InnerText)
                    bForceCreate = true;

                XmlElement xeRenameLoan = (XmlElement)doc.SelectSingleNode("SERVER_PROC/PARAM[@name='renameLoan']");
                bool bRenameLoan = false;
                if (null != xeRenameLoan && "T" == xeRenameLoan.InnerText)
                    bRenameLoan = true;

                XmlElement xeLoanOfficer = (XmlElement)doc.SelectSingleNode("SERVER_PROC/PARAM[@name='loanOfficerID']");
                Guid defaultAssignLoanOfficerID = new Guid("11111111-1111-1111-1111-111111111111");
                if (null != xeLoanOfficer)
                {
                    try
                    {
                        defaultAssignLoanOfficerID = new Guid(xeLoanOfficer.InnerText);
                    }
                    catch { }
                }

                XmlElement xeLoanProcessor = (XmlElement)doc.SelectSingleNode("SERVER_PROC/PARAM[@name='loanProcessorID']");
                Guid defaultAssignLoanProcessorID = new Guid("11111111-1111-1111-1111-111111111111");
                if (null != xeLoanProcessor)
                {
                    try
                    {
                        defaultAssignLoanProcessorID = new Guid(xeLoanProcessor.InnerText);
                    }
                    catch { }
                }

                XmlElement xeLoanOpener = (XmlElement)doc.SelectSingleNode("SERVER_PROC/PARAM[@name='loanOpenerID']");
                Guid defaultAssignLoanOpenerID = Guid.Empty;
                if (null != xeLoanOpener)
                {
                    try
                    {
                        defaultAssignLoanOpenerID = new Guid(xeLoanOpener.InnerText);
                    }
                    catch { }
                }

                XmlElement xeLenderAccountExec = (XmlElement)doc.SelectSingleNode("SERVER_PROC/PARAM[@name='lenderAccountExecID']");
                Guid defaultAssignLenderAccountExecID = Guid.Empty;
                if (null != xeLenderAccountExec)
                {
                    try
                    {
                        defaultAssignLenderAccountExecID = new Guid(xeLenderAccountExec.InnerText);
                    }
                    catch { }
                }

                XmlElement xeCallCenterAgent = (XmlElement)doc.SelectSingleNode("SERVER_PROC/PARAM[@name='callCenterAgentID']");
                Guid defaultAssignCallCenterAgentID = Guid.Empty;
                if (null != xeCallCenterAgent)
                {
                    try
                    {
                        defaultAssignCallCenterAgentID = new Guid(xeCallCenterAgent.InnerText);
                    }
                    catch { }
                }

                XmlElement xeSellingAgent = (XmlElement)doc.SelectSingleNode("SERVER_PROC/PARAM[@name='sellingAgentID']");
                Guid defaultAssignRealEstateAgentID = Guid.Empty;
                if (null != xeSellingAgent)
                {
                    try
                    {
                        defaultAssignRealEstateAgentID = new Guid(xeSellingAgent.InnerText);
                    }
                    catch { }
                }

                XmlElement xeManager = (XmlElement)doc.SelectSingleNode("SERVER_PROC/PARAM[@name='managerID']");
                Guid defaultAssignManagerID = Guid.Empty;
                if (null != xeManager)
                {
                    try
                    {
                        defaultAssignManagerID = new Guid(xeManager.InnerText);
                    }
                    catch { }
                }

                if (0 == xeData.InnerText.Length)
                {
                    Response.Write(new ServerProcReturn(ServerProcStatus.ERROR, "Nothing to import.").ToString());
                    return;
                }

                NameValueCollectionWrapper input = GetNameValueData(xeData);

                // 09/09/02-Binh-Added this code to check for duplicate loans. If the user
                // does not want to overwrite the information contained in a loan file, then
                // I should skip it.

                CPointData loanData = null;

                if ("" != input.Shared["1"])
                {
                    // 10/4/2004 kb - We previously used a reader to load up a possibly
                    // existing loan file by the same name as what we want to import.
                    // When names match, we assume the files are the same -- for the
                    // most part.  In any event, the user has the option to overwrite
                    // the existing loan with the imported one.  We now check if the
                    // user has permission to do so.  If not, then we punt on this
                    // request.  Failing here shouldn't affect other file imports.

                    Object oId;

                    SqlParameter[] parameters = {
                                                    new SqlParameter( "@BrokerId"   , principal.BrokerId )
                                                    , new SqlParameter( "@LoanNumber" , input.Shared["1"]  )
                                                };
                    oId = StoredProcedureHelper.ExecuteScalar(principal.BrokerId, "IsLoanNumberExisting", parameters);

                    if (oId != null && oId is Guid == true)
                    {
                        if (bOverwrite) // if overwite is on
                        {
                            // 10/4/2004 kb - Check if importing user has permission to
                            // overwrite. If not, then punt with an error and skip it.
                            // We will try to be vocal about blocking overwriting on the
                            // client side.  **Note: if the importing user is a manager,
                            // then they can overwrite whatever they want.  To a point.

                            Guid loanId = new Guid(oId.ToString());

                            if (HasEditLoanPermission(loanId) == false)
                            {
                                Response.Write(new ServerProcReturn(ServerProcStatus.ERROR, "Access denied.  No permission to overwrite.").ToString());

                                return;
                            }

                            loanData = new CImportPointData(loanId);
                            bOverwritten = true;
                        }
                        else if (bForceCreate == false)
                        {
                            Response.Write(new ServerProcReturn(ServerProcStatus.ERROR, "Duplicate file found").ToString());

                            return;
                        }
                    }
                }

                CLoanFileCreator creator = null;

                // create new loan
                if (null == loanData)
                {
                    creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.PointImport);
                    Guid loanId = creator.BeginCreateImportBaseLoanFile(
                        sourceFileId: Guid.Empty,
                        setInitialEmployeeRoles: false,
                        addEmployeeOfficialAgent: false,
                        assignEmployeesFromRelationships: false,
                        branchIdToUse: Guid.Empty);

                    loanData = new CImportPointData(loanId);
                }

                loanData.InitSave(ConstAppDavid.SkipVersionCheck);

                if (bOverwritten)
                {
                    while (loanData.nApps > 1)
                    {
                        loanData.DelApp(1);
                        loanData.InitSave(ConstAppDavid.SkipVersionCheck);
                    }
                }

                // Transfer data from the field/value list into
                // the CPointData object.
                //
                // 8/3/2004 kb - Note that the transfer that takes place here
                // copies the point data's name into our loan data.  If we
                // commit without specifying the point file name, then we
                // will get what the commit generates: an auto name.  Import
                // preserves the name of the point record, so we need to
                // commit and force the name unless we are creating a new
                // loan file.

                PointDataConversion pointConversion = new PointDataConversion(principal.BrokerId, loanData.sLId);
                //E_sStatusT currentStatus = loanData.sStatusT;

                pointConversion.DefaultAssignLoanOfficerID = defaultAssignLoanOfficerID;
                pointConversion.DefaultAssignLoanProcessorID = defaultAssignLoanProcessorID;
                pointConversion.DefaultAssignLoanOpenerID = defaultAssignLoanOpenerID;
                pointConversion.DefaultAssignLenderAccountExecID = defaultAssignLenderAccountExecID;
                pointConversion.DefaultAssignCallCenterAgentID = defaultAssignCallCenterAgentID;
                pointConversion.DefaultAssignRealEstateAgentID = defaultAssignRealEstateAgentID;
                pointConversion.DefaultAssignManagerID = defaultAssignManagerID;

                pointConversion.TransferDataFromPoint(loanData, input, 0);

                #region COBORROWER FILES
                XmlNodeList xeCobDataList = doc.SelectNodes("SERVER_PROC/PARAM[@name='dataCoborrower']");
                for (int i = 0; i < xeCobDataList.Count; ++i)
                {
                    XmlElement xeCobData = (XmlElement)(xeCobDataList[i]);
                    if (0 == xeCobData.InnerText.Length)
                    {
                        Response.Write(new ServerProcReturn(ServerProcStatus.ERROR, "Error. An empty coborrower file encountered.").ToString());
                        return;
                    }

                    NameValueCollectionWrapper cobInput = GetNameValueData(xeCobData);

                    int iNewApp;

                    if (bForceCreate == true || bOverwritten == false && bRenameLoan == true)
                    {
                        loanData.SetsLNmWithPermissionBypass(creator.LoanName);
                    }

                    loanData.Save();

                    iNewApp = loanData.AddNewApp();

                    loanData.InitSave(ConstAppDavid.SkipVersionCheck);

                    pointConversion.TransferDataFromPoint(loanData, cobInput, iNewApp);
                }
                #endregion //COBORROWER FILES

                // 8/3/2004 kb - Save loan information.  We force the temp
                // name back into the loan data because the point transfer
                // overwrites it with what the point record holds.  To
                // prevent collisions before we commit, we need to save
                // with a temp, unique name.
                //
                // 9/7/2004 kb - Changes to how we auto name have allowed
                // us to name files after begin() and have it stick after
                // commit, so let the point transfer prevail.  Now, if
                // we are forcing a create, then we need the temp name
                // as it was retrieved just after beginning the new loan
                // file.

                if (bForceCreate == true || bOverwritten == false && bRenameLoan == true)
                {
                    loanData.SetsLNmWithPermissionBypass(creator.LoanName);
                }

                loanData.Save();

                if (null != creator)
                {
                    // Officially declare that this loan is valid.  We use
                    // the default commit, which generates an auto name for
                    // the new loan.

                    creator.CommitFileCreation(false);
                }

                // 8/3/2004 kb - Now that we have successfully committed the
                // new file or overwritten the existing target, we need to
                // send an email to the employees who got bumped on import.
                // The loan data we saved must be current after commit --
                // we need the loan's id.
                //
                // 8/10/2004 kb - In an effort to notify users of overwritten
                // loan files, we send out emails to the users who are nixed
                // by the new loan file.  This means that we queue up a note
                // for the overwrite event for the final user, and as well,
                // a note for the import action.  Both notes pertain to the
                // same loan file and same final user.  The import is sent
                // after the overwrite (if any) so that the email to the
                // final user will note the import event and not the overwrite
                // event.  The trickiness lies in that we are sending out 2
                // notes for the same role of a single loan.  The 2 notes
                // contradict, yet are processed in the same batch.  Last
                // one in the queue should win.
                //
                // 11/18/2004 kb - After sending out assignment change
                // events, we now send out status changes for overwrite.

                try
                {
                    // Sending out the message to the background processor
                    // is of secondary importance.  It shouldn't hold up
                    // the entire import.
                    //
                    // 11/5/2004 kb - New consolidated security for each
                    // save has imposed that we auto-assign when we create
                    // the new file so we can have a valid set of assignments
                    // with which we can pass the access control check for
                    // saving changes.

                    foreach (Guid roleId in pointConversion.RoleChanges.Keys)
                    {
                        PointDataConversion.RoleChangeRecord rcR = pointConversion.RoleChanges[roleId] as PointDataConversion.RoleChangeRecord;

                        if (rcR == null)
                        {
                            continue;
                        }

                        if (bOverwritten == true)
                        {
                            RoleChange.Mark
                                (principal.EmployeeId
                                , principal.BrokerId
                                , loanData
                                , rcR.Current
                                , rcR.Changed
                                , roleId
                                , E_RoleChangeT.Overwrite
                                );
                        }

                        RoleChange.Mark
                            (principal.EmployeeId
                            , principal.BrokerId
                            , loanData
                            , rcR.Default
                            , rcR.Changed
                            , roleId
                            , E_RoleChangeT.Import
                            );
                    }
                }
                catch (Exception e)
                {
                    Tools.LogError(e);
                }

                //if( currentStatus != loanData.sStatusT && bOverwrite == true )
                //{
                //// 11/18/2004 kb - We now track status changes and
                //// email out to the lender account exec and loan
                //// officer if the broker option is enabled.
                ////
                //// 8/26/2005 kb - For now, we send to all assigned
                //// employees to make sure all are covered.  A new
                //// notification subscription framework that's in
                //// the works will obsolete this.  This loop will
                //// get obsoleted by a revised notification subscription
                //// config module (see case 1800).

                //LoanAssignments loanEmp = new LoanAssignments();
                //ArrayList       empList = new ArrayList();

                //try
                //{
                //    loanEmp.Retrieve( loanData.sLId );
                //}
                //catch( Exception e )
                //{
                //    Tools.LogError( e );
                //}

                //foreach( LoanAssignments.Spec lS in loanEmp )
                //{
                //    if( lS.RoleName != "Administrator" )
                //    {
                //        if( empList.Contains( lS.EmployeeId ) == false )
                //        {
                //            empList.Add( lS.EmployeeId );
                //        }
                //    }
                //}

                //foreach( Guid sendEmployeeId in empList )
                //{
                //    try
                //    {
                //        LoanStatusChanged loanEvt = new LoanStatusChanged();

                //        loanEvt.Initialize
                //            ( loanData.sStatusT_rep
                //            , BrokerUser.DisplayName
                //            , sendEmployeeId
                //            , loanData.sLId
                //            , BrokerUser.ApplicationType
                //            );

                //        loanEvt.Send();
                //    }
                //    catch( Exception e )
                //    {
                //        Tools.LogError( e );
                //    }
                //}
                //int nCount = loanData.GetAgentRecordCount();
                //ArrayList agentEmailList = new ArrayList();
                //for (int i = 0; i < nCount; i++) 
                //{
                //    try 
                //    {
                //        CAgentFields agent = loanData.GetAgentFields(i);

                //        if (!agent.IsNotifyWhenLoanStatusChange)
                //            continue;

                //        if (agentEmailList.Contains(agent.EmailAddr.ToLower()))
                //            continue;

                //        agentEmailList.Add(agent.EmailAddr.ToLower());

                //        LoanStatusChanged loanEvt = new LoanStatusChanged();
                //        loanEvt.InitializeWithAgent(loanData.sStatusT_rep, BrokerUser.DisplayName, agent.RecordId, BrokerUser.BrokerId, loanData.sLId, BrokerUser.ApplicationType);
                //        loanEvt.Send();
                //    } 
                //    catch (Exception e) 
                //    {
                //        Tools.LogError(e);
                //    }
                //}
                //}

                // Return the result.

                if (bOverwritten)
                {
                    Response.Write
                        (new ServerProcReturn(ServerProcStatus.OK, "Existing file overwritten.").ToString()
                        );
                }
                else
                {
                    Response.Write
                        (new ServerProcReturn(ServerProcStatus.OK, "New file import successful.").ToString()
                        );
                }
            }
            catch (PageDataSaveDenied e)
            {
                Tools.LogError(e);
                Response.Write(new ServerProcReturn(ServerProcStatus.ERROR, "Import failed. Save denied.").ToString());
            }
            catch (AccessDenied e)
            {
                Tools.LogError(e);
                Response.Write(new ServerProcReturn(ServerProcStatus.ERROR, "Import failed. Access denied.").ToString());
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                Response.Write(new ServerProcReturn(ServerProcStatus.ERROR, "Failed.").ToString());
            }
        }

        /// <summary>
        /// Test the loan to see if the importing user can overwrite.
        /// This test must stay in sync with what we use for the los
        /// app in all of its loan edit pages.  A consolidated design
        /// for access control should show up soon.
        /// </summary>
        /// <returns>
        /// Returns true if the current employee has permission.
        /// </returns>

        private bool HasEditLoanPermission(Guid loanID)
        {
            // 10/4/2004 kb - This check should match 1:1 with the base
            // loan page implementation of the same name!  I'm not happy
            // with how these 2 are separate, but I plan to have a more
            // generic, easier to maintain approach, based on Thinh's
            // design for loan program sharing, in place soon.  This is
            // certainly a fast way to go to get up and running, but
            // access control is becoming increasingly more important
            // and we don't have the manpower to maintain disparate
            // implementations.  Expect consolidation soon.
            //
            // 11/5/2004 kb - And it has arrived.
            // 9/26/2010 dd - Hook in new engine.

            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(BrokerUser.BrokerId, loanID, WorkflowOperations.WriteLoanOrTemplate);
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(BrokerUser));
            bool canWrite = LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteLoanOrTemplate, valueEvaluator);

            if (canWrite == true)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Save the Point index file into a temporary file. Return
        /// the temporary filename.
        /// </summary>
        /// <param name="doc">
        /// </param>

        void CacheIndexFile(XmlDocument doc)
        {
            XmlElement xeContent = (XmlElement)doc.SelectSingleNode("SERVER_PROC/PARAM[@name='index_file_content']");
            if (null != xeContent)
            {
                // * Save the point data to a file
                // make a temp filename
                string sFilePath = TempFileUtils.NewTempFilePath();
                string sFileName = Path.GetFileName(sFilePath);

                if (0 == xeContent.InnerText.Length)
                {
                    Response.Write(new ServerProcReturn(ServerProcStatus.ERROR, "Nothing to import.").ToString());
                    return;
                }

                // decode the data
                byte[] bytes = Convert.FromBase64String(xeContent.InnerText);

                // write the data to the temporary file
                BinaryFileHelper.WriteAllBytes(sFilePath, bytes);

                // * Parse the point data from the file and convert it into an XML document
                // convert the data from Point to XML format
                PointInteropLib.IndexFile reader = new PointInteropLib.IndexFile();
                reader.Convert2XML(sFilePath, sFilePath);

                // return the temp filename
                Response.Write(new ServerProcReturn(ServerProcStatus.OK, "OK", sFileName).ToString());
            }
        }
        void UploadTempFile(XmlDocument doc)
        {
            XmlElement xeData = (XmlElement)doc.SelectSingleNode("SERVER_PROC/PARAM[@name='bin-data']");
            if (null == xeData)
                xeData = (XmlElement)doc.SelectSingleNode("SERVER_PROC/PARAM[@name='text-data']");
            if (null == xeData)
                Response.Write(new ServerProcReturn(ServerProcStatus.ERROR, "The parameter 'text-data' or 'bin-data' is missing.").ToString());

            // * Save the point data to a file
            // make a temp filename
            string sFilePath = TempFileUtils.NewTempFilePath();
            string sFileName = Path.GetFileName(sFilePath);

            // decode the data
            byte[] bytes;
            if (xeData.GetAttribute("name") == "bin-data")
                bytes = Convert.FromBase64String(xeData.InnerText);
            else
                bytes = Encoding.ASCII.GetBytes(xeData.InnerText);

            // write the data to the temporary file
            BinaryFileHelper.WriteAllBytes(sFilePath, bytes);

            // return the temp filename
            Response.Write(new ServerProcReturn(ServerProcStatus.OK, "OK", sFileName).ToString());
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);

        }
        #endregion
    }

    public enum ServerProcStatus { OK, ERROR };
    public class ServerProcReturn
    {
        ServerProcStatus m_eStatus;
        string m_sStatusMessage;
        string m_sReturnValue;
        public ServerProcReturn(ServerProcStatus eStatus, string sStatusMessage)
        {
            m_eStatus = eStatus;
            m_sStatusMessage = sStatusMessage;
            m_sReturnValue = "";
        }
        public ServerProcReturn(ServerProcStatus eStatus, string sStatusMessage, string sReturnValue)
        {
            m_eStatus = eStatus;
            m_sStatusMessage = sStatusMessage;
            m_sReturnValue = sReturnValue;
        }
        public new string ToString()
        {
            MemoryStream stream = new MemoryStream();


            XmlWriter writer = new XmlTextWriter(stream, Encoding.ASCII);
            writer.WriteStartDocument();
            writer.WriteStartElement("STATUS");

            // CODE
            writer.WriteStartElement("CODE");
            switch (m_eStatus)
            {
                case ServerProcStatus.OK:
                    writer.WriteString("OK");
                    break;
                case ServerProcStatus.ERROR:
                    writer.WriteString("ERROR");
                    break;
            }
            writer.WriteEndElement();

            // MESSAGE
            writer.WriteStartElement("MESSAGE");
            writer.WriteString(m_sStatusMessage);
            writer.WriteEndElement();

            // RETURN
            writer.WriteStartElement("RETURN");
            writer.WriteString(m_sReturnValue);
            writer.WriteEndElement();

            writer.WriteEndElement();   // status
            writer.WriteEndDocument();
            writer.Close();

            // get the XML result
            string ret = Encoding.ASCII.GetString(stream.ToArray());
            stream.Close();
            return ret;
        }
    }

}
