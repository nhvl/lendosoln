<%
' current assumptions
' 1: Import will create new loans (no dup checking)
' 2: 
'
'

' global vars
const DEBUG_MODE = false
dim WORKING_FOLDER		' init in main
dim IMPORT_HISTORY_FILE	' init in main

const PROC_CACHE_INDEX_FILE = "CacheIndexFile"
const PROC_UPLOAD_TEMP_FILE = "UploadTempFile"
const PROC_IMPORT_FILE = "ImportFile"
const PROC_RETURN_OK = "OK"
const PROC_RETURN_ERROR = "ERROR"

' global vars
' app -- explorer window
' approot - the root of the application. e.g., "/" or "/lendersofficeapp/"
' serverroot - the root of the server + application. e.g., "http://www.lendersoffice.com/lendersofficeapp/"

dim g_oImporter: set g_oImporter = new cFileImporter
dim g_oReg: set g_oReg = new cRegistry

' exec main
main

sub main
	dim oFS: set oFS = CreateObject("Scripting.FileSystemObject")
	g_oReg.OpenKey "HKEY_LOCAL_MACHINE\Software\MeridianLink\LOSAgent"
	
	dim sDefaultPath: sDefaultPath = g_oReg.ReadValue("pointdbpath")
	
	WORKING_FOLDER = oFS.GetAbsolutePathName(".")
	IMPORT_HISTORY_FILE = WORKING_FOLDER & "\ImportHistory.xml"
	
	' redirect the user to the login page when he first logs in
	RedirectView MapLegacyPath("CalyxPoint/Login.aspx?ReturnURL=" & URLEncode(MapLegacyPath("CalyxPoint/BrowseFile.aspx?path=" & sDefaultPath)))
end sub



' ****************************************
' EVENT HANDLERS
' ****************************************
' executes BEFORE a URL navigation begins. This method is executed
' only on "event.asp" method calls.
function OnBeforeNavDispatcher(sParam, sPOST)
	dim cmd : cmd = parseQueryString(sParam ,"cmd")
	select case cmd
		case "BrowseFile.Refresh.Silent"
			BrowseFile_Refresh sParam, true
		case "BrowseFile.Refresh"
			BrowseFile_Refresh sParam, false
		case "BrowseFile.Import"
			BrowseFile_Import sParam
		case "BrowseFile.CancelImport"
			BrowseFile_CancelImport sParam
		case else
			TRACE "ERR", "ClientCode::Unknown command: " & sParam
	end select
end function

' executes when a URL navigation completes
function OnNavigateCompleteDispatcher(sParam)
end function

' This member function is called by the framework to notify an application that a 
' document has reached the READYSTATE_COMPLETE</B> state. 
function OnDocumentCompleteDispatcher(sParam)
end function

' executes when IE begins a download
function OnDownloadBeginDispatcher()
end function

' executes when IE completes a download
function OnDownloadCompleteDispatcher()
end function

' executes when the AgentLink shuts down
function OnQuitDispatcher
end function


' ****************************************
' OnBeforeNavDispatcher Functions
' ****************************************
sub BrowseFile_Refresh(sParam, bSilent)
	dim oFS: set oFS = createobject("Scripting.FileSystemObject")
	dim sPath: sPath = parseQueryString(sParam, "path")
	
	if not oFS.FolderExists(sPath) then
		if not bSilent then
			MsgBox "The path you specified is invalid or cannot be accessed.", vbExclamation, "LegacyLink"
		end if
		exit sub
	end if
	
	dim oFolder: set oFolder = oFS.GetFolder(sPath)
	dim sIndexFile: sIndexFile = oFS.GetParentFolderName(sPath) & "\" & oFolder.Name & ".IDX"
	if not oFS.FileExists(sIndexFile) then
		if not bSilent then
			MsgBox "The index file does not exist or this is not a valid Calyx Point database folder.", vbExclamation, "LegacyLink"
		end if
		
		exit sub
	end if
	
	dim oBinFile: set oBinFile = CreateObject("MLTools.BinFile")
	dim oProc: set oProc = new cServerProc
	oProc.ProcName = PROC_CACHE_INDEX_FILE
	oProc.AddBinaryParam "index_file_content", oBinFile.OpenBinaryFile(sIndexFile)
	
	' reload the file list and initialize its content with data from the
	' input filename
	dim oProcReturn: set oProcReturn = new cServerProcReturn
	oProcReturn.Init oProc.Execute
	if oProcReturn.StatusCode = PROC_RETURN_OK then
		dim sFileName: sFileName = oProcReturn.ReturnValue
		RedirectView MapLegacyPath("CalyxPoint/BrowseFile.aspx?cmd=loadindex&path=" & Escape(sPath) & "&indexfile=" + sFileName)
	else
		MsgBox "Cannot upload index file to the server: " & oProcReturn.StatusMessage, vbCritical, "LegacyLink"
		CloseLegacyLink
	end if
end sub

sub BrowseFile_Import(sParam)
	dim sPath: sPath = parseQueryString(sParam, "path")
	if Right(sPath, 1) = "\" then sPath = Left(sPath, Len(sPath) - 1)	' strip out trailing slash
	
	dim rgFiles: rgFiles = Split(parseQueryString(sParam, "files"), ";")
	dim bOverwrite: bOverwrite = CBool(parseQueryString(sParam, "overwrite"))
	
	' save the new point path in the db
	g_oReg.WriteValue "pointdbpath", sPath

	g_oImporter.StartImporting sPath, rgFiles, bOverwrite
end sub

sub BrowseFile_CancelImport(sParam)
	g_oImporter.StopImporting
end sub

' ****************************************
' UTILITY FUNCTIONS
' ****************************************

' redirect the explorer window to a particular URL
sub RedirectView(sURL)
	app.document.parentWindow.RedirectView sURL
end sub

sub CloseLegacyLink
	RedirectView "closeapp.asp"
end sub

function MapLegacyPath(sSubPath)
	MapLegacyPath = AppRoot & "LegacyLink/" & sSubPath
end function

' encodes a URL similar to server.URLEncode
function URLEncode(sURL)
	URLEncode = Escape(sURL)
end function

' parse the query string for a request variable
function parseQueryString(sParam, sName)
	dim n1, n2
	n1 = InStr(sParam, sName & "=")
	if n1 > 0 then
		n1 = n1 + Len(sName) + 1		
		n2 = InStr(n1, sParam, "&")
		if n2 = 0 then
			parseQueryString = Mid(sParam, n1)
		else
			parseQueryString = Mid(sParam, n1, n2-n1)
		end if
	else
		parseQueryString = ""
	end if
end function

dim g_oPB
function TRACE(sType, sMsg)
	if not DEBUG_MODE then exit function
	
	if IsEmpty(g_oPB) then
		set g_oPB = CreateObject("PB.Logger.1")
		g_oPB.Component = "ClientCode.asp"
	end if
	g_oPB.Log sType, sMsg
end function

' Tells the script to halt program execution. For debugging purposes ONLY!
sub DebugWait(nSeconds)
	dim oWait: set oWait = CreateObject("MCLUtils.CWait")
	oWait.Wait nSeconds
end sub

class cServerProc
	public property let ProcName(sProcName)
		m_xd.documentElement.setAttribute "name", sProcName
	end property
	
	public sub AddTextParam(sName, sValue)
		dim xeParam: set xeParam = m_xd.createElement("PARAM")
		xeParam.setAttribute "name", sName
		xeParam.text = sValue
		m_xd.documentElement.appendChild xeParam
	end sub
	
	public sub AddBinaryParam(sName, sValue)
		dim xeParam: set xeParam = m_xd.createElement("PARAM")
		
		' we use bin.base64 and not bin.hex because it's easier to
		' convert base64 than hex on the server side.
		xeParam.setAttribute "name", sName
		xeParam.dataType = "bin.base64"
		xeParam.nodeTypedValue = sValue
		
		m_xd.documentElement.appendChild xeParam
	end sub
	
	public function Execute
		' the call is delegated to the window frame so that session state
		' will be maintained
		Execute = app.document.parentWindow.HttpPostDoc("POST", MapLegacyPath("CalyxPoint/ServerProc.aspx"), m_xd)
		
		TRACE "VER", "Execute Result: " & Execute
		
		' reset the function parameters
		m_xd.documentElement = m_xd.createElement("SERVER_PROC")
	end function
	
	public sub Class_Initialize
		set m_xd = CreateObject("MSXML2.DOMDocument.3.0")
		m_xd.documentElement = m_xd.createElement("SERVER_PROC")
	end sub
	
	private m_xd
end class

class cFileImporter
	' import Point files
	public sub StartImporting(sPath, rgFiles, bOverwrite)
		' do not start new import if current one is still running
		if m_bImporting then
			MsgBox "Currently importing files"
			exit sub
		end if

		m_bImporting = true

		' import files in a loop
		dim oProc: set oProc = new cServerProc
		dim oProcReturn: set oProcReturn = new cServerProcReturn
		dim oHistory: set oHistory = new cImportHistory
		dim oBinFile: set oBinFile = CreateObject("MLTools.BinFile")
		dim oFS: set oFS = CreateObject("Scripting.FileSystemObject")
		dim i
		for i = LBound(rgFiles) to UBound(rgFiles)
			dim sFileName: sFileName = rgFiles(i)
			
			if i mod 10 = 0 then
				' show waiting view
				dim sMessage: sMessage = "Importing Point File #" & i+1 & " out of " & UBound(rgFiles) + 1
				dim sCancelURL: sCancelURL = "event.asp?cmd=BrowseFile.CancelImport"
				RedirectView MapLegacyPath("Processing.aspx?message=" & Escape(sMessage) & "&cancelURL=" & Escape(sCancelURL))
			end if
			
			dim sPathName
			sPathName = sPath & "\" & sFileName
			TRACE "VER", "Importing " & sPathName
			
			if oFS.FileExists(sPathName) then
				' TODO: Here is the place to pull the CB files.
				oProc.ProcName = PROC_IMPORT_FILE
				oProc.AddBinaryParam "data", oBinFile.OpenBinaryFile(sPathName)
				oProc.AddTextParam "overwrite", IIF(bOverwrite, "T", "F")
				if Len( sFileName ) > 4 then
					dim iCob
					'asd = iCob
					for iCob = 1 to 5
						dim sCobFileName
						sCobFileName = sPath & "\" & Left( sFileName, Len( sFileName ) - 3 ) & "CB" & iCob
						
						if oFS.FileExists( sCobFileName ) then
							oProc.AddBinaryParam "dataCoborrower", oBinFile.OpenBinaryFile( sCobFileName )
						end if
					next
				end if	
				oProcReturn.Init oProc.Execute

				TRACE "VER", oProcReturn.StatusMessage

				oHistory.AddEntry sPathName, oProcReturn.StatusCode, oProcReturn.StatusMessage
			else
				oHistory.AddEntry sPathName, PROC_RETURN_ERROR, "File does not exist on client computer."
			end if
			
			if not m_bImporting then
				MsgBox "Importing canceled.", vbInformation, "LegacyLink"
				exit for
			end if
		next
		
		m_bImporting = false
		
		oProc.ProcName = PROC_UPLOAD_TEMP_FILE
		oProc.AddTextParam "text-data", oHistory.SessionData
		oProcReturn.Init oProc.Execute
		
		if oProcReturn.StatusCode = PROC_RETURN_OK then
			RedirectView MapLegacyPath("ImportSummary.aspx?filename=" & oProcReturn.ReturnValue)
		else
			MsgBox "Import success but error occurred trying to load ImportSummary", vbCritical, "LegacyLink"
			CloseLegacyLink
		end if
	end sub
	
	' tell the StartImporting loop to quit
	public sub StopImporting
		' no need to cancel if not importing
		if not m_bImporting then exit sub
		
		' get importation confirmation.
		if vbYes = MsgBox("Cancel importation?", vbYesNo, "LegacyLink") then
			m_bImporting = false
		end if
	end sub
	
	public sub Class_Initialize
		m_bImporting = false
	end sub

	dim m_bImporting
end class

class cImportHistory
	private m_xdHistory
	private m_xeSession
	
	public sub Class_Initialize
		set m_xdHistory = CreateObject("MSXML2.DOMDocument.3.0")
		dim oFS: set oFS = CreateObject("Scripting.FileSystemObject")
		if oFS.FileExists(IMPORT_HISTORY_FILE) then
			if not m_xdHistory.load(IMPORT_HISTORY_FILE) then
				err.Raise -1, "ImportHistory", "Cannot load import history file."
			end if
		else
			set m_xdHistory.documentElement = m_xdHistory.createElement("HISTORY")
		end if
		
		set m_xeSession = m_xdHistory.createElement("SESSION")
		m_xdHistory.documentElement.appendChild m_xeSession
	end sub
	
	public sub AddEntry(sFileName, sStatusCode, sStatusMessage)
		dim xe
		set xe = m_xdHistory.createElement("ENTRY")
		
		xe.setAttribute "filename", sFileName
		xe.setAttribute "status_code", sStatusCode
		xe.setAttribute "status_message", sStatusMessage
		
		m_xeSession.appendChild xe
	end sub
	
	public function SessionData
		SessionData = m_xeSession.xml
	end function
	
	public sub Class_Terminate
		m_xdHistory.save IMPORT_HISTORY_FILE
	end sub
end class

class cServerProcReturn
	private m_xd
	
	public sub Init(s)
		TRACE "VER", s
		
		if IsEmpty(m_xd) then
			set m_xd = CreateObject("MSXML2.DOMDocument.3.0")
		end if
		
		if not m_xd.loadXml(s) then
			m_xd.loadxml "<STATUS><CODE>ERROR</CODE><MESSAGE>UNKNOWN SERVER ERROR</MESSAGE></STATUS>"
		end if
	end sub
	
	public property get StatusCode
		if IsEmpty(m_xd) then
			StatusCode = "ERROR"
		else
			StatusCode = m_xd.documentElement.selectSingleNode("CODE").Text
		end if
	end property
	
	public property get StatusMessage
		if IsEmpty(m_xd) then
			StatusMessage = "ServerProcReturn not initialized."
		else
			StatusMessage = m_xd.documentElement.selectSingleNode("MESSAGE").Text
		end if
	end property
	
	public property get ReturnValue
		if IsEmpty(m_xd) then
			ReturnValue = "ServerProcReturn not initialized."
		else
			ReturnValue = m_xd.documentElement.selectSingleNode("RETURN").Text
		end if
	end property
end class

class cRegistry
'	private m_oShell
'	private m_sKey
'
'	public sub OpenKey(sKey)
'		set m_oShell = CreateObject("WScript.Shell")
'		m_sKey = sKey
'	end sub
'	
'	public function ReadValue(sName)
'		ReadValue = m_oShell.RegRead(m_sKey & "\" & sValue)
'	end function

	private m_oRegDLL
	private m_sKey

	public sub OpenKey(sKey)
		set m_oRegDLL = CreateObject("IISSample.RegistryAccess")
		m_sKey = sKey
	end sub

	public function ReadValue(sName)
		dim entryList: set entryList = m_oRegDLL.Values(m_sKey) 
		dim entry
		for each entry in EntryList
			if entry = sName then
				ReadValue = entry.value
				exit function
			end if
		next	
		
		ReadValue = ""
	end function
	
	public function WriteValue(sName, sValue)
		on error resume next
		m_oRegDLL.set m_sKey & "\" & sName, sValue, false
		on error goto 0
	end function
end class

function IIF(cond, t, f)
	if cond then
		IIF = t
	else
		IIF = f
	end if
end function

' ****************************************
' CONSTANTS
' ****************************************
' OpenTextStream params
const ForReading = 1
const ForWriting = 2
const ForAppending = 8

' buttons for the MsgBox function
'const vbOKOnly = 0 			' Display OK button only. 
'const vbOKCancel = 1 		' Display OK and Cancel buttons. 
'const vbAbortRetryIgnore = 2 ' Display Abort, Retry, and Ignore buttons. 
'const vbYesNoCancel = 3 	' Display Yes, No, and Cancel buttons. 
'const vbYesNo = 4 			' Display Yes and No buttons. 
'const vbRetryCancel = 5 	' Display Retry and Cancel buttons. 
'const vbCritical = 16		'  Display Critical Message icon.  
'const vbQuestion = 32		'  Display Warning Query icon. 
'const vbExclamation = 48	'  Display Warning Message icon. 
'const vbInformation = 64	'  Display Information Message icon. 
'const vbDefaultButton1 = 0 	' First button is default. 
'const vbDefaultButton2 = 25	' 6 Second button is default. 
'const vbDefaultButton3 = 51	' 2 Third button is default. 
'const vbDefaultButton4 = 76	' 8 Fourth button is default. 
'const vbApplicationModal = 0 ' Application modal; the user must respond to the message box before continuing work in the current application. 
'const vbSystemModal = 40	' 96 System modal; all applications are suspended until the user responds to the message box.

' return values for the MsgBox function
'const vbOK = 1 		' OK 
'const vbCancel = 2 	' Cancel 
'const vbAbort = 3 	' Abort 
'const vbRetry = 4 	' Retry 
'const vbIgnore = 5 	' Ignore 
'const vbYes = 6 	' Yes 
'const vbNo = 7 		' No 
%>
