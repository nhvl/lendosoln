using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOfficeApp.common;
using DataAccess;
using LendersOffice.Common;
using System.Linq;

namespace LendersOfficeApp.LegacyLink.CalyxPoint
{
    public partial class ExportOptions : LendersOfficeApp.newlos.BaseLoanPage
	{
		protected void PageLoad(object sender, System.EventArgs e)
		{

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(ExportOptions));
            dataLoan.InitLoad();
            FileName.Value =  dataLoan.sLNm;
		}

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageID = "ExportPoint";
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion
	}
}
