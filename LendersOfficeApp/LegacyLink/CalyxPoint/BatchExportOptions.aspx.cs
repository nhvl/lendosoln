using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;

namespace LendersOfficeApp.LegacyLink.CalyxPoint
{
	/// <summary>
	/// Summary description for BatchExportOptions.
	/// </summary>
	public partial class BatchExportOptions : LendersOffice.Common.BasePage
	{
    
		protected void PageLoad(object sender, System.EventArgs e)
		{
			// 06/02/06 mf - OPM 2583.  We do not use server cache anymore.
			// We pull the list of loans from our custom cache.
            RegisterJsGlobalVariables("cacheId", RequestHelper.GetSafeQueryString("id"));
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
