<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="ExportOptions.aspx.cs" validateRequest="true" AutoEventWireup="false" Inherits="LendersOfficeApp.LegacyLink.CalyxPoint.ExportOptions" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import namespace="LendersOffice.Security"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE>
<HTML>
	<HEAD runat="server">
		<title>ExportOptions</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<style>
		.userData { BEHAVIOR: url(#default#userdata) }

        label {
            display: inline-block;
            font-weight: bold;
            width: 100px;
        }

        #FileLinkContainer {
            display: inline-block;
            width: 400px;
        }

        .applicant-link {
            color: blue;
            display: inline-block;
            cursor: pointer;
            padding: 3px;
        }
        
        .MainRightHeader, .form-content div {
            margin-bottom: 5px;
        }

		</style>
	</HEAD>
	<body MS_POSITIONING="FlowLayout" class="EditBackground" scroll="no">
		<script language="JavaScript" type="text/javascript">

		function _init()
		{
			try
			{
				<% if (!BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanExportLoan)) { %>   
			    return;
				<% } %>  		
			}
			catch( e )
			{
				alert( <%=AspxTools.JsString(ErrorMessages.Generic)%> );
			}
		}


		    function Export(event) {
		        var anchor = document.createElement("a");
		        var fileName = document.getElementById("FileName").value;
		        var extension = document.getElementById("BRW").checked ? "BRW": "PRS";
		        anchor.setAttribute("download", fileName);
		        anchor.href = gVirtualRoot + "/LegacyLink/CalyxPoint/CalyxPointExport.aspx?" 
                    + "filename=" + encodeURIComponent(fileName)
                    + "&extension=" + extension
                    + "&loanid=" + <%= AspxTools.JsString(LoanID.ToString()) %>; 
		        document.body.appendChild(anchor);
		        anchor.click();
		        document.body.removeChild(anchor);
		        document.getElementById("WaitMessage").className = "";
            }
        </script>
	
		<form id="ExportOptions" method="post" runat="server">
		    <div nowrap colspan="2" class="MainRightHeader">Export to Calyx Point</div>

            <div class="form-content">
                <div>
                    Please select the data folder and enter the file name.&nbsp; If 
						Calyx Point is installed on your system, you will need to <a href="https://support.lendingqb.com/index.php?/Knowledgebase/Article/View/229" target="_blank" title="Click here for help">rebuild the file list</a> in Calyx Point after the export is complete.
                <input type="hidden" id="config" name="config" class="userData">
                </div>

                <div>
                    <label>File Name</label>
                    <input runat="server" type="text" id="FileName" />
                </div>
                <div>
                    <label>File Type</label>
                    <input type="radio" name="fileType" id="BRW" value="BRW" checked /><label for="BRW">Borrower File</label>
                    <input type="radio" name="fileType" id="PRS" value="PRS" /><label for="PRS">Prospect File</label>
                </div>
                <div>
                    <button type="button" onclick="Export()">Export</button>
                    <span class="Hidden" id="WaitMessage">Please wait while the zip file is prepared.</span>
                </div>
            </div>
		</form>
	</body>
</HTML>
