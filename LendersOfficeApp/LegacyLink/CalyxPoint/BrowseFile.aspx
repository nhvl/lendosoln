<%@ Page language="c#" Codebehind="BrowseFile.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LegacyLink.CalyxPoint.BrowseFile" %>
<%@ Import Namespace="LendersOffice.AntiXss"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>BrowseFile</title>
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<script type="text/javascript">
		function RefreshClick()
		{
			if ("" == document.BrowseFile.txtPath.value)
			{
				alert('Please enter a path.') ;
				return ;
			}
			location.href = "event.asp?cmd=BrowseFile.Refresh&path=" + document.BrowseFile.txtPath.value ;
		}
		function CloseProgram()
		{
			if (confirm('Quit program?'))
				location.href = "closeapp.asp" ;
		}
		function OnLoad()
		{
			var sFileNames = "<%=AspxTools.JsStringUnquoted(ImportFileNames)%>" ;
			if ("" != sFileNames)
				location.href = "event.asp?cmd=BrowseFile.Import&path=" + document.BrowseFile.txtPath.value + "&files=" + sFileNames + "&overwrite=<%=AspxTools.JsBool(cbOverwrite.Checked)%>" ;
		}
		function SetCheckbox(cb)
		{
			for (var i = 0 ; i < CheckBoxes.length ; i++)
			{
				eval('document.getElementById(' + CheckBoxes[i] + ').checked = ' + cb.checked) ;
			}
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="FlowLayout" onload="OnLoad();">
		<form id="BrowseFile" method="post" runat="server">
			<TABLE cellSpacing="5" border="0" width="100%">
				<TR>
					<td class="FormTable">
						&nbsp; <font class="FieldLabel">Path</font>
						<asp:TextBox id="txtPath" runat="server" Columns="50"></asp:TextBox>
						<input type="button" value="Show Files" onclick="RefreshClick();"> <INPUT type="button" value="Close" onclick="CloseProgram();">
						<hr>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:CheckBox id="cbOverwrite" runat="server" Text="Overwite existing loan files"></asp:CheckBox>
						&nbsp;&nbsp;
						<asp:Button id="btnImport" runat="server" Text="Import" onclick="btnImport_Click"></asp:Button>
						<br>
						<asp:DataGrid id="dgFileList" runat="server" AutoGenerateColumns="False" Width="100%" AllowSorting="True" DataKeyField="FileName">
							<SelectedItemStyle CssClass="GridSelectedItem"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="GridAlternatingItem"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem"></ItemStyle>
							<HeaderStyle CssClass="GridHeader"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn>
									<HeaderTemplate>
										<center>
											<asp:CheckBox id="cbSelectHeader" runat="server" Enabled="False"></asp:CheckBox>
										</center>
									</HeaderTemplate>
									<ItemTemplate>
										<CENTER>
											<asp:CheckBox id="cbSelect" runat="server"></asp:CheckBox></CENTER>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="FileName" SortExpression="FileName" HeaderText="File Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="FirstName" SortExpression="FirstName" HeaderText="First Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="LastName" SortExpression="LastName" HeaderText="Last Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="ContactDate" HeaderText="Contact Date"></asp:BoundColumn>
								<asp:BoundColumn DataField="RepName" SortExpression="RepName" HeaderText="Rep Name"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
