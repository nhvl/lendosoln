<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="BatchExportOptions.aspx.cs" validateRequest="true" AutoEventWireup="false" Inherits="LendersOfficeApp.LegacyLink.CalyxPoint.BatchExportOptions" %>
<%@ Import Namespace="LendersOffice.AntiXss"%>
<!doctype HTML>
<html>
<head>
    <title>POINT Batch Export</title>
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
    <style>
        .userData { BEHAVIOR: url(#default#userdata) }
    </style>
    <script language="javascript" src="../ServerProc.js"></script>
    <script language=javascript>
        function _init() {
            resize(650, 400);
        }

        function Export() {
            var anchor = document.createElement("a");
            anchor.href = gVirtualRoot + "/LegacyLink/CalyxPoint/CalyxPointExport.aspx?"
                + "id=" + ML.cacheId
                + "&isBatch=" + true
                + "&useBorrName=" + document.getElementById("fileNameOption_1").checked;

            document.body.appendChild(anchor);
            anchor.click();
            document.body.removeChild(anchor);
            document.getElementById("WaitMessage").className = "";
        }
    </script>
</head>
<body ms_positioning="FlowLayout" class="EditBackground">
    <h4 class="page-header">POINT Batch Export</h4>
    <form id="BatchExportOptions" method="post" runat="server" class="little-indent">
        <table id="Table1" cellspacing="0" cellpadding="5" border="0">
            <tr>
                <td class="fieldlabel" valign="top">FILE NAME</td>
                <td nowrap>
                    <asp:RadioButtonList ID="fileNameOption" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
                        <asp:ListItem Value="0" Selected="True">Use loan number</asp:ListItem>
                        <asp:ListItem Value="1">Use borrower's name and loan number (First_Last_LoanNumber)</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
            <td></td>
                <td>
                    <input type="button" id="btnStartExport" onclick="Export()" name="export" nohighlight value="Start Export">
                    <input type="button" id="btnClose" onclick="onClosePopup();" value="Close" nohighlight>
                    <span class="Hidden" id="WaitMessage">Please wait while the zip file is prepared.</span>
                </td>
            </tr>
        </table>
        <br>
        <b>Note</b>: &nbsp;You may need to rebuild Point's file list before Point will recognize the exported files -- <a href="/help/faq/IexportedloanstoPointbutP.aspx" target="_blank">click here</a> for instructions.
        <uc1:cModalDlg ID="CModalDlg1" runat="server" />
    </form>

</body>
</html>
