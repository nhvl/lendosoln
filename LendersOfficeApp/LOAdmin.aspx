<%@ Page language="c#" Codebehind="LOAdmin.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdminLoginPage" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc1" TagName="LoginUserControl" Src="LoginUserControl.ascx" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>LOAdmin</title>
</head>
<body onload="init();">
<script type="text/javascript">
	function init()
	{
		_initFrameAllow();

		var beu = document.getElementById( <%= AspxTools.JsGetClientIdString( m_BecomeUser ) %>);
		var body = document.getElementsByTagName('body');
		var beub = document.getElementById(<%= AspxTools.JsGetClientIdString( m_BecomeUserButton ) %>); 
		var login = document.getElementById( <%= AspxTools.JsString( m_LoginUserControl1.GetLoginButtonClientID() ) %>);
		if ( beu ) 
		{
			body[0].onkeypress = function(pressEvent) {
				var eventObject = pressEvent || window.event;
				if ( eventObject.keyCode != 13 ) return true;
				if (beu.value != "" ) {
					beub.click();
				}
				else  
				{
					login.click();  
				}
				return false;
			};
		}

	}

	function onSelectClick()
	{
		if(document.getElementById(<%=AspxTools.JsGetClientIdString( m_Brokers) %>).selectedIndex > 0)
			document.getElementById(<%=AspxTools.JsGetClientIdString( m_SelectButton ) %>).click();
	}
</script>
<form id="LOAdmin" method="post" runat="server">
	<table height="100%" width="100%">
		<tr>
			<td vAlign="middle" align="center">
				<table>
					<tr>
						<td style="PADDING-LEFT: 84px">LendingQB Administrator Login</td>
					</tr>
					<TR>
						<TD style="PADDING-LEFT: 84px"><br /><uc1:loginusercontrol id="m_LoginUserControl1" runat="server" /></TD>
					</TR>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>
							<table>
								<tr>
									<td>
										<font style="PADDING-RIGHT: 5px; PADDING-LEFT: 80px">Become Other User</font>
									</td>
								</tr>
								<tr>
									<td>
										<br>
										<strong><font color="#666666" style="PADDING-RIGHT: 25px; PADDING-LEFT: 80px">By: </font>
										</strong>
										<asp:dropdownlist id="m_LoginType" Runat="server" Width="120">
											<asp:ListItem Selected="True" Value="LoginName">Login Name</asp:ListItem>
											<asp:ListItem Value="UserID">User ID</asp:ListItem>
											<asp:ListItem Value="EmployeeID">Employee ID</asp:ListItem>
										</asp:dropdownlist>
										<strong><font color="#666666">Type:&nbsp;</font></strong>
										<asp:dropdownlist id="m_TypeDdl" Runat="server">
											<asp:ListItem Selected="True" Value="BLqb">B (LQB)</asp:ListItem>
											<asp:ListItem Value="BTpo">B (TPO)</asp:ListItem>
											<asp:ListItem Value="P">P</asp:ListItem>
										</asp:dropdownlist>
									</td>
								</tr>
								<tr>
									<td style="white-space:nowrap;">
										<strong><font color="#666666" style="PADDING-RIGHT: 5px; PADDING-LEFT: 80px">User:</font>
										</strong>&nbsp;
										<asp:textbox id="m_BecomeUser" Runat="server" Width="275"></asp:textbox>
										<asp:button id="m_BecomeUserButton" onclick="BecomeClick" Runat="server" Text="Become (Pipeline - Old UI)"></asp:button>&nbsp;
										<asp:Button ID="m_BecomeUserButton2016UI" OnClick="BecomeClick" runat="server" Text="Become (Pipeline - New UI)" />
									</td>
								</tr>
								<tr>
									<td>
										<ml:EncodedLabel id="m_ErrMsg" style="PADDING-LEFT: 135px; PADDING-TOP:5px" runat="server" Height="35"></ml:EncodedLabel>
										<br>
										<asp:panel id="m_BrokersPanel" style="PADDING-LEFT: 135px" Runat="server">
				<asp:DropDownList id="m_Brokers" Runat="server" Width="275"></asp:DropDownList>
				<input type="button" onclick="onSelectClick();" Runat="server" Value="Select"></asp:Button>
				<asp:Button id="m_CancelButton" Runat="server" Text="Cancel"></asp:Button>
				<asp:Button id="m_SelectButton" onclick="SelectClick" Runat="server" Text="Select" style="display:none"></asp:Button>
			</asp:panel>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>
</body>
</html>
