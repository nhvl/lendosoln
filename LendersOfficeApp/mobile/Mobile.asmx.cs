﻿namespace LendersOfficeApp.WebService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Services;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Security;

    /// <summary>
    /// Container for methods used for the mobile app.
    /// </summary>
    [WebService(Namespace = "http://www.lendersoffice.com/los/webservices/")]
    public class Mobile : System.Web.Services.WebService
    {
        /// <summary>
        /// Retrieve a list of recent active loans.
        /// </summary>
        /// <param name="sTicket">The authentication ticket.</param>
        /// <returns>A list of recent active loans.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed. Parameters kept with hungarian notation to maintain public interface.")]
        [WebMethod]
        public string RetrieveRecentActiveLoanList(string sTicket)
        {
            // 11/8/2011 dd - This method is only design to use in mobile app. 
            AbstractUserPrincipal principal = this.GetPrincipalFromTicket(sTicket, null);

            return LendersOffice.LoanSearch.LendingQBSearch.ListRecentLoansForMobileInXmlFormat(principal);
        }

        /// <summary>
        /// Retrieve activity list of a loan.
        /// </summary>
        /// <param name="sTicket">An authentication ticket.</param>
        /// <param name="sLNm">A loan number to retrieve.</param>
        /// <param name="version">Version of mobile app.</param>
        /// <returns>A list of activity in xml.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed. Parameters kept with hungarian notation to maintain public interface.")]
        [WebMethod]
        public string RetrieveLoanActivityList(string sTicket, string sLNm, int version)
        {
            // 11/8/2011 dd - This method is only design to use in mobile app.
            if (version != 1)
            {
                // 11/8/2011 dd - For now we only support version 1.
                throw new CBaseException("Expect version=1", "Expect version=1");
            }

            AbstractUserPrincipal principal = this.GetPrincipalFromTicket(sTicket, sLNm);
            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            AuthServiceHelper.CheckLoanAuthorization(sLId, WorkflowOperations.ReadLoan);

            if (sLId == Guid.Empty)
            {
                throw new IntegrationInvalidLoanNumberException(sLNm);
            }

            return LendersOffice.MobileApp.LatestActivity.GenerateLoanActivityXml(sLId, principal);
        }

        /// <summary>
        /// Check whether the user has permission to run PML.
        /// </summary>
        /// <param name="sTicket">An authentication ticket.</param>
        /// <param name="sLNm">A loan number to check.</param>
        /// <returns>Whether user has permission to run PML.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed. Parameters kept with hungarian notation to maintain public interface.")]
        [WebMethod]
        public bool HasPermissionToRunPML(string sTicket, string sLNm)
        {
            AbstractUserPrincipal principal = this.GetPrincipalFromTicket(sTicket, sLNm);
            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            return AuthServiceHelper.IsOperationAuthorized(sLId, WorkflowOperations.RunPmlForAllLoans);
        }

        /// <summary>
        /// Check whether the user has permission to request rate lock.
        /// </summary>
        /// <param name="sTicket">An authentication ticket.</param>
        /// <param name="sLNm">A loan number.</param>
        /// <returns>Whether user has permission to request rate lock.</returns>
        [WebMethod]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed. Parameters kept with hungarian notation to maintain public interface.")]
        public bool HasPermissionToRequestRateLock(string sTicket, string sLNm)
        {
            AbstractUserPrincipal principal = this.GetPrincipalFromTicket(sTicket, sLNm);
            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            List<WorkflowOperation> operations = new List<WorkflowOperation>()
            {
                WorkflowOperations.RequestRateLockCausesLock,
                WorkflowOperations.RequestRateLockRequiresManualLock
            };

            var authorizedOperationResults = AuthServiceHelper.AreOperationsAuthorized(sLId, operations);
            return authorizedOperationResults.Values.Any((result) => result);
        }

        /// <summary>
        /// Determine whether the version of mobile app is still supported.
        /// </summary>
        /// <param name="vendorName">Vendor name to check.</param>
        /// <param name="versionNumber">Version number to check.</param>
        /// <returns>Whether the version of mobile app is still supported.</returns>
        [WebMethod]
        public string GetVersionStatus(string vendorName, double versionNumber)
        {
            if (vendorName.Equals("LendingQB"))
            {
                if (versionNumber < 3.2)
                {
                    return "Yes: New Version Available";
                }
                else
                {
                    return "Yes";
                }
            }
            else
            {
                return "Unsupported Vendor";
            }
        }

        /// <summary>
        /// Get the principal object from authentication ticket.
        /// </summary>
        /// <param name="ticket">An authentication ticket.</param>
        /// <param name="loanNumber">The loan number.</param>
        /// <returns>A principal object.</returns>
        private AbstractUserPrincipal GetPrincipalFromTicket(string ticket, string loanNumber)
        {
            var principal = AuthServiceHelper.GetPrincipalFromTicket(ticket, loanNumber);

            if (!principal.BrokerDB.EnableLqbMobileApp)
            {
                throw new AccessDenied(
                    ErrorMessages.LqbMobileAppNotEnabled,
                    $"LQB mobile app disabled for user {principal.LoginNm} ({principal.UserId})");
            }

            return principal;
        }
    }
}
