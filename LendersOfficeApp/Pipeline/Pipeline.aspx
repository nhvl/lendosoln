<!DOCTYPE html>
<html>
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">

    <title>Pipeline</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" id="Theme" href="../css/Prototype/SASS/PipelineTheme.css">

    <link rel="stylesheet" href="../css/bootstrap-datepicker3-1.4.0.css" />
    <link rel="stylesheet" href="../css/angular-ui-select-0.16.1.min.css">
    <link rel="stylesheet" href="../css/toastr-2.1.2.css"/>
    <style>*[hidden] { display: none; }</style>
    <script src="../inc/modernizr-2.8.3.js"></script>
</head>
<body class="wrap" ng-app="app">

<pipeline-app></pipeline-app>

<script src="../inc/console-stub.js"></script>

<script src="../inc/jquery-2.2.3.min.js"></script>
<script src="../inc/autoNumeric-1.9.41.js"></script>
<script src="../inc/toastr-2.1.2.min.js"></script>
<script src="../inc/jquery_nicescroll-3.6.6.js"></script>
<script src="../inc/bootstrap-3.3.4.min.js"></script>
<script src="../inc/jquery.mask-1.14.0.js"></script>

<script src="../inc/bootstrap-datepicker-1.4.0.min.js"></script>


<script src="../inc/angular-1.5.5.min.js"></script>
<script src="../inc/angular-sanitize-1.5.5.min.js"></script>
<script src="../inc/angular-ui-router-0.2.15.min.js"></script>
<script src="../inc/angular-ui-select-0.16.1.min.js"></script>

<script src="../inc/dragular.js"></script>

<script src="../inc/Pipeline.js"></script>

</body>
</html>
