﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using LendersOffice.Constants;
using DataAccess;
using LqbGrammar;
using LqbGrammar.Exceptions;

namespace DocReceiver
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            InitializeApplicationFramework();

            Tools.SetupServicePointManager();
        }

        private static void InitializeApplicationFramework()
        {
            IExceptionHandlerFactory[] arrHandlers = ExceptionHandlerUtil.GetCoreExceptionHandlerFactories();
            using (IApplicationInitialize iAppInit = LqbApplication.CreateInitializer(arrHandlers))
            {
                const string AppName = "DocReceiver";
                iAppInit.SetName(AppName);

                Adapter.ApplicationInitializer.RegisterFactories(AppName, iAppInit);
                LendersOffice.ApplicationInitializer.RegisterFactories(AppName, iAppInit);
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {   

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();
            
            try
            {
                Tools.LogErrorWithCriticalTracking(exc);
            }
            catch (Exception logExc)
            {
                Tools.LogError("Fail to send error (check server log):" + exc.Message, logExc);
                throw;
            }

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}