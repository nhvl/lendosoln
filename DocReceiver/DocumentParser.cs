﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Xml;
using DataAccess;
using EDocs;
using System.IO;

namespace DocReceiver
{
    // Simple document parser for the xml
    public class DocumentParser
    {
        #region Member props
        public string Login { get; private set; }
        public string Password { get; private set; }
        public string Sender { get; private set; }
        public string TimeReceived { get; private set; }
        public int Pages { get; private set; }
        public List<Barcode> Barcodes { get; private set; }
        public string FaxNumber { get; private set; }
        public byte[] Document { get; private set; }
        #endregion

        public DocumentParser()
        {
        }

        public override string ToString()
        {
            string barcodeList = string.Empty;
            if (Barcodes != null)
            {
                foreach (Barcode code in Barcodes)
                    barcodeList += Environment.NewLine + code.ToString();
            }

            return string.Format("Login: {1}{0}Password: {2}{0}Sender: {3}{0}TimeReceived: {4}{0}Pages: {5}{0}Barcodes: {6}{0}"
                , Environment.NewLine
                , Login
                , Password
                , Sender
                , TimeReceived
                , Pages
                , barcodeList
                );
        }

        #region Parsing Methods
        public void Parse(Stream streamToParse)
        {
            long streamLength;
            try
            {
                streamLength = streamToParse.Length;
            }
            catch (NotSupportedException)
            {
                streamLength = long.MaxValue;
            }
            if (streamLength < 150000) //150k Arbitrary.  But we definately don't want to do this with very large posts.
            {
                // If the post is small enough, we can keep in memory so we can log to PB
                // the first few hundred characters of posted data 
                // to help debug if XmlTextReader is unable to parse it out

                string postStr = string.Empty;
                using ( StreamReader postReader = new StreamReader(streamToParse ) )
                {
                    postStr = postReader.ReadToEnd();
                }
                string logStr = (postStr.Length <= 1500) ? postStr : (postStr.Substring(0, 1500) + "..." );
                Tools.LogInfo("<DocReceiver> Post occurs: " + "(" + streamLength+ " bytes)" + Environment.NewLine + logStr);

                using (XmlReader reader = XmlReader.Create( new StringReader( postStr) ) )
                {
                    Parse(reader);
                }
            }
            else
            {
                Tools.LogInfo("<DocReceiver> Post occurs." + (streamLength != long.MaxValue ? " Bytes: " + streamLength : string.Empty));

                using (XmlReader reader = XmlReader.Create(streamToParse))
                {
                    Parse(reader);
                }
            }
        }

        private void Parse(XmlReader reader)
        {
            // It is reported that XmlReader is several times faster than XmlDocument 
            // for forward-only read access.

            // It will stop doc router, but we should let any exceptions related to the parsing of the doc
            // bubble up--it would imply our assumptions of the doc router format are incorrect. 

            Login = MoveAndReadStringContent(reader, "LOGIN");
            Password = MoveAndReadStringContent(reader, "PASSWORD");

            // Check for this error right away to avoid over-processing a rejected post
            FaxNumber = EDocsFaxNumber.GetFaxNumber(Login, Password);
            if (FaxNumber == null) throw new InvalidLoginPasswordException();

            Sender = MoveAndReadStringContent(reader, "SENDER");
            TimeReceived = MoveAndReadStringContent(reader, "TIME_RECEIVED");

            ParseBarCodes(reader);

            Pages = int.Parse(MoveAndReadStringContent(reader, "PAGES") );

            ParseDocument(reader);
        }
        #endregion

        #region Parsing_Helpers
        private void ParseDocument(XmlReader reader)
        {
            SmartMoveToContent(reader, "DATA");

            using (MemoryStream memStream = new MemoryStream())
            {
                int readBytes = 0;

                byte[] buffer = new byte[32768];

                while ((readBytes = reader.ReadElementContentAsBase64(buffer, 0, buffer.Length)) > 0)
                {
                    memStream.Write(buffer, 0, readBytes);
                }
                Document = memStream.ToArray();
            }

        }

        // Get the list of all barcodes across all pages.
        private void ParseBarCodes(XmlReader reader)
        {
            Barcodes = new List<Barcode>();

            SmartMoveToContent(reader, "BARCODES");

            if (reader.IsEmptyElement == false) // Empty element means self-closing tag.->  '<BARCODES />'
            {
                bool doneReading = false;
                while (doneReading == false)
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (reader.Name == "BARCODE")
                            {
                                int pageNum = int.Parse(reader.GetAttribute("page"));

                                string rawData = reader.ReadElementContentAsString();

                                Barcode newBarcode;
                                try
                                {
                                    newBarcode = ParseBarCode(rawData);
                                    newBarcode.Page = pageNum;
                                }
                                catch
                                {
                                    // We could not validate the data for this barcode.  If we can read the document,
                                    // it shall be sent do Lost Docs. There is also risk that this barcode did not 
                                    // come from our coversheet, and is in the middle of a doc.  Revisit if that becomes a problem.
                                    newBarcode = new UnreadBarcode() { RawBarcodeData = rawData, Page = pageNum };
                                }

                                Barcodes.Add(newBarcode);

                                continue; // We've already moved the reader
                            }
                            break;
                        case XmlNodeType.EndElement:
                            if (reader.Name == "BARCODES")
                                doneReading = true;
                            break;
                    }

                    if (doneReading) break;

                    if (reader.Read() == false) break;
                }
            }
            else
            {
                // Doc Router found no barcodes in the packet, which means the entire thing goes to lost docs
                //Barcodes.Add(new UnreadBarcode() { RawBarcodeData = string.Empty, Page = 0 });
            }
        }

        public Barcode ParseBarCode(string barCodeData)
        {
            try
            {

                if (barCodeData.StartsWith("JMAC:"))
                {
                    string[] parts = barCodeData.Substring(5).Split(new char[] { '|' });
                    if (parts.Length != 4)
                    {
                        throw new UnreadableBarcodeException("Could not process JMAC barcode, Incorrect number of barcode entries: " + parts.Length);
                    }

                    JMACBarcode orig = new JMACBarcode();
                    orig.DocTypeName = parts[1];
                    orig.LoanName = parts[0];
                    orig.PageNumberWithinForm = int.Parse(parts[2]);
                    orig.TotalPageCount = int.Parse(parts[3]);
                    orig.RawBarcodeData = barCodeData;
                    return orig;
                }
                // Old bar codes looked like this:
                // LoanId|DocTypeId|Description
                // New bar codes look like this for Edocs faxes:
                // LoanId|AppId|DocTypeId|Description
                // And like this for Consumer Portal Requests:
                // CPRequestId|Guid.Empty|DocTypeId|Description
                // To determine if we are reading an old barcode, we'll first check the second index
                // If it's an int, then it's a DocTypeId, and is the old barcode format
                // Otherwise, if it's a Guid, it's the new doc type format, and we will process accordingly

                string rawBarCodeData = LendersOffice.Common.EncryptionHelper.Decrypt(barCodeData);

                string[] codedValues = rawBarCodeData.Split('|');
                if (codedValues.Length > 2)
                {
                    Guid? loanId = null;
                    try
                    {
                        loanId = new Guid(codedValues[0]);
                    }
                    catch
                    {
                        // This was not a Guid
                        //LoanId = null;
                    }

                    if (loanId.HasValue)
                    {
                        // Process as edocs format

                        if (codedValues.Length == 3)
                        {
                            // Process as Old edocs format
                            EdocsBarcodeOriginal barcode = new EdocsBarcodeOriginal();
                            barcode.RawBarcodeData = rawBarCodeData;

                            string loanStr = codedValues[0];
                            string docStr = codedValues[1];

                            barcode.LoanId = loanId.Value;
                            barcode.DocType = int.Parse(docStr);

                            int descIndex = loanStr.Length + docStr.Length + 2; // So we don't have to worry about encoding delimiter
                            barcode.Description = rawBarCodeData.Substring(descIndex, rawBarCodeData.Length - descIndex);
                            return barcode;
                        }
                        else if (codedValues.Length == 4)
                        {
                            // Process as New edocs format
                            EdocsBarcode barcode = new EdocsBarcode();
                            barcode.RawBarcodeData = rawBarCodeData;

                            string loanStr = codedValues[0];
                            string appStr = codedValues[1];
                            string docStr = codedValues[2];

                            barcode.LoanId = new Guid(loanStr);
                            barcode.AppId = new Guid(appStr);
                            barcode.DocType = int.Parse(docStr);

                            int descIndex = loanStr.Length + appStr.Length + docStr.Length + 3; // So we don't have to worry about encoding delimiter
                            barcode.Description = rawBarCodeData.Substring(descIndex, rawBarCodeData.Length - descIndex);

                            return barcode;
                        }
                        else
                            throw new UnreadableBarcodeException("Could not process barcode, Incorrect number of barcode entries: " + codedValues.Length);
                    }
                    else
                    {
                        // Process as consumer portal format
                        ConsumerPortalBarcode barcode = new ConsumerPortalBarcode();
                        barcode.RawBarcodeData = rawBarCodeData;
                        barcode.ConsumerPortalRequestId = long.Parse(codedValues[0]);
                        Guid brokerId;
                        Guid.TryParse(codedValues[1], out brokerId); // missing brokerids will happen for older CP barcodes.  While this error is fatal, it will be handled downstream.
                        barcode.BrokerId = brokerId;
                        return barcode;
                    }
                }
                else
                {
                    throw new UnreadableBarcodeException("Could not process barcode, Barcode missing entries: " + rawBarCodeData);
                }
            }
            catch (Exception exc)
            {
                throw new UnreadableBarcodeException("Could not process barcode: " + barCodeData, exc);
            }
        }


        private string MoveAndReadStringContent(XmlReader reader, string element)
        {
            SmartMoveToContent(reader, element);
            return reader.ReadElementContentAsString();
        }

        private void SmartMoveToContent(XmlReader reader, string element)
        {
            XmlNodeType nodeType = reader.MoveToContent();
            if (nodeType == XmlNodeType.Element && reader.Name != null && reader.Name == element)
            {
                // We are positioned on the node we want
            }
            else
            {
                if (reader.ReadToFollowing(element) == false)
                {
                    throw new CBaseException("Xml parsing error.", "XmlDocument missing element" + element);
                }
            }
        }
        #endregion

        #region Document Data
        
        //POD class for a ready-to-save document
        public class ParsedDocSpec
        {
            public Barcode Barcode { get; set; }
            public byte[] Document { get; set; }
            public string FaxNumber { get; set; }
        }

        // Get list of documents after parsing
        public IEnumerable<ParsedDocSpec> GetDocuments()
        {
            if (Barcodes.Count == 0)
            {
                // No barcodes found.  This entire document should go to lost docs.
                yield return new ParsedDocSpec() { Barcode = new UnreadBarcode() { RawBarcodeData = "NO BARCODES FOUND ON DOC" }, Document = Document, FaxNumber = FaxNumber };
            }
            else if (Barcodes.Any(p => p.RawBarcodeData.StartsWith("JMAC:", StringComparison.OrdinalIgnoreCase)))
            {
                Barcodes.Sort();
                List<JMACBarcode> jmacBarcodes = new List<JMACBarcode>();
                
                int pageCount; 
                bool hasPassword;
                EDocumentViewer.GetPdfInfo(Document, out hasPassword, out pageCount);
                List<ParsedDocSpec> specs = new List<ParsedDocSpec>();
                //First lets make sure each page has a barcode and the barcode contents are okay.
                int currentFormIndex = -1;
                string currentLoanNumber = "";
                int currentBarcodeStartPage = 1;
                
                foreach (Barcode barcode in Barcodes)
                {
                    JMACBarcode jmacBarcode = barcode as JMACBarcode;
                    if (jmacBarcode == null )
                    {
                        continue;
                    }
                    jmacBarcodes.Add(jmacBarcode);
                    if (jmacBarcode.PageNumberWithinForm == 1)
                    {
                        currentFormIndex = 0;
                        currentLoanNumber = jmacBarcode.LoanName;
                        currentBarcodeStartPage = jmacBarcode.Page;
                    }
                    currentFormIndex++;
                    if (jmacBarcode.PageNumberWithinForm != currentFormIndex || jmacBarcode.LoanName != currentLoanNumber)
                    {
                        yield return new ParsedDocSpec() { Barcode = new UnreadBarcode() { RawBarcodeData = "JMAC Barcodes are not correct" }, Document = Document, FaxNumber = FaxNumber };
                    }
                    if (jmacBarcode.PageNumberWithinForm == jmacBarcode.TotalPageCount)
                    {
                        ParsedDocSpec spec = new ParsedDocSpec();
                        spec.Barcode = jmacBarcode;
                        spec.Document = EdocFaxCover.GetPageRange(Document, currentBarcodeStartPage, barcode.Page);
                        specs.Add(spec);
                    }
                }

                if (jmacBarcodes.Count != pageCount)
                {
                    yield return new ParsedDocSpec() { Barcode = new UnreadBarcode() { RawBarcodeData = "Did not find 1 JMacBarcode per page" }, Document = Document, FaxNumber = FaxNumber };
                }

                foreach (var spec in specs)
                {
                    yield return spec;
                }

            }
            else 
            {
                // Sort barcode list by page.
                Barcodes.Sort();

                for (int i = 0; i < Barcodes.Count; i++)
                {
                    if (i == 0 && Barcodes[i].Page > 1)
                    {
                        // This means that there are pages that come before the first barcode we scanned, It is a lost doc.
                        
                        byte[] firstDoc = EdocFaxCover.GetPageRange(Document, 1, Barcodes[i].Page - 1);
                        yield return new ParsedDocSpec() { Barcode = new UnreadBarcode() { RawBarcodeData = "NO BARCODE FOUND" }, Document = firstDoc, FaxNumber = FaxNumber };
                    }

                    // Use the next coversheet or end of doc to determine where this subdoc ends.
                    int lastPage = (i == Barcodes.Count - 1) ? Pages : Barcodes[i + 1].Page - 1;

                    byte[] newDoc = EdocFaxCover.GetPageRange(Document, Barcodes[i].Page + 1, lastPage);

                    yield return new ParsedDocSpec() { Barcode = Barcodes[i], Document = newDoc, FaxNumber = FaxNumber };
                }
            }
        }
        #endregion

    }


    #region Barcode data
    // POD Class representing a read bardcode
    public abstract class Barcode : IComparable<Barcode>
    {
        public int Page { get; set; }
        public string RawBarcodeData { get; set; }

        public override string ToString()
        {
            string data = string.Format("{0}Page: {1}{0}RawBarCode: {2}{0}"
                , Environment.NewLine
                , Page
                , RawBarcodeData
                );

            return base.ToString() + data;
        }

        public int CompareTo(Barcode other)
        {
            return Page.CompareTo(other.Page);
        }
    }

    public class UnreadBarcode : Barcode
    {
        // Use this one to represent a barcode that we could not read
    }

    public class JMACBarcode : Barcode
    {
        public string LoanName { get; set; }
        public string DocTypeName { get; set; }
        public string Description { get; set; }
        /// <summary>
        /// 1 based
        /// </summary>
        public int PageNumberWithinForm { get; set; }

        public int TotalPageCount { get; set; }

        public override string ToString()
        {
            string data = string.Format("LoanName: {1}{0}DocType: {2}{0}Description: {3}{0}"
    , Environment.NewLine
    , LoanName
    , DocTypeName
    , Description
    );

            return base.ToString() + data;
        }

    }

    public class EdocsBarcodeOriginal : Barcode
    {
        public Guid LoanId { get; set; }
        public int DocType { get; set; }
        public string Description { get; set; }

        public override string ToString()
        {
            string data = string.Format("LoanId: {1}{0}DocType: {2}{0}Description: {3}{0}"
    , Environment.NewLine
    , LoanId
    , DocType
    , Description
    );

            return base.ToString() + data;
        }

    }

    public class EdocsBarcode : EdocsBarcodeOriginal
    {
        public Guid AppId { get; set; }

        public override string ToString()
        {
            string data = string.Format("AppId: {1}{0}"
    , Environment.NewLine
    , AppId
    );
            return base.ToString() + data;
        }

    }

    public class ConsumerPortalBarcode : Barcode
    {
        public long ConsumerPortalRequestId { get; set; }

        public Guid BrokerId { get; set; }

        public override string ToString()
        {
            string data = string.Format(
                "ConsumerPortalRequestId: {1}{0}BrokerId: {2}{0}",
                Environment.NewLine,
                this.ConsumerPortalRequestId,
                this.BrokerId);

            return base.ToString() + data;
        }

    }
    #endregion

    #region Exceptions
    public class InvalidLoginPasswordException : CBaseException
    {
        public InvalidLoginPasswordException()
            : base("Invalid Login Credentials", "Invalid Login Credentials")
        {
        }
    }

    public class UnreadableBarcodeException : CBaseException
    {
        public UnreadableBarcodeException(string message)
            : base(message, message)
        {
        }
        public UnreadableBarcodeException(string message, Exception exc)
            : base(message, exc)
        {
        }
    }
    #endregion
}
