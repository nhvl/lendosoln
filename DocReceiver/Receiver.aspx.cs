﻿namespace DocReceiver
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using EDocs;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Gateways;

    public partial class Receiver : System.Web.UI.Page
    {

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Request.HttpMethod == "POST")
            {
                string statusCode = string.Empty;
                string statusMessage = string.Empty;
                bool shouldSave = false;

                // 1. Attempt to parse the posted Xml doc
                DocumentParser parser = new DocumentParser();
                try
                {
                    parser.Parse(Request.InputStream);
                    shouldSave = true;
                }
                catch (InvalidLoginPasswordException)
                {
                    // Invalid login credentials were posted. Fail immediately.

                    Tools.LogWarning("<DocReceiver> Invalid login: " + Environment.NewLine + parser.ToString());

                    shouldSave = false;
                    statusCode = "LOGIN_FAILED";
                    statusMessage = "Login failed";
                }
                catch (Exception exc)
                {
                    // Some unknown error occured while parsing.  We have to fail so Doc Router keeps the doc.
                    Tools.LogWarning("<DocReceiver> Could not parse Doc: " + Environment.NewLine + parser.ToString(), exc);
                    
                    shouldSave = false;
                    statusCode = "PARSE_FAILURE";
                    statusMessage = exc.Message;
                }

                if (shouldSave == true)
                {
                    foreach (DocumentParser.ParsedDocSpec docSpec in parser.GetDocuments())
                    {
                        if (SaveToLoanFile(docSpec))
                        {
                            Tools.LogInfo("<DocReceiver> Accepted Doc: " + Environment.NewLine + docSpec.Barcode.ToString());
                        }
                        else
                        {
                            if (SaveToLostDocuments(docSpec))
                            {
                                Tools.LogWarning("<DocReceiver> Saved lost Doc: " + Environment.NewLine + docSpec.Barcode.ToString());
                            }
                            else
                            {
                                Tools.LogError("<DocReceiver> Failed to saved lost Doc: " + Environment.NewLine + docSpec.Barcode.ToString());
                                // This is unlikely event that the lost docs save failed.
                                // We are running the rack saving, this could be the 4th doc of 7 that arrived.
                                // If this doc is unsavable as edoc may be some how corrupted
                            }
                        }
                    }

                    // Success -- We report OK even if item was saved to lost documents
                    statusCode = "OK";
                }

                WriteResponse(statusCode, statusMessage);
            
            } // If (http verb == "POST")
        }

        private void WriteResponse(string statusCode, string message)
        {
            string response = string.Format(
                @"<RESPONSE><STATUS><CODE>{0}</CODE>{1}</STATUS></RESPONSE>"
                , statusCode
                , message != string.Empty ? ("<MESSAGE>" + message + "</MESSAGE>") : string.Empty
                );

            Response.Clear();
            Response.ClearContent();
            Response.ContentType = "text/xml";
            Response.Write(response);
            Response.End();
        }

        // Save a single doc to a loan file
        private bool SaveToLoanFile(DocumentParser.ParsedDocSpec doc)
        {
            Barcode barcode = doc.Barcode;

            try
            {
                if (barcode is JMACBarcode)
                {
                    JMACBarcode code = barcode as JMACBarcode;
                    //JMac
                    Guid brokerId = new Guid("855b4ac6-00cf-4e47-8b43-2076c5c0d1dc");
                    //Guid brokerId = new Guid("839127ef-72d1-4873-bfdf-18a23416b146"); //thinh trial
                    Guid sLId = Tools.GetLoanIdByLoanName(brokerId, code.LoanName);
                    if (sLId == Guid.Empty)
                    {
                        Tools.LogError("<DocReceiver> Did not find JMAC Loan for " + code.ToString());
                        return false;
                    }
                    
                    CPageData d = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(Receiver));
                    d.InitLoad();
                    Guid aAppId = d.GetAppData(0).aAppId;

                    var docTypes = EDocumentDocType.GetDocTypesByBroker(brokerId, E_EnforceFolderPermissions.True);
                    DocType docType = docTypes.FirstOrDefault(p=>p.DocTypeName.ToLower().Equals(code.DocTypeName.ToLower()));

                    EDocumentRepository repository = EDocumentRepository.GetSystemRepository(brokerId);
                    EDocument eDoc = repository.CreateDocument(E_EDocumentSource.Fax);

                    if (docType == null)
                    {
                        Tools.LogError(String.Concat("<DocReceiver> Did not find doctype ", code.DocTypeName, " in JMAcs Barcodes."));
                        return false;
                    }

                    eDoc.IsUploadedByPmlUser = false; // Anything faxed in is not considered public.
                    eDoc.FaxNumber = doc.FaxNumber;
                    eDoc.LoanId = sLId;
                    eDoc.AppId = aAppId;
                    eDoc.DocumentTypeId = docType.Id;
                    eDoc.PublicDescription = code.Description ?? "";
                    string path = Path.GetTempFileName();
                    BinaryFileHelper.WriteAllBytes(path, doc.Document);
                    eDoc.UpdatePDFContentOnSave(path);
                    eDoc.EDocOrigin = E_EDocOrigin.Fax;
                    repository.Save(eDoc);
                    FileOperationHelper.Delete(path);
                    return true;
                }
                else if (barcode is EdocsBarcode)
                {
                    EdocsBarcode code = barcode as EdocsBarcode;
                    Guid brokerId = GetBrokerIdFromLoanId(code.LoanId);
                    EDocumentRepository repository = EDocumentRepository.GetSystemRepository(brokerId);
                    EDocument eDoc = repository.CreateDocument(E_EDocumentSource.Fax);
                    eDoc.IsUploadedByPmlUser = false; // Anything faxed in is not considered public.
                    eDoc.FaxNumber = doc.FaxNumber;
                    eDoc.LoanId = code.LoanId;
                    eDoc.AppId = code.AppId;
                    eDoc.DocumentTypeId = code.DocType;
                    eDoc.PublicDescription = code.Description;
                    string path = Path.GetTempFileName();
                    BinaryFileHelper.WriteAllBytes(path, doc.Document);
                    eDoc.UpdatePDFContentOnSave(path);
                    eDoc.EDocOrigin = E_EDocOrigin.Fax;
                    repository.Save(eDoc);
                    FileOperationHelper.Delete(path);
                    return true;

                }
                else if (barcode is EdocsBarcodeOriginal)
                {
                    EdocsBarcodeOriginal code = barcode as EdocsBarcodeOriginal;
                    Guid brokerId = GetBrokerIdFromLoanId(code.LoanId);
                    EDocumentRepository repository = EDocumentRepository.GetSystemRepository(brokerId);
                    EDocument eDoc = repository.CreateDocument(E_EDocumentSource.Fax);

                    eDoc.IsUploadedByPmlUser = false; // Anything faxed in is not considered public.
                    eDoc.FaxNumber = doc.FaxNumber;
                    eDoc.LoanId = code.LoanId;
                    eDoc.AppId = null;
                    eDoc.DocumentTypeId = code.DocType;
                    eDoc.PublicDescription = code.Description ?? "";
                    string path = Path.GetTempFileName();
                    BinaryFileHelper.WriteAllBytes(path, doc.Document);
                    eDoc.UpdatePDFContentOnSave(path);
                    eDoc.EDocOrigin = E_EDocOrigin.Fax;
                    repository.Save(eDoc);
                    FileOperationHelper.Delete(path);
                    return true;

                }
                else if (barcode is ConsumerPortalBarcode)
                {
                    ConsumerPortalBarcode code = barcode as ConsumerPortalBarcode;
                    if (code.BrokerId == Guid.Empty)
                    {
                        return false; // OPM 459863 - If the barcode didn't have a brokerid, we'll send it to lost docs rather than risking the doc going to the wrong lender
                    }

                    ConsumerActionItem item = new ConsumerActionItem(code.ConsumerPortalRequestId, code.BrokerId);

                    item.AttachArrivedFax(doc.FaxNumber, doc.Document);
                    item.Save();
                    return true;
                }
                else if (barcode is UnreadBarcode)
                {
                    return false;  // Will try Lost eDocs.
                }
                else
                    throw new CBaseException(ErrorMessages.Generic, "Unknown barcode type: " + barcode.GetType());
            }
            catch (Exception exc)
            {
                // A save failure occured. 
                // Log and we can try lost docs next.
                Tools.LogError("<DocReceiver> Failed to save doc.  Will try lost Docs next. " + barcode.ToString(), exc);
                return false;
            }
        }

        private bool SaveToLostDocuments(DocumentParser.ParsedDocSpec doc)
        {
            // We have a missing or misread barcode.  We cannot trust
            // any data from it. (DocType, LoanId, Description)
            try
            {
                LostEDocument eDoc = new LostEDocument();

                eDoc.FaxNumber = doc.FaxNumber;
                eDoc.PDFData = doc.Document;

                eDoc.Save();
                return true;
            }
            catch (Exception exc)
            {
                Tools.LogError("Failed to save lost eDoc. " + doc.Barcode.ToString(), exc);
                return false;
            }
        }

        private Guid GetBrokerIdFromLoanId(Guid sLId)
        {
            Guid brokerId = Guid.Empty;

            DbConnectionInfo.GetConnectionInfoByLoanId(sLId, out brokerId);

            return brokerId;
        }


    }
}