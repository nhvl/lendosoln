using System.Configuration;

namespace RatesheetMap
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;
    using DataAccess;
    using ExcelLibraryClient;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;

    public class SystemVariables
	{
        private static SystemVariables s_singletonInstance = null;
        private static object s_lock = new object();
        /// <summary>
        /// SystemVariables does not varies between ratesheet during a single run.
        /// Therefore to optimize for performance, going to cache this object per run.
        /// </summary>
        /// <returns></returns>
        public static SystemVariables GetGlobalSystemVariables()
        {
            lock (s_lock)
            {
                if (s_singletonInstance == null)
                {
                    s_singletonInstance = new SystemVariables();
                    s_singletonInstance.Load();
                }
                return s_singletonInstance;
            }
        }

        public static void ClearGlobalSystemVariables()
        {
            lock (s_lock)
            {
                s_singletonInstance = null;
            }
        }
		#region Variables
		
		private Hashtable		m_systemVariables;
		private bool			m_accessedSystemVars = false;
		
		public bool AccessedSystemVariables
		{
			get { return m_accessedSystemVars; }
		}

		private string MARKET_INDEX_PATH
		{
			get { return ConfigurationManager.AppSettings["MARKET_INDEX_PATH"]; }
		}
		#endregion
		
		private SystemVariables()
		{
			m_systemVariables = new Hashtable();
		}

		public bool Contains(string varName)
		{
			m_accessedSystemVars = true;
			return m_systemVariables.ContainsKey(varName);
		}

		//System variables are never arrays
		private void Add(string name, string val)
		{
			if(m_systemVariables.ContainsKey(name))
				m_systemVariables[name] = val;
			else
				m_systemVariables.Add(name, val);
		}

		public string GetValue(string varName)
		{
			m_accessedSystemVars = true;
			if(m_systemVariables.ContainsKey(varName))
				return m_systemVariables[varName].ToString();
			else
				throw new CBaseException(String.Format("Could not find System Variable '{0}'.", varName), "");
		}

		// System variables are defined in an external file called MarketIndex.xls.  This file is downloaded every day
		// along with the ratesheets (typically the beginning of the day).
		private void Load()
		{

            try
            {
                //TODO - change this to .xslx if we ever upgrade it to the new excel format
                List<string> dependentFileList = new List<string>();
                foreach (string sFilePath in RatesheetMapCommon.GetExcelFiles(MARKET_INDEX_PATH))
                {
                    if (!FileOperationHelper.Exists(sFilePath))
                        continue;
                    System.IO.FileInfo info = new System.IO.FileInfo(sFilePath);
                    string sFileName = info.Name;
                    if (!(sFileName.ToLower().Equals("marketindex.xls")))
                    {
                        dependentFileList.Add(sFilePath);
                    }
                }

                //TODO - change this to .xslx if we ever upgrade it to the new excel format
                string combined = System.IO.Path.Combine(MARKET_INDEX_PATH, "MarketIndex.xls");
                if (!FileOperationHelper.Exists(combined))
                {
                    Tools.LogWarning(String.Format("MarketIndex file not found (looking for '{0}') - No system variables will be available.", combined));
                    return;
                }
                IEnumerable<KeyValuePair<string, string>> systemVariableList = null;

                using (PerformanceStopwatch.Start("ExcelLibrary.GetSystemVariablesForRatesheetProcessor"))
                {
                   systemVariableList =  ExcelLibrary.GetSystemVariablesForRatesheetProcessor(combined, dependentFileList.ToArray(), 200);
                }
                foreach (var o in systemVariableList )
                {
                    Add(o.Key, o.Value);
                }

            }
            catch (Exception e)
            {
                Tools.LogWarning(String.Format("Error retrieving system variables - No system variables will be available. {0}", e));
            }

		}

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (string key in m_systemVariables.Keys)
            {
                sb.Append(Environment.NewLine + key + " -> " + m_systemVariables[key].ToString());
            }
            
            return sb.ToString();
        }
	}
}