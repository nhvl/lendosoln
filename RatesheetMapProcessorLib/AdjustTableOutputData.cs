using System;
using System.Collections;
using System.Text;
using DataAccess;

namespace RatesheetMap
{
	public class AdjustmentTableOutputData : OutputGroup
	{
		#region Variables
		ArrayList			m_groups;
		private string		m_regionId;
		private bool		m_hasData = false;
		private Hashtable	m_regionVariables = new Hashtable();
		private ArrayList	m_allColumns = new ArrayList();
		private ArrayList	m_warningList = new ArrayList();

		public string RegionId
		{
			get { return m_regionId; }
		}

		public ArrayList WarningList
		{
			get { return m_warningList; }
		}

		public ArrayList Columns
		{
			get { return new ArrayList(); }
		}

		public ArrayList AllColumns
		{
			get { return m_allColumns; }
		}

		public bool HasData
		{
			get { return m_hasData; }
		}

		public ArrayList OutputRegions
		{
			get { return m_groups; }
		}

		public GroupTag RegionFilter
		{
			get { return null; }
		}

		public bool IsContainer
		{
			get { return true; }
		}

		public bool HasContainers
		{
			get { return false; }
		}

		public Hashtable RegionVariables
		{
			get { return m_regionVariables; }
			set { m_regionVariables = value; }
		}
		#endregion

		public AdjustmentTableOutputData(string regionId)
		{
			m_regionId = regionId;
			m_groups = new ArrayList();
		}
		
		public AdjustmentTableOutputData(string regionId, OutputColumn oColumn)
		{
			m_regionId = regionId;
			m_groups = new ArrayList();
			AddColumn(oColumn);
		}

		public int WriteDataToWorksheet2D(OutputWriter outputWriter, ref int startRow, ref int startCol, ref bool hasIO, bool firstFragment, bool ignoreHasIO, bool ignoreNonIOHeaders)
		{
            foreach(OutputGroup group in m_groups)
			{
                group.WriteDataToWorksheet2D(outputWriter, ref startRow, ref startCol, ref hasIO, firstFragment, ignoreHasIO, ignoreNonIOHeaders);
			}
            // We need to subtract 1 from the row number that we ended on because AdjustTables (NOT RateOptions) must
            // output the entire file without any line breaks, so backstepping by 1 line will effectively remove the line
            // break that would normally exist between the ending rule of one policy to the beginning of another policy.
			return startRow - 1;
		}

		public int WriteDataToWorksheet1D(OutputWriter outputWriter, int startRow, ArrayList headers)
		{
            int startCol = 1;
			bool hasIO = false;
			return WriteDataToWorksheet2D(outputWriter, ref startRow, ref startCol, ref hasIO, false, false, false);
		}

		public GroupTag GetFirstGroupTag()
		{
			return null;
		}

		public GroupTag GetFirstGroupTagByType(E_OperatorAttributeType attributeType)
		{
			return null;
		}

		public void FinalizeCellValues(Variables variables)
		{
			//NO-OP
		}

		public void FinalizeData(Operators operators, Variables variables)
		{
			foreach(OutputGroup group in m_groups)
			{
				group.FinalizeCellValues(variables);
				group.FinalizeData(operators, variables);
			}
		}

		public void AddColumn(OutputColumn oColumn)
		{
			m_hasData = true;
			m_allColumns.Add(oColumn);
			m_groups.Add(new AdjustmentTableOutputMatrix(m_regionId, oColumn));
		}
	}

	public class AdjustmentTableOutputMatrix : OutputGroup
	{
		#region Variables
		private string		m_regionId;
		private static int	NUM_COLUMNS = 5;	//NR, P, M, QR, TR
		private static int	NUM_INFORMATION_COLUMNS = 1; //Holds row Information/Operators
		private static int	NUM_INFORMATION_ROWS = 1;	//Holds column Information/Operators
		private				OutputCell[][] m_outputMatrix;	
		private int			m_rows, m_columns;
		private Variables	m_variables;
		private bool		m_hasData = false;
		private ArrayList	m_warningList = new ArrayList();
		
		public string RegionId
		{
			get { return m_regionId; }
		}

		public ArrayList Columns
		{
			get { return new ArrayList(); }
		}

		public ArrayList WarningList
		{
			get { return m_warningList; }
		}

		public ArrayList AllColumns
		{
			get { return new ArrayList(); }
		}

		public GroupTag RegionFilter
		{
			get { return null; }
		}

		public bool HasData
		{
			get { return m_hasData; }
		}

		public Hashtable RegionVariables
		{
			get { return m_variables.GetRegionVariables(m_regionId); }
			set { m_variables.SetRegionVariables(m_regionId, value); }
		}

		public bool IsContainer
		{
			get { return false; }
		}

		public bool HasContainers
		{
			get { return false; }
		}
		#endregion

		public GroupTag GetFirstGroupTag()
		{
			return null;
		}

		public GroupTag GetFirstGroupTagByType(E_OperatorAttributeType attributeType)
		{
			return null;
		}

		private int GetColumnLocation(int colLocation)
		{
			return colLocation + NUM_INFORMATION_COLUMNS;
		}

		private int GetRowLocation(int rowLocation)
		{
			return rowLocation + NUM_INFORMATION_ROWS;
		}

		public AdjustmentTableOutputMatrix(string regionId, OutputColumn oColumn)
		{
			m_regionId = regionId;
			m_rows = oColumn.Cells.Count + NUM_INFORMATION_ROWS;
			m_columns = NUM_INFORMATION_COLUMNS + NUM_COLUMNS;

			m_outputMatrix = new OutputCell[m_rows][];
			for(int i = 0; i < m_rows; i++)
			{
				m_outputMatrix[i] = new OutputCell[m_columns];
			}
			AddColumn(oColumn);
		}

		public void AddColumn(OutputColumn oColumn)
		{
			m_hasData = true;
			int row = GetRowLocation(0);
			int intDestLocation = (int)oColumn.Destination;
			int column = GetColumnLocation(intDestLocation);
			foreach(OutputCell oCell in oColumn.Cells)
			{
				m_outputMatrix[row++][column] = oCell;
			}
		}

		private string GetCombinedRuleAttribute(Operators operators, int rowOffset, int columnOffset, E_RuleAttributeType ruleAttributeType)
		{
			string combined = "";
			ArrayList ruleOperators = operators.GetOperatorsForColumnOffset(columnOffset, E_OperatorTagType.RuleAttribute);
			ruleOperators.AddRange(operators.GetOperatorsForRowOffset(rowOffset, E_OperatorTagType.RuleAttribute));
			if(ruleOperators.Count > 0)
			{
				for(int i = 0; i < ruleOperators.Count; ++i)
				{
					RuleAttribute rAtt = (RuleAttribute)ruleOperators[i];
					if(rAtt.Type == ruleAttributeType)
					{
						if(ruleAttributeType == E_RuleAttributeType.Condition)
							combined = RuleAttribute.Combine(combined, ((RuleAttribute)ruleOperators[i]).StrAttribute);
						else
							combined = RuleAttribute.CommaCombine(combined, ((RuleAttribute)ruleOperators[i]).StrAttribute);
					}
				}
			}
			return combined;
		}

		public void FinalizeData(Operators operators, Variables variables)
		{
			m_variables = variables;
			FinalizeData(operators);
		}

		public void FinalizeData(Operators operators)
		{
			int columnOffset = 0;
			for(int i = GetColumnLocation(0); i < m_columns; ++i)
			{
				ArrayList mathOperators = new ArrayList();
				
				if(m_outputMatrix[GetRowLocation(0)][i] != null)
				{
					//Get Math operator for this entire column
					columnOffset = ((DataCell)m_outputMatrix[GetRowLocation(0)][i]).ColumnOffset;
					mathOperators = operators.GetOperatorsForColumnOffset(columnOffset, E_OperatorTagType.Math);
				}
				for(int j = GetRowLocation(0); j < m_rows; ++j)
				{
					if(m_outputMatrix[j][i] == null)
						continue;

					if(mathOperators.Count == 0)
					{
						//Get Math operator for this entire column
						columnOffset = ((DataCell)m_outputMatrix[GetRowLocation(0)][i]).ColumnOffset;
						mathOperators = operators.GetOperatorsForColumnOffset(columnOffset, E_OperatorTagType.Math);
					}
					ArrayList cellMathOps = new ArrayList();
					cellMathOps.AddRange(mathOperators);
					DataCell dCell = (DataCell)m_outputMatrix[j][i];
					
					int rowOffset = dCell.RowOffset;
					cellMathOps.AddRange(operators.GetOperatorsForRowOffset(rowOffset, E_OperatorTagType.Math));
					
					string description = GetCombinedRuleAttribute(operators, rowOffset, columnOffset, E_RuleAttributeType.Description);
					string condition = GetCombinedRuleAttribute(operators, rowOffset, columnOffset, E_RuleAttributeType.Condition);
					m_outputMatrix[j][0] = new AdjustmentTableRowInformationCell(m_regionId, dCell.Destination, description, condition);
					
					if(dCell != null && !dCell.Finalized)
					{
						if(cellMathOps.Count > 0)
						{
							string[] rowData = new string[NUM_COLUMNS];
							int index;
							for(int z = GetColumnLocation(0); z < m_columns; ++z)
							{
								index = z - NUM_INFORMATION_COLUMNS;
								if(m_outputMatrix[j][z] != null)
									rowData[index] = ((DataCell)m_outputMatrix[j][z]).UnoperatedData;
								else
									rowData[index] = "0";
							}
							
							dCell.FinalizeData(cellMathOps, m_variables, rowData, true /*userIgnoreSet*/, j);
						}
						else
							dCell.FinalizeData(true /*useIgnoreSet*/);
					}
				}
			}
		}

		public void FinalizeCellValues(Variables variables)
		{
			m_variables = variables;
			
			for(int i = GetColumnLocation(0); i < m_columns; ++i)
			{
				for(int j = GetRowLocation(0); j < m_rows; ++j)
				{
					if(m_outputMatrix[j][i] == null)
						continue;

					DataCell dCell = (DataCell)m_outputMatrix[j][i];

					if(dCell != null && !dCell.Finalized)
					{
						try
						{
							if(dCell.Source != E_ReadLocations.FILE)
								SetCellValueFromNonInputLocation(dCell, j);
						}
						catch(CBaseException c)
						{
							throw c;
						}
						catch(Exception e)
						{
							throw new CBaseException(String.Format("Unable to set the value of a cell from a variable or other column in region '{0}'.  The cell is located at map row {1}, column {2}, and the source of the cell is '{3}'.", m_regionId, dCell.Row, dCell.ColumnLetter, dCell.Source), e);
						}
					}
				}
			}
		}

		public void SetCellValueFromNonInputLocation(DataCell dCell, int row)
		{
			switch(dCell.Source)
			{
				case E_ReadLocations.FILE:
					break;
				case E_ReadLocations.VARIABLE:
					if(dCell.SourceVariableContext == E_VariableContext.Region)
					{
						if(m_variables.ContainsRegionVariable(m_regionId, dCell.SourceVariableName))
							dCell.Data = m_variables.GetNonArrayRegionVariable(m_regionId, dCell.SourceVariableName);
						else
							throw new CBaseException(String.Format("Region '{0}' is trying to read from Region variable '{1}', but that variable was never defined.", m_regionId, dCell.SourceVariableName), "");
					}
					else
					{
						if(m_variables.ContainsMapOrSystemVariable(dCell.SourceVariableName))
							dCell.Data = m_variables.GetNonArrayMapOrSystemVariable(dCell.SourceVariableName);
						else
							throw new CBaseException(String.Format("Region '{0}' is trying to read from Map/System variable '{1}', but that variable was never defined.", m_regionId, dCell.SourceVariableName), "");
					}
					break;
				case E_ReadLocations.NR:
					if(m_outputMatrix[row][GetColumnLocation((int)E_ReadLocations.NR)] != null)
						dCell.Data = ((DataCell)m_outputMatrix[row][GetColumnLocation((int)E_ReadLocations.NR)]).Data;
					break;
				case E_ReadLocations.P:
					if(m_outputMatrix[row][GetColumnLocation((int)E_ReadLocations.P)] != null)
						dCell.Data = ((DataCell)m_outputMatrix[row][GetColumnLocation((int)E_ReadLocations.P)]).Data;
					break;
				case E_ReadLocations.M:
					if(m_outputMatrix[row][GetColumnLocation((int)E_ReadLocations.M)] != null)
						dCell.Data = ((DataCell)m_outputMatrix[row][GetColumnLocation((int)E_ReadLocations.M)]).Data;
					break;
				case E_ReadLocations.QR:
					if(m_outputMatrix[row][GetColumnLocation((int)E_ReadLocations.QR)] != null)
						dCell.Data = ((DataCell)m_outputMatrix[row][GetColumnLocation((int)E_ReadLocations.QR)]).Data;
					break;
				case E_ReadLocations.TR:
					if(m_outputMatrix[row][GetColumnLocation((int)E_ReadLocations.TR)] != null)
						dCell.Data = ((DataCell)m_outputMatrix[row][GetColumnLocation((int)E_ReadLocations.TR)]).Data;
					break;
				default:
					throw new CBaseException(String.Format("Unable to set the value of a cell in region '{0}' from source location '{1}' because that location is not valid.", m_regionId, dCell.Source), "");
			}
		}

		// TODO - make these columns dynamically generated so the column order can be changed around easily by simply
        // changing the order in an enum possibly
        // This is just for code clarify and efficiency, not urgent.
        public int WriteDataToWorksheet2D(OutputWriter outputWriter, ref int startRow, ref int startCol, ref bool hasIO, bool firstFragment, bool ignoreHasIO, bool ignoreNonIOHeaders)
		{
            int stRow = startRow;
			int stCol = startCol;
			int matrixStartRow = GetRowLocation(0);
			int matrixStartColumn = GetColumnLocation(0);
			
			for(int i = matrixStartRow; i < m_rows; i++)
			{
				AdjustmentTableRowInformationCell aCell = (AdjustmentTableRowInformationCell)m_outputMatrix[i][0];
                outputWriter.WriteCell(stRow, stCol++, "Rule");
                // TODO - UNCOMMENT the following line when case 25872 is going to be released and the import requires it
                //outputWriter.WriteCell(stRow, stCol++, ""); // QBC - OPM 25872
                outputWriter.WriteCell(stRow, stCol++, aCell.Description.ToUpper()); // Rule Name
                outputWriter.WriteCell(stRow, stCol++, aCell.Condition); // Rule condition text

                outputWriter.WriteCell(stRow, stCol++, ""); // SKP
                outputWriter.WriteCell(stRow, stCol++, ""); // DIS
				
				// NR, P, M, QR, TR
                for( int j = matrixStartColumn; j < m_columns; j++)
				{
					if(m_outputMatrix[i][j] == null)
					{
						outputWriter.WriteCell(stRow, stCol++, "0.000");
					}
					else
					{
						outputWriter.WriteCell(stRow, stCol++, ((DataCell)m_outputMatrix[i][j]).Data);
					}
				}

                outputWriter.WriteCell(stRow, stCol++, ""); // MAXYSP
                outputWriter.WriteCell(stRow, stCol++, ""); // STIPS
                outputWriter.WriteCell(stRow, stCol++, ""); // MAXDTI
                outputWriter.WriteCell(stRow, stCol++, ""); // DTIRANGE
                outputWriter.WriteCell(stRow, stCol++, ""); // QSCORE
                outputWriter.WriteCell(stRow, stCol++, ""); // QLTV
                outputWriter.WriteCell(stRow, stCol++, ""); // QCLTV
                outputWriter.WriteCell(stRow, stCol++, ""); // QLAMT

                outputWriter.WriteCell(stRow, stCol++, Guid.NewGuid().ToString()); // Rule ID
				
				stCol = startCol;
				stRow++;
			}
			startRow = stRow;
			return stRow;
		}

		public int WriteDataToWorksheet1D(OutputWriter outputWriter, int startRow, ArrayList headers)
		{
            int startCol = 1;
			bool hasIO = false;
			return WriteDataToWorksheet2D(outputWriter, ref startRow, ref startCol, ref hasIO, false, false, false);
		}
	}

	public class AdjustmentTableRowInformationCell : OutputCell
	{
		#region Variables
		private string				m_regionId;
		private string				m_description;
		private string				m_condition;
		private E_ReadLocations		m_destination;

		public override string RegionId
		{
			get { return m_regionId; }
		}
		
		public string Description
		{
			get { return m_description; }
		}

		public string Condition
		{
			get { return m_condition; }
		}
		#endregion

		public AdjustmentTableRowInformationCell(string regionId, E_ReadLocations destination, string description, string condition)
		{
			m_regionId = regionId;
			m_destination = destination;
			m_description = description;
			m_condition = condition;
		}
	}
}
