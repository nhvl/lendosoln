using System;
using Microsoft.JScript;
using System.Reflection;
using System.CodeDom.Compiler;
using DataAccess;

namespace RatesheetMap
{
	public class FormulaEvaluator
	{
        /// <summary>
        /// Only call this method if we need to re-compile the FormulaEvaluator.dll
        /// </summary>
        public static void GenerateDlls()
        {
            JScriptCodeProvider compiler = new JScriptCodeProvider();

            CompilerParameters parameters;
            parameters = new CompilerParameters();
            parameters.GenerateInMemory = false;
            parameters.OutputAssembly = "FormularEvaluator.dll";


            CompilerResults results;
            results = compiler.CompileAssemblyFromSource(parameters, _jscriptSource);

        }

        //public static int EvalToInteger(string statement)
        //{
        //    string s = EvalToString(statement);
        //    return int.Parse(s.ToString());
        //}

		public static double EvalToDouble(string statement)
		{
			string s = EvalToString(statement);
			return double.Parse(s);
		}

		private static string EvalToString(string statement)
		{
            return _eval.Eval(statement);
            //object o = EvalToObject(statement);
            //return o.ToString();
		}

        //private static object EvalToObject(string statement)
        //{
        //    return _evaluatorType.InvokeMember(
        //        "Eval", 
        //        BindingFlags.InvokeMethod, 
        //        null, 
        //        _evaluator, 
        //        new object[] { statement } 
        //        );
        //}
          
		static FormulaEvaluator()
		{
            //JScriptCodeProvider compiler = new JScriptCodeProvider();

            //CompilerParameters parameters;
            //parameters = new CompilerParameters();
            //parameters.GenerateInMemory = true;
         
            //CompilerResults results;
            //results = compiler.CompileAssemblyFromSource(parameters, _jscriptSource);

            //Assembly assembly = results.CompiledAssembly;
            //_evaluatorType = assembly.GetType("Evaluator.Evaluator");
         
            //_evaluator = Activator.CreateInstance(_evaluatorType);
		}

        private static Evaluator.Evaluator _eval = new Evaluator.Evaluator();
      
        //private static object _evaluator = null;
        //private static Type _evaluatorType = null;
		private static readonly string _jscriptSource = 
         
			@"package Evaluator
            {
               class Evaluator
               {
                  public function Eval(expr : String) : String 
                  { 
                     return eval(expr); 
                  }
               }
            }";
	}
}
