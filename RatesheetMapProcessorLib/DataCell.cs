using System;
using System.Collections;
using System.Text;
using DataAccess;
using System.Text.RegularExpressions;

namespace RatesheetMap
{
	public abstract class OutputCell
	{
		public virtual string Data 
		{ 
			get{ return ""; }
			set{ } 
		}
		public virtual bool IsValid
		{
			get { return true; }
		}
		public virtual bool HasData
		{
			get { return false; }
		}
		public abstract string RegionId
		{
			get;
		}
	}
	
	public class DataCell : OutputCell
	{
		#region Variables
		private string				m_regionId = "";
		private E_ReadLocations		m_source;
		private E_ReadLocations		m_destination;
		protected string			m_sourceVariableName = "";
		protected E_VariableContext	m_sourceVariableContext;
		private string				m_data;
		private string				m_unOperatedData;
		private int					m_rowOffset;
		private int					m_colOffset;
		private int					m_row;
		private int					m_column;
		private string				m_columnLetter;
		private bool				m_finalized = false;
		private ArrayList			m_mathOperators = new ArrayList();
		private ArrayList			m_ignoreSet = new ArrayList();

		public override string RegionId
		{
			get { return m_regionId; }
		}

		public bool Finalized
		{
			get { return m_finalized; }
		}

		public int Row
		{
			get { return m_row; }
		}

		public int Column
		{
			get { return m_column; }
		}

		public string ColumnLetter
		{
			get { return m_columnLetter; }
		}

		public E_ReadLocations Destination
		{
			get{ return m_destination; }
		}

		public E_ReadLocations Source
		{
			get{ return m_source; }
		}

		public E_VariableContext SourceVariableContext
		{
			get { return m_sourceVariableContext; }
		}

		public string SourceVariableName
		{
			get { return m_sourceVariableName; }
		}

		public override string Data
		{
			get{ return m_data; }
			set 
			{ 
				m_data = value; 
				m_unOperatedData = value;
			}
		}

		public string UnoperatedData
		{
			get{ return m_unOperatedData; }
		}

		public int RowOffset
		{
			get{ return m_rowOffset; }
		}

		public int ColumnOffset
		{
			get{ return m_colOffset; }
		}
		
		public override bool HasData
		{
			get
			{
				// If the data is coming from a variable that is not an array variable, we want to return
				// true here, but if it's an array variable, we want to treat it like regular data since
				// array variables are presumably taking a column of "regular" data that could be empty.
				if(m_source == E_ReadLocations.VARIABLE && !Variables.IsArrayVariable(m_sourceVariableName))
					return true;

				if(m_data == null || m_data.Equals(""))
					return false;

				return true;
			}
		}

		public override bool IsValid
		{
			get
			{
				if((m_data != null) && (!m_data.Equals("")) && !RatesheetMapCommon.IsNumeric(m_data))
					return false;
				
				switch(m_destination)
				{
					case E_ReadLocations.NR:
						if(m_data == null || m_data.Equals("") || !RatesheetMapCommon.IsNumeric(m_data) || double.Parse(m_data) <= 0)
							return false;
						break;
					case E_ReadLocations.P:
						if(m_data == null || m_data.Equals("") || !RatesheetMapCommon.IsNumeric(m_data))
							return false;
						break;
					case E_ReadLocations.M: //Margin can take any value as long as it's numeric, which will have been verified above
						break;
					case E_ReadLocations.QR:
					case E_ReadLocations.TR:
						if(m_data == null || m_data.Equals(""))
							return true;
						if(double.Parse(m_data) < 0)
							return false;
						break;
					case E_ReadLocations.VARIABLE:
						return true;
					default:
						throw new CBaseException(String.Format("Unable to determine validity of cell in region '{0}' - Invalid read tag destination '{1}'.  Cell Info: Row = {2}, Column = {3}, Data = '{4}'.", m_regionId, m_destination, this.Row, this.ColumnLetter, this.Data), "");
				}
				return true;
			}
		}
		#endregion

		public DataCell(string regionId, E_ReadLocations source, E_ReadLocations destination, string cellData, int row, int column, int rowOffset, int colOffset)
		{
			m_regionId = regionId;
			m_source = source;
			m_destination = destination;
			m_data = cellData;
			m_unOperatedData = cellData;
			m_row = row;
			m_column = column;
			m_columnLetter = RatesheetMapCommon.GetColumnLetterFromNumber(m_column);
			m_rowOffset = rowOffset;
			m_colOffset = colOffset;
			String[] valsToIgnore = Regex.Split("n/a,-,none,not eligible,na, ," , ",");
			FillIgnoreSet(valsToIgnore);
		}

		private void FillIgnoreSet(string[] valsToIgnore)
		{
			foreach(string valToIgnore in valsToIgnore)
			{
				m_ignoreSet.Add(valToIgnore.ToUpper());
			}
		}

		public void AddMathOperators(ArrayList mathOperators)
		{
			m_mathOperators.AddRange(mathOperators);
		}

		public void SetSourceVariableInfo(E_VariableContext sourceVariableContext, string sourceVariableName)
		{
			if(sourceVariableName.Trim().Equals(""))
				throw new CBaseException(String.Format("Invalid source variable name in region '{0}' - The source variable name is empty.", m_regionId), "");
			m_sourceVariableContext = sourceVariableContext;
			m_sourceVariableName = sourceVariableName;
		}

		private bool IsInIgnoreSet(string val)
		{
			return m_ignoreSet.Contains(val.ToUpper());
		}

		public void FinalizeData(ArrayList mathOperators, Variables variables, string[] rowData, bool useIgnoreSet, int relativeRow)
		{
			double cellValDouble;
			
			if(RatesheetMapCommon.IsNumeric(m_data))
			{
				bool valHasChanged = false;
				cellValDouble = double.Parse(m_data);
				try
				{
					foreach(MathTag op in mathOperators)
					{
						cellValDouble = op.ApplyFormula(cellValDouble, variables, rowData, relativeRow, m_ignoreSet);
						valHasChanged = true;
					}
				}
				catch(RatesheetMapNonFatalException)
				{
					// OPM 19144 - no longer throw an error here, just set a value that will cause the row to be removed
					m_data = "IGNOREFORMULAERROR";
				}
				catch(CBaseException c)
				{
					throw c;
				}
				if(valHasChanged)
					m_data = cellValDouble.ToString();
			}
			else
			{
				if(IsInIgnoreSet(m_data))
				{
					m_data = (useIgnoreSet)?"0":"";
					
					// Apply formula only if formula doesn't depend on input value (since to be here, the input value is not numeric, which means we cannot apply a formula to it)
					bool valHasChanged = false;
					cellValDouble = 0;
					try
					{
						foreach(MathTag op in mathOperators)
						{
							if(!op.DependsOnInput)
							{
								cellValDouble = op.ApplyFormula(cellValDouble, variables, rowData, relativeRow, m_ignoreSet);
								valHasChanged = true;
							}
						}
					}
					catch(RatesheetMapNonFatalException)
					{
						// OPM 19144 - no longer throw an error here, just set a value that will cause the row to be removed
						m_data = "IGNOREFORMULAERROR";
					}
					catch(CBaseException c)
					{
						throw c;
					}
					if(valHasChanged)
						m_data = cellValDouble.ToString();
				}
				else // only apply operators if the formula isn't using the data from the ratesheet (since to be here, the input value is not numeric, which means we cannot apply a formula to it)
				{
					string stringCellVal = m_data;
					try
					{
						foreach(MathTag op in mathOperators)
						{
							if(!op.DependsOnInput)
							{
								cellValDouble = op.ApplyFormula(stringCellVal, variables, rowData, relativeRow, m_ignoreSet);
								stringCellVal = cellValDouble.ToString();
							}
						}
					}
					catch(RatesheetMapNonFatalException)
					{
						// OPM 19144 - no longer throw an error here, just set a value that will cause the row to be removed
						stringCellVal = "IGNOREFORMULAERROR";
					}
					catch(CBaseException c)
					{
						throw c;
					}
					m_data = stringCellVal;
				}
			}
			m_finalized = true;
		}

		//This function can be expanded for all non-math operator related data finalization
		public void FinalizeData(bool useIgnoreSet)
		{
			if(useIgnoreSet && IsInIgnoreSet(m_data))
				m_data = "0";
			m_finalized = true;
		}

		public override string ToString()
		{
			return String.Format("ReadLocation: {0} Data: {1}", m_destination, m_data);
		}
	}
}
