using System;
using System.Collections;
using DataAccess;
using System.Diagnostics; 

namespace RatesheetMap
{
	public class MathTag : AbstractOperator
	{
		#region Variables
		const char		VariableSymbol = 'x';
		private string	m_formula = ""; //required
		
		public override E_OperatorTagType OperatorTagType
		{
			get { return E_OperatorTagType.Math; }
		}

		public bool DependsOnInput
		{
			get
			{ 
				string localFormula = String.Copy(m_formula);
				int endVarIndex = 0;
				try
				{
					while(true)
					{
						char[] varIndicators = {'%','$'};
						int varStartIndex = localFormula.IndexOfAny(varIndicators, 0);
						if(varStartIndex >= 0)
						{
							char var = 	localFormula[varStartIndex];
							if(var == '%')
								endVarIndex = localFormula.IndexOf('%', varStartIndex+1);
							else //var == '$'
								endVarIndex = localFormula.IndexOf('$', varStartIndex+1);

							if(endVarIndex < 0)
								throw new CBaseException("Invalid formula - could not parse variables", "");

							string varName = localFormula.Substring(varStartIndex, (endVarIndex - varStartIndex)+1);
							localFormula = localFormula.Replace(varName, "");
						}
						else
							break;
					}
				}
				catch
				{
					// default to false if there are any issues, because this just means that the formula will be applied, 
					// and if there are any issues, they will be caught there.
					return false; 
				}
				
				return localFormula.Replace("max", "").IndexOf('x') >= 0;
			}
		}
		#endregion

		public MathTag(string regionId, string formula, int rowOffset, int columnOffset) : base(regionId, rowOffset, columnOffset)
		{
			m_formula = formula.Replace("\\n", "");
			if(m_formula.Equals(""))
				throw new CBaseException(String.Format("Invalid math tag definition in region '{0}' - The 'formula' attribute is required for math tags.", regionId), "This is most likely from a copied math tag (which should have been validated upon creation).");
		}			
		
		public MathTag(string regionId, CellRep operatorCell, LandmarkCell anchorLandmarkInMap) : base(regionId, operatorCell, anchorLandmarkInMap)
		{
			try
			{
				m_formula = operatorCell.GetAttribute("formula").Replace("\\n", "");
			}
			catch(CBaseException c)
			{
				throw new CBaseException(String.Format("Unable to parse MathTag attribute in region '{0}'.  The location of the cell being parsed is map row {1}, column {2}.  DETAILS: {3}", regionId, operatorCell.Row, operatorCell.ColumnLetter, c.UserMessage), "");
			}

			if(m_formula.Equals(""))
				throw new CBaseException(String.Format("Invalid math tag definition in region '{0}' - The 'formula' attribute is required for math tags.  The invalid math tag is located at map row {1}, column {2} and its definition is '{3}'.  ", regionId, operatorCell.Row, operatorCell.ColumnLetter, operatorCell.Value), "");
		}

		public double ApplyFormula(double val, Variables variables, string[] rowData, int rowNum, ArrayList ignoreSet)
		{
            if (m_formula.Equals("x", StringComparison.OrdinalIgnoreCase))
            {
                // 8/24/2014 dd - If formula is just "X" then return val as-is.

                return val;
            }
            return ApplyFormula(val.ToString(), variables, rowData, rowNum, ignoreSet);
		}

		public double ApplyFormula(string val, Variables variables, string[] rowData, int rowNum, ArrayList ignoreSet)
		{
			string localFormula = String.Copy(m_formula);
			//Parse variables
			int endVarIndex = 0;
			try
			{
				while(true)
				{
					char[] varIndicators = {'%','$'};
					int varStartIndex = localFormula.IndexOfAny(varIndicators, 0);
					if(varStartIndex >= 0)
					{
						char var = 	localFormula[varStartIndex];
						if(var == '%')
							endVarIndex = localFormula.IndexOf('%', varStartIndex+1);
						else //var == '$'
							endVarIndex = localFormula.IndexOf('$', varStartIndex+1);

						if(endVarIndex < 0)
							throw new CBaseException("Invalid formula - could not parse variables", "");

						string varName = localFormula.Substring(varStartIndex, (endVarIndex - varStartIndex)+1);
						string varValue = variables.GetVariableValue(m_regionId, varName, rowNum-1); // We have to subtract 1 because rowNum starts at 1, but normal indices start at 0.  This is a result of using excel, which starts indices at 1.
						
						localFormula = localFormula.Replace(varName, varValue);
						// OPM 19144 - if one of the variable values is in our ignore set, we will throw a non fatal
						// error that will cause the row to be removed silently (ie: no error will be thrown)
						if(ignoreSet.Contains(varValue.Trim()))
						{
							throw new RatesheetMapNonFatalException("Invalid variable value found in formula: " + localFormula + ", however, this value is part of the allowed ignore set and can be safely ignored.", "");
						}
					}
					else
						break;
				}

				//Handle Max/Min manually since the built-in eval doesn't support them
				if(localFormula.StartsWith("max"))
				{
					localFormula = localFormula.Replace("max", "").Replace("x", " " + val + " ");
					localFormula = ParseColumnVariables(localFormula, rowData);
					return ParseFunction(localFormula.Trim(), E_FormulaFunctionType.MAX);
				}
				else if(localFormula.StartsWith("min"))
				{
					localFormula = localFormula.Replace("min", "").Replace("x", " " + val + " ");
					localFormula = ParseColumnVariables(localFormula, rowData);
					return ParseFunction(localFormula.Trim(), E_FormulaFunctionType.MIN);
				}
				else
				{
					string completedFormula = localFormula.Replace("x", " " + val + " ");
					completedFormula = ParseColumnVariables(completedFormula, rowData);
					return FormulaEvaluator.EvalToDouble(completedFormula.Trim());
				}
			}
			catch(RatesheetMapNonFatalException r)
			{
				throw r;
			}
			catch(CBaseException c)
			{
				throw new CBaseException("Error attempting to apply formula: " + c.UserMessage, c.DeveloperMessage);
			}
			catch(Exception e)
			{
				throw new CBaseException(String.Format("Invalid formula - unable to parse variables in formula '{0}'.  Final parsed value is '{1}'.", m_formula, localFormula), e);
			}
		}

		// Replaces column names in formulas with the actual value from that column
		private string ParseColumnVariables(string formula, string[] rowData)
		{
            foreach (var readLocationItem in RatesheetMapCommon.GetReadLocationDefinitionList())
            {
                if (formula.IndexOf(readLocationItem.Key) >= 0)
                {
                    formula = formula.Replace(readLocationItem.Key, rowData[readLocationItem.Value]);
                }
            }

			return formula;
		}

		private double ParseFunction(string toEval, E_FormulaFunctionType functionType)
		{
			double returnVal = 0;
			bool isSet = false;
			int indexOpenParen = toEval.IndexOf('(');
			int indexCloseParen = toEval.LastIndexOf(')');
			if(indexOpenParen < 0 || indexCloseParen < 0)
				throw new CBaseException("Invalid formula in math operator - mismatch in parens", "");

			char[] trimChars = {'(', ')'};
			string inner = toEval.Substring(indexOpenParen+1, indexCloseParen-1);

			char[] delim = {','};
			string[] tokens = inner.Split(delim);
			
			string[] result = new string[tokens.Length];
			for(int i = 0; i < tokens.Length; ++i)
			{
				if(functionType == E_FormulaFunctionType.MAX)
				{
					double val = FormulaEvaluator.EvalToDouble(tokens[i]);
					if(val > returnVal || !isSet)
					{
						returnVal =  val;
						isSet = true;
					}
				}
				else if(functionType == E_FormulaFunctionType.MIN)
				{
					double val = FormulaEvaluator.EvalToDouble(tokens[i]);
					if(val < returnVal || !isSet)
					{
						returnVal = val;
						isSet = true;
					}
				}
				else
					throw new CBaseException("Invalid function used in math tag", "");
			}

			return returnVal;
		}

		public override AbstractOperator GetCopy()
		{
			return new MathTag(this.RegionId, m_formula, this.RowOffset, this.ColumnOffset);
		}

		public override string ToString()
		{
			return RatesheetMapCommon.GetTabString(3) + String.Format("Op Type: Math, Formula: {0}", m_formula);
		}
	}
}