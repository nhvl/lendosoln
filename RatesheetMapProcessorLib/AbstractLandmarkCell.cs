using System;
using System.Collections;
using DataAccess;
using System.Text;
using System.Diagnostics; 

namespace RatesheetMap
{
    /// <summary>
    /// Only 1 of the landmark cells is considered the anchor (it doesn't matter which, and is just the first one
    /// it finds).  The row and column offset for the anchor are both 0 since it's the anchor.  Other landmark cells have 
    /// a row and column offset that is in relation to the anchor.  So if another landmark cell is located directly underneath
    /// the anchor, it would have a row offset of +1 and a column offset of 0.  If it were directly above the anchor, it would
    /// have a row offset of -1 and a column offset of 0.
    /// </summary>
    
    [DebuggerDisplay("LandMarkTag: IsAnchor={isAnchor}, IsMatched={matchedInRSFile}, RowOffset={rowOffset}, ColOffset={colOffset}, SearchString={m_searchS}, Wildcard={m_wildcard}")]
	public abstract class LandmarkCell : Tag
	{
		#region Variables
		string m_searchS, m_wildcard;
		int rowOffset, colOffset;
		bool isAnchor, matchedInRSFile;
		public abstract void ValidateInput(CellRep cell);
		public abstract void ValidateInput(RatesheetCellRep cell);

		public String SearchString
		{
			get{ return m_searchS; }
		}
		public String Wildcard
		{
			get{ return m_wildcard; }
		}
		public int RowOffset
		{
			get{ return rowOffset; }
		}
		public int ColumnOffset
		{
			get{ return colOffset; }
		}
		public Boolean IsAnchor
		{
			get { return isAnchor; }
		}
		// This variable is used because if we initially find multiple matching regions based on the landmarks, we 
		// will continue matching landmarks in the map file until either only one region matches, or we detetermine multiple
		// regions match, which will return an error.  If we eliminate down to just 1 region, that region could have already
		// had many, if not all, landmarks matched in the map, so we don't want to try and match those again.
		public Boolean IsMatchedInMap
		{
			get { return matchedInRSFile; }
			set { matchedInRSFile = value; }
		}
		#endregion

		public LandmarkCell(string regionId, E_LandmarkType landmarkType, RatesheetCellRep range, bool isAnchorLandmark, int anchorRow, int anchorCol) : base(E_TagType.Landmark, regionId)
		{
			m_regionId = regionId;
			m_searchS = "";
			m_wildcard = "";
			isAnchor = isAnchorLandmark;
			m_row = range.Row;
			m_col = range.Column;
			if(isAnchor)
			{
				rowOffset = 0;
				colOffset = 0;
			}
			else
			{
				rowOffset = range.Row - anchorRow;
				colOffset = range.Column - anchorCol;
			}

			matchedInRSFile = false;
			
			//TODO - move this to the newly added MapLandmarkCell as it now makes more sense there (just for proper object oriented principles, will not affect execution in any way).
			if(landmarkType == E_LandmarkType.Map)
			{
				m_searchS = Tag.GetAttribute("str", range);
				m_wildcard = Tag.GetAttribute("wildcard", range);
			}

			ValidateInput(range);
		}

		public LandmarkCell(string regionId, E_LandmarkType landmarkType, CellRep cell, bool isAnchorLandmark, int anchorRow, int anchorCol) : base(E_TagType.Landmark, regionId)
		{
			m_regionId = regionId;
			m_searchS = "";
			m_wildcard = "";
			isAnchor = isAnchorLandmark;
			m_row = cell.Row;
			m_col = cell.Column;
			if(isAnchor)
			{
				rowOffset = 0;
				colOffset = 0;
			}
			else
			{
				rowOffset = cell.Row - anchorRow;
				colOffset = cell.Column - anchorCol;
			}
			matchedInRSFile = false;

            //TODO - move this to the newly added MapLandmarkCell as it now makes more sense there (just for proper object oriented principles, will not affect execution in any way).
			if(landmarkType == E_LandmarkType.Map)
			{
				m_searchS = cell.GetAttribute("str");
				m_wildcard = cell.GetAttribute("wildcard");
			}

			ValidateInput(cell);
		}

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			if(isAnchor)
				sb.AppendFormat("(ANCHOR) {0} ---> Row: {1} Col: {2}", m_searchS, rowOffset, RatesheetMapCommon.GetColumnLetterFromNumber(colOffset));
			else
				sb.AppendFormat("{0} ---> RowOffset: {1} ColOffset: {2}", m_searchS, rowOffset, colOffset);
			return sb.ToString();
		}
	}
}
