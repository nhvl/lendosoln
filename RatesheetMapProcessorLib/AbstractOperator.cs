using System;
using System.Collections;
using DataAccess;
using System.Diagnostics; 

namespace RatesheetMap
{
    /// <summary>
    /// Operator tags affect all read tags in the same row and column that the operator tag is located in.  Generally,
    /// the SAE creates the maps so that only 1 dimension is affected by the operator (for example, the operator will sit
    /// on top of a column of data and there will be no read tags to the side of it for that region).
    /// </summary>
    [DebuggerDisplay("OperatorTag: Type={OperatorTagType}, RowOffset={m_rowOffset}, ColOffset={m_colOffset}")]
	public abstract class AbstractOperator : Tag
	{
		#region Variables
		//offsets from the anchor landmark
		private int m_rowOffset = 0;
		private int m_colOffset = 0;
		public abstract E_OperatorTagType OperatorTagType{ get; }
		
		public int RowOffset
		{
			get { return m_rowOffset; }
		}

		public int ColumnOffset
		{
			get { return m_colOffset; }
		}
		#endregion
		public abstract override string ToString();
		public abstract AbstractOperator GetCopy();

        // Factory for creating the different types of operator tags, which as of Oct 2010 can only be math, group, or ruleattribute operators.
		public static AbstractOperator CreateOperatorTag(string regionId, E_RegionType regionType, CellRep operatorCell, LandmarkCell anchorLandmarkInMap)
		{
			string val= "";
			try
			{
				val = operatorCell.GetAttribute("type");
			}
			catch(CBaseException c)
			{
				throw new CBaseException(String.Format("Unable to parse Operator 'type' attribute in region '{0}'.  The location of the cell being parsed is map row {1}, column {2}.  DETAILS: {3}", regionId, operatorCell.Row, operatorCell.ColumnLetter, c.UserMessage), "");
			}

			try
			{
				E_OperatorTagType opTagType = (E_OperatorTagType) Enum.Parse(typeof(E_OperatorTagType), val, true); 
				switch(opTagType)
				{
					case E_OperatorTagType.Math:
						return new MathTag(regionId, operatorCell, anchorLandmarkInMap);
					case E_OperatorTagType.Group:
                        // Adjustment table regions can't have group operators because the groups are things like lock period, prepayment penalty period, etc, and these don't 
                        // make sense in adjustment tables, they are only for rate option groupings.
                        if (regionType == E_RegionType.AdjustTable)
                        {
                            throw new CBaseException(String.Format("Error creating group operator in region '{0}' - Adjustment Table regions cannot define group tags.", regionId), "");
                        }
                        return new GroupTag(regionId, operatorCell, anchorLandmarkInMap);
					default:
						throw new CBaseException(String.Format("Invalid Operator Type '{0}' in Region '{1}'", opTagType, regionId), "");
				}	
			}
			catch(CBaseException c)
			{
				throw c;
			}
			catch{} //intentionally swallowing - not elegant.  If the code makes it to the end of this function, then there's an error and an exception will be thrown for real there.

			try
			{
				E_RuleAttributeType operatorType = (E_RuleAttributeType) Enum.Parse(typeof(E_RuleAttributeType), val, true); 
				return new RuleAttribute(regionId, operatorCell, anchorLandmarkInMap);
			}
			catch(CBaseException c)
			{
				throw c;
			}
            catch { } //intentionally swallowing - not elegant.  If the code makes it to the end of this function, then there's an error and an exception will be thrown for real there.
			
			throw new CBaseException(String.Format("Invalid Operator or RuleAttribute type '{0}' in region '{1}' located at map row {2}, column {3}.", val, regionId, operatorCell.Row, operatorCell.ColumnLetter), "");
		}

		public AbstractOperator(string regionId, CellRep operatorCell, LandmarkCell anchorLandmarkInMap) : base(E_TagType.Operator, regionId)
		{
			int row = operatorCell.Row;
			int col = operatorCell.Column;
			
			m_rowOffset = row - anchorLandmarkInMap.Row;
			m_colOffset = col - anchorLandmarkInMap.Column;
		}

		public AbstractOperator(string regionId, int rowOffset, int colOffset) : base(E_TagType.Operator, regionId)
		{
			m_rowOffset = rowOffset;
			m_colOffset = colOffset;
		}

		public AbstractOperator(string regionId) : base(E_TagType.Operator, regionId)
		{
		}
	}
}
