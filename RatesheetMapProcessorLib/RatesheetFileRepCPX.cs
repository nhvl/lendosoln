namespace RatesheetMap
{
    using System;
    using System.Collections;
    using System.IO;
    using System.Text;
    using DataAccess;
    using ExcelLibraryClient;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Gateways;

    public class RatesheetFileRepCPX : RatesheetFileRep
	{
		public override void ImportRatesheet(string ratesheetPath)
		{
			m_ratesheetPath = ratesheetPath;
			m_ratesheetName = System.IO.Path.GetFileName(ratesheetPath);

            if (RatesheetMapCommon.IsCPXFile(ratesheetPath)) // OPM 32391
            {
                CreateRepFromCPXFile(m_ratesheetPath);
            }
            else
            {
                string outfilePath = System.IO.Path.GetDirectoryName(ratesheetPath);
                string outputFileName = System.IO.Path.Combine(outfilePath, System.IO.Path.GetFileNameWithoutExtension(ratesheetPath) + ".CPX");
                using (PerformanceStopwatch.Start("ExcelLibrary.ConvertToCompactFormat"))
                {
                    ExcelLibrary.ConvertToCompactFormat(ratesheetPath, outputFileName);
                }

                CreateRepFromCPXFile(outputFileName);
                FileOperationHelper.Delete(outputFileName);
            }
		}

		private void CreateRepFromCPXFile(string filename)
		{
			if(!FileOperationHelper.Exists(filename))
				return;
			
			try
			{
				using(StreamReader sR = new StreamReader( filename ))
				{
					RatesheetWorksheet ratesheetRS = null;

					try
					{
						string line = "";
						while(line != null)
						{
							line = sR.ReadLine();
							ProcessLine(line, ref ratesheetRS);
						}
					}
					catch( EndOfStreamException )
					{
					}
				}
			}
			catch(CBaseException c)
			{
				throw c;
			}
			catch(Exception e)
			{
				throw new CBaseException("Unable to read from input file", e);
			}
		}

		
		private void ProcessLine(string line, ref RatesheetWorksheet worksheet)
		{
			if(line == null)
				return;

			line = line.ToLower();

			if(line.StartsWith("w:")) //New Worksheet
			{
				line = line.Trim().Remove(0,2);
				worksheet = new RatesheetWorksheet(line, m_ratesheetName);
				m_worksheets.Add(line, worksheet);
			}
			else
			{
				if(worksheet == null)
				{
					throw new CBaseException("Unable to process input CPX file.", "CPX file not created correctly - Does not start with a worksheet name.");
				}
				else
				{
					try
					{
						AddCell(worksheet, line);
					}
					catch(Exception e)
					{
						throw e;
					}
				}
			}
		}

		private void AddCell(RatesheetWorksheet worksheet, string line)
		{
			if(line == null)
				return;

			int row = -1;
			int col = -1;

			try
			{
				int index1 = line.IndexOf(",");
				if(index1 >= 0)
				{
					string rowStr = line.Substring(0, index1);
					row = int.Parse(rowStr);
				}

				int index2 = line.IndexOf(",", index1+1);

				if(index2 >= 0)
				{
					int startAt = index1 + 1;
					string colStr = line.Substring(startAt, index2 - startAt);
					col = int.Parse(colStr);
				}

				RatesheetCellRep cellRep = new RatesheetCellRep(row, col, line.Substring(index2 + 1));
				worksheet.AddCell(cellRep);
			}
			catch(CBaseException c)
			{
				throw new CBaseException(String.Format("Unable to add cell to worksheet representation of tab '{0}' from compressed ratesheet '{2}' - {1}", worksheet.WorksheetName, c.UserMessage, m_ratesheetName), c.DeveloperMessage);
			}
			catch(Exception e)
			{
				throw new CBaseException(String.Format("Unable to add cell to worksheet representation of tab '{0}' from compressed ratesheet '{2}' - {1}", worksheet.WorksheetName, e.Message, m_ratesheetName), e);
			}
		}
	}
}