using System;
using System.Collections;
using System.Text;
using DataAccess;
using System.IO;
using LendersOffice.Constants;

namespace RatesheetMap
{
	public class MapVariables
	{
		private Hashtable		m_mapVariables;
		
		public MapVariables()
		{
			m_mapVariables = new Hashtable();
		}

		public void Clear()
		{
			m_mapVariables = new Hashtable();
		}

		public bool Contains(string varName)
		{
			return m_mapVariables.ContainsKey(varName);
		}

		public void AddVariables(Hashtable mapVariables)
		{
			if(mapVariables == null)
				return;
			
			foreach(string key in mapVariables.Keys)
			{
				if(Variables.IsArrayVariable(key))
				{
					AddArrayVariable(key, (ArrayList)mapVariables[key]);
				}
				else
				{
					if(m_mapVariables.ContainsKey(key))
						m_mapVariables[key] = mapVariables[key];
					else
						m_mapVariables.Add(key, mapVariables[key]);
				}
			}
		}

		// This function will handle either an array or non-array variable
		public void AddVariable(string varName, string val)
		{
			if(Variables.IsArrayVariable(varName))
			{
				AddArrayVariable(varName, val);
			}
			else
			{
				AddNonArrayVariable(varName, val);
			}
		}

		public void AddNonArrayVariable(string varName, string val)
		{
			if(Variables.IsArrayVariable(varName))
				throw new CBaseException(String.Format("Error while attempting to add non-array map variable '{0}' - this variable name has the syntax of an array variable (ie: ends with '[]').", varName), "");
			
			if(m_mapVariables.ContainsKey(varName))
				m_mapVariables[varName] = val;
			else
				m_mapVariables.Add(varName, val);
		}

		public void AddArrayVariable(string varName, string val)
		{
			if(!Variables.IsArrayVariable(varName))
				throw new CBaseException(String.Format("Error while attempting to add array map variable '{0}' - this variable name does not have the syntax of an array variable (ie: ends with '[]').", varName), "");
			
			
			if(m_mapVariables.ContainsKey(varName))
				((ArrayList)m_mapVariables[varName]).Add(val);
			else
			{
				ArrayList temp = new ArrayList();
				temp.Add(val);
				m_mapVariables.Add(varName, temp);
			}
		}

		public void AddArrayVariable(string varName, ArrayList val)
		{
			if(!Variables.IsArrayVariable(varName))
				throw new CBaseException(String.Format("Error while attempting to add array map variable '{0}' - this variable name does not have the syntax of an array variable (ie: ends with '[]').", varName), "");
			
			if(m_mapVariables.ContainsKey(varName))
				((ArrayList)m_mapVariables[varName]).AddRange(val);
			else
			{
				m_mapVariables.Add(varName, val);
			}
		}

		public string GetNonArrayVariableValue(string varName)
		{
			if(Variables.IsArrayVariable(varName))
				throw new CBaseException(String.Format("Error while attempting to retrieve non-array map variable '{0}' - this variable name has the syntax of an array variable (ie: ends with '[]').", varName), "");
			
			if(m_mapVariables.ContainsKey(varName))
				return m_mapVariables[varName].ToString();
			else
				throw new CBaseException(String.Format("Could not find Map Variable '{0}'.", varName), "");
		}

		public ArrayList GetArrayVariable(string varName)
		{
			if(!Variables.IsArrayVariable(varName))
				throw new CBaseException(String.Format("Error while attempting to retrieve non-array map variable '{0}' - this variable name has the syntax of an array variable (ie: ends with '[]').", varName), "");
			
			if(m_mapVariables.ContainsKey(varName))
				return (ArrayList)m_mapVariables[varName];
			else
				throw new CBaseException(String.Format("Could not find Map Variable '{0}'.", varName), "");
		}

		public string GetArrayVariableAtIndex(string varName, int index)
		{
            if (!Variables.IsArrayVariable(varName))
            {
                throw new CBaseException(String.Format("Error while attempting to retrieve non-array map variable '{0}' - this variable name has the syntax of an array variable (ie: ends with '[]').", varName), "");
            }

            if (m_mapVariables.ContainsKey(varName))
            {
                ArrayList temp = (ArrayList)m_mapVariables[varName];
                if (temp.Count <= index)
                {
                    return null;
                }
                else
                {
                    return temp[index].ToString();
                }
            }
            else
            {
                throw new CBaseException(String.Format("Could not find Map Variable '{0}'.", varName), "");
            }
		}

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (string key in m_mapVariables.Keys)
            {
                sb.Append(Environment.NewLine + key + " -> " + m_mapVariables[key].ToString());
            }

            return sb.ToString();
        }
	}
}