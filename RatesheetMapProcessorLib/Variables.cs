using System;
using System.Collections;
using System.Text;
using DataAccess;
using System.IO;
using LendersOffice.Constants;

namespace RatesheetMap
{
	/**
	 * This Variables class is responsible for handling all variable requests and delegating them to the proper
	 * objects.  There are 3 types of variables available:
	 * 
	 * 1)	System Variables - these variables are pre-defined using the MarketIndex.xls file, and therefore their values
	 *		cannot be set by the map, nor can new system variables be created via the map.  These are typically index
	 *		variables, such as "1 YR LIBOR" and "PRIME RATE".  The syntax for using a system variable is:
	 *		$1 YR LIBOR$
	 * 
	 * 2)	Map Variables - these variables can be defined by the map and have the same syntax as system variables.
	 *		They are available for all maps that are processing the same ratesheet.
	 * 
	 * 3)	Region Variables - these variables can also be defined by the map, but have a different syntax from map/system
	 *		variables.  The syntax for a region variable is %Variable Name%.  Region variables are only available
	 *		in the region in which they were defined.
	 * 
	 * Each Variable type has an object that handles the requests for that type.  Note that map and region variables can
	 * be "arrays", meaning one variable name contains an array of data.  System variables, however, can never be
	 * array variables.  Array variables are designated by a [] appended to the end of the variable name, such as
	 * "%Variable Name[]%"
	 */
	
	public class Variables
	{
		#region Class Variables
		private MapVariables		m_mapVariables;
		private SystemVariables		m_systemVariables;
		private RegionVariables		m_regionVariables;

		public bool AccessedSystemVariables
		{
			get { return m_systemVariables.AccessedSystemVariables; }
		}
		#endregion

		public Variables(bool isStandAloneMode)
		{
			m_mapVariables = new MapVariables();
            m_systemVariables = SystemVariables.GetGlobalSystemVariables();// new SystemVariables();
			m_regionVariables = new RegionVariables();
		}

		public static bool IsArrayVariable(string varName)
		{
			string strippedVarName = varName;

			if(varName.StartsWith("%"))
			{
				char[] trimChar = {'%'};
				strippedVarName = strippedVarName.Trim(trimChar);
			}
			else if(varName.StartsWith("$"))
			{
				char[] trimChar = {'$'};
				strippedVarName = strippedVarName.Trim(trimChar);
			}
			
			return strippedVarName.EndsWith("[]");
		}

		#region Map Variables

		public void ClearMapVariables()
		{
			m_mapVariables.Clear();
		}

		public void AddMapVariable(string varName, string val)
		{
			m_mapVariables.AddVariable(varName, val);
		}

		#endregion

		#region Region Variables

		public Hashtable GetRegionVariables(string regionId)
		{
			return m_regionVariables.GetAll(regionId);
		}

		public void SetRegionVariables(string regionId, Hashtable regionVariables)
		{
			m_regionVariables.SetVariables(regionId, regionVariables);
		}

		public void ClearRegionVariables(string regionId)
		{
			m_regionVariables.Clear(regionId);
		}

		public string GetNonArrayRegionVariable(string regionId, string varName)
		{
			return m_regionVariables.GetNonArrayVariable(regionId, varName);
		}

		public void AddRegionVariable(string regionId, string varName, string val)
		{
            m_regionVariables.AddVariable(regionId, varName, val);
		}

		public bool ContainsRegionVariable(string regionId, string varName)
		{
			return m_regionVariables.Contains(regionId, varName);
		}
		#endregion

		public string GetNonArrayMapOrSystemVariable(string varName)
		{
			if(varName.EndsWith("[]"))
				throw new CBaseException(String.Format("Error while attempting to retrieve non-array map/system variable '{0}' - this variable name has the syntax of an array variable (ie: ends with '[]').", varName), "");
			
			if(m_mapVariables.Contains(varName))
				return m_mapVariables.GetNonArrayVariableValue(varName);
			else if(m_systemVariables.Contains(varName))
				return m_systemVariables.GetValue(varName);
			else
				return null;
		}

		public bool ContainsMapOrSystemVariable(string varName)
		{
			return (m_mapVariables.Contains(varName) || m_systemVariables.Contains(varName));
		}
		
		public string GetVariableValue(string regionId, string varName, int rowNum, E_VariableContext varContext)
		{
			if(varContext == E_VariableContext.Region)
			{
				char[] trimChar = {'%'};
				varName = varName.Trim(trimChar);

				if(IsArrayVariable(varName))
					return m_regionVariables.GetArrayVariableAtIndex(regionId, varName, rowNum);
				else
					return m_regionVariables.GetNonArrayVariable(regionId, varName);
			}
			else if(varContext == E_VariableContext.Map)
			{
				char[] trimChar = {'$'};
				varName = varName.Trim(trimChar);
				
				if(m_mapVariables.Contains(varName))
				{
					if(IsArrayVariable(varName))
						return m_mapVariables.GetArrayVariableAtIndex(varName, rowNum);
					else
						return m_mapVariables.GetNonArrayVariableValue(varName);
				}
				else if(m_systemVariables.Contains(varName))
				{
					if(IsArrayVariable(varName))
						throw new CBaseException(String.Format("Error trying to retrieve system variable '{0}' - the variable syntax is that of an array variable (ie: ends with '[]'), but system variables cannot be array variables.", varName), "");
					else
						return m_systemVariables.GetValue(varName);
				}
				else
					throw new CBaseException(String.Format("Could not find Map/System Variable '{0}'.", varName), "");
			}
			else
				throw new CBaseException(String.Format("Invalid variable context for variable '{0}'.  The context is '{1}'.", varName, varContext), "");
		}

		public string GetVariableValue(string regionId, string varName, int rowNum)
		{
			if(varName.StartsWith("%"))
				return GetVariableValue(regionId, varName, rowNum, E_VariableContext.Region);
			else if(varName.StartsWith("$"))
				return GetVariableValue(regionId, varName, rowNum, E_VariableContext.Map);
			else
				throw new CBaseException(String.Format("Invalid variable name syntax for variable '{0}'.  Variable name must begin with '%' or '$'.", varName), "");
		}

		// NOTE - this function, unlike all the others, requires the variable to have its context string on it (% or $)
        // to be able to figure out what the context should be.  You can call the other GetVariableValue (GetVariableValue(string regionId, string varName, int rowNum, E_VariableContext varContext))
        // function directly if you already know what the context is.
		public string GetVariableValue(string regionId, string varName)
		{
			if(varName.StartsWith("%"))
			{
				char[] trimChar = {'%'};
				varName = varName.Trim(trimChar);
				
				return m_regionVariables.GetNonArrayVariable(regionId, varName);
			}
			else if(varName.StartsWith("$"))
			{
				char[] trimChar = {'$'};
				varName = varName.Trim(trimChar);
				
				if(m_mapVariables.Contains(varName))
					return m_mapVariables.GetNonArrayVariableValue(varName);
				else if(m_systemVariables.Contains(varName))
					return m_systemVariables.GetValue(varName);
				else
					throw new CBaseException(String.Format("Could not find Map/System Variable '{0}'.", varName), "");
			}
			else
				throw new CBaseException(String.Format("Invalid variable name syntax for variable '{0}'.  Variable name must begin with '%' or '$'.", varName), "");
		}

        public override string ToString()
        {
            return "SYSTEM VARIABLES:" + Environment.NewLine + Environment.NewLine + m_systemVariables.ToString() + Environment.NewLine + Environment.NewLine + "MAP VARIABLES:" + Environment.NewLine + Environment.NewLine + m_mapVariables.ToString() + Environment.NewLine + Environment.NewLine + "REGION VARIABLES:" + Environment.NewLine + Environment.NewLine + m_regionVariables.ToString();
        }
	}
}