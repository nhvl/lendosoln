using System;
using System.Collections;
using System.Text;
using DataAccess;
using System.Text.RegularExpressions;
using System.Diagnostics; 

namespace RatesheetMap
{
	public interface OutputGroup
	{
		void		FinalizeData(Operators operators, Variables variables);
		void		AddColumn(OutputColumn oColumn);
		int			WriteDataToWorksheet2D(OutputWriter outputWriter, ref int startRow, ref int startCol, ref bool hasIO, bool firstFragment, bool ignoreHasIO, bool ignoreNonIOHeaders);
		GroupTag	RegionFilter{ get; }
		Hashtable	RegionVariables { set; }
		bool		IsContainer{ get; }
		ArrayList	Columns{ get; } //only gets the columns in the group
		ArrayList	AllColumns{ get; } //returns all columns in all groups
		ArrayList	WarningList{ get; } 
		bool		HasContainers{ get; }
		bool		HasData{ get; }
		string		RegionId{ get; }
		void		FinalizeCellValues(Variables variables);
		GroupTag	GetFirstGroupTag();
		GroupTag	GetFirstGroupTagByType(E_OperatorAttributeType attributeType);
		int			WriteDataToWorksheet1D(OutputWriter outputWriter, int startRow, ArrayList headers);
	}

    /// <summary>
    /// In the first revision of this output process, we were writing 2D output, but it was not at all machine-friendly and was extremely ugly
    /// I noticed that the SAE's weren't even using 2D output, so I switched to making the much friendlier 1D output.  1D output is what the importer takes
    /// anyways, and previously the 2D output was just translated to 1D output for the importer, so it was better to just skip that step.
    /// 2D output was only designed to make the output more human-readable.
    /// 
    /// For the output, groups of data are collected together and organized into the lock period groups, pp groups, IO groups, etc.
    /// Each group/container contains sub groups/containers, and so on, until the very inner grouping which houses the actual rate options.
    /// </summary>
    [DebuggerDisplay("OutputData: RegionId={m_regionId}, IsRoot={m_isRoot}, ThrowErrors={m_throwInvalidRowErrors}")]
	public class OutputData : OutputGroup
	{
		#region Variables
		private string		m_regionId;
		private ArrayList	m_groups;
		private ArrayList	m_commonColumns; //Columns that aren't filtered, but need to be included in all regions (such as NR typically)
		private	GroupTag	m_filter;
		private Hashtable	m_regionVariables = new Hashtable();
		private bool		m_hasContainers;
		private bool		m_hasData = false;
		private ArrayList	m_allColumns = new ArrayList();
		private bool		m_isRoot = false;
		private bool		m_throwInvalidRowErrors = false; // OPM 19544
		protected ArrayList	m_warningList = new ArrayList();
		
		public string RegionId
		{
			get { return m_regionId; }
		}

		public ArrayList WarningList
		{
			get { return m_warningList; }
		}

		public ArrayList Columns
		{
			get { return new ArrayList(); }
		}

		public ArrayList AllColumns
		{
			get { return m_allColumns; }
		}

		public bool HasData
		{
			get { return m_hasData; }
		}

		public string Title
		{
			get 
			{ 
				if(m_filter == null)
					return "";
				string description = RatesheetMapCommon.GetDescription(m_filter.AttributeType);
				//return m_filter.AttributeType.ToString().ToUpper() + " " + m_filter.RangeFrom + ":" + m_filter.RangeTo;
				return description + ": " + m_filter.RangeFrom + "-" + m_filter.RangeTo;
			}
		}
		
		public ArrayList OutputRegions
		{
			get { return m_groups; }
		}

		public bool IsContainer
		{
			get { return true; }
		}

		public bool HasContainers
		{
			get { return m_hasContainers; }
		}

		public GroupTag RegionFilter
		{
			get { return m_filter; }
		}
		
		public Hashtable RegionVariables
		{
			get { return m_regionVariables; }
			set { m_regionVariables = value; }
		}
		#endregion

		public GroupTag GetFirstGroupTagByType(E_OperatorAttributeType attributeType)
		{
			GroupTag first = null;
			if(m_filter == null || (m_filter.AttributeType != attributeType))
			{
				foreach(OutputGroup oRegion in m_groups)
				{
					GroupTag temp = oRegion.GetFirstGroupTagByType(attributeType);
					if(first == null)
					{
						if(temp != null && temp.AttributeType == attributeType)
							first = temp;
					}
					else
					{
						if(temp == null)
							continue;
						else
						{
							if(temp.OperatorTagType != first.OperatorTagType)
								throw new CBaseException("Mismatch in group tags found when attempting to get the first group tag by type in region: " + m_regionId, "");
							else
							{
								if((temp.AttributeType == first.AttributeType) && (temp.RangeFrom < first.RangeFrom))
									first = temp;
							}
						}
					}
				}
			}
			else
				first = m_filter;

			return first;
		}

		public GroupTag GetFirstGroupTag()
		{
			GroupTag first = null;
			if(m_filter == null)
			{
				foreach(OutputGroup oRegion in m_groups)
				{
					GroupTag temp = oRegion.GetFirstGroupTag();
					if(first == null)
						first = temp;
					else
					{
						if(temp == null)
							continue;
						else
						{
							if(temp.OperatorTagType != first.OperatorTagType)
								throw new CBaseException("Mismatch in group tags found when attempting to get the first group tag in region: " + m_regionId, "");
							else
							{
								if(temp.RangeFrom < first.RangeFrom)
									first = temp;
							}
						}
					}
				}
			}
			else
				first = m_filter;

			return first;
		}
		
		public OutputData(string regionId, OutputColumn oColumn, ArrayList commonColumns, GroupTag filter, bool throwInvalidRowErrors)
		{
			m_regionId = regionId;
			m_groups = new ArrayList();
			m_commonColumns = commonColumns;
			m_filter = filter;
			m_hasContainers = false;
			m_throwInvalidRowErrors = throwInvalidRowErrors;
			AddColumn(oColumn);
		}

		public OutputData(string regionId, OutputColumn oColumn, OutputGroup oGroup, GroupTag filter, bool throwInvalidRowErrors)
		{
			m_regionId = regionId;
			m_throwInvalidRowErrors = throwInvalidRowErrors;
			m_groups = new ArrayList();
			if(oGroup.HasContainers)
			{
				//TODO - get index here - just for convenience to make a more useful error message.
				throw new CBaseException(String.Format("Unable to add column to output data in region '{0}' - You must declare the same set of groups for all columns in a region.", m_regionId), "");
			}
			else
			{
				m_commonColumns = oGroup.Columns;
			}

			m_filter = filter;
			m_hasContainers = false;
			AddColumn(oColumn);
		}

		public OutputData(string regionId, bool isRoot, bool throwInvalidRowErrors)
		{
			m_regionId = regionId;
			m_groups = new ArrayList();
			m_commonColumns = new ArrayList();
			m_filter = null;
			m_hasContainers = false;
			m_isRoot = isRoot;
			m_throwInvalidRowErrors = throwInvalidRowErrors;
		}

		public void AddWarning(string warning, bool logMessage)
		{
			m_warningList.Add(warning);
			if(logMessage)
				LogHelper.LogWarning(warning);
		}

		private void AddWarnings(ArrayList warnings, bool logMessages)
		{
			foreach(string warning in warnings)
			{
				m_warningList.Add(warning);
				if(logMessages)
					LogHelper.LogWarning(warning);
			}
		}

		public void ClearWarningsList()
		{
			m_warningList = new ArrayList();
		}

		public void FinalizeCellValues(Variables variables)
		{
			//TODO - remove this required function - just for code clarity
		}

		public void FinalizeData(Operators operators, Variables variables)
		{
			if(m_groups.Count == 0 && m_commonColumns.Count > 0)
			{
				OutputGroup newOutputRegion = new OutputMatrix(m_regionId, m_commonColumns, m_throwInvalidRowErrors);
				m_groups.Add(newOutputRegion);
			}
			
			foreach(OutputGroup group in m_groups)
			{
				group.FinalizeCellValues(variables);
				group.FinalizeData(operators, variables);
				AddWarnings(group.WarningList, false);
			}
		}

		private void AddCommonColumn(OutputColumn commonColumn)
		{
			m_commonColumns.Add(commonColumn);
			foreach(OutputGroup oRegion in m_groups)
			{
				oRegion.AddColumn(commonColumn);
			}	
		}

		public void AddColumn(OutputColumn oColumn)
		{
			m_hasData = true;
			m_allColumns.Add(oColumn);
			//If the column being added has no filters at all, it is a common column meant to be added to 
			//all filter groups, unless no filter groups exist for any columns
			if(oColumn.GroupFilters.Count == 0)
			{
				AddCommonColumn(oColumn);
			}
			else
			{
				GroupTag highestColumnFilter = oColumn.GetHighestUnusedGroupTag();
				if((highestColumnFilter == null) || (oColumn.GetGroupTag(highestColumnFilter.AttributeType).RangeFrom == -1))
				{
					AddCommonColumn(oColumn);
					//Tools.LogError(String.Format("Unable to add column to output data in region '{0}'.", m_regionId) + ", DEV MSG:" + String.Format("The current object is of type OutputData but should be of type OutputMatrix because there are no group filters left.  The group type of this OutputGroup is '{0}', its range is from {1} to {2}.", m_filter.AttributeType, m_filter.RangeFrom, m_filter.RangeTo));
					//throw new CBaseException(String.Format("Unable to add column to output data in region '{0}'.", m_regionId), String.Format("The current object is of type OutputData but should be of type OutputMatrix because there are no group filters left.  The group type of this OutputGroup is '{0}', its range is from {1} to {2}.", m_filter.AttributeType, m_filter.RangeFrom, m_filter.RangeTo));
				}
				else
				{	
					highestColumnFilter.AlreadyUsed = true;
					//Find the group this column belongs to
					bool inserted = false;
					for(int i = 0; i < m_groups.Count; ++i)
					{
						OutputGroup oRegion = (OutputGroup)m_groups[i];
						GroupTag existingFilter = oRegion.RegionFilter;

						if(existingFilter.AttributeType != highestColumnFilter.AttributeType)
							throw new CBaseException(String.Format("Unable to add column to output data in region '{0}' - You must declare the same set of groups for all columns in a region.", m_regionId), String.Format("Existing filter attribute type = '{0}', filter being added attribute type = '{1}'.", existingFilter.AttributeType, highestColumnFilter.AttributeType));

						if(existingFilter.RangeFrom < highestColumnFilter.RangeFrom)
						{
							if(existingFilter.RangeTo > highestColumnFilter.RangeFrom)
								throw new CBaseException(String.Format("Overlap in group filters detected in region '{0}'.  The existing filter's 'to' value is {1}, which is greater than the filter being added's 'from' value of {2}.", m_regionId, existingFilter.RangeTo, highestColumnFilter.RangeFrom), "");
							else
								continue;
						}

						if(existingFilter.RangeFrom == highestColumnFilter.RangeFrom)
						{
							if(existingFilter.RangeTo != highestColumnFilter.RangeTo)
								throw new CBaseException(String.Format("Mismatch in range values for group tag filter in region '{0}'.  The 'from' range values of the existing filters and the filter being added match with value = {1}, but the 'to' values do not.  The 'to' value of the existing filter is {2} and the 'to' value of the filter being added is {3}.  Existing filter info: '{4}', filter being added info: '{5}'.", m_regionId, existingFilter.RangeFrom, existingFilter.RangeTo, highestColumnFilter.RangeTo, existingFilter.ToString(), highestColumnFilter.ToString()), "");
							
							if(oColumn.HasMoreUnusedGroupFilters && oRegion.GetType().Equals(typeof(OutputMatrix)))
							{
								OutputGroup newOutputRegion = new OutputData(m_regionId, oColumn, oRegion, highestColumnFilter, m_throwInvalidRowErrors);
								m_hasContainers = true;
								m_groups[i] = newOutputRegion;
							}
							else
							{
								oRegion.AddColumn(oColumn);
							}
							inserted = true;
							break;
						}
						
						if(existingFilter.RangeFrom > highestColumnFilter.RangeFrom)
						{
							if(existingFilter.RangeFrom < highestColumnFilter.RangeTo)
								throw new CBaseException(String.Format("Overlap in group filters detected in region '{0}'.  The existing filter's 'from' value is greater than the filter being added's 'from' value but less than the filter being added's 'to' value.  Existing filter = '{1}', Filter being added = '{2}'.", m_regionId, existingFilter.ToString(), highestColumnFilter.ToString()), "");
							
							InsertColumnIntoGroup(oColumn, i, highestColumnFilter);
							inserted = true;
							break;
						}
					}

					if(!inserted)
					{
						InsertColumnIntoGroup(oColumn, -1, highestColumnFilter);
					}
				}	
			}
		}

		//position < 0 = "insert at end"
		private void InsertColumnIntoGroup(OutputColumn oColumn, int position, GroupTag highestColumnFilter)
		{
			//If this column has more filters, create a container type, else create a matrix type
			OutputGroup newOutputRegion;
			if(oColumn.HasMoreUnusedGroupFilters)
			{
				newOutputRegion = new OutputData(m_regionId, oColumn, m_commonColumns, highestColumnFilter, m_throwInvalidRowErrors);
				m_hasContainers = true;
			}
			else
			{
				newOutputRegion = new OutputMatrix(m_regionId, oColumn, m_commonColumns, highestColumnFilter, m_throwInvalidRowErrors);
			}

			if(position < 0)
				m_groups.Add(newOutputRegion);
			else
				m_groups.Insert(position, newOutputRegion);
		}

		public int WriteDataToWorksheet1D(OutputWriter outputWriter, int startRow, ArrayList headers)
		{
			if(m_filter != null)
				headers.Add(m_filter);

			foreach(OutputGroup group in m_groups)
			{
				startRow = group.WriteDataToWorksheet1D(outputWriter, startRow, headers);
				// If this is the root group, then it does not have any filters, and instead is just a high level container
				// for all groups.  Therefore, we need to clear out the headers between each container processed.
				if(m_isRoot)
					headers = new ArrayList();
			}

			return startRow; //this is now the end row
		}
		
		// Note - this is obsolete as of 5/1/08 and has not been maintained.  If you require 2D output, you will need
		// to verify this function.
		public int WriteDataToWorksheet2D(OutputWriter outputWriter, ref int startRow, ref int startCol, ref bool hasIO, bool firstFragment, bool ignoreHasIO, bool ignoreNonIOHeaders)
		{
			//Put the title of the group first
			if(!this.Title.Trim().Equals(""))
			{
				if(m_filter.AttributeType == E_OperatorAttributeType.IO)
				{
					if(!hasIO)
					{
						outputWriter.WriteCell(startRow + 1, startCol++ , this.Title);
					}
					else
					{
						if(!ignoreHasIO)
						{
							outputWriter.WriteCell(startRow, startCol++ , this.Title);
						}
					}
				}
				else
				{
					if(!ignoreNonIOHeaders)
						outputWriter.WriteCell(startRow++, startCol , this.Title);
				}
			}
			
			int endRow = startRow;
			int returnedEndRow = startRow;
			foreach(OutputGroup group in m_groups)
			{
				returnedEndRow = group.WriteDataToWorksheet2D(outputWriter, ref startRow, ref startCol, ref hasIO, firstFragment, ignoreHasIO, ignoreNonIOHeaders);
				if(returnedEndRow > endRow)
					endRow = returnedEndRow;
			}
			
			if(m_filter != null && m_filter.AttributeType == E_OperatorAttributeType.IO)
			{
				if(!ignoreHasIO)
					startCol = 1;
				hasIO = true;
			}
			
			startRow = endRow;
			
			return endRow;
		}
	}
}