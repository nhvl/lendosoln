using System;
using System.Collections;
using System.Text;
using DataAccess;
using LendersOffice.Common;
using System.Text.RegularExpressions;
using System.Diagnostics; 

namespace RatesheetMap
{
    [DebuggerDisplay("Region {m_regionType}: IsFragment={m_fragment}, IsOptional={m_optional}, MapContext={m_mapContext}, CreateOutput={m_createOutput}")]
 	public abstract class Region : Tag
	{
		#region Variables
		public const string			REGION_TAG_NAME = "region:";
		protected string			m_loObjId = "";						// Optional: This is used to identify the LPE Object that this Region extracts data for
		protected string			m_srcLoProgramId = null;			// Optional: See OPM 19312 - Pointer Regions
		protected E_LoObjType		m_loObjType = E_LoObjType.None;		// Optional: Value related to m_loObjId {LoProgramId, LoPolicyId, LoRatesheetId}
		protected E_RegionType		m_regionType;						// Required: The region type {RateOption, AdjustTable}
		protected bool				m_fragment = false;					// Optional: Does this region contain multiple fragments scattered through the excel file (only applicable to RateOption type)
		protected bool				m_optional = false;					// Optional: If this is set to true, no error will be thrown if this region can't be found
		protected Operators			m_operators;						// The list of operators for this region
		protected OutputGroup		m_oData;							// The set of output data that will get written to the CSV for this region
		protected ArrayList			m_formulaCells = new ArrayList();	
		protected LandmarkCell		m_anchorLandmarkInMap;
		protected LandmarkCell		m_anchorLandmarkInRS;
		protected bool				m_createOutput = true;
		protected bool				m_hasError = false;
		protected bool				m_hasOutputData = true;
		protected bool				m_isFound = false;
		protected E_MapContext		m_mapContext;
		protected ArrayList			m_warningList = new ArrayList();
		protected bool				m_ignoreInvalidRows = false;
		protected WorksheetFileRep	m_mapRep;
		protected RatesheetWorksheet m_ratesheetRep = null;
		protected DateTime			m_effectiveDate = SmallDateTime.MinValue;
		protected DateTime			m_effectiveTime = SmallDateTime.MinValue;
		protected DateTime			m_effectiveDateTime = SmallDateTime.MinValue;
		protected bool				m_setEffectiveDateTime = false;
		protected bool				m_timezoneSet = false;
		protected E_Timezone		m_timezone = E_Timezone.P;

		public bool CreateOutput
		{
			get { return m_createOutput; }
			set { m_createOutput = value; }
		}

		public E_Timezone Timezone
		{
			get { return m_timezone; }
		}

		public bool TimezoneSet
		{
			get { return m_timezoneSet; }
		}

		public int MaxGroupOperators
		{
			get { return m_operators.MaxColumnGroups; }
		}	

		public string IdLabel
		{
			get 
			{ 
				//RatesheetId value will return the program ID that matches the Client Code and RatesheetId
				if(m_loObjType == E_LoObjType.LoRatesheetId)
					return RatesheetMapCommon.GetDescription(E_LoObjType.LoProgramId);
				else
					return RatesheetMapCommon.GetDescription(m_loObjType);
			}
		}

		public bool IsEffectiveDateTimeSet
		{
			get { return m_setEffectiveDateTime; }
		}

		public DateTime EffectiveDate
		{
			get { return m_effectiveDate; }
		}

		public DateTime EffectiveTime
		{
			get { return m_effectiveTime; }
		}

		public DateTime EffectiveDateTime
		{
			get { return m_effectiveDateTime; }
		}

		public bool IsFound
		{
			get { return m_isFound; }
		}

		public ArrayList WarningList
		{
			get { return m_warningList; }
		}

		public string RatesheetName
		{
			get { return m_ratesheetRep.RatesheetName; }
		}

		public string TabName
		{
			get { return m_ratesheetRep.WorksheetName; }
		}

		public bool HasOutputData
		{
			get { return m_hasOutputData; }
		}

		public bool HasError
		{
			get { return m_hasError; }
			set { m_hasError = value; }
		}
		
		public E_RegionType RegionType
		{
			get { return m_regionType; }
		}

		public OutputGroup RegionOutputData
		{
			get { return m_oData; }
		}

		public E_LoObjType ObjectType
		{
			get { return m_loObjType; }
		}

		public string ObjectId
		{
			get { return m_loObjId; }
		}

		public string SrcLoProgramId
		{
			get { return m_srcLoProgramId; }
		}

        // OPM 19312
		public bool IsPointerRegion
		{
			get { return (m_srcLoProgramId != null); }
		}

		public bool IsFragment
		{
			get { return m_fragment; }
		}
		#endregion
		public abstract void ProcessOperators();
		public abstract void ValidateInput(CellRep region);

		//Factory to create the appropriate region type
		public static Region CreateRegion(CellRep regionRep, WorksheetFileRep mapRep, RatesheetWorksheet ratesheet, E_MapContext mapContext)
		{
			string type = regionRep.GetAttribute("type");
			if(type == null || type.Equals(""))
				throw new CBaseException(String.Format("Region tags must define the 'type' attribute - the text of the current tag is: {0}", regionRep.Value), "");
			
			try
			{
				E_RegionType regionType = (E_RegionType) Enum.Parse(typeof(E_RegionType), type, true);
				switch(regionType)
				{
					case E_RegionType.RateOption:
						return new RateOptionRegion(regionRep, mapRep, ratesheet, mapContext);
					case E_RegionType.AdjustTable:
						return new AdjustmentTableRegion(regionRep, mapRep, ratesheet, mapContext);
					case E_RegionType.RSEffective:
						return new RatesheetEffectiveDateRegion(regionRep, mapRep, ratesheet, mapContext);
					case E_RegionType.NoOutput:
						return new NoOutputRegion(regionRep, mapRep, ratesheet, mapContext);
					default:
						throw new CBaseException("Unable to create new region, region tag definition is: " + regionRep.Value, "Invalid region enum type");
				}
			}
			catch(CBaseException c)
			{
				throw c;
			}
			catch(System.ArgumentException ae)
			{
				throw new CBaseException(String.Format("Invalid Region Type '{0}' in region tag definition: {1}", type, regionRep.Value), ae);
			}
			catch(Exception e)
			{
				throw new CBaseException("Unable to create new region." , e);
			}
		}

		public Region(CellRep region, WorksheetFileRep mapRep, RatesheetWorksheet ratesheet, E_RegionType regionType, E_MapContext mapContext) : base(E_TagType.Region, "")
		{
			m_mapRep = mapRep;
			m_ratesheetRep = ratesheet;
			m_regionType = regionType;
			m_mapContext = mapContext;
			
			foreach(string attributeName in region.Attributes.Keys)
			{
				string attributeVal = region.Attributes[attributeName].ToString();
				switch(attributeName)
				{
					case "type":
						continue; //This was already handled when deciding which region object instance to create
					case "rid":
						if(attributeVal.Equals(""))
							throw new CBaseException("Region tag must contain an RId attribute, region tag text is: " + region.Value, "");
						m_regionId = attributeVal;
						if(!RatesheetMapCommon.IsAlphanumericWithUnderscore(m_regionId))
							throw new CBaseException(String.Format("Region ID must be an alphanumeric value (underscore is allowed).  The invalid region Id is: '{0}'.", m_regionId), "");
						break;
					case "loprogramid":
						m_loObjType = (attributeVal.Trim().Equals(""))?E_LoObjType.None:E_LoObjType.LoProgramId;
						m_loObjId = attributeVal;
						break;
					case "lopolicyid":
						m_loObjType = (attributeVal.Trim().Equals(""))?E_LoObjType.None:E_LoObjType.LoPolicyId;
						m_loObjId = attributeVal;
						break;
					case "loratesheetid":
						m_loObjType = (attributeVal.Trim().Equals(""))?E_LoObjType.None:E_LoObjType.LoRatesheetId;
						m_loObjId = attributeVal;
						break;
					case "srcloprogramid": // OPM 19312
						m_srcLoProgramId = attributeVal;
						break;
					case "srcloratesheetid": // OPM 19312
						m_srcLoProgramId = attributeVal;
						break;
					case "fragment":
						try
						{
							m_fragment = bool.Parse(attributeVal);
						}
						catch(Exception)
						{
							throw new CBaseException("'Fragment' attribute of region tag must contain a boolean value.  Region tag text is: " + region.Value, "");
						}
						break;
					case "optional":
						try
						{
							m_optional = bool.Parse(attributeVal);
						}
						catch(Exception)
						{
							throw new CBaseException("'Optional' attribute of region tag must contain a boolean value.  Region tag text is: " + region.Value, "");
						}
						break;
					case "ignoreinvalidrows":
						try
						{
							m_ignoreInvalidRows = bool.Parse(attributeVal);
						}
						catch(Exception)
						{
							throw new CBaseException("'IgnoreInvalidRows' attribute of region tag must contain a boolean value.  Region tag text is: " + region.Value, "");
						}
						break;
					case "timezone":
						try
						{
							m_timezone = (E_Timezone) Enum.Parse(typeof(E_Timezone), attributeVal, true);
							m_timezoneSet = true;
						}
						catch(Exception)
						{
							throw new CBaseException("'Timezone' attribute of region tag must contain one of the following values: P, M, C, or E.  Region tag text is: " + region.Value, "");
						}
						break;
					case "cm":
						break;
					default:
						throw new CBaseException(String.Format("Invalid attribute '{0}' found in the Region tag located at map row {1}, column {2}.", attributeName, region.Row, region.ColumnLetter), "");
				}
			}

			try
			{
				ValidateInput(region);
			}
			catch(CBaseException c)
			{
				throw c;
			}
		}

		public void AddWarning(string warning, bool logMessage)
		{
			m_warningList.Add(warning);
			if(logMessage)
				LogHelper.LogWarning(warning);
		}

		public void ClearWarningsList()
		{
			m_warningList = new ArrayList();
		}

		public void ProcessLandmarks()
		{
			if(this.IsPointerRegion)
				return;
			
			// Find all landmarks that belong to this region
			ArrayList landmarkTags = m_mapRep.GetAllCellsByType(m_regionId, E_TagType.Landmark);
			if(landmarkTags.Count == 0)
			{
				if(m_optional == false)
					throw new CBaseException(string.Format("Region '{0}' does not have any landmarks specified.  The most common cause of this is that the landmarks defined for this region have an incorrect region ID defined in them (typically a copy/paste error).", m_regionId), "");
				else
				{
					AddWarning(string.Format("Region '{0}' in ratesheet '{1}', tab '{2}' does not have any landmarks specified (The 'Optional' attribute = true, so there is no error).", m_regionId, m_ratesheetRep.RatesheetName, m_ratesheetRep.WorksheetName), false);
					return;
				}
			}
			
			CellRep anchor = null;
			LandmarkCell fCell;
			ArrayList blankLandmarks = new ArrayList();
			foreach(CellRep cell in landmarkTags)
			{
				if(anchor == null && !cell.GetAttribute("str").Equals(""))
				{
					anchor = cell;
					m_anchorLandmarkInMap = new MapLandmarkCell(m_regionId, anchor, true /*isAnchorLandmark*/, 0, 0);
				}
				else
				{
					if(anchor == null)
						blankLandmarks.Add(cell);
					else
					{
						fCell = new MapLandmarkCell(m_regionId, cell, false /*isAnchorLandmark*/, m_anchorLandmarkInMap.Row, m_anchorLandmarkInMap.Column);
						m_formulaCells.Add(fCell);
					}
				}
			}
			if(anchor == null)
			{
				if(m_optional == false)
					throw new CBaseException(string.Format("Region '{0}' in ratesheet '{1}' does not have any valid non-blank landmarks specified", m_regionId, m_ratesheetRep.RatesheetName), "");
				else
				{
					AddWarning(string.Format("Region '{0}' in ratesheet '{1}', tab '{2}' does not have any valid non-blank landmarks specified (The 'Optional' attribute = true, so there is no error).", m_regionId, m_ratesheetRep.RatesheetName, m_ratesheetRep.WorksheetName), false);
					return;
				}
			}
			else
			{
				foreach(CellRep cell in blankLandmarks)
				{
					fCell = new MapLandmarkCell(m_regionId, cell, false /*isAnchorLandmark*/, m_anchorLandmarkInMap.Row, m_anchorLandmarkInMap.Column);
					m_formulaCells.Add(fCell);
				}
			}
			
			MatchLandmarksInRSFile();
		}

		private void MatchLandmarksInRSFile()
		{
			//We've already processed the landmarks defined in the map file, now we need to find a (unique) match in the ratesheet file 
			//First find any cells that match the anchor in the RS file, if there's only 1, we don't need to worry about eliminating other potential anchor cell locations
			ArrayList foundAnchors = new ArrayList();
			try
			{
				foundAnchors = m_ratesheetRep.GetAllCellsByValueRegex(m_anchorLandmarkInMap.SearchString, m_anchorLandmarkInMap.Wildcard);
			}
			catch(Exception e)
			{
				throw new CBaseException("Error while trying to find landmarks in ratesheet for Region " + m_regionId, e);
			}
			
			int nCount = (foundAnchors == null)? 0 : foundAnchors.Count;  //number of matching regions
			if(nCount == 0)
			{
				if(m_optional == false)
				{
					throw new CBaseException(String.Format("Could not match landmarks for Region '{0}'.  The value that cannot be found is '{1}' and is located at map row {2}, column {3}." , m_regionId, m_anchorLandmarkInMap.SearchString, m_anchorLandmarkInMap.Row, m_anchorLandmarkInMap.ColumnLetter), "");
				}
				else
				{
					AddWarning(string.Format("Could not match landmarks for Region '{0}' in ratesheet '{1}', tab '{3}'.  The value that cannot be found is '{2}' and is located at map row {4}, column {5}. (The 'Optional' attribute = true, so there is no error).", m_regionId, m_ratesheetRep.RatesheetName, m_anchorLandmarkInMap.SearchString, m_ratesheetRep.WorksheetName, m_anchorLandmarkInMap.Row, m_anchorLandmarkInMap.ColumnLetter), false);
					return;
				}
			}

			if(nCount == 1) // There's only 1 matching cell in the Excel file that matches the anchor landmark text
			{
				m_anchorLandmarkInMap.IsMatchedInMap = true;
				m_anchorLandmarkInRS = new RatesheetLandmarkCell(m_regionId, (RatesheetCellRep)foundAnchors[0], true /*IsAnchorLandmark*/, 0, 0);
				VerifyAllLandmarksMatch();
			}
			else
			{
				// Continue to match subsequent landmarks until we either eliminate down to 1 region, or we use up all the landmarks
				// and we must return an error since there are multiple regions matching the set of landmarks, or none matching
				int row = -1;
				int col = -1;
				string searchString = "";
				string correspondingRSVal = "";
				LandmarkCell lCell = null;

				for(int i = 0; i < m_formulaCells.Count; ++i)
				{
					lCell = (LandmarkCell)m_formulaCells[i];
					searchString = lCell.SearchString;
					
					if(searchString.Trim() != "") // we need to preserve strings of only blank spaces
						searchString = searchString.Trim();
					
					string regex = RatesheetMapCommon.WildcardToRegexFullString(searchString, lCell.Wildcard);
					ArrayList rangesToRemove = new ArrayList();
					foreach(RatesheetCellRep potentialAnchor in foundAnchors)
					{
						row = lCell.RowOffset + potentialAnchor.Row;
						col = lCell.ColumnOffset + potentialAnchor.Column;
						if(row < 1 || col < 1) // Excel uses 1-indexing, and this was originally written using excel (TODO - change to 0 indexing now that excel is no longer used.  Just for code clarity, not urgent.)
						{
							--nCount;
							rangesToRemove.Add(potentialAnchor);
							continue;
						}

						try
						{
							RatesheetCellRep rCellRep = m_ratesheetRep.GetCell(row, col);
							correspondingRSVal = (rCellRep == null)?"":rCellRep.Value.Trim().ToLower();
						}
						catch(CBaseException cb)
						{
							throw cb;
						}
						
						if(Regex.Match(correspondingRSVal, regex).Success == true)
						{
							lCell.IsMatchedInMap = true;
						}
						else
						{
							--nCount;
							rangesToRemove.Add(potentialAnchor);
						}
					}

					foreach(RatesheetCellRep range in rangesToRemove)
					{
						try
						{
							foundAnchors.Remove(range);
						}
						catch(Exception e)
						{
							throw new CBaseException("Error removing eliminated range from potential anchors list.", e);
						}	
					}

					if(nCount < 1)
					{
						if(m_optional == false)
							throw new CBaseException(String.Format("Could not match all landmarks for Region {0}.  The value that cannot be found is '{1}', found in map row {2}, column {3}.  The corresponding value that is found in the rate sheet is '{4}' and is found in ratesheet row {5}, column {6}.", m_regionId, searchString, lCell.Row, lCell.ColumnLetter, correspondingRSVal, row, RatesheetMapCommon.GetColumnLetterFromNumber(col) ), "LM not found Loc 1");
						else
						{
							AddWarning(string.Format("Could not match landmarks for Region '{0}' in ratesheet '{7}', tab '{8}'.  The value that cannot be found is '{1}', found in map row {2}, column {3}. The corresponding value that is found in the rate sheet is '{4}' and is found in ratesheet row {5}, column {6} (Note: The 'Optional' attribute = true, so there is no error).", m_regionId, searchString, lCell.Row, lCell.ColumnLetter, correspondingRSVal, row, RatesheetMapCommon.GetColumnLetterFromNumber(col), m_ratesheetRep.RatesheetName, m_ratesheetRep.WorksheetName), false);
							return;
						}
					}
					
					if(nCount == 1) //Success
						break;
				}
				
				if(nCount < 1)
				{
					if(m_optional == false)
						throw new CBaseException(String.Format("Could not match all landmarks for Region {0}.  The value that cannot be found is '{1}'.", m_regionId, searchString), "");
					else
					{
						AddWarning(string.Format("Could not match landmarks for Region '{0}' in ratesheet '{2}', tab '{3}'.  The value that cannot be found is '{1}'. (The 'Optional' attribute = true, so there is no error).", m_regionId, searchString, m_ratesheetRep.RatesheetName, m_ratesheetRep.WorksheetName), false);
						return;
					}
				}
				if(nCount > 1)
				{
					if(lCell != null)
					{
						StringBuilder sb = new StringBuilder();
						foreach(RatesheetCellRep foundAnchor in foundAnchors)
						{
							if(sb.Length == 0)
								sb.AppendFormat("Row {0} : Col {1} ", (lCell.RowOffset + foundAnchor.Row), RatesheetMapCommon.GetColumnLetterFromNumber(lCell.ColumnOffset + foundAnchor.Column));
							else
								sb.AppendFormat(", Row {0} : Col {1} ", (lCell.RowOffset + foundAnchor.Row), RatesheetMapCommon.GetColumnLetterFromNumber(lCell.ColumnOffset + foundAnchor.Column));
						}
						throw new CBaseException(String.Format("Multiple areas in the ratesheet match the landmarks defined for Region '{0}'.  You will need to define more landmarks for this region, or try to add a landmark that is completely unique to this region.  The last landmark value that was used (and was unable to narrow down the choices to 1 area of the map) has value '{1}' and can be found at the following {2} locations in the ratesheet: {3}", m_regionId, searchString, foundAnchors.Count, sb.ToString()), "");
					}
					else
						throw new CBaseException(String.Format("Multiple areas in the ratesheet match the landmarks defined for Region '{0}'.  You will need to define more landmarks for this region, or try to add a landmark that is completely unique to this region.  There are currently {1} locations in the ratesheet that match these landmarks.", m_regionId, foundAnchors.Count), "");
				}
				m_anchorLandmarkInRS = new RatesheetLandmarkCell(m_regionId, (RatesheetCellRep)foundAnchors[0], true /*IsAnchorLandmark*/, 0, 0);
				
				VerifyAllLandmarksMatch();
			}
		}

		private void VerifyAllLandmarksMatch()
		{
			// Verify that all the landmark cells match 
			// (but we can skip over those that were already matched in MatchLandmarksInRSFile while trying to eliminate duplicate matches)
			foreach(LandmarkCell lCell in m_formulaCells)
			{
				if(lCell.IsMatchedInMap)
					continue;

				string regex = RatesheetMapCommon.WildcardToRegexFullString(lCell.SearchString.Trim(), lCell.Wildcard);
				string cellVal = "";
				//Get the exact row and column that this landmark cell should be located at in the ratesheet file
				int exactRow = lCell.RowOffset + m_anchorLandmarkInRS.Row; 
				int exactCol = lCell.ColumnOffset + m_anchorLandmarkInRS.Column;

				RatesheetCellRep rCellRep = m_ratesheetRep.GetCell(exactRow, exactCol);

				if(rCellRep != null)
					cellVal = rCellRep.Value.Trim().ToLower().Replace("\n", "");
				
				if(Regex.Match(cellVal, regex).Success == false)
				{
					if(m_optional == false)
					{
						throw new CBaseException(String.Format("Could not match all landmarks for Region '{0}'.  The value that could not be found is '{1}' and is located at map row {2}, column {3}.  The corresponding value found in the ratesheet is '{4}' and is located at ratesheet row {5}, column {6}", m_regionId, lCell.SearchString, lCell.Row, lCell.ColumnLetter, cellVal, exactRow, RatesheetMapCommon.GetColumnLetterFromNumber(exactCol)), "LM not found Loc 2");
					}
					else
					{
						AddWarning(string.Format("Could not match landmarks for Region '{0}' in ratesheet '{7}', tab '{8}'.  The value that cannot be found is '{1}' and is located at map row {2}, column {3}.  The corresponding value found in the ratesheet is '{4}' and is located at ratesheet row {5}, column {6} (The 'Optional' attribute = true, so there is no error).", m_regionId, lCell.SearchString, lCell.Row, lCell.ColumnLetter, cellVal, exactRow, RatesheetMapCommon.GetColumnLetterFromNumber(exactCol), m_ratesheetRep.RatesheetName, m_ratesheetRep.WorksheetName), false);
						return;
					}
				}
			}
			m_isFound = true;
		}
	
		public virtual void ProcessReads(Variables variables, bool throwInvalidRowErrors)
		{
			if(this.IsFound == false)
				return;
			
			if(this.IsPointerRegion)
				return;

			try
			{
				m_oData = ReadTags.CreateOutputData(m_regionId, m_regionType, m_mapRep, m_ratesheetRep, m_anchorLandmarkInMap, m_anchorLandmarkInRS, m_operators, variables, throwInvalidRowErrors);
			}
			catch(CBaseException c)
			{
				throw c;
			}
			catch(Exception e)
			{
				throw new CBaseException("Error creating output data for region: " + m_regionId, e);
			}
			
			if(m_oData == null || !m_oData.HasData)
			{
				if(m_regionType != E_RegionType.NoOutput)
					AddWarning(string.Format("No non-variable read tags found for region '{0}' in ratesheet '{1}', tab '{2}' - no output will be created for this region.", m_regionId, m_ratesheetRep.RatesheetName, m_ratesheetRep.WorksheetName), false);
				m_hasOutputData = false;
				m_createOutput = false;
			}
			else
			{
				if(m_regionType == E_RegionType.NoOutput)
				{
					throw new CBaseException(String.Format("Error in region '{0}': NoOutput Regions cannot create output.", m_regionId), "");
				}
			}
		}

		public void FinalizeData(Variables variables)
		{
			if(m_oData != null)
			{
				m_oData.FinalizeData(m_operators, variables);
				AddWarnings(m_oData.WarningList, false);
			}
		}

		private void AddWarnings(ArrayList warnings, bool logMessages)
		{
			foreach(string warning in warnings)
			{
				m_warningList.Add(warning);
				if(logMessages)
					LogHelper.LogWarning(warning);
			}
		}

		//NOTE - This is obsolete, can be removed from all places it's called
		public void WriteDataToWorksheet2D(OutputWriter outputWriter, ref int startRow, ref int startCol, bool firstFragment, ref bool startAtEnd, bool ignoreHasIO, bool ignoreNonIOHeaders)
		{
			if(m_oData != null)
				m_oData.WriteDataToWorksheet2D(outputWriter, ref startRow, ref startCol, ref startAtEnd, firstFragment, ignoreHasIO, ignoreNonIOHeaders);
		}

		public int WriteDataToWorksheet1D(OutputWriter outputWriter, int startRow, ArrayList headers)
		{
			if(m_oData != null)
				return m_oData.WriteDataToWorksheet1D(outputWriter, startRow, headers);
			else
				return 0;
		}

		public GroupTag GetFirstGroupTag()
		{
			if(m_oData == null)
				return null;
			else
				return m_oData.GetFirstGroupTag();
		}

		public GroupTag GetFirstGroupTagByType(E_OperatorAttributeType attributeType)
		{
			if(m_oData == null)
				return null;
			else
				return m_oData.GetFirstGroupTagByType(attributeType);
		}

		public override string ToString()
		{
			if(m_hasError == true)
				return String.Format("<br><b>ERROR IN REGION {0} - Not creating output for this region.</b>", m_regionId);

			StringBuilder sb = new StringBuilder();

			if(m_warningList.Count > 0)
			{
				sb.AppendFormat("<br><b>WARNINGS FOR REGION {0}:</b>", m_regionId);
				foreach(string warningMsg in m_warningList)
				{
					sb.Append("<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- " + warningMsg);
				}
			}

			if(m_createOutput == false)
			{
				sb.AppendFormat("<br><b>NOT CREATING OUTPUT FOR REGION {0}</b><br>", m_regionId);
				return sb.ToString();
			}

			try
			{
				sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + m_anchorLandmarkInMap + "<br>");
				for(int i = 0; i < m_formulaCells.Count; ++i)
				{
					sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + m_formulaCells[i] + "<br>");
				}	
				//string oDataString = (m_oData == null)?"":m_oData.ToString();
				return string.Format(@"
			<b>Region Id:</b> {0} <br> 
			<b>&nbsp;&nbsp;&nbsp;&nbsp;Type:</b> {1} <br> 
			<b>&nbsp;&nbsp;&nbsp;&nbsp;{2}</b>: {3} <br> 
			<b>&nbsp;&nbsp;&nbsp;&nbsp;Optional:</b> {4} <br> 
			<b>&nbsp;&nbsp;&nbsp;&nbsp;Fragment:</b> {5} <br>
			<b>&nbsp;&nbsp;&nbsp;&nbsp;Landmarks:</b> {6} <br>
			{7}
			{8}
			",m_regionId, m_regionType, m_loObjType.ToString(), m_loObjId, m_optional, m_fragment, m_formulaCells.Count, sb.ToString(), m_operators.ToString());
			}
			catch(Exception)
			{
				return String.Format("<br><b>ERROR PRINTING REGION {0}</b><br>", m_regionId);
			}
		}
	}
}