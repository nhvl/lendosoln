using System;
using System.Collections;
using DataAccess;

namespace RatesheetMap
{
	/**
	 *  This class is very similar to CBaseException in that it has an error reference number for easy lookup, 
	 *  and can be used to throw non-fatal exceptions that occur in the Ratesheet Map Processor.
	 */

	public class RatesheetMapNonFatalException : Exception
	{
		private string m_userMessage = "";
		private string m_errorReferenceNumber = "";
		private string m_developerMessage = "";
		
		public RatesheetMapNonFatalException(string userMessage, string developerMessage)
		{
			Initialize(userMessage, developerMessage);
		}

		public RatesheetMapNonFatalException(string userMessage, Exception innerException)
		{
			Initialize(userMessage, innerException.ToString());
		}

		public string ErrorReferenceNumber 
		{
			get { return m_errorReferenceNumber; }
		}

		public string UserMessage 
		{
			get { return m_userMessage; }
			set { m_userMessage = value; }
		}

		public string DeveloperMessage 
		{
			get { return m_developerMessage; }
			set { m_developerMessage = value; }
		}

		public override string Message 
		{
			get { return ToString(); }
		}

		private void Initialize(string userMessage, string developerMessage) 
		{
			m_userMessage = userMessage;
			m_developerMessage = developerMessage;
			CPmlFIdGenerator generator = new CPmlFIdGenerator();
			m_errorReferenceNumber = generator.GenerateNewFriendlyId();
		}

		public override string ToString() 
		{
			return string.Format("Reference #:{1}{0}{0}UserMessage: {2}{0}{0}DeveloperMessage: {3}", Environment.NewLine, m_errorReferenceNumber, m_userMessage, m_developerMessage);
		}
	}
}