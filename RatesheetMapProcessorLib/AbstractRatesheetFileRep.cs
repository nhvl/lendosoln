using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using DataAccess;
using System.IO;
using LendersOffice.Common;
using System.Text.RegularExpressions;
using System.Linq;

namespace RatesheetMap
{
	public abstract class RatesheetFileRep : IDisposable
	{
		#region Variables
		protected Hashtable m_worksheets;
		protected string m_ratesheetName = "";
		protected string m_ratesheetPath = "";

		public int NumWorksheets
		{
			get { return m_worksheets.Count; }
		}

		public string RatesheetName
		{
			get { return m_ratesheetName; }
		}
		#endregion

		public abstract void ImportRatesheet(string ratesheetPath) ;

		public RatesheetFileRep()
		{
			m_worksheets = new Hashtable();
		}

        private string ReduceStringTo29OrLessAndReplaceSpecialChars(string name)
        {
            //opm 91852
            name = name.Replace("\\", "`");
            name = name.Replace("/", "~");
            name = name.Replace(":", "@");
            name = name.Replace("?", "#");
            name = name.Replace("\"", "$");
            name = name.Replace("<", "%");
            name = name.Replace(">", "(");
            name = name.Replace("|", ")");
            
            if (name.Length <= 29)
            {
                return name;
            }

            return name.Substring(0, 29);
        }

		public RatesheetWorksheet GetWorksheet(string worksheetName)
		{
            worksheetName = worksheetName.Trim();
            foreach (string tempName in m_worksheets.Keys)
            {
                //OPM 
               // if (tempName.Length > 29 && tempName.Substring(0,29).Equals(worksheetName.Trim().ToLower()))
                if (ReduceStringTo29OrLessAndReplaceSpecialChars(tempName).Equals(worksheetName, StringComparison.OrdinalIgnoreCase))
                {
                    return (RatesheetWorksheet)m_worksheets[tempName];
                }
       
            }
			return null;
		}

		void IDisposable.Dispose()
		{
			try
			{
				foreach(string key in m_worksheets.Keys)
				{
					RatesheetWorksheet ws = (RatesheetWorksheet)m_worksheets[key];
					((IDisposable)ws).Dispose() ;
				}
			}
			finally
			{
				m_worksheets = null;
			}
		}
	}
	
	public class RatesheetWorksheet : IDisposable
	{
		#region Variables
		//private Hashtable m_columns;
        private Dictionary<int, Dictionary<int, RatesheetCellRep>> m_columns;
		private int m_maxRow = 0;
		private int m_maxColumn = 0;
		private LandmarkHelperRep m_landmarkHelperRep;

		public string RatesheetName { get; private set;}

		public string WorksheetName { get; private set;}
		#endregion

		public RatesheetWorksheet(string worksheetName, string ratesheetName)
		{
			WorksheetName = worksheetName;
			RatesheetName = ratesheetName;
            m_columns = new Dictionary<int, Dictionary<int, RatesheetCellRep>>(); // new Hashtable();
			m_landmarkHelperRep = new LandmarkHelperRep();
		}

		public void AddCell(RatesheetCellRep cell)
		{
			int row = cell.Row;
			int col = cell.Column;

			m_maxRow = Math.Max(row, m_maxRow);
			m_maxColumn = Math.Max(col, m_maxColumn);
			
			if(m_columns.ContainsKey(col))
			{
                Dictionary<int, RatesheetCellRep> column = m_columns[col];
				if(column.ContainsKey(row))
				{
					Tools.LogWarning("Adding a cell value to col: " + col + ", row: " + row + " that already exists");
					column[row] = cell;
				}
				else
				{
					column.Add(row, cell);
				}
			}
			else
			{
                Dictionary<int, RatesheetCellRep> column = new Dictionary<int, RatesheetCellRep>();
				column.Add(row, cell);
				m_columns.Add(col, column);
			}

			if(!RatesheetMapCommon.IsNumeric(cell.Value))
			{
				m_landmarkHelperRep.AddCell(cell);
			}
		}

		public RatesheetCellRep GetNextCellInColumnByValue(RatesheetCellRep topCell, string val, bool trimBlanksFromEnd)
		{
			return GetNextCellInColumnByValue(topCell, val, "" /*wildcard*/, trimBlanksFromEnd);
		}

		public RatesheetCellRep GetNextCellInColumnByValue(RatesheetCellRep topCell, string val, string wildcard, bool trimBlanksFromEnd)
		{
			if(!m_columns.ContainsKey(topCell.Column))
			{
				return null;
			}
			else
			{
				string regex = RatesheetMapCommon.WildcardToRegexFullString(val,wildcard);
                Dictionary<int, RatesheetCellRep> column = m_columns[topCell.Column];
				for(int i = topCell.Row+1; i <= m_maxRow; ++i)
				{
					if(column.ContainsKey(i) == false)
					{
						if(val.Trim().Equals(""))
							return new RatesheetCellRep(i, topCell.Column, "");
						else
							continue;
					}

					RatesheetCellRep rCellRep = (RatesheetCellRep)column[i];

					if(rCellRep != null && Regex.Match(rCellRep.Value.ToLower(), regex).Success == true)
					{
						if(trimBlanksFromEnd)
							return GetTrueDynamicListEnd(topCell, rCellRep, column);
						else
							return rCellRep;
					}
				}

				if(val.Trim().Equals(""))
				{
					RatesheetCellRep rCellRep = new RatesheetCellRep(++m_maxRow, topCell.Column, "");
					column.Add((m_maxRow), rCellRep);
					return rCellRep;
				}
			}
			return null;
		}

        private RatesheetCellRep GetTrueDynamicListEnd(RatesheetCellRep startFrom, RatesheetCellRep foundEnd, Dictionary<int, RatesheetCellRep> column)
		{
			if(startFrom == null || foundEnd == null)
				return null;
			else
			{
				RatesheetCellRep trueEndCell = foundEnd;
				int startRow = startFrom.Row;
				int endRow = foundEnd.Row;
				int col = foundEnd.Column;

				for(int i = endRow; i > startRow; --i)
				{
					RatesheetCellRep temp = null;
                    if (column.TryGetValue(i - 1, out temp) == false)
					{
						trueEndCell = new RatesheetCellRep(i-1, col, "");
						continue;
					}
					else if(temp.Value.Trim().Equals(""))
					{
						trueEndCell = temp;
						continue;
					}
					else
					{
						break;
					}
				}
				return trueEndCell;
			}
		}

		// TODO - use the non-numeric optimizer for this when the parameter val is non-numeric (Just like GetAllNonNumericCellsByValueRegex)
        // This will just speed things up, it will not affect accuracy or anything.
		public RatesheetCellRep GetCellByValue(string val)
		{
			foreach(int key in m_columns.Keys)
			{
                Dictionary<int, RatesheetCellRep> column = m_columns[key];
				foreach(int row in column.Keys)
				{
					RatesheetCellRep rsCellRep = (RatesheetCellRep)column[row];
					if(rsCellRep.Value.Equals(val))
					{
						return rsCellRep;
					}
				}
			}
			return null;
		}

		//TODO - use the non-numeric optimzer for this when the parameter val is non-numeric (Just like GetAllNonNumericCellsByValueRegex)
        // This will just speed things up, it will not affect accuracy or anything.
        public RatesheetCellRep GetCellByValueRegex(string val, string wildcard)
		{
			string regex = RatesheetMapCommon.WildcardToRegexFullString(val,wildcard);
			foreach(int key in m_columns.Keys)
			{
                Dictionary<int, RatesheetCellRep> column = m_columns[key];
				foreach(int row in column.Keys)
				{
					RatesheetCellRep rsCellRep = (RatesheetCellRep)column[row];
					if(Regex.Match(rsCellRep.Value.ToLower(), regex).Success == true || Regex.Match(rsCellRep.Value.ToLower().Trim(), regex).Success == true)
					{
						return rsCellRep;
					}
				}
			}
			return null;
		}

		private ArrayList GetAllNonNumericCellsByValueRegex(string val, string wildcard)
		{
			return m_landmarkHelperRep.GetAllCellsByValueRegex(val, wildcard);
		}

		//Limitation - the user will not be able to find values if the ratesheet value is numeric and they are
		//using a non-numeric search value (ie: they are using a regex in their search value).
		public ArrayList GetAllCellsByValueRegex(string val, string wildcard)
		{
			if(!RatesheetMapCommon.IsNumeric(val))
			{
				return GetAllNonNumericCellsByValueRegex(val, wildcard);
			}
			else
			{
				return GetAllCellsByValueRegex_FullRsSearch(val, wildcard);
			}
		}
		
		public RatesheetCellRep GetCellByValueRegexStartAtCell(string val, string wildcard, RatesheetCellRep startAt)
		{
			int row = 0;
			int column = 1;
			
			if(startAt != null)
			{
				row = startAt.Row;
				column = startAt.Column;
			}

			string regex = RatesheetMapCommon.WildcardToRegexFullString(val,wildcard);

            Dictionary<int, RatesheetCellRep> columnHash = null;

			for(int x = column; x < m_maxColumn; ++x)
			{
                if (m_columns.ContainsKey(x) == false)
                {
                    continue;
                }

				columnHash = m_columns[x];
				
				for(int i = row+1; i <= m_maxRow; ++i)
				{
					if(columnHash.ContainsKey(i) == false)
					{
						if(val.Trim().Equals(""))
						{
							return new RatesheetCellRep(i, x, "");
						}
						else
							continue;
					}

					RatesheetCellRep rCellRep = (RatesheetCellRep)columnHash[i];

					if(rCellRep != null && (Regex.Match(rCellRep.Value.ToLower(), regex).Success == true || Regex.Match(rCellRep.Value.ToLower().Trim(), regex).Success == true))
					{
						return rCellRep;
					}
				}
			}
			return null;
		}
		
		//TODO - use the non-numeric optimizer for this when the parameter val is non-numeric (Just like GetAllNonNumericCellsByValueRegex)
        // This will just speed things up, it will not affect accuracy or anything.
        public ArrayList GetAllCellsByValue(string val)
		{
			ArrayList foundCells = new ArrayList();
			foreach(int key in m_columns.Keys)
			{
                Dictionary<int, RatesheetCellRep> column = m_columns[key];
				foreach(int row in column.Keys)
				{
					RatesheetCellRep rsCellRep = (RatesheetCellRep)column[row];
					
                    if (rsCellRep.Value.ToLower().Equals(val.ToLower()) || rsCellRep.Value.ToLower().Trim().Equals(val.ToLower()))
                    {
                        foundCells.Add(rsCellRep);
                    }
				}
			}
			return foundCells;
		}

		public ArrayList GetAllCellsByValueRegex_FullRsSearch(string val, string wildcard)
		{
			string regex = RatesheetMapCommon.WildcardToRegexFullString(val,wildcard);
			ArrayList foundCells = new ArrayList();
			RatesheetCellRep rsCellRep = null;
			foreach(int key in m_columns.Keys)
			{
                Dictionary<int, RatesheetCellRep> column = m_columns[key];
				
				foreach(int row in column.Keys)
				{
					rsCellRep = (RatesheetCellRep)column[row];

                    if (Regex.Match(rsCellRep.Value.ToLower(), regex).Success == true || Regex.Match(rsCellRep.Value.ToLower().Trim(), regex).Success == true)
                    {
                        foundCells.Add(rsCellRep);
                    }
				}
			}

			return foundCells;
		}

		public string GetCellValue(int row, int column)
		{
			RatesheetCellRep rCellRep = GetCell(row, column);
			if(rCellRep == null)
				return "";
            else if (IsCVErr(rCellRep.Value))
                return "n/a";
            else
            {
                return rCellRep.Value;
            }
		}

		public bool IsCVErr(string val)
		{
			switch(val)
			{
				case "2146826288": // #NULL!
				case "2146826281": // #DIV/0!
				case "2146826273": // #VALUE!
				case "2146826265": // #REF!
				case "2146826259": // #NAME?
				case "2146826252": // #NUM!
				case "2146826246": // #N/A
				case "-2146826288": // #NULL!
				case "-2146826281": // #DIV/0!
				case "-2146826273": // #VALUE!
				case "-2146826265": // #REF!
				case "-2146826259": // #NAME?
				case "-2146826252": // #NUM!
				case "-2146826246": // #N/A
				case "#NULL!":
				case "#DIV/0!":
				case "#VALUE!":
				case "#REF!":
				case "#NAME?":
				case "#NUM!":
				case "#N/A":
					return true;
				default:
					return false;
			}
		}

		public RatesheetCellRep GetCell(int row, int column)
		{
			if(!m_columns.ContainsKey(column))
			{	
				return null;
			}
			else
			{
                Dictionary<int, RatesheetCellRep> currentColumn = m_columns[column];
				if(!currentColumn.ContainsKey(row))
				{
					return null;
				}
				else
				{
					return (RatesheetCellRep)currentColumn[row];
				}
			}
		}

		void IDisposable.Dispose()
		{
            //try
            //{
            //    foreach(int key in m_columns.Keys)
            //    {
            //        Dictionary<int, RatesheetCellRep> column = null;
            //        if(column != null)
            //        {
            //            foreach(int key2 in column.Keys)
            //            {
            //                RatesheetCellRep cRep = (RatesheetCellRep)column[key2];
            //                ((IDisposable)cRep).Dispose() ;
            //            }
            //        }
            //        column = null;
            //    }
            //}
            //finally
            //{
            //    m_columns = null;
            //    ((IDisposable)m_landmarkHelperRep).Dispose() ;
            //}
		}
	}

	public class RatesheetCellRep
	{
		#region Variables

		public int Row { get; private set;}
		public int Column { get; private set;}

        //public string ColumnLetter
        //{
        //    get { return RatesheetMapCommon.GetColumnLetterFromNumber(Column); }
        //}

		public string Value { get; private set;}
		#endregion
			
		public RatesheetCellRep(int row, int column, string data)
		{
			Row = row;
			Column = column;
			Value = data;
		}

	}

	//TODO - make this better - Huffman coding maybe
    // Just for effiencies' sake, not urgent
	public class LandmarkHelperRep : IDisposable
	{
		private Hashtable m_landmarks;

		public LandmarkHelperRep()
		{
			m_landmarks = new Hashtable();
		}

		public void AddCell(RatesheetCellRep rCellRep)
		{
			if(rCellRep == null)
				return;
			
			string indexString = "";
			string rCellRepValue = rCellRep.Value.ToLower();

			if(!rCellRepValue.Trim().Equals(""))
				rCellRepValue = rCellRepValue.Trim();

			if(rCellRep.Value.Length > 0)
			{
				indexString = rCellRepValue.Substring(0, 1);
			}

			if(!m_landmarks.ContainsKey(indexString))
			{
				m_landmarks.Add(indexString, new Hashtable());
			}
			
			Hashtable temp = (Hashtable)m_landmarks[indexString];
		
			if(temp.ContainsKey(rCellRepValue))
			{
				((ArrayList)temp[rCellRepValue]).Add(rCellRep);
			}
			else
			{
				ArrayList tempValueHolder = new ArrayList();
				tempValueHolder.Add(rCellRep);
				temp.Add(rCellRepValue, tempValueHolder);
			}
		}

		private ArrayList GetAllCellsByValue(string val)
		{
			val = val.ToLower();
			
			if(!val.Trim().Equals(""))
				val = val.Trim();

			string indexString = "";
			if(val.Length > 0)
			{
				indexString = val.Substring(0, 1);
			}

			if(!m_landmarks.ContainsKey(indexString))
			{
				return null;
			}
			else
			{
				Hashtable temp = (Hashtable)m_landmarks[indexString];
				if(temp.ContainsKey(val))
				{
					return new ArrayList((ArrayList)temp[val]);
				}
			}

			return null;
		}

		public ArrayList GetAllCellsByValueRegex(string val, string wildcard)
		{
			if(val == null)
				return null;
			
			//If we do not need to perform a regex match, then call the faster function
			if(wildcard == null || wildcard.Equals("") || val.IndexOf(wildcard) < 0)
			{
				return GetAllCellsByValue(val);
			}
			
			val = val.ToLower();
			if(!val.Trim().Equals(""))
				val = val.Trim();
			string regex = RatesheetMapCommon.WildcardToRegexFullString(val,wildcard);
			
			string indexString = "";
			if(val.Length > 0)
			{
				indexString = val.Substring(0, 1);
			}

			if(!m_landmarks.ContainsKey(indexString))
			{
				return null;
			}
			else
			{
				Hashtable temp = (Hashtable)m_landmarks[indexString];
				
				foreach(string key in temp.Keys)
				{
					if(Regex.Match(key.ToLower(), regex).Success == true || Regex.Match(key.ToLower().Trim(), regex).Success == true)
					{
						return new ArrayList((ArrayList)temp[key]);
					}
				}
			}

			return null;
		}

		void IDisposable.Dispose()
		{
			m_landmarks = null;
		}
	}

}