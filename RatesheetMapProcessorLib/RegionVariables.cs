using System;
using System.Collections;
using System.Text;
using DataAccess;
using System.IO;
using LendersOffice.Constants;

namespace RatesheetMap
{
	public class RegionVariables
	{
		private Hashtable		m_regionVariables;

		public RegionVariables()
		{
			m_regionVariables = new Hashtable();
		}

		public void Clear(string regionId)
		{
			m_regionVariables = new Hashtable();
		}

		public bool Contains(string regionId, string varName)
		{
			if(m_regionVariables.ContainsKey(regionId))
				return ((RegionVariablesContainer)m_regionVariables[regionId]).ContainsRegionVariable(varName);
			else
				return false;
		}

		// This function will handle either an array or non-array variable
		public void AddVariable(string regionId, string varName, string val)
		{
			if(Variables.IsArrayVariable(varName))
				AddArrayVariable(regionId, varName, val);
			else
				AddNonArrayVariable(regionId, varName, val);
		}

		public void AddNonArrayVariable(string regionId, string name, string val)
		{
			if(m_regionVariables.ContainsKey(regionId))
				((RegionVariablesContainer)m_regionVariables[regionId]).AddNonArrayRegionVariable(name, val);
			else
			{
				RegionVariablesContainer rVars = new RegionVariablesContainer(regionId);
				rVars.AddNonArrayRegionVariable(name, val);
				m_regionVariables.Add(regionId, rVars);
			}
		}

		public void AddArrayVariable(string regionId, string varName, ArrayList val)
		{
			if(!varName.EndsWith("[]"))
				throw new CBaseException(String.Format("Error while attempting to add array region variable '{0}' - this variable name does not have the syntax of an array variable (ie: ends with '[]').", varName), "");
			
			if(m_regionVariables.ContainsKey(regionId))
				((RegionVariablesContainer)m_regionVariables[regionId]).AddArrayRegionVariable(varName, val);
			else
			{
				RegionVariablesContainer rVars = new RegionVariablesContainer(regionId);
				rVars.AddArrayRegionVariable(varName, val);
				m_regionVariables.Add(regionId, rVars);
			}
		}

		public void AddArrayVariable(string regionId, string varName, string val)
		{
			if(!varName.EndsWith("[]"))
				throw new CBaseException(String.Format("Error while attempting to add array region variable '{0}' - this variable name does not have the syntax of an array variable (ie: ends with '[]').", varName), "");
			
			if(m_regionVariables.ContainsKey(regionId))
				((RegionVariablesContainer)m_regionVariables[regionId]).AddArrayRegionVariable(varName, val);
			else
			{
				RegionVariablesContainer rVars = new RegionVariablesContainer(regionId);
				rVars.AddArrayRegionVariable(varName, val);
				m_regionVariables.Add(regionId, rVars);
			}
		}

		// This function replaces the current set of region variables with the Variables in the given hashtable.  
		// Therefore, any region variables that previously existed will be effectively wiped out.
		public void SetVariables(string regionId, Hashtable regionVariables)
		{
			if(m_regionVariables.ContainsKey(regionId))
				((RegionVariablesContainer)m_regionVariables[regionId]).Variables = regionVariables;
			else
			{
				RegionVariablesContainer rVars = new RegionVariablesContainer(regionId);
				rVars.AddRegionVariables(regionVariables);
				m_regionVariables.Add(regionId, rVars);
			}
		}

		public Hashtable GetAll(string regionId)
		{
			if(m_regionVariables.ContainsKey(regionId))
				return ((RegionVariablesContainer)m_regionVariables[regionId]).Variables;
			else
				return null;
		}

		public string GetNonArrayVariable(string regionId, string varName)
		{
            if (Variables.IsArrayVariable(varName))
            {
                throw new CBaseException(String.Format("Error while attempting to retrieve non-array region variable '{0}' for region '{1}' - this variable name has the syntax of an array variable (ie: ends with '[]').", varName, regionId), "");
            }

            if (varName.StartsWith("%"))
            {
                char[] trimChar = { '%' };
                varName = varName.Trim(trimChar);
            }
            
            if (m_regionVariables.ContainsKey(regionId))
            {
                return ((RegionVariablesContainer)m_regionVariables[regionId]).GetNonArrayRegionVariable(varName);
            }
            else
            {
                throw new CBaseException(String.Format("Could not find Region Variable '{0}' for region '{1}'.", varName, regionId), "");
            }
		}
	
		public string GetArrayVariableAtIndex(string regionId, string varName, int index)
		{
			if(!Variables.IsArrayVariable(varName))
				throw new CBaseException(String.Format("Error while attempting to retrieve array region variable '{0}' for region '{1}' - this variable name does not have the syntax of an array variable (ie: ends with '[]').", varName, regionId), "");
			
			if(m_regionVariables.ContainsKey(regionId))
				return ((RegionVariablesContainer)m_regionVariables[regionId]).GetArrayVariableAtIndex(varName, index);
			else
				return null;
		}

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (string key in m_regionVariables.Keys)
            {
                sb.Append(Environment.NewLine + key + " -> " + m_regionVariables[key].ToString());
            }

            return sb.ToString();
        }
	}

	public class RegionVariablesContainer
	{
		private Hashtable m_regionVariables;
		private string	m_regionId = "";

		public string RegionId
		{
			get { return m_regionId; }
		}

		public Hashtable Variables
		{
			get { return m_regionVariables; }
			set { m_regionVariables = value;}
		}

		public RegionVariablesContainer(string regionId)
		{
			m_regionVariables = new Hashtable();
			m_regionId = regionId;
		}

		public bool ContainsRegionVariable(string varName)
		{
			return m_regionVariables.ContainsKey(varName);
		}

		public string GetNonArrayRegionVariable(string varName)
		{
			if(m_regionVariables.ContainsKey(varName))
				return m_regionVariables[varName].ToString();
			else
				throw new CBaseException(String.Format("Could not find Region Variable '{0}' for region '{1}'.", varName, m_regionId), "");
		}

		public void AddNonArrayRegionVariable(string name, string val)
		{
			if(m_regionVariables.ContainsKey(name))
				m_regionVariables[name] = val;
			else
				m_regionVariables.Add(name, val);
		}

		public void AddArrayRegionVariable(string name, string val)
		{
			if(m_regionVariables.ContainsKey(name))
			{
				((ArrayList)m_regionVariables[name]).Add(val);
			}
			else
			{
				ArrayList temp = new ArrayList();
				temp.Add(val);
				m_regionVariables.Add(name, temp);
			}
		}

		public void AddArrayRegionVariable(string name, ArrayList val)
		{
			if(m_regionVariables.ContainsKey(name))
				((ArrayList)m_regionVariables[name]).AddRange(val);
			else
				m_regionVariables.Add(name, val);
		}

		public void AddRegionVariables(Hashtable regionVariables)
		{
			foreach(string key in regionVariables.Keys)
			{
				if(m_regionVariables.ContainsKey(key))
				{
					m_regionVariables[key] = regionVariables[key];
					//AddWarning(String.Format("Region variable '{0}' has been defined multiple times.  The new value of '{1}' will overwrite the old value of '{2}'.", key, regionVariables[key], m_regionVariables[key]), false);
				}
				else
					m_regionVariables.Add(key, regionVariables[key]);
			}
		}

		public ArrayList GetArrayVariable(string varName)
		{
			if(m_regionVariables.ContainsKey(varName))
				return (ArrayList)m_regionVariables[varName];
			else
				throw new CBaseException(String.Format("Could not find Region Variable '{0}' for region '{1}'.", varName, m_regionId), "");
		}

		public string GetArrayVariableAtIndex(string varName, int index)
		{
			if(m_regionVariables.ContainsKey(varName))
			{
				ArrayList temp = (ArrayList)m_regionVariables[varName];
				
				if(temp.Count <= index)
					return null;
				else
					return temp[index].ToString();
			}
			else
				throw new CBaseException(String.Format("Could not find Region Variable '{0}' for region '{1}'.", varName, m_regionId), "");
		}

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (string key in m_regionVariables.Keys)
            {
                sb.Append(Environment.NewLine + key + " -> " + m_regionVariables[key].ToString());
            }

            return sb.ToString();
        }
	}
}