using System;
using System.Collections;
using DataAccess;

namespace RatesheetMap
{
	public class GroupTag : AbstractOperator
	{
		#region Variables
		private int m_rangeFrom = -1;
		private int m_rangeTo = -1;
		private E_OperatorAttributeType m_attributeType;
		private bool m_used = false;

		public override E_OperatorTagType OperatorTagType
		{
			get { return E_OperatorTagType.Group; }
		}
		public int RangeFrom
		{
			get { return m_rangeFrom; }
		}

		public int RangeTo
		{
			get { return m_rangeTo; }
		}

		public bool AlreadyUsed
		{
			get { return m_used; }
			set { m_used = value; }
		}

		public E_OperatorAttributeType AttributeType
		{
			get { return m_attributeType; }	
		}
		#endregion

		public GroupTag(string regionId, CellRep operatorCell, LandmarkCell anchorLandmarkInMap) : base(regionId, operatorCell, anchorLandmarkInMap)
		{
			string attribute = "";
			try
			{
				attribute = operatorCell.GetAttribute("attribute");
			}
			catch(CBaseException c)
			{
				throw new CBaseException(String.Format("Unable to parse GroupTag attribute in region '{0}'.  The location of the cell being parsed is map row {1}, column {2}.  DETAILS: {3}", regionId, operatorCell.Row, operatorCell.ColumnLetter, c.UserMessage), "");
			}

			if(attribute.Equals(""))
				throw new CBaseException(String.Format("Attribute 'attribute' is required for Group Tag at map row {0}, column {1} in Region '{2}'. ", operatorCell.Row, operatorCell.ColumnLetter, regionId), "");
			
			ParseGroupAttribute(attribute);
		}

		public GroupTag(string regionId, int rowOffset, int colOffset, int rangeFrom, int rangeTo, E_OperatorAttributeType attributeType) : base(regionId, rowOffset, colOffset)
		{
			m_rangeFrom = rangeFrom;
			m_rangeTo = rangeTo;
			m_attributeType = attributeType;
			if(m_rangeFrom > m_rangeTo)
				throw new CBaseException(String.Format("Invalid group tag definition in region '{0}' - The 'from' value of the range is greater than the 'to' value.  From = {1}, To = {2}.", regionId, m_rangeFrom, m_rangeTo), "");
		}

		public override AbstractOperator GetCopy()
		{
			return new GroupTag(this.RegionId, this.RowOffset, this.ColumnOffset, this.RangeFrom, this.RangeTo, this.AttributeType);
		}

		//This constructor is used to create a standalone default placeholder group tag
		public GroupTag(string regionId, E_OperatorAttributeType operatorType) : base(regionId) 
		{
			m_attributeType = operatorType;
			m_rangeFrom = -1;
			m_rangeTo = -1;
		}

		private void ParseGroupAttribute(string groupAttribute)
		{
			//Example groupAttribute: Lk(0,15)
			if(groupAttribute.ToLower().IndexOf("lk") >= 0)
				m_attributeType = E_OperatorAttributeType.LK;
			else if(groupAttribute.ToLower().IndexOf("ppp") >= 0)
				m_attributeType = E_OperatorAttributeType.PPP;
			else if(groupAttribute.ToLower().IndexOf("io") >= 0)
				m_attributeType = E_OperatorAttributeType.IO;
			else
				throw new CBaseException(String.Format("Invalid Group Tag type attribute (example: 'Lk'): '{0}' in region '{1}'.  ", groupAttribute, m_regionId), "");
			
			int leftParenIndex = groupAttribute.IndexOf("(");
			int rightParenIndex = groupAttribute.IndexOf(")");
			if(leftParenIndex < 0 || rightParenIndex < 0)
				throw new CBaseException(String.Format("Invalid Group Tag range format in region '{0}'.  This usually occurs if there is a mismatch in parenthesis.  The group tag attribute text is '{1}'.", m_regionId, groupAttribute), "");
			
			//TODO - use SafeString here for code cleanup purposes, not urgent
			string range = groupAttribute.Substring(leftParenIndex+1, rightParenIndex-leftParenIndex-1);
			string[] tokens = range.Split(',');
			if(tokens.Length != 2)
				throw new CBaseException(String.Format("Invalid Group Tag type attribute (example: 'Lk(0,15)'): '{0}' in region '{1}'.  ", groupAttribute, m_regionId), "");

			m_rangeFrom = int.Parse(tokens[0].Trim());
			m_rangeTo = int.Parse(tokens[1].Trim());
			if(m_rangeFrom > m_rangeTo)
				throw new CBaseException(String.Format("Invalid group tag definition in region '{0}' - The 'from' value of the range is greater than the 'to' value.  From = {1}, To = {2}.  The group tag attribute text is '{3}'.", m_regionId, m_rangeFrom, m_rangeTo, groupAttribute), "");
		}

		public override string ToString()
		{
			return RatesheetMapCommon.GetTabString(3) + String.Format("Op Type: Group, RegionId: {3}, OperatorAttributeType: {0}, RangeFrom: {1}, RangeTo: {2}", m_attributeType, m_rangeFrom, m_rangeTo, m_regionId);
		}
	}
}
