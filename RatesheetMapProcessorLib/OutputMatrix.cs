using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using DataAccess;
using System.Text.RegularExpressions;

namespace RatesheetMap
{
	public class OutputMatrix : ISwap, OutputGroup
	{
		#region Variables
		private string		m_regionId;
		private static int	NUM_COLUMNS = 5;	//NR, P, M, QR, TR
		private static int	NUM_INFORMATION_COLUMNS = 1; //Holds row Information/Operators
		private static int	NUM_INFORMATION_ROWS = 1;	//Holds column Information/Operators
		private				OutputCell[][] m_outputMatrix;	
		private				GroupTag m_filter;
		private int			m_rows, m_columns;
		private	bool[]		m_validityData = {false,false,false,false,false};
		private HashSet<int> m_invalidRows = new HashSet<int>();
		private Variables	m_variables;
		private bool		m_isReady = false;
		private ArrayList	m_storedColumns = new ArrayList();
		private ArrayList	m_allColumns = new ArrayList();
		private bool		m_isDynamicList = false;
		private bool		m_hasData = false;
		private bool		m_throwInvalidRowErrors = false;
		protected ArrayList	m_warningList = new ArrayList();
		private bool		m_reportErrorForLastInvalidRow = true; // OPM 19144

		public string RegionId
		{
			get { return m_regionId; }
		}
		public ArrayList Columns
		{
			get { return m_allColumns; }
		}
		public ArrayList AllColumns
		{
			get { return m_allColumns; }
		}
		public bool HasData
		{
			get { return m_hasData; }
		}
		public bool IsDynamicList
		{
			get { return m_isDynamicList; }
		}
		public bool IsReady
		{
			get { return m_isReady; }
		}
		public GroupTag RegionFilter
		{
			get { return m_filter; }
		}

		public bool IsContainer
		{
			get { return false; }
		}

		public bool HasContainers
		{
			get { return false; }
		}

		public string Title
		{
			get 
			{ 
				if(m_filter == null)
					return "";
				string description = RatesheetMapCommon.GetDescription(m_filter.AttributeType);
				//return m_filter.AttributeType.ToString().ToUpper() + " " + m_filter.RangeFrom + ":" + m_filter.RangeTo;
				return description + ": " + m_filter.RangeFrom + "-" + m_filter.RangeTo;
			}
		}

		public Hashtable RegionVariables
		{
			set { m_variables.SetRegionVariables(m_regionId, value); }
		}

		public ArrayList WarningList
		{
			get { return m_warningList; }
		}

		#endregion

		public GroupTag GetFirstGroupTag()
		{
			return m_filter;
		}

		public GroupTag GetFirstGroupTagByType(E_OperatorAttributeType attributeType)
		{
			if(m_filter != null && (m_filter.AttributeType == attributeType))
				return m_filter;
			else
				return null;
		}

		// Gets the integer value (for indexing) of the column number that corresponds to the column type (NR, P, M, QR or TR)
		private int GetColumnLocation(int colLocation)
		{
			return colLocation + NUM_INFORMATION_COLUMNS;
		}

		private int GetRowLocation(int rowLocation)
		{
			return rowLocation + NUM_INFORMATION_ROWS;
		}
		
		public OutputMatrix(string regionId, OutputColumn oColumn, ArrayList commonColumns, GroupTag filter, bool throwInvalidRowErrors)
		{
			m_regionId = regionId;
			m_filter = filter;
			m_throwInvalidRowErrors = throwInvalidRowErrors;

			foreach(OutputColumn oCol in commonColumns)
			{
				AddColumn(oCol);
			}

			AddColumn(oColumn);
		}

		public OutputMatrix(string regionId, ArrayList commonColumns, bool throwInvalidRowErrors)
		{
			m_regionId = regionId;
			m_throwInvalidRowErrors = throwInvalidRowErrors;
			m_filter = null;
			if(commonColumns.Count == 0)
				throw new CBaseException(String.Format("Unable to create output data in region '{0}'.", m_regionId), "Cannot create output matrix without any columns.");

			foreach(OutputColumn oCol in commonColumns)
			{
				AddColumn(oCol);
			}
		}

		// A ready matrix is one that already has an NR column defined.
		private void AddColumnToReadyMatrix(OutputColumn oColumn)
		{
			if(!m_isReady)
				throw new CBaseException(String.Format("Unable to create output data for region '{0}' - The region does not have an NR column defined.", m_regionId), "");

			if(oColumn.Cells.Count > (m_rows - NUM_INFORMATION_ROWS))
				throw new CBaseException(String.Format("Unable to add column with destination '{0}' to output in region '{1}' because the number of rows in the column being added is {2}, which is larger than the number of rows ({3}) in the existing columns.  This typically occurs when the map defines a(n) {0} column in 2 different regions, but accidentally declares the same region ID for both columns.", oColumn.Destination, m_regionId, oColumn.Cells.Count, (m_rows - NUM_INFORMATION_ROWS)), "");
			
			if(oColumn.IsDynamicList)
				m_isDynamicList = true;
			int row = GetRowLocation(0);
			int intDestLocation = (int)oColumn.Destination;
			int column = GetColumnLocation(intDestLocation);
			
			foreach(OutputCell oCell in oColumn.Cells)
			{
				if((m_validityData[intDestLocation] == false) && oCell.HasData)
				{
					m_validityData[intDestLocation] = true;
				}

				if(m_outputMatrix[row][column] != null)
				{
					throw new CBaseException(String.Format("Error while adding a column in region '{0}' - Cannot add multiple of the same type of column to the same group (in this case, the column type is '{1}').  Sometimes this happens because the map has declared a group tag, but the region ID does not match the current region (perhaps a copy/paste error), and therefore the group tag is not getting applied.", m_regionId, oColumn.Destination), "");
				}
				
				m_outputMatrix[row++][column] = oCell;
			}
		}

		public void AddColumn(OutputColumn oColumn)
		{
			m_allColumns.Add(oColumn);
			m_hasData = true;
			try
			{
				if(!m_isReady && (oColumn.Destination != E_ReadLocations.NR))
				{
					m_storedColumns.Add(oColumn);
				}
				else if(!m_isReady && (oColumn.Destination == E_ReadLocations.NR))
				{
					m_isReady = true;
					m_rows = oColumn.Cells.Count + NUM_INFORMATION_ROWS;
					m_columns = NUM_INFORMATION_COLUMNS + NUM_COLUMNS;
					m_outputMatrix = new OutputCell[m_rows][];

					for(int i = 0; i < m_rows; i++)
					{
						m_outputMatrix[i] = new OutputCell[m_columns];
					}

					AddColumnToReadyMatrix(oColumn);
					foreach(OutputColumn oCol in m_storedColumns)
					{
						AddColumnToReadyMatrix(oCol);
					}
				}
				else
				{
					AddColumnToReadyMatrix(oColumn);
				}
			}
			catch(CBaseException c)
			{
				throw c;
			}
		}

		public void Swap(ArrayList array, int left, int right)
		{
			OutputCell[] rowLeft = m_outputMatrix[GetColumnLocation(left)];
			m_outputMatrix[GetColumnLocation(left)] = m_outputMatrix[GetColumnLocation(right)];
			m_outputMatrix[GetColumnLocation(right)] = rowLeft;
			OutputCell temp = (OutputCell)array[left];
			array[left] = (OutputCell)array[right];
			array[right] = temp;
		}

		//ArrayList of OutputCell objects
		private bool IsColumnSorted(ArrayList column, ref bool allSame, IComparer comparer)
		{
			allSame = true;
			bool isSorted = true;
			OutputCell previousCell = null;
			OutputCell currentCell = null;
			string data = "";
			
			for(int i = 0; i < column.Count; ++i)
			{
				if(column[i] != null)
					data = ((DataCell)column[i]).Data;
				else
					data = "";
				if(previousCell == null)
				{
					if(RatesheetMapCommon.IsNumeric(data))
						previousCell = (OutputCell)column[i];
				}
				else
				{
					if(RatesheetMapCommon.IsNumeric(data))
					{
						currentCell = (OutputCell)column[i];
						
						if(comparer.Compare(previousCell, currentCell) != 0)
							allSame = false;
						
						if(comparer.Compare(previousCell, currentCell) > 0)
						{
							return false;
						}
					}
				}
			}
			return isSorted;
		}

		public void SortData()
		{
			if(!m_isReady)
				throw new CBaseException(String.Format("Unable to sort region '{0}' - The region does not have an NR column defined.", m_regionId), "");
			try
			{
				// Using array lists to hold the columns because outputMatrix is a jagged array
				ArrayList nrcolumn = new ArrayList();
				ArrayList pcolumn = new ArrayList();
				bool allSame = false;

				for(int i = GetRowLocation(0); i < m_rows; i++)
				{
					OutputCell nrcell = m_outputMatrix[i][GetColumnLocation((int)E_ReadLocations.NR)];
					nrcolumn.Add(nrcell);

					OutputCell pcell = m_outputMatrix[i][GetColumnLocation((int)E_ReadLocations.P)];
					pcolumn.Add(pcell);
				}

				bool isSorted = IsColumnSorted(nrcolumn, ref allSame, new NumericComparer(m_throwInvalidRowErrors));
				
				if(allSame)
				{
					bool isPSorted = IsColumnSorted(pcolumn, ref allSame, new NumericComparerAsc());
					if(isPSorted)
						return;
					else
						RatesheetMapSort.QuickSort(pcolumn, new NumericComparerAsc(), this);
				}
				else
				{
					if(isSorted)
						return;
					else
						RatesheetMapSort.QuickSort(nrcolumn, new NumericComparer(m_throwInvalidRowErrors), this);
				}
			}
			catch(CBaseException c)
			{
				throw new CBaseException(String.Format("Error while sorting region '{0}'.  DETAILS: '{1}'.", m_regionId, c.UserMessage), c.DeveloperMessage);
			}
			catch(Exception e)
			{
				throw new CBaseException(String.Format("Error while sorting region '{0}'.", m_regionId), e);
			}
		}

		public void SetCellValueFromNonInputLocation(DataCell dCell, int row)
		{
			if(!m_isReady)
				throw new CBaseException(String.Format("Error creating output for region '{0}' - The region does not have an NR column defined.", m_regionId), "");
			
			switch(dCell.Source)
			{
				case E_ReadLocations.FILE:
					break;
				case E_ReadLocations.VARIABLE:
					//TODO consolidate these - just for code clarity
					if(dCell.SourceVariableContext == E_VariableContext.Region)
					{
						if(m_variables.ContainsRegionVariable(m_regionId, dCell.SourceVariableName))
							dCell.Data = m_variables.GetVariableValue(m_regionId, dCell.SourceVariableName, row-1, dCell.SourceVariableContext);
						else
							throw new CBaseException(String.Format("Region '{0}' is trying to read from Region variable '{1}', but that variable was never defined.", m_regionId, dCell.SourceVariableName), "");
					}
					else
					{
						if(m_variables.ContainsMapOrSystemVariable(dCell.SourceVariableName))
							dCell.Data = m_variables.GetVariableValue(m_regionId, dCell.SourceVariableName, row-1, dCell.SourceVariableContext);
						else
							throw new CBaseException(String.Format("Region '{0}' is trying to read from Map/System variable '{1}', but that variable was never defined.", m_regionId, dCell.SourceVariableName), "");
					}
					break;
				case E_ReadLocations.NR:
					if(m_outputMatrix[row][GetColumnLocation((int)E_ReadLocations.NR)] != null)
						dCell.Data = ((DataCell)m_outputMatrix[row][GetColumnLocation((int)E_ReadLocations.NR)]).Data;
					break;
				case E_ReadLocations.P:
					if(m_outputMatrix[row][GetColumnLocation((int)E_ReadLocations.P)] != null)
						dCell.Data = ((DataCell)m_outputMatrix[row][GetColumnLocation((int)E_ReadLocations.P)]).Data;
					break;
				case E_ReadLocations.M:
					if(m_outputMatrix[row][GetColumnLocation((int)E_ReadLocations.M)] != null)
						dCell.Data = ((DataCell)m_outputMatrix[row][GetColumnLocation((int)E_ReadLocations.M)]).Data;
					break;
				case E_ReadLocations.QR:
					if(m_outputMatrix[row][GetColumnLocation((int)E_ReadLocations.QR)] != null)
						dCell.Data = ((DataCell)m_outputMatrix[row][GetColumnLocation((int)E_ReadLocations.QR)]).Data;
					break;
				case E_ReadLocations.TR:
					if(m_outputMatrix[row][GetColumnLocation((int)E_ReadLocations.TR)] != null)
						dCell.Data = ((DataCell)m_outputMatrix[row][GetColumnLocation((int)E_ReadLocations.TR)]).Data;
					break;
				default:
					throw new CBaseException(String.Format("Unable to set the value of a cell in region '{0}' from source location '{1}' because that location is not valid.", m_regionId, dCell.Source), "");
			}

			// When we initially add data to this output matrix, we set any column (NR, P, M, QR, TR) validity 
			// data to true if any cell in that column has valid data.  The problem is that initially, not all of the 
			// columns will have data.  Some will only get data later (here in this function) by grabbing it from another
			// column or from a variable.  We want to make sure that we update the validity of these columns to reflect the
			// fact that they now have valid data.  This is required later on for row validation.
			if(dCell.Destination == E_ReadLocations.NR 
				|| dCell.Destination == E_ReadLocations.P 
				|| dCell.Destination == E_ReadLocations.M 
				|| dCell.Destination == E_ReadLocations.QR 
				|| dCell.Destination == E_ReadLocations.TR)
			{
				if(m_validityData[(int)dCell.Destination] == false && CellContainsData(dCell))
					m_validityData[(int)dCell.Destination] = true;
			}
		}

		public void FinalizeCellValues(Variables variables)
		{
			if(!m_isReady)
				throw new CBaseException(String.Format("Error finalizing output for region '{0}' - The region does not have an NR column defined.", m_regionId), "");
		
			m_variables = variables;

			for(int i = GetColumnLocation(0); i < m_columns; ++i)
			{
				if(m_outputMatrix[GetRowLocation(0)][i] == null)
					continue;
				
				for(int j = GetRowLocation(0); j < m_rows; ++j)
				{
					DataCell dCell = (DataCell)m_outputMatrix[j][i];
					if(dCell != null && !dCell.Finalized)
					{
						try
						{
							if(dCell.Source != E_ReadLocations.FILE)
								SetCellValueFromNonInputLocation(dCell, j);
						}
						catch(CBaseException c)
						{
							throw c;
						}
					}
				}
			}
		}
		
		public void FinalizeData(Operators operators, Variables variables)
		{
			if(!m_isReady)
				throw new CBaseException(String.Format("Error finalizing output for region '{0}' - The region does not have an NR column defined.", m_regionId), "");
			
			m_variables = variables;
			
			for(int i = GetColumnLocation(0); i < m_columns; ++i)
			{
				if(m_outputMatrix[GetRowLocation(0)][i] == null)
					continue;

				//Get Math operator for this entire column
				int columnOffset = ((DataCell)m_outputMatrix[GetRowLocation(0)][i]).ColumnOffset;
				ArrayList mathOperators = operators.GetOperatorsForColumnOffset(columnOffset, E_OperatorTagType.Math);

				//grab data from sources other than input and perform math operations column by column
				for(int j = GetRowLocation(0); j < m_rows; ++j)
				{
					ArrayList cellMathOps = new ArrayList();
					cellMathOps.AddRange(mathOperators);
					DataCell dCell = (DataCell)m_outputMatrix[j][i];
					if(dCell != null && !dCell.Finalized)
					{
						try
						{
                            if (m_isDynamicList == false)
                            {
                                ArrayList rowOps = operators.GetOperatorsForRowOffset(dCell.RowOffset, E_OperatorTagType.Math);
                                foreach (AbstractOperator op in rowOps)
                                {
                                    if ((op.ColumnOffset >= columnOffset) || (cellMathOps.Count == 0))
                                        cellMathOps.Add(op);
                                    else
                                        cellMathOps.Insert(0, op);
                                }
                            }
						}
						catch(CBaseException c)
						{
							throw c;
						}

						if(cellMathOps.Count > 0)
						{
							string[] rowData = new string[NUM_COLUMNS];
							int index;
							for(int z = GetColumnLocation(0); z < m_columns; ++z)
							{
								index = z - NUM_INFORMATION_COLUMNS;
								if(m_outputMatrix[j][z] != null)
									rowData[index] = ((DataCell)m_outputMatrix[j][z]).UnoperatedData;
								else
									rowData[index] = "0";
							}
							dCell.FinalizeData(cellMathOps, m_variables, rowData, false /*useIgnoreSet*/, j);
						}
					}
				}
			}

			SortData();
			ValidateData();
		}

		private void ValidateData()
		{
			if(!m_isReady)
				throw new CBaseException(String.Format("Error validating output for region '{0}' - The region does not have an NR column defined.", m_regionId), "");
			
			int latestInvalidRowNum = -1;
			bool hasSeenValidData = false;
			string details = "";
			string prevInvalidDetails = "";
			
			
			for(int i = GetRowLocation(0); i < m_rows; i++)
			{
				if(!IsRowValid(i, ref details))
				{
					// We want to ignore invalid data at the beginning of the list of rates, so we will not record
					// this invalid row number unless we've already seen some valid data.
					if(hasSeenValidData == true)
					{
						prevInvalidDetails = details;
						latestInvalidRowNum = i;
					}
					else
					{
						if(i == m_rows-1) // OPM 19544 - record the last invalid row info to display to the user if none are valid at all
							prevInvalidDetails = details;
					}
					
					m_invalidRows.Add(i);
					//Debug
					//Tools.LogWarning("Invalid row found in region " + m_regionId + ", rowNum = " + i + ", DETAILS: " + details);
					continue;
				}
				else
				{
					hasSeenValidData = true;
					// OPM 19544 - Ignore invalid rows even in the middle of the set of data
					// Note: rows that have blank or invalid NR values have already been removed or stuck at the end
					// therefore, the only invalid rows that we should have remaining in the middle are those rows who
					// are invalid because of something wrong with their P, QR, TR or M columns
					// If all values in the NR column are the same, then invalid P values are also removed/stuck at the end
					if((latestInvalidRowNum >= 0) && (i > latestInvalidRowNum) && (hasSeenValidData == true))
					{
						string msg = String.Format("A valid row was encountered after an invalid row in region '{0}'.  Details from the previous invalid row: '{1}'",m_regionId, prevInvalidDetails );
						
						// ThrowInvalidRowErrors is a map-wide setting that people can turn off if they don't
						// want the invalid row errors to be reported.  ReportErrorForLastInvalidRow is a value that
						// is set by the IsRowValid() function for OPM 19144.  If the row was invalid because it contained
						// the value "IGNOREFORMULAERROR" in one of its cells, we will remove the row but not throw
						// an error.  This is because that particular string indicates that a formula error occured when
						// finalizing data for that cell, but that the value that was invalid is one of our allowable
						// values, such as blank, or a dash, and therefore we can silently remove it.
						if(m_throwInvalidRowErrors && m_reportErrorForLastInvalidRow)
							throw new CBaseException(msg, String.Format("Latest invalid row num = {0}, Current row = {1}", latestInvalidRowNum, i));
						else
							AddWarning(msg, false);
					}
				}
			}

			if(hasSeenValidData == false)
			{
				AddWarning(String.Format("Region '{0}' does not contain any valid rows, and therefore no data will be output.  The latest invalid row data is: '{1}'", m_regionId, prevInvalidDetails), false);
			}
		}

		public void AddWarning(string warning, bool logMessage)
		{
			m_warningList.Add(warning);
			if(logMessage)
				LogHelper.LogWarning(warning);
		}

		public void ClearWarningsList()
		{
			m_warningList = new ArrayList();
		}

		private bool IgnoreRow(int row)
		{
			return m_invalidRows.Contains(row);
		}

		public int WriteDataToWorksheet1D(OutputWriter outputWriter, int startRow, ArrayList headers)
		{
			if(!m_isReady)
				throw new CBaseException(String.Format("Error writing output for region '{0}' - The region does not have an NR column defined.", m_regionId), "");
			
			int matrixStartRow = GetRowLocation(0);
			int matrixStartColumn = GetColumnLocation(0);
			int startCol = 1;

			//Display the filters first
			foreach(GroupTag header in headers)
			{
				outputWriter.WriteCell(startRow, startCol, RatesheetMapCommon.GetDescription(header.AttributeType));
				outputWriter.WriteCell(startRow, startCol + 1, header.RangeFrom.ToString());
				outputWriter.WriteCell(startRow, startCol + 2, header.RangeTo.ToString());
				++startRow;
			}
			if(m_filter != null)
			{
				outputWriter.WriteCell(startRow, startCol, RatesheetMapCommon.GetDescription(m_filter.AttributeType));
				outputWriter.WriteCell(startRow, startCol + 1, m_filter.RangeFrom.ToString());
				outputWriter.WriteCell(startRow, startCol + 2, m_filter.RangeTo.ToString());
				++startRow;
			}

			for(int i = matrixStartRow; i < m_rows; i++)
			{
				if(IgnoreRow(i))
				{
					continue;
				}
				startCol = 1;
				for( int j = matrixStartColumn; j < m_columns; j++)
				{
					if(m_outputMatrix[i][j] == null)
					{
						outputWriter.WriteCell(startRow, startCol++, "0.000");
					}
					else
					{
						outputWriter.WriteCell(startRow, startCol++, ((DataCell)m_outputMatrix[i][j]).Data);
					}
				}
				startRow++;
			}
			return startRow; //This is now the end row
		}

		// Note - this is obsolete as of 5/1/08 and has not been maintained.  If you require 2D output, you will need
		// to verify and possibly update this function.
		public int WriteDataToWorksheet2D(OutputWriter outputWriter, ref int startRow, ref int startCol, ref bool hasIO, bool firstFragment, bool ignoreHasIO, bool ignoreNonIOHeaders)
		{
			if(!m_isReady)
				throw new CBaseException(String.Format("Error writing output for region '{0}' - The region does not have an NR column defined.", m_regionId), "");
			int matrixStartRow = GetRowLocation(0);
			int matrixStartColumn = GetColumnLocation(0);
			int stRow = startRow;
			int stCol = startCol;
			//Put the title of the group first
			if(!this.Title.Trim().Equals(""))
			{
				if((m_filter != null) && (m_filter.AttributeType == E_OperatorAttributeType.IO))
				{
					if(ignoreHasIO)
					{
						stCol = 1;
						outputWriter.WriteCell(stRow, stCol++, this.Title);
					}
				}
				else
				{
					if((!hasIO || ignoreHasIO) && !ignoreNonIOHeaders)
					{
						outputWriter.WriteCell(stRow++, stCol, this.Title);
					}
				}
			}
			int savedStartCol = stCol;
			int savedStartRow = startRow;
			for(int i = matrixStartRow; i < m_rows; i++)
			{
				if(IgnoreRow(i))
				{
					continue;
				}
				
				for( int j = matrixStartColumn; j < m_columns; j++)
				{
					if(m_outputMatrix[i][j] == null)
					{
						outputWriter.WriteCell(stRow, stCol++, "0.000");
					}
					else
					{
						outputWriter.WriteCell(stRow, stCol++, ((DataCell)m_outputMatrix[i][j]).Data);
					}
				}
				stCol = savedStartCol;
				stRow++;
			}

			if((m_filter != null) && (m_filter.AttributeType == E_OperatorAttributeType.IO))
			{
				if(ignoreHasIO)
				{
					startRow = savedStartRow;
					startCol += NUM_COLUMNS;
				}
				else
				{
					startRow = stRow;
					startCol = savedStartCol;
				}
				hasIO = true;
			}
			else
			{
				//--stRow; //temp comment
				startRow = savedStartRow;
				startCol += NUM_COLUMNS;
			}
			return stRow;
		}

		private bool IsRowValid(int row, ref string details)
		{
			if(!m_isReady)
				throw new CBaseException(String.Format("Error determining row validity in region '{0}' - The region does not have an NR column defined.", m_regionId), "");
			
			// The row is invalid if any of the following are true:
			// 1) Note Rate is <= 0
			// 2) Teaser Rate or Qual Rate is negative
			// 3) Teaser Rate, Qual Rate, or Margin is 0 while another valid row has a non-zero TR, QR or M
			OutputCell NRCell = m_outputMatrix[row][ GetColumnLocation((int)E_ReadLocations.NR) ];
			OutputCell PCell = m_outputMatrix[row][ GetColumnLocation((int)E_ReadLocations.P) ];
			OutputCell QRCell = m_outputMatrix[row][ GetColumnLocation((int)E_ReadLocations.QR) ];
			OutputCell TRCell = m_outputMatrix[row][ GetColumnLocation((int)E_ReadLocations.TR) ];
			OutputCell MCell = m_outputMatrix[row][ GetColumnLocation((int)E_ReadLocations.M) ];

			bool nrValid = IsCellValid(NRCell, E_ReadLocations.NR);
			bool pValid = IsCellValid(PCell, E_ReadLocations.P);
			bool qrValid = IsCellValid(QRCell, E_ReadLocations.QR);
			bool trValid = IsCellValid(TRCell, E_ReadLocations.TR);
			bool mValid = IsCellValid(MCell, E_ReadLocations.M);
			
			if( !nrValid || !pValid || !qrValid || !trValid || !mValid )
			{
				StringBuilder sb = new StringBuilder();
				string nrVal = (NRCell == null)?"null": NRCell.Data;
				string pVal = (PCell == null)?"null": PCell.Data;
				string qrVal = (QRCell == null)?"null": QRCell.Data;
				string trVal = (TRCell == null)?"null": TRCell.Data;
				string mVal = (MCell == null)?"null": MCell.Data;
				
				// OPM 19144 - a value of "IGNOREFORMULAERROR" indicates that there was an invalid value in the 
				// formula for this cell, BUT that the invalid value is one of our ignorable values, such as blank or dash,
				// and therefore we want to remove this row, but we don't want to throw an error
				if(nrVal.Equals("IGNOREFORMULAERROR") || pVal.Equals("IGNOREFORMULAERROR") || qrVal.Equals("IGNOREFORMULAERROR") || trVal.Equals("IGNOREFORMULAERROR") || mVal.Equals("IGNOREFORMULAERROR"))
					m_reportErrorForLastInvalidRow = false;
				else
					m_reportErrorForLastInvalidRow = true;

				sb.AppendFormat("Row {0} is invalid in region '{1}' because one of its cells contains invalid data (such as a string where a number should be, or NR or P is blank, or TR is negative, etc).  ", row, m_regionId);
				sb.AppendFormat("The values for the cells are as follows: NR = '{0}', P = '{1}', QR = '{2}', TR = '{3}', M = '{4}'.", nrVal, pVal, qrVal, trVal, mVal);
				details = sb.ToString();
				return false;
			}

			if(( m_validityData[(int)E_ReadLocations.QR] == true) && !CellContainsData(QRCell)
				|| ( m_validityData[(int)E_ReadLocations.TR] == true) && !CellContainsData(TRCell)
				|| ( m_validityData[(int)E_ReadLocations.M] == true) && !CellContainsData(MCell))
			{
				m_reportErrorForLastInvalidRow = true;
				string qrVal = (QRCell == null)?"null": QRCell.Data;
				string trVal = (TRCell == null)?"null": TRCell.Data;
				string mVal = (MCell == null)?"null": MCell.Data;
				StringBuilder sb = new StringBuilder();
				sb.AppendFormat("Row {0} is invalid in region '{1}' because one of its QR, TR, or M cells are blank while that cell in another row has a value.  ", row, m_regionId);
				sb.AppendFormat("The values for the cells are as follows: QR = '{0}', TR = '{1}', M = '{2}'.", qrVal, trVal, mVal);
				details = sb.ToString();
				return false;
			}

			return true;
		}

		private bool IsCellValid(OutputCell oCell, E_ReadLocations readLocation)
		{
			if(oCell == null)
			{
				if((readLocation == E_ReadLocations.NR) || (readLocation == E_ReadLocations.P))	
					return false;
				else
					return true;
			}
			return oCell.IsValid;
		}

		private bool CellContainsData(OutputCell oCell)
		{
			if(oCell == null)
				return false;
			return oCell.HasData;
		}

		public override string ToString()
		{
			if(!m_isReady)
				return "Matrix for region " + m_regionId + " is not ready (does not have an NR column yet).";
			string toReturn = "";
			for(int i = 0; i < m_rows; i++)
			{
				for(int j = 0; j < m_columns; j++)
				{
					if(m_outputMatrix[i][j] == null)
						continue;
					toReturn += ((DataCell)m_outputMatrix[i][j]).ToString() + ",  ";
				}
				toReturn += "\n";
			}
			return toReturn;
		}
	}
}