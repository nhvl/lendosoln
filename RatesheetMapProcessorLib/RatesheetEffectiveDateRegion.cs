using System;
using System.Collections;
using DataAccess;
using LendersOffice.Common;

namespace RatesheetMap
{
	public class RatesheetEffectiveDateRegion : Region
	{
		public RatesheetEffectiveDateRegion(CellRep region, WorksheetFileRep mapRep, RatesheetWorksheet ratesheet, E_MapContext mapContext) : base(region, mapRep, ratesheet, E_RegionType.RSEffective, mapContext){}
		public override void ValidateInput(CellRep region)
		{
			if(m_regionId.Trim().Equals(""))
				throw new CBaseException(String.Format("Error creating region: Region ID cannot be blank.  Region definition is located at map row {0}, column {1}, text = '{2}'.", region.Row, region.ColumnLetter, region.Value), "");
			
			if(!m_loObjType.Equals(E_LoObjType.None))
				throw new CBaseException(String.Format("Error in region '{0}': RSEffective Regions cannot define an LoProgramId or LoRatesheetId.", m_regionId), "");
		
			if(m_fragment == true)
				throw new CBaseException(String.Format("Error in region '{0}': RSEffective Regions cannot be fragments.", m_regionId), "");
		
			if(m_srcLoProgramId != null)
				throw new CBaseException(String.Format("Error in region '{0}': SrcLoProgramId attribute can only be declared in RateOption regions.", m_regionId), "");
			
			//if(m_optional == true) // Temporarily removing for case 22044
			//throw new CBaseException(String.Format("Error in region '{0}': RSEffective Regions cannot be defined as 'optional'.", m_regionId), "");
		}

		public override void ProcessOperators()
		{
			m_operators = null;
		}

		public override void ProcessReads(Variables variables, bool throwInvalidRowErrors)
		{
			if(this.IsFound == false)
				return;
			m_oData = null;
			m_hasOutputData = false;
			m_createOutput = false;

			ArrayList foundCells = m_mapRep.GetAllCellsByType(m_regionId, E_TagType.Read);
			ReadTag rTag = null;
			DataCell oCell = null;
			try
			{
				foreach(CellRep cell in foundCells)
				{
					rTag = new ReadTag(m_regionId, cell, m_anchorLandmarkInMap);
					oCell = rTag.ProcessCorrespondingRSCell(m_ratesheetRep, m_anchorLandmarkInRS);
					switch(rTag.DestinationLocation)
					{
						case E_ReadLocations.RSEDate:
							if(m_effectiveDate != SmallDateTime.MinValue)
								throw new CBaseException(String.Format("An effective date has already been declared in region '{0}', and is being declared again in map row {1}, column {2}.  Please only declare the effective date once.", m_regionId, cell.Row, cell.ColumnLetter), "");
							if(RatesheetMapCommon.IsNumeric(oCell.Data.Trim()))
								m_effectiveDate =  DateTime.FromOADate(double.Parse(oCell.Data.Trim()));
							else
								m_effectiveDate = DateTime.Parse(oCell.Data.Trim());
							break;
						case E_ReadLocations.RSETime:
							if(m_effectiveTime != SmallDateTime.MinValue)
								throw new CBaseException(String.Format("An effective time has already been declared in region '{0}', and is being declared again in map row {1}, column {2}.  Please only declare the effective time once.", m_regionId, cell.Row, cell.ColumnLetter), "");
							if(RatesheetMapCommon.IsNumeric(oCell.Data.Trim()))
								m_effectiveTime =  DateTime.FromOADate(double.Parse(oCell.Data.Trim()));
							else
								m_effectiveTime = DateTime.Parse(oCell.Data.Trim());
							break;
						case E_ReadLocations.RSEDatetime:
							if(m_effectiveDateTime != SmallDateTime.MinValue)
								throw new CBaseException(String.Format("An effective datetime has already been declared in region '{0}', and is being declared again in map row {1}, column {2}.  Please only declare the effective datetime once.", m_regionId, cell.Row, cell.ColumnLetter), "");
							if(RatesheetMapCommon.IsNumeric(oCell.Data.Trim()))
								m_effectiveDateTime =  DateTime.FromOADate(double.Parse(oCell.Data.Trim()));
							else
								m_effectiveDateTime = DateTime.Parse(oCell.Data.Trim());
							break;
						default:
							throw new CBaseException(String.Format("Invalid read tag destination '{0}' in region '{1}'.   This region is declared as a Ratesheet Expiration region (RSEffective) and the only destinations allowed are: RSEDate, RSETime, and RSEDatetime.", rTag.DestinationLocation, m_regionId ), "");
					}

					m_setEffectiveDateTime = true;
				}
			}
			catch(CBaseException c)
			{
				if(m_optional == true)
					AddWarning(c.UserMessage, false);
				else
					throw c;
			}
			catch(Exception e)
			{
				string msg = "";
				if(oCell != null)
					msg = String.Format("Error processing the read tags in region '{0}'.  The current ratesheet value being processed is '{1}' and is located at ratesheet row {2}, column {3}.  DETAILS - {4}", m_regionId, oCell.Data, oCell.Row, oCell.ColumnLetter, e.Message );
				else
					msg = String.Format("Error processing the read tags in region '{0}'.  DETAILS - {1}", m_regionId, e.Message );
				
				if(m_optional == true)
					AddWarning(msg, false);
				else
					throw new CBaseException(msg, "");
			}
		}
	}
}
