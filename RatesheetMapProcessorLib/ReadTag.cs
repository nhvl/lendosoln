using System;
using System.Collections;
using DataAccess;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;
using RatesheetMap.Extensions; 

namespace RatesheetMap
{
    [DebuggerDisplay("ReadTag: RegionId={RegionId}, Dst={m_destination}, Src={m_source}, RowOffset={m_rowOffset}, ColOffset={m_colOffset}, IsDynamicListStart={m_isDynamicListStart}")] 
	public class ReadTag : Tag
	{
		#region Variables
		public const string			CONTROL_NAME = "ControlName";
		public const string			DYNAMIC_LIST_TOP = "DynamicListTop";
		protected E_ReadLocations	m_source; //Optional
		protected E_ReadLocations	m_destination; //Required
		protected string			m_controlName = ""; //Optional
		protected string			m_parseStr = ""; //optional
		protected int				m_rowOffset = 0; // The difference between the row location of this read tag in the map and the anchor landmark's row
		protected int				m_colOffset = 0; // The difference between the column location of this read tag in the map and the anchor landmark's column
		protected string			m_sourceVariableName = ""; 
		protected string			m_destinationVariableName = "";
		protected E_VariableContext	m_sourceVariableContext;
		protected E_VariableContext	m_destinationVariableContext;
		protected bool				m_isDynamicListStart = false;
		protected string			m_wildcard; // Optional

		public int RowOffset
		{
			get { return m_rowOffset; }
		}

		public int ColumnOffset
		{
			get { return m_colOffset; }
		}

		public E_ReadLocations SourceLocation
		{
			get { return m_source; }
		}

		public E_ReadLocations DestinationLocation
		{
			get { return m_destination; }
		}

		public E_VariableContext SourceVariableContext
		{
			get { return m_sourceVariableContext; }
		}

		public E_VariableContext DestinationVariableContext
		{
			get { return m_destinationVariableContext; }
		}

		public string SourceVariableName
		{
			get { return m_sourceVariableName; }
		}

		public string DestinationVariableName
		{
			get { return m_destinationVariableName; }
		}

		public bool IsDynamicListStart
		{
			get { return m_isDynamicListStart; }
		}

		public string Wildcard
		{
			get { return m_wildcard; }
		}
		#endregion

        /// <summary>
        /// Instatiates new ReadTag. 
        /// </summary>
        /// <param name="regionId"></param>
        /// <param name="readTag"></param>
        /// <param name="anchorLandmarkInMap"></param>
        /// <exception cref="CBaseException">Thrown when the read tag cannot be parsed.</exception>
		public ReadTag(string regionId, CellRep readTag, LandmarkCell anchorLandmarkInMap) : base(E_TagType.Read, regionId)
		{
			m_regionId = regionId;

			try
			{
				string source = readTag.GetAttribute("src");
				m_source = ParseReadLocation(source);

				string destination = readTag.GetAttribute("dest");
				m_destination = ParseReadLocation(destination);

				if(m_source == E_ReadLocations.VARIABLE)
					ParseSourceVariable(source);

				if(m_destination == E_ReadLocations.VARIABLE)
					ParseDestinationVariable(destination);

				m_controlName = readTag.GetAttribute(CONTROL_NAME);
				m_wildcard = readTag.GetAttribute("wildcard");
				m_parseStr = readTag.GetAttribute("parsestr");
				if(m_parseStr == null)
					m_parseStr = "";
			}
			catch(CBaseException c)
			{
				throw new CBaseException(String.Format("Unable to parse a Read Tag attribute in region '{0}'.  The location of the cell being parsed is map row {1}, column {2}.  DETAILS: {3}", regionId, readTag.Row, readTag.ColumnLetter, c.UserMessage), "");
			}
			
			if(m_controlName != null && !m_controlName.Equals(""))
			{
				if(m_controlName.Trim().ToLower().Equals(DYNAMIC_LIST_TOP.ToLower()))
					m_isDynamicListStart = true;
				else
					m_isDynamicListStart = false;
			}

			m_row = readTag.Row;
			m_col = readTag.Column;
			m_rowOffset = m_row - anchorLandmarkInMap.Row;
			m_colOffset = m_col - anchorLandmarkInMap.Column;

			ValidateInput();
		}

        private E_ReadLocations ParseReadLocation(string location)
        {

            if (string.IsNullOrEmpty(location))
            {
                return E_ReadLocations.FILE;
            }

            location = location.Trim().ToLower();

            if (location == string.Empty)
            {
                return E_ReadLocations.FILE;
            }
            else if (location == "nr")
            {
                return E_ReadLocations.NR;
            }
            else if (location == "p")
            {
                return E_ReadLocations.P;
            }
            else if (location == "m")
            {
                return E_ReadLocations.M;
            }
            else if (location == "qr")
            {
                return E_ReadLocations.QR;
            }
            else if (location == "tr")
            {
                return E_ReadLocations.TR;
            }
            else if (location == "file")
            {
                return E_ReadLocations.FILE;
            }
            else if (location == "rsedate")
            {
                return E_ReadLocations.RSEDate;
            }
            else if (location == "rsetime")
            {
                return E_ReadLocations.RSETime;
            }
            else if (location == "rsedatetime")
            {
                return E_ReadLocations.RSEDatetime;
            }
            else
            {
                return E_ReadLocations.VARIABLE;
            }

        }

		// TODO - make these variable symbols constants so they can be easily changed if we ever want
        // Just for code maintenence improvement, not urgent.
		private void ParseSourceVariable(string unparsedVarName)
		{
			if(unparsedVarName.StartsWith("$"))
			{
				m_sourceVariableContext = E_VariableContext.Map;
				m_sourceVariableName = unparsedVarName.Trim('$');
			}
			else if(unparsedVarName.StartsWith("%"))
			{
				m_sourceVariableContext = E_VariableContext.Region;
				m_sourceVariableName = unparsedVarName.Trim('%');
			}
			else
				throw new CBaseException(String.Format("Invalid source variable name: {0} in region '{1}'.", unparsedVarName, m_regionId), "");
		}

		// TODO - make these constants for code maintenence ease
		private void ParseDestinationVariable(string unparsedVarName)
		{
			if(unparsedVarName.StartsWith("$"))
			{
				m_destinationVariableContext = E_VariableContext.Map;
				m_destinationVariableName = unparsedVarName.Trim('$');
			}
			else if(unparsedVarName.StartsWith("%"))
			{
				m_destinationVariableContext = E_VariableContext.Region;
				m_destinationVariableName = unparsedVarName.Trim('%');
			}
			else
				throw new CBaseException(String.Format("Invalid destination variable name '{0}' in region '{1}'.  Sometimes this happens because the tag itself is malformed (such as an '=' missing after the attribute name).", unparsedVarName, m_regionId), "");
		}

		public int GetRatesheetRow(LandmarkCell anchorLandmarkInRS)
		{
			return m_rowOffset + anchorLandmarkInRS.Row; 
		}

		public int GetRatesheetColumn(LandmarkCell anchorLandmarkInRS)
		{
			return m_colOffset + anchorLandmarkInRS.Column;
		}

        /// <summary>
        /// Gets the cel value. If there is a parse str it handles it. It also removes leading ( and trailing )  then adds a - to the front. 
        /// </summary>
        /// <param name="ratesheet"></param>
        /// <param name="anchorLandmarkInRS"></param>
        /// <returns></returns>
		public DataCell ProcessCorrespondingRSCell(RatesheetWorksheet ratesheet, LandmarkCell anchorLandmarkInRS)
		{
			int exactRow = m_rowOffset + anchorLandmarkInRS.Row; 
			int exactCol = m_colOffset + anchorLandmarkInRS.Column;
			string cellVal = "";
			if(m_source == E_ReadLocations.FILE)
			{
				cellVal = ratesheet.GetCellValue(exactRow, exactCol).Trim().ToLower();
				if(cellVal.StartsWith("(") && cellVal.EndsWith(")"))
				{
					//Trim off leading and trailing () and spaces and add negative sign
					cellVal = "-" + cellVal.TrimStart('(').TrimEnd(')').Trim();
				}
				if(!m_parseStr.Equals(""))
				{
					try
					{
						cellVal = ApplyParseStringMultiple(cellVal);
					}
					catch(CBaseException c)
					{
						throw new CBaseException(String.Format("Unable to apply parse string for cell located at ratesheet row {0}, column {1}.  DETAILS - '{2}'", exactRow, RatesheetMapCommon.GetColumnLetterFromNumber(exactCol), c.UserMessage), c.DeveloperMessage);
					}
				}

                cellVal = cellVal.ConvertPercent(); 
			}

			DataCell dCell = new DataCell(m_regionId, m_source, m_destination, cellVal, exactRow, exactCol, m_rowOffset, m_colOffset);
			
			if(m_source == E_ReadLocations.VARIABLE)
			{
				dCell.SetSourceVariableInfo(m_sourceVariableContext, m_sourceVariableName);
			}
			
			return dCell;
		}

        /// <summary>
        /// Goes ahead and does the parsestr operation on the value given. 
        /// Note, it the final val is a percent it will convert it to decimal. This only applies when the parsestr contains %s since 
        /// %f should always be a decimal. If what %s represents is not valid % then there will be no change of the final value. 
        /// </summary>
        /// <param name="inputValue"></param>
        /// <returns></returns>
        /// <exception cref="CBaseException">For no valid delimeters found in the read tag parse string. Or if it cant parse the floating point value in a %f.</exception>
		private string ApplyParseStringMultiple(string inputValue)
		{
			string finalVal = inputValue;
			string replace = "%f"; //default
			int index = m_parseStr.IndexOf("%f");
			if(index < 0)
			{
				index = m_parseStr.IndexOf("%s");
				replace = "%s";
			}

			if(index < 0)
				throw new CBaseException(String.Format("No valid delimeters found for read tag parse string '{0}' in region '{1}'.", m_parseStr, m_regionId), "");

			String[] segments = Regex.Split(m_parseStr, replace);
			int segIndex = -1;
			foreach(string segment in segments)
			{
				Match match = Regex.Match(finalVal, RatesheetMapCommon.WildcardToRegex(segment, m_wildcard));
				if(!match.Success)
				{
					throw new CBaseException(String.Format("Unable to match all parts of ParseStr value '{0}' in region '{1}'.  The entire input value from the ratesheet is '{2}'.  The part that cannot be found is '{3}'.", m_parseStr, m_regionId, inputValue, segment), "");
				}
				else
				{
					string matchedString = match.Groups[0].ToString();
					segIndex = finalVal.IndexOf(matchedString);
					int segLength = matchedString.Length;
					finalVal = finalVal.Remove(segIndex, segLength);
				}
			}	

			if(replace.Equals("%f"))
			{
				try
				{
					if(finalVal.Equals(""))
						finalVal = "0.000";
					else
						finalVal = double.Parse(finalVal.Trim()).ToString();
				}
				catch(Exception e)
				{
					throw new CBaseException(String.Format("Unable to parse floating point value using ParseStr attribute '{0}' in region '{1}'.  The entire input value that is being parsed is '{2}', specifically '{3}'.", m_parseStr, m_regionId, inputValue, finalVal), e);
				}
			}

			return finalVal;
		}	

		//Future use
		/*private string GetVariableValueFromExistingVariables(Variables variables)
		{
			//TODO consolidate these for code clarity, not urgent
			if(m_sourceVariableContext == E_VariableContext.Region)
			{
				if(variables.ContainsRegionVariable(m_regionId, m_sourceVariableName))
					return variables.GetVariableValue(m_regionId, m_sourceVariableName);
				else
					throw new CBaseException(String.Format("Could not find Region Variable '{0}'.", m_sourceVariableName), "");
			}
			else if(m_sourceVariableContext == E_VariableContext.Map)
			{
				if(variables.ContainsMapVariable(m_sourceVariableName))
					return variables.GetVariableValue(m_regionId, m_sourceVariableName);
				else
					throw new CBaseException(String.Format("Could not find Map Variable '{0}'.", m_sourceVariableName), "");
			}
			else
				throw new CBaseException(String.Format("Invalid variable name syntax for variable '{0}' used in a read tag.", m_sourceVariableName), "");
		}*/
		
		public string GetVariableValue(RatesheetWorksheet ratesheetRep, int exactRsRow, int exactRsColumn)
		{
			string cellVal = "";

			if(m_destination != E_ReadLocations.VARIABLE)
				return cellVal;

			if(m_source == E_ReadLocations.VARIABLE)
			{
				//Future use - allow for this.  Not urgent, no one has been asking for it.
				/*cellVal = GetVariableValueFromExistingVariables(regionVariables, mapVariables);
				return cellVal;*/
				throw new CBaseException(String.Format("Error getting a variable value in region '{0}'. Variable values must be read from the input, not another variable or column.", m_regionId), "");
			}
			
			cellVal = ratesheetRep.GetCellValue(exactRsRow, exactRsColumn).Trim().ToLower();
			
			if(!m_parseStr.Equals(""))
			{
				try
				{
					cellVal = ApplyParseStringMultiple(cellVal);
				}
				catch(CBaseException c)
				{
					throw new CBaseException(String.Format("Unable to apply parse string for cell located at ratesheet row {0}, column {1}.  DETAILS - '{2}'", exactRsRow, RatesheetMapCommon.GetColumnLetterFromNumber(exactRsColumn), c.UserMessage), c.DeveloperMessage);
				}
			}
			
			return cellVal.ConvertPercent();
		}
		
		public string GetVariableValue(RatesheetWorksheet ratesheetRep, LandmarkCell anchorLandmarkInRS)
		{
			int exactRow = m_rowOffset + anchorLandmarkInRS.Row; 
			int exactCol = m_colOffset + anchorLandmarkInRS.Column;
			return GetVariableValue(ratesheetRep, exactRow, exactCol);
		}

        /// <summary>
        /// Validates the read tag. 
        /// </summary>
        /// <exception cref="CBaseException">Thrown when the destination is the file.</exception>
		public void ValidateInput()
		{
			if(m_destination == E_ReadLocations.FILE)
				throw new CBaseException(String.Format("Error in region '{0}': The destination ('dest') attribute is required for every read tag, but is not found in the read tag found at map row {1}, column {2}.", m_regionId, m_row, this.ColumnLetter), "");
		}
	}
}