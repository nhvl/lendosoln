using System;
using System.Collections;

namespace RatesheetMap
{
	//http://www.codeproject.com/cs/algorithms/csquicksort.asp?df=100&forumid=32114&exp=0&select=718677#xx718677xx
	public interface ISwap
	{
		void Swap(ArrayList array, int left, int right);
	}

	public class RatesheetMapSort : ISwap, IComparer
	{
		public static IComparer comparer;
		public static ISwap swapper;
		
		public static void QuickSort(ArrayList array, IComparer comparer, ISwap swapper)
		{
			RatesheetMapSort.comparer=comparer;
			RatesheetMapSort.swapper=swapper;
			QuickSort(array, 0, array.Count-1, 0);
		}

		private static void QuickSort(ArrayList array, int lower, int upper, int passNum)
		{
			// Check for non-base case
			if (lower < upper)
			{
				// Split and sort partitions
				int split = Pivot(array, lower, upper, passNum);
				++passNum;
				QuickSort(array, lower, split-1, passNum);
				QuickSort(array, split+1, upper, passNum);
			}
		}

		private static int Pivot(ArrayList array, int lower, int upper, int passNum)
		{
			// Pivot with first element
			int left = lower+1;
			object pivot = array[lower];
			int right = upper;

			// Partition array elements
			while (left <= right)
			{
				// Find item out of place
				while ( (left <= right) && (comparer.Compare(array[left], pivot) <= 0) )
				{
					++left;
				}

				while ( (left <= right) && (comparer.Compare(array[right], pivot) > 0) )
				{
					--right;
				}

				// Swap values if necessary
				if (left < right)
				{
					swapper.Swap(array, left, right);
					++left;
					--right;
				}
			}

			// Move pivot element
			swapper.Swap(array, lower, right);
			return right;
		}

		public void Swap(ArrayList array, int left, int right)
		{
			object swap=array[left];
			array[left]=array[right];
			array[right]=swap;
		}

		public int Compare(object a, object b)
		{
			return a.ToString().CompareTo(b.ToString());
		}
	}
}
