using System.Web.Configuration;

namespace RatesheetMap
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Gateways;

    public class RatesheetMapProcessor
	{
		#region Variables
        private static string   ALL_TABS = "ALL_TABS";
		private Hashtable		m_RateOptionRegions;
		private Hashtable		m_AdjustTableRegions;
		private HashSet<string>	m_RegionNames;
		private double			m_executionSeconds = 0;
		private double			m_executionSecondsInput = 0;
		private double			m_executionSecondsOutput = 0;
		private double			m_executionSecondsPrepare = 0;
		private Variables		m_variables = null;
        private string          m_ratesheetName;
        private string          m_ratesheetNameNoPath;
        private string          m_mapName;
        private string          m_mapNameNoPath;
        private string          m_tabName;
		private MapTag			m_mapTag;
		private ArrayList		m_errorList;
		private ArrayList		m_warningList;
		private int				m_numRegions;
		private bool			m_isStandAloneMode;
		private int				m_numROProgramsOutput;
		private int				m_numATProgramsOutput;
		private int				m_numRORegionsFound;
		private int				m_numATRegionsFound;
		private ArrayList		m_rateOptionOutputFiles = new ArrayList();
		private ArrayList		m_adjustTableOutputFiles = new ArrayList();
		private bool			m_mapExists = false;
		private bool			m_success = true;
		private DateTime		m_effectiveDate = SmallDateTime.MinValue;
		private DateTime		m_effectiveTime = SmallDateTime.MinValue;
		private DateTime		m_effectiveDateTime = SmallDateTime.MinValue;
		private E_Timezone		m_timezone = E_Timezone.P;
		private bool			m_dateInfoSet = false;
		//private bool			m_rsEffectiveRegionDeclared = false; // Future use
		private string			m_SAEName = "";
		private Hashtable		m_SystemVariablesToOverwrite = null;
		private bool			m_isDevTester = false; // Useful for testing new features on localhost
		private bool			m_getDetailedRegionInfo = false;
		private ArrayList		m_pointerRegionPrograms = new ArrayList();
		private InputFileRep					m_rsMapInputFile;
		private RatesheetFileRep				m_rsFileRep = null;
		private RatesheetWorksheet				m_rsWorksheet = null;
        private SortedList      m_outputFiles = new SortedList();

		
		public static string RS_MAP_TEST_OUTPUT_FILES_PATH
		{
			get { return System.Configuration.ConfigurationManager.AppSettings["RS_MAP_TEST_OUTPUT_FILES_PATH"]; }
		}

		public static string FILES_PATH
		{
			get { return System.Configuration.ConfigurationManager.AppSettings["FILES_PATH"]; }
		}

		public static string RS_MAP_FILES_PATH
		{
			get { return System.Configuration.ConfigurationManager.AppSettings["RS_MAP_FILES_PATH"]; }
		}

		public static string RS_MAP_TEST_FILES_PATH
		{
			get { return System.Configuration.ConfigurationManager.AppSettings["RS_MAP_TEST_FILES_PATH"]; }
		}

		public static bool PrintRatesheetMapOutputIn1D
		{
			get { return System.Configuration.ConfigurationManager.AppSettings["PrintRatesheetMapOutputIn1D"] == "True";  }
		}
		
		public ArrayList RateOptionOutputFilenames
		{
			get { return m_rateOptionOutputFiles; }
		}

		public DateTime EffectiveDate
		{
			get { return m_effectiveDate; }
		}

		public bool DateInfoSet
		{
			get { return m_dateInfoSet; }
		}

		public DateTime EffectiveTime
		{
			get { return m_effectiveTime; }
		}

		public E_Timezone Timezone
		{
			get { return m_timezone;}
		}

		public DateTime EffectiveDateTime
		{
			get 
			{ 
				DateTime effDT = m_effectiveDateTime;
				if(m_effectiveDateTime == SmallDateTime.MinValue)
				{	
					if(m_effectiveDate != SmallDateTime.MinValue)
					{
						effDT = m_effectiveDate;
						if(m_effectiveTime != SmallDateTime.MinValue)
						{
							string effDTString = m_effectiveDate.ToShortDateString() + m_effectiveTime.ToShortTimeString();
							try
							{
								effDT = DateTime.Parse(effDTString);
							}
							catch{}
						}
					}
				}
				return effDT; 
			}
		}

		public DateTime EffectiveDateTimePT
		{
			get 
			{ 
				DateTime effDT = m_effectiveDateTime;
				if(m_effectiveDateTime == SmallDateTime.MinValue)
				{	
					if(m_effectiveDate != SmallDateTime.MinValue)
					{
						effDT = m_effectiveDate;
						if(m_effectiveTime != SmallDateTime.MinValue)
						{
							string effDTString = m_effectiveDate.ToShortDateString() + " " + m_effectiveTime.ToShortTimeString();
							try
							{
								effDT = DateTime.Parse(effDTString);
							}
							catch {}
						}
					}
				}

				// OPM 20654 - do not apply timezone to effective date if it's midnight
				if(!effDT.ToShortTimeString().Equals("12:00 AM"))
				{
					switch(m_timezone)
					{
						case E_Timezone.P:
							break;
						case E_Timezone.E:
							effDT = effDT.AddHours(-3);
							break;
						case E_Timezone.C:
							effDT = effDT.AddHours(-2);
							break;
						case E_Timezone.M:
							effDT = effDT.AddHours(-1);
							break;
						default:
							break;
					}
				}

				return effDT; 
			}
		}

		public bool AccessedMarkedIndexFiles
		{
			get 
			{ 
				if(m_variables == null)
					return false;
				else
					return m_variables.AccessedSystemVariables; 
			}
		}

		public ArrayList AdjustmentTableOutputFilenames
		{
			get { return m_adjustTableOutputFiles; }
		}

		public bool MapExists
		{
			get { return m_mapExists; }
		}

		// Indicates whether or not there are any errors at all during this run
        public bool IsCompletelySuccessful
		{
			get { return m_success; }
		}

        private string CurrentRateOptionOutputFilename
        {
            get 
            {
                if (string.IsNullOrEmpty(m_ratesheetName) || string.IsNullOrEmpty(m_mapName) ||  string.IsNullOrEmpty(m_tabName))
                    return "";
                
                string ratesheetFilenameNoExtension = System.IO.Path.GetFileNameWithoutExtension(m_ratesheetName);
                string mapFilenameNoExtension = System.IO.Path.GetFileNameWithoutExtension(m_mapName);
                string outputFilename = ratesheetFilenameNoExtension + "_" + mapFilenameNoExtension + "_" + m_tabName + "_Output.csv";

                string combinedRateOption = "";
                if (m_isStandAloneMode)
                    combinedRateOption = System.IO.Path.Combine(RS_MAP_TEST_OUTPUT_FILES_PATH, outputFilename);
                else
                    combinedRateOption = System.IO.Path.Combine(FILES_PATH + "Temp", outputFilename);

                return combinedRateOption; 
            }
        }

        private string CurrentAdjustTableOutputFilename
        {
            get 
            {
                if (m_ratesheetName == "" || m_mapName == "" || m_tabName == "")
                    return "";
                
                string ratesheetFilenameNoExtension = System.IO.Path.GetFileNameWithoutExtension(m_ratesheetName);
                string mapFilenameNoExtension = System.IO.Path.GetFileNameWithoutExtension(m_mapName);
                string outputFilenameAdjTable = ratesheetFilenameNoExtension + "_" + mapFilenameNoExtension + "_" + m_tabName + "_AdjTableOutput.csv";

                string combinedAdjTable = "";
                if (m_isStandAloneMode)
                    combinedAdjTable = System.IO.Path.Combine(RS_MAP_TEST_OUTPUT_FILES_PATH, outputFilenameAdjTable);
                else
                    combinedAdjTable = System.IO.Path.Combine(FILES_PATH + "Temp", outputFilenameAdjTable);

                return combinedAdjTable; 
            }
        }

		#endregion

        public int ErrorCount
        {
            get { return m_errorList.Count; }
        }
        private StringBuilder m_processDebugStringBuilder = new StringBuilder();
        public StringBuilder ProcessDebugStringBuilder { get { return m_processDebugStringBuilder; } }
		public RatesheetMapProcessor(bool runStandaloneMode, Hashtable systemVariables)
		{
			m_isStandAloneMode = runStandaloneMode;
			m_SystemVariablesToOverwrite = systemVariables;
            m_outputFiles.Add(ALL_TABS, new OutputFile(ALL_TABS, false));
            EnsureDirectoriesAreCreated();
		}

		public RatesheetMapProcessor(bool runStandaloneMode)
		{
			m_isStandAloneMode = runStandaloneMode;
            m_outputFiles.Add(ALL_TABS, new OutputFile(ALL_TABS, false));
            EnsureDirectoriesAreCreated();
		}

		public RatesheetMapProcessor(string ratesheetLocation, bool runStandaloneMode)
		{
			m_isStandAloneMode = runStandaloneMode;
            m_outputFiles.Add(ALL_TABS, new OutputFile(ALL_TABS, false));
            EnsureDirectoriesAreCreated();
			Process(ratesheetLocation);
		}

		public RatesheetMapProcessor(string ratesheetLocation, bool runStandaloneMode, string saeName, bool getDetailedRegionInfo, bool isDevTester)
		{
			m_isDevTester = isDevTester;
			m_getDetailedRegionInfo = getDetailedRegionInfo;
			m_isStandAloneMode = runStandaloneMode;
			m_SAEName = saeName;
            m_outputFiles.Add(ALL_TABS, new OutputFile(ALL_TABS, false));
            EnsureDirectoriesAreCreated();
			Process(ratesheetLocation);
		}

        // OPM 26575 - just need to create the test output files directory for now.
        private void EnsureDirectoriesAreCreated()
        {
            try
            {
                if (!System.IO.Directory.Exists(WebConfigurationManager.AppSettings["RS_MAP_TEST_OUTPUT_FILES_PATH"]))
                {
                    System.IO.Directory.CreateDirectory(RS_MAP_TEST_OUTPUT_FILES_PATH);
                }
            }
            catch (Exception e)
            {
                Tools.LogError("Unable to create the test output files directory: " + e.ToString());
            }
        }
		
		public string Process(string ratesheetLocation)
		{
            m_processDebugStringBuilder = new StringBuilder();

            Stopwatch sw = Stopwatch.StartNew();

			DateTime startTime = DateTime.Now;
			m_rsMapInputFile = new InputFileRep();

			m_variables = new Variables(m_isStandAloneMode);
			
			// Future use
			/*if(!m_isStandAloneMode && m_SystemVariablesToOverwrite != null)
			{
				m_variables.OverwriteSystemVariables(m_SystemVariablesToOverwrite);
			}*/

			m_RateOptionRegions = new Hashtable();
			m_AdjustTableRegions = new Hashtable();
			m_RegionNames = new HashSet<string>();
			m_errorList = new ArrayList();
			m_warningList = new ArrayList();
			m_numRegions = 0;
			m_numROProgramsOutput = 0;
			m_numATProgramsOutput = 0;
			m_numRORegionsFound = 0;
			m_numATRegionsFound = 0;

			try
			{
				m_ratesheetName = ratesheetLocation;
				m_ratesheetNameNoPath = System.IO.Path.GetFileName(ratesheetLocation);
				
				if(!FileOperationHelper.Exists(ratesheetLocation))
				{
					AddError("Ratesheet file does not exist: " + ratesheetLocation, false, false, true /*mark entire map error*/);
					CreateAndLogFinalInfo();
					return "";
				}

                if (!RatesheetMapCommon.IsCPXFile(ratesheetLocation) && !RatesheetMapCommon.IsExcelFile(ratesheetLocation))
				{
                    AddError("Ratesheet file is not a valid excel or CPX file: " + ratesheetLocation, false, false, true /*mark entire map error*/);
					CreateAndLogFinalInfo();
					return "";
				}

				try
				{
					ArrayList mapLocations = new ArrayList();
					try
					{
						// If there is a reference file for this ratesheet, there might be multiple maps that need to be processed
						mapLocations = GetMapLocations(ratesheetLocation);
					}
					catch(CBaseException cb)
					{
						AddError(cb.Message, false, false, true /*maek entire map error*/);
					}

					if(m_mapExists)
					{
						// Create the representation of the ratesheet, but only if there exists at least 1 map to process it
                        sw = Stopwatch.StartNew();
                        using (PerformanceStopwatch.Start("RatesheetFileRepCPX.ImportRatesheet"))
                        {
                            m_rsFileRep = new RatesheetFileRepCPX();
                            m_rsFileRep.ImportRatesheet(ratesheetLocation);
                        }
                        sw.Stop();
                        m_processDebugStringBuilder.AppendLine("        RatesheetFileRepCPX.ImportRatesheet executed in " + sw.ElapsedMilliseconds + "ms.");
					}
                    sw = Stopwatch.StartNew();
                    using (PerformanceStopwatch.Start("RatesheetMapProcess ProcessRegion"))
                    {
                        foreach (string mapFilename in mapLocations)
                        {
                            m_variables.ClearMapVariables();

                            try
                            {
                                // Dispose of the previous map representation - we might be processing another map from a reference file for this ratesheet
                                ((IDisposable)m_rsMapInputFile).Dispose();

                                // Create the representation of the map file being processed
                                m_rsMapInputFile = new InputFileRep();
                                m_rsMapInputFile.CreateRep(mapFilename);
                            }
                            catch (CBaseException cbe)
                            {
                                throw cbe;
                            }
                            catch (Exception e)
                            {
                                throw new CBaseException("Unable to create representation from compressed map file: " + mapFilename, e);
                            }

                            m_mapName = mapFilename;
                            m_mapNameNoPath = System.IO.Path.GetFileName(mapFilename);

                            int numMapWorksheets = m_rsMapInputFile.NumWorksheets;

                            if (numMapWorksheets == 0)
                            {
                                throw new CBaseException(String.Format("No worksheets (that begin with 'm-') found in file: '{0}'.", mapFilename), "");
                            }

                            if (numMapWorksheets > m_rsFileRep.NumWorksheets)
                            {
                                AddWarning(String.Format("There are more map tabs ({0}) than ratesheet tabs ({1}) for ratesheet '{2}' and map file '{3}'. ", numMapWorksheets, m_rsFileRep.NumWorksheets, m_rsFileRep.RatesheetName, m_mapNameNoPath), false);
                            }

                            //if(numMapWorksheets < m_rsFileRep.NumWorksheets) // Removing per OPM 21917 - can be put back in if desired in the future
                            //AddWarning(String.Format("There are more ratesheet tabs ({0}) than map tabs ({1}) in ratesheet file '{2}' and map file '{3}'. ", m_rsFileRep.NumWorksheets, numMapWorksheets, m_rsFileRep.RatesheetName, m_mapNameNoPath), false);

                            //Process each tab in the map file
                            for (int i = 0; i < numMapWorksheets; i++)
                            {
                                string mapTabName = (string)m_rsMapInputFile.WorksheetNames[i];
                                if (!mapTabName.StartsWith("m-"))
                                {
                                    string warningMsg = string.Format("Ignoring map tab '{0}' in map '{1}' while processing ratesheet '{2}' because its name is not preceeded by 'M-'", mapTabName, m_mapNameNoPath, m_rsFileRep.RatesheetName);
                                    AddWarning(warningMsg, false);
                                    continue;
                                }

                                //Remove the M- in the map tab name to get the matching tab name in the ratesheet file
                                string ratesheetTabName = mapTabName.Substring(2).Trim();
                                m_rsWorksheet = m_rsFileRep.GetWorksheet(ratesheetTabName);

                                if (m_rsWorksheet == null)
                                {
                                    AddError(String.Format("Unable to find ratesheet tab name '{0}'.  Ratesheet filename = '{1}', Map filename = '{2}'.", ratesheetTabName, m_rsFileRep.RatesheetName, m_mapNameNoPath), false, false, false);
                                    continue;
                                }

                                m_tabName = mapTabName;

                                //Create the representation of the the current tab/worksheet in the map file
                                WorksheetFileRep wsFileRep = m_rsMapInputFile.GetWorksheet(m_tabName);
                                if (wsFileRep == null)
                                {
                                    throw new CBaseException(String.Format("Could not find worksheet called '{0}'", m_tabName), "");
                                }

                                //Record unused tags in this worksheet (helps for debugging incorrect output)
                                foreach (CellRep unusedTag in wsFileRep.UnusedTags)
                                {
                                    AddWarning(String.Format("Unused tag in map tab '{0}' located at row {1}, column {2}, Text= '{3}'.  Tags are marked as unused if they do not contain a region ID.  If your tag does contain a region ID, make sure it does not follow an 'str' or 'parseStr' tag.", wsFileRep.Name, unusedTag.Row, unusedTag.ColumnLetter, unusedTag.Value), false);
                                }

                                // Get the map tag that will tell what context (global or lender) this map should be processed in.
                                ArrayList mapCells = wsFileRep.GetAllCellsByType(E_TagType.Map);
                                m_mapTag = new MapTag(mapCells, ratesheetLocation, m_mapNameNoPath); //There should only be 1 for now

                                TimeSpan interval1 = DateTime.Now - startTime; // testing
                                m_executionSecondsPrepare = interval1.TotalSeconds; //testing
                                ProcessRegions(); // Process all of the regions in this tab of the map file

                            } //end loop through map tabs
                        } //end loop through map files
                    }
                    sw.Stop();
                    m_processDebugStringBuilder.AppendLine("        Map Count=" + mapLocations.Count + " process regions in " + sw.ElapsedMilliseconds + "ms.");

					if((m_effectiveDateTime == SmallDateTime.MinValue) && (m_effectiveDate == SmallDateTime.MinValue))
					{
						if(m_effectiveTime != SmallDateTime.MinValue)
						{
							m_effectiveDate = DateTime.Today;
						}
					}
				} // End inner try
				catch(CBaseException c)
				{
					AddError(c.Message, false, false, true /*mark entire map error*/);
				}
				catch(Exception e)
				{
					AddError("Error processing ratesheet map: " + e.ToString(), false, false, true /*mark entire map error*/);
				}
			} // End outer try
			catch(CBaseException c)
			{
				AddError(c.Message, false, false, true /*mark entire map error*/);
			}
			catch(Exception e)
			{
				AddError("Error processing ratesheet: " + e.ToString(), false, false, true /*mark entire map error*/);
			}
			finally //remove this
			{
				if(m_rsMapInputFile != null)
				{
					((IDisposable)m_rsMapInputFile).Dispose();
					m_rsMapInputFile = null;
				}
				if(m_rsFileRep != null)
				{
					((IDisposable)m_rsFileRep).Dispose();
					m_rsFileRep = null;
				}
			}

			TimeSpan interval = DateTime.Now - startTime;
			m_executionSeconds = interval.TotalSeconds;
			return CreateAndLogFinalInfo();
		}

		private string CreateAndLogFinalInfo()
		{
			string finalInfo = "";
			try
			{
				StringBuilder info = new StringBuilder();
				string ratesheetName = "";
                if (m_rsFileRep == null || m_rsFileRep.RatesheetName.Trim().Equals(""))
                {
                    ratesheetName = m_ratesheetNameNoPath;
                }
                else
                {
                    ratesheetName = m_rsFileRep.RatesheetName;
                }

				info.AppendFormat("Ratesheet: {0}{1}", ratesheetName, Environment.NewLine);
                if (m_isStandAloneMode)
                {
                    info.AppendFormat("SAE Name: {0}{1}", m_SAEName, Environment.NewLine);
                }

				info.AppendFormat("{2}REGIONS: {0}{2}TOTAL EXECUTION TIME (SECONDS): {1}{2}", m_numRegions ,m_executionSeconds, Environment.NewLine);
				info.AppendFormat("EXECUTION TIME INPUT ONLY (SECONDS): {0}{2}EXECUTION TIME OUTPUT ONLY (SECONDS): {1}{2}", m_executionSecondsInput ,m_executionSecondsOutput, Environment.NewLine);
				info.AppendFormat("# RATE OPTION REGIONS PROCESSED: {0}{2}# ADJUSTMENT TABLE REGIONS PROCESSED: {1}{2}", m_numRORegionsFound, m_numATRegionsFound, Environment.NewLine);
				info.AppendFormat("# RATE OPTION PROGRAMS OUTPUT: {0}{2}# ADJUSTMENT TABLE POLICIES OUTPUT: {1}{2}", m_numROProgramsOutput, m_numATProgramsOutput, Environment.NewLine);

                if (m_dateInfoSet)
                {
                    info.AppendFormat("EFFECTIVE DATETIME (PT): {0}{1}", this.EffectiveDateTimePT, Environment.NewLine);
                }
                else
                {
                    info.AppendFormat("EFFECTIVE DATETIME (PT): {0}{1}", "Not Declared", Environment.NewLine);
                }
				
				info.AppendFormat("ERROR COUNT: {0}{2}WARNING COUNT: {1}{2}", m_errorList.Count , m_warningList.Count, Environment.NewLine);
			
				if(m_errorList.Count > 0)
				{
					StringBuilder errors = new StringBuilder();
					foreach(string errorMsg in m_errorList)
					{
						errors.AppendFormat("{0}{0}-- {1}", Environment.NewLine, errorMsg);
					}
					info.AppendFormat("{0}{0}ERRORS: {1}{0}{0}", Environment.NewLine, errors.ToString());
				}

				if(m_warningList.Count > 0)
				{
					StringBuilder warnings = new StringBuilder();
					foreach(string warningMsg in m_warningList)
					{
						warnings.AppendFormat("{0}{0}-- {1}", Environment.NewLine, warningMsg);
					}
					info.AppendFormat("{0}{0}WARNINGS: {1}{0}{0}", Environment.NewLine, warnings.ToString());
				}

				finalInfo = info.ToString();

                if (m_errorList.Count > 0)
                {
                    LogHelper.LogError(finalInfo);
                }
                else if (m_warningList.Count > 0)
                {
                    LogHelper.LogWarning(finalInfo);
                }
                else
                {
                    LogHelper.Log(finalInfo);
                }
			}
			catch(Exception e)
			{
				LogHelper.LogError(String.Format("Error when trying to log final messages for ratesheet map processor: {0}", e.ToString()));
			}

			return finalInfo;
		}

        public bool DoesOutputFileHaveError(string outputFilename)
        {
            if (m_outputFiles.ContainsKey(ALL_TABS))
            {
                if (((OutputFile)m_outputFiles[ALL_TABS]).HasError)
                {
                    return true;
                }
            }

            if (m_outputFiles.ContainsKey(outputFilename))
            {
                return ((OutputFile)m_outputFiles[outputFilename]).HasError;
            }
            else
            {
                return false;
            }
        }

        private void MarkEntireMapError()
        {
            if (m_outputFiles.ContainsKey(ALL_TABS))
            {
                ((OutputFile)m_outputFiles[ALL_TABS]).HasError = true;
            }
            else
            {
                OutputFile oFile = new OutputFile(ALL_TABS, true);
                m_outputFiles.Add(ALL_TABS, oFile);
            }
        }
        
        private void MarkOutputFileError()
        {
            if (!string.IsNullOrEmpty(CurrentRateOptionOutputFilename))
            {
                if (m_outputFiles.ContainsKey(CurrentRateOptionOutputFilename))
                {
                    ((OutputFile)m_outputFiles[CurrentRateOptionOutputFilename]).HasError = true;
                }
                else
                {
                    OutputFile oFile = new OutputFile(CurrentRateOptionOutputFilename, true);
                    m_outputFiles.Add(CurrentRateOptionOutputFilename, oFile);
                }
            }

            if (!string.IsNullOrEmpty(CurrentAdjustTableOutputFilename))
            {
                if (m_outputFiles.ContainsKey(CurrentAdjustTableOutputFilename))
                {
                    ((OutputFile)m_outputFiles[CurrentAdjustTableOutputFilename]).HasError = true;
                }
                else
                {
                    OutputFile oFile = new OutputFile(CurrentAdjustTableOutputFilename, true);
                    m_outputFiles.Add(CurrentAdjustTableOutputFilename, oFile);
                }
            }
        }

		private void AddError(string error, bool logMessage)
		{
            m_success = false;
			m_errorList.Add(error);
            MarkOutputFileError();

            if (logMessage)
            {
                LogHelper.LogError(error);
            }
		}

        private void AddError(string error, bool logMessage, bool markOutputFileError, bool markEntireMapError)
        {
            m_success = false;
            m_errorList.Add(error);
            if (markOutputFileError)
            {
                MarkOutputFileError();
            }

            if (markEntireMapError)
            {
                MarkEntireMapError();
            }

            if (logMessage)
            {
                LogHelper.LogError(error);
            }
        }

		private void AddWarning(string warning, bool logMessage)
		{
			m_warningList.Add(warning);
            if (logMessage)
            {
                LogHelper.LogWarning(warning);
            }
		}

		private void AddErrors(ArrayList errors, bool logMessages)
		{
            if (errors.Count > 0)
            {
                m_success = false;
                MarkOutputFileError();
            }

			foreach(string error in errors)
			{
				m_errorList.Add(error);
                if (logMessages)
                {
                    LogHelper.LogError(error);
                }
			}
		}

		private void AddWarnings(ArrayList warnings, bool logMessages)
		{
			foreach(string warning in warnings)
			{
				m_warningList.Add(warning);
                if (logMessages)
                {
                    LogHelper.LogWarning(warning);
                }
			}
		}

		// We might be using a reference file that contains multiple maps to process this ratesheet
		// Map filenames are the ratesheet name followed by _MAP, for example, for a ratesheet called
		// CitiRs.xls, the corresponding map file will be called CitiRs_Map.cpx or CitiRs_Map.ref if it
		// is a reference file.
		private ArrayList GetMapLocations(string ratesheetLocation)
		{
			ArrayList filenames = new ArrayList();
			string rsFilenameNoExtension = System.IO.Path.GetFileNameWithoutExtension(ratesheetLocation);
			string mapFilename = rsFilenameNoExtension + "_MAP.CPX";

			string combined = "";
            if (m_isStandAloneMode)
            {
                combined = System.IO.Path.Combine(RS_MAP_TEST_FILES_PATH, mapFilename);
            }
            else
            {
                combined = System.IO.Path.Combine(RS_MAP_FILES_PATH, mapFilename);
            }
			
			if(!FileOperationHelper.Exists(combined))
			{
				filenames = GetMapFilenamesFromRef(combined);
                if (filenames.Count == 0)
                {
                    throw new CBaseException(String.Format("Either no valid map filenames exist in reference file, or unable to find a map file or reference file for ratesheet '{0}'. Looking for '{1}' or .REF", rsFilenameNoExtension, combined), "");
                }
			}
			else
			{
				filenames.Add(combined);
				string refFilename = System.IO.Path.ChangeExtension(combined, ".ref");
                if (FileOperationHelper.Exists(refFilename))
                {
                    AddWarning(String.Format("A map file exists for ratesheet '{0}', but a reference file also exists, and will not get processed.", rsFilenameNoExtension), false);
                }
			}


			m_mapExists = true;
			return filenames;
		}
		
		// Retrieve all of the map filenames listed in the reference file for this ratesheet.  This is
		// done if no maps exist with a name that matches the ratesheet name. 
		private ArrayList GetMapFilenamesFromRef(string fullPath)
		{
			string mapPath = "";
			string line = null;
			string refFilename = "";
			try
			{
				ArrayList mapFilenamesFromRef = new ArrayList();
				refFilename = System.IO.Path.ChangeExtension(fullPath, ".ref");

				if(!FileOperationHelper.Exists(refFilename))
					return mapFilenamesFromRef;

				mapPath = System.IO.Path.GetDirectoryName(fullPath);
				StreamReader reader = new StreamReader(refFilename);
				while ((line = reader.ReadLine()) != null) 
				{
					if(line.Trim() != "")
					{
						string refEntry = System.IO.Path.GetFileNameWithoutExtension(line.Trim()) + ".CPX";
						string mapFilename = System.IO.Path.Combine(mapPath, refEntry);
						if(mapFilenamesFromRef.Contains(mapFilename))
						{
							AddWarning(String.Format("Map filename '{0}' has been listed more than once in REF file '{1}'.  This duplicate entry will be ignored.", line, refFilename), false);
							continue;
						}

						if(FileOperationHelper.Exists(mapFilename))
							mapFilenamesFromRef.Add(mapFilename);
						else
							AddError(String.Format("Unable to find file '{0}' listed in REF file '{1}'.  This file will not be used in processing.", line, refFilename), false);
					}
				}
				return mapFilenamesFromRef;
			}
			catch(Exception e)
			{
				if(line == null)
					line = "";
				throw new CBaseException(String.Format("Unable to get map filename(s) from reference file '{0}' - {1}  Map path = '{2}', map filename = '{3}'.", refFilename, e.Message, mapPath, line), e);
			}
		}

		private void ProcessRegions()
		{	
			DateTime startTime = DateTime.Now;
			
			ArrayList regionTags = new ArrayList();
			WorksheetFileRep wsFileRep = null;
			ArrayList foundCells = new ArrayList();
			
			wsFileRep = m_rsMapInputFile.GetWorksheet(m_tabName);
            if (wsFileRep != null)
            {
                foundCells = wsFileRep.GetAllCellsByType(E_TagType.Region);
            }
			
			m_numRegions += foundCells.Count;
            if (foundCells.Count == 0)
            {
                throw new CBaseException(String.Format("Unable to process map '{0}' for ratesheet '{1}'- No regions found in tab '{2}'.", m_mapNameNoPath, m_rsFileRep.RatesheetName, m_tabName), "");
            }
			
			Region region = null;
			foreach(CellRep cRep in foundCells)
			{
				try
				{
					region = Region.CreateRegion(cRep, wsFileRep, m_rsWorksheet, m_mapTag.MapContext);
					AddNewRegion(region);
					
					if((m_mapTag.MapContext == E_MapContext.Lender) && (region.RegionType == E_RegionType.RateOption))
					{
						try
						{
							if(!m_mapTag.HasMatchingProgramIds(region.ObjectId))
							{
								region.CreateOutput = false;
								if(m_mapTag.IsLPOptional == true && m_isStandAloneMode == false)
								{
									AddWarning(String.Format("Cannot find program IDs for Region '{0}' in map '{1}', but LpOptional = 'Y', so this region is being skipped.", region.RegionId, m_mapNameNoPath), false);
									continue;
								}
								else
								{
									throw new CBaseException(String.Format("Unable to get program ID in map '{0}' using Client Code '{1}' and RatesheetId '{2}'.", m_mapNameNoPath, m_mapTag.ClientCode, region.ObjectId), "");
								}
							}
						}
						catch(CBaseException c)
						{
                            if (m_mapTag.IsLPOptional == true && m_isStandAloneMode == false)
                            {
                                region.CreateOutput = false;
                                AddWarning("Error obtaining program IDs (but LpOptional = 'Y', so this region will be ignored): " + c.ToString(), false);
                                continue;
                            }
                            else
                            {
                                throw c;
                            }
						}
					}

					region.ProcessLandmarks();

					if(region.IsFound)
					{
						region.ProcessOperators(); 
						region.ProcessReads(m_variables, m_mapTag.ThrowInvalidRowErrors);

						if(region.RegionType == E_RegionType.RSEffective)
						{
                            if (m_dateInfoSet)
                            {
                                AddWarning(String.Format("Region '{0}' is of type RSEffective, but another RSEffective region was already declared in this map.  Therefore, the date information from this region will be ignored.", region.RegionId), false);
                            }
                            else
                            {
                                SetEffectiveDateInfo(region);
                            }
						}
					}
					else
					{
						region.CreateOutput = false;
					}
				}
				catch(CBaseException c)
				{
					string tempRId = "";
                    if (region != null)
                    {
                        tempRId = region.RegionId;
                    }
					string msg = String.Format("Error processing region '{4}' in ratesheet '{0}', map '{3}', map tab '{1}' - {2}", m_ratesheetNameNoPath, m_tabName, c, m_mapNameNoPath, tempRId);
                    if (region != null)
                    {
                        region.HasError = true;
                    }
					AddError(msg, false);
					continue;
				}
				catch(Exception e)
				{
					string msg = String.Format("Error processing region in ratesheet '{0}', map '{3}', map tab '{1}' - {2}", m_ratesheetNameNoPath, m_tabName, e, m_mapNameNoPath);
                    if (region != null)
                    {
                        region.HasError = true;
                    }
					AddError(msg, false);
					continue;
				}
				finally
				{
					if(region != null)
					{
						AddWarnings(region.WarningList, false);
						region.ClearWarningsList();
					}
				}	
			} //end foreach

			TimeSpan interval = DateTime.Now - startTime;
			m_executionSecondsInput += interval.TotalSeconds;

			#region WriteOutput
			try
			{
				DateTime startTime2 = DateTime.Now;
                WriteRateOptionOutput(CurrentRateOptionOutputFilename);
                WriteAdjustTableOutput(CurrentAdjustTableOutputFilename);
				TimeSpan interval2 = DateTime.Now - startTime2;
				m_executionSecondsOutput += interval2.TotalSeconds;
			}
			catch(CBaseException c)
			{
				AddError(String.Format("Error writing output for ratesheet '{0}', map '{3}', map tab '{1}' - {2}", m_ratesheetNameNoPath, m_tabName, c, m_mapNameNoPath), false);
			}
			#endregion
		}

		private void SetEffectiveDateInfo(Region region)
		{
			if(region.IsEffectiveDateTimeSet)
			{
				m_timezone = region.Timezone;
				m_dateInfoSet = true;
				if(region.EffectiveDateTime != SmallDateTime.MinValue)
				{
                    if (m_effectiveDateTime != SmallDateTime.MinValue)
                    {
                        AddWarning(String.Format("An effective datetime of {0} has already been declared for this map.  It is being overwritten with a value of {1} declared in region '{2}'.", m_effectiveDateTime, region.EffectiveDateTime, region.RegionId), false);
                    }
					m_effectiveDateTime = region.EffectiveDateTime;
				}
				else
				{
					if(region.EffectiveDate != SmallDateTime.MinValue)
					{
                        if (m_effectiveDate != SmallDateTime.MinValue)
                        {
                            AddWarning(String.Format("An effective date of {0} has already been declared for this map.  It is being overwritten with a value of {1} declared in region '{2}'.", m_effectiveDate, region.EffectiveDate, region.RegionId), false);
                        }
						m_effectiveDate = region.EffectiveDate;
					}
					if(region.EffectiveTime != SmallDateTime.MinValue)
					{
                        if (m_effectiveTime != SmallDateTime.MinValue)
                        {
                            AddWarning(String.Format("An effective time of {0} has already been declared for this map.  It is being overwritten with a value of {1} declared in region '{2}'.", m_effectiveTime, region.EffectiveTime, region.RegionId), false);
                        }
						m_effectiveTime = region.EffectiveTime;
					}
				}
			}
		}

		private void AddNewRegion(Region region)
		{
            if (region.RegionType == E_RegionType.RateOption)
            {
                AddNewRateOptionRegion(region);
            }
            else if (region.RegionType == E_RegionType.AdjustTable)
            {
                AddNewAdjustTableRegion(region);
            }
		}
		
		private void AddNewAdjustTableRegion(Region region)
		{
			m_numATRegionsFound++;

            if (m_RegionNames.Contains(region.RegionId))
            {
                throw new CBaseException(String.Format("Multiple regions have defined the same Region Id of '{0}' in ratesheet '{1}'.", region.RegionId, m_rsFileRep.RatesheetName), "");
            }
				
			m_RegionNames.Add(region.RegionId);
			
			RegionContainer container;
			if(m_AdjustTableRegions.ContainsKey(region.ObjectId))
			{
				AddWarning(String.Format("Region '{0}' in ratesheet '{1}' has declared an Object ID that already exists.  The object ID is '{2}'.", region.RegionId, m_ratesheetNameNoPath, region.ObjectId), false);
				container = (RegionContainer)m_AdjustTableRegions[region.ObjectId];
				container.AddRegion(region);
			}
			else
			{
				container = new RegionContainer(region, m_ratesheetNameNoPath, m_tabName, m_mapNameNoPath, m_mapTag);
				m_AdjustTableRegions.Add(region.ObjectId, container);
			}
		}

		private void AddNewRateOptionRegion(Region region)
		{
			m_numRORegionsFound++;

            if (m_RegionNames.Contains(region.RegionId))
            {
                throw new CBaseException(String.Format("Multiple regions have defined the same Region Id of '{0}' in ratesheet '{1}'.", region.RegionId, m_rsFileRep.RatesheetName), "");
            }
				
			m_RegionNames.Add(region.RegionId);

			//Pointer and non-pointer programs cannot share program Id, regardless if 1 is set as fragment or not
			if(region.IsPointerRegion)
			{
                if (m_RateOptionRegions.ContainsKey(region.ObjectId) || m_pointerRegionPrograms.Contains(region.ObjectId))
                {
                    throw new CBaseException(String.Format("Pointer Region '{0}' has declared an Object ID that has already been declared.  This is not allowed, as pointer regions cannot be fragmented with other regions.  The object ID is '{1}'.", region.RegionId, region.ObjectId), "");
                }
					
				m_pointerRegionPrograms.Add(region.ObjectId);
			}
			else
			{
                if (m_pointerRegionPrograms.Contains(region.ObjectId))
                {
                    throw new CBaseException(String.Format("Region '{0}' has declared an Object ID that has already been declared by a pointer region.  This is not allowed, as pointer regions cannot be fragmented with other regions.  The object ID is '{1}'.", region.RegionId, region.ObjectId), "");
                }
			}
			
			RegionContainer container;

			if(region.IsFragment == false && region.IsPointerRegion == false)
			{
				if(m_RateOptionRegions.ContainsKey(region.ObjectId))
				{
					RegionContainer container1 = (RegionContainer)m_RateOptionRegions[region.ObjectId];

					if(container1.ContainsOnlyPointerRegions)
					{
						container1.AddRegion(region);
						return;
					}
					else
					{
						SortedList regionsInContainer = container1.Regions;
						StringBuilder sb = new StringBuilder();
						foreach(string key in regionsInContainer.Keys)
						{
                            if (sb.Length == 0)
                            {
                                sb.Append(((Region)regionsInContainer[key]).RegionId);
                            }
                            else
                            {
                                sb.Append(", " + ((Region)regionsInContainer[key]).RegionId);
                            }
						}
						throw new CBaseException(String.Format("Region '{0}' has declared an Object ID that already exists, but it is not declared as a fragment.  The object ID is '{1}'.  Other regions that have declared this Object ID are: {2}", region.RegionId, region.ObjectId, sb.ToString()), "");
					}
				}

				container = new RegionContainer(region, m_ratesheetNameNoPath, m_tabName, m_mapNameNoPath, m_mapTag);
				m_RateOptionRegions.Add(region.ObjectId, container);
			}
			else
			{
				string objectId = (region.IsPointerRegion)?region.SrcLoProgramId:region.ObjectId;
				
				if(m_RateOptionRegions.ContainsKey(objectId))
				{
					container = (RegionContainer)m_RateOptionRegions[objectId];
					container.AddRegion(region);
				}
				else
				{
					container = new RegionContainer(region, m_ratesheetNameNoPath, m_tabName, m_mapNameNoPath, m_mapTag);
					m_RateOptionRegions.Add(objectId, container);
				}
			}
		}
		
		private void WriteAdjustTableOutput(string outputLocation)
		{
			try
			{
                if (m_AdjustTableRegions.Keys.Count == 0)
                {
                    return;
                }

				OutputWriter outputWriter = new RatesheetMapOutputWriter();
				int col = 1;
				int row = 1;
				int currentRow = 1;
				int numSectionsOutput = 0;
				
				foreach(string regionKey in m_AdjustTableRegions.Keys)
				{
					ArrayList localWarningList = new ArrayList();
					ArrayList localErrorList = new ArrayList();
					try
					{
						RegionContainer container = (RegionContainer)m_AdjustTableRegions[regionKey];
                        if (PrintRatesheetMapOutputIn1D == true)
                        {
                            numSectionsOutput += container.WriteDataToWorksheet1D(outputWriter, m_variables, ref row, ref localWarningList, ref localErrorList);
                        }
                        else
                        {
                            numSectionsOutput += container.WriteDataToWorksheet2D(outputWriter, m_mapTag, m_variables, row, ref currentRow, ref col, ref localWarningList, ref localErrorList);
                        }

						AddErrors(localErrorList, false);
						AddWarnings(localWarningList, false);
						col = 1;
                        if (PrintRatesheetMapOutputIn1D == false)
                        {
                            row = currentRow;
                        }
					}
					catch(CBaseException c)
					{
						AddError(String.Format("Error writing AdjustTable output for ratesheet '{0}', map '{3}', map tab '{1}' - {2}", m_ratesheetNameNoPath, m_tabName, c, m_mapNameNoPath), false);
						continue;
					}
					catch(Exception e)
					{
						AddError(String.Format("Error writing AdjustTable output for ratesheet '{0}', map '{3}', map tab '{1}' - {2}", m_ratesheetNameNoPath, m_tabName, e, m_mapNameNoPath), false);
						continue;
					}
				}

				if(numSectionsOutput > 0)
				{
					m_adjustTableOutputFiles.Add(outputLocation);
					outputWriter.SaveToCSV(outputLocation);
				}

				m_numATProgramsOutput += numSectionsOutput;
			}
			catch(CBaseException b)
			{
				throw b;
			}
			catch(System.Runtime.InteropServices.COMException c)
			{
				throw new CBaseException("Unable to open file for output (this usually means the file is currently open)", c);
			}
			catch(Exception e)
			{
				throw new CBaseException("Error writing output.", e);
			}
		}
		
		private void WriteRateOptionOutput(string outputLocation)
		{
			try
			{
                if (m_RateOptionRegions.Keys.Count == 0)
                {
                    return;
                }

				RatesheetMapOutputWriter outputWriter = new RatesheetMapOutputWriter();
				
				int col = 1;
				int row = 1;
				int currentRow = 1;
				int numSectionsOutput = 0;
				
				foreach(string regionKey in m_RateOptionRegions.Keys)
				{
					ArrayList localWarningList = new ArrayList();
					ArrayList localErrorList = new ArrayList();
					try
					{
						RegionContainer container = (RegionContainer)m_RateOptionRegions[regionKey];
                        if (PrintRatesheetMapOutputIn1D == true)
                        {
                            numSectionsOutput += container.WriteDataToWorksheet1D(outputWriter, m_variables, ref row, ref localWarningList, ref localErrorList);
                        }
                        else
                        {
                            numSectionsOutput += container.WriteDataToWorksheet2D(outputWriter, m_mapTag, m_variables, row, ref currentRow, ref col, ref localWarningList, ref localErrorList);
                        }
						
						AddErrors(localErrorList, false);
						AddWarnings(localWarningList, false);
						col = 1;

                        if (PrintRatesheetMapOutputIn1D == false)
                        {
                            row = currentRow;
                        }
					}
					catch(CBaseException c)
					{
						AddError(String.Format("Error writing RateOption output for ratesheet '{0}', map '{3}', map tab '{1}' - {2}", m_ratesheetNameNoPath, m_tabName, c, m_mapNameNoPath), false);
						continue;
					}
					catch(Exception e)
					{
						AddError(String.Format("Error writing RateOption output for ratesheet '{0}', map '{3}', map tab '{1}' - {2}", m_ratesheetNameNoPath, m_tabName, e, m_mapNameNoPath), false);
						continue;
					}
				}

				if(numSectionsOutput > 0)
				{
					m_rateOptionOutputFiles.Add(outputLocation);
					outputWriter.SaveToCSV(outputLocation);
				}
				
				m_numROProgramsOutput += numSectionsOutput;
			}
			catch(CBaseException b)
			{
				throw b;
			}
			catch(System.Runtime.InteropServices.COMException c)
			{
				throw new CBaseException("Unable to open file for output (this usually means the file is currently open)", c);
			}
			catch(Exception e)
			{
				throw new CBaseException("Error writing output.", e);
			}
		}

		public override string ToString()
		{
			StringBuilder rmProc = new StringBuilder();
			string ratesheetName = "";
            if (m_ratesheetNameNoPath == null || m_ratesheetNameNoPath.Trim().Equals(""))
            {
                ratesheetName = "(Unknown)";
            }
            else
            {
                ratesheetName = m_ratesheetNameNoPath;
            }

			rmProc.AppendFormat("<b>Executed Ratesheet Map Processor for Ratesheet: {0}</b><br><br>", ratesheetName);
			rmProc.AppendFormat("<b>REGIONS:</b> {0} <b>TOTAL EXECUTION TIME (SECONDS):</b> {1}<br><br>", m_numRegions ,m_executionSeconds);
			rmProc.AppendFormat("<b>EXECUTION TIME INPUT ONLY (SECONDS):</b> {0} <b>EXECUTION TIME OUTPUT ONLY (SECONDS):</b> {1}<br><br>", m_executionSecondsInput ,m_executionSecondsOutput);
			rmProc.AppendFormat("<b># RATE OPTION REGIONS PROCESSED:</b> {0} <b># ADJUSTMENT TABLE REGIONS PROCESSED:</b> {1}<br><br>", m_numRORegionsFound, m_numATRegionsFound);
			rmProc.AppendFormat("<b># RATE OPTION PROGRAMS OUTPUT:</b> {0} <b># ADJUSTMENT TABLE POLICIES OUTPUT:</b> {1}<br><br>", m_numROProgramsOutput, m_numATProgramsOutput);

            if (m_dateInfoSet)
            {
                rmProc.AppendFormat("<b>EFFECTIVE DATETIME (PT):</b> {0}<br><br>", this.EffectiveDateTimePT);
            }
            else
            {
                rmProc.AppendFormat("<b>EFFECTIVE DATETIME (PT):</b> {0}<br><br>", "Not declared");
            }

			rmProc.AppendFormat("<b>ERROR COUNT:</b> {0} <b>WARNING COUNT:</b> {1}<br><br>", m_errorList.Count , m_warningList.Count);

			if(m_errorList.Count > 0)
			{
				StringBuilder errors = new StringBuilder();
				foreach(string errorMsg in m_errorList)
				{
					errors.Append("<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- " + errorMsg);
				}
				rmProc.AppendFormat("<br><b>ERRORS: {0}</b><br>", errors.ToString());
			}

			if(m_warningList.Count > 0)
			{
				StringBuilder warnings = new StringBuilder();
				foreach(string warningMsg in m_warningList)
				{
					warnings.Append("<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- " + warningMsg);
				}
				rmProc.AppendFormat("<br><b>WARNINGS: {0}</b><br>", warnings.ToString());
			}
			
			if(m_getDetailedRegionInfo == true)
			{
				foreach(string key in m_RateOptionRegions.Keys)
				{
					RegionContainer container = (RegionContainer)m_RateOptionRegions[key];
					foreach(string ckey in container.Regions.Keys)
					{
						rmProc.Append("<br>" + container.Regions[ckey].ToString() + "<br>");
					}
				}

				foreach(string key in m_AdjustTableRegions.Keys)
				{
					RegionContainer container = (RegionContainer)m_AdjustTableRegions[key];
					foreach(string ckey in container.Regions.Keys)
					{
						rmProc.Append("<br>" + container.Regions[ckey].ToString() + "<br>");
					}
				}
			}

			return rmProc.ToString();
		}
	}

    public class OutputFile
    {
        #region Variables

        public bool HasError { get; set;}

        public string OutputFilename { get; private set;}

        public OutputFile(string outputFilename, bool hasError)
        {
            this.OutputFilename = outputFilename;
            this.HasError = hasError;
        }
        #endregion
    }
}