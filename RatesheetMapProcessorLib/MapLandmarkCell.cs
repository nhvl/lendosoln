using System;
using System.Collections;
using DataAccess;

namespace RatesheetMap
{
	public class MapLandmarkCell : LandmarkCell
	{
		public MapLandmarkCell(string regionId, CellRep cell, bool isAnchorLandmark, int anchorRow, int anchorCol) : base(regionId, E_LandmarkType.Map, cell, isAnchorLandmark, anchorRow, anchorCol){}
		public override void ValidateInput(CellRep cell)
		{
			if(!cell.ContainsAttribute("str"))
				throw new CBaseException(String.Format("Error parsing read tag in region '{0}' - Landmark tag does not contain an 'str' attribute.  The read tag is located at map row {1}, column {2} and the text = '{3}'", m_regionId, cell.Row, cell.ColumnLetter, cell.Value), "");
		}
		public override void ValidateInput(RatesheetCellRep cell){}
	}
}
