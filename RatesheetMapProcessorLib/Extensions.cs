﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RatesheetMap.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Converts single percent in a given string to decimal. Else return original string. 
        /// Note it will only convert strings in the format of 1.24%. If theres any extra characters it will not convert.
        /// After a trim % has to be the last character. 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ConvertPercent(this string str)
        {
            string val = str.Trim();
            if (val.Length != 0 && val.Length > 1 && val.IndexOf('%') == val.Length - 1)
            {
                val = val.Substring(0, val.Length - 1); //remove % 
                double p;
                if (double.TryParse(val, out p))
                {
                  return (p / 100.00).ToString();
                }
            }

            return str;
        }

    }
}