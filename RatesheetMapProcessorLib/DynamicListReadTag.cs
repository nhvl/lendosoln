using System;
using DataAccess;

namespace RatesheetMap
{
	public class DynamicListReadTag : ReadTag
	{
		public DynamicListReadTag(string regionId, CellRep readTag, LandmarkCell anchorLandmarkInMap, int dynamicRowOffset) : base(regionId, readTag, anchorLandmarkInMap)
		{
			m_row = readTag.Row + dynamicRowOffset;
			m_rowOffset = m_row - anchorLandmarkInMap.Row;
		}
	}
}
