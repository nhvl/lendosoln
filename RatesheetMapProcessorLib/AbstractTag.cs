using System;
using System.Diagnostics;
using DataAccess; 

namespace RatesheetMap
{

    [DebuggerDisplay("Tag: Type={m_tagType} Value={m_value}")] 
	public abstract class Tag
	{
		#region Variables
		protected E_TagType		m_tagType;
		protected string		m_regionId = ""; // Required: The region identifier, all elements in this region will identify this value
		protected int			m_row = 0;
		protected int			m_col = 0;
		protected string		m_value = "";

		public int Row
		{
			get { return m_row; }
		}

		public int Column
		{
			get { return m_col; }
		}

		public string ColumnLetter
		{
			get { return RatesheetMapCommon.GetColumnLetterFromNumber(m_col); }
		}

		public string Value
		{
			get { return m_value; }
		}

		public E_TagType TagType
		{
			get { return m_tagType; }
		}

		public string RegionId
		{
			get { return m_regionId; }
		}
		#endregion

		public Tag(E_TagType tagType, string regionId)
		{
			m_tagType = tagType;
			m_regionId = regionId;
		}

		public Tag(E_TagType tagType)
		{
			m_tagType = tagType;
			m_regionId = "";
		}

		public static E_TagType GetTagType(string inputValue)
		{
			if(inputValue.ToLower().Replace(" ", "").StartsWith("cm="))
			{
				return E_TagType.Comment;
			}

			int index = inputValue.IndexOf(":");
			if(index < 0)
			{
				throw new CBaseException(String.Format("Unable to get tag type from input value '{0}'.", inputValue), "");
			}
			else
			{
				string potentialTagType = "";
				try
				{
					potentialTagType = inputValue.Substring(0, index);
					switch(potentialTagType.Trim().ToLower())
					{
						case "op":
							return E_TagType.Operator;
						case "cm":
							return E_TagType.Comment;
						case "lm":
							return E_TagType.Landmark;
						case "read":
							return E_TagType.Read;
						case "region":
							return E_TagType.Region;
                        case "context": // Note - this can be removed and we can just use the newly created map tag now
							return E_TagType.Map; 
						case "map":
							return E_TagType.Map;
						case "dynamiclistbottom":
							return E_TagType.DynamicListBottom;
						case "ruleattribute":
							return E_TagType.RuleAttribute;
						default:
							throw new CBaseException("Invalid tag type: " + potentialTagType.Trim().ToLower(), "");
					}
				}
				catch(Exception e)
				{
					throw new CBaseException(String.Format("Invalid tag type '{0}'.  Input Value = '{1}'", potentialTagType, inputValue), e);
				}
			}
		}

		public static string GetAttribute(string attributeName, string valToSearch)
		{
			valToSearch = valToSearch.Trim().ToLower().Replace("\n", "");
			if(attributeName == null || valToSearch == null)
				return "";

			int index = valToSearch.IndexOf(attributeName.Trim().ToLower());
			if(index < 0 )
				return "";

			int indexOfEquals = valToSearch.IndexOf('=', index);
			if(indexOfEquals < 0)
			{
				throw new CBaseException(String.Format("Improperly formed attribute '{0}'.  No value has been set to this attribute (there is no '=' found after the attribute name).  The value of the map cell containing this attribute is: '{1}.'", attributeName, valToSearch), "");
			}
			int indexEndOfToken = valToSearch.IndexOf(';', indexOfEquals);
			
			if(indexEndOfToken < 0)
				indexEndOfToken = valToSearch.Length;

			int lengthToGet = indexEndOfToken - indexOfEquals;
			string attributeVal = "";
			try
			{
				attributeVal = valToSearch.Substring(indexOfEquals+1, lengthToGet-1);
			}
			catch(Exception e)
			{
				throw new CBaseException(String.Format("Unable to get attribute '{0} in '{1}.'", attributeName, valToSearch), e);
			}

			return attributeVal.Trim().ToLower();
		}

		public static string GetAttribute(string attributeName, RatesheetCellRep cell)
		{
			try
			{
				if(cell == null || cell.Value == null)
					return "";
				return GetAttribute(attributeName, cell.Value);
			}
			catch(CBaseException c)
			{
				throw new CBaseException(String.Format("Unable to get attribute from tag located at map row {0}, column {1}.  DETAILS: {2}", cell.Row, RatesheetMapCommon.GetColumnLetterFromNumber(cell.Column), c.UserMessage), c.DeveloperMessage + " - " + c.StackTrace);
			}
		}
	}
}