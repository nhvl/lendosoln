using System;
using System.Collections;
using DataAccess;
using System.Text;
using System.IO;
using System.Globalization;

namespace RatesheetMap
{
	public interface OutputWriter
	{
		void WriteCell(int row, int column, string data);
		void SaveToCSV(string filename);
	}
	
	public class RatesheetMapOutputWriter : OutputWriter
	{
		private ArrayList m_rows;
		public RatesheetMapOutputWriter()
		{
			m_rows = new ArrayList();
		}

		public void WriteCell(int row, int column, string data)
		{
			if(data == null)
				data = "";
			try
			{
				if(data.ToLower().IndexOf("e") >= 0 && RatesheetMapCommon.IsNumeric(data)) // OPM 20532
				{
					NumberFormatInfo nfi = new NumberFormatInfo();
					nfi.NumberDecimalDigits = 15;
					data = double.Parse(data).ToString("F", nfi);
				}

				if(row < 0)
					row = 0;
				if(column < 0)
					column = 0;

				if(data.IndexOf(",") >= 0)
				{
					data = "\"" + data + "\"";
				}
				
				//TODO - remove the necessity for this - this is here because excel starts indices at 1 instead of 0
				// We are now using CPX files so we can migrate away from this for code clarity
                if(row > 0)
					--row;
				if(column > 0)
					--column;

				if(m_rows.Count == row)
				{
					ArrayList newRow = new ArrayList();
					m_rows.Add(newRow);
					AddDataToColumn(newRow, column, data);
				}
				else if(m_rows.Count < row)
				{
					int lastRowIndex = m_rows.Count - 1;
					int numCellsToInsert = row - lastRowIndex;

					for(int i = 0; i < numCellsToInsert; ++i)
					{
						m_rows.Add(new ArrayList());
					}

					ArrayList rowList = (ArrayList)m_rows[row];
					AddDataToColumn(rowList, column, data);
				}
				else
				{
					ArrayList rowList = (ArrayList)m_rows[row];
					AddDataToColumn(rowList, column, data);
				}
			}
			catch(Exception e)
			{
				Tools.LogError("Error writing cells in output writer: " + e.ToString() + ", Writing to row: " + row + ", column: " + RatesheetMapCommon.GetColumnLetterFromNumber(column) + ", data: " + data + ", mRowsCount: " + m_rows.Count);
				throw e;
			}
		}

		private void AddDataToColumn(ArrayList rowList, int column, string data)
		{
			if(rowList.Count == column)
			{
				rowList.Add(data);
			}	
			if(rowList.Count < column)
			{
				int lastColumnIndex = rowList.Count - 1;
				int numCellsToInsert = column - lastColumnIndex;
	
				for(int i = 0; i < numCellsToInsert; ++i)
				{
					if(RatesheetMapCommon.IsNumeric(data) && column > 1) // 4/7/08 db - TEMPORARY - This can be removed once OPM 20473 is complete.
						rowList.Add("N/A");
					else
						rowList.Add("");
				}

				rowList[column] = data;
			}
			else
			{
				rowList[column] = data;
			}
		}

		public void SaveToCSV(string filename)
		{
			try
			{
				StringBuilder csvTable = new StringBuilder();

				foreach(ArrayList column in m_rows)
				{
					StringBuilder sRow = new StringBuilder();
				
					foreach(string data in column)
					{
						sRow.AppendFormat("{0},", data);
					}
				
					sRow.Append(Environment.NewLine);
					csvTable.Append(sRow);
				}

				TextWriter tw = new StreamWriter(filename, false);
				tw.Write(csvTable.ToString());
				tw.Close();
			}
			catch(IOException io)
			{
				throw new CBaseException("Unable to open file for output (this usually means the file is currently open).  The filename is: " + filename, io);
			}
			catch(Exception e)
			{
				Tools.LogError("Error writing csv: " + e.ToString());
				throw e;
			}
		}
	}
}