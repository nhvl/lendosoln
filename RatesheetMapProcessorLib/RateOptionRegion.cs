using System;
using System.Collections;
using DataAccess;

namespace RatesheetMap
{
	public class RateOptionRegion : Region
	{
		public RateOptionRegion(CellRep region, WorksheetFileRep mapRep, RatesheetWorksheet ratesheet, E_MapContext mapContext) : base(region, mapRep, ratesheet, E_RegionType.RateOption, mapContext){}
		public override void ValidateInput(CellRep region)
		{
			if(m_regionId.Trim().Equals(""))
				throw new CBaseException(String.Format("Error creating RateOption region: Region ID cannot be blank.  Region definition is located at map row {0}, column {1}, text = '{2}'.", region.Row, region.ColumnLetter, region.Value), "");
			
			if(m_loObjType.Equals(E_LoObjType.LoPolicyId))
				throw new CBaseException(String.Format("Error in region '{0}': Rate Option Regions cannot have an LoPolicyId", m_regionId), "");
			
			if(m_loObjType.Equals(E_LoObjType.LoRatesheetId) && !m_mapContext.Equals(E_MapContext.Lender))
				throw new CBaseException(String.Format("Error in region '{0}': This region has declared an LoRatesheetId, but has not been declared as Lender Context in the map tag.", m_regionId), "");
				
			if(m_loObjType.Equals(E_LoObjType.None) && (m_optional == false))
				throw new CBaseException(String.Format("Error in region '{0}': Rate Option Regions must define an LoProgramId or LoRatesheetId unless the 'Optional' attribute is set to true", m_regionId), "");
		
			if(!m_timezone.Equals(E_Timezone.P))
				throw new CBaseException(String.Format("Error in region '{0}': Rate Option Regions cannot define a Timezone - you must use the RSEffective region type to define the timezone.", m_regionId), "");
		
			if(m_loObjType.Equals(E_LoObjType.LoProgramId) && !RatesheetMapCommon.IsGuid(m_loObjId))
				throw new CBaseException(String.Format("Error in region '{0}': Program Id '{1}' is not a Guid value.", m_regionId, m_loObjId), "");
		
			if(m_srcLoProgramId != null)
			{
				if(m_srcLoProgramId.Equals(""))
					throw new CBaseException(String.Format("Error in region '{0}': SrcLoProgramId must not contain a blank value if it is defined.", m_regionId), "");
				if(m_fragment == true)
					throw new CBaseException(String.Format("Error in region '{0}': SrcLoProgramId cannot be used in a region that is declared as a fragment.", m_regionId), "");
			}
		}

		public override void ProcessOperators()
		{
			if(this.IsFound == false)
				return;

			if(this.IsPointerRegion)
				return;
			
			ArrayList operatorList = m_mapRep.GetAllCellsByType(m_regionId, E_TagType.Operator);
			m_operators = new Operators(operatorList, RegionId, E_RegionType.RateOption, m_anchorLandmarkInMap);
		}
	}
}
