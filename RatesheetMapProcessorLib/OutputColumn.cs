using System;
using System.Collections;
using System.Text;
using DataAccess;
using System.Text.RegularExpressions;

namespace RatesheetMap
{
	public class OutputColumn
	{
		#region Variables
		private string		m_regionId = "";
		//TODO - make the filters a hashtable instead - just for code clarity
		private ArrayList	m_column;
		private ArrayList	m_filters;
		private ArrayList	m_dynamicListRowMathOperators;
		private GroupTag	m_lkFilter = new GroupTag("", E_OperatorAttributeType.LK);
		private GroupTag	m_pppFilter = new GroupTag("", E_OperatorAttributeType.PPP);
		private GroupTag	m_ioFilter = new GroupTag("", E_OperatorAttributeType.IO);
		private E_ReadLocations m_readLocation;
		private bool		m_isDynamicList;
		
		public string RegionId
		{
			get { return m_regionId; }
		}
		public E_ReadLocations Destination
		{
			get { return m_readLocation; }
		}
		public ArrayList Cells
		{
			get{ return m_column; }
		}
		public bool IsDynamicList
		{
			get { return m_isDynamicList; }
			set { m_isDynamicList = value; }
		}
		public GroupTag LkFilter
		{
			get { return m_lkFilter; }
		}
		public GroupTag PPPFilter
		{
			get { return m_pppFilter; }
		}
		public GroupTag IOFilter
		{
			get { return m_ioFilter; }
		}
		public bool HasMoreUnusedGroupFilters
		{
			get { return (GetHighestUnusedGroupTag() != null); }
		}	

		public ArrayList GroupFilters
		{
			get { return m_filters; }
			set 
			{
				m_filters = value;
				foreach(GroupTag filter in m_filters)
				{
					switch(filter.AttributeType)
					{
						case E_OperatorAttributeType.LK:
							m_lkFilter = filter;
							break;
						case E_OperatorAttributeType.PPP:
							m_pppFilter = filter;
							break;
						case E_OperatorAttributeType.IO:
							m_ioFilter = filter;
							break;
						default:
							throw new CBaseException(String.Format("Invalid Group Tag 'type' attribute (example: 'Lk') in region '{0}'.  The invalid type is '{1}'.", m_regionId, filter.AttributeType), "");
					}
				}
			}
		}
		#endregion

		public OutputColumn(string regionId)
		{
			if((regionId == null) || (regionId.Equals("")))
				throw new CBaseException("Unable to create output column.", "OutputColumn must contain a valid non-empty Region Id value.");
			m_regionId = regionId;
			m_column = new ArrayList();
			m_filters = new ArrayList();
			m_dynamicListRowMathOperators = new ArrayList();
			m_isDynamicList = false;
		}

		public void AddDynamicListRowMathOperators(ArrayList mathOperators)
		{
			m_dynamicListRowMathOperators.AddRange(mathOperators);
		}

		public void SetAllGroupTagsAsUnused()
		{
			int numElementsInEnum = Enum.GetValues(typeof(E_OperatorAttributeType)).Length;
			E_OperatorAttributeType group;
			for(int i = 0; i < numElementsInEnum; ++i)
			{
				group = (E_OperatorAttributeType)Enum.GetValues(typeof(E_OperatorAttributeType)).GetValue(i);
				GroupTag groupTag = GetGroupTag(group);
				groupTag.AlreadyUsed = false;
			}
		}

		public GroupTag GetHighestUnusedGroupTag()
		{
			int numElementsInEnum = Enum.GetValues(typeof(E_OperatorAttributeType)).Length;
			E_OperatorAttributeType group;
			for(int i = 0; i < numElementsInEnum; ++i)
			{
				group = (E_OperatorAttributeType)Enum.GetValues(typeof(E_OperatorAttributeType)).GetValue(i);
				GroupTag groupTag = GetGroupTag(group);
				if((groupTag.RangeFrom >= 0) && (groupTag.AlreadyUsed == false))
					return groupTag;
			}
			return null;
		}

		public GroupTag GetGroupTag(E_OperatorAttributeType groupType)
		{
			switch(groupType)
			{
				case E_OperatorAttributeType.IO:
					return m_ioFilter;
				case E_OperatorAttributeType.PPP:
					return m_pppFilter;
				case E_OperatorAttributeType.LK:
					return m_lkFilter;
				default:
					throw new CBaseException(String.Format("Unable to get group tag in region '{0}' - Invalid group tag type '{1}'.", m_regionId, groupType), "");
			}
		}

		public void AddOutputCell(DataCell oCell)
		{
			if(m_column.Count == 0)
				m_readLocation = oCell.Destination;
			m_column.Add(oCell);
		}

		public override string ToString()
		{
			string toReturn = "Filters: ";
			foreach(GroupTag filter in m_filters)
			{
				toReturn += filter.ToString() + ", ";
			}
			toReturn += "<br>";
			foreach(DataCell oCell in m_column)
			{
				toReturn += oCell.ToString() + "<br>";
			}

			return toReturn;
		}
	}
}
