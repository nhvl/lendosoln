using System;
using System.Collections;
using DataAccess;
using System.Diagnostics; 

namespace RatesheetMap
{
    [DebuggerDisplay("Operators")]
	public class Operators
	{
		#region Variables
		private Hashtable	m_RowOperators = new Hashtable();
		private Hashtable	m_ColumnOperators = new Hashtable();
		private ArrayList	m_GlobalMathOperators = new ArrayList();
		private string		m_regionId = "";

		public string RegionId
		{
			get{ return m_regionId; }
		}

		public int MaxColumnGroups
		{
			get
			{
				int maxColumnGroups = 0;
				int tempNumColumnGroups = 0;
				foreach(int columnOffset in m_ColumnOperators.Keys)
				{
					foreach(AbstractOperator op in (ArrayList)m_ColumnOperators[columnOffset])
					{
						if(op.OperatorTagType == E_OperatorTagType.Group)
							tempNumColumnGroups++;	
					}

					if(tempNumColumnGroups > maxColumnGroups)
						maxColumnGroups = tempNumColumnGroups;
					
					tempNumColumnGroups = 0;
				}
				return maxColumnGroups;
			}
		}
		
		#endregion
		
		public Operators(ArrayList operatorList, string regionId, E_RegionType regionType, LandmarkCell anchorLandmarkInMap)
		{
			m_regionId = regionId;
			foreach(CellRep operatorCell in operatorList)
			{
				AbstractOperator op = AbstractOperator.CreateOperatorTag(regionId, regionType, operatorCell, anchorLandmarkInMap);
				int rowOffset = op.RowOffset;
				int colOffset = op.ColumnOffset;

				//Add the operator to the row operator list
				if(!m_RowOperators.ContainsKey(rowOffset))
				{
					ArrayList rowList = new ArrayList();
					rowList.Add(op);
					m_RowOperators.Add(rowOffset, rowList); 
				}
				else
					((ArrayList)m_RowOperators[rowOffset]).Add(op);

				//Add the operator to the column operator list
				if(!m_ColumnOperators.ContainsKey(colOffset))
				{
					ArrayList colList = new ArrayList();
					colList.Add(op);
					m_ColumnOperators.Add(colOffset, colList); 
				}
				else
					((ArrayList)m_ColumnOperators[colOffset]).Add(op);
			}
		}	
		

		public void AddGlobalMathOperators(ArrayList mathOperators)
		{
			if(m_GlobalMathOperators.Count == 0)
				m_GlobalMathOperators.AddRange(mathOperators);
		}

		public ArrayList GetOperatorsForRowOffset(int rowOffset)
		{
			if(m_RowOperators.ContainsKey(rowOffset))
				return (ArrayList)m_RowOperators[rowOffset];
			else
				return new ArrayList();
		}

		public ArrayList GetOperatorsForRowOffset(int rowOffset, E_OperatorTagType operatorTagType)
		{
			if(m_RowOperators.ContainsKey(rowOffset))
			{
				ArrayList matchingOperators = new ArrayList();
				foreach(AbstractOperator op in (ArrayList)m_RowOperators[rowOffset])
				{
					if(op.OperatorTagType == operatorTagType)
						matchingOperators.Add(op);
				}
				return matchingOperators;
			}
			else
				return new ArrayList();
		}

		public ArrayList GetOperatorsForColumnOffset(int columnOffset)
		{
			ArrayList returnOps = new ArrayList();
			returnOps.AddRange(m_GlobalMathOperators);
			if(m_ColumnOperators.ContainsKey(columnOffset))
				returnOps.AddRange((ArrayList)m_ColumnOperators[columnOffset]);
			return returnOps;
		}

		public ArrayList GetOperatorsForColumnOffset(int columnOffset, E_OperatorTagType operatorTagType)
		{
			ArrayList matchingOperators = new ArrayList();
			if(operatorTagType == E_OperatorTagType.Math)
				matchingOperators.AddRange(m_GlobalMathOperators);
			if(m_ColumnOperators.ContainsKey(columnOffset))
			{
				foreach(AbstractOperator op in (ArrayList)m_ColumnOperators[columnOffset])
				{
					if(op.OperatorTagType == operatorTagType)
						matchingOperators.Add(op);
				}
			}

			return matchingOperators;
		}

		public override string ToString()
		{
			string toReturn = RatesheetMapCommon.GetTabString(1) + "<b>Row Operators:</b><br>";
			foreach(int key in m_RowOperators.Keys)
			{
				toReturn += RatesheetMapCommon.GetTabString(2) + String.Format("<b>Row Offset: {0}</b><br>", key);
				foreach(AbstractOperator op in GetOperatorsForRowOffset(key))
				{
					toReturn += op.ToString() + "<br>";
				}
			}

			toReturn += RatesheetMapCommon.GetTabString(1) + "<b>Column Operators:</b><br>";
			foreach(int key in m_ColumnOperators.Keys)
			{
				toReturn += RatesheetMapCommon.GetTabString(2) + String.Format("<b>Column Offset: {0}</b><br>", key);
				foreach(AbstractOperator op in GetOperatorsForColumnOffset(key))
				{
					toReturn += op.ToString() + "<br>";
				}
			}
			return toReturn;
		}
	}
}