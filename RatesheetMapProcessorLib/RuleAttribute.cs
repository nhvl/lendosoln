using System;
using System.Collections;
using DataAccess;

namespace RatesheetMap
{
	//A rule attribute is basically a type of operator for adjustment table regions
	//TODO - make sure HIDES are always put first no matter where they come from.  Without this, some things might get used when they shouldn't.
    // Note that the SAE's are supposed to do this anyways, so this is a convenience for them if we add it.
	public class RuleAttribute : AbstractOperator
	{
		#region Variables
		private string					m_strAttribute = ""; //required
		private E_RuleAttributeType		m_ruleAttributeType; //required

		public string StrAttribute
		{
			get { return m_strAttribute; }
		}

		public E_RuleAttributeType Type
		{
			get { return m_ruleAttributeType; }
		}
		#endregion
		
		public RuleAttribute(string regionId, CellRep ruleAttributeCell, LandmarkCell anchorLandmarkInMap) : base(regionId, ruleAttributeCell, anchorLandmarkInMap)
		{
			if(m_regionId.Equals(""))
				throw new CBaseException(String.Format("Attribute 'rid' is required for RuleAttribute tag in region '{0}' at map row {1}, column {2}.  The current defintion of the invalid RuleAttribute tag is '{3}'.", m_regionId, ruleAttributeCell.Row, ruleAttributeCell.ColumnLetter, ruleAttributeCell.Value), "");
			try
			{
				m_strAttribute = ruleAttributeCell.GetAttribute("str");
				if(m_strAttribute.Equals(""))
					throw new CBaseException(String.Format("Attribute 'str' is required for RuleAttribute tag in region '{0}' at map row {1}, column {2}.  The current defintion of the invalid RuleAttribute tag is '{3}'.", m_regionId, ruleAttributeCell.Row, ruleAttributeCell.ColumnLetter, ruleAttributeCell.Value), "");
			
				m_ruleAttributeType = (E_RuleAttributeType) Enum.Parse(typeof(E_RuleAttributeType), ruleAttributeCell.GetAttribute("type"), true); 
			}
			catch(CBaseException c)
			{
				throw new CBaseException(String.Format("Unable to parse RuleAttribute attribute in region '{0}'.  The location of the cell being parsed is map row {1}, column {2}.  DETAILS: {3}", regionId, ruleAttributeCell.Row, ruleAttributeCell.ColumnLetter, c.UserMessage), "");
			}
		}

		public static string Combine(RuleAttribute rule1, RuleAttribute rule2)
		{
			if(rule1.Type != rule2.Type)
				throw new CBaseException(String.Format("Unable to combine rule attributes (adjustment table conditions/descriptions) - The rule attributes cannot be of different types.  The type of RuleAttribute1 is '{0}' and the type of RuleAttribute2 is '{1}'.", rule1.Type, rule2.Type), "");
			
			if(rule1 == null && rule2 == null)
				return "";
			else if(rule1 == null)
				return rule2.StrAttribute;
			else if(rule2 == null)
				return rule1.StrAttribute;
			else
			{
				if(rule1.Type == E_RuleAttributeType.Condition)
					return Combine(rule1.StrAttribute, rule2.StrAttribute);
				else if(rule1.Type == E_RuleAttributeType.Description)
					return CommaCombine(rule1.StrAttribute, rule2.StrAttribute);
				else
					throw new CBaseException(String.Format("Unable to combine rule attributes (adjust table conditions/descriptions) - The RuleAttribute type '{0}' is invalid.", rule1.Type), "");
			}
		}

		public static string Combine(string rule1, string rule2)
		{
			if((rule1 == null || rule1.Equals("")) && (rule2 == null || rule2.Equals("")))
				return "";
			else if(rule1 == null || rule1.Equals("" ))
				return rule2;
			else if(rule2 == null || rule2.Equals(""))
				return rule1;
			else
				return String.Format("( {0} ) AND ( {1} )", rule1, rule2);
		}

		public static string CommaCombine(string rule1, string rule2)
		{
			if((rule1 == null || rule1.Equals("")) && (rule2 == null || rule2.Equals("")))
				return "";
			else if(rule1 == null || rule1.Equals("" ))
				return rule2;
			else if(rule2 == null || rule2.Equals(""))
				return rule1;
			else
				return String.Format("{0}, {1}", rule1, rule2);
		}

		public override AbstractOperator GetCopy()
		{
			return new RuleAttribute(this.RegionId, this.StrAttribute, this.Type, this.RowOffset, this.ColumnOffset);
		}

		public RuleAttribute(string regionId, string strAttribute, E_RuleAttributeType ruleAttributeType, int rowOffset, int colOffset): base(regionId, rowOffset, colOffset)
		{
			if(m_regionId.Equals(""))
				throw new CBaseException(String.Format("Attribute 'rid' is required for RuleAttribute tag in region '{0}'.", m_regionId), "");
		}

		public override E_OperatorTagType OperatorTagType
		{
			get { return E_OperatorTagType.RuleAttribute; }
		}

		public override string ToString()
		{
			return RatesheetMapCommon.GetTabString(3) + String.Format("Op Type: RuleAttribute");
		}
	}
}
