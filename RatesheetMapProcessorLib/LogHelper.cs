/// Author: David Dao

using System;
using DataAccess;

namespace RatesheetMap
{
	public class LogHelper
	{
		public static void LogWarning(string msg) 
		{
            Tools.LogWarning(msg);
		}

        public static void Log(string msg) 
        {
            Tools.LogInfo(msg);
        }
        public static void LogError(string errMsg) 
        {
            Tools.LogError(errMsg);
        }
        public static void LogError(Exception exc) 
        {
            Tools.LogError(exc);
        }
	}
}