using System;
using System.Collections;
using System.Collections.Generic;
using DataAccess;
using System.Text;
using System.IO;
using System.Reflection;

using System.Diagnostics;
using ExcelLibraryClient;
using LendersOffice.Common; 

namespace RatesheetMap
{
    /// <summary>
    /// Keeps track of all the worksheets for each of the given files.It can also convert xls to cpx. A representation for a XLS map file. 
    /// Someone feeds it a list of files to process. The files are cpx and will contain the worsheet name ( from excel as the first line ). It will then 
    /// go ahead and read all of it parsing one line at a time. 
    /// </summary>
    [DebuggerDisplay("InputFileRep: MapName:{m_mapName}")]
	public class InputFileRep : IDisposable
	{
		#region Variables
		private Dictionary<string, WorksheetFileRep> m_worksheets;
		private bool		m_ignoringLines = false;
		private string		m_mapName = "";

		public ArrayList WorksheetNames
		{
			get { return new ArrayList(m_worksheets.Keys); }
		}
		public int NumWorksheets
		{
			get { return m_worksheets.Count; }
		}
		public string Name
		{
			get { return m_mapName; }
		}
		#endregion
		
		public InputFileRep()
		{
			m_worksheets = new Dictionary<string, WorksheetFileRep>();
		}

        /// <summary>
        /// Retrieves the given worksheet. 
        /// </summary>
        /// <param name="worksheetName"></param>
        /// <returns>Null if it doesnt exist.</returns>
        public WorksheetFileRep GetWorksheet(string worksheetName)
        {
            if (m_worksheets.ContainsKey(worksheetName.Trim().ToLower()))
                return (WorksheetFileRep)m_worksheets[worksheetName.Trim().ToLower()];
            else
                return null;
		}

        
        /// <summary>
        /// Given a file name it creates a representation. 
        /// </summary>
        /// <param name="filename">The full path to the file.</param>
        /// <returns>True if the operation goes smoothly, false otherwise. </returns>
        /// <exception cref="CBaseException"> If the read fails for any reason. Or the representation cannot be completed. </exception>
		public bool CreateRep(string filename)
		{
			bool success = false;
			string inputFileText = "";
			m_mapName =  System.IO.Path.GetFileName(filename);
			
			try
			{
				using(StreamReader sR = new StreamReader( filename ))
				{
					inputFileText = sR.ReadToEnd();
				}
			}
			catch(CBaseException c)
			{
				throw c;
			}
			catch(Exception e)
			{
				throw new CBaseException("Unable to read from input file", e);
			}
			
			try
			{
				WorksheetFileRep worksheet = null;
				foreach( String sLine in inputFileText.Split( '\n' ) )
				{
					ProcessLine(sLine, ref worksheet);
				}
				inputFileText = null;
			}
			catch(CBaseException c)
			{
				throw c;
			}
			catch(Exception e)
			{
				throw new CBaseException("Unable to create representation from input file", e);
			}

			return success;
		}
        /// <summary>
        /// Parses the line and adds it to worksheet. 
        /// </summary>
        /// <param name="line">The line to parse.</param>
        /// <param name="worksheet">The worksheet object the line belongs to.</param>
        /// <exception cref="CBaseException"> THrows when the cpx file is not created correctly (ig worksheet name is missing) </exception>
		private void ProcessLine(string line, ref WorksheetFileRep worksheet)
		{
			if(line.Trim().Equals(""))
				return;

			line = line.Substring(0, line.Length-1).ToLower(); //Strip out last end of line char
			
			if(line.StartsWith("w:")) //New Worksheet
			{
				line = line.Trim().Remove(0,2);
				if(!line.StartsWith("m-"))
				{
					m_ignoringLines = true;
				}
				else
				{
					m_ignoringLines = false;
					worksheet = new WorksheetFileRep(line, m_mapName);
					m_worksheets.Add(line, worksheet);
				}
			}
			else
			{
				if(worksheet == null && !m_ignoringLines)
				{
					throw new CBaseException("Unable to process input CPX file.", "CPX file not created correctly - Does not start with a worksheet name.");
				}
				if(!m_ignoringLines)
				{
					worksheet.AddCell(line); 
				}
			}
		}


		public static string ConvertToCPX(string filename)
		{
			string outfilePath = System.IO.Path.GetDirectoryName(filename);
			string outfileName = System.IO.Path.Combine(outfilePath, System.IO.Path.GetFileNameWithoutExtension(filename) + ".CPX");
            using (PerformanceStopwatch.Start("ExcelLibrary.ConvertToCompactFormat"))
            {
                ExcelLibrary.ConvertToCompactFormat(filename, outfileName, "^m-"); // OPM 20115 - Only compress worksheet start with "m-"
            }

			return outfileName;
		}


	
		void IDisposable.Dispose()
		{
			try
			{
				foreach(string key in m_worksheets.Keys)
				{
					WorksheetFileRep ws = (WorksheetFileRep)m_worksheets[key];
					((IDisposable)ws).Dispose() ;
				}
			}
			finally
			{
				m_worksheets = null;
			}
		}
	}

    /// <summary>
    /// Maintains list of regions in a worksheet. It also maintains  the list of map tags.  Tags that are not used and comment tags. 
    /// </summary>
    [DebuggerDisplay("WorksheetFileRep: Name={m_worksheetName} MapName={m_mapName}")]
	public class WorksheetFileRep : IDisposable
	{
		#region Variables
		private string m_worksheetName;
		private string m_mapName;
		private Hashtable m_regionReps;
		private ArrayList m_mapTags;
		private ArrayList m_regionTags;
		private ArrayList m_miscTags;
		private ArrayList m_commentTags;

		public ArrayList UnusedTags
		{
			get { return m_miscTags; }
		}

		public string Name
		{
			get { return m_worksheetName; }
		}
		#endregion
		/// <summary>
		/// Instantiates a new WorksheetFileRep object. 
		/// </summary>
		/// <param name="wsName">worksheet name</param>
		/// <param name="mapName">map name</param>
		public WorksheetFileRep(string wsName, string mapName)
		{
			m_worksheetName = wsName;
			m_mapName = mapName;
			m_regionReps = new Hashtable();
			m_mapTags = new ArrayList();
			m_regionTags = new ArrayList();
			m_miscTags = new ArrayList();
			m_commentTags = new ArrayList();
		}

        /// <summary>
        /// Retrieves the next cell in the specified region that belongs to the same column and is 
        /// the given tag type. In excel this would be the next nonempty cell with given tag type. 
        /// </summary>
        /// <param name="regionId"></param>
        /// <param name="topCell"></param>
        /// <param name="tagType"></param>
        /// <returns>Null if it doesnt exist</returns>
		public CellRep GetNextCellInColumnByType(string regionId, CellRep topCell, E_TagType tagType)
		{
			if(m_regionReps.ContainsKey(regionId))
			{
				return ((RegionRep)m_regionReps[regionId]).GetNextCellInColumnByType(topCell, tagType);
			}
			else
			{
				return null;
			}
		}
        /// <summary>
        /// Retrieves the cell at the given location in the region.
        /// </summary>
        /// <param name="regionId"></param>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns>Null if it doesnt exist.</returns>
		public CellRep GetCell(string regionId, int row, int column)
		{
			if(m_regionReps.ContainsKey(regionId))
			{
				return ((RegionRep)m_regionReps[regionId]).GetCell(row, column);
			}
			else
			{
				return null;
			}
		}
        /// <summary>
        /// Retrieves all the cell with the given type in the specified region. 
        /// </summary>
        /// <param name="regionId">The id of the given region.</param>
        /// <param name="tagType"></param>
        /// <returns>Empty arraylist if there are none.</returns>
		public ArrayList GetAllCellsByType(string regionId, E_TagType tagType)
		{
			if(m_regionReps.ContainsKey(regionId))
			{
				return ((RegionRep)m_regionReps[regionId]).GetAllCellsByType(tagType);
			}
			else
			{
				return new ArrayList();
			}
		}
        /// <summary>
        /// Retrieves column list of list of cells that belong to the given region and are
        /// the specified tagtype. 
        /// </summary>
        /// <param name="regionId"></param>
        /// <param name="tagType"></param>
        /// <returns></returns>
		public ArrayList GetColumnsByType(string regionId, E_TagType tagType)
		{
			if(m_regionReps.ContainsKey(regionId))
			{
				return ((RegionRep)m_regionReps[regionId]).GetColumnsByType(tagType);
			}
			else
			{
				return new ArrayList();
			}
		}
        /// <summary>
        /// REtrieves arraylist containing all the map or region tags. 
        /// </summary>
        /// <param name="tagType"></param>
        /// <returns>Empty array list if there are none</returns>
        /// <exception cref="CBaseExcetouin"> If something other than map or region are retrived. </exception>
		public ArrayList GetAllCellsByType(E_TagType tagType)
		{
			if(tagType == E_TagType.Map)
				return m_mapTags;
			else if(tagType == E_TagType.Region)
				return m_regionTags;
			else
				throw new CBaseException(String.Format("Unable to retrieve cells of type '{0}'.", tagType),"Bug: Call to GetAllCellsByType(tagType) can only return cells that are not contained in region.");
		}
        
		//TODO - list cells without matching region ID declarations  - av possible improvements regex capture? should help minimize code will probably be slower 
		// not urgent, just for code improvement
        /// <summary>
		/// Takes a line and turns it in a cell rep. 
        /// It adds them to the appropiate regions or where they belong if they dotn belong to a specific region. 
		/// </summary>
        /// <param name="line">Line format : {row},{col},{tagtype}:{attributes}.</param>
        /// <exception cref="CBaseException"></exception>
        public void AddCell(string line)
		{
			if(line == null || line.Trim().Equals(""))
				return;

			int row = -1;
			int col = -1;
			try
			{
				int index1 = line.IndexOf(",");
				if(index1 >= 0)
				{
					string rowStr = line.Substring(0, index1);
					row = int.Parse(rowStr);
				}

				int index2 = line.IndexOf(",", index1+1);

				if(index2 >= 0)
				{
					int startAt = index1 + 1;
					string colStr = line.Substring(startAt, index2 - startAt);
					col = int.Parse(colStr);
				}
		
				CellRep cell = new CellRep(row, col, line.Substring(index2 + 1));
		
				string rid = cell.GetAttribute("rid");
		
				if(cell.TagType == E_TagType.Map)
				{
					m_mapTags.Add(cell);
				}
				else if(cell.TagType == E_TagType.Comment)
				{
					m_commentTags.Add(cell);
				}
				else if(rid == null || rid.Equals(""))
				{
					m_miscTags.Add(cell);
				}
				else if(cell.TagType == E_TagType.Region)
				{
					m_regionTags.Add(cell);
					//The region object itself might have already been added if a cell from that region was encountered before the region tag declaration
					if(!m_regionReps.ContainsKey(rid))
					{
						RegionRep rRep = new RegionRep(rid);
						m_regionReps.Add(rid, rRep);
					}
				}
				else //lm read tags blah blah these are region specific 
				{
					if(m_regionReps.ContainsKey(rid))
					{
						((RegionRep)m_regionReps[rid]).AddCell(cell);
					}
					else
					{
						RegionRep rRep = new RegionRep(rid);
						rRep.AddCell(cell);
						m_regionReps.Add(rid, rRep);
					}
				}
			}
			catch(CBaseException c)
			{
				throw new CBaseException(String.Format("Unable to add cell to worksheet representation of tab '{0}' from compressed map '{2}' - {1}", m_worksheetName, c.UserMessage, m_mapName), c.DeveloperMessage);
			}
			catch(Exception e)
			{
				throw new CBaseException(String.Format("Unable to add cell to worksheet representation of tab '{0}' from compressed map '{2}' - {1}", m_worksheetName, e.Message, m_mapName), e);
			}
		}

		void IDisposable.Dispose()
		{
			try
			{
				foreach(string key in m_regionReps.Keys)
				{
					RegionRep rRep = (RegionRep)m_regionReps[key];
					((IDisposable)rRep).Dispose() ;
				}
			}
			finally
			{
				m_regionReps = null;
				m_mapTags = null;
				m_regionTags = null;
				m_miscTags = null;
			}
		}
	}

    /// <summary>
    /// Region rep keeps track of all the cells in a given region. It currently keeps track of specific locations
    /// by using a hashtable where the keys are the columns. Each value is another hashtable of rows and finally the value 
    /// is a cell rep. It also uses a hashtable to keep track of the tag types. Each tag type is a key. These insure we have a O(1) lookup. 
    /// Building the intial representation takes a bit more work because  it has to be added to multiple places. Also we are trading memory for 
    /// speed. Note, since hashtable do not gurantee order when we do a lookup by row,col it has to actually iterate trhough eveyr item in a column until found.  
    /// </summary>
    [DebuggerDisplay("RegionRep: Id={m_regionId}")]
	public class RegionRep : IDisposable 
	{
		#region Variables
		private string m_regionId;
		private Hashtable m_columns;
		private Hashtable m_tagTypes;
		
		public string RegionID
		{
			get { return m_regionId; }
		}
		#endregion

		public RegionRep(string regionId)
		{
			m_regionId = regionId;
			m_columns = new Hashtable();
			m_tagTypes = new Hashtable();
		}

		//TODO - refactor to be more efficient - not urgent, just for clarity
        /// <summary>
        /// Retrieves the next cell in the column. Note there is no representation of empty cells so it would be whatever nonempty
        /// cell follows which has the specified tag type. 
        /// </summary>
        /// <param name="topCell">The cell which the next cell should come after in the column.</param>
        /// <param name="tagType">The type of cell interedt in.</param>
        /// <returns>The CellRep if it meets the condition or null if it doesnt or it doesnt exist.</returns>
		public CellRep GetNextCellInColumnByType(CellRep topCell, E_TagType tagType)
		{
			if(m_columns.Contains(topCell.Column))
			{
				int topRow = topCell.Row;
				
				Hashtable column = (Hashtable)m_columns[topCell.Column];
				
				int latestRow = -1;
				
				CellRep foundCell = null;
				foreach(int key in column.Keys)
				{
					CellRep cell = (CellRep)column[key];

					if((cell.Row <= topRow) || ((cell.Row >= latestRow) && (latestRow >= 0)))
						continue;
					else
					{
						if(cell.TagType == tagType)
						{
							foundCell = cell;
							latestRow = cell.Row;
						}
					}
				}
				return foundCell;
			}
			else
			{
				return null;
			}
		}

        /// <summary>
        /// Creates a array list of column for the given type. 
        /// Basically get all cells that match the given tag type and put them in their respective columns.
        /// </summary>
        /// <param name="tagType">The tag type interested in.</param>
        /// <returns>Array list s containing arraylist which contain all the cell reps in the given column.</returns>
		public ArrayList GetColumnsByType(E_TagType tagType)
		{
			ArrayList tags = GetAllCellsByType(tagType);
			Hashtable columns = new Hashtable();
			ArrayList finalColumnList = new ArrayList();

			foreach(CellRep tag in tags)
			{
				int column = tag.Column;
				if(columns.Contains(column))
				{
					((ArrayList)columns[column]).Add(tag);
				}
				else
				{
					ArrayList newColumn = new ArrayList();
					newColumn.Add(tag);
					columns.Add(column, newColumn);
				}
			}
			foreach(int key in columns.Keys)
			{
				ArrayList currentColumn = (ArrayList)columns[key];
				//SortColumnByRowNumber(currentColumn);
				finalColumnList.Add(currentColumn);
			}

			return finalColumnList;
		}

        //private void SortColumnByRowNumber(ArrayList column)
        //{
        //    //TODO implement this, for efficiency, is not currently necessary
        //}

        /// <summary>
        /// Returns the list that contains the given tag type. 
        /// </summary>
        /// <param name="tagType"></param>
        /// <returns></returns>
		public ArrayList GetAllCellsByType(E_TagType tagType)
		{
			if(m_tagTypes.ContainsKey(tagType))
			{
				return (ArrayList)m_tagTypes[tagType];
			}
			else
			{
				return new ArrayList();
			}
		}
        /// <summary>
        /// Retrieves the cell at the given row,column. 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns>Null if the cel doesnt exist. </returns>
		public CellRep GetCell(int row, int column)
		{
			if(!m_columns.ContainsKey(column))
			{	
				return null;
			}
			else
			{
				Hashtable currentColumn = (Hashtable)m_columns[column];
				if(!currentColumn.ContainsKey(row))
				{
					return null;
				}
				else
				{
					return (CellRep)currentColumn[row];
				}
			}
		}
	
        /// <summary>
        /// Adds the given cell to the representation. Currently overwrites the given cell.
        /// </summary>
        /// <param name="cell">The cell to add.</param>
		public void AddCell(CellRep cell)
		{
			int col = cell.Column;
			int row = cell.Row;
			
			if(m_columns.ContainsKey(col))
			{
				Hashtable column = (Hashtable)m_columns[col];
				if(column.ContainsKey(row))
				{
					column[row] = cell;
				}
				else
				{
					column.Add(row, cell);
				}
			}
			else
			{
				Hashtable column = new Hashtable();
				column.Add(row, cell);
				m_columns.Add(col, column);
			}

			//Also insert this into it's appropriate type group for faster lookup
			E_TagType tagType = cell.TagType;
				
			if(m_tagTypes.ContainsKey(tagType))
			{
				((ArrayList)m_tagTypes[tagType]).Add(cell);
			}
			else
			{
				ArrayList tags = new ArrayList();
				tags.Add(cell);
				m_tagTypes.Add(tagType, tags);
			}
		}
	
		void IDisposable.Dispose()
		{
			try
			{
				foreach(int key in m_columns.Keys)
				{
					Hashtable column = (Hashtable)m_columns[key];
					if(column != null)
					{
						foreach(int key2 in column.Keys)
						{
							CellRep cRep = (CellRep)column[key2];
							((IDisposable)cRep).Dispose() ;
						}
					}
					column = null;
				}
			}
			finally
			{
				m_columns = null;
				m_tagTypes = null;
			}
		}
	}
    
    /// <summary>
    /// Represents a cell in the original xls map file. 
    /// </summary>
    [DebuggerDisplay("CellRep: Type={m_tagType} Row={m_row} Col={m_column} Value={m_value}")]
	public class CellRep : IDisposable 
	{
		#region Variables
		private E_TagType m_tagType;
		private int m_row;
		private int m_column;
		private string m_columnLetter;
		private string m_value;
		private Hashtable m_attributes;

		public E_TagType TagType
		{
			get { return m_tagType; }
		}

		public int Row
		{
			get { return m_row; }
		}

		public int Column
		{
			get { return m_column; }
		}

		public string ColumnLetter
		{
			get { return m_columnLetter; }	
		}

		public string Value
		{
			get {return m_value; }
		}

		public Hashtable Attributes
		{
			get { return m_attributes; }
		}
		#endregion
		
        /// <summary>
        /// Instantiates a new cellrep object. 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <param name="cellValue">Leading new line is removed.</param>
        /// <exception cref="CBaseException">If row or column are less than zero or the tag type could not be determined.</exception>
		public CellRep(int row, int column, string cellValue)
		{
			m_value = cellValue;
			m_row = row;
			m_column = column;
			m_attributes = new Hashtable();
			m_columnLetter = RatesheetMapCommon.GetColumnLetterFromNumber(m_column);

			if(cellValue.StartsWith("\\n"))
			{
				cellValue = cellValue.Substring(2);
			}
			
			if(row < 0)
			{
				throw new CBaseException(String.Format("Invalid compressed map - A value of '{0}' was found in the compressed file, and this value does not contain a valid row number.", cellValue), "");
			}
			if(column < 0)
			{
				throw new CBaseException(String.Format("Invalid compressed map - A value of '{0}' was found in the compressed file, and this value does not contain a valid column number.", cellValue), "");
			}
			
			if(cellValue.Trim().Equals(""))
			{
				m_tagType = E_TagType.Comment;
				return;
			}
			try
			{
				m_tagType = Tag.GetTagType(cellValue);
			}
			catch(CBaseException c)
			{
				throw new CBaseException(String.Format("Error while attempting to get tag type of tag located at map row {0}, column {1}. DETAILS: '{2}'", row, RatesheetMapCommon.GetColumnLetterFromNumber(column), c.UserMessage), c.DeveloperMessage); 
			}
			
			if(m_tagType != E_TagType.Comment)
			{
				int colonIndex = cellValue.IndexOf(":");
				ParseAttributes(cellValue.Substring(colonIndex + 1));
			}
		}

        /// <summary>
        /// Checks to see if the given attribute is in the list. Case insensitive trimed. 
        /// </summary>
        /// <param name="attributeName"></param>
        /// <returns>True if it exists false otherwise.</returns>
		public bool ContainsAttribute(string attributeName)
		{
			attributeName = attributeName.ToLower().Trim();
			return m_attributes.Contains(attributeName);
		}
        /// <summary>
        /// Retrieves the value for the given attribute name. Case insesitive. Name is Trimmed. 
        /// </summary>
        /// <param name="attributeName"></param>
        /// <returns>Attribute value or null if it doesnt exist</returns>
		public string GetAttribute(string attributeName)
		{
			attributeName = attributeName.ToLower().Trim();
			
			if(m_attributes.Contains(attributeName))
			{
				return m_attributes[attributeName].ToString();
			}
			else
			{
				return null;
			}
		}
        /// <summary>
        /// Parses all the key value pairs ( attributes ) from the given cell.
        /// New Lines in the string are removed. 
        /// </summary>
        /// <param name="cellValue"></param>
        /// <exception cref="CBaseException">Is Thrown when there is an invalid attribute in the cell. Or a attribute is added twice. </exception>
		private void ParseAttributes(string cellValue)
		{
			if(cellValue.EndsWith(";"))
				cellValue = cellValue.Substring(0, cellValue.Length-1);
			
			string[] attributes = cellValue.Split(';');
			for(int i = 0; i < attributes.Length; ++i)
			{
				string potentialAttribute = attributes[i];
				
				if(potentialAttribute == null || potentialAttribute.Trim().Equals(""))
					continue;

				string[] attributePair = potentialAttribute.Split('=');
				
				if(attributePair.Length < 2)
				{
					throw new CBaseException(String.Format("Invalid attribute '{0}' in cell located at map row {1}, column {2}.  Entire cell value is '{3}'.  DETAILS: Attribute is missing the '='.", potentialAttribute, m_row, m_column, cellValue), "");
				}
				string attributeName = attributePair[0].Trim().Replace("\\n", "");
				
				String attributeVal = "";

				if(attributeName.Equals("str") || attributeName.Equals("cm") || attributeName.Equals("parsestr"))
				{
					int indexPotAtt = cellValue.IndexOf(potentialAttribute);
					int indexEquals = cellValue.IndexOf("=", indexPotAtt);
					
					attributeVal = cellValue.Substring(indexEquals + 1);
					m_attributes.Add(attributeName, attributeVal.ToLower());
					break;
				}
				else
				{
					int indexEquals = potentialAttribute.IndexOf("=");
					attributeVal = potentialAttribute.Substring(indexEquals + 1);
				}

				if(m_attributes.ContainsKey(attributeName))
					throw new CBaseException(String.Format("Attribute '{0}' has been defined more than once in tag located at map row {1}, column {2}.", attributeName, m_row, m_column), "");

                if (m_attributes.ContainsKey(attributeName))
                {
                    m_attributes[attributeName] = attributeVal.ToLower();
                }
                else
                {
                    m_attributes.Add(attributeName, attributeVal.ToLower());
                }
			}
		}

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("TAG TYPE = " + m_tagType + Environment.NewLine);
			sb.Append("ROW = " + m_row + ", COL = " + m_column + Environment.NewLine);
			sb.Append("VALUE: " + m_value + Environment.NewLine);
			
			foreach(string key in m_attributes.Keys)
			{
				sb.Append("ATTRIBUTE NAME: " + key + " ATTRIBUTE VALUE: " + m_attributes[key].ToString() + Environment.NewLine);
			}

			return sb.ToString();
		}
	
		
		void IDisposable.Dispose()
		{
			m_attributes = null;
		}
	}
}