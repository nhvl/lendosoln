using System;
using System.Collections;
using DataAccess;
using System.Text;
using System.Text.RegularExpressions;

namespace RatesheetMap
{
	/**
	 * This ReadTagColumn class processes a column of read tags that was defined in the map.  Columns of read tags
	 * can be either "static" or "dynamic".  
	 * 
	 * Static columns are those where the map-maker defined an explicit list of read tags, and that exact number of read
	 * tags in the column will be processed.
	 * 
	 * Dynamic columns are those where the map-maker defined a start and end, and the ReadTagColumn class must fill in
	 * all the data in between.
	 * 
	 * Columns may have operators associated with them.  They might be math operators, such as a formula that will get
	 * applied to the data in each cell of the column, or a filter that will determine how the output data is grouped.
	 * Filters are things like Lock Period, Prepayment Penalty, and IO.
	 * 
	 * Some read tags in the column (or even all of them) might be variable declarations, and therefore some read tag
	 * columns will actually not create any output at all.
	 * 
	 * This class will take the column of read tags from the map and create an output column from it that contains
	 * the data in the ratesheet that corresponds to each read tag cell in the map column.
	 */
	
	public class ReadTagColumn
	{
		#region Variables
		private ArrayList		m_readTags;
		private int				m_colOffset;
		private bool			m_offsetIsSet;
		private ArrayList		m_filters;
		private ArrayList		m_dynamicListMathTags;
		private OutputColumn	m_oColumn;
		private bool			m_onlyVariables;
		private bool			m_isDynamicList;

		public OutputColumn OutputColumn
		{
			get { return m_oColumn; }
		}

		public bool OnlyVariables
		{
			get { return m_onlyVariables; }
		}

		public bool IsDynamicList
		{
			get { return m_isDynamicList; }
		}

		#endregion

		public ReadTagColumn(E_RegionType regionType, RatesheetWorksheet ratesheetRep, ArrayList columnElements, LandmarkCell anchorLandmarkInMap, LandmarkCell anchorLandmarkInRS, Operators operators, string regionId, int dynamicListEndRow, int dynamicListEndMapRow, Variables variables)
		{
			m_onlyVariables = true;
			m_offsetIsSet = false;
			m_readTags = new ArrayList();
			m_filters = new ArrayList();
			m_dynamicListMathTags = new ArrayList(); // When a math tag is located on the top row of a dynamic list only, it is treated as if it was located at the top of each column
			m_oColumn = new OutputColumn(regionId);
			m_isDynamicList = false;
			
			//Cycle through each read tag in this column and process it
			foreach(CellRep cell in columnElements)
			{
				ReadTag rTag = new ReadTag(regionId, cell, anchorLandmarkInMap);
				
				// If m_isDynamicList is already set to true, it means we already saw the top of the dynamic list
				// Therefore, if we are seeing other read tags in the same column, we need to ignore them (unless
				// they're variables), because static read tags cannot be mixed in with a dynamic list.
				if(m_isDynamicList && rTag.DestinationLocation != E_ReadLocations.VARIABLE)
					continue;

				if(rTag.DestinationLocation == E_ReadLocations.VARIABLE)
				{
					if(rTag.IsDynamicListStart && Variables.IsArrayVariable(rTag.DestinationVariableName))
					{
						m_isDynamicList = true;
						m_oColumn.IsDynamicList = true;
						DataCell dynCell = rTag.ProcessCorrespondingRSCell(ratesheetRep, anchorLandmarkInRS);

						int numRowsToRead = dynamicListEndRow - dynCell.Row;
						for(int i = 0; i < numRowsToRead; ++i)
						{
							DynamicListReadTag dlTag = new DynamicListReadTag(regionId, cell, anchorLandmarkInMap, i);
							DataCell dynamicCell = dlTag.ProcessCorrespondingRSCell(ratesheetRep, anchorLandmarkInRS);
							switch(rTag.DestinationVariableContext)
							{
								case E_VariableContext.Map:
									variables.AddMapVariable(rTag.DestinationVariableName, dynamicCell.Data);
									break;
								case E_VariableContext.Region:
									variables.AddRegionVariable(regionId, rTag.DestinationVariableName, dynamicCell.Data);
									break;
								default:
									throw new CBaseException(String.Format("Error while processing variables from a read tag in region '{0}'.  The read tag has an invalid destination context of '{1}.'", regionId, rTag.DestinationVariableContext),"");
							}
						}
						continue;
					}
					
					
					string destinationValue = "";
					if(m_isDynamicList)
					{
						if(dynamicListEndMapRow < 0)
							throw new CBaseException(String.Format("Unable to process a variable read tag in region '{0}' because this tag is located in a column that contains a dynamic list, but no dynamic list end has been defined specifically for this column.  The read tag is located at map row {1}, column {2}.", regionId, rTag.Row, rTag.ColumnLetter), "");
						
						int exactColumn = rTag.ColumnOffset + anchorLandmarkInRS.Column;
						int rowOffset = rTag.Row - dynamicListEndMapRow;
						int exactRow = dynamicListEndRow + rowOffset;
						destinationValue = rTag.GetVariableValue(ratesheetRep, exactRow, exactColumn);
					}
					else
					{
						destinationValue = rTag.GetVariableValue(ratesheetRep, anchorLandmarkInRS);
					}

					switch(rTag.DestinationVariableContext)
					{
						case E_VariableContext.Map:
							variables.AddMapVariable(rTag.DestinationVariableName, destinationValue);
							break;
						case E_VariableContext.Region:
							variables.AddRegionVariable(regionId, rTag.DestinationVariableName, destinationValue);
							break;
						default:
							throw new CBaseException(String.Format("Error while processing variables from a read tag in region '{0}'.  The read tag has an invalid destination context of '{1}.'", regionId, rTag.DestinationVariableContext),"");
					}
					continue;
				}
				
				m_onlyVariables = false;

				if(!m_offsetIsSet)
				{
					m_colOffset = rTag.ColumnOffset;
					m_offsetIsSet = true;
					if(regionType == E_RegionType.RateOption)
						AddFilters(operators.GetOperatorsForColumnOffset(m_colOffset), false /*copyFilter*/);
				}

				DataCell oCell = rTag.ProcessCorrespondingRSCell(ratesheetRep, anchorLandmarkInRS);

				if((regionType == E_RegionType.RateOption) && rTag.IsDynamicListStart && m_filters.Count != 0)
				{
					AddFilters(operators.GetOperatorsForRowOffset(rTag.RowOffset), true /*copyFilter*/);
					AddDynamicListMathTags(operators.GetOperatorsForRowOffset(rTag.RowOffset), true /*copyFilter*/);
				}

				m_oColumn.AddOutputCell(oCell);
				m_oColumn.GroupFilters = m_filters;
				
				if(operators != null)
					operators.AddGlobalMathOperators(m_dynamicListMathTags);

				//If this read tag is a dynamic list start, fill in the rest of the reads in this column
				if(rTag.IsDynamicListStart)
				{
					m_isDynamicList = true;
					m_oColumn.IsDynamicList = true;
					int numRowsToRead = dynamicListEndRow - oCell.Row;
					for(int i = 1; i < numRowsToRead; ++i)
					{
						DynamicListReadTag dlTag = new DynamicListReadTag(regionId, cell, anchorLandmarkInMap, i);
						DataCell dynamicCell = dlTag.ProcessCorrespondingRSCell(ratesheetRep, anchorLandmarkInRS);
						m_oColumn.AddOutputCell(dynamicCell);
					}
				}
			}
		}

		
		private void AddDynamicListMathTags(ArrayList operators, bool copyFilter)
		{
			foreach(AbstractOperator op in operators)
			{
				if(op.OperatorTagType.Equals(E_OperatorTagType.Math))
				{
					if(copyFilter)
						m_dynamicListMathTags.Add(op.GetCopy());
					else
						m_dynamicListMathTags.Add(op);
				}
			}
		}

		private void AddFilters(ArrayList operators, bool copyFilter)
		{
			foreach(AbstractOperator op in operators)
			{
				if(op.OperatorTagType.Equals(E_OperatorTagType.Group))
				{
					if(copyFilter)
						m_filters.Add(op.GetCopy());
					else
						m_filters.Add(op);
				}
			}
		}
	}
}
