namespace RatesheetMap
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;

    public class RatesheetMapCommon
	{
        private static readonly string[] ExcelColumnLetters = {
                                                        "0","A","B","C","D","E","F","G","H","I",
                                                        "J","K","L","M","N","O","P","Q","R","S",
                                                        "T","U","V","W","X","Y","Z","AA","AB","AC",
                                                        "AD","AE","AF","AG","AH","AI","AJ","AK",
                                                        "AL","AM","AN","AO","AP","AQ","AR","AS",
                                                        "AT","AU","AV","AW","AX","AY","AZ","BA",
                                                        "BB","BC","BD","BE","BF","BG","BH","BI",
                                                        "BJ","BK","BL","BM","BN","BO","BP","BQ",
                                                        "BR","BS","BT","BU","BV","BW","BX","BY",
                                                        "BZ","CA","CB","CC","CD","CE","CF","CG",
                                                        "CH","CI","CJ","CK","CL","CM","CN","CO",
                                                        "CP","CQ","CR","CS","CT","CU","CV","CW",
                                                        "CX","CY","CZ","DA","DB","DC","DD","DE",
                                                        "DF","DG","DH","DI","DJ","DK","DL","DM",
                                                        "DN","DO","DP","DQ","DR","DS","DT","DU",
                                                        "DV","DW","DX","DY","DZ","EA","EB","EC",
                                                        "ED","EE","EF","EG","EH","EI","EJ","EK",
                                                        "EL","EM","EN","EO","EP","EQ","ER","ES",
                                                        "ET","EU","EV","EW","EX","EY","EZ","FA",
                                                        "FB","FC","FD","FE","FF","FG","FH","FI",
                                                        "FJ","FK","FL","FM","FN","FO","FP","FQ",
                                                        "FR","FS","FT","FU","FV","FW","FX","FY",
                                                        "FZ","GA","GB","GC","GD","GE","GF","GG",
                                                        "GH","GI","GJ","GK","GL","GM","GN","GO",
                                                        "GP","GQ","GR","GS","GT","GU","GV","GW",
                                                        "GX","GY","GZ","HA","HB","HC","HD","HE",
                                                        "HF","HG","HH","HI","HJ","HK","HL","HM",
                                                        "HN","HO","HP","HQ","HR","HS","HT","HU",
                                                        "HV","HW","HX","HY","HZ","IA","IB","IC",
                                                        "ID","IE","IF","IG","IH","II","IJ","IK",
                                                        "IL","IM","IN","IO","IP","IQ","IR","IS",
                                                        "IT","IU","IV"
                                                    };
		public static bool IsNumeric(string val)
		{
            if (string.IsNullOrEmpty(val))
            {
                return false;
            }
            if (char.IsLetter(val[0]))
            {
                return false;
            }
            double v;
            return double.TryParse(val, out v);
		}

        public static List<string> GetExcelFiles(string path)
        {
            List<string> searchPatterns = new List<string>() { "*.xls", "*.cpx", "*.xlsx" };
            var result = searchPatterns.SelectMany(x => Directory.GetFiles(path, x));

            return result.ToList();
        }

		public static bool IsExcelFile(string sFileName)
		{
            if (!FileOperationHelper.Exists(sFileName))
            {
                return false;
            }

            if (!sFileName.ToLower().EndsWith(".xls") && !sFileName.ToLower().EndsWith(".xlsx"))
            {
                return false; // filename must be in the format of NAME.XLS
            }

			return true ;
		}

        internal static bool IsCPXFile(string sFileName)
        {
            if (!FileOperationHelper.Exists(sFileName))
            {
                return false;
            }

            if (!sFileName.ToLower().EndsWith(".cpx"))
            {
                return false; // filename must be in the format of NAME.CPX
            }

            return true;
        }

		public static bool IsGuid(string val)
		{
			Guid tryParse = Guid.Empty;
			try
			{
				tryParse = new Guid(val);
			}
			catch{}

			return !tryParse.Equals(Guid.Empty);
		}

		public static string GetDescription(Enum en)        
		{            
			Type type = en.GetType();            
			MemberInfo[] memInfo = type.GetMember(en.ToString());            
			if (memInfo != null && memInfo.Length > 0)            
			{                
				object[] attrs = memInfo[0].GetCustomAttributes(typeof(Description), false);
				if (attrs != null && attrs.Length > 0)
					return ((Description)attrs[0]).Text;
			}            
			return en.ToString();
		}

		public static string WildcardToRegex(string pattern, string wildcard)
		{
			if(pattern == null)
				return "";
			if(wildcard == null || wildcard.Equals(""))
				return Regex.Escape(pattern);
			else
			{
				if(IsSpecialRegexChar(wildcard))
					return Regex.Escape(pattern).Replace("\\"+wildcard, ".*");
				else
					return Regex.Escape(pattern).Replace(wildcard, ".*");
			}
		}

		public static string WildcardToRegexFullString(string pattern, string wildcard)
		{
			if(pattern == null)
				return "";
			
			if(wildcard == null || wildcard.Equals(""))
				return "^" + Regex.Escape(pattern) + "$";
			else if(pattern.Replace(wildcard, "") == "")
				throw new CBaseException("There must be more than just a wildcard value in the search string.", "");
			else
			{
				// The SAE's can define what character they want to use as the regex wildcard because some ratesheets might use
                // the standard wildcard characters in their ratesheets to mean something else.  Therefore, we must handle the situation
                // when the SAE actually chooses a standard regex character by replacing the previously escaped character with the standard
                // regex wildcard character, otherwise it will get parsed by the Regex class and something unintended.
                if(IsSpecialRegexChar(wildcard))
					return "^" + Regex.Escape(pattern).Replace("\\"+wildcard, ".*") + "$";
				else
					return "^" + Regex.Escape(pattern).Replace(wildcard, ".*") + "$";
			}
		}

		private static bool IsSpecialRegexChar(string val)
		{
			if(val.Equals(".") 
				|| val.Equals("$") 
				|| val.Equals("^") 
				|| val.Equals("*") 
				|| val.Equals("\\") 
				|| val.Equals("[")
				|| val.Equals("]")
				|| val.Equals("{")
				|| val.Equals("}")
				|| val.Equals("(")
				|| val.Equals(")")
				|| val.Equals("<")
				|| val.Equals(">")
				|| val.Equals("|")
				|| val.Equals("+")
				|| val.Equals("?"))
			{
				return true;
			}
			else
				return false;
		}

		// Retrieves the letter of the column as displayed in Excel from the column number, as retrieved from the excel interop
		public static string GetColumnLetterFromNumber(int colNum)
		{
			if(colNum <= 0)
				return colNum.ToString();

            // 2/6/2014 dd - Pregenerate the first 256 columns of Excel.
            if (colNum < ExcelColumnLetters.Length)
            {
                return ExcelColumnLetters[colNum];
            }
			string s = "";
			double colDouble = Convert.ToDouble(colNum);
			double d1 = Convert.ToDouble(25 * (colDouble + 1));
			int l1 = Convert.ToInt32(Math.Log(d1) / Math.Log(26)) - 1;
			for (int i = l1; i >= 0; i--)
			{
				int x = Convert.ToInt32(Math.Pow(26, i + 1) - 1) / 25 - 1;
				if (colNum > x)
				{
					s += (char)(((colNum - x - 1) / Convert.ToInt32(Math.Pow(26, i))) % 26 + 65);
				}
			}
																					
			return s;
		}

		// For debugging
		public static string GetTabString(int numTabs)
		{
			string tabs = "&nbsp;&nbsp;&nbsp;&nbsp;";
			string toReturn = "";
			for(int i = 0; i < numTabs; ++i)
			{
				toReturn += tabs;
			}
			return toReturn;
		}

		// 1/7/08 - Currently includes underscore and dash - so this name could be changed to be more generic
		public static bool IsAlphanumericWithUnderscore(string val)
		{
			return !Regex.IsMatch (val.Replace(" ",""), "[^a-zA-Z0-9_/-]");
		}

        private static List<KeyValuePair<string, int>> s_readLocationDefinitionList = null;

        public static IEnumerable<KeyValuePair<string, int>> GetReadLocationDefinitionList()
        {
            if (s_readLocationDefinitionList == null)
            {
                s_readLocationDefinitionList = new List<KeyValuePair<string, int>>();

                foreach (E_ReadLocations o in Enum.GetValues(typeof(E_ReadLocations)))
                {
                    s_readLocationDefinitionList.Add(new KeyValuePair<string, int>(o.ToString().ToLower(), (int)o));
                }
            }

            return s_readLocationDefinitionList;
        }
	}

	sealed class NumericComparer: IComparer
	{
		private bool m_throwInvalidRowErrors = true;
		public NumericComparer(bool throwInvalidRowErrors)
		{
			m_throwInvalidRowErrors = throwInvalidRowErrors;
		}
		
		public int Compare(object x, object y)
		{
			if(x == null || y == null)
				throw new CBaseException("Cannot sort region with null values in the sorting column (NR).", "");
			
			string dataX = ((DataCell)x).Data;
			string dataY = ((DataCell)y).Data;

			if(m_throwInvalidRowErrors == false)
			{
				if(!RatesheetMapCommon.IsNumeric(dataX) && !RatesheetMapCommon.IsNumeric(dataY))
					return 0;
				else if(!RatesheetMapCommon.IsNumeric(dataX))
					return 1;
				else if(!RatesheetMapCommon.IsNumeric(dataY))
					return -1;
			}
			
			try
			{
				double dX = Convert.ToDouble(dataX);
				double dY = Convert.ToDouble(dataY);
				if(dX == dY)
					return 0;
				else if(dX < dY)
					return 1;
				else
					return -1;
			}
			catch(Exception e)
			{
				throw new CBaseException(String.Format("Invalid numeric type for sorting, value1 = {0}, value2 = {1}.  Sometimes this happens when a dynamic list end tag has been declared in the wrong place (ie: lower than it should be).", dataX, dataY), e);
			}
		}
	}

	sealed class NumericComparerAsc: IComparer
	{
		public int Compare(object x, object y)
		{
			if(x == null || y == null)
				throw new CBaseException("Cannot sort region with null values in the sorting column (P).", "");
			
			string dataX = ((DataCell)x).Data;
			string dataY = ((DataCell)y).Data;

			/*if(dataX.Equals("") && dataY.Equals(""))
				return 0;
			else if(dataX.Equals(""))
				return 1;
			else if(dataY.Equals(""))
				return -1;*/
			if(dataX.Equals("") || dataY.Equals(""))
			{
				throw new CBaseException("There are blank values in the sorting column (P).  If this is a dynamic list, it might need to be made into a static list to avoid blank values.", "");
			}

			try
			{
				double dX = Convert.ToDouble(dataX);
				double dY = Convert.ToDouble(dataY);
				if(dX == dY)
					return 0;
				else if(dX > dY)
					return 1;
				else
					return -1;
			}
			catch(Exception e)
			{
				throw new CBaseException(String.Format("Invalid numeric type for sorting, value1 = {0}, value2 = {1}.  Sometimes this happens when a dynamic list end tag has been declared in the wrong place (ie: lower than it should be).", dataX, dataY), e);
			}
		}
	}

	public enum E_FormulaFunctionType
	{
		MAX = 0,
		MIN = 1
	}

	public enum E_Timezone
	{
		P = 0,
		M = 1,
		C = 2,
		E = 3
	}
	
	public enum E_MapContext
	{
		Global = 0,
		Lender = 1
	}

	public enum E_VariableContext
	{
		Map = 0,
		Region = 1
	}

	public enum E_TagType
	{
		Region = 0,
		Landmark = 1,
		Read = 2,
		Operator = 3,
		DynamicListBottom = 4,
		RuleAttribute = 5,
		Map = 6,
		Comment = 7
	}

	public enum E_OperatorTagType
	{
		Math = 0,
		Group = 1,
		RuleAttribute = 2
	}

	public enum E_LandmarkType
	{
		Map = 0,
		Ratesheet = 1
	}

	public enum E_RuleAttributeType
	{
		Description = 0,
		Condition = 1
	}

	//Please leave these in this order (or when adding new, make them descending in order of importance/nesting)
	public enum E_OperatorAttributeType
	{
		[Description("IO")]
		IO = 0,
		[Description("PP")]
		PPP = 1, // The only reason this doesn't match the description is because the original spec differed from the data required by the importer, so instead of making everyone change their maps, I just added the description attribute to handle what needs to be in the output for the importer
		[Description("LOCK")]
		LK = 2
	}

	public enum E_SortOrder
	{
		Asc = 0,
		Desc = 1
	}

	//Please leave these in this order
	public enum E_ReadLocations
	{
		NR = 0,
		P = 1,
		M = 2,
		QR = 3,
		TR = 4,
		FILE = 5,
		VARIABLE = 6,
		RSEDate = 7,
		RSETime = 8,
		RSEDatetime = 9
	}

	public enum E_RegionType
	{
		RateOption = 0,
		AdjustTable = 1,
		RSEffective = 2,
		NoOutput = 3
	}

	class Description : Attribute    
	{        
		public string Text;        
		public Description(string text)        
		{            
			Text = text;        
		}    
	}

	public enum E_LoObjType
	{
		[Description("")]
		None = 0,
		[Description("Program Id")]
		LoProgramId = 1,
		[Description("Policy")]
		LoPolicyId = 2,
		[Description("Ratesheet Id")]
		LoRatesheetId = 3
	}
}