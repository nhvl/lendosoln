using System;
using System.Collections;
using DataAccess;

namespace RatesheetMap
{
	public class AdjustmentTableRegion : Region
	{
		public AdjustmentTableRegion(CellRep region, WorksheetFileRep mapRep, RatesheetWorksheet ratesheet, E_MapContext mapContext) : base(region, mapRep, ratesheet, E_RegionType.AdjustTable, mapContext){}
		public override void ValidateInput(CellRep region)
		{
			if(m_regionId.Trim().Equals(""))
				throw new CBaseException(String.Format("Error creating AdjustTable region: Region ID cannot be blank.  Region definition is located at map row {0}, column {1}, text = '{2}'.", region.Row, region.ColumnLetter, region.Value), "");
			
			if(m_loObjType.Equals(E_LoObjType.LoProgramId) || m_loObjType.Equals(E_LoObjType.LoRatesheetId))
				throw new CBaseException(String.Format("Error in region '{0}': Adjustment Table Regions cannot have an LoProgramId or LoRatesheetId", m_regionId), "");
		
			if(m_loObjType.Equals(E_LoObjType.None) && (m_optional == false))
				throw new CBaseException(String.Format("Error in region '{0}': Adjustment Table Regions must define an LoPolicyId unless the 'Optional' attribute is set to true", m_regionId), "");
		
			if(m_fragment == true)
				throw new CBaseException(String.Format("Error in region '{0}': Adjustment Table Regions cannot be fragments.", m_regionId), "");
		
			if(!m_timezone.Equals(E_Timezone.P))
				throw new CBaseException(String.Format("Error in region '{0}': Adjustment Table Regions cannot define a Timezone - you must use the RSEffective region type to define the timezone.", m_regionId), "");
		
			if(m_loObjType.Equals(E_LoObjType.LoPolicyId) && !RatesheetMapCommon.IsGuid(m_loObjId))
				throw new CBaseException(String.Format("Error in region '{0}': Policy Id '{1}' is not a Guid value.", m_regionId, m_loObjId), "");
		
			if(m_srcLoProgramId != null)
				throw new CBaseException(String.Format("Error in region '{0}': SrcLoProgramId attribute can only be declared in RateOption regions.", m_regionId), "");
		}
		
		public override void ProcessOperators()
		{
			if(this.IsFound == false)
				return;
			ArrayList operatorList = m_mapRep.GetAllCellsByType(m_regionId, E_TagType.Operator);
			operatorList.AddRange(m_mapRep.GetAllCellsByType(m_regionId, E_TagType.RuleAttribute));
			m_operators = new Operators(operatorList, RegionId, E_RegionType.AdjustTable, m_anchorLandmarkInMap);
		}
	}
}
