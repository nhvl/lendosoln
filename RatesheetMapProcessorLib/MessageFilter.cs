//using System;
//using EnvDTE;
//using EnvDTE80;
//using System.Runtime.InteropServices;
//
//namespace RatesheetMap
//{
//	[ComImport(), Guid("00000016-0000-0000-C000-000000000046"),    
//	InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]
//	interface IOleMessageFilter // deliberately renamed to avoid confusion w/ System.Windows.Forms.IMessageFilter
//	{
//		[PreserveSig]
//		int HandleInComingCall( 
//			int dwCallType, 
//			IntPtr hTaskCaller, 
//			int dwTickCount, 
//			IntPtr lpInterfaceInfo);
//
//		[PreserveSig]
//		int RetryRejectedCall( 
//			IntPtr hTaskCallee, 
//			int dwTickCount,
//			int dwRejectType);
//
//		[PreserveSig]
//		int MessagePending( 
//			IntPtr hTaskCallee, 
//			int dwTickCount,
//			int dwPendingType);
//	}
//
//
//	public class MessageFilter : IOleMessageFilter
//	{
//		public static void Register()
//		{
//			IOleMessageFilter newfilter = new MessageFilter(); 
//
//			IOleMessageFilter oldfilter = null; 
//			CoRegisterMessageFilter(newfilter, out oldfilter);
//		}
//
//		public static void Revoke()
//		{
//			IOleMessageFilter oldfilter = null; 
//			CoRegisterMessageFilter(null, out oldfilter);
//		}
//
//		//
//		// IOleMessageFilter impl
//    
//		int IOleMessageFilter.HandleInComingCall(int dwCallType, System.IntPtr hTaskCaller, int dwTickCount, System.IntPtr lpInterfaceInfo) 
//		{
//			//LogHelper.LogError("IOleMessageFilter::HandleInComingCall");
//			//System.Diagnostics.Debug.WriteLine("IOleMessageFilter::HandleInComingCall");
//
//			return 0; //SERVERCALL_ISHANDLED
//		}
//
//		int IOleMessageFilter.RetryRejectedCall(System.IntPtr hTaskCallee, int dwTickCount, int dwRejectType)
//		{
//			//LogHelper.LogError("IOleMessageFilter::RetryRejectedCall");
//			//System.Diagnostics.Debug.WriteLine("IOleMessageFilter::RetryRejectedCall");
//
//			if (dwRejectType == 2 ) //SERVERCALL_RETRYLATER
//			{
//				//System.Diagnostics.Debug.WriteLine("Retry call later");
//				return 99; //retry immediately if return >=0 & <100
//			}
//			return -1; //cancel call
//		}
//
//
//		int IOleMessageFilter.MessagePending(System.IntPtr hTaskCallee, int dwTickCount, int dwPendingType)
//		{
//			//LogHelper.LogError("IOleMessageFilter::MessagePending");
//			//System.Diagnostics.Debug.WriteLine("IOleMessageFilter::MessagePending");
//
//			return 2; //PENDINGMSG_WAITDEFPROCESS 
//		}
//
//		// Implement the IOleMessageFilter interface.
//		[DllImport("Ole32.dll")]
//		private static extern int CoRegisterMessageFilter(IOleMessageFilter newFilter, out IOleMessageFilter oldFilter);
//	}
//}
