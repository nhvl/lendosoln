﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Admin;

namespace RatesheetMap
{
    public static class CacheDatabaseData
    {

        private static object s_lock = new object();

        private static bool s_isInitialized = false;

        private static Dictionary<string, List<Guid>> s_brokerIdRateSheetIdDictionary = null;
        public static void Initialize()
        {
            // 2/4/2014 dd - This method is public because just in case we want to measure the execution time or
            // reload from database.
            lock (s_lock)
            {
                InitializeImpl();
            }
        }
        private class _Item
        {
            public Guid lLpTemplateId;
            public string lRateOptionBaseId;
            public Guid BrokerId;
        }
        private static void InitializeImpl()
        {
            s_brokerIdRateSheetIdDictionary = new Dictionary<string, List<Guid>>(StringComparer.OrdinalIgnoreCase);

            string sql = @"SELECT lLpTemplateId, lRateOptionBaseId, l.BrokerId
FROM LOAN_PROGRAM_TEMPLATE l WITH(NOLOCK) 
WHERE l.IsLpe=1";


            List<_Item> temporaryList = new List<_Item>();

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, sql))
            {
                while (reader.Read())
                {
                    _Item item = new _Item();
                    item.lLpTemplateId = (Guid) reader["lLpTemplateId"];
                    item.lRateOptionBaseId = (string) reader["lRateOptionBaseId"];
                    item.BrokerId = (Guid) reader["BrokerId"];

                    temporaryList.Add(item);
                }
            }

            Dictionary<Guid, BrokerDbLite> brokerDictionary = new Dictionary<Guid, BrokerDbLite>();

            foreach (_Item item in temporaryList)
            {
                BrokerDbLite brokerDbLite = null;

                if (brokerDictionary.TryGetValue(item.BrokerId, out brokerDbLite) == false)
                {
                    brokerDbLite = BrokerDbLite.RetrieveById(item.BrokerId);
                    brokerDictionary.Add(item.BrokerId, brokerDbLite);
                }

                if (brokerDbLite.Status == 0)
                {
                    continue; // Skip inactive broker.
                }

                string key = brokerDbLite.CustomerCode + "::" + item.lRateOptionBaseId;
                List<Guid> list = null;

                if (s_brokerIdRateSheetIdDictionary.TryGetValue(key, out list) == false)
                {
                    list = new List<Guid>();
                    s_brokerIdRateSheetIdDictionary.Add(key, list);
                }
                list.Add(item.lLpTemplateId);
            }

            s_isInitialized = true;

        }

        public static IEnumerable<Guid> GetProgramIds(string customerCode, string ratesheetId)
        {
            lock (s_lock)
            {
                if (s_isInitialized == false)
                {
                    InitializeImpl();
                }
            }

            string key = customerCode + "::" + ratesheetId;


            List<Guid> list = null;

            if (s_brokerIdRateSheetIdDictionary.TryGetValue(key, out list) == false)
            {
                list = new List<Guid>();
            }
            return list;
        }
    }
}
