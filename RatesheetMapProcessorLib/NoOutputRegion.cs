using System;
using System.Collections;
using DataAccess;

namespace RatesheetMap
{
	public class NoOutputRegion : Region
	{
		public NoOutputRegion(CellRep region, WorksheetFileRep mapRep, RatesheetWorksheet ratesheet, E_MapContext mapContext) : base(region, mapRep, ratesheet, E_RegionType.NoOutput, mapContext){}
		
		public override void ValidateInput(CellRep region)
		{
			if(m_regionId.Trim().Equals(""))
				throw new CBaseException(String.Format("Error creating NoOutput region: Region ID cannot be blank.  Region definition is located at map row {0}, column {1}, text = '{2}'.", region.Row, region.ColumnLetter, region.Value), "");
			
			if(!m_loObjType.Equals(E_LoObjType.None))
				throw new CBaseException(String.Format("Error in region '{0}': NoOutput Regions cannot declare an object type (ie: ProgramID or PolicyID).", m_regionId), "");
		
			if(m_fragment == true)
				throw new CBaseException(String.Format("Error in region '{0}': NoOutput Regions cannot be fragments.", m_regionId), "");
		
			if(m_optional == true)
				throw new CBaseException(String.Format("Error in region '{0}': NoOutput Regions cannot set 'Optional' attribute to 'true'.", m_regionId), "");

			if(!m_timezone.Equals(E_Timezone.P))
				throw new CBaseException(String.Format("Error in region '{0}': NoOutput Regions cannot define a Timezone - you must use the RSEffective region type to define the timezone.", m_regionId), "");
			
			if(m_srcLoProgramId != null)
				throw new CBaseException(String.Format("Error in region '{0}': SrcLoProgramId attribute can only be declared in RateOption regions.", m_regionId), "");
		}

		public override void ProcessOperators()
		{
			m_operators = null;
		}
	}
}
