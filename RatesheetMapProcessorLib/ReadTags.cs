using System;
using System.Collections;
using DataAccess;
using System.Text;
using System.Text.RegularExpressions;


namespace RatesheetMap
{
	public class ReadTags
	{
		public static OutputGroup CreateOutputData( string regionId, E_RegionType regionType, WorksheetFileRep mapRep, RatesheetWorksheet ratesheetRep, LandmarkCell anchorLandmarkInMap, LandmarkCell anchorLandmarkInRS, Operators operators, Variables variables, bool throwInvalidRowErrors)
		{
			OutputGroup oData;
			switch(regionType)
			{
				case E_RegionType.NoOutput:
				case E_RegionType.RateOption:
					oData = new OutputData(regionId, true /*IsRoot*/, throwInvalidRowErrors);
					break;
				case E_RegionType.AdjustTable:
					oData = new AdjustmentTableOutputData(regionId);
					break;
				default:
					throw new CBaseException(String.Format("Error while attempting to create output data in region '{0}'.  The region has an invalid type of '{1}.'", regionId, regionType),"");
			}
			
			ArrayList foundColumns = mapRep.GetColumnsByType(regionId, E_TagType.Read);
			
			if(foundColumns.Count == 0) // No read tags were declared for this region, so there is no output data
				return null;

			int dynamicListEndRow = GetSmallestDynamicListEndRowInRS(foundColumns, mapRep, ratesheetRep, anchorLandmarkInRS, anchorLandmarkInMap, regionId);
			
			foreach(ArrayList column in foundColumns)
			{
				int dynamicListEndMapRow = GetDynamicListEndRowInMap(column, mapRep, anchorLandmarkInMap, regionId);
				ReadTagColumn rtCol = new ReadTagColumn(regionType, ratesheetRep, column, anchorLandmarkInMap, anchorLandmarkInRS, operators, regionId, dynamicListEndRow, dynamicListEndMapRow, variables);

				if(!rtCol.OnlyVariables)
					oData.AddColumn(rtCol.OutputColumn);
			}
			
			oData.RegionVariables = variables.GetRegionVariables(regionId);

			return oData;
		}

		//TODO - change this dynamic list system to be more centralized for code clarity
		private static int GetDynamicListEndRowInMap(ArrayList column, WorksheetFileRep mapRep, LandmarkCell anchorLandmarkInMap, string regionId )
		{
			if(column.Count == 0)
				return -1;

			ReadTag rTag = new ReadTag(regionId, (CellRep)column[0], anchorLandmarkInMap);
			if(rTag.DestinationLocation == E_ReadLocations.VARIABLE)
			{
				for(int i = 1; i < column.Count; ++i)
				{
					rTag = new ReadTag(regionId, (CellRep)column[i], anchorLandmarkInMap);
					if(rTag.DestinationLocation != E_ReadLocations.VARIABLE)
						break;
				}
			}
			
			if(!rTag.IsDynamicListStart)
				return -1;

			CellRep mapTop = mapRep.GetCell(regionId, rTag.Row, rTag.Column);
			CellRep dlEndCell = mapRep.GetNextCellInColumnByType(regionId, mapTop, E_TagType.DynamicListBottom);
			
			if(dlEndCell == null)
				return -1;
			else
				return dlEndCell.Row;
		}
		
		private static int GetSmallestDynamicListEndRowInRS(ArrayList readColumns, WorksheetFileRep mapRep, RatesheetWorksheet ratesheet, LandmarkCell anchorLandmarkInRS, LandmarkCell anchorLandmarkInMap, string regionId )
		{
			int endRow = -1;
			bool isDynamicListStart = false;

			foreach(ArrayList column in readColumns)
			{
				if(column.Count == 0)
					continue;

				ReadTag rTag = new ReadTag(regionId, (CellRep)column[0], anchorLandmarkInMap);

				if(rTag.DestinationLocation == E_ReadLocations.VARIABLE)
				{
					for(int i = 1; i < column.Count; ++i)
					{
						rTag = new ReadTag(regionId, (CellRep)column[i], anchorLandmarkInMap);
						//Keep searching until a non-variable read tag is found
						if(rTag.DestinationLocation != E_ReadLocations.VARIABLE)
							break;
					}
				}

				if(!rTag.IsDynamicListStart)
				{
					if(rTag.DestinationLocation == E_ReadLocations.VARIABLE) //It's ok for these to not be dynamic list starts
						continue;
					else
						break;
				}

				isDynamicListStart = true;

				CellRep mapTop = mapRep.GetCell(regionId, rTag.Row, rTag.Column);
				CellRep dlEndRange = mapRep.GetNextCellInColumnByType(regionId, mapTop, E_TagType.DynamicListBottom);
				
				if(dlEndRange == null)
					continue;

				LandmarkCell dCell = new MapLandmarkCell(regionId, dlEndRange, false /*isAnchorLandmark*/, anchorLandmarkInMap.Row, anchorLandmarkInMap.Column);
				
				RatesheetCellRep dlTopRange = ratesheet.GetCell(rTag.GetRatesheetRow(anchorLandmarkInRS), rTag.GetRatesheetColumn(anchorLandmarkInRS));
				
				if(dlTopRange == null)
					continue;
				
				RatesheetCellRep foundDLEnd = ratesheet.GetNextCellInColumnByValue(dlTopRange, dCell.SearchString, dlEndRange.GetAttribute("wildcard"), true /*trim blanks off end*/);
				
				if(foundDLEnd == null)
					continue;
				else
				{
					if(endRow < 0 || (foundDLEnd.Row < endRow))
					{
						endRow = foundDLEnd.Row;
					}
				}
			}

			if(isDynamicListStart && (endRow < 0))
				throw new CBaseException(String.Format("Could not find any valid Dynamic List End tags for region '{0}'.  (Dynamic List End tags must be in the same column as their respective Dynamic List Top)", regionId), "");
			return endRow;
		}
	}
}