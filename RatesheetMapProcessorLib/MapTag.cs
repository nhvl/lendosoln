using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using DataAccess;

namespace RatesheetMap
{
    /// <summary>
    /// Map_Tag_Name : "context"
    /// </summary>
	public class MapTag : Tag
	{
		#region Variables
		private string	m_ratesheetLocation;
		
		public E_MapContext MapContext { get; private set; }

		public bool ThrowInvalidRowErrors { get; private set; }

		public bool IsLPOptional { get; private set; }

		public string ClientCode { get; private set; }
		#endregion
		
		public MapTag(ArrayList mapCells, string ratesheetLocation, string mapName) : base(E_TagType.Map)
		{
            this.MapContext = E_MapContext.Global;

			m_ratesheetLocation = ratesheetLocation;

            if (mapCells.Count < 1)
            {
                return;
            }

			CellRep mapCell = (CellRep)mapCells[0];

			string context = mapCell.GetAttribute("context");

			//TODO - remove this if statement after all context tags have been removed (only temporary for migration)
            // db - as of 10/7/10, need to verify if all context tags have been removed from maps.  This is just for code clarity, not urgent.
			if(context == null || context.Trim().Equals(""))
			{
				context = mapCell.GetAttribute("type");
			}

			if(context != null && !context.Equals(""))
			{
				try
				{
					this.MapContext = (E_MapContext) Enum.Parse(typeof(E_MapContext), context, true);
				}
				catch(Exception e)
				{
					throw new CBaseException(String.Format("Invalid 'type/context' attribute for map tag in map file '{0}'.  The map tag text is '{1}'.", mapName, mapCell.Value), e);
				}
			}

            this.IsLPOptional = mapCell.GetAttribute("lpoptional") == "y";
			this.ThrowInvalidRowErrors = (mapCell.GetAttribute("throwinvalidrowerrors") == "y");

            if (this.MapContext == E_MapContext.Global && this.IsLPOptional == true)
            {
                throw new CBaseException(String.Format("Invalid LpOptional attribute in map '{0}' - Maps that are declared as global context cannot have LpOptional = 'Y'.", mapName), "");
            }

            if (this.MapContext == E_MapContext.Lender)
            {
                this.ClientCode = GetClientCode(ratesheetLocation);
            }
		}

        // OPM 27032
        // db - this is assuming these are the only locations where this will be run, which is ok for the
        // dd - 9/4/2015 - Assume a production environment unless there is a settings in config.
        private bool IsTestEnvironment()
        {
            string env = ConfigurationManager.AppSettings["RatesheetMapTestEnvironment"];

            if (string.IsNullOrEmpty(env))
            {
                return false;
            }

            return env == "true";
        }

		private string GetClientCode(string ratesheetLocation)
		{
			string name = System.IO.Path.GetFileNameWithoutExtension(ratesheetLocation);
			//Client Code comes after the last underscore
			int index = name.LastIndexOf("_");
			if(index < 0)
			{
				throw new CBaseException(String.Format("Unable to get client code from ratesheet name '{0}' - The client code is whatever comes after the last underscore, but there is no underscore in the ratesheet name.", System.IO.Path.GetFileName(ratesheetLocation)), "");
			}
			return name.Substring(index+1);
		}

		public bool HasMatchingProgramIds(string ratesheetId)
		{
            if (this.MapContext == E_MapContext.Global)
            {
                LogHelper.LogWarning("A call has been made to get the program ID in a map that is being processed under the global, not lender, context.  RatesheetId = " + ratesheetId);
                return false;
            }

            try
            {
                var list = CacheDatabaseData.GetProgramIds(this.ClientCode, ratesheetId);

                if (IsTestEnvironment())
                {
                    return true;
                }

                return list.Count() > 0;
            }
            catch (CBaseException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new CBaseException(String.Format("Unable to get program ID in ratesheet file '{0}' using Client Code '{1}' and RatesheetId '{2}'.", m_ratesheetLocation, this.ClientCode, ratesheetId), e);
            }

		}

		public ArrayList GetProgramIds(string ratesheetId)
		{
            ArrayList programIds = new ArrayList();

            if (this.MapContext == E_MapContext.Global)
            {
                LogHelper.LogWarning("A call has been made to get the program ID in a map that is being processed under the global, not lender, context.  RatesheetId = " + ratesheetId);
                return programIds;
            }

            try
            {

                IEnumerable<Guid> list = CacheDatabaseData.GetProgramIds(this.ClientCode, ratesheetId);
                foreach (var id in list)
                {
                    programIds.Add(id.ToString());
                }

                if (IsTestEnvironment())
                {
                    if (programIds.Count == 0)
                    {
                        for (int i = 0; i < 1; ++i)
                        {
                            programIds.Add("test program id " + i);
                        }
                    }
                }

                if (programIds.Count == 0)
                {
                    throw new CBaseException(String.Format("Unable to get program ID in ratesheet file '{0}' using Client Code '{1}' and RatesheetId '{2}'.", m_ratesheetLocation, this.ClientCode, ratesheetId), "");
                }

            }
            catch (CBaseException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new CBaseException(String.Format("Unable to get program ID in ratesheet file '{0}' using Client Code '{1}' and RatesheetId '{2}'.", m_ratesheetLocation, this.ClientCode, ratesheetId), e);
            }

            return programIds;
		}
	}
}