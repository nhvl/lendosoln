using System;
using System.Collections;
using System.Text;
using System.Threading;
using System.Text.RegularExpressions;
using DataAccess;
using System.IO;
using LendersOffice.Constants;

namespace RatesheetMap
{
	/// <summary>
	/// Container for all regions that are fragments of the same loan program
	/// </summary>
	public class RegionContainer
	{
		#region Variables
		private	SortedList	m_sortedList; //Sorted by region ID
		private	SortedList	m_sortedListByFilter; //Sorted by group filter
		private string		m_objectIdStr = "";
		private string		m_ratesheetName;
		private string		m_mapName;
		private string		m_tabName;
		private GroupTag	m_highestFilter;
		private bool		m_finished = false;
		private bool		m_writingAdjustTableOutput = false;
		private ArrayList	m_programIds;
		private ArrayList	m_pointerRegions;
		private MapTag		m_mapTag;

		public SortedList Regions
		{
			get { return m_sortedList; }
		}

		public bool ContainsOnlyPointerRegions
		{
			get { return (m_pointerRegions.Count > 0 && m_sortedList.Count == 0);}
		}
		#endregion

		public RegionContainer(Region region, string ratesheetName, string tabName, string mapName, MapTag mapTag)
		{
			m_ratesheetName = ratesheetName;
			m_tabName = tabName;
			m_mapName = mapName;
			m_sortedList = new SortedList();
			m_sortedListByFilter = new SortedList();
			m_highestFilter = null;
			m_programIds = new ArrayList();
			m_pointerRegions = new ArrayList();
			m_mapTag = mapTag;
			AddRegion(region);
		}

		public void AddRegion(Region region)
		{
			if(region == null)
				return;

			m_finished = false;

			if(region.IsPointerRegion)
			{
				//SrcLoRatesheetId attribute does not exist, and the source ratesheet ID is just put into the SrcLoProgramId attribute - should add a SrcLoRatesheetId attribute
				if(m_objectIdStr.Equals(""))
					m_objectIdStr = region.SrcLoProgramId;

				AddPointerRegion(region);
				return;
			}

			if(m_objectIdStr.Equals(""))
				m_objectIdStr = region.ObjectId;

			m_sortedList.Add(region.RegionId, region);
			
			if(region.RegionType == E_RegionType.AdjustTable)
			{
				if(m_pointerRegions.Count > 0) // This should never happen, as pointer regions are only inserted into RateOption containers
					throw new CBaseException(String.Format("Region '{0}' is an adjustment table region, but a pointer region has also declared the same object Id of '{1}'.  Pointer regions may only point to RateOption programs, not policies.", region.RegionId, region.ObjectId) ,"");
				
				m_writingAdjustTableOutput = true;
			}
		}

		private void AddPointerRegion(Region region)
		{
			// This should never happen, as pointer regions are only inserted into RateOption containers
			if(m_writingAdjustTableOutput == true)
				throw new CBaseException(String.Format("The pointer region '{0}' points to a policy (meaning, the region(s) that define data for the SrcProgramId are adjustment table regions). ", region.RegionId), "");
			
			if(region.ObjectType == E_LoObjType.LoRatesheetId)
				m_programIds.AddRange(m_mapTag.GetProgramIds(region.ObjectId));
			else
				m_programIds.Add(region.ObjectId);
			
			m_pointerRegions.Add(region);
		}

		private void InsertGroupedByFilter(Region region, GroupTag groupFilter)
		{
			try
			{
				if(m_highestFilter == null)
				{
					m_highestFilter = groupFilter;
					if(groupFilter == null)
						m_sortedListByFilter.Add(m_sortedListByFilter.Count, region.RegionId);
					else
						m_sortedListByFilter.Add(groupFilter.RangeFrom, region.RegionId);
				}
				else
				{
					if(groupFilter == null)
						throw new CBaseException(String.Format("Unable to stitch fragments together because they do not contain the same type of filters in ratesheet '{0}'.", m_ratesheetName), "");
					else if(m_highestFilter.AttributeType != groupFilter.AttributeType)
						throw new CBaseException(String.Format("Unable to stitch fragments together because they do not contain the same type of filters in ratesheet '{0}'.  Filter 1 Type = '{1}', Filter 2 Type = '{2}'.", m_ratesheetName, m_highestFilter.AttributeType, groupFilter.AttributeType), "");
					else
					{
						if(m_sortedListByFilter.ContainsKey(groupFilter.RangeFrom))
						{
							int insertIndex = m_sortedListByFilter.Count;
							while(m_sortedListByFilter.ContainsKey(insertIndex))
							{
								++insertIndex;
							}
							m_sortedListByFilter.Add(insertIndex, region.RegionId);
						}
						else
						{
							m_sortedListByFilter.Add(groupFilter.RangeFrom, region.RegionId);
						}
					}
				}
			}
			catch(Exception e)
			{
				throw new CBaseException(String.Format("Error while adding region '{0}' into its list of region fragments.  DETAILS: {1}", region.RegionId, e.Message), e.ToString());
			}
		}

		public int WriteDataToWorksheet1D(OutputWriter outputWriter, Variables variables, ref int row, ref ArrayList warningList, ref ArrayList errorList)
		{
            if(m_finished)
				return 0;

			// Check if the container contains only pointer regions, and no non-pointer regions that actually output data
			if((m_pointerRegions.Count > 0) && (m_sortedList.Count == 0))
			{
				StringBuilder sb = new StringBuilder();
				foreach(Region region in m_pointerRegions)
				{
					if(sb.Length != 0)
						sb.Append(", ");
					sb.Append(region.RegionId);
				}
				throw new CBaseException(String.Format("The following regions are pointer regions for program '{0}', but no valid non-pointer regions defined data for this program: {1}", m_objectIdStr, sb.ToString()), "");
			}

			int nNumSectionsOutput = 0;  //This could be greater than the # of regions if regions are getting output in multiple copies, such as with lender context
			int col = 1;
			m_finished = true;

			#region Pre-process regions
			foreach(string key in m_sortedList.Keys)
			{
				Region region = (Region)m_sortedList[key];
				
				try
				{
					if(region.HasError)
						return 0;
					region.FinalizeData(variables);
				}
				catch(CBaseException c)
				{
					region.HasError = true;
					string msg = String.Format("Error finalizing data in map '{0}', map tab '{3}', region '{1}' - {2}", m_mapName, region.RegionId, c, m_tabName );
					errorList.Add(msg);
				}
				catch(Exception e)
				{
					region.HasError = true;
					string msg = String.Format("Error finalizing data in map '{0}', map tab '{3}', region '{1}' - {2}", m_mapName, region.RegionId, e, m_tabName );
					errorList.Add(msg);
				}

				warningList.AddRange(region.WarningList);
			
				if(region.HasError)
					return 0;
			
				if(!region.CreateOutput)
					continue;

				if(region.IsFragment && m_sortedList.Keys.Count < 2)
				{
					warningList.Add(String.Format("Region '{0}' is declared as a fragment, but no other fragments have been found in the same worksheet or previous worksheets that match this region's object Id of '{1}'.  Therefore, no output will be written for this region until a matching fragment is found in another worksheet.", region.RegionId, region.ObjectId ));
					continue;
				}

				try
				{
					InsertGroupedByFilter(region, region.GetFirstGroupTag());
				}
				catch(CBaseException c)
				{
					throw c;
				}
			}
			#endregion

			if(m_sortedListByFilter.Count == 0)
				return 0;
			
			int loopNum = 1;  // There is always at least 1 program that needs to be output (more if there are pointer regions or this is a lender context map)
			
			if((m_mapTag.MapContext == E_MapContext.Lender) && (m_writingAdjustTableOutput == false))
			{
				m_programIds.AddRange(m_mapTag.GetProgramIds(m_objectIdStr));
				loopNum = m_programIds.Count;
			}
			else
			{
				loopNum += m_pointerRegions.Count;
			}

			bool firstRun = true; // indicates the first region being output for a particular program, which means we'll need to first output the program name and program ID to label the subsequent data
			#region Output data
			for(int i = 0; i < loopNum; ++i) //loop through all of the programs that need to output the same set of data
			{
				firstRun = true;
				col = 1;

                foreach(int key in m_sortedListByFilter.Keys)
				{
					string regionId = (string)m_sortedListByFilter[key];
					Region region = (Region)m_sortedList[regionId];
					
					if(!region.CreateOutput)
						return 0;
				
					if(firstRun)
					{
						firstRun = false;
						if(region.RegionType == E_RegionType.RateOption)
						{
							outputWriter.WriteCell(row, col, "Product Name");
							string productName = region.RegionId.ToUpper();
							
							if(i < m_pointerRegions.Count) // If we are printing data for a pointer region, we want to use the pointer region's ID as the program name
								productName = ((Region)m_pointerRegions[i]).RegionId.ToUpper();
							
							outputWriter.WriteCell(row++, col + 1, productName);
						
						    outputWriter.WriteCell(row, col, region.IdLabel); //ie: "Program Id" or "Policy"

						    if(m_mapTag.MapContext == E_MapContext.Lender)
							    outputWriter.WriteCell(row, col + 1, m_programIds[i].ToString());
						    else
						    {
							    if(i < m_pointerRegions.Count) // If we are printing data for a pointer region, we want to use the pointer region's program ID
								    outputWriter.WriteCell(row, col + 1, ((Region)m_pointerRegions[i]).ObjectId);
							    else
								    outputWriter.WriteCell(row, col + 1, region.ObjectId);
						    }
                        }
						
						if(region.RegionType == E_RegionType.AdjustTable)
						{
                            outputWriter.WriteCell(row, col, region.IdLabel); //ie: "Program Id" or "Policy"
                            // TODO - UNCOMMENT the following line when case 25872 is going to be released
                            // ALSO make the 2 lines after it say col + 2 and col + 3 respectively (instead of + 1 and + 2)
                            //outputWriter.WriteCell(row, col + 1, ""); // QBC - OPM 25872
							outputWriter.WriteCell(row, col + 1, region.RegionId.ToUpper()); 
                            outputWriter.WriteCell(row, col + 2, region.ObjectId);
						}
						++row;
                    } // end if(firstRun)

					row = region.WriteDataToWorksheet1D(outputWriter, row, new ArrayList());
					nNumSectionsOutput++;

                } // end foreach(int key in m_sortedListByFilter.Keys)
				
                ++row;
            
            } // end for(int i = 0; i < loopNum; ++i)
			#endregion
			return nNumSectionsOutput;
		}

		private bool FragmentsHaveOverlappingIO(GroupTag group1, GroupTag group2)
		{
			if(group1 == null || group2 == null)
				return false;
			else
			{
				if((group1.RangeFrom == group2.RangeFrom) && (group1.RangeTo == group2.RangeTo))
					return true;
				else
					return false;
			}
		}
	
		//NOTE: as of 4/7/08, this is no longer being updated and will become obsolete, so if 2D output is required, this
		//will need to be redone/updated.
		#region Obsolete Write 2D Output
		public int WriteDataToWorksheet2D(OutputWriter outputWriter, MapTag mapTag, Variables variables, int row, ref int currentRow, ref int col, ref ArrayList warningList, ref ArrayList errorList)
		{
            if(m_finished)
				return 0;

			m_finished = true;

			#region Pre-process regions
			foreach(string key in m_sortedList.Keys)
			{
				Region region = (Region)m_sortedList[key];
				
				try
				{
					if(region.HasError)
						return 0;
					region.FinalizeData(variables);
				}
				catch(CBaseException c)
				{
					region.HasError = true;
					string msg = String.Format("Error finalizing data in map '{0}', map tab '{3}', region '{1}' - {2}", m_mapName, region.RegionId, c, m_tabName );
					errorList.Add(msg);
				}
				catch(Exception e)
				{
					region.HasError = true;
					string msg = String.Format("Error finalizing data in map '{0}', map tab '{3}', region '{1}' - {2}", m_mapName, region.RegionId, e, m_tabName );
					errorList.Add(msg);
				}

				warningList.AddRange(region.WarningList);
			
				if(region.HasError)
					return 0;
			
				if(!region.CreateOutput)
					continue;

				if(region.IsFragment && m_sortedList.Keys.Count < 2)
				{
					warningList.Add(String.Format("Region '{0}' is declared as a fragment, but no other fragments have been found in the same worksheet or previous worksheets that match this region's object Id of '{1}'.  Therefore, no output will be written for this region until a matching fragment is found in another worksheet.", region.RegionId, region.ObjectId ));
					continue;
				}

				try
				{
					InsertGroupedByFilter(region, region.GetFirstGroupTag());
				}
				catch(CBaseException c)
				{
					throw c;
				}
			}
			#endregion

			bool firstRun = true;
			int loopNum = 1;
			if((mapTag.MapContext == E_MapContext.Lender) && (m_writingAdjustTableOutput == false))
			{
				m_programIds.AddRange(mapTag.GetProgramIds(m_objectIdStr));
				loopNum = m_programIds.Count;
			}
		
			int savedStartRow = 1;
			int maxEndRow = -1;
		
			int nNumSectionsOutput = 0;  //This could be greater than the # of regions if regions are getting output in multiple copies, such as with lender context
		
			#region Output data
			for(int i = 0; i < loopNum; ++i)
			{
				firstRun = true;
				bool hasIO = false;
				bool firstDupIOFragment = true;
				bool ignoreHasIO = false;
				bool ignoreNonIOHeaders = false;
				col = 1;
				GroupTag prevIOGroupTag = null;

				foreach(int key in m_sortedListByFilter.Keys)
				{
					string regionId = (string)m_sortedListByFilter[key];
					Region region = (Region)m_sortedList[regionId];
				
					if(!region.CreateOutput)
						return 0;
				
					if(firstRun)
					{
						if(region.RegionType == E_RegionType.RateOption)
						{
							outputWriter.WriteCell(row, col, "Product Name");
							outputWriter.WriteCell(row++, col + 1, region.RegionId.ToUpper());
						}
					
						outputWriter.WriteCell(row, col, region.IdLabel);

						if(mapTag.MapContext == E_MapContext.Lender)
							outputWriter.WriteCell(row, col + 1, m_programIds[i].ToString());
						else
							outputWriter.WriteCell(row, col + 1, region.ObjectId);

						if(region.RegionType == E_RegionType.AdjustTable)
						{
							outputWriter.WriteCell(row, col + 2, "Policy Name");
							outputWriter.WriteCell(row, col + 3, region.RegionId.ToUpper());
						}
					
						currentRow = row + 1;
						savedStartRow = currentRow;
						if(region.MaxGroupOperators > 2)
							ignoreHasIO = true;
						region.WriteDataToWorksheet2D(outputWriter, ref currentRow, ref col, firstRun, ref hasIO, ignoreHasIO, ignoreNonIOHeaders);
						
						if(hasIO)
							prevIOGroupTag = region.GetFirstGroupTagByType(E_OperatorAttributeType.IO);
						
						row = (hasIO || m_writingAdjustTableOutput)?currentRow:savedStartRow;

						nNumSectionsOutput++;
						firstRun = false;
					}
					else
					{
						GroupTag currentIOGroup = region.GetFirstGroupTagByType(E_OperatorAttributeType.IO);
						if(hasIO)
						{
							if(FragmentsHaveOverlappingIO(currentIOGroup, prevIOGroupTag))
							{
								ignoreHasIO = true;
								row = savedStartRow;
								if(firstDupIOFragment)
								{
									if(region.MaxGroupOperators <= 2)
										col += 6;
									firstDupIOFragment = false;
								}
							}
							else
							{
								ignoreNonIOHeaders = true;
								col = 1;
								row = currentRow;
								savedStartRow = row;
								firstDupIOFragment = true;
								ignoreHasIO = false;
								prevIOGroupTag = currentIOGroup;
							}
						}
						currentRow = row;
						region.WriteDataToWorksheet2D(outputWriter, ref currentRow, ref col, firstRun, ref hasIO, ignoreHasIO, ignoreNonIOHeaders);

						if(hasIO)
							prevIOGroupTag = region.GetFirstGroupTagByType(E_OperatorAttributeType.IO);
						
						row = (hasIO || m_writingAdjustTableOutput)?currentRow:savedStartRow;
					}
				
					if(maxEndRow < currentRow)
					{
						maxEndRow = currentRow;
					}
				}
			
				if(maxEndRow >= 0)
					currentRow = maxEndRow;

				if(m_sortedList.Keys.Count > 0)
				{
					if(m_writingAdjustTableOutput == false)
						++currentRow;
					row = currentRow;
				}
			}
			#endregion

			return nNumSectionsOutput;
		}
		#endregion
	}
}