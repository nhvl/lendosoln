using System;
using System.Collections;
using DataAccess;

namespace RatesheetMap
{
	public class RatesheetLandmarkCell : LandmarkCell
	{
		public RatesheetLandmarkCell(string regionId, RatesheetCellRep range, bool isAnchorLandmark, int anchorRow, int anchorCol) : base(regionId, E_LandmarkType.Ratesheet, range, isAnchorLandmark, anchorRow, anchorCol){}
		public override void ValidateInput(CellRep cell){}
		public override void ValidateInput(RatesheetCellRep cell){}
	}
}
