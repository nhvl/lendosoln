﻿Write-Output "Running Script";

cd C:\LendingQbNodeModules;
$updatedPackages = "";

function publish-node-modules ([string] $directory)
{
    pushd $directory;
    
    if(test-path package.json)
    {
        $installedVersion = (node -pe "require('./package.json').version");
        $packageName = (node -pe "require('./package.json').name");
        
        $repoVersionAddress = "$packageName@$installedVersion"
        
        $repoVersion = (npm info $repoVersionAddress version);
        
        
        Write-Host "$($packageName) ($installedVersion) vs ($repoVersion)";
        if($installedVersion -ne $repoVersion)
        {
            npm publish --access public --ignore-scripts;
            $updatedPackages = $updatedPackages + " " + $packageName + "\n";
        }
    }
    
        Get-ChildItem |? {$_.psiscontainer -and $_.name -ne ".bin"} | foreach {
            publish-node-modules $_.name;
        }
    
    popd;
} 


Get-ChildItem |? {$_.psiscontainer -and $_.name -ne ".bin" -and $_.name -ne ".git"} | foreach {publish-node-modules $_.name;}
Write-Host $updatedPackages;