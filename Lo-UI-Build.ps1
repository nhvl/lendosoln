﻿Param(
    [string]$BuildMode
)

npm config set registry -g "https://proget.meridianlink.com/npm/NPM/"
npm install typescript@2.6.2 -g # This has to be installed globally for the tsc path variable to exist.

Write-Output "LendersOfficeApp/inc/src"
pushd "LendersOfficeApp/inc/src"
$nodeModulesAlreadyExist = Test-Path ./node_modules -PathType Container
npm install
npm run webpack
if (!$nodeModulesAlreadyExist -and $BuildMode -eq 'Release')
{
    Remove-Item ./node_modules -recurse -force
}
popd
