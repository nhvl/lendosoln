﻿namespace PdfRasterizerLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Text;
    using System.Windows.Media.Imaging;
    using ExpertPdf.HtmlToPdf;
    using FileDB3;
    using iTextSharp.text.pdf;
    using TallComponents.PDF.Rasterizer;

    internal class PdfRasterizerImpl : MarshalByRefObject, IPdfRasterizer
    {
        private class _TimerHelper
        {
            public int Count { get; private set; }
            public string Name { get; private set; }
            private Stopwatch m_stopwatch = null;

            public _TimerHelper(string name)
            {
                this.Name = name;
                this.m_stopwatch = new Stopwatch();
                this.Count = 0;
            }

            public void Start()
            {
                this.Count++;
                m_stopwatch.Start();
            }

            public void Stop()
            {
                m_stopwatch.Stop();
            }

            public string GetDisplayString(long totalElapsed)
            {
                return string.Format("{0, -40} - Executed {1, -5} times in {2, -10} ms. ({3, 6}%)", 
                    this.Name, 
                    this.Count.ToString("#,#"), 
                    this.m_stopwatch.ElapsedMilliseconds.ToString("#,#"),
                    (totalElapsed > 0 ? (float)this.m_stopwatch.ElapsedMilliseconds / (float)totalElapsed * 100: 0).ToString("#.000"));
            }



        }
        private string m_sFileDBName; 
        private List<string> m_sFileDbToCleanUpOnError = new List<string>();
        private readonly object m_exceptionLock = new object();

        public PdfRasterizerImpl()
        {
        }

        #region IPdfRasterizer Members
        public string ConvertHtmlToPdfFile(string baseUrl, string htmlSourceFilePath, string licenseKey, PDFConversionOptions options)
        {
            PdfConverter.LoadHtmlConcurrencyLevel = 1;
            PdfConverter pdfConverter = new PdfConverter();

            PDFConversionOptionsConverter.UpdatePdfConverterFromPdfConversionOptions(pdfConverter, options, licenseKey);

            string tempFilePath = htmlSourceFilePath;
            FileInfo info = new FileInfo(tempFilePath);
            bool moveFileBack  = false;

            //if the fie does not end in HTML the process will crash.
            string htmlExtension = ".html";
            if (!info.Extension.Equals(htmlExtension, StringComparison.OrdinalIgnoreCase))
            {
                tempFilePath = tempFilePath + htmlExtension;
                info.MoveTo(tempFilePath);
                moveFileBack = true;
            }
            
            ExpertPdf.HtmlToPdf.PdfDocument.Document pdfDocument = null;

            try
            {
                using (FileStream fs = File.OpenRead(tempFilePath))
                {
                    pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlStream(fs, Encoding.UTF8, baseUrl, null);
                }

                if (!string.IsNullOrEmpty(options.LogoCheckId))
                {
                    foreach (HtmlElementMapping elementMapping in pdfConverter.HtmlElementsMappingOptions.HtmlElementsMappingResult)
                    {
                        if (elementMapping.HtmlElementId == options.LogoCheckId)
                        {
                            foreach (var r in elementMapping.PdfRectangles)
                            {
                                if (r.Rectangle.Height < 30 && r.Rectangle.Width < 30)
                                {
                                    Trace.WriteLine("ESCALATE - PDF logo is too small", "ERROR");
                                }
                            }
                        }
                    }

                }

                string tempFile = GetTempFile();
                pdfDocument.Save(tempFile);
                return tempFile;
            }
            finally
            {
                if (pdfDocument != null)
                {
                    pdfDocument.Close(); // Need to cleanup.
                }

                if (moveFileBack)
                {
                    File.Move(tempFilePath, htmlSourceFilePath);
                }
            }
        }

        /// <summary>
        /// Flattens the PDF. 
        /// </summary>
        /// <param name="pdfContent">The PDf to flatten.</param>
        /// <returns>The flattened PDF.</returns>
        private byte[] FlattenPdf(byte[] pdfContent)
        {
            byte[] flattenedPdf;
            using (MemoryStream stream = new MemoryStream())
            {
                PdfReader reader = new PdfReader(pdfContent);
                PdfStamper stamper = new PdfStamper(reader, stream) { FormFlattening = true };
                stamper.Close();
                flattenedPdf = stream.ToArray();
            }

            return flattenedPdf;
        }

        /// <summary>
        /// This method will return a list of png content base on pdfContent. It will return null if
        /// pdfContent is not PDF file or PDF is protect with password.
        /// </summary>
        /// <param name="pdfContent"></param>
        /// <returns></returns>
        public List<byte[]> ConvertToPng(byte[] pdfContent, float scale)
        {
            if (null == pdfContent)
            {
                return null;
            }

            List<byte[]> list = new List<byte[]>();
            try
            {
                pdfContent = this.FlattenPdf(pdfContent);

                using (MemoryStream stream = new MemoryStream(pdfContent))
                {
                    TallComponents.PDF.Rasterizer.Document pdfDocument = new TallComponents.PDF.Rasterizer.Document(stream);

                    for (int i = 0; i < pdfDocument.Pages.Count; i++)
                    {
                        TallComponents.PDF.Rasterizer.Page pdfPage = pdfDocument.Pages[i];

                        int pngWidth = (int)(scale * pdfPage.Width);
                        int pngHeight = (int)(scale * pdfPage.Height);
                        bool isGrayScale = true;
                        using (Bitmap bitmap = new Bitmap(pngWidth, pngHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb))
                        {
                            using (Graphics g = Graphics.FromImage(bitmap))
                            {
                                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                                g.ScaleTransform(scale, scale);
                                pdfPage.Draw(g);
                            }
                            using (MemoryStream bmpStream = new MemoryStream())
                            {
                                bitmap.Save(bmpStream, System.Drawing.Imaging.ImageFormat.Png);

                                isGrayScale = ImageProducer.IsGrayScale(bitmap);

                                bmpStream.Seek(0, SeekOrigin.Begin);

                                #region Convert 32RGB PNG to 4 bit gray scale to save space
                                BitmapSource bitmapSource = BitmapFrame.Create(bmpStream);

                                System.Windows.Media.PixelFormat pixelFormat = System.Windows.Media.PixelFormats.Gray4;

                                if (isGrayScale == false)
                                {
                                    pixelFormat = System.Windows.Media.PixelFormats.Bgr555;
                                }
                                FormatConvertedBitmap convertedBitmap = new FormatConvertedBitmap(bitmapSource,
                                    pixelFormat, null, 0);

                                PngBitmapEncoder pngEncoder = new PngBitmapEncoder();
                                pngEncoder.Interlace = PngInterlaceOption.Off;
                                pngEncoder.Frames.Add(BitmapFrame.Create(convertedBitmap));

                                #endregion

                                using (MemoryStream pngStream = new MemoryStream())
                                {
                                    pngEncoder.Save(pngStream);
                                    list.Add(pngStream.ToArray());
                                }
                            }

                        }
                    }

                }
            }
            catch (InvalidPdfException)
            {
                list = null; // 2/2/2010 dd - Content is not a PDF file.
            }
            catch (WrongPasswordException)
            {
                list = null;
            }
            catch (Exception exc)
            {
                PdfRasterizerFactory.WriteEventLog(System.Diagnostics.EventLogEntryType.Error, exc.ToString());
                list = null;
            }
            return list;
        }

        /// <summary>
        /// 2015-06-02 - Refactor so that only use TallsComponent API to convert the main page only.
        /// </summary>
        /// <param name="dbName"></param>
        /// <param name="path"></param>
        /// <param name="scales"></param>
        /// <param name="pageAndIds"></param>
        /// <returns></returns>
        private string ConvertToPndAndSaveToFileDBVersion2015(string dbName, string path, List<float> scales, List<Dictionary<int, string>> pageAndIds)
        {
            StringBuilder debugLog = new StringBuilder();

            try
            {

                Stopwatch sw = Stopwatch.StartNew();
                float fullScales = scales[0];
                float thumbScales = scales[1];

                int fullScaleIndex = 0;
                int thumbScaleIndex = 1;

                if (fullScales < thumbScales)
                {
                    // 6/2/2015 dd - Reverse to make sure full scales always larger then thumb.
                    float tmp = thumbScales;
                    thumbScales = fullScales;
                    fullScales = tmp;
                    fullScaleIndex = 1;
                    thumbScaleIndex = 0;
                }
                _TimerHelper constructPdfDocTimer = new _TimerHelper("TallsComponent.Document.ctor");
                _TimerHelper pdfLoadInMemoryTimer = new _TimerHelper("LoadPdfToMemory");
                _TimerHelper getPdfPageTimer = new _TimerHelper("TallsComponent.Document.Pages[idx]");
                _TimerHelper rasterizerTimer = new _TimerHelper("TallsComponent.Rasterizer");
                _TimerHelper resizeTimer = new _TimerHelper("Resize");
                _TimerHelper grayscaleDetectionTimer = new _TimerHelper("IsGrayScale Check");
                _TimerHelper finalSavePngTimer = new _TimerHelper("FinalSavePng");
                _TimerHelper putFileDBTimer = new _TimerHelper("FileDB.Put");

                _TimerHelper[] timerList = {
                                           pdfLoadInMemoryTimer,
                                           constructPdfDocTimer,
                                           getPdfPageTimer,
                                         rasterizerTimer,
                                         resizeTimer,
                                         grayscaleDetectionTimer,
                                         finalSavePngTimer,
                                         putFileDBTimer
                                     };
                pdfLoadInMemoryTimer.Start();
                byte[] file = File.ReadAllBytes(path);
                pdfLoadInMemoryTimer.Stop();
                int numberOfPages = 0;
                long fileDbPutBytes = 0;
                file = this.FlattenPdf(file);

                using (MemoryStream stream = new MemoryStream(file))
                {
                    constructPdfDocTimer.Start();
                    Document document = new Document(stream);
                    constructPdfDocTimer.Stop();

                    Dictionary<int, string> fullPageAndScaleSet = pageAndIds[fullScaleIndex];
                    Dictionary<int, string> thumbPageAndScaleSet = pageAndIds[thumbScaleIndex];
                    numberOfPages = fullPageAndScaleSet.Count;

                    foreach (KeyValuePair<int, string> entry in fullPageAndScaleSet)
                    {
                        getPdfPageTimer.Start();
                        Page pdfPage = document.Pages[entry.Key];
                        getPdfPageTimer.Stop();

                        int pdfWidth = (int)pdfPage.Width;

                        int pngFullWidth = (int)(fullScales * pdfPage.Width);
                        int pngFullHeight = (int)(fullScales * pdfPage.Height);

                        int pngThumbWidth = (int)(thumbScales * pdfPage.Width);

                        string tempFullPngPath = GetTempFile();

                        // 6/2/2015 dd - Step 1 Generate Full Size Image.
                        rasterizerTimer.Start();
                        using (Stream bmpStream = File.OpenWrite(tempFullPngPath))
                        {
                            using (Bitmap bitmap = new Bitmap(pngFullWidth, pngFullHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb))
                            {
                                using (Graphics g = Graphics.FromImage(bitmap))
                                {
                                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                                    g.ScaleTransform(fullScales, fullScales);
                                    pdfPage.Draw(g);
                                }

                                bitmap.Save(bmpStream, ImageFormat.Png);
                            }
                        }
                        rasterizerTimer.Stop();

                        // 6/2/2015 dd - Step 2 - Generate Thumbnail using WPF.
                        string tempThumbPngPath = GetTempFile();
                        resizeTimer.Start();
                        using (Stream fullImageStream = new FileStream(tempFullPngPath, FileMode.Open))
                        {
                            BitmapImage bi = new BitmapImage();
                            bi.BeginInit();
                            bi.StreamSource = fullImageStream;
                            bi.DecodePixelWidth = pngThumbWidth; // The height will automatically scale to match.
                            bi.EndInit();

                            using (Stream thumbStream = new FileStream(tempThumbPngPath, FileMode.Create))
                            {
                                PngBitmapEncoder pngEncoder = new PngBitmapEncoder();
                                pngEncoder.Interlace = PngInterlaceOption.Off;
                                pngEncoder.Frames.Add(BitmapFrame.Create(bi));
                                pngEncoder.Save(thumbStream);
                            }
                        }
                        resizeTimer.Stop();

                        // 6/2/2015 dd - Step 3 - Detect thumbnail if it is grayscale.
                        bool isGrayScale = false;
                        grayscaleDetectionTimer.Start();
                        using (Bitmap bitmap = (Bitmap)Bitmap.FromFile(tempThumbPngPath))
                        {
                            isGrayScale = ImageProducer.IsGrayScale(bitmap);
                        }
                        grayscaleDetectionTimer.Stop();

                        System.Windows.Media.PixelFormat pixelFormat = System.Windows.Media.PixelFormats.Gray4;
                        if (isGrayScale == false)
                        {
                            pixelFormat = System.Windows.Media.PixelFormats.Bgr555;
                        }
                        FileDB fileDB = new FileDB();

                        for (int i = 0; i < 2; i++)
                        {
                            string dbKey = string.Empty;
                            string pngPath = string.Empty;

                            if (i == 0)
                            {
                                // Full Image
                                dbKey = entry.Value;
                                pngPath = tempFullPngPath;
                            }
                            else if (i == 1)
                            {
                                // THumb image
                                dbKey = thumbPageAndScaleSet[entry.Key]; // Get DB key of thumb.
                                pngPath = tempThumbPngPath;
                            }
                            else
                            {
                                throw new NotImplementedException("There is a programming error.");
                            }

                            using (FileHandle fh = fileDB.NewFileHandle())
                            {
                                finalSavePngTimer.Start();
                                using (Stream imageStream = new FileStream(pngPath, FileMode.Open))
                                {
                                    BitmapImage bi = new BitmapImage();
                                    bi.BeginInit();
                                    bi.StreamSource = imageStream;
                                    bi.EndInit();

                                    FormatConvertedBitmap convertedBitmap = new FormatConvertedBitmap(bi, pixelFormat, null, 0);

                                    using (FileStream outImageStream = File.OpenWrite(fh.LocalFileName))
                                    {
                                        PngBitmapEncoder pngEncoder = new PngBitmapEncoder();
                                        pngEncoder.Interlace = PngInterlaceOption.Off;
                                        pngEncoder.Frames.Add(BitmapFrame.Create(convertedBitmap));
                                        pngEncoder.Save(outImageStream);
                                    }
                                }
                                finalSavePngTimer.Stop();

                                putFileDBTimer.Start();

                                fileDB.PutFile(dbName, dbKey, fh);

                                try
                                {
                                    FileInfo fi = new FileInfo(fh.LocalFileName);
                                    fileDbPutBytes += fi.Length;
                                }
                                catch (IOException)
                                {
                                }

                                putFileDBTimer.Stop();
                            }
                        }

                    }

                }
                sw.Stop();
                debugLog.AppendLine("[PdfRasterizer]. # pages:" + numberOfPages + ". Executed in " + sw.ElapsedMilliseconds.ToString("#,#") + "ms, FullScale=" + fullScales + ", thumb scales=" + thumbScales);

                foreach (var o in timerList)
                {
                    debugLog.AppendLine("    " + o.GetDisplayString(sw.ElapsedMilliseconds));
                }

                debugLog.AppendLine();
                debugLog.AppendLine("FileDB.Put Bytes=" + fileDbPutBytes.ToString("#,0"));
                Trace.Write(debugLog.ToString(), "PdfRasterizer");
            }
            catch (Exception e)
            {
                Trace.Write(e, "PdfRasterizer");
                Trace.Write(debugLog.ToString(), "PdfRasterizer");
                throw;
            }
            return string.Empty;
        }

        public string ConvertToPngAndSaveToFileDBFromFileDB(string dbName, string pdfFileKey, List<float> scales, List<Dictionary<int, string>> pageAndIDs)
        {
            FileDB fileDB = new FileDB();
            using (FileHandle inputFileHandle = fileDB.GetFile(dbName, pdfFileKey))
            {
                return ConvertToPngAndSaveToFileDBFromFile(dbName, inputFileHandle.LocalFileName, scales, pageAndIDs);
            }
        }

        public string ConvertToPngAndSaveToFileDBFromFile(string dbName, string path, List<float> scales, List<Dictionary<int, string>> pageAndIds)
        {
            if (scales.Count != pageAndIds.Count || (scales.Count != 2 && pageAndIds[0].Count != pageAndIds[1].Count))
            {
                throw new Win32Exception("Scales count doesnt match page id count");
            }

            m_sFileDBName = dbName;

            return ConvertToPndAndSaveToFileDBVersion2015(dbName, path, scales, pageAndIds);
        }
             
        #endregion

        private string GetTempFile()
        {
            string directory = "C:\\LOTEMP";
            return string.Format("{0}/{1:N}.pdf", directory, Guid.NewGuid());  
        }
    }
}