﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace PdfRasterizerLib
{
    internal class ImageProducer
    {
        /// <summary>
        /// This method will determine if the Bitmap data is grayscale or color.
        /// Image is grayscale if every pixel R=G=B.
        /// </summary>
        /// <param name="bitmap"></param>
        /// <returns></returns>
        unsafe public static bool IsGrayScale(Bitmap bitmap)
        {
            if (null == bitmap)
            {
                throw new ArgumentNullException("bitmap");
            }
            if (bitmap.PixelFormat != System.Drawing.Imaging.PixelFormat.Format24bppRgb)
            {
                throw new Exception("Only support Format24bppRgb");
            }
            int width = bitmap.Width;
            int height = bitmap.Height;
            BitmapData bitmapData = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, bitmap.PixelFormat);

            try
            {
                // 4/11/2011 dd - The code that travel through bitmap data is taken from http://www.bobpowell.net/lockingbits.htm
                for (int y = 0; y < height; y++)
                {
                    byte* row = (byte*)bitmapData.Scan0 + y * bitmapData.Stride;
                    for (int x = 0; x < width; x++)
                    {
                        int offset = x * 3;
                        byte b = row[offset]; // blue
                        byte g = row[offset + 1]; //green
                        byte r = row[offset + 2]; //red
                        if (b != g || g != r || b != r)
                        {
                            return false;
                        }
                    }
                }
            }
            finally
            {
                bitmap.UnlockBits(bitmapData);
            }
            return true;

        }
    }
}