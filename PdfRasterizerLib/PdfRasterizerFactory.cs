﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Diagnostics;
using System.ServiceProcess;

namespace PdfRasterizerLib
{
    /// <summary>
    /// The only reason we need to have the Factory class is because in order for PdfRasterizer component to work in full
    /// version, the identity running the code must belong to specific domain ('MIDDLEEARTH'). Therefore in the production
    /// environment we need to instantiate IPdfRasterizer using .NET remoting. On developing machine we can just instantiate
    /// the object directly.
    /// </summary>
    public static class PdfRasterizerFactory
    {
        /// <summary>
        /// host - empty string or null - will not use .net remoting
        /// host - non empty - will use remoting. It will make connection to tcp://{host}:{port}/{friendlyname}.
        ///      {port} and {friendlyname} are define in constants.
        /// </summary>
        /// <param name="host"></param>
        /// <returns></returns>
        public static IPdfRasterizer Create(string host)
        {
            IPdfRasterizer rasterizer = null;

            if (string.IsNullOrEmpty(host))
            {
                rasterizer = new PdfRasterizerImpl();
            }
            else
            {
                string url = string.Format("tcp://{0}:{1}/{2}", host, Constants.PortNumber, Constants.ServiceFriendlyName);
                rasterizer = (IPdfRasterizer)Activator.GetObject(typeof(IPdfRasterizer), url);
            }

            return rasterizer;
        }

        public static IPdfRasterizer Restart(string host)
        {
            if (!string.IsNullOrEmpty(host))
            {
                ServiceController controller = new ServiceController("PdfRasterizer", host);
                if (controller.Status == ServiceControllerStatus.Running)
                {
                    controller.Stop();
                    controller.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(10));
                }
                controller.Start();
                controller.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(10));
            }

            return Create(host);
        }


        /// <summary>
        /// Checks to see if the service is running. Will return true all the time if there was no host specified.
        /// </summary>
        /// <param name="host">The host to contact for the service.</param>
        /// <returns>True if the host is not set or if the service is running at the host.</returns>
        public static bool IsRunning(string host)
        {
            if (string.IsNullOrEmpty(host))
            {
                return true;
            }

            ServiceController controller = new ServiceController("PdfRasterizer", host);

            try
            {
                switch (controller.Status)
                {
                    case ServiceControllerStatus.StartPending:
                    case ServiceControllerStatus.Running:
                    case ServiceControllerStatus.ContinuePending:
                        return true;
                    case ServiceControllerStatus.Stopped:
                    case ServiceControllerStatus.StopPending:
                    case ServiceControllerStatus.Paused:
                    case ServiceControllerStatus.PausePending:
                    default:
                        return false;
                }
            }
            catch (InvalidOperationException)
            {
                return false;
            }
        }

        /// <summary>
        /// This method will create a TcpChannel that will listen for the remoting request. Only invoke this method if you 
        /// are inside Window Service start up code.
        /// </summary>
        /// <returns></returns>
        public static IChannel RegisterService()
        {
            IChannel channel = new TcpChannel(Constants.PortNumber);
            ChannelServices.RegisterChannel(channel, false);

            RemotingConfiguration.RegisterWellKnownServiceType(typeof(PdfRasterizerImpl), Constants.ServiceFriendlyName, WellKnownObjectMode.SingleCall);
            WriteEventLog(EventLogEntryType.Information, "PdfRasterizerFactor.RegisterService successfully");
            return channel;

        }

        public static void WriteEventLog(EventLogEntryType type, string msg)
        {
            string sourceName = "PdfRasterizer";
            if (EventLog.SourceExists(sourceName) == false)
            {
                EventLog.CreateEventSource(sourceName, "Application");
            }
            EventLog.WriteEntry(sourceName, msg, type);
        }
        
    }
}
