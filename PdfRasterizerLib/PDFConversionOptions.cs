﻿using System;

namespace PdfRasterizerLib
{
    [Serializable]
    public class PDFConversionOptions
    {
        public int? BottomMargin { get; set; }
        public int? LeftMargin { get; set; }
        public int? RightMargin { get; set; }
        public int? TopMargin { get; set; }
        public int? PageWidth { get; set; }
        public int? PageSize { get; set; }
        public int? PageOrientation { get; set; }
        public string LogoCheckId { get; set; }
        public string OwnerPassword { get; set; }
        public string UserPassword { get; set; }

        public bool? ShowFooter { get; set; }
        public bool? ShowPageNumber { get; set; }
        public string FooterText { get; set; }
        public int? FooterTextFontSize { get; set; }
        public string PageNumberText { get; set; }
        public bool? DrawFooterLine { get; set; }

        public bool? ShowHeader { get; set; }
        public bool? DrawHeaderLine { get; set; }
        public string HeaderText { get; set; }
        public int? HeaderTextAlign { get; set; }
        public int? HeaderTextFontSize { get; set; }
        public int? HeaderHeight { get; set; }
        public bool? ShowHeaderOnFirstPage { get; set; }
        public bool? ShowHeaderOnOddPages { get; set; }
        public bool? ShowHeaderOnEvenPages { get; set; }
    }
}
