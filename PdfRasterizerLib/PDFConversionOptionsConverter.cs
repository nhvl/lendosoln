﻿namespace PdfRasterizerLib
{
    using System;
    using ExpertPdf.HtmlToPdf;

    public static class PDFConversionOptionsConverter
    {
        public static void UpdatePdfConverterFromPdfConversionOptions(PdfConverter pdfConverter, PDFConversionOptions options, string licenseKey)
        {
            SetValue(licenseKey, (x) => pdfConverter.LicenseKey = x);
            SetValue(options.BottomMargin, (x) => pdfConverter.PdfDocumentOptions.BottomMargin = x);
            SetValue(options.LeftMargin, (x) => pdfConverter.PdfDocumentOptions.LeftMargin = x);
            SetValue(options.RightMargin, (x) => pdfConverter.PdfDocumentOptions.RightMargin = x);
            SetValue(options.TopMargin, (x) => pdfConverter.PdfDocumentOptions.TopMargin = x);
            SetValue(options.ShowFooter, (x) => pdfConverter.PdfDocumentOptions.ShowFooter = x);
            SetValue(options.ShowPageNumber, (x) => pdfConverter.PdfFooterOptions.ShowPageNumber = x);
            SetValue(options.PageWidth, (x) => pdfConverter.PageWidth = x);
            SetValue(options.PageSize, (x) => pdfConverter.PdfDocumentOptions.PdfPageSize = (PdfPageSize)x);
            SetValue(options.PageOrientation, (x) => pdfConverter.PdfDocumentOptions.PdfPageOrientation = (PDFPageOrientation)x);
            SetValue(options.FooterText, (x) => pdfConverter.PdfFooterOptions.FooterText = x);
            SetValue(options.PageNumberText, (x) => pdfConverter.PdfFooterOptions.PageNumberText = x);
            SetValue(options.OwnerPassword, (x) => pdfConverter.PdfSecurityOptions.OwnerPassword = x);
            SetValue(options.UserPassword, (x) => pdfConverter.PdfSecurityOptions.UserPassword = x);

            if (!string.IsNullOrEmpty(options.LogoCheckId))
            {
                pdfConverter.HtmlElementsMappingOptions.HtmlElementIds = new string[] { options.LogoCheckId };
            }

            SetValue(options.ShowHeader, (x) => pdfConverter.PdfDocumentOptions.ShowHeader = x);
            SetValue(options.DrawHeaderLine, (x) => pdfConverter.PdfHeaderOptions.DrawHeaderLine = x);
            SetValue(options.HeaderText, (x) => pdfConverter.PdfHeaderOptions.HeaderText = x);
            SetValue(options.HeaderTextFontSize, (x) => pdfConverter.PdfHeaderOptions.HeaderTextFontSize = x);
            SetValue(options.HeaderTextAlign, (x) => pdfConverter.PdfHeaderOptions.HeaderTextAlign = (HorizontalTextAlign)x);
            SetValue(options.HeaderHeight, (x) => pdfConverter.PdfHeaderOptions.HeaderHeight = x);
            SetValue(options.ShowHeaderOnFirstPage, (x) => pdfConverter.PdfHeaderOptions.ShowOnFirstPage = x);
            SetValue(options.ShowHeaderOnEvenPages, (x) => pdfConverter.PdfHeaderOptions.ShowOnEvenPages = x);
            SetValue(options.ShowHeaderOnOddPages, (x) => pdfConverter.PdfHeaderOptions.ShowOnOddPages = x);

            SetValue(options.FooterTextFontSize, (x) => pdfConverter.PdfFooterOptions.FooterTextFontSize = x);
            SetValue(options.DrawFooterLine, (x) => pdfConverter.PdfFooterOptions.DrawFooterLine = x);
        }

        private static void SetValue<T>(Nullable<T> v, Action<T> setter) where T : struct
        {
            if (v.HasValue)
            {
                setter(v.Value);
            }
        }

        private static void SetValue(string v, Action<string> setter)
        {
            if (string.IsNullOrEmpty(v))
            {
                return;
            }

            setter(v);
        }
    }
}