﻿// <copyright file="PageMetadata.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is PageMetadata class.</summary>
// <author>david</author>
// <date>$date$</author>
namespace PdfRasterizerLib
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Newtonsoft.Json;

    /// <summary>
    /// A metadata description describe the page.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class PageMetadata
    {
        /// <summary>
        /// Gets or sets the key of the original PDF.
        /// </summary>
        /// <value>Key of the original PDF.</value>
        [JsonProperty("ref_doc_id")]
        public Guid ReferenceDocumentId { get; set; }

        /// <summary>
        /// Gets or sets the 0-indexed page number of the original PDF.
        /// </summary>
        /// <value>The 0-indexed page number of the original PDF.</value>
        [JsonProperty("ref_doc_pg")]
        public int ReferenceDocumentPageNumber { get; set; }

        /// <summary>
        /// Gets or sets the rotation clockwise degree. Only 0, 90, 180, 270 are valid values.
        /// </summary>
        /// <value>Rotation clockwise degree.</value>
        [JsonProperty("rotation")]
        public int Rotation { get; set; }

        /// <summary>
        /// Gets or sets the width in pixel of PDF page.
        /// </summary>
        /// <value>Width in pixel of PDF page.</value>
        [JsonProperty("pdf_width")]
        public int PdfWidth { get; set; }

        /// <summary>
        /// Gets or sets the height in pixel of PDF page.
        /// </summary>
        /// <value>Height in pixel of PDF page.</value>
        [JsonProperty("pdf_height")]
        public int PdfHeight { get; set; }

        /// <summary>
        /// Gets or sets the width in pixel of full size PNG image.
        /// </summary>
        /// <value>Width in pixel of full size PNG image.</value>
        [JsonProperty("png_full_width")]
        public int PngFullWidth { get; set; }

        /// <summary>
        /// Gets or sets the height in pixel of full size PNG image.
        /// </summary>
        /// <value>Height in pixel of full size PNG image.</value>
        [JsonProperty("png_full_height")]
        public int PngFullHeight { get; set; }

        [JsonProperty("png_full_bytes")]
        public long PngFullSize { get; set; }

        /// <summary>
        /// Gets or sets the width in pixel of thumb size PNG image.
        /// </summary>
        /// <value>Width in pixel of thumb size PNG image.</value>
        [JsonProperty("png_thumb_width")]
        public int PngThumbWidth { get; set; }

        /// <summary>
        /// Gets or sets the height in pixel of thumb size PNG image.
        /// </summary>
        /// <value>Height in pixel of thumb size PNG image.</value>
        [JsonProperty("png_thumb_height")]
        public int PngThumbHeight { get; set; }

        [JsonProperty("png_thumb_bytes")]
        public long PngThumbSize { get; set; }
    }

}
