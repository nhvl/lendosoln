﻿// <copyright file="DocumentMetadata.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is DocumentMetadata class.</summary>
// <author>david</author>
// <date>$date$</author>
namespace PdfRasterizerLib
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
using Newtonsoft.Json;

    /// <summary></summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class DocumentMetadata
    {
        [JsonProperty("pages")]
        private List<PageMetadata> pages = null;

        [JsonProperty("debug_info")]
        public string DebugInfo { get; set; }

        [JsonProperty("pdf_bytes")]
        public long PdfSize { get; set; }

        [JsonProperty("id")]
        public Guid DocumentId { get; set; }

        public long PngFullSize
        {
            get
            {
                return pages.Sum(o => o.PngFullSize);
            }
        }

        public long PngThumbSize
        {
            get
            {
                return pages.Sum(o => o.PngThumbSize);
            }
        }

        public IEnumerable<PageMetadata> Pages
        {
            get { return this.pages; }
        }

        public DocumentMetadata()
        {
            this.pages = new List<PageMetadata>();
        }

        public void AddPage(PageMetadata page)
        {
            this.pages.Add(page);
        }
    }
}
