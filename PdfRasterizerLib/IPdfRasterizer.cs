﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PdfRasterizerLib
{
    public interface IPdfRasterizer
    {
        List<byte[]> ConvertToPng(byte[] pdf, float scale);
        string ConvertToPngAndSaveToFileDBFromFileDB(string dbName, string pdfFileKey, List<float> scales, List<Dictionary<int, string>> pageAndIDs);
        string ConvertToPngAndSaveToFileDBFromFile(string dbName, string path, List<float> scales, List<Dictionary<int, string>> pageAndIds);
        string ConvertHtmlToPdfFile(string baseUrl, string htmlSourceFilePath, string licenseKey, PDFConversionOptions options);
    }
}
