﻿// <copyright file="PdfRasterizerV2.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is PdfRasterizerV2 class.
//    Date: 6/19/2015
// </summary>
// <author>david</author>
namespace PdfRasterizerLib
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows.Media.Imaging;
    using FileDB3;
    using TallComponents.PDF.Rasterizer;

    /// <summary>
    /// Use in convert PDF to PNG.
    /// </summary>
    public static class PdfRasterizerV2
    {
        private class _TimerHelper
        {
            public int Count { get; private set; }
            public string Name { get; private set; }
            private Stopwatch m_stopwatch = null;

            public _TimerHelper(string name)
            {
                this.Name = name;
                this.m_stopwatch = new Stopwatch();
                this.Count = 0;
            }

            public void Start()
            {
                this.Count++;
                m_stopwatch.Start();
            }

            public void Stop()
            {
                m_stopwatch.Stop();
            }

            public string GetDisplayString(long totalElapsed)
            {
                return string.Format("{0, -40} - Executed {1, -5} times in {2, -10} ms. ({3, 6}%)",
                    this.Name,
                    this.Count.ToString("#,#"),
                    this.m_stopwatch.ElapsedMilliseconds.ToString("#,#"),
                    (totalElapsed > 0 ? (float)this.m_stopwatch.ElapsedMilliseconds / (float)totalElapsed * 100 : 0).ToString("#.000"));
            }

        }

        private static string GetThumbName(string pdfFileDbKey, int pg)
        {
            return pdfFileDbKey + "_" + pg + "_thumb";
        }

        private static string GetFullName(string pdfFileDbKey, int pg)
        {
            return pdfFileDbKey + "_" + pg + "_full";
        }

        private static long GetFileSize(string path)
        {
            FileInfo fi = new FileInfo(path);
            return fi.Length;
        }

        public static DocumentMetadata ConvertToPng(bool isEnforceLicense, string dbName, string pdfFileDbKey, float fullScale, float thumbScale)
        {
            if (isEnforceLicense)
            {
                // 6/19/2015 dd - Throw exception when PdfRasterizer component did not have proper license setup.
                if (TallComponents.Licensing.License.IsValid() == false)
                {
                    throw new Exception("TallsComponents.PdfRasterizer does not have proper license. Make sure valid key is in 'PDFRasterizer.NET 3.0 Windows Domain Key' of the config file");
                }
            }

            if (fullScale < thumbScale)
            {
                throw new ArgumentException("FullScale=" + fullScale + " must be greater than thumbScale=" + thumbScale);
            }

            StringBuilder debugLog = new StringBuilder();

            FileDB fileDB = new FileDB();
            
            DocumentMetadata documentMetadata = new DocumentMetadata();
            documentMetadata.DocumentId = new Guid(pdfFileDbKey);
            try
            {

                Stopwatch sw = Stopwatch.StartNew();

                _TimerHelper constructPdfDocTimer = new _TimerHelper("TallsComponent.Document.ctor");
                _TimerHelper pdfLoadInMemoryTimer = new _TimerHelper("FileDB.Get");
                _TimerHelper getPdfPageTimer = new _TimerHelper("TallsComponent.Document.Pages[idx]");
                _TimerHelper rasterizerTimer = new _TimerHelper("TallsComponent.Rasterizer");
                _TimerHelper resizeTimer = new _TimerHelper("Resize");
                _TimerHelper grayscaleDetectionTimer = new _TimerHelper("IsGrayScale Check");
                _TimerHelper finalSavePngTimer = new _TimerHelper("FinalSavePng");
                _TimerHelper putFileDBTimer = new _TimerHelper("FileDB.Put");

                _TimerHelper[] timerList = {
                                           pdfLoadInMemoryTimer,
                                           constructPdfDocTimer,
                                           getPdfPageTimer,
                                         rasterizerTimer,
                                         resizeTimer,
                                         grayscaleDetectionTimer,
                                         finalSavePngTimer,
                                         putFileDBTimer
                                     };
                pdfLoadInMemoryTimer.Start();

                int numberOfPages = 0;

                using (FileHandle originalPdfFileHandle = fileDB.GetFile(dbName, pdfFileDbKey))
                {
                    pdfLoadInMemoryTimer.Stop();
                    
                    documentMetadata.PdfSize = GetFileSize(originalPdfFileHandle.LocalFileName);

                    using (FileStream stream = File.OpenRead(originalPdfFileHandle.LocalFileName))
                    {
                        constructPdfDocTimer.Start();
                        Document document = new Document(stream);
                        constructPdfDocTimer.Stop();

                        numberOfPages = document.Pages.Count;

                        for (int pg = 0; pg < numberOfPages; pg++)
                        {
                            getPdfPageTimer.Start();
                            
                            Page pdfPage = document.Pages[pg];

                            getPdfPageTimer.Stop();

                            PageMetadata pageMetadata = new PageMetadata();
                            pageMetadata.ReferenceDocumentPageNumber = pg;
                            pageMetadata.ReferenceDocumentId = documentMetadata.DocumentId;

                            documentMetadata.AddPage(pageMetadata);

                            int pdfWidth = (int)pdfPage.Width;


                            int pngFullWidth = (int)(fullScale * pdfPage.Width);
                            int pngFullHeight = (int)(fullScale * pdfPage.Height);

                            int pngThumbWidth = (int)(thumbScale * pdfPage.Width);
                            int pngThumbHeight = (int)(thumbScale * pdfPage.Height);

                            pageMetadata.PdfWidth = (int)pdfPage.Width;
                            pageMetadata.PdfHeight = (int)pdfPage.Height;

                            pageMetadata.PngFullWidth = pngFullWidth;
                            pageMetadata.PngFullHeight = pngFullHeight;

                            pageMetadata.PngThumbWidth = pngThumbWidth;
                            pageMetadata.PngThumbHeight = pngThumbHeight;

                            string tempFullPngPath = GetTempFile();

                            // 6/2/2015 dd - Step 1 Generate Full Size Image.
                            rasterizerTimer.Start();
                            using (Stream bmpStream = File.OpenWrite(tempFullPngPath))
                            {
                                using (Bitmap bitmap = new Bitmap(pngFullWidth, pngFullHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb))
                                {
                                    using (Graphics g = Graphics.FromImage(bitmap))
                                    {
                                        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                                        g.ScaleTransform(fullScale, fullScale);
                                        pdfPage.Draw(g);
                                    }

                                    bitmap.Save(bmpStream, ImageFormat.Png);
                                }
                            }
                            rasterizerTimer.Stop();

                            // 6/2/2015 dd - Step 2 - Generate Thumbnail using WPF.
                            string tempThumbPngPath = GetTempFile();
                            resizeTimer.Start();
                            using (Stream fullImageStream = new FileStream(tempFullPngPath, FileMode.Open))
                            {
                                BitmapImage bi = new BitmapImage();
                                bi.BeginInit();
                                bi.StreamSource = fullImageStream;
                                bi.DecodePixelWidth = pngThumbWidth; // The height will automatically scale to match.
                                bi.EndInit();

                                using (Stream thumbStream = new FileStream(tempThumbPngPath, FileMode.Create))
                                {
                                    PngBitmapEncoder pngEncoder = new PngBitmapEncoder();
                                    pngEncoder.Interlace = PngInterlaceOption.Off;
                                    pngEncoder.Frames.Add(BitmapFrame.Create(bi));
                                    pngEncoder.Save(thumbStream);
                                }
                            }
                            resizeTimer.Stop();

                            // 6/2/2015 dd - Step 3 - Detect thumbnail if it is grayscale.
                            bool isGrayScale = false;
                            grayscaleDetectionTimer.Start();
                            using (Bitmap bitmap = (Bitmap)Bitmap.FromFile(tempThumbPngPath))
                            {
                                isGrayScale = ImageProducer.IsGrayScale(bitmap);
                            }
                            grayscaleDetectionTimer.Stop();

                            System.Windows.Media.PixelFormat pixelFormat = System.Windows.Media.PixelFormats.Gray4;
                            if (isGrayScale == false)
                            {
                                pixelFormat = System.Windows.Media.PixelFormats.Bgr555;
                            }

                            for (int i = 0; i < 2; i++)
                            {
                                string dbKey = string.Empty;
                                string pngPath = string.Empty;

                                if (i == 0)
                                {
                                    // Full Image
                                    dbKey = GetFullName(pdfFileDbKey, pg);
                                    pngPath = tempFullPngPath;
                                }
                                else if (i == 1)
                                {
                                    // THumb image
                                    dbKey = GetThumbName(pdfFileDbKey, pg);
                                    pngPath = tempThumbPngPath;
                                }
                                else
                                {
                                    throw new NotImplementedException("There is a programming error.");
                                }

                                using (FileHandle fh = fileDB.NewFileHandle())
                                {
                                    finalSavePngTimer.Start();
                                    using (Stream imageStream = new FileStream(pngPath, FileMode.Open))
                                    {
                                        BitmapImage bi = new BitmapImage();
                                        bi.BeginInit();
                                        bi.StreamSource = imageStream;
                                        bi.EndInit();

                                        FormatConvertedBitmap convertedBitmap = new FormatConvertedBitmap(bi, pixelFormat, null, 0);

                                        using (FileStream outImageStream = File.OpenWrite(fh.LocalFileName))
                                        {
                                            PngBitmapEncoder pngEncoder = new PngBitmapEncoder();
                                            pngEncoder.Interlace = PngInterlaceOption.Off;
                                            pngEncoder.Frames.Add(BitmapFrame.Create(convertedBitmap));
                                            pngEncoder.Save(outImageStream);
                                        }
                                    }
                                    finalSavePngTimer.Stop();

                                    putFileDBTimer.Start();

                                    fileDB.PutFile(dbName, dbKey, fh);

                                    if (i == 0)
                                    {
                                        pageMetadata.PngFullSize += GetFileSize(fh.LocalFileName);
                                    }
                                    else if (i == 1)
                                    {
                                        pageMetadata.PngThumbSize += GetFileSize(fh.LocalFileName);
                                    }

                                    putFileDBTimer.Stop();
                                }
                            }

                        }

                    }
                }
                sw.Stop();
                debugLog.AppendLine("[PdfRasterizer]. # pages:" + numberOfPages + ". Executed in " + sw.ElapsedMilliseconds.ToString("#,#") + "ms, OriginalDocId=" + pdfFileDbKey + ", FullScale=" + fullScale + ", thumb scales=" + thumbScale);

                foreach (var o in timerList)
                {
                    debugLog.AppendLine("    " + o.GetDisplayString(sw.ElapsedMilliseconds));
                }

                debugLog.AppendLine();
                debugLog.AppendLine("PDF Size: " + documentMetadata.PdfSize.ToString("#,#") + " bytes. PNG Full Size: " + documentMetadata.PngFullSize.ToString("#,#") + " bytes. PNG Thumb Size: " + documentMetadata.PngThumbSize.ToString("#,#"));

                return documentMetadata;
            }
            catch (Exception e)
            {
                e.Data.Add("OriginalDocId", pdfFileDbKey);
                Trace.Write(e, "PdfRasterizer");
                
                throw;
            }
            finally
            {
                string debugInfo = debugLog.ToString();
                documentMetadata.DebugInfo = debugInfo;
                Trace.Write(debugInfo, "PdfRasterizer");
            }
        }


        private static string GetTempFile()
        {
            // 6/2/2015 dd - Expected a hard code C:\LOTemp folder.
            // Why do I use LOTemp folder? because there is an auto clean up process.

            return @"C:\LOTemp\" + Guid.NewGuid();
        }
    }
}
