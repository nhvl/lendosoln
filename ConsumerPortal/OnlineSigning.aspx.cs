﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ConsumerPortal.Common;
using LendersOffice.PdfForm;
using ConsumerPortal.lib;
using DataAccess;
using LendersOffice.PdfLayout;
using System.Web.Services;
using LendersOffice.ObjLib.Security;
using System.Security.Cryptography;
using System.Text;
using ConsumerPortal.Constants;

namespace ConsumerPortal
{
    public partial class OnlineSigning : ConsumerPortal.common.BaseCPortalPage
    {
        protected long RequestId
        {
            get
            {
                long id = -1;
                string query = CPRequestHelper.GetSafeQueryString("reqId");
                long.TryParse(query, out id);
                return id;
            }
        }

        private ConsumerActionItem m_request = null;

        protected string aBNm
        {
            get
            {
                if (m_request != null)
                    return m_request.aBFullName;
                return "";
            }
        }

        protected string aCNm
        {
            get
            {
                if (m_request != null)
                    return m_request.aCFullName;
                return "";
            }
        }

        protected string aBIni
        {
            get
            {
                if (m_request != null)
                    return m_request.aBInitials;
                return "";
            }
        }

        protected string aCIni
        {
            get
            {
                if (m_request != null)
                    return m_request.aCInitials;
                return "";
            }
        }

        private Guid m_session = Guid.NewGuid();
        protected Guid SessionId
        {
            get
            {
                return m_session;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            RegisterJsScript("managesigners.js");
            RegisterJsScript("json.js");
            RegisterJsScript("jquery-ui-1.8.custom.min.js");
            RegisterJsScript("PdfEditor.js");
            IncludeStyleSheet("~/css/pdfeditor.css");
            IncludeStyleSheet("~/css/jquery-ui-1.8.custom.css");

            ChangeLinkBtn.Visible = !ConstApp.IsChangeSignerDisabled;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            m_request = new ConsumerActionItem(RequestId);

            if (CPTools.CurrentUser.NeedsSsnVerification(m_request.sLId))
            {
                CPTools.RedirectValidateSsn();
            }

            List<SigningPageItem> pages = new List<SigningPageItem>();
            List<PdfField> fields = new List<PdfField>();

            if (ConstApp.IsChangeSignerDisabled)
            {
                //Detect current user or redirect on Error!
                string m_LoggedInUser = "";
                if (!ManageSigner.DetectLoggedInUser(CPTools.CurrentUser, m_request, out m_LoggedInUser))
                {
                    CPTools.RedirectError("Unable to detect logged in User.");
                    return;
                }
            }
            

            if (!Page.IsPostBack)
            {
                Guid formId = m_request.PdfFileDbKey;
                
                PdfFormCache formCache = PdfFormCache.LoadPdfFormCache(formId, SessionId);
                if (formCache == null)
                {
                    formCache = PdfFormCache.CacheThis(PdfForm.LoadConsumerFormById(formId), SessionId);
                }

                //PdfForm pdfForm = PdfForm.LoadConsumerFormById(formId);
                int pageCount = formCache.NumberOfPages;
                pages = GetPdfPageInfoList(formCache);
                //for (int i = 1; i <= pageCount; i++)
                //{
                //    pages.Add(new SigningPageItem() { Page = i, RequestId = formId });
                //}

                PdfFormLayout layout = new PdfFormLayout();
                layout.LoadContent(m_request.PdfMetaDataSnapshot);

                foreach (PdfField field in layout.FieldList)
                {
                    if (field.Type == PdfFieldType.Signature || field.Type == PdfFieldType.Initial)
                    {
                        if ((field.Name == "aBSignature" || field.Name == "aBInitials") && m_request.IsApplicableToBorrower && !m_request.HasBorrowerEsigned)
                        {
                            fields.Add(field);
                        }
                        else if ((field.Name == "aCSignature" || field.Name == "aCInitials") && m_request.IsApplicableToCoborrower && !m_request.HasCoborrowerEsigned)
                        {
                            fields.Add(field);
                        }
                    }
                }

                VerifyMetaData(layout.FieldList, pageCount, m_request);
                
            }

            this.RegisterJsObject("PdfFields", fields);
            this.RegisterJsObject("SigningPages", pages);
            
        }

        private List<SigningPageItem> GetPdfPageInfoList(PdfFormCache pdfFormCache)
        {
            iTextSharp.text.pdf.PdfReader pdfReader = new iTextSharp.text.pdf.PdfReader(pdfFormCache.pdfContent);

            int pageCount = pdfReader.NumberOfPages;
            List<SigningPageItem> list = new List<SigningPageItem>(pageCount);
            for (int i = 1; i <= pageCount; i++)
            {
                var size = pdfReader.GetPageSizeWithRotation(i);
                list.Add(new SigningPageItem() { RequestId = pdfFormCache.formId, Page = i, PageHeight = (int)size.Height, PageWidth = (int)size.Width });

            }
            return list;
        }
        private void VerifyMetaData(IEnumerable<PdfField> fieldList, int pageCount, ConsumerActionItem item)
        {
            bool print = false;

            IEnumerable<PdfField> Invalidfields = fieldList.Where(o => (o.Type == PdfFieldType.Signature || o.Type == PdfFieldType.Initial) && o.PageNumber > pageCount);

            StringBuilder sb = new StringBuilder();
            
            sb.AppendLine("Invalid Metadata and PDF match.");
            sb.AppendLine("RequestId: " + RequestId);
            sb.AppendLine("LoanId: " + item.sLId);
            sb.AppendLine("FormId: " + item.PdfFileDbKey);
            sb.AppendLine("PDF Total Page Number: " + pageCount);
            sb.AppendLine("Signature fields on invalid pages:");

            foreach (PdfField field in Invalidfields)
            {
                sb.AppendLine("* '" + field.Name + "' on page " + field.PageNumber);
                print = true;
            }

            if (print)
                Tools.LogError(sb.ToString());
        }

        private void SignAction()
        {
            E_eSignBorrowerMode mode = E_eSignBorrowerMode.Both;

            try
            {

                if (ConstApp.IsChangeSignerDisabled)
                {
                    if (!ManageSigner.DetectLoggedInUser(CPTools.CurrentUser, m_request, out mode))
                    {
                        CPTools.RedirectError("Unable to detect logged in User.");
                        return;
                    }
                    m_request.SubmitSignature(mode);
                    m_request.Save();
                    return;
                }
                else
                {
                    switch (m_Signer.Value)
                    {
                        case "aBSignature":
                            mode = E_eSignBorrowerMode.Borrower;
                            break;
                        case "aCSignature":
                            mode = E_eSignBorrowerMode.Coborrower;
                            break;
                        case "both":
                            mode = E_eSignBorrowerMode.Both;
                            break;
                        default:
                            throw new Exception("Unable to identify signer.");
                    }

                    m_request.SubmitSignature(mode);
                    m_request.Save();
                }
                
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }
        }

        protected void SignBtn_OnClick(object sender, System.EventArgs a)
        {

            SignAction();
            InitScript.Visible = true;
            EditorScript.Visible = false;
        }
       
    }

    
}
