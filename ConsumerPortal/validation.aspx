﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="validation.aspx.cs" Inherits="ConsumerPortal.validation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>SSN Validation</title>
    <!--[if lte IE 7]><style type="text/css"> body { padding-top:75px; }</style><![endif]-->
    <style type="text/css">
        #ValidatePage  { margin-top: 175px;}
        table#ValidatePage input.Validatebtn { font-size:11px; font-weight: bold; font-family: Verdana, Arial, Helvetica, sans-serif; padding: 1px 10px;}
        #lgout { position:absolute; top:10px; right: 15px;  }
        table#ValidatePage td { text-align: center; }
    </style>
</head>
<body>
<form id="form1" runat="server">
    <div id="content">
        <a id="lgout" href="logout.aspx">Logout</a>
        <table id="ValidatePage" width="100%">
            <tr>
                <td>
                    <table width="100%">
                    <asp:PlaceHolder id="SsnPanel" runat="server">
                        <tr>
                            <td>
                                Please help us verify your identity by entering the last 4 digits of your social security number:
                                <asp:TextBox ID="SsnDigits" MaxLength="4" Width="40" CssClass="login" runat="server" autocomplete="off" TabIndex="1"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                               <input type="button" class="Validatebtn" tabindex="2" value ="Confirm" id="m_ConfirmSsn" />
                               <asp:Button ID="m_ConfirmServer" style="display:none;" runat="server" Text="C" OnClick="OnConfirmClick"></asp:Button>
                            </td>
                        </tr>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder id="ErrorMessages" runat="server">
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle">
                                    <b>
                                        <ml:EncodedLabel ID="m_ErrMsg" CssClass="ErrorLabel" runat="server" ForeColor="Red" EnableViewState="false"></ml:EncodedLabel><br/>
                                        <ml:EncodedLabel ID="m_ErrMsg2" CssClass="ErrorLabel" runat="server" ForeColor="Red" EnableViewState="false"></ml:EncodedLabel>
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td align="middle" colspan="3">
                                    <div id="MessageLabel" />
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
    <script type="text/javascript">
    <!--
        $j(document).ready(function() {
            $j("input[type='text']").keypress(function(e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) {
                    $j('#m_ConfirmSsn').click();
                }
            });

            var keyUpListener = function(e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) return;
                
                var IsInvalid = !f_IsValidSsn($j("#SsnDigits").val());
                $j("#m_ConfirmSsn").attr('disabled', IsInvalid);
            };

            $j("#SsnDigits").keyup(keyUpListener);

            $j("#m_ConfirmSsn").click(function(event) {
                var IsValid = f_IsValidSsn($j("#SsnDigits").val());
                if (!IsValid)
                    event.preventDefault();
                else {
                    $j("#m_ConfirmSsn").attr('disabled', true);
                    $j("#m_ConfirmServer").click();
                }
            });

            $j("#m_ConfirmSsn").attr('disabled', !f_IsValidSsn($j("#SsnDigits").val()));
            $j("#SsnDigits").focus();
        });

        f_IsValidSsn = function(pw) {
            var regex = /[0-9][0-9][0-9][0-9]/;
            try {
                if (!pw || pw.length < 4) return false;
                if (pw.match(regex)) return true;
            } catch (e) { return false };
            return false;
        }
    //-->
    </script>
</body>
</html>
