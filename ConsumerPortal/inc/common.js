var gHighlightRowColor = "Bisque"; // Color of highlighted row.
var gReadonlyBackgroundColor = 'lightgrey';
var gHighlightColor = 'yellow';
var oldTabIndex = 0;
var gVirtualRoot = document.getElementById("VirtualRoot") == null ? "" : document.getElementById("VirtualRoot").value;
//---- Put a transparent layer on top of the current screen.
function debugObject(o) {
    var str = '';
    var i = 0;
    for (p in o)
        if (p != 'outerText' && p != 'innerHTML' && p != 'innerText' && p != 'outerHTML')
        str += 'o.' + p + ' = ' + o[p] + (i++ % 10 == 0 ? '\n\r' : ';');
    alert(str);
}  

function f_disabledInputForBlockMask() {
  var coll = document.getElementsByTagName('select');
  for (var i = 0; i < coll.length; i++) {
    var o = coll[i];
    o.style.display = 'none';
    o._blockTabIndex = o.tabIndex;
    o.tabIndex = -1;
  }
  var oTaggableTags = new Array("INPUT", "A", "BUTTON", "TEXTAREA", "IFRAME");
  for (var i = 0; i < oTaggableTags.length; i++) {
    var oList = document.getElementsByTagName(oTaggableTags[i]);
    for (var j = 0; j < oList.length; j++) {
      var o = oList[j];
      o._blockTabIndex = o.tabIndex;
      o.tabIndex = -1;
    }  
  }
}
function f_enabledInputForBlockMask() {
  var coll = document.getElementsByTagName('select');
  for (var i = 0; i < coll.length; i++) {
    var o = coll[i];
    o.style.display = '';
    o.tabIndex = o._blockTabIndex;
  }
  var oTaggableTags = new Array("INPUT", "A", "BUTTON", "TEXTAREA", "IFRAME");
  for (var i = 0; i < oTaggableTags.length; i++) {
    var oList = document.getElementsByTagName(oTaggableTags[i]);
    for (var j = 0; j < oList.length; j++) {
      var o = oList[j];
      o.tabIndex = o._blockTabIndex;
    }  
  }
}
function f_getViewportHeight() {
	if (window.innerHeight!=window.undefined) return window.innerHeight;
	if (document.compatMode=='CSS1Compat') return document.documentElement.clientHeight;
	if (document.body) return document.body.clientHeight; 
	return window.undefined; 
}
function f_getViewportWidth() {
	if (window.innerWidth!=window.undefined) return window.innerWidth; 
	if (document.compatMode=='CSS1Compat') return document.documentElement.clientWidth; 
	if (document.body) return document.body.clientWidth; 
	return window.undefined; 
}
function f_getScrollLeft() {
  return document.body.scrollLeft;
}
function f_getScrollTop() {
  return document.body.scrollTop;
}

function f_attachEvent(o, type, listener) {
    if (o.attachEvent) {
        o.attachEvent("on" + type, listener);
    }
    else {
        o.addEventListener(type, listener);
    }
}

function f_detachEvent(o, type, listener) {
    if (o.detachEvent) {
        o.detachEvent("on" + type, listener);
    }
    else {
        o.removeEventListener(type, listener);
    }
}

function f_displayBlockMask(bDisplay) {
    var sUniqueId = '__BlockMask210587__'
    var oBlockMaskContent = $(sUniqueId);
    if (null == oBlockMaskContent && bDisplay) {

        var oBody = document.getElementsByTagName("BODY")[0];
        oBlockMaskContent = document.createElement("div");
        oBlockMaskContent.id = sUniqueId;
        oBlockMaskContent.className = 'BlockMask';
        oBody.appendChild(oBlockMaskContent);

    } else if (null != oBlockMaskContent && !bDisplay) {
        oBlockMaskContent.style.display = 'none';
    }

    if (bDisplay) {
        f_disabledInputForBlockMask();
        f_resizeBlockMask();

        f_attachEvent(window, "scroll", f_resizeBlockMask);
        f_attachEvent(window, "resize", f_resizeBlockMask);
    } else {
        f_enabledInputForBlockMask();
        f_detachEvent(window, "scroll", f_resizeBlockMask);
        f_detachEvent(window, "resize", f_resizeBlockMask);

    }
}

function f_resizeBlockMask() {
  var sUniqueId = '__BlockMask210587__'
  var oBlockMaskContent = $(sUniqueId);

  oBlockMaskContent.style.display = '';
  oBlockMaskContent.style.width = f_getViewportWidth() + 'px';
  oBlockMaskContent.style.height = f_getViewportHeight() + 'px';
  oBlockMaskContent.style.left = f_getScrollLeft() + 'px';
  oBlockMaskContent.style.top = f_getScrollTop() + 'px';

}

//---- END Put a transparent layer on top of the current screen.

// return true if browser is IE 5.5+ on Windows.
// IF false redirect to error page saying only IE support.
function checkWinIE() { return true; }
function checkWinIE_NoAlert() { return true; }
function checkWinIE_NoAlert_Old() { return true; }

function $_() {
  var elements = new Array();

  for (var i = 0; i < arguments.length; i++) {
    var element = arguments[i];
    if (typeof element == 'string')
      element = document.getElementById(element);

    if (arguments.length == 1)
      return element;

    elements.push(element);
  }

  return elements;
}

if (!($ && $.fn && $.fn.jquery == '1.12.4')) {
    window.$ = $_;
}

// ------------------- Zero - One CheckBoxList

function _registerCBL(name, c) {
  for (var i = 0; i < c; i++) {
    var o = document.all[name + "_" + i];
    
    o.onclick = function() { onZeroOneCheckboxClick(this, name, c); };
  }
}
// The hidden variables use to store the value of checkbox list will have prefix of 'cbl_'.
// If there is no prefix then error will occur when normal postback occur on CheckBoxList. dd 12/8/03
function onZeroOneCheckboxClick(cb, name, count) {
  //alert('Name = ' + name + ', count = ' + count);
  var b = cb.checked;
  for (var i = 0; i < count; i++) {
    document.all[name + "_" + i].checked = false;
  }
  cb.checked = b;    
  var index = cb.name.substr(name.length + 1, cb.name.length - name.length);
  if (document.forms[0]["cbl_" + name] != null)
    document.forms[0]["cbl_" + name].value = b ? eval(name +"_items[" + index + "]") : "";
//  alert(name + ' = ' + document.forms[0][name].value);
}
// ---- End Zero - One CheckBoxList
function TextAreaMaxLength(fieldObj,maxChars)
{
  var diff = maxChars - fieldObj.value.length;
  if (diff < 0) fieldObj.value = fieldObj.value.substring(0,maxChars);
}
function parseMoneyFloat(str) 
{
  var a = str.indexOf("(") >= 0 ? -1 : 1;
  str = str.replace(/[,$%()]/g, "");
  if (str == "") str = "0";
  return parseFloat(str) * a;
} 
function displayHelp()
{   
    var helpUrl = 'https://support.lendingqb.com/imgstor/imagestore/KBSCPDF_Accessing the LendingQB Support Center PDF.pdf';
    var handle = window.open(helpUrl, 'help', 'resizable=yes,scrollbars=yes,menubar=no,toolbar=yes,height=550pt,width=785pt') ;
    if (handle != null) handle.focus() ;
}




function populateForm(obj, clientID) {
  var start = (new Date()).getTime();
  var count = 0;
  var prefix = (clientID == null || clientID == "") ? "" : clientID + ":";
  
  var coll = document.forms[0];
  for (property in obj) {
    var field = coll[prefix + property];
    if (field != null) {
      if (field.tagName == null) { // A radio button list.
        for (index = 0; index < field.length; index++)
          field[index].checked = field[index].value == obj[property];
      } else if (field.type == 'checkbox') {
        field.checked = obj[property] == 'True';
      } else {
        field.value = obj[property];
      }
    }
    count++;
  }
  var stop = (new Date()).getTime();
  if (typeof pml_action == 'function') pml_action('populateForm', new Array( start, stop, count, coll.length) );
}
function getAllFormValues(clientID, skipIgnore) {
  var start = (new Date()).getTime();
  var obj = new Object();
  var coll = document.forms[0];
  var count = 0;
  for (var i = 0; i < coll.length; i++) {
    var f = coll[i];

    if (null == f.name) continue;
              
    var name = f.name.replace(/:/g, "_"); // : will create a invalid xml string.
    if (name == "IsUpdate" || name == "__VIEWSTATE" || name == "__EVENTTARGET" || name == "__EVENTARGUMENT")
      continue;
    if (f.type == 'submit' || f.type == 'button')
      continue;
    if (skipIgnore && f.SkipMe != null) {
      continue;
    }
    
    if (clientID != null && clientID != "" && !name.match(clientID + "_"))
      continue; // Skip name if it does not belong to this control's clientID
    if (clientID != null && clientID != "")
      name = name.replace(clientID + "_", ""); // Remove clientID_ in front of input name.
      
    if (f.tagName == null) { // A radio button list.

      for (index = 0; index < f.length; index++)
        if (f[index].checked) {
          obj[name] = f[index].value;
        }
    } else if (f.type == 'checkbox') {
      obj[name] = f.checked ? 'True' : 'False';
    } else if (f.type == 'radio') {
      if (f.checked)
        obj[name] = f.value; // Only set value for radio box when checked.
    } else {
      obj[name] = f.value;
    }      
    count++;

  }
  var stop = (new Date()).getTime();
  if (typeof pml_action == 'function') pml_action('getAllFormValues', new Array( start, stop, count ) );
  return obj;
}

function highlightRowByCheckbox(o, bgColor) {
  // Highlight row if checkbox is check.
  if (o.checked != null && o.checked) {
    highlightRow(o, bgColor);
  } else {
    unhighlightRow(o, "");
  }
}
function highlightRow(o, bgColor) {
  // Highlight the inner most <tr> tag.
  if (null == o) return;
  var e = o;
  while (e != null && e.tagName.toLowerCase() != "body") {
    if (e.tagName.toLowerCase() == "tr") {
      e.previousBackgroundColor = e.style.backgroundColor;
      bgColor = bgColor == null ? gHighlightRowColor : bgColor;
      e.style.backgroundColor = bgColor;
      break;
    }
    e = e.parentElement;
  }
}
function unhighlightRow(o, bgColor) {
  // unhighlight the inner most <tr> tag.
  if (null == o) return;
  var e = o;
  

  while (e != null && e.tagName.toLowerCase() != "body") {
    if (e.tagName.toLowerCase() == "tr") {
      if (null != bgColor)
        e.style.backgroundColor = bgColor;
      else
        e.style.backgroundColor= e.previousBackgroundColor == null ? "" : e.previousBackgroundColor;
      break;
    }
    e = e.parentElement;
  }

}

function smartZipcodeAuto(event) {
    var zipcode = event.srcElement || event.target;
    if (zipcode == null) {
        return;
    }

    var retrieveLinkedInputForZipCode = function (zipcode, inputType) {
        var inputId = zipcode.getAttribute(inputType);
        if (inputId) {
            return document.getElementById(inputId);
        }

        return null;
    }

    var city = retrieveLinkedInputForZipCode(zipcode, "city");
    var state = retrieveLinkedInputForZipCode(zipcode, "state");
    var county = retrieveLinkedInputForZipCode(zipcode, "county");

    smartZipcode(zipcode, city, state, county, event);
}

function smartZipcode(zipcode, city, state, county, event) {
  if (gService == null || gService.utils == null || zipcode == null) return;
  if (typeof(zipcode.oldValue) != null && zipcode.oldValue == zipcode.value)
  {
	if ( county != null && typeof(county.value) != null && county.value != "" ) 
		return; 
	else if ( county == null )
		return;
  }

  updateDirtyBit();
  
  var args = new Object();
  args.GetCounties = county != null && county.nodeName.toLowerCase() == "select" ? "true" : "false" ;
    
  args.zipcode = zipcode.value;
  var result = gService.utils.call("smartzipcode", args);
  if (!result.error) {
    var val = result.value;
    if (val.IsValid == "1") {
    	if (state != null) {
			state.oldValue = val.State;
			state.value = val.State;
		}
		if (city != null) {
			if ( state.selectedIndex != -1 ) {	
				city.value = val.City;
			}
		}

		if (county != null && county.nodeName.toLowerCase() != "select") { 
			county.value = val.County;
		}
		
		else if ( county != null )  {

			county.options.length = 0; 				
			if ( state.selectedIndex != -1 ) {
				for( i = 0; i < val.CountyCount; i++ ) {
					p = "val.County" + i; 
					countyName = eval( p );
					selected = countyName.toLowerCase() == val.County.toLowerCase();
					county.options[i] = new Option( countyName, countyName, false, selected ); 
				}
			}
			
		}  
    }
    else if (county != null && county.nodeName.toLowerCase() == "select") 
	{
		county.selectedIndex = -1;
	}
   f_fireEvent(zipcode, "change"); 
  }

  
}

function f_fireEvent(o, event) {
    if (o.fireEvent) {
        o.fireEvent("on" + event);
    }
    else {
        var eventObj = document.createEvent("Event");
        eventObj.initEvent(event, true, true);
        o.dispatchEvent(eventObj);
    }
}

function UpdateCounties(state,county) 
{
	if (gService == null || gService.utils == null || state == null || county == null) return; 
    if (typeof(state.oldValue) != null && state.oldValue == state.selectedIndex ) return; 

	
	updateDirtyBit(); 
	var args = new Object(); 
	args.state = state.options[state.selectedIndex].value;  
	
	 var result = gService.utils.call("GetCountiesForState", args);
	
	 if ( result.error ) 
		return; 
	 
	 val = result.value;
	 county.options.length = 0; 
	
	 for( i = 0; i < val.CountyCount; i++) 
	 {
		p = "val.County" + i; 
		countyName = eval( p );
		county.options[i] = new Option( countyName, countyName, false, false ); 
	 }
	  county.options.selectedIndex = -1;
}

function updateDirtyBit() {
  var src = event != null ? event.srcElement : null;
  if (null != src && (src.readOnly || src.disabled)) return; // DO NOT update dirty bit if field is readonly.

  // Enable save button on the upper left frame. This only apply to /los/loanapp.aspx.
  if (parent.info != null && typeof(parent.info.f_enableSave) == 'function')
    parent.info.f_enableSave(true);

  if (document.all["IsUpdate"] != null) document.all["IsUpdate"].value = "1";

  if (typeof(updateDirtyBit_callback) == 'function') {
    // Define this method on page when you want notify when dirty bit is set.
    updateDirtyBit_callback();
  }  
}
function isDirty() {
  if (document.all["IsUpdate"] != null) return document.all["IsUpdate"].value == "1";
  else return true;
}
function isReadOnly() {
  if (document.all["_ReadOnly"] != null) return document.all["_ReadOnly"].value == "True";
  else return false;
}
function clearDirty() {
  // Enable save button on the upper left frame. This only apply to /los/loanapp.aspx.
  if (parent.info != null && typeof(parent.info.f_enableSave) == 'function')
    parent.info.f_enableSave(false);

  if (document.all["IsUpdate"] != null) document.all["IsUpdate"].value = "0";
}
function formatReadonlyField(o) {
  o.style.backgroundColor = gReadonlyBackgroundColor;
  o.oldTabIndex = o.tabIndex;
  o.tabIndex = -1;
}
function highlightField() {
  var e = event.srcElement;
  if (e.NoHighlight != null) return; // 5/26/2004 dd - Explicitly request by component to not highlight
  if (!e.readOnly && e.tagName.toLowerCase() != 'select') {
    e.style.backgroundColor = gHighlightColor;
    if (typeof (e.select) != "undefined" && e.type != "textarea" && e.type != 'button' && e.type != 'submit') {
      e.select();
    }
  }
  
  if (typeof pml_action == 'function') pml_action('highlightField', e.id );
}
function cancelMouseWheel() {
  // Only work in IE 6.0+
  event.returnValue = false;
}
function unhighlightField() {
  var e = event.srcElement;
  if (e.NoHighlight != null) return; // 5/26/2004 dd - Explicitly request by component to not highlight
  if (!e.readOnly && e.tagName.toLowerCase() != 'select') e.style.backgroundColor = '';
  
}
function attachCommonEvents(o) {
  if (isReadOnly()) {
    if (o.type == 'button') o.disabled = true;
    return;
  }
    // 10/26/2007 dd - In IE7, if you highlight the background of drop down list, you will need to click twice to expand the list
    f_attachEvent(o, "focus", highlightField);
    f_attachEvent(o, "blur", unhighlightField);

  // Change from 'onchange' to 'onkeyup'. As soon as user enter text in input  
  // modify dirty bit.
  if (o.NotForEdit == null) {
    if (o.type == 'checkbox' || o.type == 'radio') 
      f_attachEvent(o, "click", updateDirtyBit);
    else if (o.tagName.toLowerCase() == 'select') {
      f_attachEvent(o, "change", updateDirtyBit);
      f_attachEvent(o, "mousewheel", cancelMouseWheel);
    } else {
      f_attachEvent(o, "paste", updateDirtyBit);    
      f_attachEvent(o, "keyup", updateDirtyBit);
    }
  }
}
function event_keydown() {
  if (event.keyCode == 8) { // Backspace key press
  
    var o = event.srcElement;
    if (o != null && ((o.tagName == "INPUT" && (o.type=="text" || o.type=="password" || o.type=="file")) || (o.tagName == "TEXTAREA"))) {
      if (o.readOnly || o.disabled) return false; // If textbox is readonly or disable then also disable backspace
      else return true; // Don't disable backspace
    } else {
      return false; // Disable backspace key
    }
    
  } else if( event.ctrlKey && event.altKey && event.keyCode == 81) {
    // Ctrl + Alt + Q - Copy object id to clipboard
    var o = event.srcElement;
    if (null != o) {
      var id = o.id;
      if (document.forms[0]["_ClientID"] != null && document.forms[0]["_ClientID"].value != "") {
        id = id.replace(document.forms[0]["_ClientID"].value + "_", "");
      }
      window.clipboardData.setData("Text", id);
    }
  }

}
function _initInput() {
  f_attachEvent(document, "keydown", event_keydown);
  var coll = document.all.tags("INPUT");
  var length = coll.length;
  if (length > 0) {
    for (i = 0; i < length; i++) {
      var o = coll[i];
      if (o.readOnly) formatReadonlyField(o);
      if (o.preset != null && typeof(_initMask) == 'function') _initMask(o);
      attachCommonEvents(o);
      f_attachEvent(o, "propertychange", event_onpropertychange);
    }
  }
  coll = document.all.tags("SELECT");
  length = coll.length;
  if (length > 0) {
    for (i = 0; i < length; i++) {
      var o = coll[i];
      if (o.disabled) disableDDL(o, true);
      attachCommonEvents(o);
      
    }
  }  
  coll = document.all.tags("TEXTAREA");
  length = coll.length;
  if (length > 0) {
    for (i = 0; i < length; i++) {
      var o = coll[i];
      if (o.readOnly) formatReadonlyField(o);    
      attachCommonEvents(o);
    }
  }
  // While init some input set dirtybit.
  // clear dirty bit after finish init. dd 6/4/2003
  clearDirty();
}
function event_onpropertychange() {
  var propertyName = event.propertyName;
  var e = event.srcElement;
  if (propertyName == "readOnly") {
    if (isReadOnly()) e.readOnly = true;
    if (e.readOnly) formatReadonlyField(e);
    else {
      e.style.backgroundColor="";
      e.tabIndex = e.oldTabIndex != null ? e.oldTabIndex : 0;
    }
  }
}
function disableDDL(ddl, b) {
  ddl.disabled = b;
  ddl.style.backgroundColor = b ? gReadonlyBackgroundColor : "";  
}
function event_onkeyup(event) {

  if (typeof pml_action == 'function') pml_action('event_onkeyup');

  if (event.ctrlKey && event.keyCode == 83) {
    // Ctrl + S - Save loan information.
    if (typeof(f_saveMe) == 'function') f_saveMe();
  } else  {
    if (typeof(f_customEventKeyup) == 'function') f_customEventKeyup(event);
  }
}

function displayObnoxiousColor() {
  var pattern = /(TextBox|SSNTextBox|DropDownList|DateTextBox|CheckBox|CheckBoxList|PhoneTextBox|ZipcodeTextBox|ComboBox|PercentTextBox|MoneyTextBox|PhoneTextBox)\d/;
  var coll = document.all.tags("INPUT");
  var length = coll.length;
  if (length > 0) {
    for (i = 0; i < length; i++) {
      var o = coll[i];
      if (o.id.match(pattern))
        o.style.backgroundColor = 'blue';
    }
  }
  coll = document.all.tags("SELECT");
  length = coll.length;
  if (length > 0) {
    for (i = 0; i < length; i++) {
      var o = coll[i];
      if (o.id.match(pattern))
        o.style.backgroundColor = 'blue';
    }
  }
  coll = document.all.tags("TEXTAREA");
  length = coll.length;
  if (length > 0) {
    for (i = 0; i < length; i++) {
      var o = coll[i];
      if (o.id.match(pattern))
        o.style.backgroundColor = 'blue';
    }
  }
  
}

// Define rolodex here. 
//
function cRolodex() {
  this.chooseFromRolodex = cRolodex_ChooseFromRolodex;
  this.addToRolodex = cRolodex_AddToRolodex;
}

function cRolodex_ChooseFromRolodex(type) {
  if (typeof(showModal) != "function") {
    alert("showModal is not define. Please include /common/ModalDlg/CModalDlg.js in your page.");
    var args = new Object();
    args.OK = false;
    return args;
  }
  return showModal('/los/RolodexList.aspx?type=' + type);
}

function cRolodex_AddToRolodex(args) {
  if (gService == null || gService.utils == null) {
    alert("Your page need to inherit from LendersOffice.Common.BaseServicePage");
    return;
  }

  args.AddCommand = "AddIfUnique";
  
  var result , name , comp , disp;

  result = gService.utils.call("AddToRolodex", args);

  name = args.AgentName;
  comp = args.AgentCompanyName;

  if( name != null && name != "" && comp != null && comp != "" )
  {
    disp = "Agent: " + name + " of company " + comp;
  }
  else
  if( name != null && name != "" )
  {
    disp = "Agent: " + name;
  }
  else
  if( comp != null && comp != "" )
  {
    disp = "Entry: " + comp;
  }
  else
  {
    disp = "Entry";
  }

  if (!result.error)
  {
    if (result.value.AddResult == "Collided")
    {
      var o = new Object();
      if( result.value.ReasonWhy != null && result.value.ReasonWhy != "" )
      {
		o.Msg = disp + " is already in Contacts.<br><br>" + result.value.ReasonWhy;
      }
      else
      {
		o.Msg = disp + " is already in the Contacts.";
	  }
      window.showModalDialog(gVirtualRoot + "/inc/customdia.htm", o, "dialogHeight:200px;dialogwidth:450px;");
      var ret = o.ret;
      
      if (ret == 6) {
        args.AddCommand = "Overwrite";
      } else if (ret == 7) {
        args.AddCommand = "Add";
      } else {
        return; 
      }
      
      result = gService.utils.call( "AddToRolodex" , args );
      
      if( !result.error && result.value.AddResult == "Added" )
      {
        if( args.AddCommand == "Overwrite" )
        {
            alert( disp + " has been replaced in Contacts." );
        }
        else
        {
            alert( disp + " added to Contacts." );
        }
      }
      else
      if( !result.error && result.value.AddResult == "Denied" )
      {
        alert( "Access denied.  You do not have permission to add"
             + " contact entries.  Please consult your account"
             + " administrator if you have any questions."
             );
      }
      else
      {
        alert( "Unable to add " + disp + " to Contacts." );
      }
    }
	else
	if( result.value.AddResult == "Denied" )
	{
		alert
			( "Access denied.  You do not have permission to add"
			+ " contact entries.  Please consult your account"
			+ " administrator if you have any questions."
			);
	}
    else
    {
      alert( disp + " added to Contacts." );
    }
  }
  else
  {
    alert( "Unable to add " + disp + " to Contacts." );
  }

}

function pageY(elem) {
    return elem.offsetParent ? (elem.offsetTop + pageY(elem.offsetParent)) : elem.offsetTop;
}

//usage call on iframe load and on window resize. buffer is how many px before the absolute bottom the iframe should 
//grow
function f_resizeIframe(id, buffer) {
    if (!buffer) { buffer = 10; }
    var iframe = document.getElementById(id);
    var height = self.innerHeight || (document.documentElement.clientHeight || document.body.clientHeight);
    height -= pageY(iframe) + buffer;
    height = (height < 0) ? 0 : height;
    iframe.style.height = height + 'px';
  }

  function __jqueryInit() {
    $j(":button,:submit").addClass('buttonStyle');
  }