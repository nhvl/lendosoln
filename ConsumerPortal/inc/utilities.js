

var TextareaUtilities =
{
    LengthCheckInit: function() {
        var areas = document.getElementsByTagName('textarea');
        var counter = document.createElement('div');
        counter.className = 'counter';
        for (var i = 0; i < areas.length; i++) {
            if (areas[i].getAttribute('maxcharactercount')) {
                var counterClone = counter.cloneNode(true);
                counterClone.relatedElement = areas[i];
                counterClone.innerHTML = '<span>0</span> of ' + areas[i].getAttribute('maxcharactercount') + ' characters';
                areas[i].parentNode.insertBefore(counterClone, areas[i].nextSibling);
                areas[i].relatedElement = counterClone.getElementsByTagName('span')[0];
                areas[i].onkeyup = areas[i].onchange = this.MaxLengthCheck;
                areas[i].onkeyup();
            }
        }
    },
    MaxLengthCheck: function() {

        var maxLength = this.getAttribute('maxcharactercount');
        var currentLength = this.value.length;
        if (currentLength > maxLength)
            this.relatedElement.className = 'InvalidTextareaLength';
        else
            this.relatedElement.className = 'ValidTextAreaLength';
        this.relatedElement.firstChild.nodeValue = currentLength;
    },
    CounterRefresh: function() {
        var areas = document.getElementsByTagName('textarea');

        for (var i = 0; i < areas.length; i++) {
            if (areas[i].getAttribute('maxcharactercount')) {
                areas[i].onkeyup();
            }
        }
    }
};
var IEOnlyJsUtilities =
{
    CopyToClipboard: function(elementId) {
        var x = document.getElementById(elementId);
        if (x) {
            window.clipboardData.setData("Text", x.value);
        }
    }
}


var Modal =
{
	activeModal : '',  
	selectMask : 'selectMask', 
	Show : function(id) 
	{	
		this.Hide();
		var obj = document.getElementById(id); 
		obj.style.display = 'block';
		this.activeModal = id;
		this.ShowFrameOverObject(obj);					
	},
	GetSelectionFrame : function () 
	{
		return document.getElementById(this.selectMask);
	},
	ShowFrameOverObject : function( object )
	{
		var frame = this.GetSelectionFrame(); 
		if ( !frame ) return; 
		
		frame.style.width   = object.offsetWidth;  
		frame.style.height  = object.offsetHeight; 
		frame.style.top     = object.offsetTop;
		frame.style.left    = object.offsetLeft;
		frame.style.zIndex = object.style.zIndex -1 ; 
		frame.style.display = 'block';
	},
	HideFrame : function() 
	{
		var frame = this.GetSelectionFrame(); 
		if ( !frame ) return; 
		frame.style.display = 'none';
	},
	Hide : function() 
	{
		if( this.activeModal == '' ) return;
		document.getElementById(this.activeModal).style.display = 'none'; 
		this.activeModal = ''; 
		this.HideFrame();
	}, 
	ShowPopup : function ( id, focusid ) 
	{
		this.Hide();
		this.activeModal = id;
		var xpos = window.event.clientX;
		var ypos = window.event.clientY;
		var elem = document.getElementById(id);
		
		elem.style.display = "block";
		var top = ypos - elem.offsetHeight - 10 + document.documentElement.scrollTop;  
		var left = xpos - ( elem.offsetWidth /2 ) + document.documentElement.scrollLeft;   
		if (  document.documentElement.scrollTop > top ) top = document.documentElement.scrollTop + ypos + 10 ;
		if ( left -10  < 0 ) left = xpos + 10 ; 
		elem.style.top = top + 'px';
		elem.style.left = left + 'px';
		this.ShowFrameOverObject( elem );
		if ( focusid != null && focusid != '' ) document.getElementById(focusid).focus();
	}

};


var Dialog = {
    resizeForIE6And7: function(w, h, offsetLeft, offsetTop, center) {
        Dialog.resize(w, h, offsetLeft, offsetTop, center);

        if (document.documentElement.offsetHeight != h) {
            var chrome = h - document.documentElement.offsetHeight - 10;
            Dialog.resize(w, h + chrome);
        }
    },

    resize: function(w, h, offsetLeft, offsetTop, center) {
        if (typeof (offsetLeft) === 'undefined') {
            offsetLeft = 0;
        }
        if (typeof (offsetTop) === 'undefined') {
            offsetTop = 0;
        }
        if (typeof (center) === 'undefined') {
            center = true;
        }
        w = w < screen.width ? w : screen.width - 10;
        h = h < screen.height ? h : screen.height - 60;
        var l = (screen.width - w) / 2;
        var t = (screen.height - h - 50) / 2;
        t += offsetTop;
        l += offsetLeft;
        self.dialogWidth = w + 'px';
        self.dialogHeight = h + 'px';
        if (center === true) {
            self.dialogLeft = l + 'px';
            self.dialogTop = t + 'px';
        }
    },

    openUrl: function(url, name, w, h) {
        var handle = window.open(url, name, 'resizable=yes,scrollbars=yes,menubar=no,toolbar=no,height=' + h + 'pt,width=' + w + 'pt');
        handle.focus();
        return false;

    }
    
};

