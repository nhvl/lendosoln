﻿// Author: David Dao
// Created Date: 1/20/2010.
/*
Requirement:
Need to include these two js scripts before following code.
- jquery   (www.jquery.com)
- jquery-ui   (www.jqueryui.com) - The custom js only include sortable class.
*/

/**
This class will load image one by one and have call back when the images are finish loading.
$(selector).loadImages(['img.png','img1.png']);
*/
(function($) {
  var IMAGE_LIST_IDX = "dd_loadImages_ImageList_Idx";
  var EVT_TYPE_NS = ".dd_LoadImage";
  var LOAD_IMAGE_EVT_TYPE = "dd_loadImage_evtType" + EVT_TYPE_NS;
  var LOAD_IMAGE_COMPLETED_EVT_TYPE = "dd_loadImage_Completed_evtType" + EVT_TYPE_NS;
  var LOAD_IMAGE_ERROR_EVT_TYPE = "dd_loadImage_Error_evtType" + EVT_TYPE_NS;

  $.fn.loadImages = function(imageList, options) {
    var defaults = {
      onLoadCompleted: function() { },
      onLoadImage: function(evt, src, o, idx, w, h) {
        var parent = evt.currentTarget;
        $(parent).append($("<img>", { "src": src }));
      },
      onLoadError: function(evt, src, o) { alert('error in ' + src); },
      toImgSrc: function(o) { return o; }
    };
    var settings = $.extend({}, defaults, options);

    return this.each(function() {
      $(this).data(IMAGE_LIST_IDX, imageList)
             .bind(LOAD_IMAGE_EVT_TYPE, settings.onLoadImage)
             .bind(LOAD_IMAGE_COMPLETED_EVT_TYPE, settings.onLoadCompleted)
             .bind(LOAD_IMAGE_ERROR_EVT_TYPE, settings.onLoadError);

      this.toImgSrc = settings.toImgSrc;

      _load(this);
      return this;
    });
  };
  function _error(e) {
    var parent = e.data.parent;
    var imageList = $(parent).data(IMAGE_LIST_IDX);
    var src = e.currentTarget.src;

    $(parent).trigger(LOAD_IMAGE_ERROR_EVT_TYPE, [src, imageList[e.data.index]]);
    _cleanUp();
  }
  function _loadImg(e) {
    var img = e.currentTarget;

    var src = img.src;
    var parent = e.data.parent;

    var imageList = $(parent).data(IMAGE_LIST_IDX);
    $(parent).trigger(LOAD_IMAGE_EVT_TYPE, [src, imageList[e.data.index], e.data.index, img.width, img.height]);
    _loadNextImage(parent, e.data.index + 1);
  }
  function _loadNextImage(parent, i) {
    var imageList = $(parent).data(IMAGE_LIST_IDX);

    if (i >= imageList.length) {

      $(parent).trigger(LOAD_IMAGE_COMPLETED_EVT_TYPE);
      _cleanUp(parent);
      return;
    }
    var img = new Image();
    $(img).bind('load', { parent: parent, index: i }, _loadImg)
          .bind('error', { parent: parent, index: i }, _error);

    img.src = parent.toImgSrc(imageList[i]);
  }
  function _load(o) {
    _loadNextImage(o, 0);
  }
  function _cleanUp(parent) {
    $(parent).removeData(IMAGE_LIST_IDX).unbind(EVT_TYPE_NS);
  }
})(jQuery);
// End loadImages.

var ____debugMsgs = [];
function console_log(msg)
{
    ____debugMsgs.push(msg);
    if (typeof (console) != "undefined")
    {
        console.log(msg);
    }
}
function display_console_log()
{
    var length = ____debugMsgs.length;
    var oParent = jQuery('#debugConsoleMsg').empty();
    
    for (var i = 0; i < length; i++)
    {
        oParent.append(jQuery('<div>').text(____debugMsgs[i]));
    }
    jQuery('#debugConsole').dialog('open');
}
/*
This script will rotate the image in the client side. Only work if parameters are in multiple of 90 degree.
$(img_selector).rotate(90);
*/
(function($) {
  $.fn.rotate = function(d) {
    var i = (d || 0) % 360;
    if (i < 0) {
      i += 360;
    }
    var styles = {};
    if (i == 0) {
      styles = { '-webkit-transform': 'rotate(0deg)', '-moz-transform': 'rotate(0deg)', 'rotation': '0deg', 'filter': 'progid:DXImageTransform.Microsoft.BasicImage(rotation=0)' };
    }
    else if (i == 90) {
      styles = { '-webkit-transform': 'rotate(90deg)', '-moz-transform': 'rotate(90deg)', 'rotation': '90deg', 'filter': 'progid:DXImageTransform.Microsoft.BasicImage(rotation=1)' };
    } else if (i == 180) {
      styles = { '-webkit-transform': 'rotate(180deg)', '-moz-transform': 'rotate(180deg)', 'rotation': '180deg', 'filter': 'progid:DXImageTransform.Microsoft.BasicImage(rotation=2)' }

    } else if (i == 270) {
      styles = { '-webkit-transform': 'rotate(270deg)', '-moz-transform': 'rotate(270deg)', 'rotation': '270deg', 'filter': 'progid:DXImageTransform.Microsoft.BasicImage(rotation=3)' }
    }
    return this.each(function() {
      $(this).css(styles);
      return this;
    });
  };

  $.fn.dimension = function() {
    return { width: this.width(), height: this.height() };
  };
})(jQuery);
// End rotate.

if (!LendersOffice) {
  var LendersOffice = {};
}
LendersOffice.PdfEditor = function(editorId, oPageList, options)
{
    if (null === editorId || null === oPageList)
    {
        alert('Missing Arguments');
        return null;
    }
    var $ = jQuery;

    var FULL_PANEL_ID = 'PdfEditorFullPanel';
    var THUMB_PANEL_ID = 'PdfEditorThumbPanel';
    var _fullPanelSelector = '#' + FULL_PANEL_ID;
    var _thumbPanelSelector = '#' + THUMB_PANEL_ID;

    var EVT_TYPE_NS = ".PdfEditor";
    var PAGE_STATUS_CHANGED_EVT_TYPE = "PageStatusChangedEventType" + EVT_TYPE_NS;
    var ERROR_EVT_TYPE = "ErrorEventType" + EVT_TYPE_NS;
    var PAGE_MODIFIED_EVT_TYPE = "PageModifiedEventType" + EVT_TYPE_NS;
    var PDF_FIELD_DROP_EVT_TYPE = "PdfFieldDropEventType" + EVT_TYPE_NS;
    var PDF_FIELD_SELECTED_STOP_EVT_TYPE = "PdfFieldSelectedStopEventType" + EVT_TYPE_NS;

    var CSS_PAGE_NUMBER = "pdf-page-number";
    var CSS_PAGE = "pdf-page";
    var CSS_PAGE_SELECTED = "pdf-page-selected";
    var CSS_PAGE_PANEL = "pdf-page-panel";
    var CSS_LOADING_PANEL = "pdf-loading-panel";
    var CSS_PDF_FIELD = "pdf-pdf-field";
    var CSS_PDF_THUMB_FIELD = "pdf-pdf-thumb-field";

    var CSS_IMG_ROTATE_0 = "img-rotate-0";
    var CSS_IMG_ROTATE_90 = "img-rotate-90";
    var CSS_IMG_ROTATE_180 = "img-rotate-180";
    var CSS_IMG_ROTATE_270 = "img-rotate-270";

    var DATA_PDF_FIELD = "pdfFieldData";
    var DATA_PDF_FIELD_ID = "pdfFieldDataId";
    var DATA_PDF_THUMB_DATA = 'pdfThumbData';
    var DATA_PDF_PAGE_NEED_DROPPABLE_SETUP = '_DATA_PDF_PAGE_NEED_DROPPABLE_SETUP';

    var DATA_CACHE_WIDTH = "___cache_width____";
    var DATA_CACHE_HEIGHT = "___cache_height____";
    var DATA_IMG_ROTATE_CSS = "___data_img_rotate__";
    var IDX_PAGE_KEY = "__id__";

    var _globalUniqueIdCount = 0;

    // Private member variables

    var _cssDefaultPanel = {
        "border": "solid 1px black",
        "overflow": "auto",
        "list-style-type": "none",
        "position": "relative",
        "float": "left",
        "padding-left": "5px",
        "background-color": "gray"
    };

    var _thumbnailScale = 0.10;
    var _editorSelector = '#' + editorId;
    var _pageList = oPageList;
    var _currentPageIndex = 0;
    var _selectableFilter = '.' + CSS_PDF_FIELD;


    // Private functions
    function _convertToJsCoord(lx, ly, ux, uy, js_page_height, js_scale, pdfScale)
    {
        js_scale = js_scale || 1;
        return {
            width: (Math.abs(ux - lx) * pdfScale * js_scale) + "px",
            height: (Math.abs(uy - ly) * pdfScale * js_scale) + "px",
            top: (js_page_height - Math.max(uy, ly) * pdfScale * js_scale) + "px",
            left: (Math.min(lx, ux) * pdfScale * js_scale) + "px",
            position: "absolute"
        };
    }
    function _convertToPdfCoord(top, left, width, height, js_page_height, pdfScale)
    {
        return {
            lx: _round(left / pdfScale),
            ly: _round((js_page_height - (top + height)) / pdfScale),
            ux: _round((left + width) / pdfScale),
            uy: _round((js_page_height - top) / pdfScale)

        };
    }
    function _round(v)
    {
        return Math.round(v * 1000) / 1000;
    }

    function _getPdfFields()
    {
        var list = [];
        $(_fullPanelSelector).find('.' + CSS_PDF_FIELD).each(function()
        {

            var pageHeight = $(this).parent().height();

            var p = $(this).position();
            var d = $(this).dimension();

            var o = $(this).data(DATA_PDF_FIELD);

            var pdfPage = _findPageObjectById($(this).closest('li').data(IDX_PAGE_KEY));

            var scale = pdfPage.Scale || 1;
            var pdfCoord = _convertToPdfCoord(p.top, p.left, d.width, d.height, pageHeight, scale);
            list.push($.extend({}, o, pdfCoord));
        });
        return list;
    }
    var g_selectedPdfFields = [];
    function _getSelectablePdfFields()
    {
        var selectedFields = $(_fullPanelSelector).find(_selectableFilter).filter(function() { return $(this).hasClass('ui-selected'); });

        var result = [];
        $.each(g_selectedPdfFields, function(idx, o)
        {
            var id = o;
            $.each(selectedFields, function() { if (id === $(this).data(DATA_PDF_FIELD_ID)) { result.push($(this)); return false; } });
        });
        return result;
    }
    function _removePdfField(o)
    {
        var id = $(o).data(DATA_PDF_FIELD_ID);
        if (!!id)
        {
            $.each(g_selectedPdfFields, function(idx, obj) { if (obj === id) { g_selectedPdfFields.splice(idx, 1); return false; } });
            var data = $(o).data(DATA_PDF_THUMB_DATA);
            if (data && data.ThumbId)
            {
                data.Thumb.remove();
            }
            $(o).remove();
            $(__pdfEditor).trigger(PDF_FIELD_SELECTED_STOP_EVT_TYPE, g_selectedPdfFields.length);

        }
    }
    function _getFields()
    {
        return $(_fullPanelSelector).find('.' + CSS_PDF_FIELD);
    }
    function _addPdfField(o, bAddToThumb)
    {
        if (o.pg == null || o.pg <= 0 || o.pg > _pageList.length)
        {
            return;
        }
        if (!__isPdfEditorFinishLoading) return;

        var uniqueId = _globalUniqueIdCount++;
        var pgObject = _pageList[o.pg - 1];
        var pdfScale = pgObject.Scale || 1;

        if (!!bAddToThumb)
        {
            var thumbPage = _FindInThumbPanel(pgObject).find('.' + CSS_PAGE);
            var thumbHeight = thumbPage.data(DATA_CACHE_HEIGHT);
            if (null == thumbHeight)
            {
                thumbHeight = $(thumbPage).height();
                thumbPage.data(DATA_CACHE_HEIGHT, thumbHeight);
            }
            var jsThumbCoord = _convertToJsCoord(o.lx, o.ly, o.ux, o.uy, thumbHeight, _thumbnailScale, pdfScale);
            var oThumbDiv = $("<div>").css(jsThumbCoord).addClass(o.cssClass);
            oThumbDiv.data(DATA_PDF_FIELD, o).data(DATA_PDF_FIELD_ID, uniqueId);
            oThumbDiv.data("__JSCOORD__", jsThumbCoord);
            thumbPage.append(oThumbDiv);
            _InitializePdfField(oThumbDiv);
            oThumbDiv.removeClass(CSS_PDF_FIELD).addClass(CSS_PDF_THUMB_FIELD);
            console_log('insert thumb');
        }
        var fullPage = _FindInFullPanel(pgObject).find('.' + CSS_PAGE);
        var fullHeight = fullPage.data(DATA_CACHE_HEIGHT);
        if (null == fullHeight)
        {
            fullHeight = $(fullPage).height();
            fullPage.data(DATA_CACHE_HEIGHT, fullHeight);
        }
        var jsCoord = _convertToJsCoord(o.lx, o.ly, o.ux, o.uy, fullHeight, 1, pdfScale);
        //var oDiv = $("<div>").css(jsCoord).addClass(o.cssClass);
        var oDiv = $("<div class='" + o.cssClass + "' style='position:absolute;width:" + jsCoord.width + ";height:" + jsCoord.height + ";top:" + jsCoord.top + ";left:" + jsCoord.left + "'></div>");
        oDiv.data("__JSCOORD__", jsCoord);
        if (!!_settings.OnRenderPdfFieldContent)
        {
            var oInner = _settings.OnRenderPdfFieldContent.call(oDiv, o);
            if (oInner != null)
            {
                oDiv.append(oInner.addClass('pdf-field-content'));
            }
        }
        if (oDiv.hasClass('pdf-field-checkbox'))
        {
            oDiv.find('.pdf-field-content').width(jsCoord.width).height(jsCoord.height);
        }
        oDiv.data(DATA_PDF_FIELD, o).data(DATA_PDF_FIELD_ID, uniqueId);
        fullPage.append(oDiv);
        _InitializePdfField(oDiv);

        return oDiv;
    }

    function _addDynamicThumbnail(oPdfField, oThumbnailContent)
    {
        var fieldData = $(oPdfField).data(DATA_PDF_FIELD);
        var uniqueId = $(oPdfField).data(DATA_PDF_FIELD_ID);
        var thumbId = 'pdf-field-thumbnail-' + uniqueId;
        var thumbData = {};
        $(oPdfField).data(DATA_PDF_THUMB_DATA, thumbData);

        var thumbPage = _FindInThumbPanel(_pageList[fieldData.pg - 1]).find('.' + CSS_PAGE);

        var jsThumbCoord = null;

        var __jsCoord__ = $(oPdfField).data("__JSCOORD__");
        if (__jsCoord__ == null)
        {
            jsThumbCoord = _convertToScaledJsCoord(oPdfField, _thumbnailScale);
        } else
        {
            jsThumbCoord = _convertToScaledJsCoordWithPosition(parseInt(__jsCoord__.width.replace("px", "")),
                    parseInt(__jsCoord__.height.replace("px", "")),
                    parseInt(__jsCoord__.top.replace("px", "")),
                    parseInt(__jsCoord__.left.replace("px", "")),
                    _thumbnailScale);
        }
        var oThumbDiv = oThumbnailContent.css(jsThumbCoord).addClass('pdf-field-thumbnail ' + thumbId);
        thumbData.ThumbId = thumbId;
        thumbData.Thumb = oThumbDiv;
        thumbPage.append(oThumbDiv);
        $(oPdfField).resize(function()
        {
            var jsThumbCoord = _convertToScaledJsCoord(this, _thumbnailScale);
            $(this).data(DATA_PDF_THUMB_DATA).Thumb.css(jsThumbCoord);
        });
    }

    function _EnforceBoundaries(o)
    {
        var oParent = $(o).parent();
        var parentWidth = oParent.data(DATA_CACHE_WIDTH);
        var parentHeight = oParent.data(DATA_CACHE_HEIGHT);

        if (parentWidth == null)
        {
            parentWidth = oParent.width();
            oParent.data(DATA_CACHE_WIDTH, parentWidth);
        }
        if (parentHeight == null)
        {
            parentHeight = oParent.height();
            oParent.data(DATA_CACHE_HEIGHT, parentHeight);
        }
        var childWidth = 0;
        var childHeight = 0;
        var positionChildLeft = 0;
        var positionChildTop = 0;

        var jsCoord = $(o).data("__JSCOORD__");
        var width1 = 0;
        var height1 = 0;
        if (jsCoord != null)
        {
            // 5/19/2011 dd - since the different between outerWidth and outerHeight vs width and height is about 2 px.
            // Therefore I will choose speed over extreme accuracy.
            //childWidth = $(o).outerWidth();
            //childHeight = $(o).outerHeight();


            positionChildLeft = parseInt(jsCoord.left.replace("px", ""));
            positionChildTop = parseInt(jsCoord.top.replace("px", ""));

            width1 = parseInt(jsCoord.width.replace("px", ""));
            height1 = parseInt(jsCoord.height.replace("px", ""));
            childWidth = width1;
            childHeight = height1;

        }
        else
        {
            //childWidth = $(o).outerWidth();
            //childHeight = $(o).outerHeight();
            var positionChild = $(o).position();
            positionChildLeft = positionChild.left;
            positionChildTop = positionChild.top;

            width1 = $(o).width();
            height1 = $(o).height();
            childWidth = width1;
            childHeight = height1;
        }

        var leftOverage = positionChildLeft + childWidth - parentWidth;
        var topOverage = positionChildTop + childHeight - parentHeight;

        var changed = false;
        if (leftOverage > 0)
        {
            changed = true;
            $(o).css('left', positionChildLeft - leftOverage);
        }

        if (topOverage > 0)
        {
            changed = true;
            $(o).css('top', positionChildTop - topOverage);
        }

        var thumbData = $(o).data(DATA_PDF_THUMB_DATA);
        if (changed && thumbData != null && thumbData.Thumb)
        {
            var coordinates = _convertToScaledJsCoordWithPosition($(o).width(), $(o).height(), positionChildTop, positionChildLeft, _thumbnailScale);
            //var coordinates = _convertToScaledJsCoord(o, _thumbnailScale);
            thumbData.Thumb.css(coordinates);
        }


    }
    function _convertToScaledJsCoordWithPosition(w, h, t, l, scale)
    {
        scale = scale || 1;
        return {
            width: (w * scale) + "px",
            height: (h * scale) + "px",
            top: (t * scale) + "px",
            left: (l * scale) + "px",
            position: "absolute"
        };

    }
    function _convertToScaledJsCoord(o, scale)
    {
        scale = scale || 1;
        return {
            width: ($(o).width() * scale) + "px",
            height: ($(o).height() * scale) + "px",
            top: ($(o).position().top * scale) + "px",
            left: ($(o).position().left * scale) + "px",
            position: "absolute"
        };
    }

    function _SetupInteractionPdfField(o)
    {
        var bInitialized = $(o).data('___INITIALIZED___');

        if (!!bInitialized)
        {
            return;
        }
        _EnforceBoundaries(o);

        if (_settings.AllowEdit || _settings.CanEditAnnotations)
        {

            if ($(o).is(':not(.disable-move)'))
            {
                $(o).draggable({
                    cancel: '.disable-move,:input,option',
                    containment: 'parent',
                    start: function(evt, ui)
                    {
                        $(_fullPanelSelector).selectable("disable");
                        $(__pdfEditor).trigger(PAGE_MODIFIED_EVT_TYPE);
                    },
                    stop: function(evt, ui)
                    {
                        $(_fullPanelSelector).selectable("enable");
                        _EnforceBoundaries(this);
                        var data = $(this).data(DATA_PDF_THUMB_DATA);
                        if (data && data.ThumbId)
                        {
                            var jsThumbCoord = _convertToScaledJsCoord(this, _thumbnailScale);
                            data.Thumb.css(jsThumbCoord);
                        }
                    }
                });
            }
            if ($(o).hasClass('pdf-field-signature') == false &&
          $(o).hasClass('pdf-field-initial') == false && $(o).is(':not(.master-disable-resize)'))
            {
                var w = $(o).attr('min-resize-width');
                var h = $(o).attr('min-resize-height');
                $(o).resizable({
                    cancel: '.disable-resize,.master-disable-resize,:input,option',
                    autoHide: true,
                    containment: 'parent',
                    minWidth: w,
                    minHeight: h,
                    start: function(evt, ui)
                    {
                        $(_fullPanelSelector).selectable("disable");
                        $(__pdfEditor).trigger(PAGE_MODIFIED_EVT_TYPE);
                    },
                    resize: function(evt, ui)
                    {
                        var o = $(evt.target);
                        var data = o.data(DATA_PDF_FIELD);
                        if (o.is('.pdf-field-note') && data.NoteStateType == 'maximized')
                        {
                            data.width = ui.size.width;
                            data.height = ui.size.height;
                        }
                    },
                    stop: function(evt, ui)
                    {
                        $(_fullPanelSelector).selectable("enable");
                        var o = $(evt.target);
                        if (o.hasClass('pdf-field-checkbox'))
                        {
                            o.find('.pdf-field-content').width(ui.size.width).height(ui.size.height);
                        }
                        _EnforceBoundaries(o.get(0));
                        var data = o.data(DATA_PDF_FIELD);
                        if (o.is('.pdf-field-note') && data.NoteStateType == 'maximized')
                        {
                            data.width = ui.size.width;
                            data.height = ui.size.height;
                        }
                        var data = o.data(DATA_PDF_THUMB_DATA);
                        if (data && data.ThumbId)
                        {
                            var jsThumbCoord = _convertToScaledJsCoord(this, _thumbnailScale);
                            data.Thumb.css(jsThumbCoord);
                        }
                    }
                });

            }
        }
        $(o).data('___INITIALIZED___', true);
    }
    function _InitializePdfField(o, bForceSetupInteraction)
    {
        $(o).data('isActualField', true).addClass(CSS_PDF_FIELD);


        if (!!bForceSetupInteraction)
        {
            // 5/19/2011 dd - This method is expensive therefore only call when necessary.
            // During load time I delay the setup.
            _SetupInteractionPdfField(o);

        }

        var data_pdf = $(o).data(DATA_PDF_FIELD);
        if (null == data_pdf)
        {
            data_pdf = {};
            $(o).data(DATA_PDF_FIELD, data_pdf);
        }
        if (null == data_pdf.pg)
        {
            // Find the pg number and set valid cssClass
            var key = $(o).parents('li').data(IDX_PAGE_KEY);

            var pg = -1;
            $.each(_pageList, function(idx, v) { if (v[IDX_PAGE_KEY] == key) { pg = idx + 1; return false; } });
            data_pdf.pg = pg;
        }
        if (null == data_pdf.cssClass)
        {
            $.each(['pdf-field-text', 'pdf-field-checkbox', 'pdf-field-signature', 'pdf-field-initial', 'pdf-field-note', 'pdf-field-highlight'], function()
            {
                if ($(o).hasClass(this))
                {
                    data_pdf.cssClass = this;
                    return false;
                }
            });
        }
    }
    function _setupDroppableOnPage(oPageDiv)
    {
        oPageDiv.droppable({
            accept: '.pdf-field-template',
            drop: function(evt, ui)
            {
                var isActualField = !!ui.helper.data('isActualField');
                if (isActualField == false)
                {
                    var o = ui.helper.clone();
                    o.appendTo(this);
                    var uniqueId = _globalUniqueIdCount++;
                    o.data(DATA_PDF_FIELD_ID, uniqueId);
                    if (_settings.OnInitialDropRenderField)
                    {
                        _settings.OnInitialDropRenderField.call(o.get(0));
                    }
                    _InitializePdfField(o, true);
                    o.offset(ui.offset);
                    _EnforceBoundaries(o.get(0));
                    $(__pdfEditor).trigger(PAGE_MODIFIED_EVT_TYPE);
                    $(__pdfEditor).trigger(PDF_FIELD_DROP_EVT_TYPE, [o]);
                }
            }
        });
    }
    function _appendPngPage(parent, src, oPage, insertAt, idx, w, h, bDroppable, bAppendPageNumberHolder, bInsertAtEnd)
    {
        // Generate <li><div><span></span><div><img src='' /></div></div></li>
        var sStyle = "style ='width:" + w + "px;height:" + h + "px;'";

        var sMainStyle = "style ='width:" + (w + 40) + "px;height:" + (h + 10) + "px;'";
        var htmlFragment = [];
        htmlFragment.push("<div class='" + CSS_PAGE_PANEL + "' " + sMainStyle + ">");
        if (bAppendPageNumberHolder)
        {
            htmlFragment.push("<span class='" + CSS_PAGE_NUMBER + "'></span>");
        }
        htmlFragment.push("<div class='" + CSS_PAGE + "' " + sStyle + ">");
        htmlFragment.push("<img src='" + src + "' " + sStyle + "/>");
        htmlFragment.push("</div>"); // </div CSS_PAGE
        htmlFragment.push("</div>"); // </div CSS_PAGE_PANEL
        /*
        var oPagePanel = $("<div class='" + CSS_PAGE_PANEL + "' style='width:" + (w + 40) + "px'></div>"); //.width(w + 40).addClass(CSS_PAGE_PANEL);
        if (bAppendPageNumberHolder)
        {
        oPagePanel.append($("<span class='" + CSS_PAGE_NUMBER + "'></span>"));
        }
        var oPageDiv = $("<div class='" + CSS_PAGE + "' style='width:" + w + "px;height:" + h + "px;'></div>"); //.addClass(CSS_PAGE).width(w).height(h)

        var oImg = $("<img>", { "src": src, "width": w + "px", "height": h + "px" });

        oPageDiv = oPageDiv.append(oImg);
        */
        var bNeedDroppable = false;
        if ((_settings.AllowEdit || _settings.CanEditAnnotations) && bDroppable)
        {
            bNeedDroppable = true;

        }
        var oData = {};
        oData[IDX_PAGE_KEY] = oPage[IDX_PAGE_KEY];
        oData[DATA_PDF_PAGE_NEED_DROPPABLE_SETUP] = bNeedDroppable;
        oData[DATA_CACHE_WIDTH] = w + 40;
        oData[DATA_CACHE_HEIGHT] = h + 10;

        var oLi = $("<li " + sMainStyle + "></li>").data(oData).html(htmlFragment.join(''));  //append(oPagePanel.append(oPageDiv));
        if (bInsertAtEnd)
        {
            $(parent).append(oLi);
        }
        else
        {
            var $liColl = $("li", parent);
            var insertedPosition = insertAt + idx;
            if (insertedPosition == $liColl.length)
            {
                $(parent).append(oLi);
            } else
            {
                $liColl.eq(insertedPosition).before(oLi);
            }
        }

    }
    var __isPdfEditorFinishLoading = false;
    function _loadPagePanel(list, insertAt, onCompletedHandle)
    {
        var length = list == null ? 0 : list.length;
        var timer = new Date();
        var _fullPanel = $(_fullPanelSelector).detach();
        var _thumbPanel = $(_thumbPanelSelector).detach();
        var bInsertAtEnd = insertAt == _fullPanel.children().length;
        for (var idx = 0; idx < length; idx++)
        {
            var o = list[idx];
            var w = o.PageWidth * (o.Scale || 1);
            var h = o.PageHeight * (o.Scale || 1);
            _appendPngPage(_fullPanel, _settings.EmptyImgSrc, o, insertAt, idx, w, h, true, false, bInsertAtEnd);
            _appendPngPage(_thumbPanel, _settings.EmptyImgSrc, o, insertAt, idx, parseInt(w * _thumbnailScale), parseInt(h * _thumbnailScale), false, true, bInsertAtEnd);

        }
        $(_editorSelector).append(_fullPanel).append(_thumbPanel);
        __isPdfEditorFinishLoading = true;
        timer = (new Date()) - timer;
        console_log('_loadPagePanel Part 1 in ' + timer + 'ms.');
        timer = new Date();

        _resyncThumbPageNumber();

        _hideWaiting();
        __didThumbPanelScrolled = true;
        __didFullPanelScrolled = true;
        _onFullPanelScroll();
        _onThumbPanelScroll();

        _onSelectedPage(_pageList[insertAt], true);

        if (typeof onCompletedHandle === 'function')
        {
            var timerCompletedHandle = (new Date());
            onCompletedHandle();
            timerCompletedHandle = (new Date()) - timerCompletedHandle;
            console_log("_loadPagePanel - onCompleteHandle in " + timerCompletedHandle + " ms.");
        }
        timer = (new Date()) - timer;
        console_log('_loadPagePanel Part 2 in ' + timer + 'ms.');

    }
    function _handleError(msg)
    {
        _hideWaiting();
        $(__pdfEditor).trigger(ERROR_EVT_TYPE, msg);
    }

    var _previousPageIndex = null;
    function _onSelectedPageByIdxPageKey(idxPageKey, bIsScrollBegining)
    {
            console_log('_onSelectedPageByIdxPageKey');
    
        var length = _pageList.length;
        var bIsFound = false;
        for (var idx = 0; idx < length; idx++)
        {
            var _o = _pageList[idx];
            if (_o[IDX_PAGE_KEY] === idxPageKey)
            {
                _currentPageIndex = idx;
                bIsFound = true;
                break;
            }
        }
    
        

        if (bIsFound == false)
        {
            console_log('break out early not found');
            return;
        }
        if (bIsScrollBegining)
        {
            var fullPanel = $(_fullPanelSelector);

            var t = fullPanel.scrollTop() + fullPanel.children().eq(_currentPageIndex).position().top;
            fullPanel.scrollTop(t);
        }
        var thumbPanel = $(_thumbPanelSelector);

        var thumbHeight = __panelHeight;

        var thumbChildren = thumbPanel.children();
        var pg = null;
        if (_previousPageIndex != null)
        {
            pg = thumbChildren.eq(_previousPageIndex);
            pg.find("." + CSS_PAGE).removeClass(CSS_PAGE_SELECTED);
        }
        pg = thumbChildren.eq(_currentPageIndex);
        var imgPg = pg.find("." + CSS_PAGE);
        imgPg.addClass(CSS_PAGE_SELECTED);
        var t = imgPg.position().top;
        if (t < 0 || t > thumbHeight)
        {
            var thumbScrollTop = thumbPanel.scrollTop();
            thumbPanel.scrollTop(thumbScrollTop + t);
        }
        _previousPageIndex = _currentPageIndex;
        /* OLD CODE
        thumbPanel.children().each(function()
        {
        if ($(this).data(IDX_PAGE_KEY) === o[IDX_PAGE_KEY])
        {
        var t = $("div." + CSS_PAGE, this).position().top;
        if (t < 0 || t > thumbHeight)
        {
        thumbPanel.scrollTop(thumbScrollTop + t);
        }

                $("." + CSS_PAGE, this).addClass(CSS_PAGE_SELECTED);
        } else
        {
        $("." + CSS_PAGE, this).removeClass(CSS_PAGE_SELECTED);
        }
        return true;
        });
        */

        $(__pdfEditor).trigger(PAGE_STATUS_CHANGED_EVT_TYPE, __pdfEditor);

    }
    function _onSelectedPage(o, bIsScrollBegining)
    {
        if (null == o) return;
        _onSelectedPageByIdxPageKey(o[IDX_PAGE_KEY], bIsScrollBegining);
    }

    function _findPageObjectById(id)
    {
        var length = _pageList.length;
        for (var i = 0; i < length; i++)
        {
            var o = _pageList[i];
            if (o[IDX_PAGE_KEY] === id)
            {
                return o;
            }
        }
        return null;
    }
    function _FindInFullPanel(o)
    {
        return _FindByIdxPageKey(_fullPanelSelector, o);
    }
    function _FindInThumbPanel(o)
    {
        return _FindByIdxPageKey(_thumbPanelSelector, o);
    }
    function _FindByIdxPageKey(selector, o)
    {
        var length = _pageList.length;

        var children = $(selector).children();
        for (var i = 0; i < length; i++)
        {
            if (_pageList[i][IDX_PAGE_KEY] === o[IDX_PAGE_KEY])
            {
                var ret = children.eq(i);
                if (ret.data(IDX_PAGE_KEY) === o[IDX_PAGE_KEY])
                {
                    return ret;
                }
                else
                {
                    break; // Exit for loop and resort to slower method.
                }
            }
        }
        // Could not search the page using above quick method. The reason is if page is rearrange and it does not match
        // with the _pageList index. When this happen we need to resort to this slow method by traverse every page.

        return children.filter(function() { return $(this).data(IDX_PAGE_KEY) === o[IDX_PAGE_KEY]; });

    }
    function _SelectPages(o)
    {
        return $(_fullPanelSelector + ' > *, ' + _thumbPanelSelector + ' > *')
                .filter(function() { return $(this).data(IDX_PAGE_KEY) === o[IDX_PAGE_KEY]; });
    }
    function _refreshPages()
    {
        if (_pageList.length <= 0) return;


        var p0 = _pageList[0];
        var el0 = _FindInFullPanel(p0);
        var length = _pageList.length;
        for (var i = 1; i < length; i++)
        {
            var p1 = _pageList[i];
            var el1 = el0.next();

            if (el1.data(IDX_PAGE_KEY) === p1[IDX_PAGE_KEY])
            {
                // NO-OP
                el0 = el1;
            } else
            {
                el1 = _FindInFullPanel(p1);
                el0.after(el1);
                el0 = el1;
            }

            p0 = p1;
        }

    }
    function _CalcPercentVisible(h, t0, h0)
    {
        return ((h < (t0 + h0) ? h : (t0 + h0)) - (t0 < 0 ? 0 : t0 < h ? t0 : h)) / h * 100;
    }

    var __didFullPanelScrolled = true;
    var __scrollDelayInMs = 500;
    var __pageScrollHandlerEnabled = true;

    function _getCachePositionForScroll(oLi, parentScrollTop)
    {
        var data = oLi.data(); // 5/2/2011 dd - Cache data object for better performance.
        // 5/2/2011 dd - $(this).position().top & $(this).height() are expensive operations. Therefore need to cache.

        var originalTop = data._originalTop;
        if (originalTop == null)
        {
            originalTop = parentScrollTop + oLi.position().top;
            data._originalTop = originalTop;
        }

        var top = originalTop - parentScrollTop;
        var height = data._originalHeight;

        if (height == null)
        {
            height = oLi.height();
            data._originalHeight = height;
        }

        return { top: top, height: height };
    }
    function _scrollImpl(selector)
    {


        var parent = $(selector);

        var parentHeight = __panelHeight; // ; parent.height();
        var parentScrollTop = parent.scrollTop();
        var children = parent.children();

        var parentTotalScrollHeight = parent.data('__TOTAL_SCROLL_HEIGHT__');

        if (parentTotalScrollHeight == null)
        {
            var lastChildPosition = _getCachePositionForScroll(children.last(), parentScrollTop);
            parentTotalScrollHeight = parentScrollTop + lastChildPosition.top;

            parent.data('__TOTAL_SCROLL_HEIGHT__', parentTotalScrollHeight);
        }


        var selectedId = null;
        var hasAtLeastOneVisible = false;
        var startingIndex = Math.floor(((parentScrollTop / parentTotalScrollHeight) - .02) * children.size());
        while (hasAtLeastOneVisible == false)
        {
            children.each(function(idx)
            {
                if (idx < startingIndex)
                {
                    return true; // Skip this
                }
                //console_log('Calculate at ' + idx);
                var oLi = $(this);
                var pageId = oLi.data(IDX_PAGE_KEY)

                var data = oLi.data(); // 5/2/2011 dd - Cache data object for better performance.
                // 5/2/2011 dd - $(this).position().top & $(this).height() are expensive operations. Therefore need to cache.

                var position = _getCachePositionForScroll(oLi, parentScrollTop);
                var top = position.top;
                var height = position.height;
                /*
                var originalTop = data._originalTop;
                if (originalTop == null)
                {
                originalTop = parentScrollTop + oLi.position().top;
                data._originalTop = originalTop;
                }

            var top = originalTop - parentScrollTop;
                var height = data._originalHeight;

            if (height == null)
                {
                height = oLi.height();
                data._originalHeight = height;
                }
                */
                var percentVisible = _CalcPercentVisible(parentHeight, top, height);

                var previousPercentVisible = data._previousPercentVisible;

                if (previousPercentVisible == null || percentVisible != previousPercentVisible)
                {
                    var src = _settings.EmptyImgSrc;
                    var img = $(this).find('.pdf-page > img');

                    if (percentVisible > 0)
                    {
                        src = _settings.GenerateFullPageSrc(_findPageObjectById(pageId));
                        img.parent().css('left', '0px'); // 5/17/2011 dd - Need this in order for IE7 to work properly. Otherwise when scroll far the thumbnail will not generate properly.

                        if (data[DATA_PDF_PAGE_NEED_DROPPABLE_SETUP] == true)
                        {
                            _setupDroppableOnPage($(this).find('.' + CSS_PAGE));
                            data[DATA_PDF_PAGE_NEED_DROPPABLE_SETUP] = false;
                        }

                        // 5/19/2011 dd - Loop through all pdf-fields and call to setup interaction pdf field
                        $(this).find('div.' + CSS_PDF_FIELD).each(function() { _SetupInteractionPdfField(this); });
                        _applyImgRotation(img, img.data(DATA_IMG_ROTATE_CSS));
                    }
                    img.attr('src', src);


                    data._previousPercentVisible = percentVisible;
                }
                if (percentVisible > 50 && selectedId == null)
                {
                    selectedId = pageId;
                }
                if (percentVisible > 0)
                {
                    hasAtLeastOneVisible = true;
                }
                else if (percentVisible <= 0 && hasAtLeastOneVisible)
                {
                    return false; // The rest of pages will be invisible.
                }

            });
            startingIndex = 0; // reset.
        }


        return selectedId;

    }
    function _onFullPanelScroll()
    {
        if (__didFullPanelScrolled == true && __pageScrollHandlerEnabled == true)
        {
            var timer = new Date();
            __didFullPanelScrolled = false;

            var selectedId = _scrollImpl(_fullPanelSelector);

            //var o = _findPageObjectById(selectedId);
            _onSelectedPageByIdxPageKey(selectedId, false);

            timer = (new Date()) - timer;

            console_log('Full Panel Scroll in ' + timer + 'ms');

        }
        setTimeout(_onFullPanelScroll, __scrollDelayInMs);
    }

    function _clearPagePositionCacheData()
    {
        $(_thumbPanelSelector).data('__TOTAL_SCROLL_HEIGHT__', null);
        $(_thumbPanelSelector).children().each(function()
        {
            var data = $(this).data();
            data._originalTop = null;
            data._originalHeight = null;
            data._previousPercentVisible = null;

        });
        $(_fullPanelSelector).data('__TOTAL_SCROLL_HEIGHT__', null);
        $(_fullPanelSelector).children().each(function()
        {
            var data = $(this).data();
            data._originalTop = null;
            data._originalHeight = null;
            data._previousPercentVisible = null;

        });
        __didThumbPanelScrolled = true;
        __didFullPanelScrolled = true;
    }
    var __didThumbPanelScrolled = true;
    function _onThumbPanelScroll()
    {
        if (__didThumbPanelScrolled == true && __pageScrollHandlerEnabled == true)
        {
            var timer = new Date();
            __didThumbPanelScrolled = false;
            var selectedId = _scrollImpl(_thumbPanelSelector); ;
            //var o = _findPageObjectById(selectedId);
            //_onSelectedPage(o, false); // dd This method is not necessary when scroll thumb.
            timer = (new Date()) - timer;
            console_log('Thumb Panel Scroll in ' + timer + 'ms');
        }
        setTimeout(_onThumbPanelScroll, __scrollDelayInMs);
    }

    function _resyncThumbPageNumber()
    {
        var timer = new Date();
        $(_thumbPanelSelector).find("." + CSS_PAGE_NUMBER).each(function(idx) { $(this).html(idx + 1); }); // Re-update thumbnail page number.
        timer = (new Date()) - timer;
        console_log('Resync ThumbPage Number in ' + timer);
    }
    function _go(d)
    {
        if (!__isPdfEditorFinishLoading) return;
        var i = _currentPageIndex + d;
        if (i < 0 || i >= _pageList.length)
            return;
        _onSelectedPage(_pageList[i], true);
    }
    function _rotate(from, to, d)
    {
        if (!_settings.AllowEdit)
        {
            return;
        }
        if (!__isPdfEditorFinishLoading) return;
        var i = (d || 0) % 360;
        if (i < 0)
        {
            i += 360;
        }
        var bNeedClearCachePagePosition = (i == 90) || (i == 270);

        $(_pageList).slice(from - 1, to).each(function()
        {
            this.Rotation += d;

            if (typeof (this.ClientRotation) == "undefined" || this.ClientRotation == null)
            {
                this.ClientRotation = this.Rotation;
            } else
            {
                this.ClientRotation += d;
            }


            //var oPg = _SelectPages(this);

            //oPg.find("." + CSS_PAGE + " img").rotate(this.Rotation);
            var b = _rotatePageImpl(this, this.ClientRotation, bNeedClearCachePagePosition);
            //ensure the fields are still within the page

        });
        $.each(_getFields(), function()
        {
            _EnforceBoundaries(this);
        })
        _clearPagePositionCacheData();
        _scrollImpl(_fullPanelSelector);
        _scrollImpl(_thumbPanelSelector);
        $(__pdfEditor).trigger(PAGE_MODIFIED_EVT_TYPE);

        _onSelectedPage(_pageList[from - 1], true);
    }

    function _applyImgRotation(oImg, cssRotate)
    {
        if (null == cssRotate || "" == cssRotate)
        {
            return; // No-op.
        }
        if (oImg.hasClass(cssRotate) == false)
        {
            oImg.removeClass(CSS_IMG_ROTATE_0);
            oImg.removeClass(CSS_IMG_ROTATE_90);
            oImg.removeClass(CSS_IMG_ROTATE_180);
            oImg.removeClass(CSS_IMG_ROTATE_270);
            oImg.addClass(cssRotate);
        }
    }
    function _rotatePageImpl(oPage, d, bNeedResize)
    {
        var i = (d || 0) % 360;

        if (i < 0)
        {
            i += 360;
        }
        var cssClass = null;
        if (i == 0)
        {
            cssClass = CSS_IMG_ROTATE_0;
        }
        else if (i == 90)
        {
            cssClass = CSS_IMG_ROTATE_90;
        }
        else if (i == 180)
        {
            cssClass = CSS_IMG_ROTATE_180;

        } else if (i == 270)
        {
            cssClass = CSS_IMG_ROTATE_270;
        }

        // Find Full Page
        var oLi = _FindInFullPanel(oPage);

        var oCssPagePanel = oLi.find('.' + CSS_PAGE_PANEL);
        if (oCssPagePanel.data(DATA_CACHE_HEIGHT) == null)
        {
            oCssPagePanel.data(DATA_CACHE_WIDTH, oLi.data(DATA_CACHE_WIDTH));
            oCssPagePanel.data(DATA_CACHE_HEIGHT, oLi.data(DATA_CACHE_HEIGHT));
        }

        var oCssPage = oCssPagePanel.find('.' + CSS_PAGE);
        if (oCssPage.data(DATA_CACHE_HEIGHT) == null)
        {
            oCssPage.data(DATA_CACHE_WIDTH, oLi.data(DATA_CACHE_WIDTH) - 40); // This number is define in _appendPngPage method.
            oCssPage.data(DATA_CACHE_HEIGHT, oLi.data(DATA_CACHE_HEIGHT) - 10); // This number is define in _appendPngPage

        }
        var oImg = oCssPage.find('img');
        oImg.data(DATA_IMG_ROTATE_CSS, cssClass); // delay this rotation because it is memory intensive

        if (bNeedResize)
        {
            __swapDimension(oCssPage);
            __swapDimension(oCssPagePanel);
            __swapDimension(oLi);
        }
        // Find Thumb nail

        oLi = _FindInThumbPanel(oPage);
        oCssPagePanel = oLi.find('.' + CSS_PAGE_PANEL);
        if (oCssPagePanel.data(DATA_CACHE_HEIGHT) == null)
        {
            oCssPagePanel.data(DATA_CACHE_WIDTH, oLi.data(DATA_CACHE_WIDTH));
            oCssPagePanel.data(DATA_CACHE_HEIGHT, oLi.data(DATA_CACHE_HEIGHT));
        }

        oCssPage = oCssPagePanel.find('.' + CSS_PAGE);
        if (oCssPage.data(DATA_CACHE_HEIGHT) == null)
        {
            oCssPage.data(DATA_CACHE_WIDTH, oLi.data(DATA_CACHE_WIDTH) - 40); // This number is define in _appendPngPage method.
            oCssPage.data(DATA_CACHE_HEIGHT, oLi.data(DATA_CACHE_HEIGHT) - 10); // This number is define in _appendPngPage

        }

        oImg = oCssPage.find('img');
        oImg.data(DATA_IMG_ROTATE_CSS, cssClass); // delay this rotatin because it is memory intensive
        //oImg.addClass('img-rotate-90');
        //oImg.css(styles);
        if (bNeedResize)
        {
            __swapDimension(oCssPage);
            __swapDimension(oCssPagePanel);
            __swapDimension(oLi);
        }
        return bNeedResize;

    }

    function __swapDimension(o)
    {
        var w = o.data(DATA_CACHE_WIDTH);
        if (w == null || isNaN(w))
        {
            w = o.width();
        }

        var h = o.data(DATA_CACHE_HEIGHT);
        if (h == null || isNaN(h))
        {
            h = o.height();
        }
        o.width(h).height(w);
        o.data(DATA_CACHE_WIDTH, h);
        o.data(DATA_CACHE_HEIGHT, w);
    }
    function _insertPages(list, insertAt)
    {
        if (!_settings.AllowEdit)
        {
            return;
        }
        if (!__isPdfEditorFinishLoading) return;
        if (insertAt < 0 || insertAt > _pageList.length)
        {
            return;
        }
        _addUniqueId(list);

        var tmp = [].concat(_pageList.slice(0, insertAt), list, _pageList.slice(insertAt, _pageList.length));
        _pageList = tmp;

        _loadPagePanel(list, insertAt);
        _resyncFields();
        _clearPagePositionCacheData();
        $(__pdfEditor).trigger(PAGE_MODIFIED_EVT_TYPE);

    }
    function _deletePages(pgStart, numOfPagesDelete)
    {
        if (!_settings.AllowEdit)
        {
            return;
        }
        if (!__isPdfEditorFinishLoading) return;
        var deletePages = _pageList.splice(pgStart - 1, numOfPagesDelete);
        $(deletePages).each(function()
        {
            _SelectPages(this).remove();
        });
        $(__pdfEditor).trigger(PAGE_MODIFIED_EVT_TYPE);

        if (_pageList.length == 0)
        {
            $(__pdfEditor).trigger(PAGE_STATUS_CHANGED_EVT_TYPE, __pdfEditor);
            return;
        }
        _resyncThumbPageNumber();
        if ((pgStart - 1) == _pageList.length)
        {
            _onSelectedPage(_pageList[_pageList.length - 1], true);
        } else
        {
            _onSelectedPage(_pageList[pgStart - 1], true);
        }

        _resyncFields();
        _clearPagePositionCacheData();
    }

    function _movePages(pg, insertAt)
    {
        if (!_settings.AllowEdit)
        {
            return;
        }
        if (!__isPdfEditorFinishLoading) return;
        var newPageIndex = insertAt;
        if (pg <= insertAt)
        {
            newPageIndex = insertAt - 1;
        }
        var o = _pageList.splice(pg - 1, 1)[0];
        _pageList.splice(newPageIndex, 0, o);

        var pgMoved = _FindInFullPanel(o).detach();
        var thumbPgMoved = _FindInThumbPanel(o).detach();
        if (newPageIndex - 1 >= 0)
        {
            _FindInFullPanel(_pageList[newPageIndex - 1]).after(pgMoved);
            _FindInThumbPanel(_pageList[newPageIndex - 1]).after(thumbPgMoved);
        }
        else
        {
            _FindInFullPanel(_pageList[1]).before(pgMoved);
            _FindInThumbPanel(_pageList[1]).before(thumbPgMoved);

        }


        _resyncThumbPageNumber();
        _resyncFields();
        _clearPagePositionCacheData();
        _onSelectedPage(o, true);
        $(__pdfEditor).trigger(PAGE_MODIFIED_EVT_TYPE);
    }
    //updates the fields page number to match the pagelist order
    function _resyncFields()
    {
        $('#' + FULL_PANEL_ID + ' li .' + CSS_PAGE).each(function(i, o)
        {
            var uniqueId = $(o).closest('li').data(IDX_PAGE_KEY);
            var newPage;
            var length = _pageList.length;
            for (var index = 0; index < length; index++)
            {
                if (uniqueId === _pageList[index][IDX_PAGE_KEY])
                {
                    newPage = index + 1;
                    break;
                }
            }
            $('.' + CSS_PDF_FIELD, this).each(function(j, p)
            {
                $(p).data(DATA_PDF_FIELD).pg = newPage;

                var thumbData = $(p).data(DATA_PDF_THUMB_DATA); //needed for ie7 or thumbs are lost on drag drop of page
                if (thumbData && thumbData.Thumb)
                {
                    var jsThumbCoord = _convertToScaledJsCoord(p, _thumbnailScale);
                    thumbData.Thumb.css(jsThumbCoord);
                }
            });
        });

    }

    var _defaultOptions = {
        PanelMargin: 5,
        PageBottomPadding: 80,
        AllowEdit: true,
        SignatureFieldImgSrc: null,
        CheckboxFieldImgSrc: null,
        InitialFieldImgSrc: null,
        GenerateFullPageSrc: function(o) { return o; },
        GenerateThumbPageSrc: function(o) { return GenerateFullPageSrc(o); },
        OnPageStatusChanged: function(editor) { },
        OnRenderPdfFieldContent: function(o) { return null; },
        OnInitialdropRenderField: function() { },
        OnError: function(msg) { },
        OnPageModified: function() { },
        OnPdfFieldDrop: function(evt, o) { },
        OnPdfFieldSelectedStop: function(evt, count) { },
        CanEditAnnotations: false,
        EmptyImgSrc: ''
    };

    var _settings = $.extend({}, _defaultOptions, options);
    var __panelHeight = 0;
    function _resizeEditor(bManual)
    {

        var doc = $(window).dimension();
        //alert(doc.width + 'x' + doc.height);
        var panelHeight = doc.height - $(_editorSelector).position().top - _settings.PageBottomPadding;
        if (panelHeight < 50)
        {
            panelHeight = 50; // Set minimum height;
        }

        var thumbPanelWidth = doc.width * .15;
        if (thumbPanelWidth < 70)
        {
            thumbPanelWidth = 70; // Minimum width of thumbnail panel
        } else if (thumbPanelWidth > 300)
        {
            thumbPanelWidth = 300;
        }
        var fullPanelWidth = doc.width - thumbPanelWidth - (4 * _settings.PanelMargin) - 30;
        if (fullPanelWidth < 100)
        {
            fullPanelWidth = 100;
        }

        $(_editorSelector).height(panelHeight + (2 * _settings.PanelMargin)).width(doc.width);
        console_log('Resize editor. ' + doc.width + 'x' + doc.height + ' panel Height:' + panelHeight);
        $('#' + FULL_PANEL_ID).width(fullPanelWidth).height(panelHeight);
        $('#' + THUMB_PANEL_ID).width(thumbPanelWidth).height(panelHeight);
        __panelHeight = panelHeight;
        __didThumbPanelScrolled = true;
        __didFullPanelScrolled = true;
    }

    function _setupPanels()
    {
        $(window).resize(_resizeEditor);

        $(_editorSelector).append($("<ul>", { 'id': FULL_PANEL_ID }).css(_cssDefaultPanel).css('margin', _settings.PanelMargin + 'px')
                .after($("<ul>", { 'id': THUMB_PANEL_ID }).css(_cssDefaultPanel).css('margin', _settings.PanelMargin + 'px')));


        $('#debugConsole').dialog({ autoOpen: false });
        _resizeEditor();

        $(_fullPanelSelector).scroll(function(evt) { __didFullPanelScrolled = true; });
        $(_thumbPanelSelector).scroll(function(evt) { __didThumbPanelScrolled = true; });

        $(_thumbPanelSelector).delegate('.' + CSS_PAGE, 'click', function() { _onSelectedPageByIdxPageKey($(this).closest('li').data(IDX_PAGE_KEY), true); });

        if (_settings.AllowEdit || _settings.CanEditAnnotations)
        {
            $(_fullPanelSelector).selectable(
      {
          filter: _selectableFilter,

          selected: function(evt, ui)
          {
              var id = $(ui.selected).data(DATA_PDF_FIELD_ID);
              if ($.inArray(id, g_selectedPdfFields) === -1)
              {
                  g_selectedPdfFields.push(id);
              }
          },
          unselected: function(evt, ui)
          {
              var id = $(ui.unselected).removeClass('pdf-first-selected').data(DATA_PDF_FIELD_ID);

              $.each(g_selectedPdfFields, function(idx, o) { if (o === id) { g_selectedPdfFields.splice(idx, 1); return false; } });
          },
          stop: function(evt, ui)
          {
              $(_getSelectablePdfFields()[0]).addClass('pdf-first-selected');
              $(__pdfEditor).trigger(PDF_FIELD_SELECTED_STOP_EVT_TYPE, g_selectedPdfFields.length);
          }
      });
        }

    }
    var _waitingPanelId = '__WAITING';
    function _displayWaiting()
    {
        return;
        var top = $(document).height() / 2 - 50;
        var left = $(document).width() / 2 - 150;
        $(_editorSelector).append($('<div>', { 'id': _waitingPanelId }).text('Loading ...').addClass(CSS_LOADING_PANEL).css({ 'top': top + 'px', 'left': left + 'px' }));
    }
    function _hideWaiting()
    {
        $('#' + _waitingPanelId).remove();
    }
    function __init(o)
    {
        $(o).bind(PAGE_STATUS_CHANGED_EVT_TYPE, _settings.OnPageStatusChanged)
        .bind(ERROR_EVT_TYPE, _settings.OnError)
        .bind(PAGE_MODIFIED_EVT_TYPE, _settings.OnPageModified)
        .bind(PDF_FIELD_DROP_EVT_TYPE, _settings.OnPdfFieldDrop)
        .bind(PDF_FIELD_SELECTED_STOP_EVT_TYPE, _settings.OnPdfFieldSelectedStop);

        $(document).keydown(function(evt)
        {
            if (evt.ctrlKey && evt.altKey && evt.which == 77)
            {
                display_console_log();
            }
        });

    }
    function _addUniqueId(list)
    {
        // 1/20/2010 dd - Append Unique Id to each page object
        $.each(list, function() { this[IDX_PAGE_KEY] = _globalUniqueIdCount++; });
    }
    // Public functions

    var __pdfEditor = {

        GetPages: function() { return _pageList; },
        GetPdfFields: function() { return _getPdfFields(); },
        GetFields: _getFields,
        GetTotalPages: function() { return _pageList.length || 0; },
        GetCurrentPageNumber: function() { return (this.GetTotalPages() > 0) ? _currentPageIndex + 1 : 0; },
        SetCurrentPageNumber: function(pg) { },
        InsertPages: function(list, insertAt) { return _insertPages(list, insertAt); },
        DeletePages: function(pgStart, numOfPagesDelete) { _deletePages(pgStart, numOfPagesDelete); },
        BatchRotate: function(pgStart, pgTo, degree) { _rotate(pgStart, pgTo, degree); },
        MoveNextPage: function() { _go(1); },
        MovePrevPage: function() { _go(-1); },
        MovePages: function(pg, insertAt) { return _movePages(pg, insertAt); },
        RotateCurrentPage: function(degree)
        {
            var current = this.GetCurrentPageNumber();
            this.BatchRotate(current, current, degree);
        },
        EnforceBoundaries: function(attachedPdfFieldElement) { _EnforceBoundaries(attachedPdfFieldElement); },
        AddPdfField: function(o, bAddToThumb) { return _addPdfField(o, bAddToThumb); },
        RemovePdfField: function(o) { return _removePdfField(o); },
        GetSelectedPdfFields: function() { return _getSelectablePdfFields(); },
        AddDynamicThumbnail: function(pdfField, thumbnailContent) { _addDynamicThumbnail(pdfField, thumbnailContent); },
        Initialize: function(onCompletedHandle)
        {
            _displayWaiting();
            _addUniqueId(_pageList);
            _setupPanels();
            if (_settings.AllowEdit)
            {


                $(_thumbPanelSelector).sortable(
            {
                axis: 'y',
                placeholder: 'placeholder',
                helper: 'clone',
                start: function(event, ui) { __pageScrollHandlerEnabled = false; },
                stop: function(event, ui) { __pageScrollHandlerEnabled = true; },
                update: function(event, ui)
                {
                    var selectedId = $(ui.item).data(IDX_PAGE_KEY);
                    var selected = null;
                    var tmp = [];
                    $("> *", this).each(function()
                    {
                        var id = $(this).data(IDX_PAGE_KEY);
                        var o = _findPageObjectById(id);
                        if (null != o)
                        {
                            tmp.push(o);
                            if (id === selectedId)
                            {
                                selected = o;
                            }
                        }

                    });
                    _pageList = tmp;
                    $(__pdfEditor).trigger(PAGE_MODIFIED_EVT_TYPE);
                    _resyncThumbPageNumber();
                    _resyncFields();
                    _refreshPages();
                    _clearPagePositionCacheData();
                    _onSelectedPage(selected, true);
                }

            }
  );
            }

            _loadPagePanel(_pageList, 0, onCompletedHandle);
        },

        Refresh: function(pageList, onCompleteHandle)
        {
            _displayWaiting();
            _pageList = pageList;
            $(_fullPanelSelector).empty();
            $(_thumbPanelSelector).empty();
            _addUniqueId(_pageList);
            _loadPagePanel(_pageList, 0, onCompleteHandle);

        }

    };

    __init(__pdfEditor);
    return __pdfEditor;

};