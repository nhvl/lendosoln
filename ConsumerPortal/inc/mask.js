var gCurrencyNegativeColor = 'red';
var gCurrencyColor = 'black';


function f_attachEvent(o, type, listener) {
    if (o.attachEvent) {
        o.attachEvent("on" + type, listener);
    }
    else {
        o.addEventListener(type, listener);
    }
}

function _initMask(o) {
  switch (o.preset) {
    case "zipcode" : 
      o.mask = '#####';
      o.maxLength = 5; 
      o.width = 50; 
      // Due to new script of get/set cursor location in textbox, there are couple
      // of problems if apply format on init. 1) Dirty bit will be set incorrectly on 
      // first load. 2) Last input will have focus by default.
      // The easiest solution is to disable format on init. dd 9/2/2003
      //if (o.value != '') mask_keyup(o);
      f_attachEvent(o, "keyup", mask_keyup);          
      break;
    case "ssn" : 
      o.maxLength = 11; 
      o.mask = '***-**-####'; // #=digit, ?=alphanumeric, *=digit or '*'
      o.width=90;
      //if (o.value != '') mask_keyup(o);
      f_attachEvent(o, "keyup", mask_keyup);
      break;
    case "ssn-last-four":
        o.maxLength = 4;
        o.mask = '####'; // #=digit, ?=alphanumeric, *=digit or '*'
        f_attachEvent(o, "keyup", mask_keyup);
        break;
    case "phone" :
      o.mask = '(###) ###-####???????';
      o.maxLength = 21;
      o.width = 120;
      f_attachEvent(o, "keyup", mask_keyup);
      //if (o.value != '') mask_keyup(o);
      break;
    case "date" :
      o.maxLength = 10;
      o.width = 75;
      if (o.value != '') date_onblur(o);
      f_attachEvent(o, "keyup", date_keyup);
      f_attachEvent(o, "blur", date_onblur);          
      break;
    case "percent" :
      o.width = 70;
      o.style.textAlign = "right";      
      // Don't format when first load. Assume initial value already formated.
      //if (o.value == '') o.value = '0.000%';
      //else percent_onblur(o);
      f_attachEvent(o, "blur", percent_onblur);
      break;
    case "money" :
      o.width = 90;
      o.style.textAlign = "right";
      // Don't format when first load. Assume initial value already formated.
      //if (o.value == '') o.value = '$0.00';
      //else money_onblur(o);
      f_attachEvent(o, "blur", money_onblur);
      break;
    case "employerIdentificationNumber":
      o.maxLength = 10;
      o.setAttribute("mask", "##-#######");
      o.width = 90;
      if (o.attachEvent)
          o.attachEvent("onkeyup", mask_keyup);
      else
          o.addEventListener("keyup", mask_keyup, false);
      break;
  }
}

function f_fireEvent(o, event) {
    if (o.fireEvent) {
        o.fireEvent("on" + event);
    }
    else {
        var eventObj = document.createEvent("Event");
        eventObj.initEvent(event, true, true);
        o.dispatchEvent(eventObj);
    }
}

function date_keyup() {
  var e = event.srcElement;
  if (event.keyCode == 84) { // enter 'T'
    if ( e.readOnly != null && ! e.readOnly ) // 12/29/06 mf - Should not change value of read-only box
    {
      e.value = dtToString(new Date());
      // 2/11/2004 dd - set value of textbox through script does not trigger onchange event. Therefore explicit action requires.
      f_fireEvent(e, "change"); 
	
      if (typeof(updateDirtyBit) == 'function') updateDirtyBit();
    }
  }
}

function dtToString(dt) {
  return (dt.getMonth() + 1) + '/' + dt.getDate() + '/' + dt.getFullYear();
}

function mask_keyup()
{
    // Validate the event throwing object.  We choose th global
    // arguments set if the event source element is null.

    var e = event.srcElement;

    if( e == null )
    {
        e = arguments[ 0 ];
    }

    if( event.keyCode == 9 || event.keyCode == 16 || event.keyCode == 8 || event.keyCode == 37 || event.keyCode == 39 )
    {
        // Skip tab, delete, and cursor keys.

        return;
    }

    // Get the current cursor position.  This is definitely
    // jscript voodoo, but it seems to work when the text
    // box has focus.

    var t , s , pos = 0;

    s = document.selection.createRange();
    t = e.createTextRange();

    if( t.inRange( s ) == true )
    {
        // We're in range, so we have focus.

        while( t.compareEndPoints( "StartToStart" , s ) < 0 )
        {
            s.move( "character" , -1 );

            ++pos;
        }
    }

    if (e.value == '')
    {
        return;
    }

    // Walk the string and apply the mask.

    var maskIndex = 0;
    var index     = 0;
    var ret       = "";
    var value     = e.value;
    var mask      = e.mask;

    while( true )
    {
        m = mask.charAt( maskIndex );
        c = value.charAt( index );

        if( m == '#' )
        {
            if( isDigit( c ) )
            { 
                ret += c;

                if( ++maskIndex > mask.length )
                    break;
            }

            if( ++index > value.length )
                break;
        }
        else
        if (m == '*') {
            if (isDigit(c) || c == '*') {
                ret += c;

                if (++maskIndex > mask.length)
                    break;
            }

            if (++index > value.length)
                break;
        }
        else
        if( m == '?' )
        {
            if( isAlphaNumeric( c ) )
            {
                ret += c;

                if( ++maskIndex > mask.length )
                    break;
            }

            if( ++index > value.length )
                break;
        }
        else
        {
            ret += m;

            if( maskIndex <= pos && c != m )
            {
                ++pos;
            }

            if( c == m )
            {
                ++index;
            }

            if( ++maskIndex > mask.length )
                break;
        }
    }

    e.caretPos = pos;
    e.value    = ret;

    if( typeof( updateDirtyBit ) == 'function' )
    {
        updateDirtyBit();
    }

    // Update the text box with the final cursor position.
    // We use the previously discovered position, which may
    // have been updated during masking, and move the cursor
    // into that.  The final select will trigger the caret
    // to appear at the start of the moved text range, which
    // has no width.

    t = e.createTextRange();
    t.collapse();

    while( pos > 0 )
    {
        t.move( "character" , 1 );

        --pos;
    }

    t.select();
}

function isDigit(ch) { 
  return ch.match(/[0-9]/) != null;
}
function isAlphaNumeric(ch) {
  return ch.match(/[a-z0-9]/i) != null;
}
function money_onblur() {
  var e = event.srcElement == null ? arguments[0] : event.srcElement;
  format_money(e);

  if (typeof (format_money_callback) === 'function') {
      format_money_callback(e);
  }
}

function format_money(e) {
  var index = 0;
  var ret = '';
  var isNegative = false;
  var isLeadingZero = true;
  var hasDecimal = false;
  var precision = 0;
  var str = e.value.replace(/[^0-9-\(.]/g, '');

  for (index = 0; index < str.length; index++) {
    ch = str.charAt(index);
    if ((ch == '-' || ch == '(') && index == 0 && !isNegative) {
      isNegative = true;
      continue;
    }
    if (isLeadingZero && ch == '0') continue;
    if (isDigit(ch)) {
      ret += ch;
      isLeadingZero = false;
      if (hasDecimal) precision++;
      if (precision > 1) break;
    } else if (ch == '.' && !hasDecimal) {
      if (isLeadingZero) ret = '0.';
      else ret += '.';
      hasDecimal = true;
      isLeadingZero = false;
    }
  }

  if (!hasDecimal) {
     if (isLeadingZero) ret = '0.00';
     else ret += '.00';
  } else if (precision == 0) ret += '00';
  else if (precision == 1) ret += '0';

  // Place thousand separator.
  var p = ret.length - 3;
  while (p > 3) {
    ret = ret.substr(0, p - 3) + ',' + ret.substr(p - 3);
    p -= 3;
  }
  
  // 9/5/03 - For performance reason, I don't reformat when value pass back from server.
  // Therefore color code on money field will not work consistencely. 
  if (isNegative) {
    e.value = '($' + ret + ')';
  //  e.style.color = gCurrencyNegativeColor;
  } else {
    e.value = '$' + ret;
  //  e.style.color = gCurrencyColor;
  }
}
function percent_onblur() {
  var e = event.srcElement == null ? arguments[0] : event.srcElement;  
  format_percent(e);
}
function format_percent(e) {
  // Remove leading 0.
  // Append 0 if first character is .
  var index = 0;
  var ret = '';
  var isLeadingZero = true;
  var hasDecimal = false;
  var precision = 0;
  var isNegative = false;  
  for (index = 0; index < e.value.length; index++) {
    ch = e.value.charAt(index);
    if ((ch == '-' || ch == '(') && index == 0 && !isNegative) {
      isNegative = true;
      continue;
    }    
    if (isLeadingZero && ch == '0') continue;
    if (isDigit(ch)) {
      ret += ch;
      isLeadingZero = false;
      if (hasDecimal) precision++;
      if (precision > 2) break;
    } else if (ch == '.' && !hasDecimal) {
      if (isLeadingZero) ret = '0.';
      else ret += '.';
      hasDecimal = true;
      isLeadingZero = false;
      
    }
  }

  if (!hasDecimal) {
     if (isLeadingZero) ret = '0.000';
     else ret += '.000';
  } else if (precision == 0) {
     ret += '000';
  } else if (precision == 1) {
     ret += '00';
  } else if (precision == 2) {
     ret += '0';
  }

  if (isNegative)
    e.value = '-' + ret + '%';
  else
    e.value = ret + '%';

}
// Enter 't' will fill in today date
// Max length = 10.
// Enter single digit will use current month and year.
// Enter mm/dd will use current year.
// Enter mm/dd/yy if year is between 30 and 99 then yyyy = 19yy else 20yy
// Support format mmddyyyy, mm-dd-yyyy, mm/dd/yyyy
function date_onblur() {

  var e = event.srcElement == null ? arguments[0] : event.srcElement;
  if (e.value == '') return;
  var value = e.value;
  
  value = value.replace(/[^0-9\/-]/g, '').replace(/-/g,'/');
  var dt = new Date();
  var mm;
  var dd;
  var yyyy;
  if (value != '' && !isNaN(value) && parseInt(value, 10) <= 31) {
    dt.setDate(parseInt(value, 10));
  } else {
    var str = value;
   
    var parts = str.split('/');
    if (parts.length == 1) {
      // must be either mmddyy or mmddyyyy
      if (str.length == 6 || str.length == 8) {
        yyyy = getYear(parseInt(str.substr(4)));
        mm = parseInt(str.substr(0, 2), 10);
        dd = parseInt(str.substr(2, 2), 10);
        if (mm < 1 || mm > 12) {
          errorMsg('Invalid month. Month need to be from 1 to 12.', e);
          return;
        }
        if (dd < 1 || dd > 31) {
          errorMsg('Invalid date. Date need to be from 1 to 31.', e);
          return;
        } 
        dt.setFullYear(yyyy, mm - 1, dd);
      } else {
        errorMsg('Invalid date format.\n\nYou can enter dates in the following formats: mm/dd/yy, mm/dd/yyyy, mm/dd, mmddyy, mmddyyyy.', e);
        return;
      }
    } else if (parts.length == 2 || parts.length == 3) {

      for (var i = 0; i < parts.length; i++) {
        if (parts[i] == '' || isNaN(parts[i])) {
          errorMsg('Invalid date format.\n\nYou can enter dates in the following formats: mm/dd/yy, mm/dd/yyyy, mm/dd, mmddyy, mmddyyyy.', e);
          return;
        }
      }
      
        mm = parseInt(parts[0], 10);
        dd = parseInt(parts[1], 10);
        yyyy = parts.length == 3 ? getYear(parseInt(parts[2])) : dt.getFullYear();
        
        if (mm < 1 || mm > 12) {
          errorMsg('Invalid month. Month need to be from 1 to 12.', e);
          return;
        }
        if (dd < 1 || dd > 31) {
          errorMsg('Invalid date. Date need to be from 1 to 31.', e);
          return;
        } 
        if (!isValidDate(yyyy, mm, dd)) {      
          return;
        }
        dt.setFullYear(yyyy, mm - 1, dd);
        
    } else {
      errorMsg('Invalid date format.\n\nYou can enter dates in the following formats: mm/dd/yy, mm/dd/yyyy, mm/dd, mmddyy, mmddyyyy.', e);
      return;
    
    }
    
  }

  e.value = dtToString(dt);
  
}
function isValidDate(yyyy, mm, dd) {
  var months = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
  if ((mm == 4 || mm == 6 || mm == 9 || mm == 11) && dd > 30) {
    errorMsg(months[mm - 1] + ' has maximum of 30 days.');
    return false;
  } else if (mm == 2 && ((yyyy % 4) > 0) && dd > 28) {
    errorMsg('February of ' + yyyy + ' has maximum of 28 days.');
    return false;
  } else if (mm == 2 && dd > 29) {
    errorMsg('February of ' + yyyy + ' has maximum of 29 days.');
    return false;
  }
  return true;
}
function getYear(y) {
  if (y < 30)
    return y += 2000;
  else if (y < 100)
    return y += 1900;
  else
    return y;
}
function errorMsg(msg, o) {
  alert(msg);
  o.focus();
  o.select();
}

// --- Functions for time control.
function time_onminuteblur(o) {
  var a = parseInt(o.value, 10);
  if (a < 10) o.value = "0" + a;
  else if (a < 60) o.value = a;
  else o.value = "00";
}
function time_onhourkeyup(o, m) {
  // skip tab & delete
  if (event.keyCode == 9 || event.keyCode == 16 || event.keyCode == 8 || 
event.keyCode == 39 || event.keyCode == 37) return;

  var v = parseInt(o.value, 10);
  var prefix = o.value.length == 2 ? o.value.charAt(0) : "0";
  //alert(o.value + " | " + parseInt(o.value));

  if (isNaN(v)) o.value = "";
  else {
    if (v > 1 && v < 10) {
      o.value = prefix + v;
      document.all[m].select();
      document.all[m].focus();
    } else if (v > 12) {
      o.value = prefix;
    } else if (v > 9 && v < 13) {
      document.all[m].select();
      document.all[m].focus();
    }
  }
}
function time_onhourblur(o) {
  var a = parseInt(o.value, 10);
  if (a < 10) o.value = "0" + a;
  else if (a < 13) o.value = a;
  else o.value = "12";
}

