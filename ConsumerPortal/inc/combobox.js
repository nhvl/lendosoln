var	gCombobox	=	null;
var	gComboboxIndex = -1;
var gIconWidth = 18;
var gHighlightBackground = 'highlight';

function getAbsolutePos(el)	{
	var	r	=	{	x: el.offsetLeft,	y: el.offsetTop	};
	if (el.offsetParent) {
		var	tmp	=	getAbsolutePos(el.offsetParent);
		r.x	+= tmp.x;
		r.y	+= tmp.y;
	}
	return r;
}

function hideShowCovered(el) {
	var	tags = new Array("applet", "iframe", "select");

	var	p	=	getAbsolutePos(el);
	var	EX1	=	p.x;
	var	EX2	=	el.offsetWidth + EX1;
	var	EY1	=	p.y;
	var	EY2	=	el.offsetHeight	+	EY1;

	for	(var k = tags.length;	k	>	0; ) {
		var	ar = document.getElementsByTagName(tags[--k]);
		var	cc = null;

		for	(var i = ar.length;	i	>	0;)	{
			cc = ar[--i];

			p	=	getAbsolutePos(cc);
			var	CX1	=	p.x;
			var	CX2	=	cc.offsetWidth + CX1;
			var	CY1	=	p.y;
			var	CY2	=	cc.offsetHeight	+	CY1;

			if (el.style.visibility	== "hidden"	|| (CX1	>	EX2) ||	(CX2 < EX1)	|| (CY1	>	EY2) ||	(CY2 < EY1)) {
				cc.style.visibility	=	"visible";
			}	else {
				cc.style.visibility	=	"hidden";
			}
		}
	}
}	

function e_comboBoxOnClick() {
	var	e	=	event.srcElement;
	if (e	!= null	&& e.className ==	"combobox_img") return;

	if (gCombobox	!= null)
		hideCombobox();
}

function onComboboxKeypress(o) {
  
	var	keyCode	=	event.keyCode;
	//alert('keyCode = ' + keyCode);
	if (keyCode	== 38 && gCombobox != null) { //	Up
		selectItem(gComboboxIndex	== 0 ? gCombobox.rows.length - 1 : --gComboboxIndex	 % gCombobox.rows.length);
	}	else if	(keyCode ==	40)	{	// Down	arrow
		if (gCombobox	== null) showCombobox(o.id);
		selectItem(++gComboboxIndex	%	gCombobox.rows.length);
	}	else if	(keyCode ==	13 && gCombobox != null)	{	// enter key
		onItemClick(gCombobox.rows[gComboboxIndex]);
		event.returnValue	=	false;
	}	else if	(keyCode ==	27 && gCombobox != null)	{ // ESC key
		hideCombobox();
	}
}

function hideCombobox()	{
	gCombobox.style.visibility = "hidden";
	gCombobox.style.display = "none";
	hideShowCovered(gCombobox);
	gCombobox	=	null;
	gComboboxIndex = -1;
}

function showCombobox(id)	{
  if (gCombobox	!= null) hideCombobox(); //	Hide previous	display	combo	box.

	var	e	=	document.getElementById(id);
	if (e.readOnly && e.NotEditable == null) return;
	e.focus(); // set focus on text box.
	var r = getAbsolutePos(e);
	var x = r.x;
	var y = r.y + e.offsetHeight;
	//var	x	=	e.offsetLeft;
	//var	y	=	e.offsetTop	+	e.offsetHeight;
	var w = e.offsetWidth + gIconWidth;
		
	//alert("x = " + x + ", y = " + y + ", w = " + w);
	var	cbl	=	document.getElementById(id + "_cbl");
	cbl.style.visibility = "visible";
	cbl.style.display = "";
	cbl.style.left = x;
	cbl.style.top	=	y;
	cbl.style.width = w;
	gCombobox	=	cbl;
  gCombobox.TextboxID = id;
	gComboboxIndex = 0;
	for	(var i = 0;	i	<	gCombobox.rows.length; i++)	{
	  gCombobox.rows[i].className = (i == Math.abs(gComboboxIndex)) ? 'combobox_select' : '';	
	  //gCombobox.rows[i].style.backgroundColor	=	(i ==	Math.abs(gComboboxIndex))	?	gHighlightBackground	:	'';
	}
	hideShowCovered(gCombobox);		 
}
function onSelect(id)	{
  if (gCombobox	== null)
		showCombobox(id);
	else
		hideCombobox();
}

function f_fireEvent(o, event) {
    if (o.fireEvent) {
        o.fireEvent("on" + event);
    }
    else {
        var eventObj = document.createEvent("Event");
        eventObj.initEvent(event, true, true);
        o.dispatchEvent(eventObj);
    }
}

function onItemClick(o)	{
	if (o.firstChild.innerText)	{
		var	tb = document.getElementById(gCombobox.TextboxID);
		tb.value = o.firstChild.innerText;
		if (typeof(updateDirtyBit) == 'function') updateDirtyBit(); // Dependacy for LendingQB
		f_fireEvent(tb, 'change');
	}
	hideCombobox();
}

function selectItem(rowIndex)	{
  if (rowIndex != null) gComboboxIndex = rowIndex;
	if (gCombobox	== null) return;
	for	(var i = 0;	i	<	gCombobox.rows.length; i++)	{
	  gCombobox.rows[i].className = (i == Math.abs(gComboboxIndex)) ? 'combobox_select' : 'combobox_unselect';
		//gCombobox.rows[i].style.backgroundColor	=	(i ==	Math.abs(gComboboxIndex))	?	gHighlightBackground	:	'';
		//gCombobox.rows[i].style.color	=	(i ==	Math.abs(gComboboxIndex))	?	'white'	:	'black';		
	}
}

function createCombobox(id, items) {
  var tbl = document.createElement("TABLE");
  tbl.id = id + "_cbl";
  tbl.className = "combobox";
  tbl.style.position = "absolute";
  tbl.style.display = "none";
  tbl.style.visibility = "hidden";
  tbl.cellPadding = 0;
  tbl.cellSpacing = 0;
	for	(var i = 0;	i	<	items.length;	i++) {
		var	oRow = tbl.insertRow();
		oRow.onmouseover = function()	{	selectItem(this.rowIndex); };
		oRow.onclick = function()	{	onItemClick(this); };

		var	oCell	=	oRow.insertCell();
		oCell.width	=	"100%";
		if (items[i] == " ")
		  oCell.innerHTML = "&nbsp;";
		else
		  oCell.innerText	=	items[i];
		
	}
  document.body.appendChild(tbl);

}

function f_attachEvent(o, type, listener) {
    if (o.attachEvent) {
        o.attachEvent("on" + type, listener);
    }
    else {
        o.addEventListener(type, listener);
    }
}

f_attachEvent(document, "click", e_comboBoxOnClick);