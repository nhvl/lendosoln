﻿if (!this.ManageSigners) {
    this.ManageSigners = ({});

    ManageSigners = (function(_req, _log) {
        var _RequestId = _req;
        var _self = this;
        var _Logger = _log;
        var _REQUEST_TIMEOUT = 30 * 1000; //30sec
        var _IsRequestInProgress = false;

        //Add public variables
        this.aBSignatureCurrentSignCount = 0;
        this.aBSignatureTotal = 0;
        this.aCSignatureCurrentSignCount = 0;
        this.aCSignatureTotal = 0;
        this.aBNm = '';
        this.aCNm = '';
        this.aBIni = '';
        this.aCIni = '';
        this.aBHasSigned = false;
        this.aCHasSigned = false;

        this.Current = new Signer();
        this.Available = new SignerList();

        this.GetCurrentSigner = function(f_complete) {
            if (_IsRequestInProgress) return;
            UpdateRequest(true);
            $j.ajax({
                type: "POST",
                url: "ManageSigner.aspx/GetCurrentSigner",
                data: "{'requestId': " + _RequestId + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    try {
                        UpdateRequest(false);
                        var signer = new Signer(msg.d);
                        _self.Current = signer;
                        if (typeof (f_complete) === 'function') {
                            f_complete(signer);
                        }
                    } catch (e) {
                        Log('Error retrieving current signer. (onsuccess)', e);
                        UpdateRequest(false);
                    }
                },
                error: function(e) {
                    try {
                        UpdateRequest(false);
                        Log('Error retrieving current signer. (onerror)', e);
                        var signer = new Signer();
                        _self.Current = signer;
                        if (typeof (f_complete) === 'function') {
                            f_complete(signer);
                        }
                    } catch (e2) {
                        Log('Error retrieving current signer. (onerror2)', e2);
                        UpdateRequest(false);
                    }
                }
            });
        };

        this.GetAvailableSigners = function(f_complete) {
            if (_IsRequestInProgress) return;
            UpdateRequest(true);

            $j.ajax({
                type: "POST",
                url: "ManageSigner.aspx/GetAvailableSigners",
                data: "{'requestId': " + _RequestId + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    try {
                        UpdateRequest(false);
                        var signers = new SignerList(msg.d);
                        _self.Available = signers;
                        if (typeof (f_complete) === 'function') {
                            f_complete(signers);
                        }
                    } catch (e) {
                        Log('Error retrieving available signers. (onsuccess)', e);
                        UpdateRequest(false);
                    }
                },
                error: function(e) {
                    try {
                        UpdateRequest(false);
                        Log('Error retrieving available signers. (onerror)', e);
                        var signers = new SignerList();
                        _self.Available = signers;
                        if (typeof (f_complete) === 'function') {
                            f_complete(signers);
                        }
                    } catch (e2) {
                        Log('Error retrieving available signers. (onerror2)', e2);
                        UpdateRequest(false);
                    }
                }
            });
        };

        this.SetCurrentSigner = function(currentName, f_complete) {
            if (_IsRequestInProgress) return;
            UpdateRequest(true);
            $j.ajax({
                type: "POST",
                url: "ManageSigner.aspx/SetCurrentSigner",
                data: "{'requestId': " + _RequestId + ", 'SignerName': '" + currentName + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    try {
                        UpdateRequest(false);
                        var signer = new Signer(msg.d);
                        _self.Current = signer;
                        if (typeof (f_complete) === 'function') {
                            f_complete(signer);
                        }
                    } catch (e) {
                        Log('Error setting current signer. (onsuccess)', e);
                        UpdateRequest(false);
                    }
                },
                error: function(e) {
                    try {
                        UpdateRequest(false);
                        Log('Error setting current signer. (onerror)', e);
                        var signer = new Signer();
                        _self.Current = signer;
                        if (typeof (f_complete) === 'function') {
                            f_complete(signer);
                        }
                    } catch (e2) {
                        Log('Error setting current signer. (onerror2)', e2);
                        UpdateRequest(false);
                    }
                }
            });
        };

        this.Initialize = function(o) {
            if (typeof o === 'undefined') return;
            if (o == null) return;
            if (typeof o === 'function') {
                return o.apply(this, null);
            }
        }

        var Log = function(msg, e) {
            try { _Logger.Error(msg, e); } catch (e) { }
        };

        var UpdateRequest = function(b) {
            try {
                if (b) { _IsRequestInProgress = true; }
                else _IsRequestInProgress = false;
            } catch (e) { }
        };

    });

    var Signer = (function(o) {
        this.IsValid = false;
        this.FullName = '';
        this.Initials = '';
        this.CanSign = false;
        this.SignatureId = '';
        this.ErrorRedirect = false;

        if (typeof (o) !== 'undefined' && o !== null) {
            this.IsValid = o.IsValid;
            this.FullName = o.IsValid ? o.FullName : '';
            this.Initials = o.IsValid ? o.Initials : '';
            this.CanSign = o.IsValid ? o.CanSign : false;
            this.SignatureId = o.IsValid ? o.SignatureId : '';
            this.ErrorRedirect = o.ErrorRedirect;
        }

    });

    var SignerList = (function(o) {
        this.IsValid = false;

        this.aBNm = '';
        this.aCNm = '';
        this.IsApplicableToBorrower = false;
        this.IsApplicableToCoborrower = false;
        this.CurrentSigner = '';
        this.CurrentLoggedIn = '';
        this.ErrorRedirect = false;

        if (typeof (o) !== 'undefined' && o !== null) {
            this.IsValid = o.IsValid;
            this.aBNm = o.IsValid ? o.aBNm : '';
            this.aCNm = o.IsValid ? o.aCNm : '';
            this.IsApplicableToBorrower = o.IsValid ? o.IsApplicableToBorrower : false;
            this.IsApplicableToCoborrower = o.IsValid ? o.IsApplicableToCoborrower : false;
            this.CurrentSigner = o.IsValid ? o.CurrentSigner : '';
            this.CurrentLoggedIn = o.IsValid ? o.CurrentLoggedIn : '';
            this.ErrorRedirect = o.ErrorRedirect;
        }
    });

}