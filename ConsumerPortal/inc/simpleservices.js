var gService = new SimpleService();

function SimpleService() {
  this.register = f_register;
}

function f_register(url, name) {
  this[name] = new UrlService(url);
}

function UrlService(url) {
  this.url = url;
  this.call = f_call;
}
function buildXmlRequest(args) {
  if (args == null) {
    return '<request><data/></request>'
  } else {
    var str = '';
    for (p in args) {
      str += p + '="' + encodeValue(args[p]) + '" ';
    }
    if (typeof pml_action == 'function') pml_action('buildXmlRequest', str);
    return '<request><data ' + str + '/></request>';

  }
}

function encodeValue(str) {
  var ret = '';
  if (str == null)
  {
      str = '';
  }
  else
  {
      str = str.toString();
  }
  for (var i = 0; i < str.length; i++) {
    var c = str.charAt(i);
    switch (c) {
      case '&': ret += '&amp;'; break;
      case '<': ret += '&lt;'; break;
      case '>': ret += '&gt;'; break;
      case '"': ret += '&quot;'; break;
      case "'": ret += '&apos;'; break;
      default: ret += c;
    }
  }
  return ret;
}
// Only do synchronous call.
function f_call(methodName, args, keepQuiet, bUseParentForError) {
  var ret = new Object();
  ret.error = true;
  var sErrorUrl = gVirtualRoot + '/common/AppError.aspx';
  var sErrorMsg = '';
  var dtStarted = new Date();

  for (var i = 0; i < 3; i++) {
    try {
      var oHttp = null;

      if (window.XMLHttpRequest) {
          oHttp = new XMLHttpRequest();
      } else {
        oHttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      oHttp.open("POST", this.url + '?method=' + methodName, false);
      oHttp.setRequestHeader('Content-Type', 'text/xml');
      oHttp.send(buildXmlRequest(args));

      var doc = oHttp.responseXML;
      if (doc == null) throw ("doc is null." + oHttp.responseText);
      if (doc.documentElement == null) {
        if (oHttp.responseText.indexOf("LP_f6d1d39dee3c435d9d14f1dc245e316d") != -1) {
          if (typeof(clearDirty) == 'function') clearDirty();
          ret.error = true;
          ret.UserMessage = 'Your session is expired. Please login again.';
          top.location = gVirtualRoot + '/SessionExpired.aspx';
          return ret;
        }
        else
          throw ("doc.documentElement is null. " + oHttp.responseText);        
      }     
      if (typeof pml_action == 'function') pml_action('f_call_response', oHttp.responseText);
      var rec = doc.documentElement.selectSingleNode("data");
      if (rec != null) {
        ret.value = new Object();
        ret.error = false;
        var coll = rec.attributes;
        for (var i = 0; i < coll.length; i++) {
          var a = coll.item(i);
          ret.value[a.name] = a.value;
        }
      }

      rec = doc.documentElement.selectSingleNode("error");
      if (rec != null) {
        if (typeof (clearDirty) == "function") clearDirty();
        var errorUrl = sErrorUrl + '?id=' + rec.getAttribute("ErrorReferenceNumber");
        if (bUseParentForError != null && bUseParentForError)
          parent.location = errorUrl;
        else
          top.location = errorUrl;
      }
      // TODO: Handle error.
      return ret;

    } catch (e) {
      if (null != e) {
        if (typeof (e) === 'string') {
          sErrorMsg = e;
        } else {
          sErrorMsg = e.message;
        }
      }
    }
  }
  if (typeof (clearDirty) == "function") clearDirty();
  var duration = new Date() - dtStarted;
  var tmp = this.url + '::' + methodName + '. Duration=' + duration + '. Err=' + sErrorMsg;
  var maxLength = tmp.length < 500 ? tmp.length : 500;

  top.location = sErrorUrl + '?id=AJAX&msg=' + escape(tmp.substring(0, maxLength));
  ret.error = true;

  ret.errorDetail = 'Internal error'; // TODO: More descriptive error message.
  return ret;

}


