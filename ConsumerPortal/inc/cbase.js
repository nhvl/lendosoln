﻿(function() {
    if (typeof $j == 'undefined') return;
    
    $j(document).ready(function(){
        $j('form input').keypress(function(e){
            var code = (e.keyCode ? e.keyCode : e.which);
            return (code != 13);
        });
    });
}());