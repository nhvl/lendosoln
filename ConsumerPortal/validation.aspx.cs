﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ConsumerPortal.lib;
using LendersOffice.ObjLib.Security;
using System.Text.RegularExpressions;
using DataAccess;
using ConsumerPortal.Common;

namespace ConsumerPortal
{
    public partial class validation : ConsumerPortal.common.BaseCPortalPage
    {
        ConsumerUserPrincipal consumer = CPTools.CurrentUser;

        protected void Page_Load(object sender, EventArgs e)
        {
            ErrorMessages.Visible = false;

            if (!Page.IsPostBack)
            {
                E_ConsumerSSnMatch match = E_ConsumerSSnMatch.NoMatch;

                //list valid ssn for this consumer
                if (!string.IsNullOrEmpty(consumer.LastSsnDigits) && consumer.LastSsnDigits.Length == 4)
                {
                    match = consumer.VerifySsn(consumer.LastSsnDigits);
                }
                if (match == E_ConsumerSSnMatch.FullMatch)
                {
                    CPTools.RedirectPipeline();
                }
                else
                {
                    //Check if he is allowed to try
                    DateTime next = DateTime.MaxValue;
                    if (!PasswordHelper.CanAttemptReadSsn(consumer, out next))
                    {
                        string msg = ConsumerPortal.Common.ErrorMessages.SsnTimeoutAttempt;
                        TimeSpan min = next.Subtract(System.DateTime.Now);
                        string time = "";

                        if (min.TotalMinutes < 60)
                        {
                            time += ConsumerPortal.Common.ErrorMessages.SsnTimeoutMinutes;
                        }
                        else if (min.TotalMinutes < 601)
                        {
                            time += ConsumerPortal.Common.ErrorMessages.SsnTimeoutHours;
                        }
                        else
                        {
                            time += ConsumerPortal.Common.ErrorMessages.SsnTimeoutTomorrow;
                        }
                        SsnPanel.Visible = false;
                        ShowErrorLabel(string.Format(msg, time));
                    }
                }
            }
            else
            {

            }
        }

        protected void OnConfirmClick(object sender, System.EventArgs a)
        {
            string ssn = SsnDigits.Text;

            if (string.IsNullOrEmpty(ssn) || ssn.Length != 4 || !Regex.IsMatch(ssn, @"\d\d\d\d"))
            {
                ShowErrorLabel(ConsumerPortal.Common.ErrorMessages.SsnInvalidFormat);
                SsnDigits.Text = "";
                return;
            }

            //Check if he is allowed to try
            DateTime next = DateTime.MaxValue;
            if (!PasswordHelper.CanAttemptReadSsn(consumer, out next))
            {
                string msg = ConsumerPortal.Common.ErrorMessages.SsnTimeoutAttempt;
                TimeSpan min = next.Subtract(System.DateTime.Now);
                string time = "";

                if (min.TotalMinutes < 60)
                {
                    time += ConsumerPortal.Common.ErrorMessages.SsnTimeoutMinutes;
                }
                else if (min.TotalMinutes < 601)
                {
                    time += ConsumerPortal.Common.ErrorMessages.SsnTimeoutHours;
                }
                else
                {
                    time += ConsumerPortal.Common.ErrorMessages.SsnTimeoutTomorrow;
                }

                ShowErrorLabel(string.Format(msg, time));
                SsnDigits.Text = "";
                return;
            }

            E_ConsumerSSnMatch match = consumer.VerifySsn(ssn);
            if (match == E_ConsumerSSnMatch.PartialMatch || match == E_ConsumerSSnMatch.FullMatch)
            {
                PasswordHelper.SetValidSsn(consumer, ssn);
                CPTools.RedirectPipeline(); //go to pipeline
            }
            else
            {
                PasswordHelper.IncreaseMissedSsn(consumer);
                
                ShowErrorLabel(ConsumerPortal.Common.ErrorMessages.SsnNotFound); //show error message
                SsnDigits.Text = "";
            }
            
        }

        private void ShowErrorLabel(string msg)
        {
            if (string.IsNullOrEmpty(msg))
            {
                m_ErrMsg.Text = "";
                ErrorMessages.Visible = false;
                return;
            }

            ErrorMessages.Visible = true;
            m_ErrMsg.Text = msg;
        }
    }
}
