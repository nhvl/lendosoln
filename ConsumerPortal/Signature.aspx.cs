﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.PdfLayout;

namespace ConsumerPortal
{
    public partial class Signature : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string text = RequestHelper.GetSafeQueryString("name");
            string mode = RequestHelper.GetSafeQueryString("mode");
            byte[] buffer = null;
            if (mode == "initial")
            {
                buffer = SignatureUtilities.ConvertToInitialImage(text, 60, 12);
            }
            else
            {
                buffer = SignatureUtilities.ConvertToSignatureImage(text, 200, 20);
            }
            RenderPng(buffer);
        }
        private void RenderPng(byte[] buffer)
        {
            Response.Clear();
            Response.ContentType = "image/png";
            Response.Cache.SetExpires(DateTime.Now.AddDays(1));
            Response.Cache.SetLastModified(DateTime.Now);

            Response.OutputStream.Write(buffer, 0, buffer.Length);
            Response.End();
        }
    }
}
