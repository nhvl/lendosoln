﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="ConsumerPortal.login" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <!--[if lte IE 7]><style type="text/css"> body { padding-top:75px; }</style><![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div id="content">
        <table id="LoginPage" width="100%">
            <tr>
                <td align="center">
                    <img src="#" id="logo" alt="LOGO" runat="server" />
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td align="center">
                    <table class="login" width="500">
                        <tr>
                            <td align="right">
                                E-mail:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="LoginName" CssClass="login" runat="server" MaxLength="80" autocomplete="off" TabIndex="1"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Password:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="Password" CssClass="pword" runat="server" TextMode="Password" MaxLength="40" autocomplete="off" TabIndex="2"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td align="left" colspan="2">
                                <asp:Button ID="m_login" CssClass="Loginbtn" runat="server" Text="Login" TabIndex="3" OnClick="OnLoginClick"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="2" align="left">
                                <a href="#" id="forgot" >Forgot password?</a>
                            </td>
                        </tr>
                        </table>
                        <asp:PlaceHolder id="ErrorMessages" runat="server">
                        <table>
                            <tr>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle" colspan="3">
                                    <b>
                                        <ml:EncodedLabel ID="m_ErrMsg" CssClass="ErrorLabel" runat="server" ForeColor="Red" EnableViewState="false"></ml:EncodedLabel><br/>
                                        <ml:EncodedLabel ID="m_ErrMsg2" CssClass="ErrorLabel" runat="server" ForeColor="Red" EnableViewState="false"></ml:EncodedLabel>
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td align="middle" colspan="3">
                                    <div id="MessageLabel" />
                                </td>
                            </tr>
                        </table>
                        </asp:PlaceHolder>
                </td>
            </tr>
        </table>
    </div>
    </form>
    <script type="text/javascript">
    <!--
        $j(document).ready(function() {
            $j('#logo').error(function () {
              $j(this).unbind("error").attr("src", "images/spacer.gif").width(216).height(59);
            });
            
            $j("input[type='text'], input[type='password']").keypress(function(e){
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13){
                 $j('#m_login').click();
                }            
            });
            $j("#forgot").click(function(event){
                 event.preventDefault();
                 window.location = "password/resetpassword.aspx?SiteId=" + <%= AspxTools.JsString(SiteId) %>;
               });
            $j("#LoginName").focus();
        });
        
    //-->
    </script>
</body>
</html>
