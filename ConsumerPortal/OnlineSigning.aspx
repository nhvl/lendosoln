﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnlineSigning.aspx.cs" Inherits="ConsumerPortal.OnlineSigning" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Online Signing</title>
    <style type="text/css">
        
   </style>
</head>
<body>
<asp:PlaceHolder ID="InitScript" Visible="false" runat="server">
<script type="text/javascript">
    try { window.opener.refreshLoan(); } catch (e) { }
    self.close();
</script>
Loading...
</asp:PlaceHolder>
<asp:PlaceHolder ID="EditorScript" runat="server">
<script type="text/javascript">
  var pdfEditor;
  var signerManager;
  var _currentTimeout = null;
  var gVirtualRoot = <%= AspxTools.JsString(Tools.VRoot) %>;
  var CurrentRequestId = <%=AspxTools.JsNumeric(RequestId) %>;
  var CurrentUser = null;
  var CurrentUserSignatureId = null;
  var CurrentInitials = null;
  
  function RedirectOnError(msg){
    var param = '';
    if (typeof msg == 'string' && msg != null){
        param = "?id=AJAX" + "&msg=" + msg;
    }
    var url = 'common/AppError.aspx' + param
    window.location.href = url;
  }
  
  function rd(b){
    if (b) return 'disabled';
    return '';
  }

  var __onRetrieveCurrentSigner = function(o) {
      try {
          if (o.IsValid) {
              $j("#AvailableSigner").show();
              $j("#DetectSigner").hide();
              CurrentUser = o.FullName;
              CurrentUserSignatureId = o.SignatureId;
              CurrentInitials = o.Initials;
              $j("#CurrentlySigning").text(CurrentUser);
              _initialize();
          }
          else {
              if (o.ErrorRedirect){
                RedirectOnError();
              }
              else{
                $j("#AvailableSigner").hide();
                $j("#DetectSigner").show();
                f_ChangeCurrentSigner(true);
              }
          }
      } catch (e) { logger.Error('Error in "__onRetrieveCurrentSigner()"', e); }
      f_loading(false);
  };
  
  var __isinit = false;
  
  function _initialize(){
    if (__isinit == false)
        __isinit = true;
    else return;
    
    var ErrorMsg = function(){};
    
    pdfEditor.Initialize(function() {
          $j.each(PdfFields, function() {
              try{
                  var o = pdfEditor.AddPdfField(this, true);
                  var signatureInfo = $j(o).data('pdfFieldData');
                  if (typeof signatureInfo != 'undefined' && signatureInfo != null){
                      if (signatureInfo.Name == 'aCSignature' || signatureInfo.Name == 'aCInitials') {
                        signerManager.aCSignatureTotal++;
                      } else {
                          signerManager.aBSignatureTotal++;
                      }
                      if ( IsUserMatch(CurrentUserSignatureId, signatureInfo.Name) ) {
                          _clearSignature(o);
                      } else {
                          _otherUserSignature(o);
                      }
                      $j(o).addClass(signatureInfo.Name);
                      updateSigningStatus();              
                  }else{ logger.Warning('Unable to extract $j(o).data("pdfFieldData") data.(1)'); }
              }catch(e){};
              
          });
      });
    
  }
  
  
  function f_ChangeCurrentSigner(isForced){
        var __onRetrieveAvailableSigners = function(o) {
            try {
                if (o.IsValid) {
                    $j('#ChangeToBorrower').text(o.aBNm);
                    $j('#ChangeToCoborrower').text(o.aCNm);
                    
                    var nSelected = 0;
                                        
                    if (!o.IsApplicableToBorrower){
                        $j('#BorrowerRow').hide();
                    }
                    else{
                        var bLoggedInMsg = '';
                        if (o.aBNm == o.CurrentLoggedIn) { bLoggedInMsg += '(Logged in As) ';  } 
                        if (o.aBNm == o.CurrentSigner)   {
                            bLoggedInMsg += '(Current Signer)';
                            $j("#BorrowerRow input[type='radio']").attr('checked', true);
                            $j('#ChangeSignerBtn').data('Name', o.CurrentSigner);
                            nSelected++;
                        }
                        $j('#BorrowerLoggedIn').text(bLoggedInMsg);
                    }
                    
                    if (!o.IsApplicableToCoborrower){
                        $j('#CoborrowerRow').hide();
                    }
                    else{
                        var cbLoggedInMsg = '';
                        if (o.aCNm == o.CurrentLoggedIn) { cbLoggedInMsg += '(Logged in As) ';  } 
                        if (o.aCNm == o.CurrentSigner)   {
                            cbLoggedInMsg += '(Current Signer)';
                            $j("#CoborrowerRow input[type='radio']").attr('checked', true);
                            $j('#ChangeSignerBtn').data('Name', o.CurrentSigner);
                            nSelected++;
                        }
                        $j('#CoborrowerLoggedIn').text(cbLoggedInMsg);
                    }
                    
                    $j('#ConfirmChangeSignerRow').hide();
                    
                    $j('#ChangeSignerBtn').attr("disabled", (nSelected==0) );
                    $j('#ChangeSignerCancelBtn').attr("disabled", isForced);
                    
                    $j("#ChangeSigner input[type='radio']").map(function(index, el) {
                        var name = '';
                        (index == 0) ? name = o.aBNm : name = o.aCNm;
                        $j(el).data('Name', name);
                    });
                    
                    $j('#ChangeSigner').dialog('open');
                }
                else {
                      ErrorMsg('Unable to perform action. Please try again later.');
                      if (o.ErrorRedirect){
                        RedirectOnError();
                      }
              
                }
            } catch (e) { logger.Error('Error in "__onRetrieveAvailableSigners()".', e); }
            f_loading(false);
          };
        
        f_loading(true);
        signerManager.GetAvailableSigners(__onRetrieveAvailableSigners);
  }
  
  function __onSetCurrentSigner(o){
        try{
            if (o.IsValid) {
              CurrentUser = o.FullName;
              CurrentInitials = o.Initials;
              CurrentUserSignatureId = o.SignatureId;

              $j("#AvailableSigner").show();
              $j("#DetectSigner").hide();
              
              var yellowbk = $j(".yellowbk");
              var graybk = $j(".graybk");
              
              yellowbk.each(function(){
                    $j(this).removeClass('yellowbk').addClass('graybk').css('background-color','#d3d3d3');
              });
              graybk.each(function(){
                    $j(this).removeClass('graybk').addClass('yellowbk').css('background-color','yellow');
              });
              
              $j('.aBSignature div.emptysign').each(function(){
                    var txt = '';
                    (CurrentUserSignatureId == 'aBSignature') ? txt = 'Click to sign' : txt = 'To be signed by ' + signerManager.aBNm;
                    $j(this).text(txt);
              });
              
              $j('.aCSignature div.emptysign').each(function(){
                    var txt = '';
                    (CurrentUserSignatureId == 'aCSignature') ? txt = 'Click to sign' : txt = 'To be signed by ' + signerManager.aCNm;
                    $j(this).text(txt);
              });
              
              $j("#CurrentlySigning").text(CurrentUser);
              $j('#ChangeSigner').dialog('close');
              _initialize(); //In case the borrower wasn't detected
            }
            else{
                ErrorMsg('Unable to perform action. Please try again later.');
                if (o.ErrorRedirect){
                    RedirectOnError();
                }
            }
            
        }catch(e){ logger.Error('Error in "__onSetCurrentSigner()"', e); }
        
        f_loading(false);
  }

  function f_updateMsgRow(o, name){
    $j('#ConfirmChangeSignerBox').attr('checked', false);
    $j('#ConfirmChangeSignerLabel').text('I confirm that I am ' + name );
    $j('#ChangeSignerBtn').data('Name', name);
    
    if (o.CurrentLoggedIn == name){
        $j('#ConfirmChangeSignerRow').hide();
        $j('#ChangeSignerBtn').attr('disabled', rd(false));
    }
    else{
        $j('#ConfirmChangeSignerRow').show();
        $j('#ChangeSignerBtn').attr('disabled', rd(true));
    }
  }


  $j(document).ready(function() {
      signerManager = new ManageSigners(CurrentRequestId, logger);
      signerManager.aBNm = <%=AspxTools.JsString(aBNm) %>;
      signerManager.aCNm = <%=AspxTools.JsString(aCNm) %>;
      signerManager.aBIni = <%=AspxTools.JsString(aBIni) %>;
      signerManager.aCIni = <%=AspxTools.JsString(aCIni) %>;
      
      pdfEditor = new LendersOffice.PdfEditor('signingEditor', SigningPages, {
          AllowEdit: false,
          GenerateFullPageSrc: function(o) { return 'ViewDocumentSigning.aspx?requestid=' + o.RequestId + '&pg=' + o.Page + '&seed=' + ((new Date()).getTime()) + '&sessionid=' + <%=AspxTools.JsString(SessionId) %>; },
          EmptyImgSrc : gVirtualRoot + '/images/spacer.gif'
      });
      
      f_loading(true);
      signerManager.GetCurrentSigner(__onRetrieveCurrentSigner);

      $j("#ChangeSignerLink").click(function(e) {
          f_ChangeCurrentSigner(false);
      });
      
      $j('#ConfirmSigningInput').css('text-transform', 'uppercase');

      $j('#ConfirmSigning').dialog({
          draggable: false,
          resizable: false,
          title: "Sign",
          autoOpen: false,
          height: 140,
          width: 350,
          modal: true,
          open: function(event, ui) {
              var btnLabel = 'OK';
              var btnDisabled = true;
              var inputDisabled = false;
              $j('#ConfirmSigningInput').val('');
              
              var signatureInfo = $j(__selectedSignatureField).data('pdfFieldData');
              var msg = 'Type your full name as shown to sign';
              var txtBoxContent = CurrentUser.toUpperCase();
              if (typeof signatureInfo != 'undefined' && signatureInfo != null){
                if (signatureInfo.Name == 'aBInitials' || signatureInfo.Name == 'aCInitials') {
                    msg = 'Type your initials as shown to sign';
                    txtBoxContent = CurrentInitials.toUpperCase();
                }
              }else{ logger.Warning('Unable to extract $j(o).data("pdfFieldData") data.(2)'); }
              
              var bHasSignature = $j(__selectedSignatureField).data('field-has-signature');
              
              if (typeof bHasSignature != 'undefined' && bHasSignature != null){
                  if (bHasSignature) {
                    btnLabel = 'Clear';
                    btnDisabled = false;
                    inputDisabled = true;
                    $j('#ConfirmSigningInput').val(txtBoxContent);
                  }
              }else{ logger.Warning('Unable to extract $j(__selectedSignatureField).data("field-has-signature") data.(0)'); }
              
              $j('#ConfirmSigning p').text(msg);
              $j('#ConfirmSigningOkBtn').attr('value',  btnLabel  ).attr('disabled', rd(btnDisabled));
              $j('#ConfirmSigningInput').attr('disabled', rd(inputDisabled));
              $j('#ConfirmSigningInput').focus();
          }
      });
      
      $j("#ConfirmSigningInput").keypress(function(e){
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13){
          $j('#ConfirmSigningOkBtn:enabled').click();
        }            
      });
      
      $j('#signingEditor').delegate('.pdf-pdf-field', 'click', _onFieldClick);
      $j('#ConfirmSigningInput').keyup(function() {
            var signatureInfo = $j(__selectedSignatureField).data('pdfFieldData');
            var isDisabled = false;
            if (typeof signatureInfo != 'undefined' && signatureInfo != null){
                if (signatureInfo.Name == 'aBInitials' || signatureInfo.Name == 'aCInitials') {
                    isDisabled = $j(this).val().toUpperCase() != CurrentInitials.toUpperCase();
                }
                else {
                    isDisabled = $j(this).val().toUpperCase() != CurrentUser.toUpperCase();
                }
            }else{ logger.Warning('Unable to extract $j(o).data("pdfFieldData") data.(3)'); }
            var txt = 'OK';
            
            var bHasSignature = $j(__selectedSignatureField).data('field-has-signature');
              
            if (typeof bHasSignature != 'undefined' && bHasSignature != null){
                if (bHasSignature) { txt = 'Clear'; isDisabled = false; }
            }else{ logger.Warning('Unable to extract $j(__selectedSignatureField).data("field-has-signature") data.(1)'); }
            
            $j('#ConfirmSigningOkBtn').attr('disabled', rd(isDisabled)).attr('value',  txt  );
        });
      $j('#ConfirmSigningCancelBtn').click(function() {
          $j('#ConfirmSigningInput').val('');
          $j('#ConfirmSigning').dialog('close');
      });

      $j('#ConfirmSigningOkBtn').click(function() {
          var v = $j('#ConfirmSigningInput').val();
          $j('#ConfirmSigningInput').val('');
          $j('#ConfirmSigning').dialog('close');

          var bHasSignature = $j(__selectedSignatureField).data('field-has-signature');
          var delta = 0;

          if (bHasSignature) {
              delta = -1;
              _clearSignature(__selectedSignatureField);
          } else if (v != '' && !bHasSignature) {
              delta = 1;
              _addSignature(__selectedSignatureField);
          }

          if (CurrentUserSignatureId == 'aBSignature') {
              signerManager.aBSignatureCurrentSignCount += delta;
              signerManager.aBHasSigned = true;
          } else if (CurrentUserSignatureId == 'aCSignature') {
              signerManager.aCSignatureCurrentSignCount += delta;
              signerManager.aCHasSigned = true;
          }
          updateSigningStatus();

          __selectedSignatureField = null;

      });
      
      $j("#ChangeSigner input[type='radio']").click(function(){
          var name = $j(this).data('Name');
          f_updateMsgRow(signerManager.Available, name);
      });
      
      $j("#ConfirmChangeSignerBox").click(function(){
        $j('#ChangeSignerBtn').attr('disabled', rd( !($j(this).is(":checked"))) );
      });
                    
                    
      $j('#ChangeSignerBtn').click(function(){
        f_loading(true, 'Saving...');
        signerManager.SetCurrentSigner($j(this).data('Name'), __onSetCurrentSigner);
      });

      $j('#ChangeSigner').dialog({
          closeOnEscape: false,
          draggable: false,
          resizable: false,
          title: "Change Signer",
          autoOpen: false,
          height: 210,
          width: 450,
          modal: true,
          open: function(event, ui) {
              $j('#ChangeSignerCancelBtn').focus();
              var closeBtn = $j(this).parent().children().children('.ui-dialog-titlebar-close');
              (__isinit == true) ? closeBtn.show() : closeBtn.hide();
          }
      });

      $j('#ChangeSignerCancelBtn').click(function() {
          $j('#ChangeSigner').dialog('close');
      });

      $j("#SignBtn").click(function(ev){
        var aBNumLocations = (signerManager.aBSignatureTotal - signerManager.aBSignatureCurrentSignCount);
        var aCNumLocations = (signerManager.aCSignatureTotal - signerManager.aCSignatureCurrentSignCount);
        
        if (signerManager.aBHasSigned && aBNumLocations > 0){
            alert('Borrower still needs to sign ' + aBNumLocations + ' locations.');
            return false;
        }
        if (signerManager.aCHasSigned && aCNumLocations > 0){
            alert('Cobborrower still needs to sign ' + aCNumLocations + ' locations.');
            return false;
        }
        
        $j(this).attr('disabled', rd(true));
        
        if (signerManager.aBHasSigned || signerManager.aCHasSigned){
            var val = 'both';
            if (!signerManager.aBHasSigned || !signerManager.aCHasSigned){
                (signerManager.aBHasSigned) ? val = 'aBSignature' : val = 'aCSignature';
            }
            $j('#m_Signer').val(val);
            $j('#ServerSignBtn').click();
            return true;
        }
        else{
            self.close();
            ev.preventDefault();
            return false;
        }
        
      });
      
      ErrorMsg = function(msg){
        if (typeof msg != 'string' || msg == null)
            return;
        
        if (_currentTimeout){
            clearTimeout(_currentTimeout);
            _currentTimeout = null;
        }
        
        if (msg.length != 0){
            $j('#ErrorMsgPanel p').text(msg);
            $j('#ErrorMsgPanel').show('slow');
            _currentTimeout = setTimeout(function(){
                $j('#ErrorMsgPanel p').text('');
                $j('#ErrorMsgPanel').hide('slow');
            }, 5000);
        }
        else{
            $j('#ErrorMsgPanel p').text('');
            $j('#ErrorMsgPanel').hide('slow');
        }    
    };


  });


  function updateSigningStatus() {
      var msg = '  ';
      var aBNumLocations = 0;
      var aCNumLocations = 0;
      var aBMsg = '';
      var aCMsg = '';
      
      if (signerManager.aBNm != '' && signerManager.aBSignatureTotal > 0) {
          aBNumLocations = (signerManager.aBSignatureTotal - signerManager.aBSignatureCurrentSignCount);
          var aBPlural = null; (aBNumLocations != 1) ? aBPlural = 's' : aBPlural = '';
          aBMsg = signerManager.aBNm + ' needs to sign in ' + aBNumLocations + ' location' + aBPlural + '. ';
      }
      if (signerManager.aCNm != '' && signerManager.aCSignatureTotal > 0) {
          aCNumLocations = (signerManager.aCSignatureTotal - signerManager.aCSignatureCurrentSignCount);
          var aCPlural = null; (aCNumLocations != 1) ? aCPlural = 's' : aCPlural = '';
          aCMsg = signerManager.aCNm + ' needs to sign in ' + aCNumLocations + ' location' + aCPlural + '. ';
      }
      
      if (CurrentUserSignatureId == 'aBSignature'){
        msg = aBMsg + aCMsg; 
      }else{
        msg = aCMsg + aBMsg; 
      }
      
      $j('#SigningMessage').text(msg);
      ((aBNumLocations-(-aCNumLocations)) != 0) ? $j('#SigningMessage').show() : $j('#SigningMessage').hide();

      var IsDisabled = (signerManager.aBHasSigned && aBNumLocations != 0) || (signerManager.aCHasSigned && aCNumLocations != 0) || (!signerManager.aBHasSigned && !signerManager.aCHasSigned);
      $j('#SignBtn').attr('disabled', rd(IsDisabled));
  }

  var __selectedSignatureField = null;
  
  function _otherUserSignature(o) {
    var msg = '  ';
    var sigInfo = $j(o).data('pdfFieldData');
    if (typeof sigInfo != 'undefined' && sigInfo != null){
        var sigId = sigInfo.Name;
        if (sigId == 'aBSignature' || sigId == 'aCSignature'){
            var Name = '';
            if (sigId == 'aBSignature') { Name = signerManager.aBNm; }
            if (sigId == 'aCSignature') { Name = signerManager.aCNm; }
            try { Name = $j.trim(Name).split(/\s+/)[0]; }catch(e){ logger.Error('Unable to compute 1st name.', e); }
            msg = 'To be signed by ' + Name;
        }
        else if (sigId == 'aBInitials' || sigId == 'aCInitials'){
            msg = 'Initials';
        }
    }
    else { logger.Warning('Unable to extract $j(o).data("pdfFieldData") data.(4)'); }
    
    $j(o).data('field-has-signature', false);
    $j(o).append($j("<div>").addClass('emptysign').text(msg));
    $j(_findThumbnailOf(o)).css('background-color', '#d3d3d3');
    $j(_findThumbnailOf(o)).addClass('graybk');
    $j(o).css('background-color', '#d3d3d3');
    $j(o).addClass('graybk');
  }
  
  
  function _clearSignature(o) {
    var msg = '  ';
    var sigInfo = $j(o).data('pdfFieldData');
    if (typeof sigInfo != 'undefined' && sigInfo != null){
        var sigId = sigInfo.Name;
        if (sigId == 'aBSignature' || sigId == 'aCSignature'){
            msg = 'Click to sign';
        }
        else if (sigId == 'aBInitials' || sigId == 'aCInitials'){
            msg = 'Initials';
        }
    }
    else { logger.Warning('Unable to extract $j(o).data("pdfFieldData") data.(5)'); }
    
    $j(o).data('field-has-signature', false);
    $j(o).find('.sign').remove();
    $j(o).append($j("<div>").addClass('emptysign').text(msg));
    $j(_findThumbnailOf(o)).css('background-color','yellow');
    $j(_findThumbnailOf(o)).addClass('yellowbk');
    $j(o).css('background-color','yellow');
    $j(o).addClass('yellowbk');
  }
  
  
  function _addSignature(o) {
    $j(o).data('field-has-signature', true);
    var sigInfo = $j(o).data('pdfFieldData');
    var w1 = $j(o).find('.emptysign').width();
    var h1 = $j(o).find('.emptysign').height();
    $j(o).find('.emptysign').remove();
    var base = 'Signature.aspx';
    var mode = 'signature';
    var name = CurrentUser;
    
    if (typeof sigInfo != 'undefined' && sigInfo != null){
        if (sigInfo.Name == 'aBInitials' || sigInfo.Name == 'aCInitials'){
            mode = 'initial';
            name = CurrentInitials;
        }
    }
    else { logger.Warning('Unable to extract $j(o).data("pdfFieldData") data.(6)'); }
    
    var url = (base + '?mode=' + mode + '&name=' + name);
    var oImg = $j("<img>", { src: url }).addClass('sign');
    
    $j(o).append(oImg);
    $j(_findThumbnailOf(o)).css('background-color','green');
    $j(o).css('background-color','green');
    $j(o).removeClass('yellowbk');
    $j(o).removeClass('graybk');
    $j(_findThumbnailOf(o)).removeClass('yellowbk');
    $j(_findThumbnailOf(o)).removeClass('graybk');
  }
  
  
  function _findThumbnailOf(o) {
    var id = $j(o).data('pdfFieldDataId');
    var ret = null;
    $j('#signingEditor').find('.pdf-pdf-thumb-field').each(function() { if ($j(this).data('pdfFieldDataId') === id) { ret = this; return false; } });
    return ret;
  }
  
  
  function _onFieldClick(evt) {
    var o = evt.currentTarget;
    
    var signatureInfo = $j(o).data('pdfFieldData');
    if (!IsUserMatch(signatureInfo.Name, CurrentUserSignatureId)){
        return false;
    }
    
    __selectedSignatureField = o;
    var label = CurrentUser.toUpperCase();
    if (signatureInfo.Name == 'aBInitials' || signatureInfo.Name == 'aCInitials'){
        label = CurrentInitials.toUpperCase();
    }
    
    $j('#ConfirmSigningLabel').text(label);
    $j('#ConfirmSigning').dialog('open');
    return;
    
  }
  
  function f_loading(bShow, msg){
    if (typeof bShow != 'undefined' && bShow){
        var txt = 'Loading...';
        if (typeof msg != 'undefined' && msg != null) { txt = msg; }
        $j('#sMsg').text(txt).show();
        return;
    }
    $j('#sMsg').hide();
 }
 
 function IsUserMatch(cUser, signatureField){
    if (!cUser || !signatureField) return false;
    try{
        if (cUser === 'aBSignature'|| cUser === 'aBInitials'){
            return (signatureField === 'aBSignature' || signatureField === 'aBInitials');
        }
        else if (cUser === 'aCSignature' || cUser === 'aCInitials'){
            return (signatureField === 'aCSignature' || signatureField === 'aCInitials');
        }
    }catch(e){ return false; }
    
    return false;
 }
</script>
    <form id="form1" runat="server">
        <div id="content" style="padding:0px;">
            <div id="CurrentSignerFrame">
                <div id="SignerLabel" runat="server">
                    <div id="AvailableSigner" style="display:none;">
                        You are currently signing as:&nbsp;
                        <ml:EncodedLabel ID="CurrentlySigning" CssClass="boldText" runat="server"></ml:EncodedLabel>&nbsp;
                        <asp:PlaceHolder ID="ChangeLinkBtn" runat="server">
                            <a id="ChangeSignerLink" runat="server" href="#" onclick="return false;">change</a>
                        </asp:PlaceHolder>
                    </div>
                    <div id="DetectSigner">Detecting current signer...</div>
                </div>
                <div id="sMsg" style="display:none;">Loading...</div>
            </div>
            <div>
                <div id="ErrorMsgPanel"><p></p></div>
                <div id="signingEditor"></div>
            </div>
            <div style="padding-left:15px;">
                <div class="signaturesMsg" id="SigningMessage"></div>
                <br/>
                <asp:Button ID="SignBtn" CssClass="LoginBtn" runat="server" Text="Submit" Enabled="false" />&nbsp;&nbsp;
                <input type="button" class="LoginBtn" value="Cancel" onclick="self.close();" runat="server" />
                <asp:Button ID="ServerSignBtn" style="display:none;" runat="server" Text="-" OnClick="SignBtn_OnClick" />
                
            </div>
        </div>
        <div id="ConfirmSigning" style="display:none;">
            <p>Type your full name as shown to sign</p>
            <div style="padding-left:30px; padding-top:5px;">
                <div id="ConfirmSigningLabel"></div>
                <input id="ConfirmSigningInput" maxlength="200" style="width:180px;"  type="text" />
            </div>
            <br />
            <div id="ButtonFrame" style="margin-left:auto; margin-right:auto; width:160px;">
                    <input id="ConfirmSigningOkBtn" class="ConsumerBtn" type="button" value="OK" />&nbsp;
                    <input id="ConfirmSigningCancelBtn" class="ConsumerBtn" type="button" value="Cancel" />
            </div>
        </div>
        <div id="ChangeSigner" style="display:none;">
            Change the signer to:
            <table class="DataGrid">
                <tr class="header">
                    <td style="width:270px;">Name</td>
                    <td align="center">Change To</td>
                </tr>
                <tr id="BorrowerRow">
                    <td>
                        <span id="ChangeToBorrower">borrower</span>&nbsp;
                        <span id="BorrowerLoggedIn">(is logged in?)</span>
                    </td>
                    <td align="center" valign="middle">
                        <input type="radio" name="signerGroup" />
                    </td>
                </tr>
                <tr id="CoborrowerRow">
                    <td>
                        <span id="ChangeToCoborrower">coborrower</span>&nbsp;
                        <span id="CoborrowerLoggedIn"></span>
                    </td>
                    <td align="center" valign="middle">
                        <input type="radio" name="signerGroup" />
                    </td>
                </tr>
            </table>
            <table>
                <tr style="height:25px;">
                    <td id="ConfirmChangeSignerRow" valign="top">
                        <input id="ConfirmChangeSignerBox" type="checkbox" />
                        <label id="ConfirmChangeSignerLabel" for="ConfirmSignerBox">I confirm that I am </label>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <br/>
            <div style="margin-left:auto; margin-right:auto; text-align:center; width:210px;">
                <input id="ChangeSignerBtn" class="ConsumerBtn" type="button" value="Change" />&nbsp;
                <input id="ChangeSignerCancelBtn" class="ConsumerBtn" type="button" value="Cancel" />
            </div>
        </div>
        <asp:HiddenField ID="m_Signer" runat="server" />
    </form>
    </asp:PlaceHolder>
</body>
</html>
