﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="loaninfo.aspx.cs" Inherits="ConsumerPortal.loaninfo" %>

<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="ConsumerPortal.Common" %>
<%@ Import Namespace="LendersOffice.Common" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Loan Dashboard</title>
    <link rel="Stylesheet" type="text/css" href="css/jquery-ui-1.8.custom.css" />
    <!--[if lte IE 6]><style type="text/css">div#rightFrame{ margin-left: 20px; }</style><![endif]-->
    <style type="text/css">
        .Action
        {
            color: Blue;
            cursor: pointer;
            text-decoration: underline;
        }

        #UploadSignedPopup .pdfOnly
        {
            background-color: #EEEEEE;
        }
        #UploadSignedPopup
        {
            text-align: center;
        }
        #UploadSignedPopup input
        {
            margin-top: 5px;
            margin-bottom: 5px;
            margin-right: 5px;
        }
        
        #OpenDoc
        {
            display: block;
            margin-left: 40%;
        }
        
        #FaxUploadUI
        {
            display: none;
        }
        
        div.Sent
        {
            display: none;
        }
        
        .UploadEdoc
        {
            width: 75px;
        }
        
        .CancelUpload
        {
            width: 75px;
        }
        .ui-widget-overlay {
            position: fixed; 
        }

    </style>

    <script type="text/javascript">
        jQuery(function($) {
            var $fileUpload = $('#UploadSignedPopup').dialog({
                autoOpen: false,
                title: "Upload File",
                modal: true,
                resizable: false,
                width: 500,
                height: 'auto',
                open: function() {
                    $('.ClearOnOpen').val('');
                    $('.UploadEdoc').attr('disabled', 'disabled');
                },
                close: function() {
                    selectedDocId = null;
                    $('.ClearOnClose').val('');
                    $('.UploadEdoc').attr('disabled', 'disabled');
                }
            });
            $fileUpload.parent().appendTo('#form1');

            if ($('#notpdf').length) {
                alert("Please ensure that the file you are attempting to upload is a pdf");
                window.location = window.location.replace("notpdf=t", "");
            }

            $('.FileUpload').change(function() {
                if (!/\.pdf$/.test($(this).val())) {
                    $('.UploadEdoc').attr('disabled', 'disabled');
                    $(this).siblings('.pdfOnly').effect('highlight', { color: "#FF9999" }, 3000);
                }
                else {
                    $('.UploadEdoc').removeAttr('disabled');
                }
            });

            $('.UploadSign').click(function() {
                $fileUpload.dialog('option', 'height', 'auto');
                $('#SignedUploadUI').show();
                $('#FaxUploadUI').hide();
                openDialog($(this));
            });

            $('.UploadFax').click(function() {
                $fileUpload.dialog('option', 'height', 120);
                $('#FaxUploadUI').show();
                $('#SignedUploadUI').hide();
                openDialog($(this));
            });

            $('.CancelUpload').click(function() {
                $fileUpload.dialog('close');
            });

            $('#ToBeSigned tbody tr').filter(":not(.header)").filter(":odd").addClass('alternate');

            $('#OpenDoc').click(function() {
                var selectedDocId = $('#UploadedDocId').val(selectedDocId);
                if (selectedDocId) window.open("FaxCover.aspx?NoFaxCover=t&faxid=" + selectedDocId, "_self");
            });

            $('.UploadEdoc').click(function() {
                $(this).val("Please wait...");
                $(this).attr('disabled', 'disabled');
                $('.CancelUpload').attr('disabled', 'disabled');
                $('#form1').submit();
            });

            function openDialog($current) {
                var selectedDocId = $current.siblings('.docId').val();
                if (!selectedDocId) {
                    alert("There was a problem retrieving your document, please try again later");
                    return;
                }

                if (selectedDocId) $('#UploadedDocId').val(selectedDocId);

                $fileUpload.dialog('open');
            }
        });
    
    </script>

</head>
<body>
    <form id="form1" enableviewstate="false" runat="server">
    <div id="UploadSignedPopup">
        <div id="SignedUploadUI">
        <h3>
            Print and Upload Document</h3>
        Open and print the document, sign as needed, scan and reupload here.
        <input type="button" value="Open Document" id="OpenDoc" />
        </div>
        <div id="FaxUploadUI">
        <h3>Upload Document</h3>
        </div>
        <label>
            Select file:
            </label>
            <input type="file" runat="server" id="RequestUpload" class="FileUpload ClearOnClose" />
        <span class="pdfOnly">(PDF files only)</span>
        <div class="Controls">
            <input type="submit" value="Submit" class="UploadEdoc" disabled="disabled" />
            <input type="button" value="Cancel" class="CancelUpload" />
        </div>
        <input type="hidden" id="UploadedDocId" class="ClearOnClose" runat="server" />
    </div>
    <div id="content">
        <table width="100%">
            <tr>
                <td class="lFrame" valign="top">
                    <div id="leftFrame">
                        <div class="topRight">
                            <a id="changePass" href="#" runat="server">Change Password</a> <a id="A1" href="logout.aspx"
                                runat="server">Logout</a>
                        </div>
                        <div style="clear: both; padding-top: 10px; padding-right: 20px;">
                            <div class="section">
                                LOAN SUMMARY</div>
                            <div class="withinSection">
                                <table>
                                    <tr>
                                        <td valign="top">
                                            <table class="loanTable">
                                                <tr>
                                                    <td class="loanheader">
                                                        Loan Information
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Borrower
                                                    </td>
                                                    <td>
                                                        <ml:EncodedLabel ID="borrowerNm" runat="server"></ml:EncodedLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Property
                                                    </td>
                                                    <td>
                                                        <ml:EncodedLabel ID="sSpAddr" runat="server"></ml:EncodedLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        <ml:EncodedLabel ID="sSpCity" runat="server"></ml:EncodedLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Loan Program
                                                    </td>
                                                    <td>
                                                        <ml:EncodedLabel ID="sLProductT" runat="server"></ml:EncodedLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Rate
                                                    </td>
                                                    <td>
                                                        <ml:EncodedLabel ID="sLRate" runat="server"></ml:EncodedLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Cash to Close
                                                    </td>
                                                    <td>
                                                        <ml:EncodedLabel ID="CashToClose" runat="server"></ml:EncodedLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Monthly Payment
                                                    </td>
                                                    <td>
                                                        <ml:EncodedLabel ID="MonthlyPayment" runat="server"></ml:EncodedLabel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="margin-left: 3em;" valign="top">
                                            <table class="loanTable">
                                                <tr>
                                                    <td class="loanheader">
                                                        Loan Status
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Opened
                                                    </td>
                                                    <td class="aRight">
                                                        <ml:EncodedLabel ID="sOpenedD" runat="server"></ml:EncodedLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Registered
                                                    </td>
                                                    <td class="aRight">
                                                        <ml:EncodedLabel ID="sRegisteredD" runat="server"></ml:EncodedLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Approved
                                                    </td>
                                                    <td class="aRight">
                                                        <ml:EncodedLabel ID="sApprovedD" runat="server"></ml:EncodedLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Estimated Closing
                                                    </td>
                                                    <td class="aRight">
                                                        <ml:EncodedLabel ID="sEstClosingD" runat="server"></ml:EncodedLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Ready to Close
                                                    </td>
                                                    <td class="aRight">
                                                        <ml:EncodedLabel ID="sReadyToCloseD" runat="server"></ml:EncodedLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Closed
                                                    </td>
                                                    <td class="aRight">
                                                        <ml:EncodedLabel ID="sClosedD" runat="server"></ml:EncodedLabel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="clear: both;">
                                &nbsp;</div>
                            <div class="section">
                                DOCUMENTATION REQUEST</div>
                            <br />
                            <div class="withinSection">
                                <asp:PlaceHolder ID="m_DocsToBeSignedPanel" runat="server"><span class="spText">The
                                    following documents need to be signed:</span>
                                    <asp:Repeater ID="m_DocsToBeSigned" EnableViewState="false" runat="server" OnItemDataBound="m_DocsToBeSignedDataBound">
                                        <HeaderTemplate>
                                        <table class="DataGrid" id="ToBeSigned" style="width: 100%;">
                                            <tr class="header">
                                                <td>
                                                    Document
                                                </td>
                                                <td>
                                                    Application
                                                </td>
                                                <td style="border-right-width: 0px; padding-left: 12px; width: 20px; padding-right: 0px;">
                                                    &nbsp;
                                                </td>
                                                <td style="border-left-width: 0px; padding-left: 4px;">
                                                    Description
                                                </td>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <ml:EncodedLabel ID="DocTitle" runat="server"></ml:EncodedLabel>
                                                </td>
                                                <td>
                                                    <ml:EncodedLabel ID="AppBorrowers" runat="server"></ml:EncodedLabel>
                                                </td>
                                                <td style="border-right-width: 0px; width: 20px; padding-left: 12px; padding-right: 0px;">
                                                    <asp:Image ID="UserAction" Height="15" Width="15" AlternateText="Action Required"
                                                        ToolTip="Action Required" Visible="false" ImageUrl="~/images/led.gif" runat="server" />
                                                </td>
                                                <td style="border-left-width: 0px; padding-left: 4px;">
                                                    <input type="hidden" class="docId" runat="server" id="docId"/>
                                                    <ml:EncodedLabel ID="signActionMsg" runat="server"></ml:EncodedLabel>
                                                    <ml:EncodedLabel ID="ReceivedActionMsg" runat="server"></ml:EncodedLabel>
                                                    <asp:PlaceHolder ID="ActionPanel1" runat="server"><a id="signBtn" runat="server">Sign online</a>
                                                        <asp:PlaceHolder ID="ActionPanel3" runat="server">, <span class="Action UploadSign">
                                                            Sign &amp; upload</span> or <a id="faxCoverBtn" runat="server">Sign &amp; fax</a>
                                                        </asp:PlaceHolder>
                                                    </asp:PlaceHolder>
                                                    <asp:PlaceHolder ID="ActionPanel2" runat="server">
                                                        <span class="Action UploadSign">Sign &amp; upload</span> or
                                                        <a id="signFaxBtn" runat="server">Sign &amp; fax</a>. 
                                                    </asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                        
                                        </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <br />
                                    <br />
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="m_sentDocsPanel" runat="server"><span class="spText">The following
                                    documents need to be sent to us:</span>
                                    <ml:CommonDataGrid EnableViewState="false" AlternatingItemStyle-CssClass="alternate"
                                        HeaderStyle-CssClass="header" ID="m_sentDocs" runat="server" Width="100%">
                                        <Columns>
                                            <asp:BoundColumn DataField="DocTitle" HeaderText="Document" />
                                            <asp:BoundColumn DataField="AppBorrowers" HeaderText="Application" />
                                            <asp:BoundColumn DataField="DocDesc" HeaderText="Description" />
                                            <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                ItemStyle-VerticalAlign="Middle">
                                                <ItemTemplate>
                                                    <div class="<%# AspxTools.HtmlString( (bool)DataBinder.Eval(Container.DataItem, "IsSent") ? "Sent" : "") %>">
                                                    <input type="hidden" value="<%# AspxTools.HtmlString( DataBinder.Eval(Container.DataItem, "faxId").ToString()) %>" class="docId" />
                                                    <span class="Action UploadFax">upload</span> or <a href="FaxCover.aspx?faxid=<%# AspxTools.HtmlString( DataBinder.Eval(Container.DataItem, "faxId").ToString()) %>">
                                                        fax</a>
                                                        </div>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </ml:CommonDataGrid>
                                    <br />
                                    <br />
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="EmptyMsgPanel" runat="server">
                                    <div class="NoDoc">
                                        <div>
                                            This loan has no pending requests.</div>
                                    </div>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="UploadDocs" runat="server"><span class="spText">Upload additional
                                    documents here:</span>
                                    <div class="DataGrid Controls">
                                        <div>
                                            <label>
                                                Select file:
                                            </label>
                                            <input type="file" accept="application/pdf" runat="server" id="FileUpload" class="FileUpload ClearOnOpen" size="65" />
                                            <span class="pdfOnly">(PDF files only)</span>
                                        </div>
                                        <div>
                                            <label>
                                                Description:
                                            </label>
                                            <input type="text" runat="server" id="FileDesc" class="ClearOnOpen" /><input type="submit" value="Upload"
                                                class="UploadEdoc" disabled="disabled" />
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                                </asp:PlaceHolder>
                            </div>
                            <div style="clear: both;">
                                &nbsp;</div>
                            <div class="section">
                                DOCUMENTS</div>
                            <br />
                            <div class="withinSection">
                                <asp:PlaceHolder ID="m_signedDocsPanel" runat="server"><span class="spText">The following
                                    documents have been signed and are available for you to download for your records:</span>
                                    <ml:CommonDataGrid EnableViewState="false" AlternatingItemStyle-CssClass="alternate"
                                        HeaderStyle-CssClass="header" ID="m_signedDocs" runat="server" Width="100%">
                                        <Columns>
                                            <asp:BoundColumn DataField="DocTitle" HeaderText="Document" />
                                            <asp:BoundColumn DataField="AppBorrowers" HeaderText="Application" />
                                            <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                ItemStyle-VerticalAlign="Middle">
                                                <ItemTemplate>
                                                    <a href="#" onclick="return f_view('<%# AspxTools.HtmlString( DataBinder.Eval(Container.DataItem, "docId").ToString()) %>')">
                                                        view</a>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </ml:CommonDataGrid>
                                    <br />
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="EmptyMsgPanel2" runat="server">
                                    <div class="NoDoc">
                                        <div>
                                            This loan has no documents available for download.</div>
                                    </div>
                                </asp:PlaceHolder>
                            </div>
                        </div>
                    </div>
                </td>
                <asp:PlaceHolder ID="m_RightFramePanel" runat="server">
                    <td class="rFrame" valign="top">
                        <div id="rightFrame">
                            <span style="padding-bottom: 7px; display: block; font-weight: bold; text-decoration: underline;">
                                YOUR TEAM</span>
                            <asp:Repeater ID="m_team" EnableViewState="false" runat="server" OnItemDataBound="m_teamDataBound">
                                <ItemTemplate>
                                    <asp:PlaceHolder ID="m_typePanel" runat="server">
                                        <ml:EncodedLabel ID="m_type" runat="server"></ml:EncodedLabel><br />
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="m_picPanel" runat="server">
                                        <asp:Image ID="m_pic" Width="128" Height="128" runat="server" /><br />
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="m_namePanel" runat="server">
                                        <ml:EncodedLabel ID="m_name" runat="server"></ml:EncodedLabel><br />
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="m_emailPanel" runat="server">
                                        <ml:EncodedLabel ID="m_email" runat="server"></ml:EncodedLabel><br />
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="m_phonePanel" runat="server">
                                        <ml:EncodedLabel ID="m_phone" runat="server"></ml:EncodedLabel><br />
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder runat="server" ID="m_nmlsPanel">
                                        NMLS#: <ml:EncodedLabel runat="server" ID="m_nmls"></ml:EncodedLabel>
                                    </asp:PlaceHolder>
                                </ItemTemplate>
                                <SeparatorTemplate>
                                    <br />
                                    <br />
                                </SeparatorTemplate>
                            </asp:Repeater>
                        </div>
                    </td>
                </asp:PlaceHolder>
            </tr>
        </table>
    </div>
    </form>

    <script type="text/javascript">
        <!--
         var f = function() { return false;  };
         var f_sign = f;
         var f_view = f;

         function PopupCenter(pageURL, title, w, h) {
             var left = (screen.width / 2) - (w / 2);
             var top = (screen.height / 2) - (h / 2);
             return window.open(pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
         } 
         
         function GetWidth()
         {
            try{
                if (screen.width < 940)
                    return screen.width;
            }
            catch(e){ return 940; }
            return 940;
         }
         function GetHeight()
         {
            try{
                if (screen.height <= 600){
                    if (screen.height > 400)
                        return (screen.height - 100);
                    return screen.height;
                }
                    
                if (screen.height <= 830)
                    return 660;
                if (screen.height <= 1050)
                    return 830;
            }
            catch(e){ return 830;}
            return 830;
         }

         $j(document).ready(function() {
             f_sign = function(id) {
                 if (window._popup) {
                     try {
                         window._popup.close();
                         var undef;
                         window._popup = undef;
                     } catch (e) { }
                 }
                 var win = PopupCenter("onlinesigning.aspx?reqId=" + id, "Online_Signing", GetWidth(), GetHeight());
                 if (typeof win == 'undefined' || win == null || !win) alert(  <%=AspxTools.JsString(ConsumerPortal.Common.ErrorMessages.PopupBlocker) %> );
                 else {
                     window._popup = win;
                 }
                 return false;
             };
             f_view = function(id) {
                    var win;
                if( id === '00000000-0000-0000-0000-000000000000' ) {
                    win = window.open("ViewEDocPdf.aspx?cmd=esign");
                }
                else {
                 win = window.open("ViewEDocPdf.aspx?docid=" + id, "_parent");
                 }
                 if (typeof win == 'undefined' || win == null || !win) alert( <%=AspxTools.JsString(ConsumerPortal.Common.ErrorMessages.PopupBlocker) %> );

                 return false;
             };

             window.refreshLoan = function() {
                 window.location.href = window.location.href;
             }

             $j(window).bind('beforeunload', function(){
                 try {
                    if (window._popup) window._popup.close();
                 } catch (e) { }
             });

         });
        //-->
    </script>

</body>
</html>
