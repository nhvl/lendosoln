﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using DataAccess;
using System.Data;
using LendersOffice.Admin;
using System.Data.SqlClient;
using ConsumerPortal.Common;
using LendersOffice.ObjLib.Security;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using ConsumerPortal.lib;

namespace ConsumerPortal
{
    public partial class pipeline : ConsumerPortal.common.BaseCPortalPage
    {
        private LoanAccessInfoTable m_loanInfos = new LoanAccessInfoTable();
        protected const int MaxLoans = 25;
        private NumberFormatInfo moneyFormat = new NumberFormatInfo();
        
        ConsumerUserPrincipal CurrentUser
        {
            get { return Page.User as ConsumerUserPrincipal; } 
        }            

        protected Guid LoanID
        {
            get { return CPRequestHelper.LoanID; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            moneyFormat.CurrencyDecimalDigits = 2;
            moneyFormat.CurrencyNegativePattern = 0; // Parenthesis
            moneyFormat.CurrencySymbol = "$";

            if (string.IsNullOrEmpty(CurrentUser.LastSsnDigits) || !Regex.IsMatch(CurrentUser.LastSsnDigits, @"\d\d\d\d"))
            {
                CPTools.RedirectValidateSsn();
            }

            //TestFillDs(ds);
            var tuple = RetrieveLoans(MaxLoans);

            DataSet ds = new DataSet();
            DBSelectUtility.FillDataSet(CurrentUser.BrokerId, ds, tuple.Item1, null, tuple.Item2);
            
            DataTable dt = ds.Tables[0];

            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                Guid sLId = (Guid)dt.Rows[i]["sLId"];
                if (!CurrentUser.CanAccessLoanFile(sLId))
                {
                    dt.Rows[i].Delete();
                }
            }

            //Filter loans without this SSN
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                Guid sLId = (Guid)dt.Rows[i]["sLId"];
                if (CurrentUser.NeedsSsnVerification(sLId))
                {
                    dt.Rows[i].Delete();
                }
            }


            DataView dv = dt.DefaultView;

            //OPM 70215: If the user needs to accept the ESIGN disclosure, we can only do it now
            //           that we can let the disclosure page know which loan(s) we're talking about
            if (false == CurrentUser.AcceptedDisclosure)
            {
                StringBuilder affectedLoanIds = new StringBuilder();

                foreach (DataRowView row in dv)
                {
                    affectedLoanIds.Append(row["sLId"] + ",");
                }

                Session["ids"] = affectedLoanIds.ToString();
                Response.Redirect(ResolveUrl("~/Disclosure.aspx"), true);
                return;
            }
            
            int preFilterCount = (dv != null) ? dv.Count : 0;
            dv.RowFilter = String.Format(CultureInfo.InvariantCulture.DateTimeFormat, "sStatusD > #{0}#", System.DateTime.Today.AddYears(-1));
            int rowCount = (dv != null) ? dv.Count : 0;

            if (dv != null && rowCount == 0 && preFilterCount != 0)
            {
                //There are loans associated with this consumer, but all of them have sStatusD > 1 year ago
                //In this case, skip pipeline and redirect to the most recent 'old' loan
                DataView dvSorted = dt.DefaultView;
                dvSorted.RowFilter = "";
                dvSorted.Sort = "sStatusD DESC";

                Guid sLId = Guid.Empty;
                sLId = (Guid)dvSorted[0]["sLId"];

                if (sLId != Guid.Empty)
                {
                    Response.Redirect("~/loaninfo.aspx?loanid=" + sLId.ToString());
                    return;
                }
            }

            m_gridPanel.Visible = rowCount != 0;
            m_emptyPanel.Visible = rowCount == 0;

            if (rowCount == 1)
            {
                Guid sLId = Guid.Empty;
                sLId = (Guid)dv[0]["sLId"];

                if (sLId != Guid.Empty)
                {
                    Response.Redirect("~/loaninfo.aspx?loanid=" + sLId.ToString());
                }
            }
            else
            {
                BindLoansGrid(dv, false);
            }

        }

        private Tuple<string, List<SqlParameter>> RetrieveLoans(int count)
        {
            var retrieveLoansParameters = new List<SqlParameter>();

            retrieveLoansParameters.Add(new SqlParameter("@top", count));
            retrieveLoansParameters.Add(new SqlParameter("@cid", CurrentUser.ConsumerId));

            // Set up the statement clauses
            var sqlQuery = new StringBuilder(@"SELECT DISTINCT TOP (@top)
					fc.sLId ,
					-- Keep the following set in sync with loan access info loading
					fc.sBrokerId ,
					fc.sBranchId ,
					fc.sLNm ,
					fc.sPrimBorrowerFullNm ,
					fc.sStatusT ,
					fc.sLienPosT ,
					fc.sOpenedD ,
                    fc.sStatusD ,
					fc.sNoteIRSubmitted ,
					fc.IsTemplate ,
					fc.IsValid ,
					fc.sEmployeeManagerId ,
					fc.sEmployeeUnderwriterId ,
					fc.sEmployeeLockDeskId ,
					fc.sEmployeeProcessorId ,
					fc.sEmployeeLoanOpenerId ,
					fc.sEmployeeLoanRepId ,
					fc.sEmployeeLenderAccExecId ,
					fc.sEmployeeRealEstateAgentId ,
					fc.sEmployeeCallCenterAgentId ,
					fc.PmlExternalManagerEmployeeId ,
                    fc.sRateLockStatusT,
					-- end sync set
					fc.sSpAddr, fc.sSpCity, fc.sSpState, fc.sSpZip, sLAmtCalc
					FROM Loan_File_Cache fc INNER JOIN Loan_File_Cache_2 fc2 ON fc.sLId = fc2.sLId
                    INNER JOIN CP_CONSUMER_X_LOANID cp ON cp.sLId = fc.sLId
					WHERE
					cp.ConsumerId = @cid AND fc.IsValid = 1
					AND fc.IsTemplate = 0");

            sqlQuery.Append(System.Environment.NewLine + " ORDER BY sOpenedD DESC ");

            return new Tuple<string, List<SqlParameter>>(sqlQuery.ToString(), retrieveLoansParameters);
        }

        private void TestFillDs(DataSet ds)
        {
            if (ds == null)
                ds = new DataSet();
            else
                ds.Clear();

            DataTable dt = new DataTable();

            DataColumn dc = new DataColumn("sSpAddr");
            dt.Columns.Add(dc);

            dc = new DataColumn("sSpCity");
            dt.Columns.Add(dc);

            dc = new DataColumn("sSpState");
            dt.Columns.Add(dc);

            dc = new DataColumn("sSpZip");
            dt.Columns.Add(dc);

            dc = new DataColumn("sLAmtCalc");
            dt.Columns.Add(dc);

            dc = new DataColumn("sLienPosT");
            dt.Columns.Add(dc);

            dc = new DataColumn("sOpenedD");
            dt.Columns.Add(dc);

            dc = new DataColumn("sLId");
            dt.Columns.Add(dc);

            dt.Rows.Add("123 Main Street", "Irvine", "CA", "92612", "130,000.00$", "2nd", "3/4/2010", Guid.Empty.ToString());
            dt.Rows.Add("7256 Embarcadero Dr.", "San Francisco", "CA", "90642", "830,000.00$", "1st", "1/4/2009", Guid.Empty.ToString());
            dt.Rows.Add("33 Fifth Ave.", "New York City", "NY", "10032", "1,920,500.00$", "1st", "1/4/2006", Guid.Empty.ToString());

            ds.Tables.Add(dt);
        }

        protected string DisplayAddress(object container)
        {

            string sSpAddr  = DataBinder.Eval(container, "sSpAddr" ).ToString();
            string sSpCity  = DataBinder.Eval(container, "sSpCity" ).ToString();
            string sSpState = DataBinder.Eval(container, "sSpState").ToString();
            string sSpZip   = DataBinder.Eval(container, "sSpZip"  ).ToString();
            string FullAddress = "";

            if (!string.IsNullOrEmpty(sSpAddr))
                FullAddress += sSpAddr;
            
            if (!string.IsNullOrEmpty(FullAddress) && !string.IsNullOrEmpty(sSpCity))
                FullAddress += ", ";
            
            if (!string.IsNullOrEmpty(sSpCity))
                FullAddress += sSpCity;

            if (!string.IsNullOrEmpty(FullAddress) && !string.IsNullOrEmpty(sSpState))
                FullAddress += ", ";
            
            if (!string.IsNullOrEmpty(sSpState))
                FullAddress += sSpState;

            if (!string.IsNullOrEmpty(FullAddress) && !string.IsNullOrEmpty(sSpZip))
                FullAddress += " ";

            if (!string.IsNullOrEmpty(sSpZip))
                FullAddress += sSpZip;

            return FullAddress;
        }

        protected string FormatMoney(decimal money)
        {
            return money.ToString("C", moneyFormat);
        }

        protected string GetLienPosition(int position)
        {
            switch (position)
            {
                case 0:
                    return "1st";
                case 1:
                    return "2nd";
                default:
                    return "N/A";
            }
        }
        
       
        private void BindLoansGrid(DataView dv, bool isSearch)
        {
            m_dg.DataSource = dv;
            m_dg.DefaultSortExpression = "sOpenedD DESC";
            m_dg.DataBind();
        }
    }
}
