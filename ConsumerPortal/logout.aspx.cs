﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using ConsumerPortal.Common;

namespace ConsumerPortal
{
    public partial class logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            System.Threading.Thread.CurrentPrincipal = null;

            Guid SiteId = Guid.Empty;
            
            try
            {
                SiteId = new Guid(Request.Cookies["CPortalSiteId"].Value);
            }
            catch { }

            string errorCode = null;
            try
            {
                errorCode = CPRequestHelper.GetSafeQueryString("errorcode");
            }
            catch { }

            
            string url = "";
            if (Guid.Empty == SiteId)
                url = "~/SessionExpired.aspx";
            else
                url = "~/login.aspx?SiteId=" + SiteId;

            if ((errorCode != null) && (errorCode.Trim() != ""))
                url += "&errorcode=" + errorCode;

            Response.Redirect(url);
        }
    }
}
