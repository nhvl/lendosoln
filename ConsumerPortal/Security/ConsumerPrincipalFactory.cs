﻿using System.Security.Principal;
using LendersOffice.Security;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.ObjLib.Security;
using System;
using LendersOffice.Common;
using System.Web;
using System.Web.Security;
using ConsumerPortal.lib;
using ConsumerPortal.Constants;
using LendersOffice.Constants;

namespace ConsumerPortal.Security
{
    public class ConsumerPrincipalFactory
    {
        public static IPrincipal CreatePrincipalWithFailureType(string username, string password, Guid brokerId, out PrincipalFactory.E_LoginProblem loginProblem)
        {
            AbstractUserPrincipal principal = null;

            try
            {
                string passwordHash = PasswordHelper.HashOfPassword(password);
                Int64 ConsumerId = -1;

                SqlParameter[] parameters = {
                                            new SqlParameter("@LoginName", username),
                                            new SqlParameter("@Password", passwordHash),
                                            new SqlParameter("@BrokerId", brokerId)
                                        };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "CP_RetrieveConsumerCredentialByUserNamePassword", parameters))
                {
                    if (reader.Read())
                    {
                        ConsumerId = Int64.Parse(reader["ConsumerId"].ToString());
                    }
                }

                if (ConsumerId != -1)
                {
                    principal = CreatePrincipal(brokerId, ConsumerId);
                }

                loginProblem = PrincipalFactory.E_LoginProblem.None;

                if (ConsumerId == -1 || principal == null)
                {
                    //Is user locked?
                    if (IsLocked(username, brokerId))
                    {
                        loginProblem = PrincipalFactory.E_LoginProblem.IsLocked;
                    }
                    else
                    {
                        //Increase fail attempts
                        int failureCount = IncrementLoginFailureCount(username, brokerId);
                        if (failureCount >= ConsumerPortal.Constants.ConstApp.MaxFailedLoginAttempts)
                        {
                            //Lock
                            LockAccount(username, brokerId);
                            loginProblem = PrincipalFactory.E_LoginProblem.IsLocked;
                        }
                        else
                        {
                            //Is valid password?
                            loginProblem = PrincipalFactory.E_LoginProblem.InvalidLoginPassword;
                        }
                    }
                }
                else
                {
                    
                    ConsumerUserPrincipal consumer = principal as ConsumerUserPrincipal;
                    bool IsValid = false;
                    
                    if (consumer != null)
                    {
                        if (!consumer.IsTempPassword)
                        {
                            IsValid = true;
                        }
                        else if (consumer.IsTempPassword)
                        {
                            if (consumer.LastLoginD == null || consumer.LastLoginD.AddDays(ConsumerPortal.Constants.ConstApp.NumDaysTempPasswordIsValid) > System.DateTime.Now)
                            {
                                IsValid = true;
                            }
                            else
                            {
                                loginProblem = PrincipalFactory.E_LoginProblem.TempPasswordExpired;
                            }
                        }                        
                    }
                    
                    if (IsValid)
                    {
                    
                        SetValidLoginInfo(ConsumerId, brokerId);

                    }
                    else
                    {
                        //If temp password expired, clear the current principal
                        principal = null;
                        System.Threading.Thread.CurrentPrincipal = null;
                    }
                }

                return principal;
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }
        }

        public static AbstractUserPrincipal CreatePrincipal(Guid brokerId, Int64 ConsumerId)
        {

            SqlParameter[] parameters = { new SqlParameter("@ConsumerId", ConsumerId) };

            // [CP_RetrieveConsumerById]
            ConsumerUserPrincipal principal = null;

            Guid BrokerId = Guid.Empty;
            string Email = "";
            bool IsLocked = false;

            DateTime LastLoggedInD = DateTime.MaxValue;
            DateTime PasswordRequestD = DateTime.MaxValue;
            int LoginFailureNumber = 0;
            string PasswordResetCode = "";
            DateTime? DisclosureAcceptedD = new DateTime?();
            bool IsTemporaryPassword = false;

            string SsnDigits = "";
            int SsnFailureNumber = 0;
            DateTime LastSsnAttemptD = DateTime.MaxValue;


            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "CP_RetrieveConsumerById", parameters))
            {
                if (reader.Read())
                {
                    BrokerId = (Guid)reader["BrokerId"];
                    Email = (string)reader["Email"];
                    IsLocked = (bool)reader["IsLocked"];

                    if (reader["LastLoggedInD"] != DBNull.Value)
                    {
                        LastLoggedInD = (DateTime)reader["LastLoggedInD"];
                    }

                    if (reader["PasswordRequestD"] != DBNull.Value)
                    {
                        PasswordRequestD = (DateTime)reader["PasswordRequestD"];
                    }
                    
                    PasswordResetCode = (string)reader["PasswordResetCode"];
                    IsTemporaryPassword = (bool)reader["IsTemporaryPassword"];
                    LoginFailureNumber = (int)reader["LoginFailureNumber"];

                    if (reader["DisclosureAcceptedD"] != DBNull.Value)
                    {
                        DisclosureAcceptedD = (DateTime)reader["DisclosureAcceptedD"];
                    }

                    SsnDigits = (string)reader["Last4SsnDigits"];
                    SsnFailureNumber = (int)reader["SsnAttemptFailureNumber"];
                    if (reader["LastSsnAttemptFailDate"] != DBNull.Value)
                    {
                        LastSsnAttemptD = (DateTime)reader["LastSsnAttemptFailDate"];
                    }
                }
                else
                {
                    Tools.LogError("ConsumerId not found! Unable to load ConsumerUserPrincipal for ConsumerId = " + ConsumerId.ToString() + ". Probably, the e-mail account was deleted while the user was logged in.");
                    CPTools.RedirectLogout();
                }
            }


            principal = new ConsumerUserPrincipal(new GenericIdentity(Email), ConsumerId, BrokerId, Email, IsLocked, LastLoggedInD, PasswordRequestD, LoginFailureNumber, PasswordResetCode, IsTemporaryPassword, SsnDigits, SsnFailureNumber, LastSsnAttemptD, DisclosureAcceptedD);

            if (null != HttpContext.Current && null != HttpContext.Current.Response)
            {
                StoreToCookie(principal, ConsumerPortal.Constants.ConstApp.CookieExpiresInMinutes);
            }

            System.Threading.Thread.CurrentPrincipal = principal;

            return principal;
        }

        public static IPrincipal Create(FormsIdentity identity)
        {
            // NOTE: UserData CANNOT exceed 1000 characters or it will not stored in cookies.
            // UserData = UserType|UserID|ApplicationType|BrokerId
            string[] data = identity.Ticket.UserData.Split('|');

            if (data.Length != 4)
            {
                CBaseException e = new CBaseException(ErrorMessages.Generic, "Invalid format in user cookie [" + identity.Ticket.UserData + "]");
                FormsAuthentication.SignOut();
                ErrorUtilities.DisplayErrorPage(e, true, Guid.Empty, Guid.Empty);
            }
            string userType = data[0];
            int ConsumerID = Int32.Parse(data[1]);
            Guid brokerId = new Guid(data[3]);
            AbstractUserPrincipal principal = CreatePrincipal(brokerId, ConsumerID);
            
            if (null == principal)
                RequestHelper.Signout();

            return principal;

        }

        public static void StoreToCookie(ConsumerUserPrincipal principal, int cookieExpiresMinutes) 
        {
            if (null != principal) 
            {
                if (null == HttpContext.Current )
                    return;

                string userData = principal.ToConsumerDataString();
				
                // NOTE: UserData CANNOT exceed 1000 characters or it will not stored in cookies.
                if (userData.Length > 1000) 
                {
                    CBaseException exc = new CBaseException(ErrorMessages.Generic,"Cookies UserData is exceed 1000 characters. UserData=[" + userData + "]");
                    ErrorUtilities.DisplayErrorPage(exc, true, Guid.Empty, Guid.Empty);
                }

                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(ConstAppDavid.CookieVersion, 
                    principal.Identity.Name,
                    DateTime.Now,
                    DateTime.Now.AddMinutes(cookieExpiresMinutes),
                    true,
                    userData);

                string str = FormsAuthentication.Encrypt(ticket);
                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, str);
                
                // 2/12/2008 dd - If the code run on production then require cookie to be "secure". "Secure" cookie will only transmit through SSL.
                if (ConstSite.IsSecureCookieRequired) 
                {
                    cookie.Secure = true;
                }

                cookie.Path = FormsAuthentication.FormsCookiePath;
                cookie.HttpOnly = true;
			
                cookie.Expires = DateTime.MaxValue;
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }


        private static int IncrementLoginFailureCount(string Email, Guid BrokerId)
        {
            int loginFailureCount = 0;
            try
            {
                SqlParameter[] parameters = { 
                                                new SqlParameter("@Email", Email), 
                                                new SqlParameter("@BrokerId", BrokerId) 
                                            };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerId, "CP_UpdateLoginFailureCount", parameters))
                {
                    if (reader.Read())
                        loginFailureCount = Int32.Parse(reader["LoginFailureNumber"].ToString());
                }
            }
            catch { }
            return loginFailureCount;
        }

        private static bool IsLocked(string Email, Guid BrokerId)
        {
            SqlParameter[] parameters = { 
                                            new SqlParameter("@Email", Email), 
                                            new SqlParameter("@BrokerId", BrokerId) 
                                        };
            try
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerId, "CP_RetrieveDisabledConsumerByEmail", parameters))
                {
                    return reader.Read();
                }
            }
            catch
            {
                return false;
            }
        }

        private static void LockAccount(string Email, Guid BrokerId)
        {
            SqlParameter[] parameters = {
											new SqlParameter("@Email", Email),
											new SqlParameter("@BrokerId", BrokerId)
										};
            StoredProcedureHelper.ExecuteNonQuery(BrokerId, "CP_SetConsumerLoginBlockByEmail", 1, parameters);
        }

        private static void SetValidLoginInfo(Int64 ConsumerId, Guid BrokerId)
        {
            SqlParameter[] parameters = {
											new SqlParameter("@ConsumerId", ConsumerId),
											new SqlParameter("@BrokerId", BrokerId)
										};
            StoredProcedureHelper.ExecuteNonQuery(BrokerId, "CP_SetValidLoginInfoByConsumerId", 1, parameters);
        }
    }

}