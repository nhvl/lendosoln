﻿using LendersOffice.AntiXss;
using ConsumerPortal.Constants;
using DataAccess;
using System;
using LendersOffice.Email;
using LendersOffice.Constants;

namespace ConsumerPortal.lib
{
    public class EmailHelper
    {

        public static bool GenerateAndSendEmail(Guid brokerId, string Email, string resetCode, DateTime requestD, string ip, string lenderName)
        {
            try
            {
                string EmailFrom = ConstStage.DefaultDoNotReplyAddress;
                string EmailTo = Email;
                string EmailSubject = string.Format(ConsumerPortal.Constants.ConstApp.PASSWORD_RESET_EMAIL_SUBJECT, lenderName);
                string EmailBody = ConsumerPortal.Constants.ConstApp.PASSWORD_RESET_EMAIL_BODY;

                string Path = ConsumerPortal.Constants.ConstApp.PASSWORD_RESET_PATH;
                string FileName = ConsumerPortal.Constants.ConstApp.PASSWORD_RESET_FILENAME;
                string url = string.Format(FileName, Path, Email, resetCode);


                string BODY = string.Format(EmailBody, AspxTools.HtmlString(Email), ip, requestD.ToString(), AspxTools.HtmlAttribute(url), AspxTools.HtmlString(url), lenderName);
                EmailProcessor emP = new EmailProcessor();
                CBaseEmail cbe = new CBaseEmail(brokerId)
                {
                    Subject=EmailSubject, 
                    From=EmailFrom, 
                    Bcc="", 
                    To=EmailTo, 
                    Message=BODY, 
                    IsHtmlEmail=true, 
                    DisclaimerType=LendersOffice.Common.E_DisclaimerType.CONSUMER
                };
                
                emP.Send(cbe);
                return true;
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }
            
        }
    }
}


