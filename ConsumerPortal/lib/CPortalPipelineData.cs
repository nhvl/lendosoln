﻿using System;
using System.Collections;
using System.Data;
using DataAccess;

public class CPortalPipelineData : CPageData
{
    private static CSelectStatementProvider s_selectProvider;
    static CPortalPipelineData()
    {
        StringList list = new StringList();

        #region Target Fields
        list.Add("sfTransformLoPropertyTypeToPml");
        list.Add("sApprVal");
        list.Add("sTransNetCash");
        list.Add("sLAmtCalc");
        list.Add("sLPurposeT");
        list.Add("aAsstLiqTot");
        list.Add("sPurchPrice");
        list.Add("sCreditScoreType2Soft");
        list.Add("sTotCcPboPbs");
        list.Add("sTotTransC");
        list.Add("sFinalLAmt");
        list.Add("sLTotI");
        list.Add("sLiaMonLTot");
        list.Add("sFfUfmip1003");
        list.Add("sFfUfmipFinanced");
        list.Add("sProdFhaUfmip");
        list.Add("aBDecCitizen");
        list.Add("aBDecResidency");
        list.Add("aBDecForeignNational");
        list.Add("aCDecCitizen");
        list.Add("aBEmpCollection");
        list.Add("aCEmpCollection");
        list.Add("aBSsn");
        list.Add("aCSsn");
        list.Add("aBFirstNm");
        list.Add("aBMidNm");
        list.Add("aBLastNm");
        list.Add("aCFirstNm");
        list.Add("aCMidNm");
        list.Add("aCLastNm");
        list.Add("aCDecOcc");
        list.Add("aAsstLiqTot");
        list.Add("aLiaMonTot");
        list.Add("aBTotI");
        list.Add("aLiaXmlContent");
        list.Add("aReXmlContent");

        list.Add("aBDecOcc");

        list.Add("aBEquifaxScore");
        list.Add("aBTransUnionScore");
        list.Add("aBEquifaxScore");
        list.Add("aCDecResidency");
        list.Add("aCEquifaxScore");
        list.Add("aCTransUnionScore");
        list.Add("aCEquifaxScore");

        list.Add("aBBaseI");
        list.Add("aLiaMonTot");        
        list.Add("sTransNetCashLckd");
        
        list.Add("aFHABBaseI");
        list.Add("aFHACBaseI");
        list.Add("aFHABOI");
        list.Add("aFHACOI");
        list.Add("aFHANetRentalI");
        list.Add("sFHAGift1gAmt");
        list.Add("sFHAGift2gAmt");
        list.Add("sFHAPro1stMPmt");
        list.Add("sTransmFntcLckd");
        list.Add("sVerifAssetAmt");
        list.Add("sFHASecondaryFinancingAmt");
        list.Add("sTotalScoreRefiT");
        list.Add("sTotalScoreCurrentMortgageStatusT");
        list.Add("sTotalScoreFhaProductT");
        list.Add("sFHASellerContribution");
        list.Add("sFinMethT");
        list.Add("sTransmFntc");

        list.Add("sTotalScoreAppraisedFairMarketRent");
        list.Add("sTotalScoreVacancyFactor");
        list.Add("sTotalScoreNetRentalIncome");
        list.Add("sTotalScoreIsIdentityOfInterest");

        list.Add("sNoteIR");
        list.Add("sTerm");
        list.Add("sRAdj1stCapMon");

        list.Add("sConcurSubFin");
        list.Add("sSubFin");

        list.Add("sFHASecondaryFinancingIsGov");
        list.Add("sFHASecondaryFinancingIsFamily");
        list.Add("sFHASecondaryFinancingIsNP");
        list.Add("sFHASecondaryFinancingIsOther");
        list.Add("sFHASecondaryFinancingOtherDesc");
        list.Add("aFHABCaivrsNum");
        list.Add("aFHACCaivrsNum");

        list.Add("aFHABLdpGsaTri");
        list.Add("aFHACLdpGsaTri");

        list.Add("aBDecPastOwnership");
        list.Add("aCDecPastOwnership");
        list.Add("sFHALenderIdCode");
        list.Add("sFHASponsorAgentIdCode");
        list.Add("sAgencyCaseNum");

        list.Add("aBTotalScoreIsCAIVRSAuthClear");
        list.Add("aCTotalScoreIsCAIVRSAuthClear");
        list.Add("aBTotalScoreFhtbCounselingT");
        list.Add("aCTotalScoreFhtbCounselingT");

        list.Add("sTotalScoreUploadDataFHATransmittal");
        list.Add("sTotalScoreUploadData1003");

        list.Add("aBSuffix");
        list.Add("aCSuffix");

        list.Add("sUnitsNum");
        list.Add("aBAddrYrs");
        list.Add("aBDependNum");
        list.Add("aPres1stM");
        list.Add("aPresRent");
        list.Add("aPresOFin");

        list.Add("aPresHazIns");
        list.Add("aPresRealETx");
        list.Add("aPresMIns");
        list.Add("aPresHoAssocDues");
        list.Add("aPresOHExp");

        list.Add("aBAge");
        list.Add("aCAge");
        list.Add("aBIsAmericanIndian");
        list.Add("aBIsAsian");
        list.Add("aBIsBlack");
        list.Add("aBIsPacificIslander");
        list.Add("aBIsWhite");
        list.Add("aBNoFurnish");
        list.Add("aCIsAmericanIndian");
        list.Add("aCIsAsian");
        list.Add("aCIsBlack");
        list.Add("aCIsPacificIslander");
        list.Add("aCIsWhite");
        list.Add("aCNoFurnish");
        list.Add("aBHispanicT");
        list.Add("aCHispanicT");
        list.Add("aBDob");
        list.Add("aCDob");
        list.Add("aBGender");
        list.Add("aCGender");
        list.Add("sTotalScoreUniqueLoanId");
        list.Add("aOpNegCfLckd");
        list.Add("aBNetNegCf");
        list.Add("aCNetNegCf");
        list.Add("sTotalScoreVersion");
        list.Add("sSpAddr");
        list.Add("sSpCity");
        list.Add("sSpState");
        list.Add("sSpLien");
        list.Add("sGseSpT");
        list.Add("sUnitsNum");
        list.Add("sSpCountyFips");
        list.Add("sFHASecondaryFinancingIsGov");
        list.Add("sFHASecondaryFinancingIsNP");
        list.Add("sFHASecondaryFinancingIsFamily");
        list.Add("sFHAConstructionT");
        list.Add("sFHAPurposeIs203k");
        list.Add("sFHAHousingActSection");
        list.Add("aBAddrYrs");
        list.Add("aCAddrYrs");
        list.Add("aCDependNum");
        list.Add("sFHAPurposeIsPurchaseExistHome");
        list.Add("sFHAPurposeIsConstructHome");
        list.Add("sTotalScoreCertificateXmlContent");
        list.Add("sProdSpT");
        list.Add("sProdSpStructureT");
        list.Add("sOccR");
        list.Add("sGrossProfit");
        list.Add("sProRealETxPe");
        list.Add("sProOHExpPe");
        list.Add("sProdCondoStories");
        list.Add("sProdIsSpInRuralArea");
        list.Add("sProdIsCondotel");
        list.Add("sProdIsNonwarrantableProj");
        list.Add("sOccT");
        list.Add("sSpZip");
        list.Add("sSpCounty");
        list.Add("sFHAProMInsLckd");
        list.Add("sFHAProMIns");
        list.Add("sFHAProHoAssocDues");
        list.Add("sFHAProGroundRent");
        list.Add("sFHAPro2ndFinPmt");
        list.Add("sFHAProHazIns");
        list.Add("sFHAProRealETx");
        list.Add("sFHADebtLckd");
        list.Add("aFHADebtInstallPmt");
        list.Add("aFHAChildSupportPmt");
        list.Add("sFHAPmtFixedTot");
        list.Add("sBranchId");
        list.Add("sTempLpeTaskMessageXml");
        list.Add("sProdLpePriceGroupId");

        list.Add("aProdBCitizenT");
        list.Add("aProdCCitizenT");
        list.Add("aOpNegCfPeval");
        list.Add("sIsSelfEmployed");
        list.Add("sOpNegCfPeval");
        list.Add("sIsCommunityLending");
        list.Add("aBorrowerCreditModeT");
        list.Add("sTotalScoreCreditRiskResultT");
        list.Add("sfLockProMInsUsingUWShorthand");
        list.Add("sFHAIncomeLckd");
        list.Add("sFHAPurposeIsFinanceImprovement");
        list.Add("sFHAPurposeIsFinanceCoopPurchase");
        list.Add("sFHAPurposeIsPurchaseNewCondo");
        list.Add("sFHAPurposeIsPurchaseExistCondo");
        list.Add("sFHAPurposeIsPurchaseNewHome");
        list.Add("sFHAPurposeIsRefinance");
        list.Add("sFHAPurposeIsConstructHome");
        list.Add("sFHAPurposeIsPurchaseManufacturedHome");
        list.Add("sFHAPurposeIsManufacturedHomeAndLot");
        list.Add("sFHAPurposeIsRefiManufacturedHomeToBuyLot");
        list.Add("sFHAPurposeIsRefiManufacturedHomeOrLotLoan");
        list.Add("sFHAPurposeIsEnergyEfficientMortgage");
        list.Add("sFHACcTot");
        list.Add("sTotalScoreDataConflictSourceT");
        list.Add("sTotalScorePreReviewResultT");
        list.Add("sTotalScoreReviewRules");
        list.Add("aBMaritalStatT");
        list.Add("sTotalScoreHasConflictBetweenFhaTransmittalAnd1003");
        list.Add("aLiaBalTot");
        list.Add("aReTotVal");
        list.Add("aReTotMAmt");
        list.Add("sMortgageLiaList");
        list.Add("sFannieCommunityLendingT");
        list.Add("sProdIncludeMyCommunityProc");
        list.Add("sTotalScoreIsIdentityOfInterestException");
        
        list.Add("sLiaBalLTot");
        list.Add("sAsstLValTot");
        list.Add("sH4HCertificateXmlContent");
        list.Add("sProd3rdPartyUwResultT");
        list.Add("sCustomField2D");
        list.Add("sfSetH4HStatus");
        list.Add("aBH4HAssetLessRetirement");
        list.Add("aCH4HAssetLessRetirement");
        list.Add("aBH4HLiabilityLessRetirement");
        list.Add("aCH4HLiabilityLessRetirement");
        list.Add("aBH4HNetworthLessRetirement");
        list.Add("aCH4HNetworthLessRetirement");
        list.Add("BranchNm");
        list.Add("sLNm");
        list.Add("sTotalScoreFhaProductT");
        list.Add("sTotalScoreFrontEndRatio");
        list.Add("sTotalScoreBackEndRatio");
        list.Add("sfTransformDataToPml");
        #endregion

        s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

    }

    public CPortalPipelineData(Guid fileId)
        : base(fileId, "CPortalPipelineData", s_selectProvider)
    {
    }
}