﻿
using System;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading;
using System.Web;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.ObjLib.Security;

namespace ConsumerPortal.lib
{
    public class CPTools
    {
        public static ConsumerUserPrincipal CurrentUser
        {
            get
            {
                return Thread.CurrentPrincipal as ConsumerUserPrincipal;
            }
        }

        public static bool hasProfilePic(Guid imageId)
        {
            string key = imageId.ToString() + ".profilepic.jpg";

            return FileDBTools.DoesFileExist(E_FileDB.Normal, key);
        }

        public static Guid RetrieveBrokerIdFromSiteId(Guid SiteId)
        {
            return BrokerDB.GetBrokerIdByBrokerPmlSiteId(SiteId);

        }

        public static Guid RetrieveSiteIdFromBrokerId(Guid BrokerId)
        {
            try
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", BrokerId)
                                            };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerId, "RetrieveLenderSiteIDByBrokerID", parameters))
                {
                    if (!reader.Read())
                    {
                        return Guid.Empty;
                    }
                    else
                    {
                        return new Guid(reader["BrokerPmlSiteID"].ToString());
                    }
                }
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                return Guid.Empty;
            }
        }

        public static void RedirectLogin(Guid siteId)
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Response.Redirect("~/login.aspx?SiteId=" + siteId.ToString(), true );
            }
        }

        public static void RedirectValidateSsn()
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Response.Redirect("~/validation.aspx", true);
            }
        }

        public static void RedirectPipeline()
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Response.Redirect("~/pipeline.aspx", true);
            }
        }

        public static void RedirectLogout()
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }

        }

        public static void RedirectError(string msg)
        {
            if (HttpContext.Current != null)
            {
                string param = "";
                if (!string.IsNullOrEmpty(msg))
                {
                    param = "?errmsg=" + msg;
                }
                HttpContext.Current.Response.Redirect("~/common/AppError.aspx" + param, true);
            }

        }

        public static void RedirectFileNotFound()
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ContentType = "text/html";
                HttpContext.Current.Response.StatusCode = 404;
                HttpContext.Current.Response.StatusDescription = "File not Found";
                HttpContext.Current.Response.Write("File not Found");
                HttpContext.Current.Response.End();
            }
        }

    }
}


