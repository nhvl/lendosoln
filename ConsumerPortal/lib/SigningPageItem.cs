﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LendersOffice.Constants;

namespace ConsumerPortal.lib
{
    public class SigningPageItem
    {
        public SigningPageItem()
        {
            Scale = ConstAppDavid.PdfPngScaling;
        }
        public Guid RequestId { get; set; }
        public int Page { get; set; }

        public float Scale { get; set; } // 1 = 100%, 1.5 = 150% etc.
        public int PageWidth { get; set; }
        public int PageHeight { get; set; }
    }
}
