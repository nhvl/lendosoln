﻿using System;
using System.Collections;
using System.Data;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using LendersOffice.ObjLib.Security;
using System.Collections.Generic;

namespace ConsumerPortal.lib
{
    public class PasswordHelper
    {
        public static bool IsValidResetCode(string email, string token, int minutes)
        {
            Int64 ConsumerId = -1;
            Guid BrokerId = Guid.Empty;
            double EllapsedMinutes = 1000; //Arbitrary large value

            RetrieveConsumerInfromFromResetCode(email, token, out BrokerId, out ConsumerId, out EllapsedMinutes);

                if (ConsumerId == -1)
                    return false;

                if (minutes > 0 && EllapsedMinutes > minutes)
                    return false;

                return true;            
        }

        public static bool IsValidResetCode(string email, string token, out int minutes)
        {
            Int64 ConsumerId = -1;
            Guid BrokerId = Guid.Empty;
            double EllapsedMinutes = 1000; //Arbitrary large value

            RetrieveConsumerInfromFromResetCode(email, token, out BrokerId, out ConsumerId, out EllapsedMinutes);
            minutes = Convert.ToInt16(EllapsedMinutes);

            if (ConsumerId == -1)
                return false;

            return true;
        }

        public static void RetrieveConsumerInfromFromResetCode(string email, string token, out Guid brokerId, out Int64 consumerId, out double nMinutes)
        {
            Int64 ConsumerId = -1;
            DateTime RequestTime = DateTime.MinValue;
            DateTime CurrentTime = DateTime.MinValue;
            Guid BrokerId = Guid.Empty;

            try
            {
                foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                {
                    SqlParameter[] parameters = {
                                            new SqlParameter("@LoginName", email),
                                            new SqlParameter("@PasswordResetCode", token)
                                        };

                    using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "CP_RetrieveConsumerIdByPasswordResetToken", parameters))
                    {
                        if (reader.Read())
                        {
                            ConsumerId = Int64.Parse(reader["ConsumerId"].ToString());
                            RequestTime = (DateTime)reader["PasswordRequestD"];
                            CurrentTime = (DateTime)reader["CurrentTimeD"];
                            BrokerId = (Guid)reader["BrokerId"];
                            break;
                        }
                    }
                }

                consumerId = ConsumerId;
                brokerId = BrokerId;
                nMinutes = CurrentTime.Subtract(RequestTime).TotalMinutes;
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }
        }

        public static Int64 RetrieveConsumerIdByEmailBroker(string username, Guid brokerId)
        {
            Int64 ConsumerId = -1;

            SqlParameter[] parameters = {
                                            new SqlParameter("@LoginName", username),
                                            new SqlParameter("@BrokerId", brokerId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "CP_RetrieveConsumerByEmailBroker", parameters))
            {
                if (reader.Read())
                {
                    ConsumerId = Int64.Parse(reader["ConsumerId"].ToString());
                }
            }

            return ConsumerId;
        }

        public static string GenerateRandomResetCode()
        {
            return Guid.NewGuid().ToString().Replace("-", "");
        }

        public static bool SetResetPasswordCode(Int64 ConsumerId, Guid BrokerId, string code)
        {
            if (ConsumerId < 0 || BrokerId == Guid.Empty || string.IsNullOrEmpty(code))
                return false;

            try
            {
                SqlParameter[] parameters = {
                                            new SqlParameter("@ConsumerId", ConsumerId),
                                            new SqlParameter("@BrokerId", BrokerId),
                                            new SqlParameter("@ResetCode", code)
                                        };

                int nRows = StoredProcedureHelper.ExecuteNonQuery(BrokerId, "CP_SetPasswordResetCodeByConsumerId", 3, parameters);

                return (nRows > 0);
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }
        }

        public static bool IsPasswordPassRuleValidation(string pw)
        {
            if (string.IsNullOrEmpty(pw)) //null or empty string
                return false;
            if (pw.Length < 6)          //minimum length = 6 chars
                return false;
            if ( !(Regex.IsMatch(pw, "[0-9]") && Regex.IsMatch(pw, "[a-zA-Z]")) ) //at least 1 letter and 1 number
                return false;

            return true;    //otherwise valid
        }

        public static string HashOfPassword(string password)
        {
            SHA256 sha1 = new SHA256Managed();
            return Convert.ToBase64String(sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password)));
        }

        public static int UpdatePasswordFromResetCode(string Email, string passwordHash, string resetCode)
        {
            try
            {
                foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                {
                    // 4/20/2015 dd - Since I don't know which database resetCode belong to, I will just issue reset to all databases.

                    SqlParameter[] parameters = {
                                            new SqlParameter("@PasswordHash", passwordHash),
                                            new SqlParameter("@LoginName", Email),
                                            new SqlParameter("@PasswordResetCode", resetCode)
                                        };

                    int nRows = StoredProcedureHelper.ExecuteNonQuery(connInfo, "CP_UpdatePasswordByResetCode", 3, parameters);

                    if (nRows > 0)
                    {
                        return nRows;
                    }
                }

                return 0;
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }

        }

        public static bool IsPasswordMatch(Guid brokerId, Int64 consumerId, string clearPassword)
        {
            string hash = HashOfPassword(clearPassword);

            SqlParameter[] parameters = {
                                            new SqlParameter("@ConsumerId", consumerId),
                                            new SqlParameter("@PasswordHash", hash)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "CP_CheckPasswordMatchByConsumerId", parameters))
            {
                if (reader.Read())
                {
                    return true;
                }
            }

            return false;
        }

        public static bool UpdateConsumerIdCurrentPassword(Int64 ConsumerId, string Email, Guid BrokerId, string currentClearPassword, string newClearPassword)
        {
            try
            {
                string hashOld = HashOfPassword(currentClearPassword);
                string hashNew = HashOfPassword(newClearPassword);

                SqlParameter[] parameters = {
                                            new SqlParameter("@ConsumerId", ConsumerId),
                                            new SqlParameter("@LoginName", Email),
                                            new SqlParameter("@BrokerId", BrokerId),
                                            new SqlParameter("@OldPasswordHash", hashOld),
                                            new SqlParameter("@NewPasswordHash", hashNew)
                                        };

                int nRows = StoredProcedureHelper.ExecuteNonQuery(BrokerId, "CP_UpdateConsumerPassword", 3, parameters);
                return (nRows > 0);

            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }
        }

        public static bool SetValidSsn(ConsumerUserPrincipal consumer, string ssn )
        {
            if (consumer == null)
                return false;

            string validSsn = (string.IsNullOrEmpty(ssn) || !Regex.IsMatch(ssn, @"\d\d\d\d")) ? "" : ssn;

            if (UpdateConsumerSsn(consumer, validSsn, 0, null))
            {
                consumer.SetSsn(ssn);
                return true;
            }
            return false;
        }

        public static bool IncreaseMissedSsn(ConsumerUserPrincipal consumer)
        {
            if (consumer == null)
                return false;

            string SsnDigits = "";
            int SsnFailureNumber = 0;
            DateTime LastSsnAttemptD = DateTime.MaxValue;

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@ConsumerId", consumer.ConsumerId) };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(consumer.BrokerId, "CP_RetrieveConsumerSsnInfoById", parameters))
                {
                    if (reader.Read())
                    {
                        SsnDigits = (string)reader["Last4SsnDigits"];
                        SsnFailureNumber = (int)reader["SsnAttemptFailureNumber"];
                        if (reader["LastSsnAttemptFailDate"] != DBNull.Value)
                        {
                            LastSsnAttemptD = (DateTime)reader["LastSsnAttemptFailDate"];
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                UpdateConsumerSsn(consumer, "", SsnFailureNumber + 1, System.DateTime.Now);
                return true;
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }

        }

        public static bool CanAttemptReadSsn(ConsumerUserPrincipal consumer, out DateTime nextAvailableTime)
        {
            nextAvailableTime = DateTime.MaxValue;

            if (consumer == null)
            {
                return false;
            }

            int SsnFailureNumber = 0;
            DateTime LastSsnAttemptD = DateTime.MaxValue;

            try
            {
                SqlParameter[] parameters = { new SqlParameter("@ConsumerId", consumer.ConsumerId) };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(consumer.BrokerId, "CP_RetrieveConsumerSsnInfoById", parameters))
                {
                    if (reader.Read())
                    {
                        SsnFailureNumber = (int)reader["SsnAttemptFailureNumber"];
                        if (reader["LastSsnAttemptFailDate"] != DBNull.Value)
                        {
                            LastSsnAttemptD = (DateTime)reader["LastSsnAttemptFailDate"];
                        }
                    }
                    else
                    {
                        Tools.LogError("Consumer entry not found. " + consumer.ToConsumerDataString() );
                        return false;
                    }
                }
               
                int nMin = CalcWaitMinutes(SsnFailureNumber);
                DateTime nextTime = DateTime.MaxValue;

                if (LastSsnAttemptD != DateTime.MaxValue)
                {
                    nextTime = LastSsnAttemptD.AddMinutes(nMin);
                }

                nextAvailableTime = nextTime;

                if (nMin == 0)
                    return true;

                return (nextTime < System.DateTime.Now);
                
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }
        }

        private static int CalcWaitMinutes(int nAttempts)
        {
            int minutes = 1;

            if (nAttempts <= 0)
                return 0;

            switch (nAttempts)
            {
                case 1:
                    minutes = 0;
                    break;
                case 2:
                    minutes = 0;
                    break;
                case 3:
                    minutes = 10;
                    break;
                case 4:
                    minutes = 30;
                    break;
                case 5:
                    minutes = 60;
                    break;
                case 6:
                    minutes = 600;
                    break;
                case 7:
                default:
                    minutes = 1140; //60*24 = 1440 minutes in 1 day
                    break;
            }

            return minutes;
        }

        private static bool UpdateConsumerSsn(ConsumerUserPrincipal consumer, string Last4SsnDigits, int SsnAttemptFailureNumber, DateTime? LastSsnAttemptFailDate)
        {
            try
            {
                if (consumer == null)
                    throw new Exception();

                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@ConsumerId", consumer.ConsumerId));
                param.Add(new SqlParameter("@BrokerId", consumer.BrokerId));

                if (Last4SsnDigits != null)
                {
                    param.Add(new SqlParameter("@Last4SsnDigits", Last4SsnDigits));
                }

                if (SsnAttemptFailureNumber >= 0 )
                {
                    param.Add(new SqlParameter("@SsnAttemptFailureNumber", SsnAttemptFailureNumber));
                }

                if (LastSsnAttemptFailDate.HasValue)
                {
                    param.Add(new SqlParameter("@LastSsnAttemptFailDate", LastSsnAttemptFailDate.Value));
                }
                
                int nRows = StoredProcedureHelper.ExecuteNonQuery(consumer.BrokerId, "CP_UpdateConsumerSsn", 3, param );
                return (nRows > 0);
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }
        }
    }
}


