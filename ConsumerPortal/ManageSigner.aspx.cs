﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using LendersOffice.ObjLib.Security;
using ConsumerPortal.lib;
using DataAccess;
using ConsumerPortal.Common;
using System.Text;
using System.Web.Services;
using ConsumerPortal.Constants;

namespace ConsumerPortal
{
    public partial class ManageSigner : ConsumerPortal.common.BaseCPortalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private static ConsumerUserPrincipal Consumer
        {
            get { return CPTools.CurrentUser; }
        }

        private static readonly string COOKIE_SIGNER = "CurrentSigner";
        private static string ReadCookie(string cookieName)
        {
            if (string.IsNullOrEmpty(cookieName))
                return "";

            HttpContext context = HttpContext.Current;
            HttpCookie ck = null;

            if (context != null)
            {
                ck = context.Request.Cookies[cookieName];
            }
            if (ck == null)
                return "";

            return ck.Value;
        }

        private static bool IsMatchCookie(ConsumerUserPrincipal con, ConsumerActionItem item, string cookieVal, out string SignerName, out string Initials, out string signatureId)
        {
            if (con == null || item == null || string.IsNullOrEmpty(cookieVal))
            {
                SignerName = "";
                signatureId = "";
                Initials = "";
                return false;
            }

            string app = item.aAppId.ToString().ToLower();
            string conId = con.ConsumerId.ToString().ToLower();
            string email = con.Email.ToLower();

            string name1 = item.aBFullName;
            string ini1 = item.aBInitials;
            string[] c1 = { app, conId, email, name1 };

            string name2 = item.aCFullName;
            string ini2 = item.aCInitials;
            string[] c2 = { app, conId, email, name2 };

            if (cookieVal == GetCookieHash(c1))
            {
                SignerName = name1;
                signatureId = "aBSignature";
                Initials = ini1;
                return true;
            }
            if (cookieVal == GetCookieHash(c2))
            {
                SignerName = name2;
                signatureId = "aCSignature";
                Initials = ini2;
                return true;
            }

            SignerName = "";
            signatureId = "";
            Initials = "";
            return false;
        }

        private static string GetCookieHash(string[] vals)
        {
            if (vals == null)
                return "";

            string str = "";
            foreach (string s in vals)
            {
                str += s;
            }

            SHA256 sha1 = new SHA256Managed();
            return Convert.ToBase64String(sha1.ComputeHash(Encoding.UTF8.GetBytes(str)));
        }

        [WebMethod]
        public static Signer GetCurrentSigner(long requestId)
        {
            Signer s1 = new Signer();

            try
            {
                ConsumerActionItem Request = new ConsumerActionItem(requestId);

                if (!ConstApp.IsChangeSignerDisabled) //We no longer allow changing signers!!!
                {
                    string SignerCookie = ReadCookie(COOKIE_SIGNER);
                    string SignerName = "";
                    string SignatureId = "";
                    string Initials = "";

                    if (IsMatchCookie(Consumer, Request, SignerCookie, out SignerName, out Initials, out SignatureId))
                    {
                        //Signer Detected
                        s1.IsValid = true;
                        s1.CanSign = true;
                        s1.FullName = SignerName;
                        s1.Initials = Initials;
                        s1.RequestId = requestId;
                        s1.SignatureId = SignatureId;
                        return s1;
                    }
                }

                //Signer Not Detected
                //Try to match logged in user by comparing e-mail address from consumer principal and request object
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(Request.sLId, typeof(ManageSigner));
                dataLoan.InitLoad();

                //Need to ensure e-mail address is unique within loan apps
                Dictionary<string, Guid> EmailDict = new Dictionary<string, Guid>();
                bool hasDuplicates = false;

                for (int i = 0; i < dataLoan.nApps; i++)
                {
                    CAppData App = dataLoan.GetAppData(i);
                    string bEmail = App.aBEmail.Trim().ToLower();
                    string cEmail = App.aCEmail.Trim().ToLower();
                    if (EmailDict.ContainsKey(bEmail) || EmailDict.ContainsKey(cEmail) || (bEmail == cEmail && bEmail.Length != 0))
                    {
                        hasDuplicates = true;
                        break;
                    }
                    if (bEmail.Length != 0)
                    {
                        EmailDict.Add(bEmail, App.aAppId);
                    }
                    if (cEmail.Length != 0)
                    {
                        EmailDict.Add(cEmail, App.aAppId);
                    }
                    
                }

                if (hasDuplicates)
                {
                    //Cannot identify signer, there's more than borrower with same e-mail in the loan
                    //Should we enforce unique e-mails within loan apps?
                    Tools.LogError(string.Format("Duplicate E-mail addresses found in loan '{0}'. E-mail list: {1}", dataLoan.sLId.ToString(), EmailDict.Values.ToString()));
                    s1.ErrorRedirect = ConstApp.IsChangeSignerDisabled;
                    return s1;
                }
                if (!EmailDict.ContainsKey(Consumer.Email.Trim().ToLower()))
                {
                    //Cannot identify signer, current consumer e-mail not found in the loan
                    //It should never happen, how can he access this loan????
                    Tools.LogBug(string.Format("Consumer '{0}' has access to loan '{1}', yet his e-mail address ({2}) is not found in any of the loan apps.", Consumer.ConsumerId, dataLoan.sLId.ToString(), Consumer.Email));
                    s1.ErrorRedirect = ConstApp.IsChangeSignerDisabled;
                    return s1;
                }

                Guid appId = EmailDict[Consumer.Email.Trim().ToLower()];
                if (appId == Request.aAppId)
                {
                    //Current Signer detected
                    CAppData App = dataLoan.GetAppData(appId);
                    string name = (App.aBEmail.Trim().ToLower() == Consumer.Email.Trim().ToLower()) ? Request.aBFullName : Request.aCFullName;
                    string initials = (App.aBEmail.Trim().ToLower() == Consumer.Email.Trim().ToLower()) ? Request.aBInitials : Request.aCInitials;
                    string signature = (App.aBEmail.Trim().ToLower() == Consumer.Email.Trim().ToLower()) ? "aBSignature" : "aCSignature";
                    string[] cc = { appId.ToString(), Consumer.ConsumerId.ToString(), Consumer.Email, name };

                    //set cookie value
                    CPRequestHelper.StoreToCookie(COOKIE_SIGNER, GetCookieHash(cc), false);

                    //return proper object
                    s1.CanSign = true;
                    s1.FullName = name;
                    s1.Initials = initials;
                    s1.RequestId = requestId;
                    s1.SignatureId = signature;
                    s1.IsValid = true;
                }
                else
                {
                    Tools.LogError("Unable to identify the appid from the consumer e-mail. Request.AppId: " + Request.aAppId.ToString());
                    s1.ErrorRedirect = ConstApp.IsChangeSignerDisabled;
                }

            }
            catch (Exception e)
            {
                Tools.LogError(e);
                s1.reset();
                s1.ErrorRedirect = ConstApp.IsChangeSignerDisabled;
            }

            return s1;
        }

        public static bool DetectLoggedInUser(ConsumerUserPrincipal consumer, ConsumerActionItem request, out E_eSignBorrowerMode UserMode)
        {
            string sign = "";
            string user = "";
            E_eSignBorrowerMode SignMode = E_eSignBorrowerMode.Both;

            if (DetectLoggedInUser(consumer, request, out user, out sign))
            {
                switch (sign)
                {
                    case "aBSignature":
                        SignMode = E_eSignBorrowerMode.Borrower;
                        break;
                    case "aCSignature":
                        SignMode = E_eSignBorrowerMode.Coborrower;
                        break;
                }
                UserMode = SignMode;
                return true;
            }
            UserMode = SignMode;
            return false;
        }

        public static bool DetectLoggedInUser(ConsumerUserPrincipal consumer, ConsumerActionItem request, out string LoggedInUser)
        {
            string sign = "";
            return DetectLoggedInUser(consumer, request, out LoggedInUser, out sign);
        }

        private static bool DetectLoggedInUser(ConsumerUserPrincipal consumer, ConsumerActionItem request, out string LoggedInUser, out string Signature)
        {
            Guid loanId = request.sLId;
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(ManageSigner));
            dataLoan.InitLoad();

            //Need to ensure e-mail address is unique within loan apps
            Dictionary<string, Guid> EmailDict = new Dictionary<string, Guid>();
            bool hasDuplicates = false;

            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData App = dataLoan.GetAppData(i);
                string bEmail = App.aBEmail.Trim().ToLower();
                string cEmail = App.aCEmail.Trim().ToLower();
                if (EmailDict.ContainsKey(bEmail) || EmailDict.ContainsKey(cEmail) || (bEmail == cEmail && bEmail.Length != 0))
                {
                    hasDuplicates = true;
                    break;
                }
                if (bEmail.Length != 0)
                {
                    EmailDict.Add(bEmail, App.aAppId);
                }
                if (cEmail.Length != 0)
                {
                    EmailDict.Add(cEmail, App.aAppId);
                }
            }

            if (hasDuplicates)
            {
                //Cannot identify signer, there's more than borrower with same e-mail in the loan
                //Should we enforce unique e-mails within loan apps?
                Tools.LogError(string.Format("Duplicate E-mail addresses found in loan '{0}'. E-mail list: {1}", dataLoan.sLId.ToString(), EmailDict.Values.ToString()));
                LoggedInUser = "";
                Signature = "";
                return false;
            }
            if (!EmailDict.ContainsKey(Consumer.Email.Trim().ToLower()))
            {
                //Cannot identify signer, current consumer e-mail not found in the loan
                //It should never happen, how can he access this loan????
                Tools.LogBug(string.Format("Consumer '{0}' has access to loan '{1}', yet his e-mail address ({2}) is not found in any of the loan apps.", Consumer.ConsumerId, dataLoan.sLId.ToString(), Consumer.Email));
                LoggedInUser = "";
                Signature = "";
                return false;
            }

            Guid appId = EmailDict[Consumer.Email.Trim().ToLower()];
            if (appId == request.aAppId)
            {
                //Logged in User detected
                CAppData App = dataLoan.GetAppData(appId);
                string name = (App.aBEmail.Trim().ToLower() == Consumer.Email.Trim().ToLower()) ? request.aBFullName : request.aCFullName;
                LoggedInUser = name;
                Signature = (App.aBEmail.Trim().ToLower() == Consumer.Email.Trim().ToLower()) ? "aBSignature" : "aCSignature";
                return true;
            }
            else
            {
                Tools.LogError("Unable to identify the appid from the consumer e-mail. Request.AppId: " + request.aAppId.ToString());
            }

            LoggedInUser = "";
            Signature = "";
            return false;
        }

        [WebMethod]
        public static SignerList GetAvailableSigners(long requestId)
        {
            
            SignerList s1 = new SignerList();

            try
            {
                ConsumerActionItem Request = new ConsumerActionItem(requestId);

                s1.aBNm = Request.aBFullName;
                s1.aCNm = Request.aCFullName;
                s1.IsApplicableToBorrower = Request.IsApplicableToBorrower;
                s1.IsApplicableToCoborrower = Request.IsApplicableToCoborrower;
                s1.RequestId = requestId;

                //Current Signer
                string SignerCookie = ReadCookie(COOKIE_SIGNER);
                string SignerName = "";
                string SignatureId = "";
                string Initials = "";

                if (IsMatchCookie(Consumer, Request, SignerCookie, out SignerName, out Initials, out SignatureId))
                {
                    //Signer detected
                    s1.CurrentSigner = SignerName;
                }

                //Current logged in
                string loggedInName = "";
                if (DetectLoggedInUser(Consumer, Request, out loggedInName))
                {
                    s1.CurrentLoggedIn = loggedInName;
                    if (SignerName.Length == 0)
                    {
                        s1.CurrentSigner = loggedInName;
                    }
                }
                s1.IsValid = true;
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                s1.reset();
            }
            finally
            {
                s1.ErrorRedirect = ConstApp.IsChangeSignerDisabled;
            }

            return s1;
        }

        [WebMethod]
        public static Signer SetCurrentSigner(long requestId, string SignerName)
        {
            Signer s1 = new Signer();

            try
            {
                if (requestId <= 0 || string.IsNullOrEmpty(SignerName))
                    throw new ArgumentException("Invalid arguments.");

                ConsumerActionItem Request = new ConsumerActionItem(requestId);

                //We no longer allow changing signers!!!
                if (!ConstApp.IsChangeSignerDisabled)
                {
                    if (SignerName.ToLower() != Request.aBFullName.ToLower() && SignerName.ToLower() != Request.aCFullName.ToLower())
                        throw new Exception("Invalid signer name.");

                    string name = (SignerName.ToLower() == Request.aBFullName.ToLower()) ? Request.aBFullName : Request.aCFullName;
                    string initials = (SignerName.ToLower() == Request.aBFullName.ToLower()) ? Request.aBInitials : Request.aCInitials;
                    string signature = (SignerName.ToLower() == Request.aBFullName.ToLower()) ? "aBSignature" : "aCSignature";
                    string[] cc = { Request.aAppId.ToString(), Consumer.ConsumerId.ToString(), Consumer.Email, name };

                    //set cookie value
                    CPRequestHelper.StoreToCookie(COOKIE_SIGNER, GetCookieHash(cc), false);

                    s1.IsValid = true;
                    s1.CanSign = true;
                    s1.FullName = SignerName;
                    s1.Initials = initials;
                    s1.RequestId = requestId;
                    s1.SignatureId = signature;

                    return s1;
                }

                //If IsChangeSignerDisabled, independently of the user they want to set, we force them to the logged in user!!!
                string CurrentSigner = "";
                if (DetectLoggedInUser(Consumer, Request, out CurrentSigner))
                {
                    string name = (CurrentSigner.ToLower() == Request.aBFullName.ToLower()) ? Request.aBFullName : Request.aCFullName;
                    string initials = (CurrentSigner.ToLower() == Request.aBFullName.ToLower()) ? Request.aBInitials : Request.aCInitials;
                    string signature = (CurrentSigner.ToLower() == Request.aBFullName.ToLower()) ? "aBSignature" : "aCSignature";

                    s1.IsValid = true;
                    s1.CanSign = true;
                    s1.FullName = SignerName;
                    s1.Initials = initials;
                    s1.RequestId = requestId;
                    s1.SignatureId = signature;

                    return s1;

                }
                else
                {
                    Tools.LogError("Unable to detect logged in User.");
                    s1.ErrorRedirect = ConstApp.IsChangeSignerDisabled;
                    return s1;
                }

                
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                s1.reset();
                s1.ErrorRedirect = ConstApp.IsChangeSignerDisabled;
            }

            return s1;
        }
    }

    public class SignerList
    {
        public bool IsValid;
        public string aBNm;
        public string aCNm;
        public bool IsApplicableToBorrower;
        public bool IsApplicableToCoborrower;
        public string CurrentSigner;
        public string CurrentLoggedIn;
        public long RequestId;
        public bool ErrorRedirect;

        public SignerList()
        {
            _init();
        }

        public void reset()
        {
            _init();
        }

        private void _init()
        {
            RequestId = -1;
            aBNm = "";
            aCNm = "";
            IsValid = false;
            CurrentSigner = "";
            CurrentLoggedIn = "";
            IsApplicableToBorrower = false;
            IsApplicableToCoborrower = false;
            ErrorRedirect = false;
        }

    }

    public class Signer
    {
        public long RequestId;
        public bool CanSign;
        public bool IsValid;
        public string FullName;
        public string Initials;
        public string SignatureId;
        public bool ErrorRedirect;

        public Signer()
        {
            _init();
        }

        public void reset()
        {
            _init();
        }

        private void _init()
        {
            RequestId = -1;
            CanSign = false;
            FullName = "";
            Initials = "";
            IsValid = false;
            SignatureId = "";
            ErrorRedirect = false;
        }
    }
}
