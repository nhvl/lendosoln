﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="changepassword.aspx.cs" Inherits="ConsumerPortal.changepassword" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="ConsumerPortal.Common" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
   <title>Change Password</title>
   <!--[if lte IE 7]><style type="text/css"> body { padding-top:175px; }</style><![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div id="content">
        <table id="UpdatePass" width="100%">
            <tr class="ResetHeader">
                <td align="center">Change Password</td>
            </tr>
            <tr>
            <td align="center">
                    <table class="reset" width="700">
                    <tr>
                        <td align="right">Current password:</td>
                        <td align="left"><asp:TextBox ID="CurrentPassword" CssClass="pword" runat="server" TextMode="Password" MaxLength="40" autocomplete="off"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="right">New password:</td>
                        <td align="left"><asp:TextBox ID="NewPassword" CssClass="pword" runat="server" TextMode="Password" MaxLength="40" autocomplete="off"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="right">Re-type new password:</td>
                        <td align="left"><asp:TextBox ID="NewPasswordConfirm" CssClass="pword" runat="server" TextMode="Password" MaxLength="40" autocomplete="off"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="UpdatePw"  CssClass="LoginBtn" runat="server" Text="Change Password" OnClick="UpdatePw_OnClick" />&nbsp;&nbsp;
                            <asp:Button ID="CancelBtn" CssClass="LoginBtn" runat="server" Text="Cancel" OnClick="CancelBtn_OnClick" />
                        </td>
                    </tr>
                    <tr>
                    <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" class="ErrorLabel" colspan="2">
                            <span id="ErrorMsg" runat="server"></span>
                        </td>
                    </tr>
                </table>
         </td>
         </tr>
       </table>
    </div>
    </form>
    <script type="text/javascript">
    <!--
        $j(document).ready(function() {
            var UpdatePw = document.getElementById("UpdatePw");
            $j(UpdatePw).attr('disabled', true);
            $j("#CurrentPassword").focus();

            $j(UpdatePw).click(function(event) {
                var IsValid = f_IsValidPassword();
                if (!IsValid)
                    event.preventDefault();
            });

            var keyUpListener = function(event) {
                var IsInvalid = ($j("#CurrentPassword").val().length == 0) || ($j("#NewPassword").val().length == 0) || ($j("#NewPassword").val().length != $j("#NewPasswordConfirm").val().length);
                $j(UpdatePw).attr('disabled', IsInvalid);
            };

            $j("#CurrentPassword").keyup(keyUpListener);
            $j("#NewPassword").keyup(keyUpListener);
            $j("#NewPasswordConfirm").keyup(keyUpListener);

            f_IsValidPassword = function() {
                try {
                    if (!f_followsPassRules($j("#CurrentPassword").val())) {
                        $j('#ErrorMsg').text(<%=AspxTools.JsString(ErrorMessages.CURRENT_PASSWORD_RULES) %>).show();
                        $j("#CurrentPassword").val('').focus();
                        return false;
                    }
                    if (!f_followsPassRules($j("#NewPassword").val())) {
                        $j('#ErrorMsg').text(<%=AspxTools.JsString(ErrorMessages.NEW_PASSWORD_RULES) %>).show();
                        $j("#NewPasswordConfirm").val('');
                        $j("#NewPassword").val('').focus();
                        return false;
                    }
                    if ($j("#NewPassword").val() != $j("#NewPasswordConfirm").val()) {
                        $j('#ErrorMsg').text(<%=AspxTools.JsString(ErrorMessages.PASSWORD_CONFIRMATION_MISSMATCH) %>).show();
                        $j("#NewPasswordConfirm").val('');
                        $j("#NewPassword").val('').focus();
                        return false;
                    }

                } catch (e) { return false; }
                $j('#ErrorMsg').val('').hide();
                return true;
            }

            f_followsPassRules = function(pw) {
                var regex1 = /[a-zA-Z]/;
                var regex2 = /[0-9]/;

                try {
                    if (!pw || pw.length < 6) return false;
                    if (!pw.match(regex1) && !pw.match(regex2)) return false;
                } catch (e) { return false };

                return true;
            }
            
            $j("input[type='password']").keypress(function(e){
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13){
                 $j(UpdatePw).click();
                }            
            });
        });
        
        

    //-->
    </script>
</body>
</html>
