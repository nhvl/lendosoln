﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="updatepassword.aspx.cs" Inherits="ConsumerPortal.resetconfirm" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="ConsumerPortal.Common" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Change Password</title>
    <!--[if lte IE 7]><style type="text/css"> body { padding-top:175px; }</style><![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div id="content">
        <table id="UpdatePass" width="100%">
        <asp:PlaceHolder ID="ResetPanel" runat="server">
            <tr>
                <td align="center">
                    <asp:Image runat="server" ID="Logo" ToolTip="Logo" />
                </td>
            </tr>
            <tr class="ResetHeader">
                <td align="center">Change Password</td>
            </tr>
                <tr>
                <td align="center">
                        <table class="reset" width="450">
                        <tr>
                            <td align="right">New password:</td>
                            <td align="left"><asp:TextBox ID="Password" CssClass="pword" runat="server" MaxLength="40" TextMode="Password" autocomplete="off"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="right">Re-type new password:</td>
                            <td align="left"><asp:TextBox ID="PasswordConfirm" CssClass="pword" runat="server" MaxLength="40" TextMode="Password" autocomplete="off"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="UpdatePw" CssClass="LoginBtn" runat="server" Text="Change Password" OnClick="UpdatePw_OnClick" />&nbsp;&nbsp;
                                <asp:Button ID="CancelBtn" CssClass="LoginBtn" runat="server" Text="Cancel" OnClick="CancelBtn_OnClick" />
                            </td>
                        </tr>
                        <tr>
                        <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <span id="ErrorMsg" class="ErrorLabel" runat="server"></span>
                                <span id="SuccessMsg" class="SuccessLabel" runat="server"></span>
                            </td>
                        </tr>
                    </table>
                    </td>
                </tr>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="EmptyPanel" Visible="false" runat="server">
                <tr class="msg">
                    <td align="center">This password reset code has expired. Please retrieve a new one and try again.</td>
                </tr>
            </asp:PlaceHolder>
        </table>
    
    </div>
    </form>
     <script type="text/javascript">
    <!--
        $j(document).ready(function() {
            var UpdatePw = document.getElementById("UpdatePw");
            $j(UpdatePw).attr('disabled', true);
            $j("#Password").focus();

            $j(UpdatePw).click(function(event) {
                var IsValid = f_IsValidPassword();
                if (!IsValid)
                    event.preventDefault();
            });

            var keyUpListener = function(event) {
                var IsInvalid = ($j("#Password").val().length == 0) || ($j("#Password").val().length != $j("#PasswordConfirm").val().length);
                $j(UpdatePw).attr('disabled', IsInvalid);
            };

            $j("#Password").keyup(keyUpListener);
            $j("#PasswordConfirm").keyup(keyUpListener);
            
            $j("input[type='password']").keypress(function(e){
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13){
                 $j(UpdatePw).click();
                }            
            });
        });
        
        f_IsValidPassword = function() {
                try {
                    if (!f_followsPassRules($j("#Password").val())) {
                        $j('#ErrorMsg').text(<%=AspxTools.JsString(ErrorMessages.NEW_PASSWORD_RULES) %>).show();
                        $j("#Password").val('').focus();
                        $j("#PasswordConfirm").val('');
                        return false;
                    }
                    if ($j("#Password").val() != $j("#PasswordConfirm").val()) {
                        $j('#ErrorMsg').text(<%=AspxTools.JsString(ErrorMessages.PASSWORD_CONFIRMATION_MISSMATCH) %>).show();
                        $j("#Password").val('').focus();
                        $j("#PasswordConfirm").val('');
                        return false;
                    }

                } catch (e) { return false; }
                $j('#ErrorMsg').val('').hide();
                return true;
            }

            f_followsPassRules = function(pw) {
                var regex1 = /[a-zA-Z]/;
                var regex2 = /[0-9]/;

                try {
                    if (!pw || pw.length < 6) return false;
                    if (!pw.match(regex1) && !pw.match(regex2)) return false;
                } catch (e) { return false };

                return true;
            }

    //-->
    </script>
</body>
</html>
