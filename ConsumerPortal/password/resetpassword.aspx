﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="resetpassword.aspx.cs" Inherits="ConsumerPortal.resetpassword" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Reset Password</title>
    <!--[if lte IE 7]><style type="text/css"> body { padding-top:175px; }</style><![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div id="content">
        <table id="ResetPass" width="100%">
        <asp:PlaceHolder ID="m_ResetPanel" runat="server">
            <tr>
                <td align="center"><asp:Image runat="server" ID="logo" ToolTip="logo" /> </td>
            </tr>
            <tr class="ResetHeader">
                <td align="center">Reset Password</td>
            </tr>
            <tr>
                <td align="center">
                    <table class="reset" width="390">
                        <tr>
                            <td nowrap="nowrap" align="right">E-mail:</td>
                            <td align="left">
                                <asp:TextBox ID="Email" runat="server" Width="240" autocomplete="off" TabIndex="1"></asp:TextBox>
                            </td>
                            <td align="left">
                                <asp:Button ID="m_next" CssClass="LoginBtn" runat="server" Text="Next" TabIndex="3" OnClick="OnNextBtnClick"></asp:Button>
                            </td>
                        </tr>    
                        <tr>
                            <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" class="ErrorLabel" colspan="3">
                                    <span id="ErrorMsg" runat="server"></span>
                                </td>
                            </tr>                    
                    </table>
                </td>
            </tr>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="m_SuccessMsgPanel" Visible="false" runat="server">
                <tr class="msg">
                    <td align="center">A password reset e-mail has been sent to you.</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">
                        <a id="goLogin" href="#">Back to login page</a>
                    </td>
                </tr>
            </asp:PlaceHolder>
        </table>
    
    </div>
    </form>
    <script type="text/javascript">
    <!--
        $j(document).ready(function() {
            var nextBtn = <%= AspxTools.JsGetElementById(m_next) %>;
            $j(nextBtn).attr('disabled', !f_IsValidEmail() );
            $j("#Email").focus();
            
            $j("#goLogin").click(function(event){
                 event.preventDefault();
                 window.location = "../login.aspx?SiteId=" + <%= AspxTools.JsString(SiteId) %>;
               });
            
             $j("#m_next").click(function(event){
                if (!f_IsValidEmail()){
                    event.preventDefault();
                    $j(nextBtn).attr('disabled', true);
                    return false;
                }
               });
            
            $j("#Email").keyup(function (event) {
                if (event.keyCode == 13){
                    event.preventDefault();
                    return false;
                }
                var IsValid = f_IsValidEmail();
                $j(nextBtn).attr('disabled', !IsValid);
            });
            $j("input[type='text']").keypress(function(e){
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13){
                 $j('#m_next').click();
                }            
            });

        });
        
        function f_IsValidEmail(){
          try{
              var regex = new RegExp(<%=AspxTools.JsString(ConstApp.EmailValidationExpression) %>);
              var src = <%=AspxTools.JsGetElementById(Email) %>;
              if (typeof src == undefined || src == null)
                return false;
              return src.value.match(regex);
          }catch(e){ return false; }
        }

    //-->
    </script>
</body>
</html>
