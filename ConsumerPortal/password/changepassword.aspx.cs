﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ConsumerPortal.lib;
using LendersOffice.ObjLib.Security;
using ConsumerPortal.Constants;
using ConsumerPortal.Common;

namespace ConsumerPortal
{
    public partial class changepassword : ConsumerPortal.common.BaseCPortalPage
    {
        protected ConsumerUserPrincipal CurrentUser
        {
            get { return CPTools.CurrentUser as ConsumerUserPrincipal; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowErrorLabel("" /*clear error message*/);
            CancelBtn.Visible = !CurrentUser.IsTempPassword;
        }

        private void ShowErrorLabel(string msg)
        {
            if (string.IsNullOrEmpty(msg))
            {
                ErrorMsg.InnerText = "";
                return;
            }

            ErrorMsg.InnerText = msg;
        }

        protected void UpdatePw_OnClick(object sender, System.EventArgs a)
        {
            string oldPw = CurrentPassword.Text.Trim();
            string newPw1 = NewPassword.Text.Trim();
            string newPw2 = NewPasswordConfirm.Text.Trim();

            if (newPw1 != newPw2)
            {
                ShowErrorLabel(ErrorMessages.PASSWORD_CONFIRMATION_MISSMATCH);
                NewPassword.Text = "";
                NewPasswordConfirm.Text = "";
                return;
            }

            if (oldPw == newPw1)
            {
                ShowErrorLabel(ErrorMessages.CANNOT_REPEAT_PASSWORD);
                NewPassword.Text = "";
                NewPasswordConfirm.Text = "";
                return;
            }

            if (!PasswordHelper.IsPasswordPassRuleValidation(newPw1))
            {
                NewPassword.Text = "";
                NewPasswordConfirm.Text = "";
                ShowErrorLabel(ErrorMessages.NEW_PASSWORD_RULES);
                return;
            }

            if (!PasswordHelper.IsPasswordMatch(CurrentUser.BrokerId, CurrentUser.ConsumerId, oldPw))
            {
                ShowErrorLabel(ErrorMessages.INVALID_CURRENT_PASSWORD);
                return;
            }

            if (!PasswordHelper.UpdateConsumerIdCurrentPassword(CurrentUser.ConsumerId, CurrentUser.Email, CurrentUser.BrokerId, oldPw, newPw1))
            {
                CurrentPassword.Text = "";
                ShowErrorLabel(ErrorMessages.UNABLE_UPDATE_PASSWORD);
                return;
            }
            
            CurrentUser.IsTempPassword = false; //The DB row has already been cleared by the previous SProc call

            //Show success message?

            if (CPRequestHelper.LoanID == Guid.Empty)
            {
                CPTools.RedirectPipeline();
            }
            else
            {
                Response.Redirect("~/loaninfo.aspx?loanid=" + CPRequestHelper.LoanID);
            }
        }

        protected void CancelBtn_OnClick(object sender, System.EventArgs a)
        {
            if (CPRequestHelper.LoanID == Guid.Empty)
            {
                CPTools.RedirectPipeline();
            }
            else
            {
                Response.Redirect("~/loaninfo.aspx?loanid=" + CPRequestHelper.LoanID);
            }
        }
        
    }
}
