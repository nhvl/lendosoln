﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ConsumerPortal.Common;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using DataAccess;
using ConsumerPortal.lib;
using ConsumerPortal.Constants;

namespace ConsumerPortal
{
    public partial class resetconfirm : ConsumerPortal.common.BaseCPortalPage
    {
        private Int64 m_consumerId = -1;
        private Guid m_brokerId = Guid.Empty;

        protected string Token
        {
            get
            {
                string token = CPRequestHelper.GetSafeQueryString("token");
                if (token != null)
                {
                    return Regex.Replace(token, "[^a-fA-F0-9]", "");
                }
                return token;
            }
        }

        protected string Email
        {
            get
            {
                return CPRequestHelper.GetSafeQueryString("email");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            int minutes = -1;

            if (string.IsNullOrEmpty(Token) || string.IsNullOrEmpty(Email) || !PasswordHelper.IsValidResetCode(Email, Token, out minutes))
            {
                CPTools.RedirectFileNotFound();
            }

      

            if (minutes > ConstApp.RESET_PASSWORD_EXPIRATION_MINUTES)
            {
                ResetPanel.Visible = false;
                EmptyPanel.Visible = true;
                return;
            }
            
            double min = 0;
            PasswordHelper.RetrieveConsumerInfromFromResetCode(Email, Token, out m_brokerId, out m_consumerId, out min);

            if (!IsPostBack)
            {
                Guid siteId = CPTools.RetrieveSiteIdFromBrokerId(m_brokerId);
                Logo.ImageUrl = "LogoPL.aspx?id=" + siteId.ToString("N");
            }

            ShowErrorLabel("" /*clear error message*/);
        }

        private void ShowErrorLabel(string msg)
        {
            if (string.IsNullOrEmpty(msg))
            {
                ErrorMsg.InnerText = "";
                return;
            }

            SuccessMsg.InnerText = "";
            ErrorMsg.InnerText = msg;
        }

        private void ShowSuccessLabel(string msg)
        {
            if (string.IsNullOrEmpty(msg))
            {
                SuccessMsg.InnerText = "";
                return;
            }
            
            ErrorMsg.InnerText = "";
            SuccessMsg.InnerText = msg;
        }


        protected void CancelBtn_OnClick(object sender, System.EventArgs a)
        {
            if (m_brokerId != Guid.Empty)
            {
                Guid SiteId = CPTools.RetrieveSiteIdFromBrokerId(m_brokerId);
                CPTools.RedirectLogin(SiteId);
            }
                

        }

        protected void UpdatePw_OnClick(object sender, System.EventArgs a)
        {
            string pw1 = Password.Text.Trim();
            string pw2 = PasswordConfirm.Text.Trim();
            int nRows = -1;
            
            if (pw1 != pw2)
            {
                ShowErrorLabel(ErrorMessages.PASSWORD_CONFIRMATION_MISSMATCH);
                Password.Text = "";
                PasswordConfirm.Text = "";
                return;
            }

            if (!PasswordHelper.IsPasswordPassRuleValidation(pw1))
            {
                ShowErrorLabel(ErrorMessages.NEW_PASSWORD_RULES);
                Password.Text = "";
                PasswordConfirm.Text = "";
                return;
            }

            if (string.IsNullOrEmpty(Token) || string.IsNullOrEmpty(Email) || !PasswordHelper.IsValidResetCode(Email, Token, ConstApp.RESET_PASSWORD_EXPIRATION_MINUTES))
            {
                ShowErrorLabel(ErrorMessages.INVALID_PASSWORD_RESET_CODE);
                return;
            }

            string passwordHash = PasswordHelper.HashOfPassword(pw1);
            nRows = PasswordHelper.UpdatePasswordFromResetCode(Email, passwordHash, Token);
            

            if (nRows <= 0)
            {
                ShowErrorLabel(ErrorMessages.UNABLE_UPDATE_PASSWORD);
                return;
            }

            if (m_brokerId != Guid.Empty)
            {
                Guid SiteId = CPTools.RetrieveSiteIdFromBrokerId(m_brokerId);
                CPTools.RedirectLogin(SiteId);
            }
            else
            {
                ShowSuccessLabel(UserMessages.PasswordSuccessfullyUpdated);
            }
        }

    }
}
