﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ConsumerPortal.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.AntiXss;
using System.Text.RegularExpressions;
using ConsumerPortal.lib;

namespace ConsumerPortal
{
    public partial class resetpassword : ConsumerPortal.common.BaseCPortalPage
    {
        protected Guid SiteId
        {
            get
            {
                return CPRequestHelper.GetGuid("SiteID", Guid.Empty);
            }
        }

        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SiteId == Guid.Empty)
            {
                CPTools.RedirectLogout();
            }

            logo.ImageUrl = "LogoPL.aspx?id=" + SiteId.ToString("N");
        }

        private void ShowErrorLabel(string msg)
        {
            if (string.IsNullOrEmpty(msg))
            {
                ErrorMsg.Visible = false;
                ErrorMsg.InnerText = "";
                return;
            }

            ErrorMsg.Visible = true;
            ErrorMsg.InnerText = msg;
        }


        protected void OnNextBtnClick(object sender, System.EventArgs a)
        {
            Guid brokerId = CPTools.RetrieveBrokerIdFromSiteId(SiteId);
            if (brokerId == Guid.Empty)
            {
                return;
            }

            Int64 ConsumerId = PasswordHelper.RetrieveConsumerIdByEmailBroker(Email.Text.Trim(), brokerId);

            if (ConsumerId == -1)
            {
                ShowErrorLabel(ErrorMessages.EmailAddressNotFound);
                return;
            }

            LendersOffice.Admin.BrokerDB broker = LendersOffice.Admin.BrokerDB.RetrieveById(brokerId);

            string resetCode = PasswordHelper.GenerateRandomResetCode();
            DateTime requestD = System.DateTime.Now;
            string ipAddress = Request.UserHostAddress;

            PasswordHelper.SetResetPasswordCode(ConsumerId, brokerId, resetCode);
            EmailHelper.GenerateAndSendEmail(brokerId, Email.Text, resetCode, requestD, ipAddress, broker.Name);
            
            //On success, show successful message:
            m_ResetPanel.Visible = false;
            m_SuccessMsgPanel.Visible = true;
        }

        

    }
}
