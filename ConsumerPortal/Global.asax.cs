﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using ConsumerPortal.Common;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.ObjLib.Security;
using LqbGrammar.Exceptions;
using LqbGrammar;

namespace ConsumerPortal
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            InitializeApplicationFramework();

            Tools.SetupServicePointCallback();
            Tools.SetupServicePointManager();
        }

        private static void InitializeApplicationFramework()
        {
            IExceptionHandlerFactory[] arrHandlers = ExceptionHandlerUtil.GetCoreExceptionHandlerFactories();
            using (IApplicationInitialize iAppInit = LqbApplication.CreateInitializer(arrHandlers))
            {
                const string AppName = "ConsumerPortal";
                iAppInit.SetName(AppName);

                Adapter.ApplicationInitializer.RegisterFactories(AppName, iAppInit);
                LendersOffice.ApplicationInitializer.RegisterFactories(AppName, iAppInit);
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        private int m_maxRequestSize = 300000;
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpRequest request = HttpContext.Current.Request;

            if (request.Url != null && request.Url.AbsolutePath != null && request.Url.AbsolutePath.ToLower().Contains("loaninfo.aspx"))
            {
                // OPM 144746.  Large file uploads on the loaninfo page are expected due to document upload.
                return;
            }

            if (request != null && request.ContentLength > m_maxRequestSize)
            {
                string str = string.Format("LARGE REQUEST PAGE - Url={0}, ContentLength={1}, TotalBytes={2}", request.RawUrl, request.ContentLength, request.TotalBytes);
                Tools.LogWarning(str);
                EmailUtilities.UpdateExceptionTracking(str);

                m_maxRequestSize = request.ContentLength;
            }

        }

        protected void Application_EndRequest(Object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            if (null != HttpContext.Current)
            {
                if (Response.ContentType == "text/html")
                {
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Cache.SetNoStore();
                }
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            CPRequestHelper.AuthenticateRequest();  
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();

            if (exc is System.OutOfMemoryException)
            {
                // A desparate call, assuming that we can get out-of-memory exception before garbage collection is invoked to do a full (time-consuming) job to collect all collectable memory.
                GC.GetTotalMemory(true);
            }

            // Transfer to custom error page for everyone except developer
            // testing on localhost.
            if (!Request.Url.IsLoopback)
            {
                ConsumerUserPrincipal principal = HttpContext.Current.User as ConsumerUserPrincipal;
                if (null != principal)
                {
                    ErrorUtilities.DisplayErrorPage(exc, true, principal.BrokerId, Guid.Empty);
                }
                else
                {
                    ErrorUtilities.DisplayErrorPage(exc, true, Guid.Empty, Guid.Empty);
                }
            }
            else
            {
                string sErrorMessage;
                if (null != exc.InnerException)
                    sErrorMessage = exc.InnerException.ToString();
                else
                    sErrorMessage = exc.ToString();

                Tools.LogError(sErrorMessage);
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}