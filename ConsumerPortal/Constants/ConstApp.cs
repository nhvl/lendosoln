﻿using System;
using System.Web.UI.HtmlControls;
using LendersOffice.Constants;
using System.Web;

namespace ConsumerPortal.Constants
{
    public class ConstApp
    {
        //Stage Constants
        public static ServerLocation CurrentServerLocation 
        {
            get 
            {
                ServerLocation ret = ServerLocation.Production;
                HttpContext context = HttpContext.Current;
                if (null != context) 
                {
                    if (context.Request.Url.IsLoopback)
                        ret = ServerLocation.LocalHost;
                    else if (context.Request.Url.Host.StartsWith("11.12.13."))
                        ret = ServerLocation.Development;
                    else if (context.Request.Url.Host.StartsWith("dev.cportal.com"))
                        ret = ServerLocation.Development;
                    else if (context.Request.Url.Host.StartsWith("test.cportal.com"))
                        ret = ServerLocation.Demo;                    
                }
                return ret;
            }
        }

        public static string CPortalPath
        {
            get
            {
                switch (CurrentServerLocation)
                {
                    case ServerLocation.LocalHost:
                        return "http://localhost/cportal/";
                    case ServerLocation.Development:
                        return "http://dev.cportal.com/";
                    case ServerLocation.Demo:
                        return "http://test.cportal.com/";
                    case ServerLocation.Production:
                    default:
                        return "https://cportal.lendingqb.com/";
                }
            }
        }

        //Login
        public static readonly int MaxFailedLoginAttempts = 3;
        public static int NumDaysTempPasswordIsValid
        {
             get
             {
                 return LendersOffice.Constants.ConstApp.CPTempPasswordValidLengthInDays;
             }
        }
        
        //Allow Change Signer
        public static readonly bool IsChangeSignerDisabled = true; //After the implementation it was decided it was to dangerous to do.

        //FileDB
        public static string FileDBSiteCode
        {
            get { return ConstStage.FileDBSiteCode; }
        }

        //Cookie
        public static readonly int CookieExpiresInMinutes = 30;

        //Password Reset
        public static readonly int RESET_PASSWORD_EXPIRATION_MINUTES = 30;

        //Images
        public static readonly string ProfileImagePath = "images/ProfileImage.aspx?ImageId=";
        public static readonly int MAX_IMG_SIZE = 500 * 1024; //500KB

        //Email
        //public static string SmtpServer
        //{
        //    get { return LendersOffice.Constants.ConstStage.SmtpServer; }
        //}
        public static readonly bool IncludeEmailDisclaimer = false;
        public static readonly string BCCEMAIL = "SystemSentBox@insightlendingsolutions.com";
        //public static readonly string NO_REPLY_EMAIL_ADDRESS = "noreply@lendingqb.com";
        public static readonly string PASSWORD_RESET_EMAIL_SUBJECT = "{0} ::: Password Reset Request";
        public static readonly string PASSWORD_RESET_EMAIL_BODY = 
                                         "<div style=\"font:Verdana; font-size:12pt; line-height:1.4em; color:#222;\">" +
                                         "<div>A password reset has been requested for your account. This request was performed on {2} from IP address {1}." +
                                         "<br/>If you did not request to reset your password, please ignore this e-mail.</div><br/>" +
                                         "<div>Otherwise, please click on <a href={3}>this link</a> or copy and paste the following address into your browser:<br/>" +
                                         "<span style=\"padding-left:2em;\">{4}</span></div><br />" +
                                         "<div>This will let you update your password during the following minutes.</div><br/>" +
                                         "<div style=\"color:#666; padding-top:15px;\">The {5} team</div>" +
                                         "<div style=\"font-size:10pt; color:#666;\">Please do not reply to this message; it was sent from an unmonitored email address.</div>" +
                                         "</div>";

        public static readonly string PASSWORD_RESET_PATH = CPortalPath + "password/";
        public static readonly string PASSWORD_RESET_FILENAME = "{0}updatepassword.aspx?email={1}&token={2}";

    }

}