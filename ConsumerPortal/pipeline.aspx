﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="pipeline.aspx.cs" Inherits="ConsumerPortal.pipeline" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register Src="~/Common/Header.ascx" tagname="Header" TagPrefix="uc" %>
<%@ Register Src="~/Common/Footer.ascx" tagname="Footer" TagPrefix="uc" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="ConsumerPortal.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Pipeline</title>
    <!--[if lte IE 7]><style type="text/css"> body { padding-top:30px; }</style><![endif]-->
    <!--This application uses open source software. For more information please see https://cportal.lendingqb.com/inc/js/thirdpartysoftware.txt.-->
</head>
<body>
    <uc:Header id="Header1" runat="server" />
    <form id="form1" runat="server">
    <div id="content">
    <table class="frameContent" cellspacing="0">
        <asp:PlaceHolder ID="m_gridPanel" runat="server">
        <tr>
            <td style="padding-left:3px;">Select the loan that you want to open</td>
            <td style="padding-right:3px;" align="right"><a href="logout.aspx" runat="server" >Logout</a></td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="body" colspan="2">
            
                  <table>
                     <tr id="Grid">
                        <td colspan="2">
                            <ml:CommonDataGrid AlternatingItemStyle-CssClass="alternate" HeaderStyle-CssClass="header" ID="m_dg" runat="server" Width="100%" EnableViewState="False">
                                <Columns>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <a href="loaninfo.aspx?loanid=<%# AspxTools.HtmlString( DataBinder.Eval(Container.DataItem, "sLId").ToString()) %>">open</a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Property" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <%# AspxTools.HtmlString(DisplayAddress(Container.DataItem)) %>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Loan Amount" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <%# AspxTools.HtmlString(FormatMoney(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "sLAmtCalc").ToString()))  )%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Lien Position" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <%# AspxTools.HtmlString(GetLienPosition( Convert.ToInt32(DataBinder.Eval(Container.DataItem, "sLienPosT").ToString())))%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="sOpenedD" HeaderText="Date Opened" DataFormatString="{0:d}" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"/>
                                </Columns>
                            </ml:CommonDataGrid>
                        </td>
                    </tr>
                  </table>
            
            </td>
        </tr>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="m_emptyPanel" runat="server">
            <tr>
                <td style="width:700px; text-align:center; padding-top:15px;">
                    <%=AspxTools.HtmlString(ConsumerPortal.Common.ErrorMessages.NoLoansAssociatedWithAccount) %>
                </td>
            </tr>
            <tr>
                <td style="text-align:center; padding-top:10px;">
                    <a href="logout.aspx" runat="server" >Logout</a>
                </td>
            </tr>
        </asp:PlaceHolder>
    </table>
    </div>
    </form>
    <uc:Footer ID="Footer1" runat="server" />
</body>
</html>
