﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data.SqlClient;
using System.Web.Security;
using LendersOffice.Security;
using LendersOffice.ObjLib.Security;
using ConsumerPortal.Security;
using ConsumerPortal.Common;
using ConsumerPortal.lib;
using System.Linq.Expressions;

namespace ConsumerPortal
{
    public partial class login : ConsumerPortal.common.BaseCPortalPage
    {
        private Guid m_brokerId = Guid.Empty;
        private Guid? m_SiteId = new Guid?();
        protected Guid SiteId
        {
            get
            {
                if (m_SiteId.HasValue)
                {
                    return m_SiteId.Value;
                }

                //handler encoded site id
                if (Request.QueryString.Count > 0 && Request.QueryString["SiteId"]  == null && Request.QueryString[0].Length == 22 ) 
                {
                    m_SiteId = Tools.GetGuidFrom(Request.QueryString[0]);   
                }
                else
                {
                    m_SiteId = CPRequestHelper.GetGuid("SiteID", Guid.Empty);
                }

                return m_SiteId.Value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ErrorMessages.Visible = false;

            if (SiteId == Guid.Empty)
            {
                Response.Redirect("~/logout.aspx");
            }
            else if (!IsPostBack && CPRequestHelper.GetSafeQueryString("errorcode") != null)
            {
                DisplayErrorMessage(CPRequestHelper.GetSafeQueryString("errorcode"));
            }
            else
            {
                m_brokerId = CPTools.RetrieveBrokerIdFromSiteId(SiteId);
                if (m_brokerId == Guid.Empty)
                {
                    Response.Redirect("~/logout.aspx");
                }
            }

            logo.Src = "images/LogoPL.aspx?id=" + SiteId.ToString();
        }

        protected void OnLoginClick(object sender, System.EventArgs a)
        {
            FormsAuthentication.SignOut();
			AbstractUserPrincipal principal = null;
			PrincipalFactory.E_LoginProblem loginProblem;

            principal = (AbstractUserPrincipal)ConsumerPrincipalFactory.CreatePrincipalWithFailureType(LoginName.Text.Trim(), Password.Text.Trim(), m_brokerId, out loginProblem);

            if (null != principal)
            {
                CPRequestHelper.StoreToCookie("CPortalSiteId", SiteId.ToString(), false);

                ConsumerUserPrincipal consumer = principal as ConsumerUserPrincipal;

                if (consumer != null && consumer.IsTempPassword)
                {
                    Response.Redirect("~/password/changepassword.aspx");
                }
                else
                {
                    CPTools.RedirectValidateSsn();
                }
            }
            else
            {
                ErrorMessages.Visible = false;
                string errCode = string.Empty;

                switch (loginProblem)
                {
                    case PrincipalFactory.E_LoginProblem.IsLocked:
                        errCode = "IsLocked";
                        break;
                    case PrincipalFactory.E_LoginProblem.InvalidLoginPassword:
                    // This batch should never be returned for a CP user; handle them anyway
                    case PrincipalFactory.E_LoginProblem.ActiveDirectorySetupIncomplete:
                    case PrincipalFactory.E_LoginProblem.InvalidAndWait:
                    case PrincipalFactory.E_LoginProblem.InvalidCustomerCode:
                    case PrincipalFactory.E_LoginProblem.IsDisabled:
                    case PrincipalFactory.E_LoginProblem.MustLoginAsActiveDirectoryUserType:
                    case PrincipalFactory.E_LoginProblem.NeedsToWait:
                        errCode = "InvalidLoginPassword";
                        break;
                    case PrincipalFactory.E_LoginProblem.TempPasswordExpired:
                        errCode = "TempExpired";
                        break;
                    case PrincipalFactory.E_LoginProblem.None:
                    default:
                        errCode = "InvalidLoginPassword";
                        Tools.LogBug("The ConsumerUserPrincipal is null, but no login problem error code was returned");
                        break;
                }

                Response.Redirect(Request.Url.AbsolutePath + "?SiteId=" + SiteId + "&errorcode=" + errCode);
            }

        }

        private void DisplayErrorMessage(string ErrorCode)
        {
            ErrorMessages.Visible = true;

            switch (ErrorCode)
            {
                case "IsLocked":
                    m_ErrMsg.Text = ConsumerPortal.Common.ErrorMessages.AccountLocked;
                    m_ErrMsg2.Text = ConsumerPortal.Common.ErrorMessages.PleaseContactYourAdministrator;
                    break;
                case "InvalidLoginPassword":
                    m_ErrMsg.Text = ConsumerPortal.Common.ErrorMessages.InvalidLoginPassword;
                    m_ErrMsg2.Text = "";
                    break;
                case "SessionExpired":
                    m_ErrMsg.Text = ConsumerPortal.Common.ErrorMessages.SessionExpired;
                    m_ErrMsg2.Text = "";
                    break;
                case "TempExpired":
                    m_ErrMsg.Text = ConsumerPortal.Common.ErrorMessages.TemporaryPasswordExpired;
                    m_ErrMsg2.Text = "";
                    break;
                case "None":
                default:
                    m_ErrMsg.Text = ConsumerPortal.Common.ErrorMessages.InvalidLoginPassword;
                    m_ErrMsg2.Text = "";
                    Tools.LogBug("Unknown login error code in Consumer Portal login page: " + ErrorCode + ".");
                    break;
            }

        }
    }
}
