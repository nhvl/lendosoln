﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Disclosure.aspx.cs" Inherits="ConsumerPortal.Disclosure" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Federal ESIGN Disclosure and Consent</title>
    <style type="text/css">
        
       div.disclosure {
        margin: 20px;
        overflow-y: auto;
        font-size: 11pt;
        width: 80%;
        height:  500px;
       }
       
       div.disclosure h1 {
            font-family: Cambria;
            font-size: 26pt;
            border-bottom: 3px rgb(123,161,206) solid;
       }
       div.disclosure h2 {
        color: rgb(123,161,206);
        font-size: 14pt;
        padding: 0;
        margin: 0;
        font-family: Cambria;
        
       }
       
       div.disclosure p, .styled {
        margin: 0;
        margin-bottom: 10px;
        padding: 0;
        font-family: Calibri;
             font-size: 11pt;
       }
       
       div.disclosure  ul {
        margin-top: 0px;
        padding-top: 0px;
        font-family: Calibri;
       
       }
        input.center { width: 100px; }
       .center { margin: 0 auto; text-align: center; display: block;}
       
    </style>
    <script type="text/javascript" src="inc/jquery-1.4.2.min.js"></script>
</head>
<body>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#Consent').click(function() {
                if (this.checked) {
                    $('.btn').removeAttr('disabled');
                } else {
                    $('.btn').attr('disabled', 'disabled');
                }
            });
            $('#Continue').click(function() {
                if ($('#Consent').checked) {
                    $(this).attr('disabled', 'disabled');
                    $('#form1').submit();
                }
            });
            $('.btn').attr('disabled', 'disabled');
        });
    </script>
    <form id="form1" runat="server">
    <div>
        <img runat="server" id="Logo" alt="logo" class="center" />
        <div class="disclosure">
            <h1>Federal ESIGN Disclosure and Consent</h1>
            <p>
                Please read this Federal ESIGN Disclosure and Consent carefully and keep a copy
                for your records. The following disclosures are required by the federal Electronic
                Signatures in Global and National Commerce Act (“ESIGN”).
            </p>
            <h2>Electronic Delivery of Disclosures and Notices</h2>
            <p>
                By clicking in the boxes at the bottom, you are consenting to receive the following
                disclosures, notices, terms and conditions, and other documents and all changes
                to the disclosures, notices, etc. electronically. In order to use the system and
                to access, receive and retain the disclosures, notices, etc. you must provide at
                your own expense an Internet connected device that is compatible with the system
                deployed at the time of access. Your device must meet the minimum requirements outlined
                below. You also confirm that your device will meet these specifications and requirements
                and will permit you to access and retain the disclosures and notices electronically
                each time you access and use the system.
            </p>
            <h2>Paper Delivery of Disclosures and Notices</h2>
            <p>
                You have the right to receive a paper copy of the disclosures, notices, terms and
                conditions, other documents, and any changes. To receive a paper copy at no charge,
                please request it in one of the following ways:
                <ul>
                    <li>Call us at
                        <ml:EncodedLiteral runat="server" ID="CompanyPhone1"></ml:EncodedLiteral>
                        and speak to the Customer Service Representative </li>
                    <li>Write to
                         <ml:EncodedLiteral runat="server" ID="CompanyName2"></ml:EncodedLiteral>, ATTN :  <ml:EncodedLiteral runat="server" ID="CompanyName3"></ml:EncodedLiteral>, <ml:EncodedLiteral runat="server" ID="CompanyAddress1"></ml:EncodedLiteral> <ml:EncodedLiteral runat="server" ID="CompanyCity"></ml:EncodedLiteral>  <ml:EncodedLiteral runat="server" ID="CompanyState"></ml:EncodedLiteral>   <ml:EncodedLiteral runat="server" ID="CompanyZip"></ml:EncodedLiteral> 
                        , with your name and mailing address. </li>
                    <li>Be sure to state that you are requesting a copy of the disclosures, notices, etc.</li>
                </ul>
            </p>
            <h2>System Requirements to Access Information</h2>
            <p>
                To receive an electronic copy of the disclosures, notices, terms and conditions,
                other documents, and changes you must have the following equipment and software:
                <ul>
                    <li>A personal computer or other device which is capable of accessing the Internet.
                        Your access to this page verifies that your system/device meets these requirements.</li>
                    <li>An Internet web browser which is capable of supporting 128-bit SSL encrypted communications,
                        which requires a minimum web browser version of either Microsoft® Internet Explorer
                        or Firefox and your system or device must have 128-bit SSL encryption software.
                        Your access to this page verifies that your browser and encryption software/device
                        meet these requirements. </li>
                    <li>You must have software which permits you to receive and access Portable Document
                        Format or “PDF” files, such as Adobe Acrobat Reader® version 8.0 and above (available
                        for downloading at <a href="http://www.adobe.com/products/acrobat/readstep2.html">http://www.adobe.com/products/acrobat/readstep2.html</a>).
                    </li>
                </ul>
            </p>
            <h2>System Requirements to Retain Information</h2>
            <p>
                To retain a copy of the disclosures, notices, terms and conditions, other documents,
                your device must have the ability to download and store PDF files. Withdrawal of
                Electronic Acceptance of Disclosures and Notices You can also contact us in any
                of the ways described in the preceding paragraph to withdraw your consent to receive
                any future disclosures, notices, terms and conditions, and other documents electronically.
            </p>
            
            <h2>Withdrawal of Electronic Acceptance of Disclosures and Notices</h2>
            <p>
                You can also contact us in any of the ways described in the preceding paragraph
                to withdraw your consent to receive any future disclosures, notices, terms and conditions,
                and other documents electronically.
            </p>

        </div>
                    <p class="styled">
                <asp:CheckBox runat="server" ID="Consent"  CssClass="accept_disc"/>
                        I consent to the electronic delivery of disclosures, notices, terms and conditions,
                        other documents, and any future changes. I also agree that
                        <ml:EncodedLiteral runat="server" ID="CompanyName1"></ml:EncodedLiteral>
                        does not need to provide me with an additional paper (non-electronic) copy of the
                        disclosures, notices, terms and conditions, and other documents, and any future
                        changes, unless specifically requested.
                        <br />
                <asp:Button  CssClass="center btn" runat="server" ID="Continue"  Text="Continue" OnClick="AcceptDisclosure" />
            </p>
    </div>
    </form>
</body>
</html>
