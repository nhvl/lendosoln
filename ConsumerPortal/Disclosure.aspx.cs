﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.Security;
using DataAccess;
using System.Data.SqlClient;
using LendersOffice.Email;
using ConsumerPortal.Common;
using LendersOffice.Admin;
using ConsumerPortal.Constants;
using LendersOffice.Constants;

namespace ConsumerPortal
{
    public partial class Disclosure : ConsumerPortal.common.BaseCPortalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ConsumerUserPrincipal p = ((ConsumerUserPrincipal)Page.User);
            if (p.AcceptedDisclosure)
            {
                Response.Redirect(ResolveUrl("~/pipeline.aspx"), true);
                return;
            }

            BrokerDB db = BrokerDB.RetrieveById(p.BrokerId);

            CompanyName1.Text = db.Name;
            CompanyName2.Text = db.Name;
            CompanyName3.Text = db.Name;
            CompanyAddress1.Text = db.Address.StreetAddress;
            CompanyCity.Text = db.Address.City;
            CompanyState.Text = db.Address.State;
            CompanyPhone1.Text = db.Phone;
            Logo.Src = "images/LogoPL.aspx?id="+ db.PmlSiteID.ToString();

        }

        protected void AcceptDisclosure(object sender, EventArgs args)
        {
            if (Consent.Checked == false)
            {
                return; //dont do anything
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@ConsumerId", ((ConsumerUserPrincipal)Page.User).ConsumerId)
                                        };

            Guid brokerId = ((ConsumerUserPrincipal)Page.User).BrokerId;

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "CP_UpdateDisclosureStatus", 3, parameters);

            //OPM 70215: They accepted, so send some e-mails.
            try
            {
                var allLoanIds = from s in Session["ids"]
                                            .ToString()
                                            .Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries) 
                                 select new Guid(s);

                string borrowerEmail = ((ConsumerUserPrincipal)Page.User).Email.Trim();
                foreach (Guid loanID in allLoanIds)
                {
                    CPageData pageData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanID, typeof(Disclosure));
                    pageData.InitLoad();
                    //Since a user might have more than one name associated with each application in the loan data,
                    //we need to see which name is associated with this particular loan

                    string borrowerName = null;

                    for (int i = 0; i < pageData.nApps; i++)
                    {
                        CAppData ad = pageData.GetAppData(i);
                        if(ad.aBEmail == borrowerEmail)
                        {
                            borrowerName = ad.aBLastFirstNm;
                            break;
                        }
                        else if(ad.aCEmail == borrowerEmail)
                        {
                            borrowerName = ad.aCLastFirstNm;
                            break;
                        }
                    }

                    //If we somehow didn't find anything, default to their e-mail address
                    if(string.IsNullOrEmpty(borrowerName.Trim()))
                    {
                        borrowerName = borrowerEmail;
                    }
                    
                    CBaseEmail toProcessor = null;
                    CBaseEmail toUnderwriter = null;

                    string emailSubject = string.Format(email_subject, pageData.sLNm, borrowerName);

                    if (pageData.sEmployeeProcessor.IsValid &&
                        !string.IsNullOrEmpty(pageData.sEmployeeProcessor.Email))
                    {
                        string emailBody = string.Format(email_body, pageData.sEmployeeProcessorName, borrowerName, pageData.sLNm, DateTime.Today.ToShortDateString());
                        toProcessor = new CBaseEmail(brokerId)
                        {
                            Subject = emailSubject,
                            From = ConstStage.DefaultDoNotReplyAddress,
                            Bcc = "",
                            To = pageData.sEmployeeProcessor.Email,
                            Message = emailBody,
                            IsHtmlEmail = false,
                            DisclaimerType = LendersOffice.Common.E_DisclaimerType.NORMAL
                        };
                    }
                    if (pageData.sEmployeeUnderwriter.IsValid &&
                        !string.IsNullOrEmpty(pageData.sEmployeeUnderwriter.Email))
                    {
                        string emailBody = string.Format(email_body, pageData.sEmployeeUnderwriterName, borrowerName, pageData.sLNm, DateTime.Today.ToShortDateString());
                        toUnderwriter = new CBaseEmail(brokerId)
                        {
                            Subject = emailSubject,
                            From = ConstStage.DefaultDoNotReplyAddress,
                            Bcc = "",
                            To = pageData.sEmployeeUnderwriter.Email,
                            Message = emailBody,
                            IsHtmlEmail = false,
                            DisclaimerType = LendersOffice.Common.E_DisclaimerType.NORMAL
                        };
                    }

                    EmailProcessor mailer = new EmailProcessor();
                    if (toProcessor != null)
                    {
                        mailer.Send(toProcessor);
                    }

                    //If the underwriter and the processor are the same person, don't e-mail them twice
                    if (toUnderwriter != null && (pageData.sEmployeeProcessorId != pageData.sEmployeeUnderwriterId))
                    {
                        mailer.Send(toUnderwriter);
                    }
                }
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }
            finally
            {
                //Even if we error out while sending email or something,
                //we want to make sure the user isn't affected by it.
                Session.Remove("ids");
                Response.Redirect(ResolveUrl("~/pipeline.aspx"));
            }

        }
        //0: Loan number
        //1: Name of consenting borrower
        private string email_subject = @"{0} - {1} - E-Sign Consent Received";

        //0: Recipient name
        //1: Name of consenting borrower
        //2: Loan number
        //3: Date of consent (today)
        private string email_body = @"
{0},

{1} has given consent to receive electronic document and signature requests for loan ""{2}"" on {3}.

This notification was automatically generated for you by LendingQB.
Please do not directly reply to this email. Please contact your LendingQB system administrator for any questions.";
    }
}
