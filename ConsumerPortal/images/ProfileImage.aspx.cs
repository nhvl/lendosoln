﻿using System;
using System.IO;
using System.Web.UI;
using ConsumerPortal.Common;
using ConsumerPortal.Constants;
using ConsumerPortal.lib;
using DataAccess;

namespace ConsumerPortal
{
    public partial class ProfileImage : ConsumerPortal.common.BaseCPortalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string imageId = CPRequestHelper.GetSafeQueryString("ImageId");

            if (!Page.IsPostBack && imageId != null)
            {
                Guid imageGuid = Guid.Empty;
                try
                {
                    imageGuid = new Guid(imageId);
                }
                catch { }

                if (imageGuid == Guid.Empty)
                    CPTools.RedirectFileNotFound();

                byte[] image = GetImageFromFileDB(imageGuid.ToString() + ".profilepic.jpg");
                
                if (image == null)
                    CPTools.RedirectFileNotFound();

                Response.Clear();
                Response.ContentType = "image/jpeg";
                Response.BinaryWrite(image);
                Response.End();
            }
        }

        private byte[] GetImageFromFileDB(string key)
        {
            byte[] image = null;

            Action<FileInfo> readHandler = delegate (FileInfo fi)
            {
                FileStream fs = new FileStream(fi.FullName, FileMode.Open);
                BufferedStream bs = new BufferedStream(fs);
                image = new byte[fs.Length];

                if (fs.Length > ConstApp.MAX_IMG_SIZE)
                {
                    throw new Exception("Image size too big. Maximum allowed size is 500KB. Found size: " + fs.Length + " bytes.");
                }

                if (fs.CanRead)
                {
                    fs.Read(image, 0, (int)fs.Length);
                }
            };

            try
            {
                FileDBTools.UseFile(E_FileDB.Normal, key, readHandler);
            }
            catch (FileNotFoundException)
            {
                return null;
            }

            return image;
        }
    }
}
