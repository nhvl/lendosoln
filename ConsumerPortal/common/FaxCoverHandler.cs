﻿using LendersOffice.Security;
using LendersOffice.ObjLib.Security;
using System.Threading;
using DataAccess;
using LendersOffice.Common;
using System.Web;
using System;

namespace ConsumerPortal.Common
{
    public class FaxCoverHandler : System.Web.IHttpHandler
    {
        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(System.Web.HttpContext context)
        {
            try
            {
                ConsumerUserPrincipal consumer = Thread.CurrentPrincipal as ConsumerUserPrincipal;
                if (consumer == null)
                {
                    DisplayAccessDeniedMessage(context);
                }
   
                string fax = CPRequestHelper.GetSafeQueryString("faxid");
                long faxid = -1;
                long.TryParse(fax, out faxid);

                if (faxid < 0)
                {
                    DisplayNotFoundMessage(context);
                }

                bool includeFaxCover = !RequestHelper.GetBool("NoFaxCover");
       
                byte[] faxCover = ConsumerActionItem.GetFaxCover(faxid, includeFaxCover);
                if (faxCover != null)
                {
                    GeneratePdf(context.Response, faxCover, "faxcover");
                }
                else
                {
                    DisplayAccessDeniedMessage(context);
                }
            }
            catch (AccessDenied)
            {
                DisplayAccessDeniedMessage(context);
            }

        }

        private void GeneratePdf(HttpResponse response, byte[] buffer, string fileName)
        {
            response.Cache.SetExpires(DateTime.Now);
            response.Cache.SetLastModified(DateTime.Now);
            response.Clear();
            response.ContentType = "application/pdf";
            response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
            response.OutputStream.Write(buffer, 0, buffer.Length);
            response.Flush();
            response.End();

        }

        private void DisplayAccessDeniedMessage(System.Web.HttpContext context)
        {
            string html = @"<html><body>Access Denied</body></html>";
            context.Response.Write(html);
            context.Response.Flush();
            context.Response.End();
        }

        private void DisplayNotFoundMessage(System.Web.HttpContext context)
        {
            string html = @"<html><body>Not found</body></html>";
            context.Response.Write(html);
            context.Response.Flush();
            context.Response.End();
        }

    }

}