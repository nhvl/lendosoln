<%@ Page language="c#" Codebehind="AppError.aspx.cs" AutoEventWireup="True" Inherits="ConsumerPortal.common.AppError" validateRequest=false%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register Src="~/Common/Footer.ascx" tagname="Footer" TagPrefix="uc" %>
<%@ Register Src="~/Common/Header.ascx" tagname="Header" TagPrefix="uc" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
  <title>Error Page</title>
</head>
<body onload="init();">
<uc:Header id="Header1" runat="server" />
<script type="text/javascript">
<!--
var g_sDisplayKey = <%= AspxTools.JsString(DISPLAY_KEY) %>;
function init() {
  <%-- Only applicable to duplicate login exception --%>
  <% if (m_isDuplicateLoginException) { %>
  if (window.dialogArguments != null) {
    window.dialogArguments.DuplicateLogin = true;
    self.close();
  }
  if (window.opener != null) {
    window.opener.location = window.opener.location;
    self.close();
  }
  <% } %>
  
  if (top.location.pathname.indexOf('/common/ModalDlg/DialogFrame.aspx') > 0) {
    if (top.frames["ModalFrame"].location.href.indexOf(g_sDisplayKey) < 0) {
      var ch = self.location.href.indexOf('?') > 0 ? '&' : '?';
      top.frames["ModalFrame"].location = self.location + ch + g_sDisplayKey + '=t'; // Don't display this error in frame.
    }
    return;
    
  }
  
  if (top.location.href.indexOf(g_sDisplayKey) < 0) {
    
    var ch = self.location.href.indexOf('?') > 0 ? '&' : '?';
    top.location = self.location + ch + g_sDisplayKey + '=t'; // Don't display this error in frame.
  }
}

function f_onCloseClick() {
  self.close();
}
//-->
</script>


  <form id="AppError" method="post" runat="server">
  <div id="content" style="width:500px;">
  <br />
  <div style="color:Red; font-weight:bold; text-align:center;">
    <%= AspxTools.HtmlString(m_errorMessage) %>
  </div>
  <br />
  <div id="MainTable" runat="server">
  <div style="background-color:#EEEEEE; padding:10px;">
  <table class="MainTable" cellspacing="0" cellpadding="0" width="35%" style="font-size:x-small;">
    <asp:Panel ID ="ErrorInformationPanel" Visible="false" runat="server">
    <tr>
      <td align="center" style="font-weight: bold; padding-left:10px; padding-right:10px;" noWrap>
        Please help us investigate the error by answering a few questions.
      </td>
    </tr>
    <tr>
      <td align="center" style="font-weight: bold; padding-left:10px; padding-right:10px">
        The error may be resolved by trying again.
      </td>
    </tr>
    <tr>
      <td>
        &nbsp;
      </td>
    </tr>
    <tr>
      <td>
        <table style="font-size:x-small">
          <tr>
            <td noWrap>
              Has this error occurred more than once today?
            </td>
            <td>
              <asp:RadioButton GroupName="m_rbError" ID="m_rbRecurringError" Text="Yes" runat="server" />
              <asp:RadioButton GroupName="m_rbError" ID="m_rbFirstTimeError" Text="No" Checked="true" runat="server" />
            </td>
          </tr>
          <tr>
            <td>
              <ml:EncodedLabel ID="Label1" runat="server" AssociatedControlID="m_tbErrorOnPage" Text="What page were you working on?" />
            </td>
            <td>
              <asp:TextBox ID="m_tbErrorOnPage" runat="server"></asp:TextBox>
            </td>
          </tr>
          <tr>
            <td>
              <ml:EncodedLabel ID="Label5" runat="server" AssociatedControlID="m_tbLastClicked" Text="What button or link did you last click?" />
            </td>
            <td>
              <asp:TextBox ID="m_tbLastClicked" runat="server"></asp:TextBox>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td>
        &nbsp;
      </td>
    </tr>
    <tr>
      <td>
        <table style="font-size:x-small">
          <tr>
            <td style="font-weight: bold;" colspan="4">
              Contact Information
            </td>
          </tr>
          <tr>
            <td colspan="2">
            </td>
            <td>
              <ml:EncodedLabel ID="Label2" runat="server" AssociatedControlID="m_tbName" Text="Name:" />
            </td>
            <td>
              <asp:TextBox Width="300px" ID="m_tbName" runat="server" />
            </td>
          </tr>
          <tr>
            <td style="font-weight: bold;" colspan="4">
              Contact me by
            </td>
          </tr>
          <tr>
            <td style="width:5%">
            </td>
            <td style="width:5%">
              <asp:CheckBox ID="m_cbPhone" runat="server" />
            </td>
            <td style="width:15%">
              <ml:EncodedLabel ID="Label3" runat="server" AssociatedControlID="m_tbPhone" Text="Phone:" />
            </td>
            <td>
              <ml:PhoneTextBox Width="300px" ID="m_tbPhone" runat="server" preset="phone" />
            </td>
          </tr>
          <tr>
            <td>
            </td>
            <td>
              <asp:CheckBox ID="m_cbEmail" runat="server" />
            </td>
            <td>
              <ml:EncodedLabel ID="Label4" runat="server" AssociatedControlID="m_tbEmail" Text="Email:" />
            </td>
            <td>
              <asp:TextBox Width="300px" ID="m_tbEmail" runat="server" />
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td>
        &nbsp;
      </td>
    </tr>
    <tr align="right">
      <td noWrap>
        <asp:button class="paddedButton" id="m_btnClose" Runat="server" Text="Ignore &amp; Close" OnClick="m_btnClose_Click" />
        <asp:button class="paddedButton" id="m_btnEmail" runat="server" Text="Submit &amp; Close" onclick="m_btnEmail_Click" />
      </td>
    </tr>  
    </asp:Panel>
    <tr>
      <td style="font-weight:bold; font-size:x-small">
        Error ID: <%= AspxTools.HtmlString(m_errorReferenceNumber) %>
      </td>
    </tr>
  </table>
  </div>
  </div>
  </div>
  </form>
<uc:Footer ID="Footer1" runat="server" />
</body>
</html>