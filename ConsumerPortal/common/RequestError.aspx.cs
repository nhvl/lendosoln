﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.AntiXss;
using System.Text.RegularExpressions;
using DataAccess;
using System.Web.UI.HtmlControls;

namespace ConsumerPortal
{
    public partial class RequestError : ConsumerPortal.common.BaseCPortalPage
    {
        protected HtmlForm form1;
        protected HtmlInputHidden hField;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string url = Request.QueryString["url"];
                string match = "^https?://" + Request.Url.Host;
                url = Server.UrlDecode(url);

                hField.Value = Regex.IsMatch(url, match, RegexOptions.IgnoreCase | RegexOptions.Singleline) ? AspxTools.HtmlString(url) : "";
            }
            catch { hField.Value = ""; }
        }
    }
}
