﻿using System;
using System.Data.Common;
using System.Data.SqlClient;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Security;
using ConsumerPortal.Security;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.ObjLib.Security;

namespace ConsumerPortal.Common
{
    public class CPRequestHelper
    {

        public static void AuthenticateRequest()
        {
            if (!IsPageRequireAuthenticate())
            {
                return;
            }

            HttpContext context = HttpContext.Current;

            if (context.SkipAuthorization || context.User == null ||
                !context.User.Identity.IsAuthenticated ||
                context.User.Identity.AuthenticationType != "Forms")
            {
                Signout();
                return;
            }

            
            FormsIdentity identity = (FormsIdentity)context.User.Identity;
            FormsAuthenticationTicket ticket = identity.Ticket;

            IPrincipal user = ConsumerPrincipalFactory.Create(identity);
            context.User = user;

            if (!context.User.Identity.IsAuthenticated || IsAccountLocked(user))
            {
                Signout();
                return;
            }

            Thread.CurrentPrincipal = context.User;
        }

        private static bool IsPageRequireAuthenticate()
        {
            string[] ignorePages = {
                                       "login.aspx",
                                       "resetpassword.aspx",
                                       "updatepassword.aspx",
                                       "logout.aspx",
                                       "logopl.aspx",
                                       "logger.aspx",
                                       "sessionexpired.aspx",
                                       "webresource.axd",
                                       "verifytransaction.aspx"
                                   };

            HttpContext context = HttpContext.Current;
            string currentPath = context.Request.Url.AbsolutePath.ToLower();
            foreach (string page in ignorePages)
            {
                // 8/26/2004 dd - These pages doesn't need to authenticate.

                if (currentPath.EndsWith(page.ToLower()))
                {
                    context.User = new GenericPrincipal(new GenericIdentity("Foo"), new string[0]);
                    return false;
                }
            }
            return true;
        }

        private static bool IsAccountLocked(IPrincipal user)
        {
            ConsumerUserPrincipal consumer = user as ConsumerUserPrincipal;
            
            if (consumer != null)
            {
                SqlParameter[] parameters = { new SqlParameter("@ConsumerId", consumer.ConsumerId) };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(consumer.BrokerId, "CP_IsConsumerLocked", parameters))
                {
                    if (reader.Read())
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static void Signout()
        {
            FormsAuthentication.SignOut();
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Response.Redirect("~/logout.aspx");
            }
        }

        public static void StoreToCookie(string key, string value, bool isPersist)
        {
            HttpCookie cookie = new HttpCookie(key, value);
            if (ConstSite.IsSecureCookieRequired)
            {
                cookie.Secure = true;
            }
            cookie.HttpOnly = true;

            if (isPersist)
            {
                cookie.Expires = DateTime.Now.AddDays(1);
            }
            if (null != HttpContext.Current)
            {
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }

        /// <summary>
        /// Return Guid from Request.QueryString. Return default value if
        /// QueryString value does not exist.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static Guid GetGuid(string key, Guid defaultValue)
        {
            Guid value = defaultValue;
            try
            {
                string val = HttpContext.Current.Request.QueryString[key];
                if (val != "" && val != null)
                {
                    value = new Guid(val);
                }

            }
            catch { }

            return value;
        }

        public static string GetSafeQueryString(string key)
        {
            return Utilities.SafeHtmlString(HttpContext.Current.Request.QueryString[key]);
        }

        /// <summary>
        /// Return loanid value in Request.QueryString.
        /// </summary>
        public static Guid LoanID
        {
            get
            {
                string key = "rLoanID";
                if (HttpContext.Current.Items[key] == null || HttpContext.Current.Items[key].GetType() != typeof(Guid))
                {
                    Guid value = GetGuid("loanid");
                    HttpContext.Current.Items[key] = value;
                }
                return (Guid)HttpContext.Current.Items[key];

            }
        }

        /// <summary>
        /// Return a guid from Request.QueryString. Redirect to error page
        /// if value does not exist or value is malformat.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static Guid GetGuid(string key)
        {
            Guid value = Guid.Empty;
            try
            {
                if (HttpContext.Current.Request.QueryString[key] != "")
                {
                    value = new Guid(HttpContext.Current.Request.QueryString[key]);
                }

            }
            catch { }
            if (value == Guid.Empty)
            {
                //throw new CBaseException(ErrorMessages.Generic, key + " is not a valid Guid format.");
            }
            return value;
        }

    }
}