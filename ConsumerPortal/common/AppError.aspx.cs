using System;
using System.Web.UI;
using DataAccess;
using LendersOffice.Common;

namespace ConsumerPortal.common
{

    public partial class AppError : ConsumerPortal.common.BaseCPortalPage
	{
        protected const string DISPLAY_KEY = "_DA115D9487E84e1e8E5C424299F5B844";

        protected bool m_isDuplicateLoginException = false;
		private bool m_ExceptionMissing = false; // 01-31-08 av 

        protected string m_errorReferenceNumber = string.Empty;

        protected string m_errorMessage = string.Empty;
        protected bool m_isBrowserUnsupportError = false;

        protected override bool EnableIEOnlyCheck
        {
            get { return false; }
        }
		/// <summary>
		/// 01-30-08 av opm 19618 Retrieve Error From Cache  All the time not just on first load.
		/// </summary>
		/// <returns></returns>
		private ExceptionInfo GetErrorFromCache() 
		{
			ExceptionInfo eI;

            string errorId = RequestHelper.GetSafeQueryString("id");

			if ( string.IsNullOrEmpty(errorId) )
			{	
				if ( RequestHelper.GetSafeQueryString("errmsg") != null && RequestHelper.GetSafeQueryString("errmsg") != String.Empty )
				{
					eI = new ExceptionInfo(new CBaseException( RequestHelper.GetSafeQueryString("errmsg"), "Exception's origin is from the querystring of AppError.")); 
				}
				else 
				{	
					eI = new ExceptionInfo(new CBaseException( ErrorMessages.Generic, "No errmsg or exception id existed in the querystring of AppError." ));
				}
				m_ExceptionMissing = true;
			}
            else if (errorId == "AJAX")
            {
                // 8/12/2009 dd - Error from our background service.
                string msg = RequestHelper.GetSafeQueryString("msg");
                eI = new ExceptionInfo(new CBaseException(ErrorMessages.Generic, msg));

                Tools.LogErrorWithCriticalTracking(eI.ErrorReferenceNumber + "::" + msg);

                m_ExceptionMissing = true;
            }
            else
            {
                // Retrieve exception from cache.
                // 05/30/06 mf - As per OPM 2582, we no longer use server cache.
                // Instead of caching the entire Exception object, we only cache
                // the data this page needs in a ExceptionInfo object.


                eI = ErrorUtilities.RetrieveExceptionFromCache(errorId);

                if (eI == null)
                {
                    // We were unable to get the exception object from cache.
                    Tools.LogError("Unable to load Exception from cache in AppError.aspx");
                    eI = new ExceptionInfo(new CBaseException(ErrorMessages.Generic, "Failed to retrieve Exception from cache.  System clock may help determine thrower."));
                    m_ExceptionMissing = true;
                }
            }
			return eI; 
		
		}
		private void DisplayErrorMessage() 
		{			
			ExceptionInfo eI = GetErrorFromCache();
			m_isDuplicateLoginException = ( eI.ExceptionType == "LendersOffice.Security.DuplicationLoginException" );
            m_errorReferenceNumber = eI.ErrorReferenceNumber;
			m_errorMessage = eI.UserMessage;
            if (m_errorMessage == ErrorMessages.Generic)
                m_errorMessage = "There was an error processing your request.";
            if (m_isBrowserUnsupportError)
            {
                MainTable.Visible = false;
            }
		}

        protected void PageLoad(object sender, System.EventArgs e)
        {
            if (RequestHelper.GetSafeQueryString(DISPLAY_KEY) == "t")
            {
                if (Request.Form[m_btnEmail.ID] == "" || Request.Form[m_btnEmail.ID] == null)
                    DisplayErrorMessage();
            }
            ErrorInformationPanel.Visible = !m_isDuplicateLoginException; // dd 12/16/04 For now Feedback only work on error that IS NOT a Duplicate login

            this.m_tbErrorOnPage.Attributes.Add("onkeypress", "if (event.keyCode == 13 ) return false;");
            this.m_tbLastClicked.Attributes.Add("onkeypress", "if (event.keyCode == 13 ) return false;");
            this.m_tbName.Attributes.Add("onkeypress", "if (event.keyCode == 13 ) return false;");
            this.m_tbPhone.Attributes.Add("onkeypress", "if (event.keyCode == 13 ) return false;");
            this.m_tbEmail.Attributes.Add("onkeypress", "if (event.keyCode == 13 ) return false;");
            
            if (!Page.IsPostBack && Request.UrlReferrer != null)
            {
                ViewState["UrlReferrer"] = Request.UrlReferrer.ToString();
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);

        }
        #endregion

		// 01-31-08 av 
		private void DeleteFromCache() 
		{	
			if ( ! this.m_ExceptionMissing ) 
			{
				ErrorUtilities.ExpireException( RequestHelper.GetSafeQueryString("id") );
			}

		}

		// 01-31-08 av 
		protected void m_btnClose_Click(object sender, System.EventArgs e)
		{
            this.AddInitScriptFunction("f_onCloseClick");
			if ( m_ExceptionMissing ) 
				DeleteFromCache(); 
		}


        protected void m_btnEmail_Click(object sender, System.EventArgs e)
        {

			//1-30-08 av 19618
			ExceptionInfo eI = GetErrorFromCache(); 

            string errorReferenceNumber = eI.ErrorReferenceNumber; 
            string userMessage = eI.UserMessage;
            string developerMessage = eI.DeveloperMessage + "\n" + eI.OtherExceptionDetails; 
            
            string referrer = ViewState["UrlReferrer"] as string ?? "";

            Tools.LogErrorFeedback(errorReferenceNumber, userMessage, developerMessage, m_tbName.Text, m_tbPhone.Text, m_cbPhone.Checked, m_tbEmail.Text, m_cbEmail.Checked, m_rbRecurringError.Checked, m_tbErrorOnPage.Text, m_tbLastClicked.Text, referrer);
			// 01-31-08 av 
			m_btnClose_Click( sender, e);
        }
	}
}
