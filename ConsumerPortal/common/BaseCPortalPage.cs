﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using System.Web.UI;
using LendersOffice.AntiXss;
using DataAccess;
using MeridianLink.CommonControls;

namespace ConsumerPortal.common
{
    public abstract class BaseCPortalPage : LendersOffice.Common.BaseServicePage
    {
        private void AddJQueryScript()
        {
            if (this.Header != null)
            {
                this.EnableJquery = true;
                string script = @"
<script type=""text/javascript"">
<!--
$j(document).ready(function() { if (typeof(__jqueryInit) == 'function') __jqueryInit(); });
//-->
</script>
";
                ClientScript.RegisterStartupScript(this.GetType(), "__jqueryInit", script);

                this.Header.Controls.Add(new PassthroughLiteral() { Text = @"<base target=""_self"" />" });
            }
        }
        public override string StyleSheet
        {
            get { return VirtualRoot + "/css/style.css"; }
        }

        protected override E_JqueryVersion GetJQueryVersion()
        {
            return E_JqueryVersion._1_4_2;
        }
        
        protected override void OnInit(EventArgs e)
        {
            m_loadLOdefaultScripts = false;
            AddJQueryScript();
            this.Init += new EventHandler(BaseCPPage_Init);
            this.Load += new EventHandler(BaseCPPage_Load);
            base.OnInit(e);
        }

        void BaseCPPage_Init(object sender, EventArgs e)
        {
            RegisterJsScript("stacktrace.js");
            RegisterJsScript("cbase.js");
        }

        void BaseCPPage_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            AbstractUserPrincipal principal = this.Page.User as AbstractUserPrincipal;

            if (null != principal)
            {
                Guid sLId = RequestHelper.GetGuid("loanid", Guid.Empty);
                if (sLId != Guid.Empty)
                {
                    string message = (Page.IsPostBack ? "POSTBACK" : "GET") + " - " + this.Request.Url.AbsolutePath;
                }
                string name = string.IsNullOrEmpty(principal.LoginNm) ? "ConsumerPortal" : AspxTools.JsString("ConsumerPortal" + principal.LoginNm);
                AddInitScriptFunction("WindowName", "window.name = " + name + ";");
            }
        }

        public override void RegisterVbsScript(string vbs)
        {
            Tools.LogError("Cannot use VB script in ConsumerPortal.");
            throw new Exception("Cannot use VB script in ConsumerPortal. VB Script is IE dependant.");
        }

    }
}
