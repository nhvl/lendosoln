﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Header.ascx.cs" Inherits="ConsumerPortal.common.Header" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>

<noscript>
<div style="padding-left:45px">
<h2 class="header_error" > ERROR </h2>
<p class="header_warning">You need to enable javascript for this page to work correctly.</p>
</div>
</noscript>