﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ConsumerPortal.Common;
using DataAccess;
using System.Data.SqlClient;

namespace ConsumerPortal
{
    public partial class VerifyTransaction : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Guid transactionId = CPRequestHelper.GetGuid("id", Guid.Empty);
            if (transactionId != Guid.Empty)
            {
                bool isFound = false;
                foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                {
                    SqlParameter[] parameters = {
                                                    new SqlParameter("@SignedTransactionId", transactionId)
                                                };

                    using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "CP_GetTransactionDateBySignedTransactionId", parameters))
                    {
                        if (reader.Read())
                        {
                            DateTime signedD = (DateTime)reader["SignedD"];
                            m_result.Text = String.Format("The transaction is valid.  The document was signed on {0}.", signedD.ToShortDateString());
                            isFound = true;
                            break;
                        }
                    }
                }
                if (isFound == false)
                {
                    // We have no record of this transaction
                    m_result.Text = "This transaction is not valid.";
                }

            }
            else
            {
                // We did not get a valid guid out of the request string.
                m_result.Text = "Cannot verify transaction.";
            }

        }
    }
}
