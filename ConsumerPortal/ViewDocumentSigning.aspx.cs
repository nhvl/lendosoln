﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.PdfForm;
using ConsumerPortal.Common;
using DataAccess;

namespace ConsumerPortal
{
    public partial class ViewDocumentSigning : ConsumerPortal.common.BaseCPortalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int page = RequestHelper.GetInt("pg", 1);

            Guid req = CPRequestHelper.GetGuid("requestid");
            Guid sessionId = CPRequestHelper.GetGuid("sessionid", Guid.NewGuid());

            PdfFormCache formCache = PdfFormCache.LoadPdfFormCache(req, sessionId);
            if (formCache == null)
            {
                formCache = PdfFormCache.CacheThis(PdfForm.LoadConsumerFormById(req), sessionId);
            }

            if (page < 1 || page > formCache.NumberOfPages)
                page = 1;

            byte[] buffer = formCache.pngPages[page -1];
            RenderPng(buffer);
        }
        private void RenderPng(byte[] buffer)
        {
            Response.Clear();
            Response.ContentType = "image/png";
            Response.Cache.SetNoStore();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            Response.OutputStream.Write(buffer, 0, buffer.Length);
            Response.End();
        }
    }
}
