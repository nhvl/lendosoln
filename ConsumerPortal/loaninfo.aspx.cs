﻿namespace ConsumerPortal
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using ConsumerPortal.Common;
    using ConsumerPortal.Constants;
    using ConsumerPortal.lib;
    using DataAccess;
    using EDocs;
    using iTextSharp.text.pdf;
    using iTextSharp.text.exceptions;
    using LendersOffice.Admin;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.ObjLib.Security;
    using MeridianLink.CommonControls;
    using LendersOffice.AntiXss;

    public enum E_ConsumerMode
    {
        None = 0,
        Borrower,
        Coborrower
    }

    public partial class loaninfo : ConsumerPortal.common.BaseCPortalPage
    {
        private const string WRITTEN_SIGNATURE = "Written signature required.";
        private static ListItemType[] x_skipList = new ListItemType[] { 
                ListItemType.Footer,
                ListItemType.Header,
                ListItemType.Pager,
                ListItemType.Separator
            };

        protected void Page_Load(object sender, EventArgs e)
        {
            EnableJquery = true;
            RegisterJsScript("jquery-ui-1.8.custom.min.js");

            if (Page.IsPostBack)
            {
                var saveSuccess = ProcessUploads();

                var redirectUrl = Request.Url;
                redirectUrl = new Uri(redirectUrl.ToString().Replace("notpdf=t", ""));
                string notpdf = "";
                if (!saveSuccess)
                {
                    char prefix = '?';
                    if (Request.Url.Query.Contains('?')) prefix = '&';
                    notpdf = prefix + "notpdf=t";
                    redirectUrl = new Uri(redirectUrl.ToString() + notpdf);
                }

                //Redirect the user back to this page so they can't accidentally POST something twice,
                //but make sure to warn them if the thing they gave us was not a pdf.
                Response.Redirect(redirectUrl.ToString());
                return;
            }

            if (Request.QueryString.AllKeys.Contains("notpdf"))
            {
                ClientScript.RegisterHiddenField("notpdf", "");
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(CPRequestHelper.LoanID, typeof(loaninfo));
            dataLoan.InitLoad();

            if (CPTools.CurrentUser.NeedsSsnVerification(dataLoan.sLId))
            {
                CPTools.RedirectValidateSsn();
            }

            List<ReadOnlyConsumerActionItem> ItemList = (ReadOnlyConsumerActionItem.GetConsumerActionItems(dataLoan.sBrokerId, dataLoan.sLId));
            List<ReadOnlyConsumerActionItem> ItemsToSign = ItemList.FindAll(x => x.IsSignatureRequired);
            List<ReadOnlyConsumerActionItem> ItemsToSend = ItemList.FindAll(x => !x.IsSignatureRequired);

            List<object> DocsToSign = new List<object>();
            DataTable DocsToSend = new DataTable();
            DataTable SignedDocs = new DataTable();
            List<object> teamMembers = new List<object>();

            changePass.HRef = "password/changepassword.aspx?loanid=" + AspxTools.HtmlString(dataLoan.sLId.ToString());

            //Populate Grid Data mostly from ConsumerRequest
            FillLoanInfo(dataLoan);
            FillTeamInfo(dataLoan, teamMembers);
            FillDocsToSignInfo(dataLoan, ItemsToSign, out DocsToSign);
            FillDocsToSendInfo(ItemsToSend, out DocsToSend);
            FillDocsToDownload(dataLoan, out SignedDocs);
            
            //Bind DataGrids and Repeaters.
            m_DocsToBeSignedPanel.Visible = (DocsToSign.Count != 0);
            DocsToSign.Sort();
            m_DocsToBeSigned.DataSource = DocsToSign;
            m_DocsToBeSigned.DataBind();

            m_sentDocsPanel.Visible = (DocsToSend.Rows.Count != 0);
            m_sentDocs.DataSource = DocsToSend;
            m_sentDocs.DataBind();

            m_signedDocsPanel.Visible = (SignedDocs.Rows.Count != 0);
            m_signedDocs.DataSource = SignedDocs;
            m_signedDocs.DataBind();

            m_team.DataSource = teamMembers;
            m_team.DataBind();

            //Show empty messaage if now ros are found
            EmptyMsgPanel.Visible = !m_DocsToBeSignedPanel.Visible && !m_sentDocsPanel.Visible;
            EmptyMsgPanel2.Visible = !m_signedDocsPanel.Visible;
            m_RightFramePanel.Visible = (teamMembers.Count != 0);

            FileUpload.Accept = "application/pdf";
        }

        private bool ProcessUploads()
        {
            //Three cases
            //1. User isn't uploading a file at all.
            //2. User is uploading a file that was requested.
            //3. User is uploading a file without a request.

            //We use FileUpload for files uploaded without request, and RequestUpload for requested files. 

            HtmlInputFile upload = null;
            //Note that it's possible to make a 0 length file
            //however, that file will not be a valid pdf, so we don't care either way.
            if (RequestUpload.PostedFile != null && RequestUpload.PostedFile.ContentLength > 0)
            {
                upload = RequestUpload;
            }
            else if (FileUpload.PostedFile != null && FileUpload.PostedFile.ContentLength > 0)
            {
                upload = FileUpload;
            }
            else
            {
                //Guess there wasn't any file
                return false;
            }

            //Need to use the system repo because our user won't have any access at all
            var repo = EDocumentRepository.GetSystemRepository(CPTools.CurrentUser.BrokerId);


            try
            {
                long docId;
                if (long.TryParse(UploadedDocId.Value, out docId))
                {
                    var actionItem = new ConsumerActionItem(docId);
                    var bytes = GetBytes(upload);

                    //Called for the side-effect of throwing an InvalidPDFFileException if the file isn't a pdf.
                    EDocumentViewer.CreatePdfReader(bytes);

                    actionItem.AcceptConsumerUpload(bytes);

                    actionItem.Save();
                }
                else
                {
                    var doc = repo.CreateDocument(E_EDocumentSource.AcceptedFromConsumer);
                    SetupDoc(doc);
                    var unclassifiedId = EDocumentDocType.GetOrCreateBorrowerUpload(CPTools.CurrentUser.BrokerId);
                    doc.DocumentTypeId = unclassifiedId;
                    doc.PublicDescription = FileDesc.Value;

                    //Ends up throwing an InvalidPDFFileException if the file isn't a pdf.
                    CommitEdoc(repo, doc, upload);
                }
            }
            catch (InvalidPDFFileException)
            {
                //It wasn't a pdf, let the user know.
                return false;
            }

            return true;
        }

        private byte[] GetBytes(HtmlInputFile toBytes)
        {
            string filePath = TempFileUtils.NewTempFilePath();
            toBytes.PostedFile.SaveAs(filePath);
            return BinaryFileHelper.ReadAllBytes(filePath);
        }

        private E_eSignBorrowerMode ConvertToESignMode(E_ConsumerMode e_ConsumerMode)
        {
            switch (e_ConsumerMode)
            {
                case E_ConsumerMode.Borrower:
                    return E_eSignBorrowerMode.Borrower;
                case E_ConsumerMode.Coborrower:
                    return E_eSignBorrowerMode.Coborrower;
                default:
                    return E_eSignBorrowerMode.Both;
            }
        }

        private EDocument SetupDoc(EDocument doc)
        {

            doc.LoanId = CPRequestHelper.LoanID;
            doc.AppId = GetAppId(CPTools.CurrentUser.Email);
            doc.IsUploadedByPmlUser = false;

            return doc;
        }

        private void CommitEdoc(EDocumentRepository repo, EDocument doc, HtmlInputFile src)
        {
            string filePath = TempFileUtils.NewTempFilePath();
            src.PostedFile.SaveAs(filePath);
            doc.UpdatePDFContentOnSave(filePath);
            doc.EDocOrigin = E_EDocOrigin.ConsumerPortal;
            repo.Save(doc);
        }

        private Guid GetAppId(string email)
        {
            email = email.Trim();
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(CPRequestHelper.LoanID, typeof(loaninfo));
            dataLoan.InitLoad();
            Guid appId = dataLoan.GetAppData(0).aAppId;

            for (int i = 0; i < dataLoan.nApps; i++)
            {
                var app = dataLoan.GetAppData(i);
                if (app.aBEmail.Equals(email, StringComparison.InvariantCultureIgnoreCase))
                {
                    appId = app.aAppId;
                    break;
                }
                if (app.aCEmail.Equals(email, StringComparison.InvariantCultureIgnoreCase))
                {
                    appId = app.aAppId;
                    break;
                }
            }

            return appId;
        }

        private void FillLoanInfo(CPageData dataLoan)
        {
            sSpAddr.Text = dataLoan.sSpAddr;
            sSpCity.Text = Tools.CombineCityStateZip(dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip);
            sLProductT.Text = dataLoan.sLpTemplateNm;
            sLRate.Text = dataLoan.sNoteIR_rep;
            CashToClose.Text = dataLoan.sTransNetCash_rep;
            MonthlyPayment.Text = dataLoan.sProThisMPmt_rep;

            CAppData App = dataLoan.GetAppData(0);
            borrowerNm.Text = App.aAppCPortalFullNm;

            sOpenedD.Text       = dataLoan.sOpenedD_rep;
            sRegisteredD.Text   = dataLoan.sSubmitD_rep;
            sApprovedD.Text     = dataLoan.sApprovD_rep;
            sEstClosingD.Text   = dataLoan.sEstCloseD_rep;
            sReadyToCloseD.Text = dataLoan.sClearToCloseD_rep;
            sClosedD.Text       = dataLoan.sClosedD_rep;

        }

        private E_ConsumerMode DetectLoggedInUser(CPageData dataLoan, ReadOnlyConsumerActionItem item)
        {
            ConsumerUserPrincipal con = CPTools.CurrentUser;

            //Need to ensure e-mail address is unique within loan apps
            Dictionary<string, Guid> EmailDict = new Dictionary<string, Guid>();
            bool hasDuplicates = false;

            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData App = dataLoan.GetAppData(i);
                string bEmail = App.aBEmail.Trim().ToLower();
                string cEmail = App.aCEmail.Trim().ToLower();
                if (EmailDict.ContainsKey(bEmail) || EmailDict.ContainsKey(cEmail) || (bEmail == cEmail && bEmail.Length != 0))
                {
                    hasDuplicates = true;
                    break;
                }
                if (bEmail.Length != 0)
                {
                    EmailDict.Add(bEmail, App.aAppId);
                }
                if (cEmail.Length != 0)
                {
                    EmailDict.Add(cEmail, App.aAppId);
                }
            }

            if (hasDuplicates)
            {
                //Cannot identify signer, there's more than borrower with same e-mail in the loan
                //Should we enforce unique e-mails within loan apps?
                Tools.LogError(string.Format("Duplicate E-mail addresses found in loan '{0}'. E-mail list: {1}", dataLoan.sLId.ToString(), EmailDict.Values.ToString()));
                return E_ConsumerMode.None;
            }
            if (!EmailDict.ContainsKey(con.Email.Trim().ToLower()))
            {
                //Cannot identify signer, current consumer e-mail not found in the loan
                //It should never happen, how can he access this loan????
                Tools.LogBug(string.Format("Consumer '{0}' has access to loan '{1}', yet his e-mail address ({2}) is not found in any of the loan apps.", con.ConsumerId, dataLoan.sLId.ToString(), con.Email));
                return E_ConsumerMode.None;
            }

            Guid appId = EmailDict[con.Email.Trim().ToLower()];
            if (appId == item.AppId)
            {
                //Logged in User detected
                CAppData App = dataLoan.GetAppData(appId);
                return (App.aBEmail.Trim().ToLower() == con.Email.Trim().ToLower()) ? E_ConsumerMode.Borrower : E_ConsumerMode.Coborrower;
            }
            else
            {
                Tools.LogError("Unable to identify the appid from the consumer e-mail. Request.AppId: " + item.AppId.ToString());
            }
            return E_ConsumerMode.None;
        }

        private void FillDocsToSignInfo(CPageData dataLoan, List<ReadOnlyConsumerActionItem> ItemsToSign, out List<object> dt)
        {
            List<object> docList = new List<object>();

            foreach (ReadOnlyConsumerActionItem item in ItemsToSign)
            {
                DocInfo d1 = new DocInfo();
                d1.DocTitle = item.DocumentTypeDescription;
                d1.AppBorrowers = getAppNameFromConsumerActionItem(item);
                d1.faxId = item.Id;
                d1.signId = item.Id;
                
                if (item.ConsumerCompletedDate.HasValue)
                {
                    //It has been signed
                    d1.IsSigned = true;
                    d1.SignDesc = string.Format("Received on {0}", item.ConsumerCompletedDate.Value.ToShortDateString());
                }
                else
                {
                    //It needs to be signed
                    if (item.IsEsignAllowed)
                    {
                        //Is it half-signed? --> Disable fax
                        d1.HasSignAction = (item.IsApplicableToBorrower && item.HasBorrowerEsigned) || (item.IsApplicableToCoBorrower && item.HasCoborrowerEsigned);

                        E_ConsumerMode user = E_ConsumerMode.None;
                        if (ConstApp.IsChangeSignerDisabled)
                        {
                            user = DetectLoggedInUser(dataLoan, item);
                        }
                        
                        if (ConstApp.IsChangeSignerDisabled && d1.HasSignAction)
                        {
                            d1.CanSign  = (user == E_ConsumerMode.Borrower   && item.IsApplicableToBorrower   && !item.HasBorrowerEsigned);
                            d1.CanSign |= (user == E_ConsumerMode.Coborrower && item.IsApplicableToCoBorrower && !item.HasCoborrowerEsigned);
                            d1.RequireAction = d1.CanSign;
                        }

                        if (!d1.HasSignAction)
                        {
                            d1.RequireAction = true;

                            if (ConstApp.IsChangeSignerDisabled)
                            {
                                d1.CanSign = (user == E_ConsumerMode.Borrower && item.IsApplicableToBorrower && !item.HasBorrowerEsigned);
                                d1.CanSign |= (user == E_ConsumerMode.Coborrower && item.IsApplicableToCoBorrower && !item.HasCoborrowerEsigned);
                            }
                        }

                        //It can be signed online
                        bool missingBorrower = item.IsApplicableToBorrower && !item.HasBorrowerEsigned;
                        bool missingCoborrower = item.IsApplicableToCoBorrower && !item.HasCoborrowerEsigned;
                        string msg = "Missing {0}'{2} signature{1}.";
                        string plural = (missingBorrower && missingCoborrower) ? "s" : "";
                        string name = "";
                        string finalS = "s";
                        
                        string[] bNameList = item.BorrowerName.Trim().Split(' ');
                        string[] cbNameList = item.CoborrowerName.Trim().Split(' ');
                        string bFirstNm = (!string.IsNullOrEmpty(bNameList[0])) ? bNameList[0] : "borrower";
                        string cbFirstNm = (!string.IsNullOrEmpty(cbNameList[0])) ? cbNameList[0] : "coborrower";

                        if (missingBorrower)
                        {
                            name += bFirstNm;
                        }
                        if (missingCoborrower && name.Length != 0 && cbFirstNm.Length != 0)
                        {
                            name += " & ";
                        }
                        if (missingCoborrower)
                        {
                            name += cbFirstNm;
                        }
                        if (name.EndsWith("s"))
                        {
                            finalS = "";
                        }
                        d1.SignDesc = string.Format(msg, name, plural, finalS);
                    }
                    else
                    {
                        //It cannot be signed online
                        d1.IsWrittenRequired = true;
                        d1.SignDesc = WRITTEN_SIGNATURE;
                        d1.RequireAction = true;
                        d1.HasSignAction = false;
                    }
                }


                docList.Add(d1);
            }

            dt = docList;
        }

        private void FillDocsToSendInfo(List<ReadOnlyConsumerActionItem> ItemsToSend, out DataTable dt)
        {
            List<DocInfo> docList = new List<DocInfo>();

            foreach (ReadOnlyConsumerActionItem item in ItemsToSend)
            {
                DocInfo d1 = new DocInfo();
                
                d1.DocTitle = item.DocumentTypeDescription;
                d1.AppBorrowers = getAppNameFromConsumerActionItem(item);
                d1.DocDesc = item.RequestDescription;
                if (item.ConsumerCompletedDate.HasValue)
                {
                    d1.IsSent = true;
                    d1.DocDesc = string.Format("Received on {0}", item.ConsumerCompletedDate.Value.ToShortDateString());
                }
                d1.faxId = item.Id;
                docList.Add(d1);
            }

            docList.Sort();
            dt = ListToDataTable<DocInfo>(docList);
        }

        private string getAppNameFromConsumerActionItem(ReadOnlyConsumerActionItem request)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(request.BorrowerName);
            if (request.BorrowerName.Length != 0 && request.CoborrowerName.Length != 0)
                sb.Append(" & ");
            sb.Append(request.CoborrowerName);
            
            return sb.ToString();
        }

        private void FillDocsToDownload(CPageData dataLoan, out DataTable dt)
        {
            List<DocInfo> docList = new List<DocInfo>();
            List<EDocument> docs = (List<EDocument>)EDocument.GetDocsAvailableForDownload(CPTools.CurrentUser, dataLoan.sLId);

            foreach (EDocument doc in docs)
            {
                DocInfo d1 = new DocInfo();
                Guid appId = (doc.AppId.HasValue) ? doc.AppId.Value : Guid.Empty;
                CAppData dApp = dataLoan.GetAppData(appId);

                if (dApp == null)
                {
                    Tools.LogBug("Null value returned when trying to read Application " + appId + " from loan " + dataLoan.sLId + ".");
                    continue;
                }

                d1.DocTitle = doc.DocTypeName;
                d1.AppBorrowers = dApp.aAppCPortalFullNm;
                d1.docId = doc.DocumentId;
                docList.Add(d1);
            }
            docList.Add(new DocInfo()
            { 
                DocTitle = "eSign Disclosure and Consent",
                AppBorrowers =  CPTools.CurrentUser.Email,
                docId  = Guid.Empty
            });

            docList.Sort();
            dt = ListToDataTable<DocInfo>(docList);
        }

        private void FillTeamInfo(CPageData dataLoan, List<object> teamMembers)
        {
            CEmployeeFields lo = dataLoan.sEmployeeLoanRep;
            CEmployeeFields proc = dataLoan.sEmployeeProcessor;

            if (lo != null)
            {
                TeamMember m1 = new TeamMember();
                m1.name = lo.FullName;
                m1.email = lo.Email;
                m1.type = (lo.Role == CEmployeeFields.E_Role.LoanRep) ? "Loan Officer" : "";
                m1.phone = lo.Phone;
                string imageId = lo.EmployeeId.ToString();
                if (CPTools.hasProfilePic(new Guid(imageId)))
                {
                    m1.img = ConstApp.ProfileImagePath + imageId;
                }

                EmployeeDB edb =  EmployeeDB.RetrieveById( CPTools.CurrentUser.BrokerId, lo.EmployeeId);
                m1.nmls = edb.LosIdentifier; 


                teamMembers.Add(m1);
            }

            if (proc != null)
            {
                TeamMember m1 = new TeamMember();
                m1.name = proc.FullName;
                m1.email = proc.Email;
                m1.type = (proc.Role == CEmployeeFields.E_Role.Processor) ? "Processor" : "";
                m1.phone = proc.Phone;
                string imageId = proc.EmployeeId.ToString();
                if (CPTools.hasProfilePic(new Guid(imageId)))
                {
                    m1.img = ConstApp.ProfileImagePath + imageId;
                }
                teamMembers.Add(m1);
            }

        }

        protected void m_teamDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (x_skipList.Contains(args.Item.ItemType))
            {
                return;
            }

            EncodedLabel type  = args.Item.FindControl("m_type") as EncodedLabel;
            Image img   = args.Item.FindControl("m_pic") as Image;
            EncodedLabel name  = args.Item.FindControl("m_name") as EncodedLabel;
            EncodedLabel email = args.Item.FindControl("m_email") as EncodedLabel;
            EncodedLabel phone = args.Item.FindControl("m_phone") as EncodedLabel;
            EncodedLabel nmls = args.Item.FindControl("m_nmls") as EncodedLabel;
            PlaceHolder typePanel = args.Item.FindControl("m_typePanel") as PlaceHolder;
            PlaceHolder imgPanel = args.Item.FindControl("m_picPanel") as PlaceHolder;
            PlaceHolder namePanel = args.Item.FindControl("m_namePanel") as PlaceHolder;
            PlaceHolder emailPanel = args.Item.FindControl("m_emailPanel") as PlaceHolder;
            PlaceHolder phonePanel = args.Item.FindControl("m_phonePanel") as PlaceHolder;
            PlaceHolder nmlsPanel = args.Item.FindControl("m_nmlsPanel") as PlaceHolder; 

            TeamMember p = args.Item.DataItem as TeamMember;
            
            type.Text = p.type;
            img.ImageUrl = p.img;
            img.AlternateText = p.name;
            name.Text = p.name;
            email.Text = p.email;
            phone.Text = p.phone;
            nmls.Text = p.nmls;

            typePanel.Visible = type.Text.Trim().Length != 0;
            imgPanel.Visible = img.ImageUrl.Trim().Length != 0;
            namePanel.Visible = name.Text.Trim().Length != 0;
            emailPanel.Visible = email.Text.Trim().Length != 0;
            phonePanel.Visible = phone.Text.Trim().Length != 0;
            nmlsPanel.Visible = nmls.Text.Trim().Length != 0;
        }

        protected void m_DocsToBeSignedDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (x_skipList.Contains(args.Item.ItemType))
            {
                return;
            }

            EncodedLabel DocTitle = args.Item.FindControl("DocTitle") as EncodedLabel;
            EncodedLabel AppBorrowers = args.Item.FindControl("AppBorrowers") as EncodedLabel;
            Image UserAction = args.Item.FindControl("UserAction") as Image;
            
            HtmlAnchor signBtn = args.Item.FindControl("signBtn") as HtmlAnchor;
            HtmlAnchor faxCoverBtn = args.Item.FindControl("faxCoverBtn") as HtmlAnchor;
            EncodedLabel signActionMsg = args.Item.FindControl("signActionMsg") as EncodedLabel;
            EncodedLabel ReceivedActionMsg = args.Item.FindControl("ReceivedActionMsg") as EncodedLabel;

            PlaceHolder ActionPanel1 = args.Item.FindControl("ActionPanel1") as PlaceHolder;
            PlaceHolder ActionPanel2 = args.Item.FindControl("ActionPanel2") as PlaceHolder;
            PlaceHolder ActionPanel3 = args.Item.FindControl("ActionPanel3") as PlaceHolder;
            
            HtmlAnchor signFaxBtn = args.Item.FindControl("signFaxBtn") as HtmlAnchor;

            HtmlInputHidden docIdHolder = (HtmlInputHidden)args.Item.FindControl("docId");

            DocInfo doc = args.Item.DataItem as DocInfo;

            DocTitle.Text = doc.DocTitle;
            AppBorrowers.Text = doc.AppBorrowers;

            if (!doc.IsSigned)
            {
                UserAction.Visible = doc.RequireAction;
                if (!doc.IsWrittenRequired)
                {
                    
                    signBtn.Visible = doc.CanSign;
                    signActionMsg.Text = doc.SignDesc;
                    if (!doc.CanSign)
                    {
                        if (doc.HasSignAction)
                        {
                            ActionPanel1.Visible = false;
                            ActionPanel2.Visible = false;
                        }
                        else
                        {
                            ActionPanel1.Visible = false; //Nobody signed. This user cannot sign but it can still be faxed in.
                            ActionPanel2.Visible = true;
                        }
                    }
                    else
                    {
                        ActionPanel1.Visible = true;
                        ActionPanel2.Visible = false;
                        ActionPanel3.Visible = !doc.HasSignAction;
                    }
                    
                }
                else
                {
                    ActionPanel1.Visible = false;
                    ActionPanel2.Visible = true;

                    signActionMsg.Text = doc.SignDesc;
                }
                
                faxCoverBtn.HRef = "FaxCover.aspx?faxid=" + Uri.EscapeDataString(doc.faxId.ToString());
                signFaxBtn.HRef = "FaxCover.aspx?faxid=" + Uri.EscapeDataString(doc.faxId.ToString());
                docIdHolder.Value = doc.faxId.ToString();
                signBtn.HRef = "#";
                signBtn.Attributes.Add("onclick", "return f_sign('" + AspxTools.JsStringUnquoted(doc.signId.ToString()) + "')");
            }
            else
            {
                ReceivedActionMsg.Text = doc.SignDesc;
                ActionPanel1.Visible = false;
                ActionPanel2.Visible = false;
            }

        }

        public static DataTable ListToDataTable<T>(List<T> list)
        {
            DataTable dt = new DataTable();

            foreach (PropertyInfo info in typeof(T).GetProperties())
            {
                dt.Columns.Add(new DataColumn(info.Name, info.PropertyType));
            }
            foreach (T t in list)
            {
                DataRow row = dt.NewRow();
                foreach (PropertyInfo info in typeof(T).GetProperties())
                {
                    row[info.Name] = info.GetValue(t, null);
                }
                dt.Rows.Add(row);
            }
            return dt;
        }
    }

    public class TeamMember
    {
        public string type;
        public string img;
        public string name;
        public string email;
        public string phone;
        public string nmls;
    }

    public class DocInfo : IComparable
    {
        public string DocTitle { get; set; }
        public string AppBorrowers { get; set; }
        public string DocDesc { get; set; }
        public string SignDesc { get; set; }
        public bool   IsSigned { get; set; }
        public bool IsSent { get; set; }
        public bool HasSignAction { get; set; }
        public bool IsWrittenRequired { get; set; }
        public long faxId { get; set; }
        public Guid docId { get; set; }
        public long signId { get; set; }
        public bool CanSign { get; set; }
        public bool RequireAction { get; set; }
        public bool IsDisclosure { get; set; }

        public DocInfo()
        {
            faxId = -1;
            docId = Guid.Empty;
            signId = -1;
            IsSigned = false;
            IsSent = false;
            HasSignAction = false;
            IsWrittenRequired = false;
            CanSign = true;
            RequireAction = false;
        }

        public int CompareTo(object o)
        {
            if (o == null) return -1;
            DocInfo d1 = o as DocInfo;
            int comp = string.Compare(DocTitle, d1.DocTitle, true);
            if (comp != 0) return comp;
            return string.Compare(AppBorrowers, d1.AppBorrowers, true);
        }
    }

    
}
