﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using DataAccess;
using System.Text;
using ConsumerPortal.common;
using LendersOffice.Common;
using System.Threading;
using LendersOffice.ObjLib.Security;

namespace ConsumerPortal
{
    public partial class Logger : BaseCPortalPage
    {
        protected void Page_Load(object sender, EventArgs e){ }

        [WebMethod]
        public static bool LogError(string error, List<string> exception, List<string> stacktrace, string page, string type)
        {
            ConsumerUserPrincipal consumer = Thread.CurrentPrincipal as ConsumerUserPrincipal;

            try
            {
                StringBuilder sb = new StringBuilder();
                
                string msg = string.IsNullOrEmpty(error) ? "empty" : error;
                string p = string.IsNullOrEmpty(page) ? "empty" : page;
                string cat = string.IsNullOrEmpty(type) ? "ERROR" : type;
                
                sb.AppendLine("JS " + cat);
                sb.AppendLine(string.Format("- Page: '{0}'", p));
                sb.AppendLine(string.Format("- Message: '{0}'.", msg));

                if (exception != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("- Exception:");
                    foreach (string str in exception)
                    {
                        sb.AppendLine(str);
                    }
                }

                if (stacktrace != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("- StackTrace:");
                    foreach (string str in stacktrace)
                    {
                        sb.AppendLine(str); 
                    }
                }
                
                if (consumer != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("- Consumer Info:");
                    sb.AppendLine(consumer.ToString());
                }

                switch (cat)
                {
                    case "INFO":
                        Tools.LogInfo(sb.ToString());
                        break;
                    case "WARN":
                        Tools.LogWarning(sb.ToString());
                        break;
                    case "ERROR":
                    default:
                        Tools.LogError(sb.ToString());
                        break;
                }
                return true;
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                return false;
            }
        }
    }
}
